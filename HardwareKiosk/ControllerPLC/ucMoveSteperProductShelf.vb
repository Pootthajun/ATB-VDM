﻿Imports System.Windows.Forms
Public Class ucMoveSteperProductShelf
    Private Sub SetShelfPosition(PosX As Integer, PosY As Integer)
        txtPositionX.Text = PosX
        txtPositionY.Text = PosY
    End Sub

    Public Sub BindShelfList(dt As DataTable)

        Dim dr As DataRow = dt.NewRow
        dr("shelf_position") = "select"
        dr("id") = "0"
        dt.Rows.InsertAt(dr, 0)

        cbShelf.DisplayMember = "shelf_position"
        cbShelf.ValueMember = "id"
        cbShelf.DataSource = dt
    End Sub

    Private Sub cbShelf_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cbShelf.SelectionChangeCommitted
        If cbShelf.SelectedValue <> "0" Then
            Dim dt As New DataTable
            dt = DirectCast(cbShelf.DataSource, DataTable).Copy
            dt.DefaultView.RowFilter = "shelf_id=" & cbShelf.SelectedValue
            If dt.DefaultView.Count > 0 Then
                txtPositionX.Text = dt.DefaultView(0)("position_x")
                txtPositionY.Text = dt.DefaultView(0)("position_y")
            End If
            dt.DefaultView.RowFilter = ""
        Else
            txtPositionX.Text = ""
            txtPositionY.Text = ""
        End If
    End Sub

    Private Sub btnMove_Click(sender As Object, e As EventArgs) Handles btnMove.Click
        If txtPositionX.Text.Trim = "" Then
            MessageBox.Show("Please input Position X")
            txtPositionX.Focus()
            Exit Sub
        End If

        If txtPositionY.Text.Trim = "" Then
            MessageBox.Show("Please input Position Y")
            txtPositionY.Focus()
            Exit Sub
        End If

        SteperMotorPLC.MoveStepMotor(txtPositionX.Text, txtPositionY.Text)
    End Sub
End Class
