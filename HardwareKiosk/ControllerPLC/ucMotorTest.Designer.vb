﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ucMotorTest
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblHead = New System.Windows.Forms.Label()
        Me.cbbLeft = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbbRight = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnLeftStart = New System.Windows.Forms.Button()
        Me.btnRightStart = New System.Windows.Forms.Button()
        Me.btnLeftStop = New System.Windows.Forms.Button()
        Me.btnRightStop = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblHead
        '
        Me.lblHead.BackColor = System.Drawing.Color.SteelBlue
        Me.lblHead.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHead.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblHead.ForeColor = System.Drawing.Color.White
        Me.lblHead.Location = New System.Drawing.Point(0, 0)
        Me.lblHead.Name = "lblHead"
        Me.lblHead.Size = New System.Drawing.Size(355, 30)
        Me.lblHead.TabIndex = 40
        Me.lblHead.Text = "Mortor"
        Me.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cbbLeft
        '
        Me.cbbLeft.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbLeft.FormattingEnabled = True
        Me.cbbLeft.Location = New System.Drawing.Point(87, 39)
        Me.cbbLeft.Name = "cbbLeft"
        Me.cbbLeft.Size = New System.Drawing.Size(107, 24)
        Me.cbbLeft.TabIndex = 42
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 17)
        Me.Label1.TabIndex = 41
        Me.Label1.Text = "Left Pin :"
        '
        'cbbRight
        '
        Me.cbbRight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbRight.FormattingEnabled = True
        Me.cbbRight.Location = New System.Drawing.Point(87, 69)
        Me.cbbRight.Name = "cbbRight"
        Me.cbbRight.Size = New System.Drawing.Size(107, 24)
        Me.cbbRight.TabIndex = 44
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 17)
        Me.Label2.TabIndex = 43
        Me.Label2.Text = "Right Pin :"
        '
        'btnLeftStart
        '
        Me.btnLeftStart.Location = New System.Drawing.Point(200, 36)
        Me.btnLeftStart.Name = "btnLeftStart"
        Me.btnLeftStart.Size = New System.Drawing.Size(69, 29)
        Me.btnLeftStart.TabIndex = 45
        Me.btnLeftStart.Text = "Start"
        Me.btnLeftStart.UseVisualStyleBackColor = True
        '
        'btnRightStart
        '
        Me.btnRightStart.Location = New System.Drawing.Point(200, 66)
        Me.btnRightStart.Name = "btnRightStart"
        Me.btnRightStart.Size = New System.Drawing.Size(69, 29)
        Me.btnRightStart.TabIndex = 46
        Me.btnRightStart.Text = "Start"
        Me.btnRightStart.UseVisualStyleBackColor = True
        '
        'btnLeftStop
        '
        Me.btnLeftStop.Location = New System.Drawing.Point(275, 36)
        Me.btnLeftStop.Name = "btnLeftStop"
        Me.btnLeftStop.Size = New System.Drawing.Size(69, 29)
        Me.btnLeftStop.TabIndex = 47
        Me.btnLeftStop.Text = "Stop"
        Me.btnLeftStop.UseVisualStyleBackColor = True
        '
        'btnRightStop
        '
        Me.btnRightStop.Location = New System.Drawing.Point(275, 66)
        Me.btnRightStop.Name = "btnRightStop"
        Me.btnRightStop.Size = New System.Drawing.Size(69, 29)
        Me.btnRightStop.TabIndex = 48
        Me.btnRightStop.Text = "Stop"
        Me.btnRightStop.UseVisualStyleBackColor = True
        '
        'ucMotorTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me.btnRightStop)
        Me.Controls.Add(Me.btnLeftStop)
        Me.Controls.Add(Me.btnRightStart)
        Me.Controls.Add(Me.btnLeftStart)
        Me.Controls.Add(Me.cbbRight)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbbLeft)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblHead)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "ucMotorTest"
        Me.Size = New System.Drawing.Size(355, 103)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblHead As System.Windows.Forms.Label
    Friend WithEvents cbbLeft As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbbRight As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnLeftStart As System.Windows.Forms.Button
    Friend WithEvents btnRightStart As System.Windows.Forms.Button
    Friend WithEvents btnLeftStop As System.Windows.Forms.Button
    Friend WithEvents btnRightStop As System.Windows.Forms.Button
End Class
