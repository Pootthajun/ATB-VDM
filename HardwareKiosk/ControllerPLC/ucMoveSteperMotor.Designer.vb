﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucMoveSteperMotor
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblHead = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnMove = New System.Windows.Forms.Button()
        Me.numPositionX = New System.Windows.Forms.NumericUpDown()
        Me.numPositionY = New System.Windows.Forms.NumericUpDown()
        CType(Me.numPositionX, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numPositionY, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblHead
        '
        Me.lblHead.BackColor = System.Drawing.Color.SteelBlue
        Me.lblHead.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHead.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblHead.ForeColor = System.Drawing.Color.White
        Me.lblHead.Location = New System.Drawing.Point(0, 0)
        Me.lblHead.Name = "lblHead"
        Me.lblHead.Size = New System.Drawing.Size(200, 30)
        Me.lblHead.TabIndex = 32
        Me.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(108, 41)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(20, 13)
        Me.Label4.TabIndex = 37
        Me.Label4.Text = "Y :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 41)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(20, 13)
        Me.Label7.TabIndex = 36
        Me.Label7.Text = "X :"
        '
        'btnMove
        '
        Me.btnMove.Location = New System.Drawing.Point(43, 67)
        Me.btnMove.Name = "btnMove"
        Me.btnMove.Size = New System.Drawing.Size(106, 30)
        Me.btnMove.TabIndex = 40
        Me.btnMove.Text = "Move to"
        Me.btnMove.UseVisualStyleBackColor = True
        '
        'numPositionX
        '
        Me.numPositionX.Enabled = False
        Me.numPositionX.Location = New System.Drawing.Point(31, 39)
        Me.numPositionX.Name = "numPositionX"
        Me.numPositionX.Size = New System.Drawing.Size(61, 20)
        Me.numPositionX.TabIndex = 41
        '
        'numPositionY
        '
        Me.numPositionY.Enabled = False
        Me.numPositionY.Location = New System.Drawing.Point(130, 39)
        Me.numPositionY.Name = "numPositionY"
        Me.numPositionY.Size = New System.Drawing.Size(61, 20)
        Me.numPositionY.TabIndex = 42
        '
        'ucMoveSteperMotor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.numPositionY)
        Me.Controls.Add(Me.numPositionX)
        Me.Controls.Add(Me.btnMove)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblHead)
        Me.Name = "ucMoveSteperMotor"
        Me.Size = New System.Drawing.Size(200, 100)
        CType(Me.numPositionX, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numPositionY, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblHead As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnMove As System.Windows.Forms.Button
    Friend WithEvents numPositionX As System.Windows.Forms.NumericUpDown
    Friend WithEvents numPositionY As System.Windows.Forms.NumericUpDown
End Class
