﻿Imports System.Windows.Forms
Public Class ucMoveSteperMotor
    Public Property Header() As String
        Get
            Return lblHead.Text.Trim
        End Get
        Set(value As String)
            lblHead.Text = value
        End Set
    End Property

    Public WriteOnly Property EnablePosition() As Boolean
        Set(value As Boolean)
            numPositionX.Enabled = value
            numPositionY.Enabled = value
        End Set
    End Property

    Public Sub SetTextPosition(PosX As Integer, PosY As Integer)
        numPositionX.Value = PosX
        numPositionY.Value = PosY
    End Sub

    Private Sub btnMove_Click(sender As Object, e As EventArgs) Handles btnMove.Click
        If numPositionX.Text.Trim = "" Then
            MessageBox.Show("Please input Position X")
            numPositionX.Focus()
            Exit Sub
        End If

        If numPositionY.Text.Trim = "" Then
            MessageBox.Show("Please input Position Y")
            numPositionY.Focus()
            Exit Sub
        End If

        SteperMotorPLC.MoveStepMotor(numPositionX.Text, numPositionY.Text)
    End Sub
End Class
