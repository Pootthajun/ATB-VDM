﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucMoveSteperProductShelf
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbShelf = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblHead = New System.Windows.Forms.Label()
        Me.btnMove = New System.Windows.Forms.Button()
        Me.txtPositionY = New System.Windows.Forms.TextBox()
        Me.txtPositionX = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cbShelf
        '
        Me.cbShelf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbShelf.FormattingEnabled = True
        Me.cbShelf.Location = New System.Drawing.Point(41, 33)
        Me.cbShelf.Name = "cbShelf"
        Me.cbShelf.Size = New System.Drawing.Size(164, 21)
        Me.cbShelf.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Shelf"
        '
        'lblHead
        '
        Me.lblHead.BackColor = System.Drawing.Color.SteelBlue
        Me.lblHead.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblHead.ForeColor = System.Drawing.Color.White
        Me.lblHead.Location = New System.Drawing.Point(3, 0)
        Me.lblHead.Name = "lblHead"
        Me.lblHead.Size = New System.Drawing.Size(218, 30)
        Me.lblHead.TabIndex = 31
        Me.lblHead.Text = "Move to Shelf"
        Me.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnMove
        '
        Me.btnMove.Location = New System.Drawing.Point(55, 90)
        Me.btnMove.Name = "btnMove"
        Me.btnMove.Size = New System.Drawing.Size(106, 30)
        Me.btnMove.TabIndex = 45
        Me.btnMove.Text = "Move to"
        Me.btnMove.UseVisualStyleBackColor = True
        '
        'txtPositionY
        '
        Me.txtPositionY.Enabled = False
        Me.txtPositionY.Location = New System.Drawing.Point(146, 61)
        Me.txtPositionY.Name = "txtPositionY"
        Me.txtPositionY.Size = New System.Drawing.Size(59, 20)
        Me.txtPositionY.TabIndex = 44
        '
        'txtPositionX
        '
        Me.txtPositionX.Enabled = False
        Me.txtPositionX.Location = New System.Drawing.Point(41, 61)
        Me.txtPositionX.Name = "txtPositionX"
        Me.txtPositionX.Size = New System.Drawing.Size(59, 20)
        Me.txtPositionX.TabIndex = 43
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(120, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(20, 13)
        Me.Label4.TabIndex = 42
        Me.Label4.Text = "Y :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(19, 64)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(20, 13)
        Me.Label7.TabIndex = 41
        Me.Label7.Text = "X :"
        '
        'ucMoveSteperProductShelf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnMove)
        Me.Controls.Add(Me.txtPositionY)
        Me.Controls.Add(Me.txtPositionX)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblHead)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbShelf)
        Me.Name = "ucMoveSteperProductShelf"
        Me.Size = New System.Drawing.Size(224, 129)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbShelf As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblHead As System.Windows.Forms.Label
    Friend WithEvents btnMove As System.Windows.Forms.Button
    Friend WithEvents txtPositionY As System.Windows.Forms.TextBox
    Friend WithEvents txtPositionX As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
