﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucSensorTest
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.cbbPin = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnLeftStop = New System.Windows.Forms.Button()
        Me.btnLeftStart = New System.Windows.Forms.Button()
        Me.lblHead = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(9, 67)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(36, 23)
        Me.TextBox1.TabIndex = 62
        '
        'cbbPin
        '
        Me.cbbPin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbPin.FormattingEnabled = True
        Me.cbbPin.Location = New System.Drawing.Point(53, 33)
        Me.cbbPin.Name = "cbbPin"
        Me.cbbPin.Size = New System.Drawing.Size(160, 24)
        Me.cbbPin.TabIndex = 54
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 17)
        Me.Label1.TabIndex = 53
        Me.Label1.Text = "Pin :"
        '
        'btnLeftStop
        '
        Me.btnLeftStop.Location = New System.Drawing.Point(134, 64)
        Me.btnLeftStop.Name = "btnLeftStop"
        Me.btnLeftStop.Size = New System.Drawing.Size(79, 29)
        Me.btnLeftStop.TabIndex = 64
        Me.btnLeftStop.Text = "Stop"
        Me.btnLeftStop.UseVisualStyleBackColor = True
        '
        'btnLeftStart
        '
        Me.btnLeftStart.Location = New System.Drawing.Point(53, 64)
        Me.btnLeftStart.Name = "btnLeftStart"
        Me.btnLeftStart.Size = New System.Drawing.Size(72, 29)
        Me.btnLeftStart.TabIndex = 63
        Me.btnLeftStart.Text = "Start"
        Me.btnLeftStart.UseVisualStyleBackColor = True
        '
        'lblHead
        '
        Me.lblHead.BackColor = System.Drawing.Color.SteelBlue
        Me.lblHead.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHead.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblHead.ForeColor = System.Drawing.Color.White
        Me.lblHead.Location = New System.Drawing.Point(0, 0)
        Me.lblHead.Name = "lblHead"
        Me.lblHead.Size = New System.Drawing.Size(226, 30)
        Me.lblHead.TabIndex = 65
        Me.lblHead.Text = "Sensor"
        Me.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ucSensorTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me.lblHead)
        Me.Controls.Add(Me.btnLeftStop)
        Me.Controls.Add(Me.btnLeftStart)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.cbbPin)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "ucSensorTest"
        Me.Size = New System.Drawing.Size(226, 106)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents cbbPin As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnLeftStop As System.Windows.Forms.Button
    Friend WithEvents btnLeftStart As System.Windows.Forms.Button
    Friend WithEvents lblHead As System.Windows.Forms.Label
End Class
