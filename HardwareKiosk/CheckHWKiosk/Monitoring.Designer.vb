﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Monitoring
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tbpFunction = New System.Windows.Forms.TabPage()
        Me.UcMotorTest1 = New ControllerPLC.ucMotorTest()
        Me.UcSensorTest1 = New ControllerPLC.ucSensorTest()
        Me.stpMovePosition = New ControllerPLC.ucMoveSteperMotor()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbComport = New System.Windows.Forms.ComboBox()
        Me.txtStatus = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblHead = New System.Windows.Forms.Label()
        Me.stpMoveToShelf = New ControllerPLC.ucMoveSteperProductShelf()
        Me.stpMoveOrgin = New ControllerPLC.ucMoveSteperMotor()
        Me.stpMovePark = New ControllerPLC.ucMoveSteperMotor()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtUserID = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDatabaseName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtServerName = New System.Windows.Forms.TextBox()
        Me.lblCaptionServername = New System.Windows.Forms.Label()
        Me.btnDbConnect = New System.Windows.Forms.Button()
        Me.PrinterConnectDevice1 = New Printer.PrinterConnectDevice()
        Me.CoinOut5 = New CoinOut.CoinOutConnectDevice()
        Me.CoinInConnectDevice1 = New CoinIn.CoinInConnectDevice()
        Me.btnSaveDeviceConfig = New System.Windows.Forms.Button()
        Me.CashInConnectDevice1 = New BanknoteIn.BanknoteInConnectDevice()
        Me.BanknoteOut100 = New BanknoteOut.CashOutConnectDevice()
        Me.BanknoteOut20 = New BanknoteOut.CashOutConnectDevice()
        Me.TabControl1.SuspendLayout()
        Me.tbpFunction.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.tbpFunction)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(3, 212)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1226, 514)
        Me.TabControl1.TabIndex = 6
        '
        'tbpFunction
        '
        Me.tbpFunction.Controls.Add(Me.UcMotorTest1)
        Me.tbpFunction.Controls.Add(Me.UcSensorTest1)
        Me.tbpFunction.Controls.Add(Me.stpMovePosition)
        Me.tbpFunction.Controls.Add(Me.Panel1)
        Me.tbpFunction.Controls.Add(Me.stpMoveToShelf)
        Me.tbpFunction.Controls.Add(Me.stpMoveOrgin)
        Me.tbpFunction.Controls.Add(Me.stpMovePark)
        Me.tbpFunction.Location = New System.Drawing.Point(4, 22)
        Me.tbpFunction.Name = "tbpFunction"
        Me.tbpFunction.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpFunction.Size = New System.Drawing.Size(1218, 488)
        Me.tbpFunction.TabIndex = 0
        Me.tbpFunction.Text = "Function"
        Me.tbpFunction.UseVisualStyleBackColor = True
        '
        'UcMotorTest1
        '
        Me.UcMotorTest1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.UcMotorTest1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.UcMotorTest1.Location = New System.Drawing.Point(642, 5)
        Me.UcMotorTest1.Margin = New System.Windows.Forms.Padding(4)
        Me.UcMotorTest1.Name = "UcMotorTest1"
        Me.UcMotorTest1.Size = New System.Drawing.Size(355, 100)
        Me.UcMotorTest1.TabIndex = 26
        '
        'UcSensorTest1
        '
        Me.UcSensorTest1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.UcSensorTest1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.UcSensorTest1.Location = New System.Drawing.Point(412, 5)
        Me.UcSensorTest1.Margin = New System.Windows.Forms.Padding(4)
        Me.UcSensorTest1.Name = "UcSensorTest1"
        Me.UcSensorTest1.Size = New System.Drawing.Size(226, 100)
        Me.UcSensorTest1.TabIndex = 25
        '
        'stpMovePosition
        '
        Me.stpMovePosition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stpMovePosition.Header = "Steper Motor"
        Me.stpMovePosition.Location = New System.Drawing.Point(209, 6)
        Me.stpMovePosition.Name = "stpMovePosition"
        Me.stpMovePosition.Size = New System.Drawing.Size(200, 100)
        Me.stpMovePosition.TabIndex = 24
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cbComport)
        Me.Panel1.Controls.Add(Me.txtStatus)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.lblHead)
        Me.Panel1.Location = New System.Drawing.Point(3, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(200, 100)
        Me.Panel1.TabIndex = 23
        '
        'cbComport
        '
        Me.cbComport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbComport.FormattingEnabled = True
        Me.cbComport.Location = New System.Drawing.Point(64, 38)
        Me.cbComport.Name = "cbComport"
        Me.cbComport.Size = New System.Drawing.Size(121, 21)
        Me.cbComport.TabIndex = 34
        '
        'txtStatus
        '
        Me.txtStatus.BackColor = System.Drawing.Color.Black
        Me.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtStatus.ForeColor = System.Drawing.Color.Lime
        Me.txtStatus.Location = New System.Drawing.Point(64, 70)
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(121, 23)
        Me.txtStatus.TabIndex = 33
        Me.txtStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 73)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Status :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 41)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 13)
        Me.Label7.TabIndex = 31
        Me.Label7.Text = "Com Port :"
        '
        'lblHead
        '
        Me.lblHead.BackColor = System.Drawing.Color.SteelBlue
        Me.lblHead.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblHead.ForeColor = System.Drawing.Color.White
        Me.lblHead.Location = New System.Drawing.Point(2, 1)
        Me.lblHead.Name = "lblHead"
        Me.lblHead.Size = New System.Drawing.Size(198, 30)
        Me.lblHead.TabIndex = 23
        Me.lblHead.Text = "Controller"
        Me.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'stpMoveToShelf
        '
        Me.stpMoveToShelf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stpMoveToShelf.Location = New System.Drawing.Point(415, 130)
        Me.stpMoveToShelf.Name = "stpMoveToShelf"
        Me.stpMoveToShelf.Size = New System.Drawing.Size(224, 129)
        Me.stpMoveToShelf.TabIndex = 2
        '
        'stpMoveOrgin
        '
        Me.stpMoveOrgin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stpMoveOrgin.Header = "Origin"
        Me.stpMoveOrgin.Location = New System.Drawing.Point(209, 130)
        Me.stpMoveOrgin.Name = "stpMoveOrgin"
        Me.stpMoveOrgin.Size = New System.Drawing.Size(200, 100)
        Me.stpMoveOrgin.TabIndex = 1
        '
        'stpMovePark
        '
        Me.stpMovePark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stpMovePark.Header = "Park"
        Me.stpMovePark.Location = New System.Drawing.Point(3, 130)
        Me.stpMovePark.Name = "stpMovePark"
        Me.stpMovePark.Size = New System.Drawing.Size(200, 100)
        Me.stpMovePark.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1218, 488)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Flow"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.txtPassword)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtUserID)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtDatabaseName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtServerName)
        Me.GroupBox1.Controls.Add(Me.lblCaptionServername)
        Me.GroupBox1.Controls.Add(Me.btnDbConnect)
        Me.GroupBox1.Location = New System.Drawing.Point(971, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(258, 149)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Database Information"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(97, 86)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(145, 20)
        Me.txtPassword.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(38, 89)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Password"
        '
        'txtUserID
        '
        Me.txtUserID.Location = New System.Drawing.Point(97, 63)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(145, 20)
        Me.txtUserID.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(51, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "UserID"
        '
        'txtDatabaseName
        '
        Me.txtDatabaseName.Location = New System.Drawing.Point(97, 40)
        Me.txtDatabaseName.Name = "txtDatabaseName"
        Me.txtDatabaseName.Size = New System.Drawing.Size(145, 20)
        Me.txtDatabaseName.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "DatabaseName"
        '
        'txtServerName
        '
        Me.txtServerName.Location = New System.Drawing.Point(97, 17)
        Me.txtServerName.Name = "txtServerName"
        Me.txtServerName.Size = New System.Drawing.Size(145, 20)
        Me.txtServerName.TabIndex = 2
        '
        'lblCaptionServername
        '
        Me.lblCaptionServername.AutoSize = True
        Me.lblCaptionServername.Location = New System.Drawing.Point(25, 20)
        Me.lblCaptionServername.Name = "lblCaptionServername"
        Me.lblCaptionServername.Size = New System.Drawing.Size(66, 13)
        Me.lblCaptionServername.TabIndex = 1
        Me.lblCaptionServername.Text = "ServerName"
        '
        'btnDbConnect
        '
        Me.btnDbConnect.Location = New System.Drawing.Point(97, 108)
        Me.btnDbConnect.Name = "btnDbConnect"
        Me.btnDbConnect.Size = New System.Drawing.Size(145, 34)
        Me.btnDbConnect.TabIndex = 0
        Me.btnDbConnect.Text = "Connect"
        Me.btnDbConnect.UseVisualStyleBackColor = True
        '
        'PrinterConnectDevice1
        '
        Me.PrinterConnectDevice1.BackColor = System.Drawing.Color.White
        Me.PrinterConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PrinterConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PrinterConnectDevice1.Location = New System.Drawing.Point(643, 106)
        Me.PrinterConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.PrinterConnectDevice1.Name = "PrinterConnectDevice1"
        Me.PrinterConnectDevice1.Size = New System.Drawing.Size(320, 99)
        Me.PrinterConnectDevice1.TabIndex = 5
        '
        'CoinOut5
        '
        Me.CoinOut5.BackColor = System.Drawing.Color.White
        Me.CoinOut5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CoinOut5.CoinValue = 5
        Me.CoinOut5.ComportName = ""
        Me.CoinOut5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CoinOut5.Location = New System.Drawing.Point(325, 106)
        Me.CoinOut5.Margin = New System.Windows.Forms.Padding(4)
        Me.CoinOut5.Name = "CoinOut5"
        Me.CoinOut5.Size = New System.Drawing.Size(320, 99)
        Me.CoinOut5.TabIndex = 4
        '
        'CoinInConnectDevice1
        '
        Me.CoinInConnectDevice1.BackColor = System.Drawing.Color.White
        Me.CoinInConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CoinInConnectDevice1.ComportName = ""
        Me.CoinInConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CoinInConnectDevice1.Location = New System.Drawing.Point(3, 106)
        Me.CoinInConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.CoinInConnectDevice1.Name = "CoinInConnectDevice1"
        Me.CoinInConnectDevice1.Size = New System.Drawing.Size(320, 99)
        Me.CoinInConnectDevice1.TabIndex = 1
        '
        'btnSaveDeviceConfig
        '
        Me.btnSaveDeviceConfig.BackColor = System.Drawing.Color.LimeGreen
        Me.btnSaveDeviceConfig.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnSaveDeviceConfig.Location = New System.Drawing.Point(970, 172)
        Me.btnSaveDeviceConfig.Name = "btnSaveDeviceConfig"
        Me.btnSaveDeviceConfig.Size = New System.Drawing.Size(255, 34)
        Me.btnSaveDeviceConfig.TabIndex = 9
        Me.btnSaveDeviceConfig.Text = "Save Device Config"
        Me.btnSaveDeviceConfig.UseVisualStyleBackColor = False
        '
        'CashInConnectDevice1
        '
        Me.CashInConnectDevice1.BackColor = System.Drawing.Color.White
        Me.CashInConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CashInConnectDevice1.ComportName = ""
        Me.CashInConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CashInConnectDevice1.Location = New System.Drawing.Point(3, 3)
        Me.CashInConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.CashInConnectDevice1.Name = "CashInConnectDevice1"
        Me.CashInConnectDevice1.Size = New System.Drawing.Size(320, 99)
        Me.CashInConnectDevice1.TabIndex = 0
        '
        'BanknoteOut100
        '
        Me.BanknoteOut100.BackColor = System.Drawing.Color.White
        Me.BanknoteOut100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BanknoteOut100.CashValue = 100
        Me.BanknoteOut100.ComportName = ""
        Me.BanknoteOut100.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BanknoteOut100.Location = New System.Drawing.Point(643, 3)
        Me.BanknoteOut100.Margin = New System.Windows.Forms.Padding(4)
        Me.BanknoteOut100.Name = "BanknoteOut100"
        Me.BanknoteOut100.Size = New System.Drawing.Size(320, 99)
        Me.BanknoteOut100.TabIndex = 3
        '
        'BanknoteOut20
        '
        Me.BanknoteOut20.BackColor = System.Drawing.Color.White
        Me.BanknoteOut20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BanknoteOut20.CashValue = 20
        Me.BanknoteOut20.ComportName = ""
        Me.BanknoteOut20.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BanknoteOut20.Location = New System.Drawing.Point(325, 3)
        Me.BanknoteOut20.Margin = New System.Windows.Forms.Padding(4)
        Me.BanknoteOut20.Name = "BanknoteOut20"
        Me.BanknoteOut20.Size = New System.Drawing.Size(320, 99)
        Me.BanknoteOut20.TabIndex = 2
        '
        'Monitoring
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1241, 738)
        Me.Controls.Add(Me.btnSaveDeviceConfig)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.PrinterConnectDevice1)
        Me.Controls.Add(Me.CoinOut5)
        Me.Controls.Add(Me.BanknoteOut100)
        Me.Controls.Add(Me.BanknoteOut20)
        Me.Controls.Add(Me.CoinInConnectDevice1)
        Me.Controls.Add(Me.CashInConnectDevice1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Name = "Monitoring"
        Me.Text = "Monitoring"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TabControl1.ResumeLayout(False)
        Me.tbpFunction.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    'Friend WithEvents ScanPassportConnectDevice1 As ScanPassport.ScanPassportConnectDevice
    Friend WithEvents CashInConnectDevice1 As BanknoteIn.BanknoteInConnectDevice
    Friend WithEvents CoinInConnectDevice1 As CoinIn.CoinInConnectDevice
    Friend WithEvents BanknoteOut20 As BanknoteOut.CashOutConnectDevice
    Friend WithEvents BanknoteOut100 As BanknoteOut.CashOutConnectDevice
    Friend WithEvents CoinOut5 As CoinOut.CoinOutConnectDevice
    Friend WithEvents PrinterConnectDevice1 As Printer.PrinterConnectDevice
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents tbpFunction As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtUserID As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtDatabaseName As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtServerName As TextBox
    Friend WithEvents lblCaptionServername As Label
    Friend WithEvents btnDbConnect As Button
    Friend WithEvents stpMovePark As ControllerPLC.ucMoveSteperMotor
    Friend WithEvents stpMoveOrgin As ControllerPLC.ucMoveSteperMotor
    Friend WithEvents stpMoveToShelf As ControllerPLC.ucMoveSteperProductShelf
    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblHead As Label
    Friend WithEvents cbComport As ComboBox
    Friend WithEvents txtStatus As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents stpMovePosition As ControllerPLC.ucMoveSteperMotor
    Friend WithEvents UcSensorTest1 As ControllerPLC.ucSensorTest
    Friend WithEvents UcMotorTest1 As ControllerPLC.ucMotorTest
    Friend WithEvents btnSaveDeviceConfig As Button
End Class
