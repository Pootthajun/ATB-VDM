﻿Imports System.Data.SqlClient
Imports Engine

Public Class Monitoring
    Dim INIFileName As String = Application.StartupPath & "\ConfigDevice.ini"
    Dim _VendingID As Long = 0

    Private Sub Monitoring_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        stpMovePosition.EnablePosition = True

        FillInDatabaseInfo()
        FillInVendingSysconfig()
        SetDeviceConfig()

        BindShelfList()
    End Sub


    Private Sub FillInVendingSysconfig()
        Try
            Dim sql As String = "select top 1 ms_vending_id, motor_park_position, motor_origin_position, motor_delivery_position"
            sql += " from cf_vending_sysconfig"

            Dim dt As DataTable = ExecuteTable(sql, Nothing)
            If dt.Rows.Count > 0 Then
                _VendingID = dt.Rows(0)("ms_vending_id")

                Dim PosPark() As String = Split(dt.Rows(0)("motor_park_position"), ",")
                Dim PosOrgin() As String = Split(dt.Rows(0)("motor_origin_position"), ",")
                Dim PosDelivery() As String = Split(dt.Rows(0)("motor_delivery_position"), ",")

                If PosPark.Length = 2 Then
                    stpMovePark.SetTextPosition(PosPark(0), PosPark(1))
                End If

                If PosOrgin.Length = 2 Then
                    stpMoveOrgin.SetTextPosition(PosOrgin(0), PosOrgin(1))
                End If
            End If
            dt.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindShelfList()
        Dim sql As String = "select id, position_x, position_y, 'Row ' + ltrim(str(position_row)) + ' Column ' + ltrim(str(position_column)) as shelf_position, shelf_id"
        sql += " from ms_vending_shelf "
        sql += " order by shelf_id"

        Dim dt As DataTable = ExecuteTable(sql, Nothing)
        If dt.Rows.Count > 0 Then
            stpMoveToShelf.BindShelfList(dt)
        End If

    End Sub

    Private Sub SetDeviceConfig()
        Dim sql As String = "select ms_device_id, comport_vid, driver_name1, driver_name2"
        sql += " from MS_VENDING_DEVICE "
        sql += " where ms_vending_id=@_VENDING_ID"

        Dim p(1) As SqlParameter
        p(0) = SetParameter("@_VENDING_ID", SqlDbType.BigInt, _VendingID)

        Dim dt As DataTable = ExecuteTable(sql, p)
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim ddr As DataRow = dt.Rows(i)

                Select Case Convert.ToInt16(ddr("ms_device_id"))
                    Case DeviceID.BankNoteIn
                        If Convert.IsDBNull(ddr("comport_vid")) = False Then
                            'VendingConfig.BanknoteInComport = dDr("comport_vid")
                            CashInConnectDevice1.ComportName = ddr("comport_vid")
                        End If
                    Case DeviceID.BankNoteOut_20
                        If Convert.IsDBNull(ddr("comport_vid")) = False Then
                            'VendingConfig.BanknoteOUT20Comport = dDr("comport_vid")
                            BanknoteOut20.ComportName = ddr("comport_vid")
                        End If

                    Case DeviceID.BankNoteOut_50
                        If Convert.IsDBNull(ddr("comport_vid")) = False Then
                            'VendingConfig.BanknoteOUT50Comport = dDr("comport_vid")
                            'BanknoteOut50.ComportName = ddr("comport_vid")
                        End If

                    Case DeviceID.BankNoteOut_100
                        If Convert.IsDBNull(ddr("comport_vid")) = False Then
                            'VendingConfig.BanknoteOUT100Comport = dDr("comport_vid")
                            BanknoteOut100.ComportName = ddr("comport_vid")
                        End If

                    Case DeviceID.BankNoteOut_500
                        If Convert.IsDBNull(ddr("comport_vid")) = False Then
                            'VendingConfig.BanknoteOUT500Comport = dDr("comport_vid")
                            'BanknoteOut500.ComportName = ddr("comport_vid")
                        End If

                    Case DeviceID.CoinIn
                        If Convert.IsDBNull(ddr("comport_vid")) = False Then
                            'VendingConfig.CoinInComport = dDr("comport_vid")
                            CoinInConnectDevice1.ComportName = ddr("comport_vid")
                        End If

                    Case DeviceID.CoinOut_1
                        If Convert.IsDBNull(ddr("comport_vid")) = False Then
                            ' VendingConfig.CoinOut1Comport = dDr("comport_vid")
                        End If

                    Case DeviceID.CoinOut_2
                        If Convert.IsDBNull(ddr("comport_vid")) = False Then
                            ' VendingConfig.CoinOut2Comport = dDr("comport_vid")
                        End If

                    Case DeviceID.CoinOut_5
                        If Convert.IsDBNull(ddr("comport_vid")) = False Then
                            'VendingConfig.CoinOut5Comport = dDr("comport_vid")
                            CoinOut5.ComportName = ddr("comport_vid")
                        End If

                    Case DeviceID.CoinOut_10
                        If Convert.IsDBNull(ddr("comport_vid")) = False Then
                            'VendingConfig.CoinOut10Comport = dDr("comport_vid")
                        End If

                    Case DeviceID.Printer
                        If Convert.IsDBNull(ddr("driver_name1")) = False Then
                            'VendingConfig.PrinterDeviceName = dDr("driver_name1")
                            PrinterConnectDevice1.PrintertName = ddr("driver_name1")
                        End If
                    Case DeviceID.NetworkConnection
                End Select

            Next
        End If
        dt.Dispose()
    End Sub


    Private Sub btnSaveDeviceConfig_Click(sender As Object, e As EventArgs) Handles btnSaveDeviceConfig.Click
        Dim sql As String = "select id, ms_device_id "
        sql += " from MS_VENDING_DEVICE "
        sql += " where ms_vending_id=@_VENDING_ID"

        Dim p(1) As SqlParameter
        p(0) = New SqlParameter("@_VENDING_ID", SqlDbType.BigInt)
        p(0).Value = _VendingID

        Dim _err As String = ""
        Dim dt As DataTable = ExecuteTable(sql, p)
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim ddr As DataRow = dt.Rows(i)

                sql = " update ms_vending_device "
                sql += " set comport_vid=@_COMPORT_VID, driver_name1=@_DRIVER_NAME1, driver_name2=@_DRIVER_NAME2 "
                sql += " where id=@_ID"

                ReDim p(4)
                p(0) = SetParameter("@_ID", SqlDbType.BigInt, ddr("id"))
                p(1) = SetParameter("@_COMPORT_VID", SqlDbType.VarChar, DBNull.Value)
                p(2) = SetParameter("@_DRIVER_NAME1", SqlDbType.VarChar, DBNull.Value)
                p(3) = SetParameter("@_DRIVER_NAME2", SqlDbType.VarChar, DBNull.Value)

                Select Case Convert.ToInt16(ddr("ms_device_id"))
                    Case DeviceID.BankNoteIn
                        If CashInConnectDevice1.ComportName.Trim <> "" Then
                            p(1) = SetParameter("@_COMPORT_VID", SqlDbType.VarChar, CashInConnectDevice1.ComportName)
                        End If
                    Case DeviceID.BankNoteOut_20
                        If BanknoteOut20.ComportName.Trim <> "" Then
                            'VendingConfig.BanknoteOUT20Comport = dDr("comport_vid")
                            p(1) = SetParameter("@_COMPORT_VID", SqlDbType.VarChar, BanknoteOut20.ComportName)
                        End If

                    'Case DeviceID.BankNoteOut_50
                    '    If Convert.IsDBNull(ddr("comport_vid")) = False Then
                    '        'VendingConfig.BanknoteOUT50Comport = dDr("comport_vid")
                    '        p(1) = SetParameter("@_COMPORT_VID", SqlDbType.VarChar, BanknoteOut50.ComportName)
                    '        p(2) = SetParameter("@_DRIVER_NAME1", SqlDbType.VarChar, DBNull.Value)
                    '        p(3) = SetParameter("@_DRIVER_NAME2", SqlDbType.VarChar, DBNull.Value)
                    '    End If

                    Case DeviceID.BankNoteOut_100
                        If BanknoteOut100.ComportName.Trim <> "" Then
                            'VendingConfig.BanknoteOUT100Comport = dDr("comport_vid")
                            p(1) = SetParameter("@_COMPORT_VID", SqlDbType.VarChar, BanknoteOut100.ComportName)
                        End If

                    'Case DeviceID.BankNoteOut_500
                    '    If Convert.IsDBNull(ddr("comport_vid")) = False Then
                    '        'VendingConfig.BanknoteOUT500Comport = dDr("comport_vid")
                    '        p(1) = SetParameter("@_COMPORT_VID", SqlDbType.VarChar, BanknoteOut50.ComportName)
                    '        p(2) = SetParameter("@_DRIVER_NAME1", SqlDbType.VarChar, DBNull.Value)
                    '        p(3) = SetParameter("@_DRIVER_NAME2", SqlDbType.VarChar, DBNull.Value)
                    '    End If

                    Case DeviceID.CoinIn
                        If CoinInConnectDevice1.ComportName.Trim <> "" Then
                            'VendingConfig.CoinInComport = dDr("comport_vid")
                            p(1) = SetParameter("@_COMPORT_VID", SqlDbType.VarChar, CoinInConnectDevice1.ComportName.Trim)
                        End If

                    'Case DeviceID.CoinOut_1
                    '    If Convert.IsDBNull(ddr("comport_vid")) = False Then
                    '        ' VendingConfig.CoinOut1Comport = dDr("comport_vid")
                    '    End If

                    'Case DeviceID.CoinOut_2
                    '    If Convert.IsDBNull(ddr("comport_vid")) = False Then
                    '        ' VendingConfig.CoinOut2Comport = dDr("comport_vid")
                    '    End If

                    Case DeviceID.CoinOut_5
                        If CoinOut5.ComportName.Trim <> "" Then
                            'VendingConfig.CoinOut5Comport = dDr("comport_vid")
                            p(1) = SetParameter("@_COMPORT_VID", SqlDbType.VarChar, CoinOut5.ComportName.Trim)
                        End If

                    'Case DeviceID.CoinOut_10
                    '    If Convert.IsDBNull(ddr("comport_vid")) = False Then
                    '        'VendingConfig.CoinOut10Comport = dDr("comport_vid")
                    '    End If

                    Case DeviceID.Printer
                        If PrinterConnectDevice1.PrintertName.Trim <> "" Then
                            'VendingConfig.PrinterDeviceName = dDr("driver_name1")
                            p(2) = SetParameter("@_DRIVER_NAME1", SqlDbType.VarChar, PrinterConnectDevice1.PrintertName.Trim)
                        End If
                    Case DeviceID.NetworkConnection
                End Select


                Dim trans As New VendingTransactionDB(GetConnection(GetConnString))
                Dim ret As String = ExecuteNonQuery(sql, trans.Trans, p)
                If ret = "true" Then
                    trans.CommitTransaction()
                Else
                    trans.RollbackTransaction()
                    _err = ret
                End If
            Next
        End If
        dt.Dispose()

        If _err.Trim <> "" Then
            MessageBox.Show(_err, "Save Device Config", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            MessageBox.Show("Save Success", "Save Device Config", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

#Region "Database Function"
    Private Function GetConnString() As String
        Dim ini As New Org.Mentalis.Files.IniReader(INIFileName)
        ini.Section = "DbSetting"

        Dim connString As String = "Data Source=" & txtServerName.Text.Trim & ";Initial Catalog=" & txtDatabaseName.Text.Trim & ";User ID=" & txtUserID.Text.Trim & ";Password=" & txtPassword.Text.Trim & ";"

        Return connString
    End Function


    Private Sub FillInDatabaseInfo()
        Dim ini As New Org.Mentalis.Files.IniReader(INIFileName)
        ini.Section = "DbSetting"
        txtServerName.Text = ini.ReadString("ServerName")
        txtDatabaseName.Text = ini.ReadString("DatabaseName")
        txtUserID.Text = ini.ReadString("UserID")
        txtPassword.Text = ini.ReadString("Password")
        ini = Nothing
    End Sub

    Private Sub btnDbConnect_Click(sender As Object, e As EventArgs) Handles btnDbConnect.Click
        Dim ini As New Org.Mentalis.Files.IniReader(INIFileName)
        ini.Section = "DbSetting"

        Dim conn As SqlConnection = GetConnection(GetConnString())
        If conn.State = ConnectionState.Open Then
            ini.Write("ServerName", txtServerName.Text.Trim)
            ini.Write("DatabaseName", txtDatabaseName.Text.Trim)
            ini.Write("UserID", txtUserID.Text.Trim)
            ini.Write("Password", txtPassword.Text.Trim)

            MessageBox.Show("Success", "Connect Database", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show("Connect Fail", "Connect Database", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
        ini = Nothing

    End Sub

    Private Function ExecuteTable(ByVal sql As String, cmdParam() As SqlParameter) As DataTable
        Dim cmd As New SqlCommand
        Dim adapter As New SqlDataAdapter
        adapter.SelectCommand = cmd
        Dim LetClose As Boolean = False
        Dim dt As New DataTable
        Try
            Dim conn As SqlConnection = GetConnection(GetConnString())
            LetClose = True

            BuildCommand(cmd, conn, Nothing, CommandType.Text, sql, cmdParam)
            adapter.Fill(dt)
            adapter.Dispose()
            If LetClose = True Then
                cmd.Dispose()
                conn.Close()
                SqlConnection.ClearAllPools()
            End If
        Catch ex As ApplicationException
            adapter.Dispose()
        Catch ex As SqlException
            adapter.Dispose()
        Catch ex As Exception
            adapter.Dispose()
        End Try

        Return dt
    End Function

    Public Function ExecuteNonQuery(ByVal Sql As String, ByVal trans As SqlTransaction, ByVal cmdParms() As SqlParameter) As String
        Dim ret As String = "false"
        Dim command As New SqlCommand
        Dim _err As String = ""
        Try
            BuildCommand(command, trans.Connection, trans, CommandType.Text, Sql, cmdParms)
            command.ExecuteNonQuery()
            ret = "true"
        Catch ex As ApplicationException
            _err = Sql & " $$$$ " & ex.Message & " " & ex.StackTrace
            ret = "false|ApplicationException: " & _err
        Catch ex As SqlException
            _err = Sql & " $$$$ " & ex.Message & " " & ex.StackTrace
            ret = "false|SqlException: " & _err
        Catch ex As Exception
            _err = Sql & " $$$$ " & ex.Message & " " & ex.StackTrace
            ret = "false|Exception: " & _err
        End Try

        Return ret
    End Function

    Private Function GetConnection(ByVal connString As String) As SqlConnection
        Dim conn As SqlConnection
        Try
            conn = New SqlConnection(connString)
            conn.Open()
            'Return conn
        Catch ex As ApplicationException
            'Throw ex
        Catch ex As SqlException
            Try
                conn = New SqlConnection(connString)
                conn.Open()
                'Return conn
            Catch ex1 As SqlException
            End Try
        Catch ex As Exception
        End Try

        Return conn
    End Function

    Private Function SetParameter(ParameterName As String, pType As SqlDbType, ParameterValue As Object) As SqlParameter
        Dim p As New SqlParameter(ParameterName, pType)
        If Convert.IsDBNull(ParameterValue) = False Then
            p.Value = ParameterValue
        Else
            p.Value = DBNull.Value
        End If
        Return p
    End Function

    Private Shared Sub BuildCommand(ByVal cmd As SqlCommand, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, ByVal cmdType As CommandType, ByVal cmdText As String, ByVal cmdParms() As SqlParameter)
        If conn.State <> ConnectionState.Open Then
            Try
                conn.Open()
            Catch ex As SqlException
                'Throw New ApplicationException(GetExceptionMessage(ex), ex)
            Catch ex As ApplicationException
                'Throw (ex)
            Catch ex As Exception
                'Throw New ApplicationException(ErrorConnection, ex)

            End Try
        End If

        Try
            cmd.Connection = conn
        Catch ex As Exception
            'Throw New ApplicationException(ErrorSetCommandConnection, ex)

        End Try
        cmd.CommandText = cmdText

        If trans IsNot Nothing Then
            cmd.Transaction = trans
        End If

        Try
            cmd.CommandType = cmdType
            cmd.CommandTimeout = 240
        Catch ex As ArgumentException
            'Throw New ApplicationException(ErrorInvalidCommandType, ex)

        End Try

        If cmdParms IsNot Nothing Then
            For Each parm As SqlParameter In cmdParms

                Try
                    If parm IsNot Nothing Then
                        cmd.Parameters.Add(parm)
                    End If
                Catch ex As ArgumentNullException
                    'Throw New ApplicationException(ErrorNullParameter, ex)

                Catch ex As ArgumentException
                    'Throw New ApplicationException(ErrorDuplicateParameter, ex)

                End Try
            Next
        End If
    End Sub




#End Region
End Class

