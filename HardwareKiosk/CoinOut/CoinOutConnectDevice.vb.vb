﻿Imports CoinOut.Org.Mentalis.Files

Public Class CoinOutConnectDevice

    Dim CoinOut As New CoinOutClass
    Dim _CoinValue As Integer

    Public Property ComportName As String
        Get
            Return cbComport.Text.Trim
        End Get
        Set(value As String)
            cbComport.SelectedIndex = cbComport.FindString(value)

            If cbComport.SelectedIndex > -1 Then
                Connect(cbComport.Text)
            End If
        End Set
    End Property

    Public Property CoinValue() As Integer
        Get
            Return _CoinValue
        End Get
        Set(ByVal value As Integer)
            _CoinValue = value
            lblHead.Text = "Coin Out " & _CoinValue
        End Set
    End Property


    Private Sub FormConnectDevice_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        CheckForIllegalCrossThreadCalls = False
        BindSetting()
    End Sub


    Sub BindSetting()
        cbComport.Items.Clear()
        cbComport.Items.Add("")
        For Each sp As String In My.Computer.Ports.SerialPortNames
            cbComport.Items.Add(sp)
        Next
    End Sub

    Sub Connect(ByVal Comport As String)
        If CoinOut.ConnectCoinOutDevice(Comport) = True Then
            'AddHandler CoinOut.MySerialPort.DataReceived, AddressOf CoinOut.MySerialPortDataReceived
            AddHandler CoinOut.ReceiveEvent, AddressOf DataReceived
            BindpbStatus(True)
        Else
            txtStatus.Text = ""
            BindpbStatus(False)
        End If
    End Sub

    Sub BindpbStatus(ByVal Status As Boolean)
        txtStatus.Text = "Disconnected"
        txtStatus.ForeColor = Drawing.Color.Red

        If Status = True Then
            CheckStatus()
        End If
    End Sub

    Private Sub btnTest_Click(sender As System.Object, e As System.EventArgs) Handles btnTest.Click
        'If txtStatus.Text = "Disconnected" Then
        '    MessageBox.Show("กรุณาเชื่อมต่ออุปกรณ์ !!", "ผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        '    Exit Sub
        'End If

        Dim f As New CoinOutTestDevice
        f.lblHead.Text = "Coin Out " & _CoinValue
        f.lblComport.Text = cbComport.Text
        CoinOut.Disconnect()
        If f.ShowDialog() = DialogResult.OK Then
            If CoinOut.ConnectCoinOutDevice(cbComport.Text) = True Then
                BindpbStatus(True)
            Else
                txtStatus.Text = ""
                BindpbStatus(False)
            End If
        End If
    End Sub

    Public Sub CheckStatus()
        CoinOut.CheckStatusDeviceCoinOut()
    End Sub

    Private Sub DataReceived(ByVal ReceiveData As String)
        'กรณีถ้าเครื่องไม่มีเงิน แล้วเพิ่งใส่เงินมา จะได้รับ Enable BA, if hopper problems recovered ซึ่งแปลว่าเครื่องพร้อมใช้งานแล้ว
        If ReceiveData = "" Then Exit Sub
        ReceiveData = CoinOut.ParserStatusCommand(ReceiveData)
        If ReceiveData = "Ready" Or ReceiveData = "Enable BA, if hopper problems recovered" Then
            txtStatus.Text = "Ready"
            txtStatus.ForeColor = Drawing.Color.Lime
        Else
            txtStatus.Text = ReceiveData
            txtStatus.ForeColor = Drawing.Color.Red
        End If
    End Sub

    Private Sub cbComport_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cbComport.SelectionChangeCommitted
        Connect(cbComport.Text)
    End Sub
End Class
