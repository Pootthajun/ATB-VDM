﻿Imports System.Management
Imports Printer.Org.Mentalis.Files
Imports PrinterClassDll

Public Class PrinterClass



    Public Enum PrinterStatus
        Online = 1
        Unknow = 2
        Offline = 3
    End Enum
    Public Function CheckPrinterStatus(PrinterName As String) As String
        Try
            Dim scope As New ManagementScope("\root\cimv2")
            scope.Connect()
            ' Select Printers from WMI Object Collections 
            Dim searcher As New ManagementObjectSearcher("SELECT * FROM Win32_Printer")
            Dim MyPrinter As String = ""
            For Each printer As ManagementObject In searcher.[Get]()
                MyPrinter = printer("Name").ToString().ToLower()
                If MyPrinter.Equals(PrinterName.ToLower) Then
                    If printer("WorkOffline").ToString().ToLower().Equals("true") Then
                        ' printer is offline by user 
                        Return PrinterStatus.Offline
                    Else
                        ' printer is not offline 
                        Return PrinterStatus.Online
                    End If
                End If
            Next
        Catch ex As Exception : End Try
        Return PrinterStatus.Unknow
    End Function

    Protected Sub PrintRectankle(PosX As Integer, PosY As Integer, h As Integer, ByRef e As System.Drawing.Printing.PrintPageEventArgs)

        Dim brsh As New System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(0, 0, 0))
        Dim vPen As New Pen(brsh)
        Dim vRect As New Rectangle(PosX, PosY, e.PageSettings.PrintableArea.Width - 1, h)

        e.Graphics.DrawRectangle(vPen, vRect)
    End Sub

    Public Sub PrintText(ByVal txt As String, ByVal fnt As System.Drawing.Font, ByVal align As Align, TextWidth As Single, TextLocation As Point, ByRef e As System.Drawing.Printing.PrintPageEventArgs)
        Dim w As Integer = e.Graphics.MeasureString(txt, fnt).Width
        Dim h As Integer = e.Graphics.MeasureString(txt, fnt).Height
        Dim x As Integer = TextLocation.X
        Dim y As Integer = TextLocation.Y

        Dim brsh As New System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(0, 0, 0))
        Select Case align
            Case 0 'Default, LEFT
                x = TextLocation.X 'e.PageSettings.PrintableArea.Left
                'y = e.PageSettings.PrintableArea.Top + _lastPrintY
            Case 1 'RIGHT
                x = (TextLocation.X + w) - TextWidth 'e.PageSettings.PrintableArea.Right - w
                'y = e.PageSettings.PrintableArea.Top + _lastPrintY
            Case 2 'CENTER
                x = (TextLocation.X + TextWidth) / 2 - w / 2
                'y = e.PageSettings.PrintableArea.Top + _lastPrintY
        End Select
        e.Graphics.DrawString(txt, fnt, brsh, x, y)

    End Sub

    Public Sub PrintImage(ByVal img As System.Drawing.Image, ImageLocation As Point, ByRef e As System.Drawing.Printing.PrintPageEventArgs)
        Dim w As Integer = img.Width
        Dim h As Integer = img.Height
        Dim x As Integer = ImageLocation.X
        Dim y As Integer = ImageLocation.Y

        e.Graphics.DrawImage(img, x, y)
        img.Dispose()
    End Sub

    Public Enum Align As Short
        Left = 0
        Right = 1
        Center = 2
    End Enum
End Class
