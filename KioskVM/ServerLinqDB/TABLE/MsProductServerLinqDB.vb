Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = ServerLinqDB.ConnectDB.ServerDB
Imports ServerLinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for MS_PRODUCT table ServerLinqDB.
    '[Create by  on September, 21 2016]
    Public Class MsProductServerLinqDB
        Public sub MsProductServerLinqDB()

        End Sub 
        ' MS_PRODUCT
        Const _tableName As String = "MS_PRODUCT"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _MS_PRODUCT_CATEGORY_ID As Long = 0
        Dim _PRODUCT_CODE As String = ""
        Dim _PRODUCT_NAME_TH As String = ""
        Dim _PRODUCT_NAME_EN As String = ""
        Dim _PRODUCT_NAME_JP As  String  = ""
        Dim _PRODUCT_NAME_CH As  String  = ""
        Dim _PRODUCT_DESC_TH As String = ""
        Dim _PRODUCT_DESC_EN As String = ""
        Dim _PRODUCT_DESC_JP As  String  = ""
        Dim _PRODUCT_DESC_CH As  String  = ""
        Dim _PRODUCT_IMAGE_BIG() As Byte
        Dim _PRODUCT_IMAGE_SMALL() As Byte
        Dim _COST As Double = 0
        Dim _PRICE As Double = 0
        Dim _ACTIVE_STATUS As Char = "Y"

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_MS_PRODUCT_CATEGORY_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property MS_PRODUCT_CATEGORY_ID() As Long
            Get
                Return _MS_PRODUCT_CATEGORY_ID
            End Get
            Set(ByVal value As Long)
               _MS_PRODUCT_CATEGORY_ID = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_CODE", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property PRODUCT_CODE() As String
            Get
                Return _PRODUCT_CODE
            End Get
            Set(ByVal value As String)
               _PRODUCT_CODE = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_NAME_TH", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property PRODUCT_NAME_TH() As String
            Get
                Return _PRODUCT_NAME_TH
            End Get
            Set(ByVal value As String)
               _PRODUCT_NAME_TH = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_NAME_EN", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property PRODUCT_NAME_EN() As String
            Get
                Return _PRODUCT_NAME_EN
            End Get
            Set(ByVal value As String)
               _PRODUCT_NAME_EN = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_NAME_JP", DbType:="VarChar(100)")>  _
        Public Property PRODUCT_NAME_JP() As  String 
            Get
                Return _PRODUCT_NAME_JP
            End Get
            Set(ByVal value As  String )
               _PRODUCT_NAME_JP = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_NAME_CH", DbType:="VarChar(100)")>  _
        Public Property PRODUCT_NAME_CH() As  String 
            Get
                Return _PRODUCT_NAME_CH
            End Get
            Set(ByVal value As  String )
               _PRODUCT_NAME_CH = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_DESC_TH", DbType:="VarChar(255) NOT NULL ",CanBeNull:=false)>  _
        Public Property PRODUCT_DESC_TH() As String
            Get
                Return _PRODUCT_DESC_TH
            End Get
            Set(ByVal value As String)
               _PRODUCT_DESC_TH = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_DESC_EN", DbType:="VarChar(255) NOT NULL ",CanBeNull:=false)>  _
        Public Property PRODUCT_DESC_EN() As String
            Get
                Return _PRODUCT_DESC_EN
            End Get
            Set(ByVal value As String)
               _PRODUCT_DESC_EN = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_DESC_JP", DbType:="VarChar(255)")>  _
        Public Property PRODUCT_DESC_JP() As  String 
            Get
                Return _PRODUCT_DESC_JP
            End Get
            Set(ByVal value As  String )
               _PRODUCT_DESC_JP = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_DESC_CH", DbType:="VarChar(255)")>  _
        Public Property PRODUCT_DESC_CH() As  String 
            Get
                Return _PRODUCT_DESC_CH
            End Get
            Set(ByVal value As  String )
               _PRODUCT_DESC_CH = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_IMAGE_BIG", DbType:="IMAGE NOT NULL ",CanBeNull:=false)>  _
        Public Property PRODUCT_IMAGE_BIG() As Byte()
            Get
                Return _PRODUCT_IMAGE_BIG
            End Get
            Set(ByVal value() As Byte)
               _PRODUCT_IMAGE_BIG = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_IMAGE_SMALL", DbType:="IMAGE NOT NULL ",CanBeNull:=false)>  _
        Public Property PRODUCT_IMAGE_SMALL() As Byte()
            Get
                Return _PRODUCT_IMAGE_SMALL
            End Get
            Set(ByVal value() As Byte)
               _PRODUCT_IMAGE_SMALL = value
            End Set
        End Property 
        <Column(Storage:="_COST", DbType:="Float NOT NULL ",CanBeNull:=false)>  _
        Public Property COST() As Double
            Get
                Return _COST
            End Get
            Set(ByVal value As Double)
               _COST = value
            End Set
        End Property 
        <Column(Storage:="_PRICE", DbType:="Float NOT NULL ",CanBeNull:=false)>  _
        Public Property PRICE() As Double
            Get
                Return _PRICE
            End Get
            Set(ByVal value As Double)
               _PRICE = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVE_STATUS() As Char
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVE_STATUS = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
            _MS_PRODUCT_CATEGORY_ID = 0
            _PRODUCT_CODE = ""
            _PRODUCT_NAME_TH = ""
            _PRODUCT_NAME_EN = ""
            _PRODUCT_NAME_JP = ""
            _PRODUCT_NAME_CH = ""
            _PRODUCT_DESC_TH = ""
            _PRODUCT_DESC_EN = ""
            _PRODUCT_DESC_JP = ""
            _PRODUCT_DESC_CH = ""
             _PRODUCT_IMAGE_BIG = Nothing
             _PRODUCT_IMAGE_SMALL = Nothing
            _COST = 0
            _PRICE = 0
            _ACTIVE_STATUS = "Y"
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into MS_PRODUCT table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_PRODUCT table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to MS_PRODUCT table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from MS_PRODUCT table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of MS_PRODUCT by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of MS_PRODUCT by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As MsProductServerLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of MS_PRODUCT by specified PRODUCT_CODE key is retrieved successfully.
        '/// <param name=cPRODUCT_CODE>The PRODUCT_CODE key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPRODUCT_CODE(cPRODUCT_CODE As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PRODUCT_CODE", cPRODUCT_CODE) 
            Return doChkData("PRODUCT_CODE = @_PRODUCT_CODE", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_PRODUCT by specified PRODUCT_CODE key is retrieved successfully.
        '/// <param name=cPRODUCT_CODE>The PRODUCT_CODE key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByPRODUCT_CODE(cPRODUCT_CODE As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PRODUCT_CODE", cPRODUCT_CODE) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("PRODUCT_CODE = @_PRODUCT_CODE And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_PRODUCT by specified PRODUCT_NAME_TH key is retrieved successfully.
        '/// <param name=cPRODUCT_NAME_TH>The PRODUCT_NAME_TH key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPRODUCT_NAME_TH(cPRODUCT_NAME_TH As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PRODUCT_NAME_TH", cPRODUCT_NAME_TH) 
            Return doChkData("PRODUCT_NAME_TH = @_PRODUCT_NAME_TH", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_PRODUCT by specified PRODUCT_NAME_TH key is retrieved successfully.
        '/// <param name=cPRODUCT_NAME_TH>The PRODUCT_NAME_TH key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByPRODUCT_NAME_TH(cPRODUCT_NAME_TH As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PRODUCT_NAME_TH", cPRODUCT_NAME_TH) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("PRODUCT_NAME_TH = @_PRODUCT_NAME_TH And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_PRODUCT by specified PRODUCT_NAME_EN key is retrieved successfully.
        '/// <param name=cPRODUCT_NAME_EN>The PRODUCT_NAME_EN key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPRODUCT_NAME_EN(cPRODUCT_NAME_EN As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PRODUCT_NAME_EN", cPRODUCT_NAME_EN) 
            Return doChkData("PRODUCT_NAME_EN = @_PRODUCT_NAME_EN", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of MS_PRODUCT by specified PRODUCT_NAME_EN key is retrieved successfully.
        '/// <param name=cPRODUCT_NAME_EN>The PRODUCT_NAME_EN key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByPRODUCT_NAME_EN(cPRODUCT_NAME_EN As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PRODUCT_NAME_EN", cPRODUCT_NAME_EN) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("PRODUCT_NAME_EN = @_PRODUCT_NAME_EN And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of MS_PRODUCT by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into MS_PRODUCT table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to MS_PRODUCT table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from MS_PRODUCT table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(19) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(1).Value = _CREATED_BY.Trim

            cmbParam(2) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(2).Value = _CREATED_DATE

            cmbParam(3) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(3).Value = _UPDATED_BY.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(4).Value = _UPDATED_DATE.Value
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_MS_PRODUCT_CATEGORY_ID", SqlDbType.BigInt)
            cmbParam(5).Value = _MS_PRODUCT_CATEGORY_ID

            cmbParam(6) = New SqlParameter("@_PRODUCT_CODE", SqlDbType.VarChar)
            cmbParam(6).Value = _PRODUCT_CODE.Trim

            cmbParam(7) = New SqlParameter("@_PRODUCT_NAME_TH", SqlDbType.VarChar)
            cmbParam(7).Value = _PRODUCT_NAME_TH.Trim

            cmbParam(8) = New SqlParameter("@_PRODUCT_NAME_EN", SqlDbType.VarChar)
            cmbParam(8).Value = _PRODUCT_NAME_EN.Trim

            cmbParam(9) = New SqlParameter("@_PRODUCT_NAME_JP", SqlDbType.VarChar)
            If _PRODUCT_NAME_JP.Trim <> "" Then 
                cmbParam(9).Value = _PRODUCT_NAME_JP.Trim
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_PRODUCT_NAME_CH", SqlDbType.VarChar)
            If _PRODUCT_NAME_CH.Trim <> "" Then 
                cmbParam(10).Value = _PRODUCT_NAME_CH.Trim
            Else
                cmbParam(10).Value = DBNull.value
            End If

            cmbParam(11) = New SqlParameter("@_PRODUCT_DESC_TH", SqlDbType.VarChar)
            cmbParam(11).Value = _PRODUCT_DESC_TH.Trim

            cmbParam(12) = New SqlParameter("@_PRODUCT_DESC_EN", SqlDbType.VarChar)
            cmbParam(12).Value = _PRODUCT_DESC_EN.Trim

            cmbParam(13) = New SqlParameter("@_PRODUCT_DESC_JP", SqlDbType.VarChar)
            If _PRODUCT_DESC_JP.Trim <> "" Then 
                cmbParam(13).Value = _PRODUCT_DESC_JP.Trim
            Else
                cmbParam(13).Value = DBNull.value
            End If

            cmbParam(14) = New SqlParameter("@_PRODUCT_DESC_CH", SqlDbType.VarChar)
            If _PRODUCT_DESC_CH.Trim <> "" Then 
                cmbParam(14).Value = _PRODUCT_DESC_CH.Trim
            Else
                cmbParam(14).Value = DBNull.value
            End If

            cmbParam(15) = New SqlParameter("@_PRODUCT_IMAGE_BIG",SqlDbType.Image, _PRODUCT_IMAGE_BIG.Length)
            cmbParam(15).Value = _PRODUCT_IMAGE_BIG

            cmbParam(16) = New SqlParameter("@_PRODUCT_IMAGE_SMALL",SqlDbType.Image, _PRODUCT_IMAGE_SMALL.Length)
            cmbParam(16).Value = _PRODUCT_IMAGE_SMALL

            cmbParam(17) = New SqlParameter("@_COST", SqlDbType.Float)
            cmbParam(17).Value = _COST

            cmbParam(18) = New SqlParameter("@_PRICE", SqlDbType.Float)
            cmbParam(18).Value = _PRICE

            cmbParam(19) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            cmbParam(19).Value = _ACTIVE_STATUS

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of MS_PRODUCT by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("ms_product_category_id")) = False Then _ms_product_category_id = Convert.ToInt64(Rdr("ms_product_category_id"))
                        If Convert.IsDBNull(Rdr("product_code")) = False Then _product_code = Rdr("product_code").ToString()
                        If Convert.IsDBNull(Rdr("product_name_th")) = False Then _product_name_th = Rdr("product_name_th").ToString()
                        If Convert.IsDBNull(Rdr("product_name_en")) = False Then _product_name_en = Rdr("product_name_en").ToString()
                        If Convert.IsDBNull(Rdr("product_name_jp")) = False Then _product_name_jp = Rdr("product_name_jp").ToString()
                        If Convert.IsDBNull(Rdr("product_name_ch")) = False Then _product_name_ch = Rdr("product_name_ch").ToString()
                        If Convert.IsDBNull(Rdr("product_desc_th")) = False Then _product_desc_th = Rdr("product_desc_th").ToString()
                        If Convert.IsDBNull(Rdr("product_desc_en")) = False Then _product_desc_en = Rdr("product_desc_en").ToString()
                        If Convert.IsDBNull(Rdr("product_desc_jp")) = False Then _product_desc_jp = Rdr("product_desc_jp").ToString()
                        If Convert.IsDBNull(Rdr("product_desc_ch")) = False Then _product_desc_ch = Rdr("product_desc_ch").ToString()
                        If Convert.IsDBNull(Rdr("product_image_big")) = False Then _product_image_big = CType(Rdr("product_image_big"), Byte())
                        If Convert.IsDBNull(Rdr("product_image_small")) = False Then _product_image_small = CType(Rdr("product_image_small"), Byte())
                        If Convert.IsDBNull(Rdr("cost")) = False Then _cost = Convert.ToDouble(Rdr("cost"))
                        If Convert.IsDBNull(Rdr("price")) = False Then _price = Convert.ToDouble(Rdr("price"))
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _active_status = Rdr("active_status").ToString()
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of MS_PRODUCT by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As MsProductServerLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("ms_product_category_id")) = False Then _ms_product_category_id = Convert.ToInt64(Rdr("ms_product_category_id"))
                        If Convert.IsDBNull(Rdr("product_code")) = False Then _product_code = Rdr("product_code").ToString()
                        If Convert.IsDBNull(Rdr("product_name_th")) = False Then _product_name_th = Rdr("product_name_th").ToString()
                        If Convert.IsDBNull(Rdr("product_name_en")) = False Then _product_name_en = Rdr("product_name_en").ToString()
                        If Convert.IsDBNull(Rdr("product_name_jp")) = False Then _product_name_jp = Rdr("product_name_jp").ToString()
                        If Convert.IsDBNull(Rdr("product_name_ch")) = False Then _product_name_ch = Rdr("product_name_ch").ToString()
                        If Convert.IsDBNull(Rdr("product_desc_th")) = False Then _product_desc_th = Rdr("product_desc_th").ToString()
                        If Convert.IsDBNull(Rdr("product_desc_en")) = False Then _product_desc_en = Rdr("product_desc_en").ToString()
                        If Convert.IsDBNull(Rdr("product_desc_jp")) = False Then _product_desc_jp = Rdr("product_desc_jp").ToString()
                        If Convert.IsDBNull(Rdr("product_desc_ch")) = False Then _product_desc_ch = Rdr("product_desc_ch").ToString()
                        If Convert.IsDBNull(Rdr("product_image_big")) = False Then _product_image_big = CType(Rdr("product_image_big"), Byte())
                        If Convert.IsDBNull(Rdr("product_image_small")) = False Then _product_image_small = CType(Rdr("product_image_small"), Byte())
                        If Convert.IsDBNull(Rdr("cost")) = False Then _cost = Convert.ToDouble(Rdr("cost"))
                        If Convert.IsDBNull(Rdr("price")) = False Then _price = Convert.ToDouble(Rdr("price"))
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _active_status = Rdr("active_status").ToString()
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table MS_PRODUCT
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (CREATED_BY, CREATED_DATE, MS_PRODUCT_CATEGORY_ID, PRODUCT_CODE, PRODUCT_NAME_TH, PRODUCT_NAME_EN, PRODUCT_NAME_JP, PRODUCT_NAME_CH, PRODUCT_DESC_TH, PRODUCT_DESC_EN, PRODUCT_DESC_JP, PRODUCT_DESC_CH, PRODUCT_IMAGE_BIG, PRODUCT_IMAGE_SMALL, COST, PRICE, ACTIVE_STATUS)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE, INSERTED.MS_PRODUCT_CATEGORY_ID, INSERTED.PRODUCT_CODE, INSERTED.PRODUCT_NAME_TH, INSERTED.PRODUCT_NAME_EN, INSERTED.PRODUCT_NAME_JP, INSERTED.PRODUCT_NAME_CH, INSERTED.PRODUCT_DESC_TH, INSERTED.PRODUCT_DESC_EN, INSERTED.PRODUCT_DESC_JP, INSERTED.PRODUCT_DESC_CH, INSERTED.PRODUCT_IMAGE_BIG, INSERTED.PRODUCT_IMAGE_SMALL, INSERTED.COST, INSERTED.PRICE, INSERTED.ACTIVE_STATUS"
                Sql += " VALUES("
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_MS_PRODUCT_CATEGORY_ID" & ", "
                sql += "@_PRODUCT_CODE" & ", "
                sql += "@_PRODUCT_NAME_TH" & ", "
                sql += "@_PRODUCT_NAME_EN" & ", "
                sql += "@_PRODUCT_NAME_JP" & ", "
                sql += "@_PRODUCT_NAME_CH" & ", "
                sql += "@_PRODUCT_DESC_TH" & ", "
                sql += "@_PRODUCT_DESC_EN" & ", "
                sql += "@_PRODUCT_DESC_JP" & ", "
                sql += "@_PRODUCT_DESC_CH" & ", "
                sql += "@_PRODUCT_IMAGE_BIG" & ", "
                sql += "@_PRODUCT_IMAGE_SMALL" & ", "
                sql += "@_COST" & ", "
                sql += "@_PRICE" & ", "
                sql += "@_ACTIVE_STATUS"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table MS_PRODUCT
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" & ", "
                Sql += "MS_PRODUCT_CATEGORY_ID = " & "@_MS_PRODUCT_CATEGORY_ID" & ", "
                Sql += "PRODUCT_CODE = " & "@_PRODUCT_CODE" & ", "
                Sql += "PRODUCT_NAME_TH = " & "@_PRODUCT_NAME_TH" & ", "
                Sql += "PRODUCT_NAME_EN = " & "@_PRODUCT_NAME_EN" & ", "
                Sql += "PRODUCT_NAME_JP = " & "@_PRODUCT_NAME_JP" & ", "
                Sql += "PRODUCT_NAME_CH = " & "@_PRODUCT_NAME_CH" & ", "
                Sql += "PRODUCT_DESC_TH = " & "@_PRODUCT_DESC_TH" & ", "
                Sql += "PRODUCT_DESC_EN = " & "@_PRODUCT_DESC_EN" & ", "
                Sql += "PRODUCT_DESC_JP = " & "@_PRODUCT_DESC_JP" & ", "
                Sql += "PRODUCT_DESC_CH = " & "@_PRODUCT_DESC_CH" & ", "
                Sql += "PRODUCT_IMAGE_BIG = " & "@_PRODUCT_IMAGE_BIG" & ", "
                Sql += "PRODUCT_IMAGE_SMALL = " & "@_PRODUCT_IMAGE_SMALL" & ", "
                Sql += "COST = " & "@_COST" & ", "
                Sql += "PRICE = " & "@_PRICE" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table MS_PRODUCT
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table MS_PRODUCT
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE, MS_PRODUCT_CATEGORY_ID, PRODUCT_CODE, PRODUCT_NAME_TH, PRODUCT_NAME_EN, PRODUCT_NAME_JP, PRODUCT_NAME_CH, PRODUCT_DESC_TH, PRODUCT_DESC_EN, PRODUCT_DESC_JP, PRODUCT_DESC_CH, PRODUCT_IMAGE_BIG, PRODUCT_IMAGE_SMALL, COST, PRICE, ACTIVE_STATUS FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
