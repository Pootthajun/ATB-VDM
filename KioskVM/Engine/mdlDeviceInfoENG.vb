﻿Public Module mdlDeviceInfoENG
#Region "Device Info"
    Public Enum DeviceID
        BankNoteIn = 1
        BankNoteOut_20 = 2
        BankNoteOut_50 = 9
        BankNoteOut_100 = 3
        BankNoteOut_500 = 12
        CoinIn = 4
        CoinOut_1 = 5
        CoinOut_5 = 6
        Printer = 7
        CoinOut_2 = 10
        CoinOut_10 = 11
        NetworkConnection = 13
    End Enum

    Public Enum DeviceType
        BanknoteIn = 1
        BanknoteOut = 2
        CoinIn = 3
        CoinOut = 4
        Printer = 5
        NetworkConnection = 7
    End Enum

    Public Enum BanknoteInStatus
        Ready = 1
        Unavailable = 40
        Disconnected = 9
        Power_OFF = 27
        Motor_Failure = 19
        Checksum_Error = 7
        Bill_Jam = 5
        Bill_Remove = 6
        Stacker_Open = 38
        Sensor_Problem = 32
        Bill_Fish = 4
    End Enum

    Public Enum CoinInStatus
        Ready = 1
        Unavailable = 40
        Disconnected = 9
        Sersor_1_Problem = 33
        Sersor_2_Problem = 34
        Sersor_3_Problem = 35
    End Enum

    Public Enum BanknoteOutStatus
        Ready = 1
        Disconnected = 9
        Single_machine_payout = 37
        Multiple_machine_payout = 20
        Payout_Successful = 3
        Payout_fails = 26
        Empty_note = 12
        Stock_less = 39
        Note_jam = 22
        Over_length = 25
        Note_Not_Exit = 23
        Sensor_adjusting = 30
        Sensor_Error = 31
        Double_note_error = 11
        Motor_Error = 18
        Dispensing_busy = 10
        Checksum_Error = 7
        Low_power_Error = 16
    End Enum


    Public Enum CoinOutStatus
        Ready = 1
        Disconnected = 9
        Enable_BA_if_hopper_problems_recovered = 13
        Inhibit_BA_if_hopper_problems_occurred = 14
        Mortor_Problem = 17
        Insufficient_Coin = 15
        Dedects_coin_dispensing_activity_after_suspending_the_dispene_signal = 8
        Reserved = 29
        Prism_Sersor_Failure = 28
        Shaft_Sersor_Failure = 36
    End Enum

    'Public Enum PassportScanerStatus
    '    Ready = 1
    '    Disconnected = 9
    'End Enum

    Public Enum PrinterStatus
        Online = 2
        Disconnected = 9
        Offline = 24
    End Enum

    'Public Enum IPCamStatus
    '    Ready = 1
    '    Disconnected = 9
    'End Enum

    'Public Enum QRCodeStatus
    '    Ready = 1
    '    Disconnected = 9
    'End Enum
    Public Enum NetworkStatus
        Ready = 1
        Disconnected = 9
    End Enum

    Public Enum BoardStatus
        Ready = 1
        Disconnected = 9
    End Enum
#End Region
End Module
