﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmDashboard.aspx.vb" Inherits="frmDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem; }
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
       <!-- main area -->
  
        <div class="page-title">
          <div class="title">Flot</div>
          <div class="sub-title">Flot chart plugin</div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="card bg-white">
              <div class="card-header">
                Line series
              </div>
              <div class="card-block text-center">
                <div class="chart line" style="height:320px"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card bg-white">
              <div class="card-header">
                Grouped bar series
              </div>
              <div class="card-block text-center">
                <div class="chart bar" style="height:320px"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card bg-white">
              <div class="card-header">
                Pie series
              </div>
              <div class="card-block text-center">
                <div class="chart pie" style="height:320px"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card bg-white">
              <div class="card-header">
                Realtime series
              </div>
              <div class="card-block text-center">
                <div class="chart realtime" style="height:320px"></div>
              </div>
            </div>
          </div>
        </div>
 
      <!-- /main area -->
        
     </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" /></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
    <script src="scripts/helpers/colors.js"></script>
    <script src="vendor/Chart.js/Chart.min.js"></script>
    <script src="vendor/flot/jquery.flot.js"></script>
    <script src="vendor/flot/jquery.flot.resize.js"></script>
    <script src="vendor/flot/jquery.flot.categories.js"></script>
    <script src="vendor/flot/jquery.flot.stack.js"></script>
    <script src="vendor/flot/jquery.flot.time.js"></script>
    <script src="vendor/flot/jquery.flot.pie.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendor/d3/d3.min.js" charset="utf-8"></script>
    <script src="vendor/c3/c3.min.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>

    <!-- end page scripts -->
    <!-- initialize page scripts -->
    <script src="scripts/helpers/sameheight.js"></script>
    <script src="scripts/ui/dashboard.js"></script>
    <script src="scripts/charts/c3.js"></script>
    <!-- end initialize page scripts -->
    <script src="scripts/charts/flot.js"></script>
    <script type="text/javascript" lang="javascript">

        var timerRefresh;
        var refreshInterval = 60000;  //อัพเดททุกๆ 1 นาที
        function setRefreshMonitoring() {
            timerRefresh = setTimeout(updateMonitoring, refreshInterval);
        }
        $(document).ready(function () {
            setRefreshMonitoring();
        });

        function updateMonitoring() {

            var btn = document.getElementById('btnRefreshData');
            if (btn) {
                btn.click();
                timerRefresh = setTimeout(updateMonitoring, refreshInterval);
            }
        }
    </script>
</asp:Content>

