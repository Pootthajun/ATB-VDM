﻿Imports System.Configuration
Imports System.IO
Imports ServerLinqDB.TABLE

Public Class frmVDOUpload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Files.Count = 0 Then Exit Sub
        Dim fileContent As HttpPostedFile = Request.Files(0)
        'Dim cf As ServerLinqDB.TABLE.CfServerSysconfigServerLinqDB = Engine.ServerENG.GetServerConfig
        Dim TempVDOPath As String = "C:\webroot\ATB-TIT\VDM\TempVDO"

        Try

            Dim Ads_ID As String = Request("Ads_ID")
            If Directory.Exists(TempVDOPath) = False Then
                Directory.CreateDirectory(TempVDOPath)
            End If

            Dim FilePath As String = TempVDOPath & "\" & Ads_ID & ".mp4"
            If File.Exists(FilePath) = True Then
                Try
                    File.Delete(FilePath)
                Catch ex As Exception
                End Try
            End If

            fileContent.SaveAs(FilePath)
            Session("VDOPath") = FilePath
            Response.Write("true")
        Catch ex As Exception
            Response.Write("false|Exception " & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub

End Class