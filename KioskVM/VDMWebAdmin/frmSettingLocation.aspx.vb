﻿Imports System.Data
Imports ServerLinqDB.ConnectDB
Imports ServerLinqDB.TABLE
Imports System.Data.SqlClient
Imports ConstantsWebAdmin

Public Class frmSettingLocation
    Inherits System.Web.UI.Page

    Dim BL As New VDMBL
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property EditLocationCode As String
        Get
            Try
                Return ViewState("location_code")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("location_code") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.NA, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterLocation, UserName)
            If auDt.Rows.Count = 0 Then
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
                Exit Sub
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()

        If Not IsPostBack Then
            BindList()
        Else
            initFormPlugin()
        End If

    End Sub

    Private Sub initFormPlugin()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Plugin", "initFormPlugin();", True)
    End Sub

    Private Sub BindList()
        Dim DT As DataTable = BL.GetList_Location("")
        rptList.DataSource = DT
        rptList.DataBind()

        lblTotalList.Text = FormatNumber(DT.Rows.Count, 0)

        'ColEdit.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'ColDelete.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'btnAdd.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        EditLocationCode = ""
        pnlList.Visible = True
        pnlEdit.Visible = False

        SetAuthorization()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        ClearEditForm()

        pnlList.Visible = False
        pnlEdit.Visible = True
    End Sub

    Private Sub ClearEditForm()
        EditLocationCode = ""
        txtLocationCode.Text = ""
        txtLocationName.Text = ""
        chkActive.Checked = True
        lblEditMode.Text = "Add"
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click

        If txtLocationCode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Location Code');", True)
            Exit Sub
        End If
        If txtLocationName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Location Name');", True)
            Exit Sub
        End If

        Dim DT As DataTable = BL.GetList_Location("")
        DT.DefaultView.RowFilter = "location_Code='" & txtLocationCode.Text.Replace("'", "''") & "' AND location_code <>'" & EditLocationCode.Replace("'", "''") & "'"
        If DT.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This Location Code is already existed');", True)
            Exit Sub
        End If

        Dim trans As New ServerTransactionDB
        Dim lnqLoc As New MsLocationServerLinqDB
        lnqLoc.ChkDataByLOCATION_CODE(EditLocationCode, trans.Trans)
        With lnqLoc
            .LOCATION_CODE = txtLocationCode.Text.Replace("'", "''")
            .LOCATION_NAME = txtLocationName.Text.Replace("'", "''")
            .ACTIVE_STATUS = IIf(chkActive.Checked, "Y", "N")
        End With

        Dim ret As New ExecuteDataInfo
        If lnqLoc.ID = 0 Then
            ret = lnqLoc.InsertData(UserName, trans.Trans)
        Else
            ret = lnqLoc.UpdateData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save success');", True)
            BindList()
        Else
            trans.RollbackTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", """") & "');", True)
        End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        BindList()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim td As HtmlTableCell = e.Item.FindControl("td")
        Dim lblLocCode As Label = e.Item.FindControl("lblLocCode")
        Dim lblLocName As Label = e.Item.FindControl("lblLocName")
        Dim lblVending As Label = e.Item.FindControl("lblVending")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim cfmDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfmDelete")

        Dim ColEdit As HtmlTableCell = e.Item.FindControl("ColEdit")
        Dim ColDelete As HtmlTableCell = e.Item.FindControl("ColDelete")

        lblLocCode.Text = e.Item.DataItem("location_code").ToString
        lblLocName.Text = e.Item.DataItem("location_name").ToString

        If Not e.Item.DataItem("active_status").ToString = "Y" Then
            td.Attributes("class") = "text-danger"
        Else
            td.Attributes("class") = "text-primary"
        End If

        lblVending.Text = FormatNumber(e.Item.DataItem("Total_Vending"), 0)
        btnEdit.CommandArgument = e.Item.DataItem("location_code")
        btnDelete.CommandArgument = e.Item.DataItem("location_code")
        cfmDelete.ConfirmText = "Confirm to delete " & e.Item.DataItem("location_name").ToString & " ?"

        'ColEdit.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'ColDelete.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                ClearEditForm()
                Dim Loc_Code As String = e.CommandArgument
                EditLocationCode = Loc_Code

                Dim DT As DataTable = BL.GetList_Location(Loc_Code)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & Loc_Code.Replace("'", """") & " is not found!');", True)
                    Exit Sub
                End If

                txtLocationCode.Text = DT.Rows(0).Item("location_code").ToString
                txtLocationName.Text = DT.Rows(0).Item("location_name").ToString

                chkActive.Checked = IIf(DT.Rows(0).Item("active_status").ToString() = "Y", True, False)
                lblEditMode.Text = "Edit"

                pnlList.Visible = False
                pnlEdit.Visible = True

                SetAuthorization()
            Case "Delete"
                Dim ret As String = BL.DeleteMasterLocation(e.CommandArgument)
                If ret = "" Then
                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.Replace("'", """") & " is not found!');", True)
                End If

        End Select
    End Sub

    Private Sub SetAuthorization()
        'ตรวจสอบสิทธิ์การแก้ไขข้อมูล
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.Edit, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterLocation, UserName)
            If auDt.Rows.Count = 0 Then
                ColEdit.InnerText = "VIEW"
                For i As Integer = 0 To rptList.Items.Count - 1
                    Dim btnDelete As ImageButton = rptList.Items(i).FindControl("btnDelete")
                    btnDelete.Enabled = False
                Next
                btnAdd.Visible = False



                txtLocationCode.Enabled = False
                txtLocationName.Enabled = False
                chkActive.Enabled = False
                btnSave.Visible = False
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()
    End Sub
End Class