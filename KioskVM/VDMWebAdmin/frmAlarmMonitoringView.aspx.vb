﻿Imports System.Data
Imports ConstantsWebAdmin

Public Class frmAlarmMonitoringView
    Inherits System.Web.UI.Page

    Dim BL As New VDMBL
    Const FunctionalID As Int16 = 6
    Const FunctionalZoneID As Int16 = 3
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property
    Public ReadOnly Property VENDING_ID As Integer
        Get
            Try
                Return Request.QueryString("V")
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If

        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.NA, WebAdminFunctionalZoneID.Monitor, WebAdminFunctionalID.VendingMachineMonitor, UserName)
            If auDt.Rows.Count = 0 Then
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
                Exit Sub
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()

        BindKiosk()
        BindHardware()
        BindPeripheral()
        BindStock()

        initFormPlugin()
    End Sub

    Private Sub initFormPlugin()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Plugin", "initFormPlugin(); ", True)
    End Sub

    Private Sub BindKiosk()
        Dim DT As DataTable = BL.Alarm_Overview(VENDING_ID, UserName)
        If DT.Rows.Count = 0 Then
            Response.Redirect("frmAlarmMonitoring.aspx")
            Exit Sub
        End If

        lblIPAddress.Text = DT.Rows(0).Item("ip_address").ToString
        lblMacAddress.Text = DT.Rows(0).Item("mac_address").ToString
        lblComputerName.Text = DT.Rows(0).Item("com_name").ToString
        lblLocation.Text = DT.Rows(0).Item("location_name").ToString
    End Sub

    Private Sub BindHardware()

        Dim DT As DataTable = BL.Alarm_Overview(VENDING_ID, UserName) '= BL.vw_Hardware_Condition(KO_ID)
        Dim animate As Integer = 1
        If Not IsPostBack Then animate = 1000

        Dim Script As String = "$(document).ready(function() {" & vbLf
        '-----------------Bind Ram + CPU
        If DT.Rows.Count = 0 Then
            Script &= " renderPie('MainContent_Monitoring_Content_CPUPie', 'green', 0," & animate & ");" & vbLf
            Script &= " renderPie('MainContent_Monitoring_Content_RAMPie', 'green', 0," & animate & ");" & vbLf
        Else
            Dim TO_DAY_AVALIABLE As Integer = CInt(Val(DT.Rows(0).Item("today_available").ToString))
            Dim CPU As Integer = CInt(Val(DT.Rows(0).Item("cpu_usage").ToString))
            Dim RAM As Integer = CInt(Val(DT.Rows(0).Item("ram_usage").ToString))
            Dim DISK As Integer = CInt(Val(DT.Rows(0).Item("disk_usage").ToString))
            Dim AVALIABLE_STATUS As String = DT.Rows(0).Item("available_status").ToString
            Script &= " renderPie('" & avaliablepie.ClientID & "', '" & IIf(TO_DAY_AVALIABLE > 80, "green", IIf(TO_DAY_AVALIABLE > 60, "orange", "red")) & "', " & TO_DAY_AVALIABLE & "," & animate & ");" & vbLf
            Script &= " renderPie('" & CPUPie.ClientID & "', '" & IIf(CPU > 80, "red", IIf(CPU > 60, "orange", "green")) & "', " & CPU & "," & animate & ");" & vbLf
            Script &= " renderPie('" & RAMPie.ClientID & "', '" & IIf(RAM > 80, "red", IIf(RAM > 60, "orange", "green")) & "', " & RAM & "," & animate & ");" & vbLf
            Script &= " renderPie('" & DrivePie.ClientID & "', '" & IIf(DISK > 80, "red", IIf(DISK > 60, "orange", "green")) & "', " & DISK & "," & animate & ");" & vbLf

            'lblAvariableStatus.ForeColor = Drawing.Color.Red
            'lblAvariableStatus.Text = "Offline"
            'If AVALIABLE_STATUS = "Y" Then
            '    lblAvariableStatus.ForeColor = Drawing.Color.Green
            '    lblAvariableStatus.Text = "Online"
            'End If
        End If
        Script &= "});" & vbLf
        ScriptManager.RegisterStartupScript(Page, GetType(String), "CPU_RAM", Script, True)

        DriveScript = "$(document).ready(function() {"
        DriveScript &= "});" & vbLf
        ScriptManager.RegisterStartupScript(Page, GetType(String), "Drive_Info", DriveScript, True)
    End Sub

    Dim DriveScript As String = ""
    Dim animate As Integer = 1


#Region "Peripheral"
    Private Sub BindPeripheral()
        Dim DT As DataTable = BL.GetList_Vending_Device_Existing(VENDING_ID, "Y")
        DT.DefaultView.RowFilter = "ms_device_type_id in (1,2,3,4,5) AND (Max_Qty IS NOT NULL OR Warning_Qty IS NOT NULL OR Critical_Qty IS NOT NULL)"
        DT = DT.DefaultView.ToTable
        rptDevice.DataSource = DT
        rptDevice.DataBind()
    End Sub

    Private Sub rptDevice_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptDevice.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim spanDevice As HtmlGenericControl = e.Item.FindControl("spanDevice")
        Dim iconDevice As Image = e.Item.FindControl("iconDevice")
        Dim lblDeviceName As Label = e.Item.FindControl("lblDeviceName")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim imgWarning As Image = e.Item.FindControl("imgWarning")

        lblDeviceName.Text = e.Item.DataItem("device_name_en").ToString
        spanDevice.Attributes("Title") = e.Item.DataItem("device_name_en").ToString
        iconDevice.ImageUrl = "Render_Hardware_Icon.aspx?C=W&D=" & e.Item.DataItem("ms_device_id")

        If Not IsDBNull(e.Item.DataItem("is_problem")) Then
            If e.Item.DataItem("is_problem") = "Y" Then
                spanDevice.Attributes("class") = "btn btn-danger col-sm-10 h7 text-left"
                lblStatus.Visible = True
                lblStatus.Text = e.Item.DataItem("status_name").ToString & " "
                lblStatus.ForeColor = Drawing.Color.Yellow
            Else
                spanDevice.Attributes("class") = "btn btn-success col-sm-10 h7 text-left"
                lblStatus.Visible = False
                lblStatus.Text = "OK "
                lblStatus.ForeColor = Drawing.Color.White
            End If
        Else
            spanDevice.Attributes("class") = "btn btn-default col-sm-10 h7 text-left"
            lblStatus.Text = "N/A "
            lblStatus.ForeColor = Drawing.Color.White
        End If
        imgWarning.Visible = lblStatus.Visible

    End Sub
#End Region

#Region "Stock"
    Private Sub BindStock()
        Dim DT As DataTable = BL.GetList_Vending_Device_Existing(VENDING_ID, "Y")
        DT.DefaultView.RowFilter = "ms_device_type_id in (1,2,3,4,5) AND (Max_Qty IS NOT NULL OR Warning_Qty IS NOT NULL OR Critical_Qty IS NOT NULL)"
        DT = DT.DefaultView.ToTable

        Script = "$(document).ready(function() {" & vbLf

        DT.DefaultView.Sort = "device_order"
        '------------ Money Stock -------------
        DT.DefaultView.RowFilter = "ms_device_type_id IN (1,2,3,4)"
        Dim Money As DataTable = DT.DefaultView.ToTable
        Money.DefaultView.RowFilter = ""
        rptMoney.DataSource = Money
        rptMoney.DataBind()
        '------------ Paper Stock -------------
        DT.DefaultView.RowFilter = "ms_device_type_id =5"
        Dim Paper As DataTable = DT.DefaultView.ToTable
        Paper.DefaultView.RowFilter = ""
        rptPrinter.DataSource = Paper
        rptPrinter.DataBind()

        Script &= "});" & vbLf
        'ScriptManager.RegisterStartupScript(Page, GetType(String), "Stock", Script, True)
    End Sub

    Dim Script As String = ""
    Private Sub rpt_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptMoney.ItemDataBound, rptPrinter.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim divAll As HtmlGenericControl = e.Item.FindControl("divAll")
        Dim divContainer As HtmlGenericControl = e.Item.FindControl("divContainer")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblLevel As Label = e.Item.FindControl("lblLevel")
        Dim progress As HtmlGenericControl = e.Item.FindControl("progress")

        Dim rpt As Repeater = sender
        Dim DT As DataTable = rpt.DataSource

        Select Case True
            Case DT.Rows.Count = 1
                divAll.Attributes("class") = "col-sm-12"
            Case DT.Rows.Count = 3
                divAll.Attributes("class") = "col-sm-4"
            Case Else
                divAll.Attributes("class") = "col-sm-6"
        End Select
        '---------------- Control Level----------------
        Dim Max_Qty As Integer = e.Item.DataItem("Max_Qty")
        Dim Warning_Qty As Integer = e.Item.DataItem("Warning_Qty")
        Dim Critical_Qty As Integer = e.Item.DataItem("Critical_Qty")
        Dim Current_Qty As Integer = 0
        If Not IsDBNull(e.Item.DataItem("Current_Qty")) Then Current_Qty = e.Item.DataItem("Current_Qty")


        lblName.Text = e.Item.DataItem("device_name_en").ToString

        '--------------- Set Control Level -----------------------
        If Current_Qty < 0 Then Current_Qty = 0
        If Current_Qty > Max_Qty Then Current_Qty = Max_Qty
        lblLevel.Text = Current_Qty & " / " & Max_Qty

        Dim W As Integer = (Current_Qty * 100 / Max_Qty)
        progress.Style("width") = W & "%"


        Dim Direction As Integer = -1
        If Not IsDBNull(e.Item.DataItem("Movement_Direction")) Then
            Direction = e.Item.DataItem("Movement_Direction")
        End If
        Select Case Direction
            Case 1
                If Current_Qty >= Critical_Qty Then
                    divContainer.Attributes("class") = "row m-a-0 text-danger"
                    progress.Attributes("class") = "progress-bar progress-bar-danger"
                ElseIf Current_Qty >= Warning_Qty Then
                    divContainer.Attributes("class") = "row m-a-0 text-warning"
                    progress.Attributes("class") = "progress-bar progress-bar-warning"
                Else
                    divContainer.Attributes("class") = "row m-a-0 text-success"
                    progress.Attributes("class") = "progress-bar progress-bar-success"
                End If
            Case -1
                If Current_Qty <= Critical_Qty Then
                    divContainer.Attributes("class") = "row m-a-0 text-danger"
                    progress.Attributes("class") = "progress-bar progress-bar-danger"
                ElseIf Current_Qty <= Warning_Qty Then
                    divContainer.Attributes("class") = "row m-a-0 text-warning"
                    progress.Attributes("class") = "progress-bar progress-bar-warning"
                Else
                    divContainer.Attributes("class") = "row m-a-0 text-success"
                    progress.Attributes("class") = "progress-bar progress-bar-success"
                End If
        End Select
    End Sub

#End Region
    Private Sub btnUpdateStatus_Click(sender As Object, e As EventArgs) Handles btnUpdateStatus.Click
        'จำเป็นต้องมี Sub นี้ไว้เพื่อใช้ Refresh หน้าจอ
    End Sub
End Class