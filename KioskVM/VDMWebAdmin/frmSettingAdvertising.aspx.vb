﻿Imports System.Data
Imports ServerLinqDB.ConnectDB
Imports ServerLinqDB.TABLE
Imports System.Data.SqlClient
Imports System.IO
Imports ConstantsWebAdmin

Partial Class frmSettingAdvertising
    Inherits System.Web.UI.Page

    Dim BL As New VDMBL

    Dim cf As ServerLinqDB.TABLE.CfServerSysconfigServerLinqDB = Engine.ServerENG.GetServerConfig
    Dim tempVDOPath As String = Server.MapPath("TempVDOPath")

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property Edit_Ads_ID As String
        Get
            Try
                Return ViewState("Ads_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("Ads_ID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.NA, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterAdvertising, UserName)
            If auDt.Rows.Count = 0 Then
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
                Exit Sub
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()

        If Not IsPostBack Then
            BindList()
            'file1.Attributes.Add("onchange", "asyncVDO('" & Edit_Ads_ID & "','" & file1.ClientID & "','lblVDOFileName');return false;")
        Else
            initFormPlugin()
        End If


    End Sub

    'Protected Sub SetAuthorizedLevel(ByVal DT As DataTable)
    '    'AuthorizedLevel = BL.GetFunctionalAuthorized(DT, FN_ID)
    'End Sub

    Private Sub initFormPlugin()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Plugin", "initFormPlugin();", True)
    End Sub

    Private Sub BindList()
        Dim DT As DataTable = BL.GetAdvertiseList("")
        rptList.DataSource = DT
        rptList.DataBind()

        lblTotalList.Text = FormatNumber(DT.Rows.Count, 0)

        Edit_Ads_ID = "0"
        pnlList.Visible = True
        pnlEdit.Visible = False

        SetAuthorization()
    End Sub

    Private Sub SetAuthorization()
        'ตรวจสอบสิทธิ์การแก้ไขข้อมูล
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.Edit, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterAdvertising, UserName)
            If auDt.Rows.Count = 0 Then
                For i As Integer = 0 To rptList.Items.Count - 1
                    Try
                        Dim btnSpanEdit As HtmlGenericControl = rptList.Items(i).FindControl("btnSpanEdit")
                        Dim btnDelete As LinkButton = rptList.Items(i).FindControl("btnDelete")

                        btnSpanEdit.InnerText = "View"
                        btnDelete.Visible = False
                    Catch ex As Exception

                    End Try
                Next
                btnAdd.Visible = False

                divChooseVDO.Visible = False
                txtAdsCode.Enabled = False
                txtAdsName.Enabled = False
                txtStartDate.Enabled = False
                txtEndDate.Enabled = False

                chkActive.Enabled = False
                btnSave.Visible = False
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        ClearEditForm()

        pnlList.Visible = False
        pnlEdit.Visible = True
    End Sub

    Private Sub ClearEditForm()
        Edit_Ads_ID = "0"
        txtAdsCode.Text = ""
        txtAdsName.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        lblEditMode.Text = ""
        chkActive.Checked = True
        lblEditMode.Text = "Add"

        BL.DeleteFilesFromFolder(tempVDOPath)
        Session.Remove("VDOPath")
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click

        If txtAdsCode.Text.Trim() = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Advertise Code');", True)
            Exit Sub
        End If

        If txtAdsName.Text.Trim() = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Advertise Name');", True)
            Exit Sub
        End If

        If txtStartDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Start Date');", True)
            Exit Sub
        End If

        If txtEndDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert End Date');", True)
            Exit Sub
        End If

        If Converter.StringToDate(txtStartDate.Text, "dd/MM/yyyy") > Converter.StringToDate(txtEndDate.Text, "dd/MM/yyyy") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Start Date must be less than End Date');", True)
            Exit Sub
        End If

        'If Converter.StringToDate(txtStartDate.Text, "dd/MM/yyyy") <= DateTime.Now.Date Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Start Date must be date in advance');", True)
        '    Exit Sub
        'End If


        Dim DT As DataTable = BL.GetAdvertiseList("")
        Dim trans As New ServerTransactionDB
        Try
            DT.DefaultView.RowFilter = "advertise_code='" & txtAdsCode.Text.Replace("'", "''") & "' AND id <> " & CInt(Edit_Ads_ID)
            If DT.DefaultView.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This Advertise Code is already existed');", True)
                Exit Sub
            End If

            DT.DefaultView.RowFilter = ""
            DT.DefaultView.RowFilter = "advertise_name='" & txtAdsName.Text.Replace("'", "''") & "' AND id <>" & CInt(Edit_Ads_ID)
            If DT.DefaultView.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This Advertise Name is already existed');", True)
                Exit Sub
            End If

            Dim fVdo As IO.FileInfo
            Dim vdopath As String = Session("VDOPath")

            Dim lnqLoc As New MsAdvertiseServerLinqDB
            lnqLoc.ChkDataByPK(Edit_Ads_ID, trans.Trans)
            With lnqLoc
                .ADVERTISE_CODE = txtAdsCode.Text.Trim().Replace("'", "''")
                .ADVERTISE_NAME = txtAdsName.Text.Trim().Replace("'", "''")

                If Not vdopath Is Nothing Then
                    fVdo = New IO.FileInfo(vdopath)
                    If fVdo.Exists Then
                        '.ADS_FILE_BYPE = IO.File.ReadAllBytes(fVdo.FullName)
                        .ADS_FILE_EXT = fVdo.Extension
                        .ADS_MIME_TYPE = fVdo.Extension
                    End If
                End If

                .START_DATE = Converter.StringToDate(txtStartDate.Text, "dd/MM/yyyy")
                .END_DATE = Converter.StringToDate(txtEndDate.Text, "dd/MM/yyyy")
                .ACTIVE_STATUS = IIf(chkActive.Checked, "Y", "N")
            End With

            Dim ret As New ExecuteDataInfo
            If lnqLoc.ID = 0 Then
                ret = lnqLoc.InsertData(UserName, trans.Trans)
            Else
                ret = lnqLoc.UpdateData(UserName, trans.Trans)
            End If
            If ret.IsSuccess = True Then
                trans.CommitTransaction()

                If Not vdopath Is Nothing Then
                    fVdo = New IO.FileInfo(vdopath)
                    If fVdo.Exists Then
                        If Directory.Exists(cf.ADS_VDO_PATH) = False Then
                            Directory.CreateDirectory(cf.ADS_VDO_PATH)
                        End If

                        Dim DestinationFilePath As String = cf.ADS_VDO_PATH & lnqLoc.ID & ".mp4"
                        If File.Exists(DestinationFilePath) = True Then
                            Try
                                File.Delete(DestinationFilePath)
                            Catch ex As Exception
                            End Try
                        End If

                        Try
                            File.Move(fVdo.FullName, DestinationFilePath)
                        Catch ex As Exception
                        End Try

                    End If
                End If

                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save success');", True)
                BindList()
            Else
                trans.RollbackTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", "") & "');", True)
            End If
            lnqLoc = Nothing
        Catch ex As Exception
            trans.RollbackTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('cant save data!' " & ex.ToString().Replace("'", "") & ");", True)
        End Try


    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        BindList()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        'Dim td As HtmlTableCell = e.Item.FindControl("td")
        Dim lblAdsID As Label = e.Item.FindControl("lblAdsID")
        Dim lblAdsCode As Label = e.Item.FindControl("lblAdsCode")
        Dim lblAdsName As Label = e.Item.FindControl("lblAdsName")
        Dim lblStartDate As Label = e.Item.FindControl("lblStartDate")
        Dim lblEndDate As Label = e.Item.FindControl("lblEndDate")

        Dim btnEdit As LinkButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As LinkButton = e.Item.FindControl("btnDelete")
        Dim cfmDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfmDelete")

        'Dim ColEdit As HtmlTableCell = e.Item.FindControl("ColEdit")
        'Dim ColDelete As HtmlTableCell = e.Item.FindControl("ColDelete")

        lblAdsID.Text = e.Item.DataItem("id").ToString
        lblAdsCode.Text = e.Item.DataItem("advertise_code").ToString
        lblAdsName.Text = e.Item.DataItem("advertise_name").ToString
        lblStartDate.Text = e.Item.DataItem("start_date").ToString
        lblEndDate.Text = e.Item.DataItem("end_date").ToString
        'txtStartDate.Text = Convert.ToDateTime(DT.Rows(0).Item("Start_Date")).ToString("dd/MM/yyyy", New Globalization.CultureInfo("en-US"))
        'txtEndDate.Text = Convert.ToDateTime(DT.Rows(0).Item("End_Date")).ToString("dd/MM/yyyy", New Globalization.CultureInfo("en-US"))

        If Not e.Item.DataItem("active_status").ToString = "Y" Then
            lblAdsCode.Attributes("class") = "text-danger"
            lblAdsName.Attributes("class") = "text-danger"
            lblStartDate.Attributes("class") = "text-danger"
            lblEndDate.Attributes("class") = "text-danger"
        Else
            lblAdsCode.Attributes("class") = "text-primary"
            lblAdsName.Attributes("class") = "text-primary"
            lblStartDate.Attributes("class") = "text-primary"
            lblEndDate.Attributes("class") = "text-primary"
        End If

        Dim vdo_url As String = cf.ADS_VDO_URL & e.Item.DataItem("id").ToString & ".mp4"
        Dim displayVDO As HtmlVideo = e.Item.FindControl("displayVDO")
        displayVDO.Attributes.Remove("src")
        displayVDO.Attributes.Add("src", vdo_url)

        btnEdit.CommandArgument = e.Item.DataItem("id")
        btnDelete.CommandArgument = e.Item.DataItem("id")
        cfmDelete.ConfirmText = "Confirm to delete " & e.Item.DataItem("advertise_name").ToString & " ?"
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                ClearEditForm()
                Dim Ads_ID As String = e.CommandArgument
                Edit_Ads_ID = Ads_ID

                Dim DT As DataTable = BL.GetAdvertiseList(Ads_ID)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & Ads_ID.Replace("'", """") & " is not found!');", True)
                    Exit Sub
                End If

                txtAdsCode.Text = DT.Rows(0).Item("advertise_code").ToString
                txtAdsName.Text = DT.Rows(0).Item("advertise_name").ToString
                txtStartDate.Text = DT.Rows(0).Item("start_date").ToString
                txtEndDate.Text = DT.Rows(0).Item("end_date").ToString

                chkActive.Checked = IIf(DT.Rows(0).Item("active_status").ToString() = "Y", True, False)
                lblEditMode.Text = "Edit"

                pnlList.Visible = False
                pnlEdit.Visible = True

                SetAuthorization()
            Case "Delete"
                Dim ret As String = BL.DeleteAdvertise(e.CommandArgument)
                If ret = "" Then
                    Dim fVdo As IO.FileInfo
                    fVdo = New IO.FileInfo(cf.ADS_VDO_PATH & e.CommandArgument & ".avi")
                    If fVdo.Exists Then
                        fVdo.Delete()
                    End If

                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.Replace("'", """") & "');", True)
                End If
        End Select
    End Sub



End Class
