﻿Imports System.Data
Imports ServerLinqDB.ConnectDB
Imports ServerLinqDB.TABLE
Imports System.Data.SqlClient
Imports ConstantsWebAdmin


Public Class frmSettingVendingMachine
    Inherits System.Web.UI.Page

    Dim BL As New VDMBL
    Dim DeviceList As DataTable
    Dim StatusList As DataTable

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property Edit_Vending_ID As Integer
        Get
            Try
                Return ViewState("KO_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            ViewState("KO_ID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If

        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.NA, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterVendingMachine, UserName)
            If auDt.Rows.Count = 0 Then
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
                Exit Sub
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()

        If Not IsPostBack Then
            BindList()

            BL.SetTextIntKeypress(txtIP1)
            BL.SetTextIntKeypress(txtIP2)
            BL.SetTextIntKeypress(txtIP3)
            BL.SetTextIntKeypress(txtIP4)

        End If
        initFormPlugin()
    End Sub

    Private Sub initFormPlugin()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Plugin", "initFormPlugin(); ", True)
    End Sub

    Private Sub btnUpdateStatus_Click(sender As Object, e As EventArgs) Handles btnUpdateStatus.Click
        BindList()
    End Sub

    Private Sub BindList()
        DeviceList = BL.GetList_Vending_Device_Existing(0, "Y")
        StatusList = BL.GetList_DeviceStatus(0, 0)

        'btnAdd.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit

        Dim dt As New DataTable
        dt = BL.GetList_Vending(UserName, 0)
        rptList.DataSource = dt
        rptList.DataBind()
        lblTotalList.Text = FormatNumber(dt.Rows.Count, 0)

        pnlList.Visible = True
        pnlEdit.Visible = False

        SetAuthorization()

    End Sub

    Private Sub SetAuthorization()
        'ตรวจสอบสิทธิ์การแก้ไขข้อมูล
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.Edit, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterVendingMachine, UserName)
            If auDt.Rows.Count = 0 Then
                btnAdd.Visible = False

                For i As Integer = 0 To rptList.Items.Count - 1
                    Dim btnDelete As LinkButton = rptList.Items(i).FindControl("btnDelete")
                    Dim btnEdit As LinkButton = rptList.Items(i).FindControl("btnEdit")

                    btnEdit.Text = "View Info"
                    btnDelete.Visible = False
                Next

                ddlLocation.Enabled = False
                txtComName.Enabled = False
                txtMAC1.Enabled = False
                txtMAC2.Enabled = False
                txtMAC3.Enabled = False
                txtMAC4.Enabled = False
                txtMAC5.Enabled = False
                txtMAC6.Enabled = False
                txtIP1.Enabled = False
                txtIP2.Enabled = False
                txtIP3.Enabled = False
                txtIP4.Enabled = False

                For i As Integer = 0 To rpt_Stock.Items.Count - 1
                    Dim txtMax As TextBox = rpt_Stock.Items(i).FindControl("txtMax")
                    Dim txtWarningDown As TextBox = rpt_Stock.Items(i).FindControl("txtWarningDown")
                    Dim txtCriticalDown As TextBox = rpt_Stock.Items(i).FindControl("txtCriticalDown")
                    Dim txtCriticalUp As TextBox = rpt_Stock.Items(i).FindControl("txtCriticalUp")
                    Dim txtWarningUp As TextBox = rpt_Stock.Items(i).FindControl("txtWarningUp")

                    txtMax.Enabled = False
                    txtWarningDown.Enabled = False
                    txtWarningUp.Enabled = False
                    txtCriticalDown.Enabled = False
                    txtCriticalUp.Enabled = False
                Next
                For i As Integer = 0 To rptProduct.Items.Count - 1
                    Dim chk As CheckBox = rptProduct.Items(i).FindControl("chk")
                    Dim txtProfitSharing As TextBox = rptProduct.Items(i).FindControl("txtProfitSharing")

                    chk.Enabled = False
                    txtProfitSharing.Enabled = False
                Next
                For i As Integer = 0 To rptAds.Items.Count - 1
                    Dim chkItemAds As CheckBox = rptAds.Items(i).FindControl("chkItemAds")
                    chkItemAds.Enabled = False
                Next

                chkActive.Enabled = False
                btnSave.Visible = False
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()
    End Sub

    Private Sub ClearEditForm()
        Edit_Vending_ID = 0

        BL.Bind_DDL_Location(ddlLocation, UserName)

        txtComName.Text = ""
        txtIP1.Text = ""
        txtIP2.Text = ""
        txtIP3.Text = ""
        txtIP4.Text = ""
        txtMAC1.Text = ""
        txtMAC2.Text = ""
        txtMAC3.Text = ""
        txtMAC4.Text = ""
        txtMAC5.Text = ""
        txtMAC6.Text = ""

        ''------- Manage Material Stock Control Level --------
        BindMaterialStock()
        BindProduct(0)
        BindAds(0)

        chkActive.Checked = True
        lblEditMode.Text = "Add"
    End Sub

    'Private Sub GenerateLockerInformation(KO_ID As Integer)
    '    Dim dt As New DataTable
    '    dt = BL.GetLockerInformation(KO_ID)
    '    'dt = BL.GetLockerInformation(1)

    '    Dim str As New StringBuilder
    '    If dt.Rows.Count > 0 Then
    '        Dim sql As String = "select t.ms_locker_id, t.trans_start_time, t.passport_no, t.idcard_no, t.nation_code, " & vbNewLine
    '        sql += " t.first_name, t.last_name, t.cust_image, l.locker_name " & vbNewLine
    '        sql += " from TB_SERVICE_TRANSACTION t " & vbNewLine
    '        sql += " inner join MS_LOCKER l On l.id=t.ms_locker_id " & vbNewLine
    '        sql += " left join TB_PICKUP_TRANSACTION p On p.deposit_trans_no=t.trans_no " & vbNewLine
    '        sql += " where t.trans_status=1 And p.id Is null " & vbNewLine
    '        sql += " And l.current_available ='N' and l.active_status='Y'" & vbNewLine
    '        Dim InfoDt As DataTable = ServerDB.ExecuteTable(sql)

    '        Dim dt_cabinet As New DataTable
    '        dt_cabinet = dt.DefaultView.ToTable(True, "order_layout").Copy()

    '        Dim table_height As Integer = 400   'height of table
    '        Dim colwidth As Integer = dt_cabinet.Rows.Count * 100
    '        str.AppendLine("<center>")
    '        str.AppendLine("<table style='height:" & table_height & "px; width:" & colwidth & "px;padding: 0; border-spacing: 0' >")
    '        str.AppendLine("<tr>")

    '        Dim pcPosition As Long = BL.GetKioskSysconfig(KO_ID).LOCKER_PC_POSITION

    '        Dim dr As DataRow
    '        dr = dt_cabinet.NewRow
    '        dr("order_layout") = "-1"
    '        dt_cabinet.Rows.InsertAt(dr, (pcPosition - 1))
    '        For i As Integer = 0 To dt_cabinet.Rows.Count - 1
    '            Dim order_layout As String = dt_cabinet.Rows(i)("order_layout").ToString()
    '            dt.DefaultView.RowFilter = "order_layout = '" & order_layout & "'"

    '            Dim dt_detail As New DataTable
    '            dt_detail = dt.DefaultView.ToTable

    '            Dim cabinet_height As Integer = table_height
    '            If dt_detail.Rows.Count > 0 Then
    '                cabinet_height = Math.Ceiling(table_height / dt_detail.Rows.Count)
    '            End If

    '            str.AppendLine("<td>")
    '            str.AppendLine("<table  border='1' style='height:" & table_height & "px; width:100px; padding: 0; border-spacing: 0'>")

    '            If order_layout = "-1" Then
    '                str.AppendLine("<tr >")
    '                str.AppendLine("    <td style='height:" & cabinet_height & "px; background-color:white; text-align:center;'>&nbsp;</td>")
    '                str.AppendLine("</tr>")
    '            Else
    '                For j As Integer = 0 To dt_detail.Rows.Count - 1
    '                    Dim locker_name As String = dt_detail.Rows(j)("Locker_Name").ToString()
    '                    Dim current_available As String = dt_detail(j)("current_available").ToString()
    '                    Dim bgcolor As String = "#000000"
    '                    Dim forecolor As String = "#FFFFFF"
    '                    Dim ScriptDialog = ""
    '                    Dim CursorStyle = ""

    '                    If current_available = "Y" Then
    '                        bgcolor = "#33B2FF"
    '                        forecolor = "#000000"
    '                    Else
    '                        InfoDt.DefaultView.RowFilter = "ms_locker_id=" & dt_detail.Rows(j)("ms_locker_id")
    '                        If InfoDt.DefaultView.Count > 0 Then
    '                            Dim iDr As DataRowView = InfoDt.DefaultView(0)

    '                            Dim FirstName As String = ""
    '                            Dim LastName As String = ""
    '                            Dim PassportNo As String = ""
    '                            Dim IDCardNo As String = ""
    '                            Dim DepositTime As String = ""
    '                            Dim CustImg As String = "&nbsp;"

    '                            If Convert.IsDBNull(iDr("first_name")) = False Then FirstName = iDr("first_name")
    '                            If Convert.IsDBNull(iDr("last_name")) = False Then LastName = iDr("last_name")
    '                            If Convert.IsDBNull(iDr("passport_no")) = False Then PassportNo = iDr("passport_no")
    '                            If Convert.IsDBNull(iDr("idcard_no")) = False Then IDCardNo = iDr("idcard_no")
    '                            If Convert.IsDBNull(iDr("trans_start_time")) = False Then DepositTime = Convert.ToDateTime(iDr("trans_start_time")).ToString("dd/MM/yyyy HH:mm", New Globalization.CultureInfo("en-US"))
    '                            If Convert.IsDBNull(iDr("cust_image")) = False Then
    '                                CustImg = "data:image/" & FirstName & ";base64," & Convert.ToBase64String(iDr("cust_image"))
    '                            End If


    '                            Dim TextInfo As String = "'<div class=' + 'col-sm-12  table-responsive' + ' >  "
    '                            TextInfo += "    <table class=' + 'table m-b' + ' >"
    '                            TextInfo += "       <thead>"
    '                            TextInfo += "           <tr>"
    '                            TextInfo += "               <th colspan='+'3'+' class='+'h4 text-center'+'>" & FirstName & " " & LastName & "</th>"
    '                            TextInfo += "           </tr>"
    '                            TextInfo += "       </thead>"
    '                            TextInfo += "       <tbody>"
    '                            TextInfo += "           <tr>"
    '                            If PassportNo.Trim <> "" Then
    '                                TextInfo += "               <td class='+'col-sm-5'+'>Passport No :</td>"
    '                                TextInfo += "               <td >" & PassportNo & "</td>"
    '                            Else
    '                                TextInfo += "               <td class='+'col-sm-5'+'>ID Card No :</td>"
    '                                TextInfo += "               <td >" & IDCardNo & "</td>"
    '                            End If
    '                            TextInfo += "               <td rowspan='+'3'+'><img src='+'" & CustImg & "'+' style='+'width:150px;'+'  /></td>"
    '                            TextInfo += "           </tr>"
    '                            TextInfo += "           <tr>"
    '                            TextInfo += "               <td >Locker Name :</td>"
    '                            TextInfo += "               <td >" & dt_detail.Rows(j)("Locker_Name").ToString() & "</td>"
    '                            TextInfo += "           </tr>"
    '                            TextInfo += "           <tr>"
    '                            TextInfo += "               <td >Deposit Time :</td>"
    '                            TextInfo += "               <td >" & DepositTime & "</td>"
    '                            TextInfo += "           </tr>"
    '                            TextInfo += "       </tbody>"
    '                            TextInfo += "   </table>"
    '                            TextInfo += "</div>'"


    '                            ScriptDialog = "onclick=""ShowDialogProfile('" & locker_name & "'," & TextInfo & ")"""

    '                            CursorStyle = "cursor:pointer;"
    '                        End If

    '                        InfoDt.DefaultView.RowFilter = ""
    '                    End If

    '                    str.AppendLine("<tr style='border-width:1px;padding: 0; border-spacing: 0; border-style:solid; border-color:black'>")
    '                    str.AppendLine("    <td " & ScriptDialog & " style='height:" & cabinet_height & "px; background-color:" & bgcolor & "; text-align:center; font-size:25px; color:" & forecolor & ";" & CursorStyle & "'>")
    '                    str.Append("        " & locker_name)
    '                    str.Append("    </td>")
    '                    str.AppendLine("</tr>")
    '                Next
    '            End If

    '            str.AppendLine("</table>")
    '            str.AppendLine("</td>")
    '            dt.DefaultView.RowFilter = ""
    '        Next
    '        InfoDt.Dispose()

    '        str.AppendLine("</tr>")
    '        str.AppendLine("</table>")
    '        str.AppendLine("</center>")
    '        ltrlockerinfo.Text = str.ToString()
    '    Else
    '        ltrlockerinfo.Text = ""
    '    End If
    'End Sub

    Private Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        ClearEditForm()

        pnlList.Visible = False
        pnlEdit.Visible = True
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        BindList()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        '-------------- Main Kiosk Info---------------------
        'Dim h3 As HtmlGenericControl = e.Item.FindControl("h3")
        Dim imgKioskIcon As Image = e.Item.FindControl("imgKioskIcon")
        Dim lblComName As Label = e.Item.FindControl("lblComName")
        Dim lblIP As Label = e.Item.FindControl("lblIP")
        Dim lblLocation As Label = e.Item.FindControl("lblLocation")
        Dim btnMonitor As LinkButton = e.Item.FindControl("btnMonitor")
        Dim btnEdit As LinkButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As LinkButton = e.Item.FindControl("btnDelete")
        Dim cfmDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfmDelete")

        btnMonitor.PostBackUrl = "frmAlarmMonitoringView.aspx?V=" & e.Item.DataItem("ms_vending_id")
        btnEdit.CommandArgument = e.Item.DataItem("ms_vending_id")
        btnDelete.CommandArgument = e.Item.DataItem("ms_vending_id")
        cfmDelete.ConfirmText = "Are you sure to delete " & e.Item.DataItem("com_name").ToString & " permanently ?"
        btnEdit.OnClientClick &= "clearInterval(timerRefresh);"

        If e.Item.DataItem("active_status").ToString() = "Y" Then
            imgKioskIcon.ImageUrl = "images/icon/100/koisk_ok.png"
        Else
            imgKioskIcon.ImageUrl = "images/icon/100/koisk_ab.png"
        End If
        lblComName.Text = e.Item.DataItem("com_name").ToString
        lblIP.Text = e.Item.DataItem("ip_address").ToString

        Dim trans As New ServerTransactionDB
        Dim lnqloc As New MsLocationServerLinqDB
        lnqloc.GetDataByPK(e.Item.DataItem("ms_location_id").ToString, trans.Trans)

        If lnqloc.LOCATION_NAME <> "" Then
            lblLocation.Text = lnqloc.LOCATION_NAME
        Else
            lblLocation.Text = e.Item.DataItem("location_code").ToString
        End If

        '--------------- Printer Info ---------------
        Dim Printer As uc_Printer_Stock_UI = e.Item.FindControl("Printer")
        Dim pnlBlankPrinter As Panel = e.Item.FindControl("pnlBlankPrinter")
        DeviceList.DefaultView.RowFilter = "ms_device_id=7 and ms_vending_id=" & e.Item.DataItem("ms_vending_id")
        If DeviceList.DefaultView.Count = 0 Then
            Printer.Visible = False
            pnlBlankPrinter.Visible = True
        Else
            Dim Max_Qty As Object = DeviceList.DefaultView(0).Item("Max_Qty")
            Dim Warning_Qty As Object = DeviceList.DefaultView(0).Item("Warning_Qty")
            Dim Critical_Qty As Object = DeviceList.DefaultView(0).Item("Critical_Qty")
            Dim Current_Qty As Object = DeviceList.DefaultView(0).Item("Current_Qty")
            Printer.BindPrinter(Current_Qty, Warning_Qty, Critical_Qty, Max_Qty)
        End If

        '--------------- Peripheral UI ---------------
        DeviceList.DefaultView.RowFilter = "ms_vending_id=" & e.Item.DataItem("ms_vending_id")
        Dim Peripheral As uc_Peripheral_UI = e.Item.FindControl("Peripheral")
        Peripheral.BindPeripheral(DeviceList.DefaultView.ToTable.Copy, StatusList)

        '-----------Money Stock Level ---------------
        DeviceList.DefaultView.RowFilter = "ms_vending_id=" & e.Item.DataItem("ms_vending_id") & " AND ms_device_type_id in(1,2,3,4) AND Max_Qty IS NOT NULL AND Warning_Qty IS NOT NULL AND Critical_Qty IS NOT NULL"
        Dim MoneyStock As uc_MoneyStock_UI = e.Item.FindControl("MoneyStock")
        MoneyStock.BindMoneyStock(DeviceList.DefaultView.ToTable.Copy)

        'btnEdit.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'btnDelete.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'btnMonitor.Visible = AuthorizedAlarm <> TSKBL.AuthorizedLevel.None
    End Sub

    Private Sub rptMoneyStock_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim divContainer As HtmlGenericControl = e.Item.FindControl("divContainer")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblLevel As Label = e.Item.FindControl("lblLevel")
        Dim progress As HtmlGenericControl = e.Item.FindControl("progress")

        '---------------- Control Level----------------
        Dim Max_Qty As Integer = e.Item.DataItem("Max_Qty")
        Dim Warning_Qty As Integer = e.Item.DataItem("Warning_Qty")
        Dim Critical_Qty As Integer = e.Item.DataItem("Critical_Qty")
        Dim Current_Qty As Integer = 0
        If Not IsDBNull(e.Item.DataItem("Current_Qty")) Then Current_Qty = e.Item.DataItem("Current_Qty")


        lblName.Text = e.Item.DataItem("D_Name").ToString

        '--------------- Set Control Level -----------------------
        If Current_Qty < 0 Then Current_Qty = 0
        If Current_Qty > Max_Qty Then Current_Qty = Max_Qty
        lblLevel.Text = Current_Qty & " / " & Max_Qty
        progress.Style("width") = (Current_Qty * 100 / Max_Qty) & "%"
        Dim Direction As Integer = -1
        If Not IsDBNull(e.Item.DataItem("Movement_Direction")) Then
            Direction = e.Item.DataItem("Movement_Direction")
        End If
        Select Case Direction
            Case 1
                If Current_Qty >= Critical_Qty Then
                    divContainer.Attributes("class") = "row m-a-0 text-danger"
                    progress.Attributes("class") = "progress-bar progress-bar-danger"
                ElseIf Current_Qty >= Warning_Qty Then
                    divContainer.Attributes("class") = "row m-a-0 text-warning"
                    progress.Attributes("class") = "progress-bar progress-bar-warning"
                Else
                    divContainer.Attributes("class") = "row m-a-0 text-success"
                    progress.Attributes("class") = "progress-bar progress-bar-success"
                End If
            Case -1
                If Current_Qty <= Critical_Qty Then
                    divContainer.Attributes("class") = "row m-a-0 text-danger"
                    progress.Attributes("class") = "progress-bar progress-bar-danger"
                ElseIf Current_Qty <= Warning_Qty Then
                    divContainer.Attributes("class") = "row m-a-0 text-warning"
                    progress.Attributes("class") = "progress-bar progress-bar-warning"
                Else
                    divContainer.Attributes("class") = "row m-a-0 text-success"
                    progress.Attributes("class") = "progress-bar progress-bar-success"
                End If
        End Select
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                ClearEditForm()
                Dim Vending_ID As Integer = e.CommandArgument
                Edit_Vending_ID = Vending_ID
                Dim lblComName As Label = e.Item.FindControl("lblComName")
                Dim DT As DataTable = BL.GetList_Vending(UserName, Vending_ID)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & lblComName.Text.Replace("'", """") & " is not found!');", True)
                    Exit Sub
                End If

                ddlLocation.SelectedValue = DT.Rows(0).Item("ms_location_id").ToString
                txtComName.Text = DT.Rows(0).Item("com_name").ToString

                Dim IP As String() = Split(DT.Rows(0).Item("ip_address").ToString, ".")
                If IP.Length > 0 Then txtIP1.Text = IP(0)
                If IP.Length > 1 Then txtIP2.Text = IP(1)
                If IP.Length > 2 Then txtIP3.Text = IP(2)
                If IP.Length > 3 Then txtIP4.Text = IP(3)

                BindMaterialStock()
                Dim mac_address As String() = Split(DT.Rows(0).Item("mac_address").ToString, "-")
                If mac_address.Length > 0 Then txtMAC1.Text = mac_address(0)
                If mac_address.Length > 1 Then txtMAC2.Text = mac_address(1)
                If mac_address.Length > 2 Then txtMAC3.Text = mac_address(2)
                If mac_address.Length > 3 Then txtMAC4.Text = mac_address(3)
                If mac_address.Length > 4 Then txtMAC5.Text = mac_address(4)
                If mac_address.Length > 5 Then txtMAC6.Text = mac_address(5)

                BindAds(Vending_ID)
                BindProduct(Vending_ID)

                chkActive.Checked = IIf(DT.Rows(0).Item("active_status").ToString = "Y", True, False)
                lblEditMode.Text = "Edit"

                pnlList.Visible = False
                pnlEdit.Visible = True

                SetAuthorization()
            Case "Delete"
                Dim ret As Boolean = False
                ret = BL.DeleteMasterVending(e.CommandArgument)
                If ret Then
                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Cant Delete Vending ID'" & e.CommandArgument & ");", True)
                End If
        End Select
    End Sub

#Region "Material Stock"
    Protected Sub BindMaterialStock()
        Dim DT As DataTable = BL.GetList_Vending_Device(Edit_Vending_ID, "Y")
        DT.DefaultView.RowFilter = "ms_device_type_id in (1,2,3,4,5) AND (Max_Qty IS NOT NULL OR Warning_Qty IS NOT NULL OR Critical_Qty IS NOT NULL)"

        DT = DT.DefaultView.ToTable
        rpt_Stock.DataSource = DT
        rpt_Stock.DataBind()
    End Sub

    Private Sub rpt_Stock_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rpt_Stock.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim imgIcon As Image = e.Item.FindControl("imgIcon")
        Dim lblDeviceName As Label = e.Item.FindControl("lblDeviceName")
        Dim txtMax As TextBox = e.Item.FindControl("txtMax")


        Dim pnlMoveDown As Panel = e.Item.FindControl("pnlMoveDown")
        Dim txtWarningDown As TextBox = e.Item.FindControl("txtWarningDown")
        Dim txtCriticalDown As TextBox = e.Item.FindControl("txtCriticalDown")

        Dim pnlMoveUp As Panel = e.Item.FindControl("pnlMoveUp")
        Dim txtWarningUp As TextBox = e.Item.FindControl("txtWarningUp")
        Dim txtCriticalUp As TextBox = e.Item.FindControl("txtCriticalUp")

        imgIcon.ImageUrl = "Render_Hardware_Icon.aspx?D=" & e.Item.DataItem("ms_device_id").ToString & "&C=G"
        lblDeviceName.Text = e.Item.DataItem("device_name_en").ToString
        If Not IsDBNull(e.Item.DataItem("Max_Qty")) Then
            txtMax.Text = e.Item.DataItem("Max_Qty")
        Else

        End If

        Select Case e.Item.DataItem("Movement_Direction")
            Case -1
                pnlMoveDown.Visible = True
                If Not IsDBNull(e.Item.DataItem("Warning_Qty")) Then
                    txtWarningDown.Text = e.Item.DataItem("Warning_Qty")
                Else

                End If
                If Not IsDBNull(e.Item.DataItem("Critical_Qty")) Then
                    txtCriticalDown.Text = e.Item.DataItem("Critical_Qty")
                Else

                End If
            Case 1
                pnlMoveUp.Visible = True
                If Not IsDBNull(e.Item.DataItem("Warning_Qty")) Then
                    txtWarningUp.Text = e.Item.DataItem("Warning_Qty")
                Else
                End If
                If Not IsDBNull(e.Item.DataItem("Critical_Qty")) Then
                    txtCriticalUp.Text = e.Item.DataItem("Critical_Qty")
                Else
                End If
        End Select
        txtMax.Attributes("ms_device_id") = e.Item.DataItem("ms_device_id")

        BL.SetTextIntKeypress(txtMax)
        BL.SetTextIntKeypress(txtWarningUp)
        BL.SetTextIntKeypress(txtWarningDown)
        BL.SetTextIntKeypress(txtCriticalUp)
        BL.SetTextIntKeypress(txtCriticalDown)
    End Sub

    Protected Function CurrentMaterialStock() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("ms_device_id", GetType(Integer))
        DT.Columns.Add("device_name_en", GetType(String))
        DT.Columns.Add("max_qty", GetType(Integer))
        DT.Columns.Add("warning_qty", GetType(Integer))
        DT.Columns.Add("critical_qty", GetType(Integer))
        DT.Columns.Add("movement_direction", GetType(Integer))

        For Each Item As RepeaterItem In rpt_Stock.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim DR As DataRow = DT.NewRow

            Dim lblDeviceName As Label = Item.FindControl("lblDeviceName")
            Dim txtMax As TextBox = Item.FindControl("txtMax")

            Dim pnlMoveDown As Panel = Item.FindControl("pnlMoveDown")
            Dim txtWarningDown As TextBox = Item.FindControl("txtWarningDown")
            Dim txtCriticalDown As TextBox = Item.FindControl("txtCriticalDown")

            Dim pnlMoveUp As Panel = Item.FindControl("pnlMoveUp")
            Dim txtWarningUp As TextBox = Item.FindControl("txtWarningUp")
            Dim txtCriticalUp As TextBox = Item.FindControl("txtCriticalUp")

            If pnlMoveUp.Visible Then
                DR("movement_direction") = 1
            Else
                DR("movement_direction") = -1
            End If

            DR("ms_device_id") = txtMax.Attributes("ms_device_id")
            DR("device_name_en") = lblDeviceName.Text
            If IsNumeric(txtMax.Text) Then
                DR("max_qty") = CInt(txtMax.Text)
            End If

            Select Case DR("movement_direction")
                Case -1
                    If IsNumeric(txtWarningDown.Text) Then
                        DR("warning_qty") = CInt(txtWarningDown.Text)
                    End If
                    If IsNumeric(txtCriticalDown.Text) Then
                        DR("critical_qty") = CInt(txtCriticalDown.Text)
                    End If
                Case 1
                    If IsNumeric(txtWarningUp.Text) Then
                        DR("warning_qty") = CInt(txtWarningUp.Text)
                    End If
                    If IsNumeric(txtCriticalUp.Text) Then
                        DR("critical_qty") = CInt(txtCriticalUp.Text)
                    End If
            End Select
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

#End Region
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

#Region "Validate"
        If ddlLocation.SelectedIndex < 1 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Select Location');", True)
            Exit Sub
        End If

        'If txtComName.Text.Trim = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Please Insert Computer Name');", True)
        '    Exit Sub
        'End If

        Dim mac_address As String = txtMAC1.Text.Trim() & "-" & txtMAC2.Text.Trim() & "-" & txtMAC3.Text.Trim() & "-" & txtMAC4.Text.Trim() & "-" & txtMAC5.Text.Trim() & "-" & txtMAC6.Text.Trim()
        If mac_address.Replace("-", "").Length <> 12 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Invalid Mac Address');", True)
            Exit Sub
        End If

        If Not IsNumeric(txtIP1.Text) Or Not IsNumeric(txtIP2.Text) Or Not IsNumeric(txtIP3.Text) Or Not IsNumeric(txtIP4.Text) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Invalid format IP Adress');", True)
            Exit Sub
        End If

        Dim ip_address As String = txtIP1.Text.Trim() & "." & txtIP2.Text.Trim() & "." & txtIP3.Text.Trim() & "." & txtIP4.Text.Trim()
        Dim IsDupplicate As Boolean = BL.IsDupplicate_Vending(ip_address, mac_address, Edit_Vending_ID)
        If IsDupplicate = True Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('IP Address or MAC Address is already existed');", True)
            Exit Sub
        End If

        Dim MatStock As DataTable = CurrentMaterialStock()
        Dim tmp As DataTable = MatStock.Copy

        tmp.DefaultView.RowFilter = "Max_Qty IS NULL"
        If tmp.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & tmp.DefaultView(0).Item("device_name_en") & "\' max quantity must be set');", True)
            Exit Sub
        End If
        tmp.DefaultView.RowFilter = "Warning_Qty IS NULL"
        If tmp.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & tmp.DefaultView(0).Item("device_name_en") & "\' warning quantity must be set');", True)
            Exit Sub
        End If
        tmp.DefaultView.RowFilter = "Critical_Qty IS NULL"
        If tmp.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & tmp.DefaultView(0).Item("device_name_en") & "\' critical quantity must be set');", True)
            Exit Sub
        End If

        tmp.DefaultView.RowFilter = "Max_Qty < 0"
        If tmp.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & tmp.DefaultView(0).Item("device_name_en") & "\' max quantity must be greater than zero');", True)
            Exit Sub
        End If
        tmp.DefaultView.RowFilter = "Warning_Qty < 0"
        If tmp.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & tmp.DefaultView(0).Item("device_name_en") & "\' warning quantity  must be greater than zero');", True)
            Exit Sub
        End If
        tmp.DefaultView.RowFilter = "Critical_Qty < 0"
        If tmp.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & tmp.DefaultView(0).Item("device_name_en") & "\' critical quantity  must be greater than zero');", True)
            Exit Sub
        End If

        Dim Filter As String = "(Movement_Direction=-1 AND Warning_Qty>Max_Qty) OR "
        Filter &= "(Movement_Direction=-1 AND Critical_Qty>Warning_Qty) OR "
        Filter &= "(Movement_Direction=1 AND Critical_Qty>Max_Qty) OR "
        Filter &= "(Movement_Direction=1 AND Warning_Qty>Critical_Qty)"

        tmp.DefaultView.RowFilter = Filter
        If tmp.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & tmp.DefaultView(0).Item("device_name_en") & "\' control level setting is not reasonable\n\nMax Qty > Warning Qty > Critical Qty');", True)
            Exit Sub
        End If

        For i As Integer = 0 To rptProduct.Items.Count - 1
            Dim chk As CheckBox = rptProduct.Items(i).FindControl("chk")
            If chk.Checked = True Then
                Dim txtProfitSharing As TextBox = rptProduct.Items(i).FindControl("txtProfitSharing")
                If CInt(txtProfitSharing.Text) > 100 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Profit Sharing must be less than 100');", True)
                    Exit Sub
                End If
            End If
        Next
#End Region

#Region "Save"
        Dim trans As New ServerTransactionDB
        Try
            Dim lnqvending As New MsVendingMachineServerLinqDB
            lnqvending.GetDataByPK(Edit_Vending_ID, trans.Trans)
            With lnqvending
                .MS_LOCATION_ID = ddlLocation.SelectedValue
                .COM_NAME = txtComName.Text.Trim.Replace("'", "''")
                .IP_ADDRESS = ip_address
                .MAC_ADDRESS = mac_address
                .TODAY_AVAILABLE = 0
                .ACTIVE_STATUS = IIf(chkActive.Checked, "Y", "N")
            End With
            Dim ret As ExecuteDataInfo
            If lnqvending.ID > 0 Then
                ret = lnqvending.UpdateData(UserName, trans.Trans)
            Else
                ret = lnqvending.InsertData(UserName, trans.Trans)
            End If

            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
                Exit Sub
            End If

            If ret.IsSuccess = True AndAlso MatStock.Rows.Count > 0 Then
                For i As Integer = 0 To MatStock.Rows.Count - 1
                    With MatStock
                        Dim ms_device_id As Integer = CInt(.Rows(i)("ms_device_id"))
                        Dim device_name_en As String = .Rows(i)("device_name_en").ToString
                        Dim max_qty As Integer = CInt(.Rows(i)("max_qty"))
                        Dim warning_qty As Integer = CInt(.Rows(i)("warning_qty"))
                        Dim critical_qty As Integer = CInt(.Rows(i)("critical_qty"))
                        Dim movement_direction As Integer = CInt(.Rows(i)("movement_direction"))
                        Dim lnqvdmdevice = New MsVendingDeviceServerLinqDB
                        lnqvdmdevice.ChkDataByMS_DEVICE_ID_MS_VENDING_ID(ms_device_id, lnqvending.ID, trans.Trans)

                        With lnqvdmdevice
                            .MS_VENDING_ID = lnqvending.ID
                            .MS_DEVICE_ID = ms_device_id
                            .MAX_QTY = max_qty
                            .WARNING_QTY = warning_qty
                            .CRITICAL_QTY = critical_qty
                            .SYNC_TO_VENDING = "N"
                            .SYNC_TO_SERVER = "Y"
                            .MS_DEVICE_STATUS_ID = "2"
                        End With

                        Dim d_ret As New ExecuteDataInfo
                        If lnqvdmdevice.ID > 0 Then
                            d_ret = lnqvdmdevice.UpdateData(UserName, trans.Trans)
                        Else
                            d_ret = lnqvdmdevice.InsertData(UserName, trans.Trans)
                        End If
                        If d_ret.IsSuccess = False Then
                            trans.RollbackTransaction()
                            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & d_ret.ErrorMessage & "');", True)
                            Exit Sub
                        End If

                        lnqvdmdevice = Nothing
                    End With 'MatStock
                Next
            End If 'MatStock.Rows.Count > 0 



            '## Vending Product
            Dim p_p(1) As SqlParameter
            p_p(0) = ServerDB.SetText("@_VENDING_ID", lnqvending.ID)
            ret = ServerDB.ExecuteNonQuery("DELETE FROM  MS_VENDING_PRODUCT WHERE MS_VENDING_ID =@_VENDING_ID", p_p)
            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
                Exit Sub
            End If
            For i As Integer = 0 To rptProduct.Items.Count - 1
                Dim chk As CheckBox = rptProduct.Items(i).FindControl("chk")
                If chk.Checked = True Then
                    Dim lblID As Label = rptProduct.Items(i).FindControl("lblID")
                    Dim txtProfitSharing As TextBox = rptProduct.Items(i).FindControl("txtProfitSharing")

                    Dim lnqvp As New MsVendingProductServerLinqDB
                    With lnqvp
                        .MS_VENDING_ID = lnqvending.ID
                        .MS_PRODUCT_ID = lblID.Text
                        .PROFIT_SHARING = txtProfitSharing.Text
                    End With
                    Dim d_ret As New ExecuteDataInfo
                    d_ret = lnqvp.InsertData(UserName, trans.Trans)

                    If d_ret.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & d_ret.ErrorMessage & "');", True)
                        Exit Sub
                    End If
                    lnqvp = Nothing

                End If
            Next

            '## Vending Ads
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetText("@_VENDING_ID", lnqvending.ID)
            ret = ServerDB.ExecuteNonQuery("DELETE FROM MS_VENDING_ADVERTISE WHERE MS_VENDING_ID=@_VENDING_ID", p)
            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
                Exit Sub
            End If
            For i As Integer = 0 To rptAds.Items.Count - 1
                Dim chkItemAds As CheckBox = rptAds.Items(i).FindControl("chkItemAds")
                If chkItemAds.Checked = True Then
                    Dim lblAdsID As Label = rptAds.Items(i).FindControl("lblAdsID")
                    Dim lblAdsName As Label = rptAds.Items(i).FindControl("lblAdsName")

                    Dim lnqvendingads = New MsVendingAdvertiseServerLinqDB
                    With lnqvendingads
                        .MS_ADVERTISE_ID = lblAdsID.Text
                        .MS_VENDING_ID = lnqvending.ID
                    End With

                    Dim d_ret As New ExecuteDataInfo
                    d_ret = lnqvendingads.InsertData(UserName, trans.Trans)

                    If d_ret.IsSuccess = False Then
                        trans.RollbackTransaction()
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & d_ret.ErrorMessage & "');", True)
                        Exit Sub
                    End If

                    lnqvendingads = Nothing

                End If
            Next '## Vending Ads

            lnqvending = Nothing
            trans.CommitTransaction()
            BindList()
        Catch ex As Exception
            trans.RollbackTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ex.ToString() & "');", True)
        End Try
#End Region

    End Sub

#Region "Select Product"

    Sub BindProduct(vending_id As String)
        Dim dt As New DataTable
        dt = BL.VendingGetProductInfo(vending_id)
        rptProduct.DataSource = dt
        rptProduct.DataBind()

    End Sub

    Private Sub rptProduct_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptProduct.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim chk As CheckBox = e.Item.FindControl("chk")
        Dim lblID As Label = e.Item.FindControl("lblID")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblPrice As Label = e.Item.FindControl("lblPrice")
        Dim txtProfitSharing As TextBox = e.Item.FindControl("txtProfitSharing")

        lblID.Text = e.Item.DataItem("id").ToString
        lblName.Text = e.Item.DataItem("product_name_th").ToString()
        lblPrice.Text = "ราคา  " & e.Item.DataItem("price").ToString & " บาท"
        txtProfitSharing.Text = e.Item.DataItem("profit_sharing").ToString()
        BL.SetTextIntKeypress(txtProfitSharing)
        txtProfitSharing.MaxLength = 3

        chk.Checked = False
        If Not IsDBNull(e.Item.DataItem("Selected")) AndAlso e.Item.DataItem("Selected") = "T" Then
            chk.Checked = True
        End If

        Dim img As HtmlImage = e.Item.FindControl("img")
        img.Attributes.Add("onclick", "return clickButton(event,'" + btnShow.ClientID + "') ")

        If Not IsDBNull(e.Item.DataItem("product_image_big")) Then
            Dim CustImg As String = "data:image/product_image_big;base64," & Convert.ToBase64String(e.Item.DataItem("product_image_big"))
            img.Attributes.Add("src", CustImg)
        End If


    End Sub

    'Private Sub rptProduct_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptProduct.ItemCommand

    '    'If e.CommandName = "ShowDialog" Then
    '    '    'lblProductName.Text = lblName.Text

    '    '    Dim mdlProduct As AjaxControlToolkit.ModalPopupExtender = e.Item.FindControl("mdlProduct")
    '    '    Dim lblProductName As Label = e.Item.FindControl("lblProductName")

    '    '    Dim lblName As Label = e.Item.FindControl("lblName")

    '    '    lblProductName.Text = lblName.Text
    '    '    mdlProduct.Show()
    '    'End If



    'End Sub

#End Region

#Region "Advertising"
    Sub BindAds(vending_id As String)
        Dim dt As New DataTable
        dt = BL.GetVendingAds(vending_id)
        rptAds.DataSource = dt
        rptAds.DataBind()

    End Sub

    Private Sub rptAds_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptAds.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim chkItemAds As CheckBox = e.Item.FindControl("chkItemAds")
        Dim lblAdsID As Label = e.Item.FindControl("lblAdsID")
        Dim lblAdsName As Label = e.Item.FindControl("lblAdsName")

        chkItemAds.Checked = False
        If e.Item.DataItem("SELECTED").ToString = "T" Then
            chkItemAds.Checked = True
        End If

        lblAdsID.Text = e.Item.DataItem("id").ToString
        lblAdsName.Text = e.Item.DataItem("advertise_name").ToString
    End Sub

    Private Sub btnCheckKioskAll_Click(sender As Object, e As EventArgs) Handles btnCheckAdsAll.Click
        For i As Integer = 0 To rptAds.Items.Count - 1
            Dim chkItemAds As CheckBox = rptAds.Items(i).FindControl("chkItemAds")
            chkItemAds.Checked = Not chkHeadAds.Checked
        Next
        chkHeadAds.Checked = Not chkHeadAds.Checked
    End Sub
#End Region

End Class