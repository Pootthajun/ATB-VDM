﻿Imports System.Data
Imports System.Data.SqlClient
Imports ServerLinqDB.ConnectDB
Imports System.IO

Public Class VDMBL

    Public Enum AuthorizedLevel
        None = 0
        View = 1
        Edit = 2
    End Enum
    Public Function GetFunctionalAuthorized(ByVal UDT As DataTable, ByVal FunctionalID As Integer) As AuthorizedLevel
        Dim Result As AuthorizedLevel = AuthorizedLevel.None
        Try
            UDT.DefaultView.RowFilter = "ms_functional_id=" & FunctionalID
            Result = UDT.DefaultView(0).Item("ms_authorization_id")
            UDT.DefaultView.RowFilter = ""
        Catch : End Try
        Return Result
    End Function

#Region "_Master Location"
    Public Function GetList_Location(location_code As String) As DataTable
        Dim sql As String = " select l.id,location_code,location_name,l.active_status,count(k.id) total_vending"
        sql &= " from ms_location l left join ms_vending_machine k on l.id = k.ms_location_id where 1=1 "
        If location_code <> "" Then
            sql &= " and location_code = @_LOCATION_CODE"
        End If
        sql &= " group by l.id,location_code,location_name,l.active_status"
        sql &= " order by location_code,location_name "

        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_LOCATION_CODE", location_code)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function DeleteMasterLocation(location_code As String) As String
        Dim trans As New ServerTransactionDB
        Try
            Dim sql As String
            sql = "Select 'y' from ms_vending_machine where ms_location_id=(select top 1 id from ms_location where location_code=@_LOCATION_CODE)"
            Dim p_k(1) As SqlParameter
            p_k(0) = ServerDB.SetText("@_LOCATION_CODE", location_code)
            Dim dt As New DataTable
            dt = ServerDB.ExecuteTable(sql, trans.Trans, p_k)
            If dt.Rows.Count > 0 Then
                Return "This Location is using by Vending"
            End If

            sql = "Select 'y' from ms_user_location where ms_location_id=(select top 1 id from ms_location where location_code=@_LOCATION_CODE)"
            Dim p_ul(1) As SqlParameter
            p_ul(0) = ServerDB.SetText("@_LOCATION_CODE", location_code)
            dt = ServerDB.ExecuteTable(sql, trans.Trans, p_ul)
            If dt.Rows.Count > 0 Then
                Return "This Location is using by user location"
            End If

            sql = "delete from ms_location where location_code=@_LOCATION_CODE"
            Dim p_l(1) As SqlParameter
            p_l(0) = ServerDB.SetText("@_LOCATION_CODE", location_code)

            Dim ret As New ExecuteDataInfo
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_l)
            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                Return ret.ErrorMessage
            End If
            trans.CommitTransaction()
            Return ""
        Catch ex As Exception
            trans.RollbackTransaction()
            Return ex.ToString()
        End Try
    End Function


#End Region

#Region "_SettingMailGroup"
    Public Function GetList_MailGroup() As DataTable
        Dim sql As String = "SELECT DISTINCT G.ID,GROUP_CODE,GROUP_NAME,G.ACTIVE_STATUS,"
        sql &= " (SELECT COUNT(ID) FROM MS_REPORT_MAIL_GROUP_REPORTS R WHERE G.ID=R.MS_REPORT_MAIL_GROUP_ID) COUNT_REPORT,"
        sql &= "(SELECT COUNT(ID) FROM MS_REPORT_MAIL_GROUP_DETAIL M WHERE G.ID = M.MS_REPORT_MAIL_GROUP_ID) COUNT_MAIL,"
        sql &= " (SELECT COUNT(ID) FROM MS_REPORT_MAIL_GROUP_LOCATION L WHERE G.ID = L.MS_REPORT_MAIL_GROUP_ID) COUNT_LOCATION"
        sql &= " FROM MS_REPORT_MAIL_GROUP G"
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql)
        Return dt
    End Function

    Public Function Get_Mail_Group_Info(group_id As String) As DataTable
        Dim sql As String = ""
        sql &= " SELECT GROUP_CODE,GROUP_NAME,ACTIVE_STATUS "
        sql &= " FROM MS_REPORT_MAIL_GROUP "
        sql &= " WHERE ID=@_GROUP_ID"

        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_GROUP_ID", group_id)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetList_MailGroup_Report(group_id As String) As DataTable
        Dim sql As String = ""
        sql &= " SELECT RM.ID,REPORT_NAME ,"
        sql &= " ISNULL((SELECT 'T' FROM MS_REPORT_MAIL_GROUP_REPORTS GR WHERE GR.MS_REPORTS_MAIL_ID = RM.ID AND GR.MS_REPORT_MAIL_GROUP_ID=@_GROUP_ID),'F') SELECTED "
        sql &= " FROM MS_REPORTS_MAIL RM ORDER BY REPORT_NAME"

        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_GROUP_ID", group_id)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetList_MailGroup_Location(group_id As String) As DataTable
        Dim sql As String = ""
        sql &= " SELECT ML.ID,LOCATION_NAME,CASE ISNULL(GL.MS_LOCATION_ID,'') WHEN '' THEN 'F' ELSE 'T' END SELECTED"
        sql &= " FROM MS_REPORT_MAIL_GROUP_LOCATION GL RIGHT JOIN MS_LOCATION ML ON GL.MS_LOCATION_ID = ML.ID AND MS_REPORT_MAIL_GROUP_ID=@_GROUP_ID"
        sql &= " ORDER BY LOCATION_NAME "

        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_GROUP_ID", group_id)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetList_MailGroup_Mail(group_id As String) As DataTable
        Dim sql As String = ""
        sql &= " SELECT ID,EMAIL"
        sql &= " FROM MS_REPORT_MAIL_GROUP_DETAIL GD "
        sql &= " WHERE MS_REPORT_MAIL_GROUP_ID=@_GROUP_ID"

        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_GROUP_ID", group_id)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function Drop_MailGroup(ByVal group_id As String) As Boolean
        Dim trans As New ServerTransactionDB
        Dim sql As String
        Try
            Dim ret As ExecuteDataInfo
            sql = "DELETE FROM MS_REPORT_MAIL_GROUP_LOCATION WHERE MS_REPORT_MAIL_GROUP_ID=@_GROUP_ID"
            Dim p_l(1) As SqlParameter
            p_l(0) = ServerDB.SetText("@_GROUP_ID", group_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_l)

            If ret.IsSuccess = False Then
                Return False
            End If

            sql = "DELETE FROM MS_REPORT_MAIL_GROUP_DETAIL WHERE MS_REPORT_MAIL_GROUP_ID=@_GROUP_ID"
            Dim p_d(1) As SqlParameter
            p_d(0) = ServerDB.SetText("@_GROUP_ID", group_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_d)

            If ret.IsSuccess = False Then
                Return False
            End If

            sql = "DELETE FROM MS_REPORT_MAIL_GROUP_REPORTS WHERE MS_REPORT_MAIL_GROUP_ID=@_GROUP_ID"
            Dim p_r(1) As SqlParameter
            p_r(0) = ServerDB.SetText("@_GROUP_ID", group_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_r)

            If ret.IsSuccess = False Then
                Return False
            End If

            sql = "DELETE FROM MS_REPORT_MAIL_GROUP WHERE ID=@_GROUP_ID"
            Dim p_g(1) As SqlParameter
            p_g(0) = ServerDB.SetText("@_GROUP_ID", group_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_g)

            If ret.IsSuccess = False Then
                Return False
            End If

            trans.CommitTransaction()
            Return True
        Catch ex As Exception
            trans.RollbackTransaction()
            Return False
        End Try
    End Function

#End Region

#Region "_BusinessOwner"
    Public Function GetList_Business(business_code As String) As DataTable
        Dim sql As String = " select business_code,business_name,active_status from MS_BUSINESS_OWNER where 1=1 "
        If business_code <> "" Then
            sql &= " and business_code = @_BUSINESS_CODE"
        End If

        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_BUSINESS_CODE", business_code)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function DeleteMasterBusiness(business_code As String) As String

        Dim trans As New ServerTransactionDB
        Try
            Dim sql As String
            sql = "delete from ms_business_owner where business_code=@_BUSINESS_CODE"
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetText("@_BUSINESS_CODE", business_code)

            Dim ret As New ExecuteDataInfo
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p)
            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                Return ret.ErrorMessage
            End If
            trans.CommitTransaction()
            Return ""
        Catch ex As Exception
            trans.RollbackTransaction()
            Return ex.ToString()
        End Try

    End Function
#End Region

#Region "_Setting Alarm"

    Public Function GetList_Alarm_Group() As DataTable
        Dim sql As String = "  Select  DISTINCT AGROUP.ID,ALARM_GROUP_CODE,ALARM_GROUP_NAME, ActiveStatus "
        sql &= " ,(SELECT COUNT(AGMON.MS_MASTER_MONITORING_ALARM_ID) FROM TB_ALARM_GROUP_MONITORING AGMON WHERE AGMON.TB_ALARM_GROUP_ID = AGROUP.ID) COUNT_ALARM"
        sql &= " ,(Select COUNT(AMAIL.ID) FROM TB_ALARM_GROUP_EMAIL AMAIL WHERE AMAIL.TB_ALARM_GROUP_ID = AGROUP.ID) COUNT_EMAIL"
        sql &= " From TB_ALARM_GROUP AGROUP"

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql)
        Return dt
    End Function

    Public Function Get_Alarm_Group_Info(group_id As String) As DataTable
        Dim sql As String = ""
        sql &= " SELECT * "
        sql &= " FROM TB_ALARM_GROUP AGROUP "
        sql &= " WHERE ID=@_GROUP_ID"

        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_GROUP_ID", group_id)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function Drop_Alarm(ByVal group_id As String) As Boolean
        Dim trans As New ServerTransactionDB
        Dim sql As String
        Try
            Dim ret As ExecuteDataInfo
            sql = "DELETE FROM TB_ALARM_GROUP_MONITORING WHERE TB_ALARM_GROUP_ID=@_GROUP_ID"
            Dim p_m(1) As SqlParameter
            p_m(0) = ServerDB.SetText("@_GROUP_ID", group_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_m)

            If ret.IsSuccess = False Then
                Return False
            End If

            sql = "DELETE FROM TB_ALARM_GROUP_COMPUTER WHERE TB_ALARM_GROUP_ID=@_GROUP_ID"
            Dim p_c(1) As SqlParameter
            p_c(0) = ServerDB.SetText("@_GROUP_ID", group_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_c)

            If ret.IsSuccess = False Then
                Return False
            End If

            sql = "DELETE FROM TB_ALARM_GROUP_EMAIL WHERE TB_ALARM_GROUP_ID=@_GROUP_ID"
            Dim p_e(1) As SqlParameter
            p_e(0) = ServerDB.SetText("@_GROUP_ID", group_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_e)

            If ret.IsSuccess = False Then
                Return False
            End If

            sql = "DELETE FROM TB_ALARM_GROUP WHERE ID=@_GROUP_ID"
            Dim p_g(1) As SqlParameter
            p_g(0) = ServerDB.SetText("@_GROUP_ID", group_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_g)

            If ret.IsSuccess = False Then
                Return False
            End If

            trans.CommitTransaction()
            Return True
        Catch ex As Exception
            trans.RollbackTransaction()
            Return False
        End Try
    End Function

    Public Function GetList_Kiosk_Alarm(ByVal group_id As String) As DataTable
        Dim sql As String = "SELECT ID,ALARM_CODE,ALARM_PROBLEM,ENG_DESC,SMS_MESSAGE,"
        sql &= " ISNULL((SELECT 'T' FROM TB_ALARM_GROUP_MONITORING TBMON WHERE TBMON.MS_MASTER_MONITORING_ALARM_ID = MSMON.ID AND TBMON.TB_ALARM_GROUP_ID=@_GROUP_ID) ,'F') SELECTED"
        sql &= " From MS_MASTER_MONITORING_ALARM MSMON"
        sql &= " WHERE MS_MASTER_MONITORING_TYPE_ID BETWEEN 1 AND 12"
        sql &= " ORDER BY ALARM_CODE"
        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_GROUP_ID", group_id)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function Get_Computer_Alarm_ByGroup(group_id As String) As DataTable
        Dim sql As String = "Select ID,TB_ALARM_GROUP_ID,MACADDRESS FROM TB_ALARM_GROUP_COMPUTER WHERE TB_ALARM_GROUP_ID=@_GROUP_ID"
        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_GROUP_ID", group_id)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)

        Return dt

    End Function

    Public Function Get_Email_Alarm_ByGroup(GROUP_ID As String) As DataTable
        Dim sql As String = "Select  ID,TB_ALARM_GROUP_ID,E_MAIL FROM TB_ALARM_GROUP_EMAIL WHERE TB_ALARM_GROUP_ID=@_GROUP_ID"
        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_GROUP_ID", GROUP_ID)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)

        Return dt
    End Function



#End Region

#Region "_SettingCategoryGroup"
    Public Function GetList_CategoryGroup(category_group_code As String) As DataTable
        Dim sql As String = " select id,category_code,category_name,active_status from MS_PRODUCT_CATEGORY where 1=1 "
        If category_group_code <> "" Then
            sql &= " and category_code = @_CATEGORY_GROUP_CODE"
        End If

        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_CATEGORY_GROUP_CODE", category_group_code)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function DeleteMasterCategory(category_group_code As String) As String

        Dim trans As New ServerTransactionDB
        Try
            Dim sql As String
            sql = "delete from MS_PRODUCT_CATEGORY where category_code=@_CATEGORY_GROUP_CODE"
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetText("@_CATEGORY_GROUP_CODE", category_group_code)

            Dim ret As New ExecuteDataInfo
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p)
            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                Return ret.ErrorMessage
            End If
            trans.CommitTransaction()
            Return ""
        Catch ex As Exception
            trans.RollbackTransaction()
            Return ex.ToString()
        End Try

    End Function
#End Region

#Region "_SettingUser&Role"
    Public Function GetList_User_Role(username As String, active_status As String) As DataTable
        Dim sql As String = "select r.id,role_name,active_status,ms_role_id,case isnull(ms_role_id,'') when '' then 'N' else 'Y' end Selected "
        sql &= " From ms_role r left join ms_user_role ur on r.id = ur.ms_role_id and ur.username= @_user_name"
        sql &= " where active_status = @_active_status"
        Dim p(2) As SqlParameter
        p(0) = ServerDB.SetText("@_user_name", username)
        p(1) = ServerDB.SetText("@_active_status", active_status)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetList_User_Location(username As String, active_status As String) As DataTable
        Dim sql As String = "select l.id,location_code,location_name,active_status,ms_location_id,case isnull(ms_location_id,'') when '' then 'N' else 'Y' end Selected "
        sql &= " From ms_location l left join ms_user_location ml on l.id=ml.ms_location_id and ml.username=@_user_name"
        sql &= " where active_status =@_active_status  "
        Dim p(2) As SqlParameter
        p(0) = ServerDB.SetText("@_user_name", username)
        p(1) = ServerDB.SetText("@_active_status", active_status)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetList_User_Functional(AuthorizeID As Long, FunctionalZoneID As Long, FunctionalID As Long, Username As String) As DataTable
        Dim ret As DataTable
        Try
            Dim p(4) As SqlParameter
            Dim Filter As String = " f.active_status='Y' and ap.id=1 "   'ap.id=1  คือ Web Admin
            If AuthorizeID <> 0 Then
                Filter &= " and rf.ms_authorization_id=@_AUTHORIZE_ID "
                p(0) = ServerDB.SetBigInt("@_AUTHORIZE_ID", AuthorizeID)
            End If
            If FunctionalZoneID <> 0 Then
                Filter &= " and f.ms_functional_zone_id=@_FUNCTIONAL_ZONE_ID "
                p(1) = ServerDB.SetBigInt("@_FUNCTIONAL_ZONE_ID", FunctionalZoneID)
            End If
            If FunctionalID <> 0 Then
                Filter &= " and f.id=@_FUNCTIONAL_ID "
                p(2) = ServerDB.SetBigInt("@_FUNCTIONAL_ID", FunctionalID)
            End If
            If Username <> "" Then
                Filter &= " and ur.username=@_USERNAME "
                p(3) = ServerDB.SetText("@_USERNAME", Username)
            End If

            Dim sql As String = "select ur.username, rf.ms_authorization_id, au.authorization_name, ur.ms_role_id, " & Environment.NewLine
            sql += " fz.ms_application_id, ap.app_name, rf.ms_functional_id, f.functional_name " & Environment.NewLine
            sql += " from MS_USER_ROLE ur " & Environment.NewLine
            sql += " inner join MS_ROLE_FUNCTIONAL rf On ur.ms_role_id=rf.ms_role_id " & Environment.NewLine
            sql += " inner join MS_FUNCTIONAL f on f.id=rf.ms_functional_id " & Environment.NewLine
            sql += " inner join MS_FUNCTIONAL_ZONE fz On fz.id=f.ms_functional_zone_id " & Environment.NewLine
            sql += " inner join MS_APPLICATION ap on ap.id=fz.ms_application_id " & Environment.NewLine
            sql += " inner join MS_AUTHORIZATION au On au.id=rf.ms_authorization_id " & Environment.NewLine
            sql += " where " & Filter
            sql += " order by ur.username, rf.ms_functional_id" & Environment.NewLine
            ret = ServerDB.ExecuteTable(sql, p)
        Catch ex As Exception
            ret = New DataTable
        End Try
        Return ret
    End Function

    Public Function Drop_User(ByVal userid As Long, username As String) As Boolean
        Dim trans As New ServerTransactionDB
        Dim sql As String
        Try


            Dim ret As ExecuteDataInfo
            sql = "DELETE FROM MS_USER_ROLE WHERE USERNAME=@_USERNAME"
            Dim p_m(1) As SqlParameter
            p_m(0) = ServerDB.SetText("@_USERNAME", username)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_m)

            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                Return False
            End If

            sql = "DELETE FROM MS_USER_LOCATION WHERE USERNAME=@_USERNAME"
            Dim p_c(1) As SqlParameter
            p_c(0) = ServerDB.SetText("@_USERNAME", username)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_c)

            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                Return False
            End If

            '// delete user from webservice
            Dim ret_w As TITWebService.ExecuteDataInfo
            Dim ws As New TITWebService.TITWebService
            ret_w = ws.DeleteUserAccount(userid)

            If ret_w.IsSuccess = False Then
                trans.RollbackTransaction()
                Return False
            End If

            trans.CommitTransaction()
            Return True
        Catch ex As Exception
            trans.RollbackTransaction()
            Return False
        End Try
    End Function
#End Region

#Region "_SettingRole"
    Public Enum Application_Type
        ADMIN_WEB = 1
        KIOSK_STAFF_CONSOLE = 2
    End Enum
    Public Function GetList_Role(role_id As Integer) As DataTable
        Dim sql As String = "Select R.ID Role_ID,R.Role_Name," & Environment.NewLine
        sql &= " SUM(Case When App.ID=1 And RF.ID>0 Then 1 Else 0 End) Admin_Web," & Environment.NewLine
        sql &= " SUM(Case When App.ID=2 And RF.ID>0 Then 1 Else 0 End) Staff_Console," & Environment.NewLine
        sql &= " ISNULL(U.Users,0) Users,R.active_status" & Environment.NewLine
        sql &= " FROM ms_Role R" & Environment.NewLine
        sql &= " LEFT JOIN ms_Role_Functional RF On R.ID=RF.MS_ROLE_ID" & Environment.NewLine
        sql &= " And RF.ID > 0" & Environment.NewLine
        sql &= " Left JOIN ms_Functional FN On RF.MS_FUNCTIONAL_ID=FN.ID" & Environment.NewLine
        sql &= " LEFT JOIN ms_Functional_Zone FZ On FN.MS_FUNCTIONAL_ZONE_ID=FZ.ID" & Environment.NewLine
        sql &= " LEFT JOIN ms_Application App On FZ.MS_APPLICATION_ID=App.ID" & Environment.NewLine
        sql &= " LEFT JOIN" & Environment.NewLine
        sql &= " (" & Environment.NewLine
        sql &= " Select UR.MS_ROLE_ID,COUNT(USERNAME) Users" & Environment.NewLine
        sql &= " FROM ms_User_Role UR" & Environment.NewLine
        sql &= " GROUP BY UR.MS_ROLE_ID) U On R.ID=U.MS_ROLE_ID" & Environment.NewLine
        sql &= " WHERE FN.active_status='Y' "
        If role_id > 0 Then
            sql &= " and R.ID=@_ROLE_ID" & Environment.NewLine
        End If
        sql &= " GROUP BY R.ID,R.Role_Name,U.Users,R.Active_Status" & Environment.NewLine
        sql &= " ORDER BY R.ID" & Environment.NewLine

        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetInt("@_ROLE_ID", role_id)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetList_Functional_Zone(application_id As Integer, function_zone_id As Integer) As DataTable
        Dim sql As String = "SELECT AP.ID App_ID,AP.App_Name," & Environment.NewLine
        sql &= " FZ.ID FN_Zone_ID,FZ.FN_Zone_Name" & Environment.NewLine
        sql &= " From MS_Application AP" & Environment.NewLine
        sql &= " INNER JOIN MS_Functional_Zone FZ On AP.ID=FZ.MS_APPLICATION_ID WHERE 1=1 " & Environment.NewLine
        If application_id > 0 Then
            sql &= " and AP.ID=@_application_id "
        End If
        If function_zone_id > 0 Then
            sql &= " And FZ.ID =@_function_id" & Environment.NewLine
        End If

        sql &= " ORDER BY AP.App_Order,FZ.FN_Zone_Order" & Environment.NewLine
        Dim p(2) As SqlParameter
        p(0) = ServerDB.SetInt("@_application_id", application_id)
        p(1) = ServerDB.SetInt("@_function_id", function_zone_id)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetList_Functional(ByVal application_id As Integer, ByVal role_id As Integer) As DataTable
        Dim FN As DataTable = GetList_Functional(application_id, 0, 0)
        FN.Columns.Add("Auth_ID", GetType(Integer))
        Dim Auth As DataTable
        If role_id = 0 Then
            Auth = GetList_Role_Functional(application_id, 0, 0, -1)
        Else
            Auth = GetList_Role_Functional(application_id, 0, 0, role_id)
        End If
        For i As Integer = 0 To FN.Rows.Count - 1
            Auth.DefaultView.RowFilter = "FN_ID=" & FN.Rows(i).Item("FN_ID")
            If Auth.DefaultView.Count = 0 OrElse IsDBNull(Auth.DefaultView(0).Item("Auth_ID")) Then
                FN.Rows(i).Item("Auth_ID") = 0
            Else
                FN.Rows(i).Item("Auth_ID") = Auth.DefaultView(0).Item("Auth_ID")
            End If
        Next
        Return FN
    End Function

    Public Function GetList_Functional(ByVal application_id As Integer, ByVal functional_zone_id As Integer, ByVal functional_id As Integer) As DataTable
        Dim sql As String = " SELECT AP.ID App_ID,AP.App_Name," & Environment.NewLine
        sql &= " FZ.ID FN_Zone_ID,FZ.FN_Zone_Name," & Environment.NewLine
        sql &= " FN.ID FN_ID,FN.FUNCTIONAL_NAME,Can_Edit" & Environment.NewLine
        sql &= " FROM MS_Application AP" & Environment.NewLine
        sql &= " INNER JOIN MS_Functional_Zone FZ On AP.ID=FZ.MS_APPLICATION_ID" & Environment.NewLine
        sql &= " INNER JOIN MS_Functional FN ON FZ.ID=FN.MS_FUNCTIONAL_ZONE_ID WHERE FN.active_status='Y' " & Environment.NewLine
        Dim p(3) As SqlParameter
        If application_id > 0 Then
            sql &= " And AP.ID =@_application_id" & Environment.NewLine
            p(0) = ServerDB.SetInt("@_application_id", application_id)
        End If
        If functional_zone_id > 0 Then
            sql &= " and FZ.ID =@_functional_zone_id" & Environment.NewLine
            p(1) = ServerDB.SetInt("@_functional_zone_id", functional_zone_id)
        End If
        If functional_id > 0 Then
            sql &= " And FN.ID=@_functional_id" & Environment.NewLine
            p(2) = ServerDB.SetInt("@_functional_id", functional_id)
        End If
        sql &= " ORDER BY AP.App_Order,FZ.FN_Zone_Order,FN.FUNCTIONAL_Order"


        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt

    End Function


    Public Function GetList_Role_Functional(ByVal application_id As String, ByVal functional_zone_id As String, ByVal functional_id As Integer, ByVal role_id As Integer) As DataTable

        Dim sql As String = "Select  AP.ID App_ID,AP.App_Name," & Environment.NewLine
        sql &= " FZ.ID FN_Zone_ID, FZ.FN_Zone_Name," & Environment.NewLine
        sql &= " FN.ID FN_ID, FN.Functional_Name," & Environment.NewLine
        sql &= " R.ID Role_ID, R.Role_Name, A.ID Auth_ID, A.Authorization_Name" & Environment.NewLine
        sql &= " From ms_Role R" & Environment.NewLine
        sql &= " INNER Join ms_Role_Functional RF ON R.id=RF.ms_role_id" & Environment.NewLine
        sql &= " INNER Join ms_Functional FN On FN.id=RF.ms_functional_id" & Environment.NewLine
        sql &= " INNER Join ms_Functional_Zone FZ ON FZ.id=FN.ms_functional_zone_id" & Environment.NewLine
        sql &= " INNER Join ms_Application AP On AP.id=FZ.ms_application_id" & Environment.NewLine
        sql &= " Left Join MS_AUTHORIZATION A ON A.id=CASE WHEN Can_Edit='N'" & Environment.NewLine
        sql &= " And RF.ms_authorization_id = 2 Then 1 Else ISNULL(RF.ms_authorization_id,0) End" & Environment.NewLine
        sql &= " WHERE fn.active_status='Y'" & Environment.NewLine
        Dim p(4) As SqlParameter
        If application_id <> 0 Then
            sql &= " And AP.ID =@_application_id" & Environment.NewLine
            p(0) = ServerDB.SetText("@_application_id", application_id)
        End If
        If functional_zone_id <> 0 Then
            sql &= " And FZ.ID=@_functional_zone_id" & Environment.NewLine
            p(1) = ServerDB.SetInt("@_functional_zone_id", functional_zone_id)
        End If
        If functional_id <> 0 Then
            sql &= " And FN.ID=@_functional_id" & Environment.NewLine
            p(2) = ServerDB.SetInt("@_functional_id", functional_id)
        End If
        If role_id <> 0 Then
            sql &= " And R.ID=@_role_id" & Environment.NewLine
            p(3) = ServerDB.SetInt("@_role_id", role_id)
        End If
        sql &= " ORDER BY R.ID, AP.App_Order, FZ.FN_Zone_Order, FN.functional_order"


        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetList_User_Role(ByVal username As String, ByVal role_id As Integer) As DataTable
        Dim sql As String = "Select username,r.id role_id,r.role_name" & Environment.NewLine
        sql &= " From ms_user_role ur " & Environment.NewLine
        sql &= " inner Join ms_role r On r.id=ur.ms_role_id" & Environment.NewLine
        sql &= " where 1=1" & Environment.NewLine
        If username <> "" Then
            sql &= " And ur.username=@_username  " & Environment.NewLine
        End If
        If role_id <> 0 Then
            sql &= " And r.id=@_role_id" & Environment.NewLine
        End If

        sql &= " ORDER BY username, r.id"
        Dim p(2) As SqlParameter
        p(0) = ServerDB.SetText("@_username", username)
        p(1) = ServerDB.SetInt("@_role_id", role_id)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function Drop_Role(ByVal role_id As Integer) As Boolean
        Dim trans As New ServerTransactionDB
        Dim sql As String
        Try
            Dim ret As ExecuteDataInfo
            sql = "DELETE FROM MS_ROLE_FUNCTIONAL WHERE MS_ROLE_ID=@_ROLEID"
            Dim p_f(1) As SqlParameter
            p_f(0) = ServerDB.SetText("@_ROLEID", role_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_f)

            If ret.IsSuccess = False Then
                Return False
            End If

            sql = "DELETE FROM MS_USER_ROLE WHERE  MS_ROLE_ID=@_ROLEID"
            Dim p_u(1) As SqlParameter
            p_u(0) = ServerDB.SetText("@_ROLEID", role_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_u)

            If ret.IsSuccess = False Then
                Return False
            End If

            sql = "DELETE FROM MS_ROLE WHERE ID=@_ROLEID"
            Dim p_r(1) As SqlParameter
            p_r(0) = ServerDB.SetText("@_ROLEID", role_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_r)
            If ret.IsSuccess = False Then
                Return False
            End If

            trans.CommitTransaction()
            Return True
        Catch ex As Exception
            trans.RollbackTransaction()
            Return False
        End Try
    End Function

#End Region

#Region "_SettingVendingMachine"
    Public Function GetList_Vending_Device_Existing(ByVal Vending_ID As Integer, ByVal IncludeInActive As String) As DataTable

        Dim sql As String = "select k.id ms_vending_id, d.id ms_device_id, d.device_name_en, d.ms_device_type_id, dt.device_type_name_en, "
        sql &= " d.unit_value, "
        sql &= " isnull(kd.max_qty, d.max_qty) max_qty, "
        sql &= " isnull(kd.warning_qty, d.warning_qty) warning_qty, "
        sql &= " isnull(kd.critical_qty, d.critical_qty) critical_qty,kd.current_qty,dt.movement_direction, "
        sql &= " ds.id ms_device_status_id, ds.status_name, ds.is_problem, cast(null As datetime) start_time, cast(null As datetime) end_time, "
        sql &= " d.device_order "
        sql &= " From ms_vending_machine k "
        sql &= " inner Join ms_vending_device kd On k.id=kd.ms_vending_id "
        sql &= " inner Join ms_device d On d.id=kd.ms_device_id "
        sql &= " inner Join ms_device_type dt On d.ms_device_type_id= dt.id "
        sql &= " inner Join ms_device_status ds On kd.ms_device_status_id=ds.id "
        sql &= " where 1=1"

        Dim p(2) As SqlParameter
        If Vending_ID <> 0 Then
            sql &= " and k.id =@_Vending_ID"
            p(1) = ServerDB.SetInt("@_Vending_ID", Vending_ID)
        End If
        If IncludeInActive <> "" Then
            sql &= " and d.active_status = @_IncludedInActive"
            p(0) = ServerDB.SetText("@_IncludedInActive", IncludeInActive)
        End If
        sql &= " order by d.device_order"

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt

    End Function

    Public Function GetList_DeviceStatus(ByVal ms_device_id As Integer, ByVal ms_device_type_id As Integer) As DataTable
        Dim sql As String = "select id ms_device_status_id,is_problem from ms_device_status where 1=1 "
        If ms_device_id <> 0 Then
            sql &= " and id =@_MS_DEVICE_ID"
        End If
        If ms_device_type_id Then
            sql &= " and ms_device_type_id =@_MS_DEVICE_TYPE_ID"
        End If

        Dim p(2) As SqlParameter
        p(0) = ServerDB.SetInt("@_MS_DEVICE_ID", ms_device_id)
        p(1) = ServerDB.SetInt("@_MS_DEVICE_TYPE_ID", ms_device_type_id)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetList_Vending(user_name As String, ByVal vending_id As Integer) As DataTable
        Dim sql As String = ""

        sql &= " select id ms_vending_id,ms_location_id,com_name,ip_address,mac_address"
        sql &= " ,active_status,updated_by,updated_date"
        sql &= " from ms_vending_machine "
        sql &= " where 1=1"
        If vending_id <> 0 Then
            sql &= " And id =@_VENDING_ID"
        End If
        If user_name <> "" Then
            sql &= " and ms_location_id IN (select ms_location_id from MS_USER_LOCATION where username=@_USER_NAME)"
        End If

        sql &= " order by id "

        Dim p(2) As SqlParameter
        p(0) = ServerDB.SetText("@_USER_NAME", user_name)
        p(1) = ServerDB.SetInt("@_VENDING_ID", vending_id)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function DeleteMasterVending(master_vending_id As String) As Boolean
        Dim sql As String = "delete from ms_vending_device where ms_vending_id=@_VENDING_ID"
        Dim pd(1) As SqlParameter
        pd(0) = ServerDB.SetText("@_VENDING_ID", master_vending_id)

        Dim trans As New ServerTransactionDB
        Dim ret As New ExecuteDataInfo
        ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, pd)
        If ret.IsSuccess = False Then
            trans.RollbackTransaction()
            Return False
        End If

        sql = "delete from ms_vending_advertise where ms_vending_id=@_VENDING_ID"
        Dim pa(1) As SqlParameter
        pa(0) = ServerDB.SetText("@_VENDING_ID", master_vending_id)
        ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, pa)
        If ret.IsSuccess = False Then
            trans.RollbackTransaction()
            Return False
        End If

        sql = "delete from ms_vending_machine where id=@_VENDING_ID"
        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_VENDING_ID", master_vending_id)
        ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p)
        If ret.IsSuccess = False Then
            trans.RollbackTransaction()
            Return False
        End If

        trans.CommitTransaction()
        Return True
    End Function

    Public Function GetList_Vending_Device(ByVal Vending_ID As Integer, ByVal IncludeInActive As String) As DataTable
        If Vending_ID = 0 Then
            Return GetList_Vending_Device_Default()
        Else
            Return GetList_Vending_Device_Existing(Vending_ID, IncludeInActive)
        End If
    End Function

    Public Function GetList_Vending_Device_Default() As DataTable
        Dim sql As String = ""
        sql &= " Select cast(null As int) ms_vending_id,d.id ms_device_id,D.device_name_en,dt.id ms_device_type_id,dt.device_type_name_en,"
        sql &= " d.unit_value,d.max_qty,d.warning_qty,d.critical_qty,cast(null As int) current_qty,dt.Movement_Direction,"
        sql &= " ds.id ms_device_status_id,ds.status_name,ds.is_problem,"
        sql &= " cast(null As datetime) start_time,cast(null As datetime) end_time,d.device_order"
        sql &= " from ms_device_type dt"
        sql &= " inner join ms_device d On  d.ms_device_type_id=dt.id"
        sql &= " left join ms_device_status ds On ds.id=2"
        sql &= " where d.active_status='Y'"
        sql &= " And d.ms_device_type_id In (1,2,3,4,5)"
        sql &= " order by d.device_order"

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql)
        Return dt
    End Function

    Public Function GetList_Device(ByVal Device_ID As Integer, ByVal IncludedInActive As String) As DataTable
        Dim sql As String = " Select d.id ms_device_id, d.device_name_en, dt.id ms_device_type_id, dt.device_type_name_en,"
        sql &= " d.unit_value, d.max_qty, d.warning_qty, d.critical_qty, DT.movement_direction,"
        sql &= " d.icon_white, d.icon_green, d.icon_red, d.device_order"
        sql &= " From ms_device d"
        sql &= " inner Join ms_device_type dt On d.ms_device_type_id=dt.id"
        If IncludedInActive <> "" Then
            sql &= " And d.active_status = @_IncludedInActive"
        End If
        If Device_ID <> 0 Then
            sql &= " where d.id = @_Device_ID"
        End If
        sql &= " order by d.device_order"

        Dim p(2) As SqlParameter
        p(0) = ServerDB.SetText("@_IncludedInActive", IncludedInActive)
        p(1) = ServerDB.SetInt("@_Device_ID", Device_ID)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)

        Return dt
    End Function

    Public Function IsDupplicate_Vending(ip_address As String, mac_address As String, edit_vending_id As String) As Boolean
        Dim sql As String = ""
        sql &= " Select 'y' from ms_vending where (ip_address=@_IP_Address or mac_address=@_Mac_Address) and id <> @_VENDING_ID"

        Dim p(3) As SqlParameter
        p(0) = ServerDB.SetText("@_IP_Address", ip_address)
        p(1) = ServerDB.SetText("@_Mac_Address", mac_address)
        p(2) = ServerDB.SetText("@_VENDING_ID", edit_vending_id)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        If dt.Rows.Count > 0 Then
            Return True
        End If
        Return False
    End Function

    Public Function VendingGetProductInfo(vending_id As String) As DataTable
        Dim sql As String = "select p.id, product_code,product_name_th,product_image_big,price,isnull(profit_sharing,'0') profit_sharing"
        sql &= ",case isnull(vp.id,'') when '' then 'F' else 'T' end Selected"
        sql &= " From ms_product p left join ms_vending_product vp on p.id=vp.ms_product_id and ms_vending_id = @_VENDING_ID"
        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_VENDING_ID", vending_id)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetVendingAds(vending_id As String) As DataTable
        Dim sql As String = " Select id,advertise_name,"
        sql &= " ISNULL((SELECT 'T' FROM ms_vending_advertise va WHERE va.ms_advertise_id = a.ID AND va.ms_vending_id=@_VENDING_ID),'F') Selected from MS_ADVERTISE a"
        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_VENDING_ID", vending_id)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

#End Region

#Region "_Setting Product"

    Public Function GetProductList(product_id As String, IsWarranty As Boolean) As DataTable
        Dim sql As String = "select p.id,category_code,category_name,category_code + '-' + category_name as category,product_code,product_name_th,product_name_en,"
        sql &= " product_name_jp,product_name_ch,product_desc_th,product_desc_en,product_desc_jp,product_desc_ch,product_image_big,"
        sql &= " product_image_small,cost,price,p.active_status,ms_product_category_id "
        If IsWarranty = True Then
            sql += " , isnull(w.warranty_th,'') warranty_th, isnull(w.warranty_en,'') warranty_en, isnull(w.warranty_jp,'') warranty_jp, isnull(w.warranty_ch,'') warranty_ch"
        End If
        sql += " from ms_product p "
        sql += " inner join ms_product_category c on p.ms_product_category_id=c.id "
        If IsWarranty = True Then
            sql += " left join MS_PRODUCT_WARRANTY w on w.ms_product_id=p.id"
        End If
        sql += " where 1=1 "
        Dim p(1) As SqlParameter
        If product_id <> "" Then
            sql &= " And p.id = @_PRODUCT_ID"
            p(0) = ServerDB.SetText("@_PRODUCT_ID", product_id)
        End If
        sql &= " order by category_name,product_code,product_name_th"
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetProductList(product_id As String) As DataTable
        Return GetProductList(product_id, False)
    End Function


    Public Function DeleteMasterProduct(product_id As String) As String
        Dim trans As New ServerTransactionDB
        Try
            Dim sql As String
            sql = "Select 'y' from ms_vending_product where ms_product_id=@_PRODUCT_ID"
            Dim p_v(1) As SqlParameter
            p_v(0) = ServerDB.SetText("@_PRODUCT_ID", product_id)
            Dim dt As New DataTable
            dt = ServerDB.ExecuteTable(sql, trans.Trans, p_v)
            If dt.Rows.Count > 0 Then
                Return "This Product is using by Vending"
            End If

            sql = "delete from MS_PRODUCT_WARRANTY where ms_product_id=@_PRODUCT_ID"
            Dim p_w(1) As SqlParameter
            p_w(0) = ServerDB.SetText("@_PRODUCT_ID", product_id)

            Dim ret As New ExecuteDataInfo
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p_w)
            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                Return ret.ErrorMessage
            End If

            sql = "delete from ms_product where id=@_PRODUCT_ID"
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetText("@_PRODUCT_ID", product_id)
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p)
            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                Return ret.ErrorMessage
            End If
            trans.CommitTransaction()
            Return ""
        Catch ex As Exception
            trans.RollbackTransaction()
            Return ex.ToString()
        End Try
    End Function

#End Region

#Region "_Setting Advertise"
    Public Function GetAdvertiseList(ads_id As String) As DataTable
        Dim sql As String = "select id,advertise_code,advertise_name,ads_file_bype,ads_file_ext,ads_mime_type,"
        sql &= " Convert(varchar(10),start_date,103) start_date,convert(varchar(10),end_date,103) end_date,active_status from  ms_advertise where 1=1 "
        Dim p(1) As SqlParameter
        If ads_id <> "" Then
            sql &= " and id = @_ADS_ID"
            p(0) = ServerDB.SetText("@_ADS_ID", ads_id)
        End If
        sql &= " order by advertise_code,advertise_name"
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function DeleteAdvertise(ads_id As String) As String
        Dim trans As New ServerTransactionDB
        Try
            Dim sql As String
            sql = "Select 'y' from ms_vending_advertise where ms_advertise_id=@_ADS_ID"
            Dim p_v(1) As SqlParameter
            p_v(0) = ServerDB.SetText("@_ADS_ID", ads_id)
            Dim dt As New DataTable
            dt = ServerDB.ExecuteTable(sql, trans.Trans, p_v)
            If dt.Rows.Count > 0 Then
                Return "This Advertise is using by Vending"
            End If

            sql = "delete from ms_advertise where id=@_ADS_ID"
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetText("@_ADS_ID", ads_id)
            Dim ret As New ExecuteDataInfo
            ret = ServerDB.ExecuteNonQuery(sql, trans.Trans, p)
            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                Return ret.ErrorMessage
            End If
            trans.CommitTransaction()
            Return ""
        Catch ex As Exception
            trans.RollbackTransaction()
            Return ex.ToString()
        End Try
    End Function


#End Region

#Region "_Vending Monitoring"
    Public Function Alarm_Overview(ByVal vending_id As Integer, username As String) As DataTable
        Dim sql As String = ""
        sql &= " Select k.id, k.com_name, l.location_name, k.ip_address, k.mac_address, " & vbNewLine
        sql &= " k.online_status available_status, k.today_available, k.hw_isproblem, " & vbNewLine
        sql &= " k.cpu_usage, k.ram_usage, k.disk_usage, " & vbNewLine
        sql &= "    isnull((Select top 1 is_problem  " & vbNewLine
        sql &= "    From v_vending_device_info " & vbNewLine
        sql &= "    Where ms_vending_id = k.id " & vbNewLine
        sql &= "    And is_problem ='Y' and device_active_status='Y'),'N') peripheral_condition, " & vbNewLine
        sql &= "    isnull((Select top 1 Case stock_condition When 'CRITICAL' then 'Y' else 'N' end " & vbNewLine
        sql &= "    From v_vending_device_info " & vbNewLine
        sql &= "    Where ms_vending_id = k.id " & vbNewLine
        sql &= "    And stock_condition='CRITICAL' and device_active_status='Y' ),'N') stock_level " & vbNewLine
        sql &= " From MS_VENDING_MACHINE k " & vbNewLine
        sql &= " inner Join ms_location l on l.id=k.ms_location_id where 1=1 " & vbNewLine

        If vending_id <> 0 Then
            sql &= " And k.id =@_VENDING_ID" & vbNewLine
        End If
        If username <> "" Then
            sql &= " and k.ms_location_id in (select ms_location_id from ms_user_location where username=@_USERNAME)" & vbNewLine
        End If
        sql &= " order by com_name"

        Dim p(2) As SqlParameter
        p(0) = ServerDB.SetInt("@_VENDING_ID", vending_id)
        p(1) = ServerDB.SetText("@_USERNAME", username)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function




    'Public Function GetList_Kiosk_BundledProduct(uLoc_Code As String, Optional ByVal KO_ID As Integer = 0, Optional ByVal BP_ID As Integer = 0) As DataTable

    '    If KO_ID = 0 Then
    '        Return DAL.GetList_BundledProduct(0)
    '    Else
    '        Return DAL.GetList_Kiosk_BundledProduct_Existing(KO_ID, BP_ID, uLoc_Code)
    '    End If

    'End Function

    'Public Function GetList_BundledProduct(ByVal BP_ID As Integer) As DataTable
    '    Dim SQL As String = ""

    '    SQL &= " Select  BP.BP_ID,BP.BP_Code,BP.BP_Alias,BP.SIM_ID,BP.AIS_SIM_ID,BP.PCK_ID" & vbLf
    '    SQL &= " ,BP.AIS_Feature_Code,BP.AIS_Access_No,BP.Start_Date,BP.Expire_Date" & vbLf
    '    SQL &= " ,BP.Name_THA,BP.Name_ENG,BP.Name_CHN,BP.Name_RUS" & vbLf
    '    SQL &= " ,BP.DESC_THA,BP.DESC_ENG,BP.DESC_CHN,BP.DESC_RUS" & vbLf
    '    SQL &= " ,BP.Icon_THA,BP.Icon_ENG,BP.Icon_CHN,BP.Icon_RUS" & vbLf
    '    SQL &= " ,BP.Topup_Value,BP.SIM_Cost" & vbLf
    '    SQL &= " ,BP.IsActive,BP.Update_By,BP.Update_Time" & vbLf
    '    SQL &= " FROM tb_Bundled_Product BP" & vbLf
    '    SQL &= " " & vbLf
    '    If BP_ID <> 0 Then
    '        SQL &= " WHERE BP.BP_ID=" & BP_ID & vbLf
    '    End If

    '    SQL &= vbLf
    '    SQL &= " ORDER BY BP.BP_Code"

    '    Dim DA As New SqlDataAdapter(SQL, ConnectionString)
    '    Dim DT As New DataTable
    '    DT.TableName = "Data"
    '    DA.Fill(DT)
    '    Return DT
    'End Function

    'Public Function GetList_Kiosk_BundledProduct_Existing(ByVal KO_ID As Integer, ByVal BP_ID As Integer, uLoc_Code As String) As DataTable

    '    Dim SQL As String = ""
    '    SQL &= " Select B.BP_ID,KB.KO_ID,KO_Alias,B.BP_Code,B.BP_Alias,B.SIM_ID,B.AIS_SIM_ID,B.PCK_ID,B.AIS_Feature_Code,B.AIS_Access_No " & vbLf
    '    SQL &= " ,B.Start_Date,B.Expire_Date,B.Name_THA,B.Name_ENG,B.Name_CHN,B.Name_RUS " & vbLf
    '    SQL &= " ,B.DESC_THA,B.DESC_ENG,B.DESC_CHN,B.DESC_RUS " & vbLf
    '    SQL &= " ,B.Icon_THA,B.Icon_ENG,B.Icon_CHN,B.Icon_RUS " & vbLf
    '    SQL &= " ,B.Topup_Value,B.SIM_Cost,B.IsActive " & vbLf
    '    SQL &= " ,KB.BP_Order " & vbLf
    '    SQL &= "  " & vbLf
    '    SQL &= " From tb_Bundled_Product B " & vbLf
    '    SQL &= " CROSS Join tb_Kiosk K  " & vbLf
    '    SQL &= " Left Join tb_Kiosk_Bundled_Product KB On B.BP_ID=KB.BP_ID  " & vbLf
    '    SQL &= "                                      And K.KO_ID=KB.KO_ID " & vbLf

    '    Dim Filter As String = ""
    '    If KO_ID <> 0 Then
    '        Filter &= " K.KO_ID=" & KO_ID & " AND "
    '    End If
    '    If BP_ID <> 0 Then
    '        Filter &= " BP.BP_ID=" & BP_ID & " AND "
    '    End If
    '    If uLoc_Code <> "" Then
    '        Filter &= " K.Loc_Code in (" & uLoc_Code & ") And "
    '    End If
    '    If Filter <> "" Then
    '        SQL &= " WHERE " & Filter.Substring(0, Filter.Length - 4) & vbLf
    '    End If
    '    SQL &= "  " & vbLf
    '    SQL &= " ORDER BY B.BP_ID,K.KO_ID " & vbLf

    '    Dim DA As New SqlDataAdapter(SQL, ConnectionString)
    '    Dim DT As New DataTable
    '    DT.TableName = "Data"
    '    DA.Fill(DT)
    '    Return DT
    'End Function
#End Region

#Region "__DDL"
    Public Sub Bind_DDL_Location(ByRef ddl As DropDownList, ByVal UserName As String)
        Dim dt As DataTable = GetList_Location_ByUserLoc(UserName)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("location_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub
    Public Function GetList_Location_ByUserLoc(ByVal user_name As String) As DataTable
        Dim sql As String = " Select distinct id,location_name from MS_LOCATION"
        sql &= " where 1 = 1 "
        If (user_name <> "") Then
            sql &= " And id in (select ms_location_id from MS_USER_LOCATION where username=@_USERNAME)"
        End If
        sql &= " order by location_name"

        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_USERNAME", user_name)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)

        Return dt
    End Function

    Public Sub Bind_DDL_Category(ByRef ddl As DropDownList)
        Dim dt As DataTable = GetList_CategoryGroup("")

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("category_name").ToString, dt.Rows(i).Item("id").ToString)
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub
#End Region

#Region "_Get  Sysconfig"
    'Public Function GetServerSysconfig() As CfServerSysconfigServerLinqDB
    '    Dim ret As New CfServerSysconfigServerLinqDB
    '    ret.GetDataByPK(1, Nothing)

    '    Return ret
    'End Function

    'Public Function GetKioskSysconfig(MsKioskID As Long) As CfKioskSysconfigServerLinqDB
    '    Dim ret As New CfKioskSysconfigServerLinqDB
    '    ret.GetDataByPK(MsKioskID, Nothing)

    '    Return ret
    'End Function
#End Region

#Region "_Vending Config"
    Public Function GetList_Vending_for_config(user_name As String, ByVal vending_id As Integer) As DataTable
        Dim sql As String = ""

        sql &= " select v.id,com_name,ip_address,mac_address,location_name,db_servername,db_databasename,db_userid,db_password,v.active_status from ms_vending_machine  v"
        sql &= " Left join ms_location l on v.ms_location_id = l.id where v.active_status ='Y'"
        If vending_id <> 0 Then
            sql &= " and v.id =@_VENDING_ID"
        End If
        If user_name <> "" Then
            sql &= " and ms_location_id IN (select ms_location_id from MS_USER_LOCATION where username=@_USER_NAME)"
        End If

        sql &= " order by v.id,com_name,ip_address,mac_address,location_name "

        Dim p(2) As SqlParameter
        p(0) = ServerDB.SetText("@_USER_NAME", user_name)
        p(1) = ServerDB.SetInt("@_VENDING_ID", vending_id)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetList_VendingConfigInfo(database_name As String, vending_id As String) As DataTable
        Dim sql As String = "select v.id,com_name,v.ip_address,v.mac_address, vd.driver_name1 device_name_th,"
        sql &= " c.time_out_sec, c.show_msg_sec, c.payment_extend_sec, c.screen_saver_sec, c.shelf_long, c.sleep_time, c.sleep_duration"
        sql &= " from ms_vending_machine  v"
        sql &= " left join ms_vending_device  vd On v.id = vd.ms_vending_id "
        sql &= " left join ms_device d On vd.ms_device_id = d.id "
        sql &= " left join [" & database_name & "].dbo.CF_VENDING_SYSCONFIG c on v.id=c.id"
        sql &= " where v.active_status ='Y' and v.id=@_VENDING_ID  and d.ms_device_type_id=" & Engine.mdlDeviceInfoENG.DeviceType.NetworkConnection
        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetInt("@_VENDING_ID", vending_id)

        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt

    End Function
#End Region

#Region "_Promotion"
    Public Function GetVendingProductByVending(vending_id As String) As DataTable
        Dim sql As String = "select vp.ms_product_id,product_code,product_name_th,product_name_en,category_name "
        sql &= " From ms_vending_product vp  inner join ms_product p on vp.ms_product_id = p.id "
        sql &= " inner join ms_product_category c on p.ms_product_category_id = c.id where 1=1"
        If vending_id <> "" Then
            sql &= " and ms_vending_id = @_VENDING_ID"
        End If
        sql &= " order by product_code,product_name_th,product_name_en,category_name"

        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetText("@_VENDING_ID", vending_id)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetVendingProductInfo(vending_id As String, product_id As String) As DataTable
        Dim sql As String = "select v.id vending_id,ip_address,product_code,cost,price,product_image_small,p.id product_id"
        sql &= " From ms_vending_machine v inner join ms_vending_product vp on vp.ms_vending_id = v.id "
        sql &= " inner join ms_product p On vp.ms_product_id = p.id  "
        sql &= "  where 1=1 "
        If vending_id <> "" Then
            sql &= "  and ms_vending_id=@_VENDING_ID"
        End If
        If product_id <> "" Then
            sql &= " and ms_product_id = @_PRODUCT_ID"
        End If
        Dim p(2) As SqlParameter
        p(0) = ServerDB.SetText("@_VENDING_ID", vending_id)
        p(1) = ServerDB.SetText("@_PRODUCT_ID", product_id)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetVendingProductPromotion(vending_id As String, product_id As String) As DataTable
        Dim sql As String = "select  id,convert(varchar(10),start_date,103) start_date,convert(varchar(10),end_date,103) end_date,discount_percent from ms_vending_product_promotion "
        sql &= "  where 1=1 "
        If vending_id <> "" Then
            sql &= "  and ms_vending_id=@_VENDING_ID"
        End If
        If product_id <> "" Then
            sql &= " and ms_product_id = @_PRODUCT_ID"
        End If

        Dim p(2) As SqlParameter
        p(0) = ServerDB.SetText("@_VENDING_ID", vending_id)
        p(1) = ServerDB.SetText("@_PRODUCT_ID", product_id)
        Dim dt As New DataTable
        dt = ServerDB.ExecuteTable(sql, p)
        Return dt
    End Function
#End Region

    Sub DeleteFilesFromFolder(Folder As String)
        If Directory.Exists(Folder) Then
            For Each _file As String In Directory.GetFiles(Folder)
                File.Delete(_file)
            Next
            For Each _folder As String In Directory.GetDirectories(Folder)

                DeleteFilesFromFolder(_folder)
            Next

        End If

    End Sub

#Region "Set validate Textbox Property"
    Public Sub SetTextIntKeypress(txt As TextBox)
        txt.Attributes.Add("OnKeyPress", "ChkMinusInt(this,event);")
        txt.Attributes.Add("onKeyDown", "CheckKeyNumber(event);")
    End Sub
    Public Sub SetTextDblKeypress(txt As TextBox)
        txt.Attributes.Add("OnKeyPress", "ChkMinusDbl(this,event);")
        txt.Attributes.Add("onKeyDown", "CheckKeyNumber(event);")
    End Sub
    Public Sub SetTextAreaMaxLength(txt As TextBox, MaxLength As Int16)
        txt.Attributes.Add("onKeyDown", "checkTextAreaMaxLength(this,event,'" & MaxLength & "');")
    End Sub
#End Region

End Class
