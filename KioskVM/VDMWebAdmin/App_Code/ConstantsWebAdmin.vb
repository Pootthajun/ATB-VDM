﻿Imports Microsoft.VisualBasic

Public Class ConstantsWebAdmin
    Public Enum Authorization
        NA = 0
        View = 1
        Edit = 2
    End Enum
    Public Enum WebAdminFunctionalID
        Dashboard = 1

        ReportTransactionLog = 2
        'ReportTransactionPerformance = 4
        ReportSummary = 5
        'ReportSummaryByProduct = 6
        MailGroupReport = 3

        VendingMachineMonitor = 7
        SettingAlarm = 8

        MasterLocation = 9
        MasterVendingMachine = 10
        MasterVendingConfig = 19
        MasterAdvertising = 20
        MasterProductCategory = 21
        MasterProduct = 22
        MasterPromotion = 23
        MasterVendingScreen = 24

        AuthRole = 11
        AuthUser = 12
    End Enum

    Public Enum WebAdminFunctionalZoneID
        Dashboard = 1
        Reports = 2
        Monitor = 3
        Setting = 4
        StaffConsole = 5
    End Enum
End Class
