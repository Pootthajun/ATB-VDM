﻿Imports System.Data
Imports ServerLinqDB.ConnectDB
Imports ServerLinqDB.TABLE
Imports System.Data.SqlClient
Imports ConstantsWebAdmin

Partial Class frmSettingProduct
    Inherits System.Web.UI.Page
    Dim BL As New VDMBL

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property Edit_Product_ID As String
        Get
            Try
                Return ViewState("product_code")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("product_code") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If

        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.NA, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterProduct, UserName)
            If auDt.Rows.Count = 0 Then
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
                Exit Sub
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()

        If Not IsPostBack Then
            BindList()

            BL.SetTextDblKeypress(txtPrice)
            BL.SetTextDblKeypress(txtCost)

            BL.SetTextAreaMaxLength(txtDetailTH, 250)
            BL.SetTextAreaMaxLength(txtDetailEN, 250)
            BL.SetTextAreaMaxLength(txtDetailJP, 250)
            BL.SetTextAreaMaxLength(txtDetailCH, 250)

            BL.SetTextAreaMaxLength(txtWarrantyTH, 1000)
            BL.SetTextAreaMaxLength(txtWarrantyEN, 1000)
            BL.SetTextAreaMaxLength(txtWarrantyJP, 1000)
            BL.SetTextAreaMaxLength(txtWarrantyCH, 1000)
        Else
            initFormPlugin()
        End If

    End Sub

    Private Sub initFormPlugin()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Plugin", "initFormPlugin();", True)
    End Sub

    Private Sub BindList()
        Dim DT As DataTable = BL.GetProductList("")
        rptList.DataSource = DT
        rptList.DataBind()

        lblTotalList.Text = FormatNumber(DT.Rows.Count, 0)

        'ColEdit.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'ColDelete.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'btnAdd.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        Edit_Product_ID = ""
        pnlList.Visible = True
        pnlEdit.Visible = False

        SetAuthorization()
    End Sub

    Private Sub SetAuthorization()
        'ตรวจสอบสิทธิ์การแก้ไขข้อมูล
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.Edit, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterProduct, UserName)
            If auDt.Rows.Count = 0 Then
                ColEdit.InnerText = "View"
                For i As Integer = 0 To rptList.Items.Count - 1
                    Dim btnDelete As ImageButton = rptList.Items(i).FindControl("btnDelete")
                    btnDelete.Enabled = False
                Next
                btnAdd.Visible = False

                divBigImage.Visible = False
                divSmallImage.Visible = False
                ddlCategory.Enabled = False
                txtProductCode.Enabled = False
                txtPrice.Enabled = False
                txtCost.Enabled = False
                txtProductNameTH.Enabled = False
                txtDetailTH.Enabled = False
                txtWarrantyTH.Enabled = False

                txtProductNameEN.Enabled = False
                txtDetailEN.Enabled = False
                txtWarrantyEN.Enabled = False

                txtProductNameJP.Enabled = False
                txtDetailJP.Enabled = False
                txtWarrantyJP.Enabled = False

                txtProductNameCH.Enabled = False
                txtDetailCH.Enabled = False
                txtWarrantyCH.Enabled = False

                chkActive.Enabled = False
                btnSave.Visible = False
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        ClearEditForm()

        pnlList.Visible = False
        pnlEdit.Visible = True
        ChangeTab(btnTabTH, e)
    End Sub

    Private Sub ClearEditForm()
        BL.Bind_DDL_Category(ddlCategory)
        Edit_Product_ID = "0"
        txtProductCode.Text = ""
        txtProductNameTH.Text = ""
        txtDetailTH.Text = ""
        txtProductNameEN.Text = ""
        txtDetailEN.Text = ""
        txtProductNameJP.Text = ""
        txtDetailJP.Text = ""
        txtProductNameCH.Text = ""
        txtDetailCH.Text = ""
        txtPrice.Text = ""
        txtCost.Text = ""
        chkActive.Checked = True
        lblEditMode.Text = "Add"

        Session.Remove("ProductSmallImage")
        Session.Remove("ProductBigImage")

        SmallImgProduct.Attributes.Remove("src")
        BigImgProduct.Attributes.Remove("src")
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click

        If ddlCategory.SelectedIndex = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Category');", True)
            Exit Sub
        End If

        If txtProductCode.Text.Trim() = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Product Code');", True)
            Exit Sub
        End If

        If txtProductNameTH.Text.Trim() = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Product Name in Thai Language');", True)
            Exit Sub
        End If

        If txtProductNameEN.Text.Trim() = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Product Name in English Language');", True)
            Exit Sub
        End If

        If txtWarrantyTH.Text.Trim() = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Condition in Thai Language');", True)
            Exit Sub
        End If

        If txtWarrantyEN.Text.Trim() = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Condition in English Language');", True)
            Exit Sub
        End If


        Dim SmallByteImage As Byte() = CType(Session("ProductSmallImage"), Byte())
        Dim BigByteImage As Byte() = CType(Session("ProductBigImage"), Byte())
        If SmallByteImage Is Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Please Add Small Image');", True)
            Exit Sub
        End If
        If BigByteImage Is Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Please Add Big Image');", True)
            Exit Sub
        End If

        Dim trans As New ServerTransactionDB
        Try
            Dim DT As DataTable = BL.GetProductList("")
            DT.DefaultView.RowFilter = "product_code='" & txtProductCode.Text.Replace("'", "''") & "' AND id <>'" & Edit_Product_ID.Replace("'", "''") & "'"
            If DT.DefaultView.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This Product Code is already existed');", True)
                Exit Sub
            End If

            DT.DefaultView.RowFilter = ""
            DT.DefaultView.RowFilter = "product_name_th='" & txtProductNameTH.Text.Replace("'", "''") & "' AND id <>'" & Edit_Product_ID.Replace("'", "''") & "'"
            If DT.DefaultView.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This Product Name in Thai Language is already existed');", True)
                Exit Sub
            End If

            DT.DefaultView.RowFilter = ""
            DT.DefaultView.RowFilter = "product_name_en='" & txtProductNameEN.Text.Replace("'", "''") & "' AND id <>'" & Edit_Product_ID.Replace("'", "''") & "'"
            If DT.DefaultView.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This Product Name in English Language is already existed');", True)
                Exit Sub
            End If

            Dim lnqLoc As New MsProductServerLinqDB
            lnqLoc.ChkDataByPK(Edit_Product_ID, trans.Trans)
            With lnqLoc
                .MS_PRODUCT_CATEGORY_ID = ddlCategory.SelectedValue
                .PRODUCT_CODE = txtProductCode.Text.Trim()
                .PRODUCT_NAME_TH = txtProductNameTH.Text.Trim()
                .PRODUCT_DESC_TH = txtDetailTH.Text.Trim()
                .PRODUCT_NAME_EN = txtProductNameEN.Text.Trim()
                .PRODUCT_DESC_EN = txtDetailEN.Text.Trim()
                .PRODUCT_NAME_JP = txtProductNameJP.Text.Trim()
                .PRODUCT_DESC_JP = txtDetailJP.Text.Trim()
                .PRODUCT_NAME_CH = txtProductNameCH.Text.Trim()
                .PRODUCT_DESC_CH = txtDetailCH.Text.Trim()
                .PRICE = txtPrice.Text.Trim
                .COST = txtCost.Text.Trim
                .PRODUCT_IMAGE_SMALL = SmallByteImage
                .PRODUCT_IMAGE_BIG = BigByteImage
                .ACTIVE_STATUS = IIf(chkActive.Checked, "Y", "N")
            End With

            Dim ret As New ExecuteDataInfo
            If lnqLoc.ID = 0 Then
                ret = lnqLoc.InsertData(UserName, trans.Trans)
            Else
                ret = lnqLoc.UpdateData(UserName, trans.Trans)
            End If
            If ret.IsSuccess = True Then
                Dim cLnq As New MsProductWarrantyServerLinqDB
                cLnq.ChkDataByMS_PRODUCT_ID(lnqLoc.ID, trans.Trans)
                cLnq.MS_PRODUCT_ID = lnqLoc.ID
                cLnq.WARRANTY_TH = txtWarrantyTH.Text
                cLnq.WARRANTY_EN = txtWarrantyEN.Text
                cLnq.WARRANTY_JP = txtWarrantyJP.Text
                cLnq.WARRANTY_CH = txtWarrantyCH.Text

                If cLnq.ID > 0 Then
                    ret = cLnq.UpdateData(UserName, trans.Trans)
                Else
                    ret = cLnq.InsertData(UserName, trans.Trans)
                End If

                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save success');", True)
                    BindList()
                Else
                    trans.RollbackTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", "") & "');", True)
                End If
            Else
                trans.RollbackTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", "") & "');", True)
            End If

        Catch ex As Exception
            trans.RollbackTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('cant save data!' " & ex.ToString().Replace("'", "") & ");", True)
        End Try


    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        BindList()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim td As HtmlTableCell = e.Item.FindControl("td")
        Dim lblCategory As Label = e.Item.FindControl("lblCategory")
        Dim lblProductID As Label = e.Item.FindControl("lblProductID")
        Dim lblProductCode As Label = e.Item.FindControl("lblProductCode")
        Dim lblProductName As Label = e.Item.FindControl("lblProductName")
        Dim lblDetail As Label = e.Item.FindControl("lblDetail")
        Dim lblPrice As Label = e.Item.FindControl("lblPrice")
        Dim lblCost As Label = e.Item.FindControl("lblCost")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim cfmDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfmDelete")

        Dim ColEdit As HtmlTableCell = e.Item.FindControl("ColEdit")
        Dim ColDelete As HtmlTableCell = e.Item.FindControl("ColDelete")

        lblProductID.Text = e.Item.DataItem("id").ToString
        lblProductCode.Text = e.Item.DataItem("product_code").ToString
        lblProductName.Text = e.Item.DataItem("product_name_th").ToString
        lblDetail.Text = e.Item.DataItem("product_desc_th").ToString
        lblCategory.Text = e.Item.DataItem("category").ToString
        If Not e.Item.DataItem("active_status").ToString = "Y" Then
            td.Attributes("class") = "text-danger"
        Else
            td.Attributes("class") = "text-primary"
        End If

        lblPrice.Text = FormatNumber(e.Item.DataItem("price"), 0)
        lblCost.Text = FormatNumber(e.Item.DataItem("cost"), 0)
        btnEdit.CommandArgument = e.Item.DataItem("id")
        btnDelete.CommandArgument = e.Item.DataItem("id")
        cfmDelete.ConfirmText = "Confirm to delete " & e.Item.DataItem("product_name_th").ToString & " ?"

        'ColEdit.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'ColDelete.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                ClearEditForm()
                Dim Product_ID As String = e.CommandArgument
                Edit_Product_ID = Product_ID

                Dim DT As DataTable = BL.GetProductList(Product_ID, True)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & Product_ID.Replace("'", """") & " is not found!');", True)
                    Exit Sub
                End If

                ddlCategory.SelectedValue = DT.Rows(0).Item("ms_product_category_id").ToString
                txtProductCode.Text = DT.Rows(0).Item("product_code").ToString
                txtProductNameTH.Text = DT.Rows(0).Item("product_name_th").ToString
                txtDetailTH.Text = DT.Rows(0).Item("product_desc_th").ToString
                txtWarrantyTH.Text = DT.Rows(0).Item("warranty_th").ToString
                txtProductNameEN.Text = DT.Rows(0).Item("product_name_en").ToString
                txtDetailEN.Text = DT.Rows(0).Item("product_desc_en").ToString
                txtWarrantyEN.Text = DT.Rows(0).Item("warranty_en").ToString
                txtProductNameJP.Text = DT.Rows(0).Item("product_name_jp").ToString
                txtDetailJP.Text = DT.Rows(0).Item("product_desc_jp").ToString
                txtWarrantyJP.Text = DT.Rows(0).Item("warranty_jp").ToString
                txtProductNameCH.Text = DT.Rows(0).Item("product_name_ch").ToString
                txtDetailCH.Text = DT.Rows(0).Item("product_desc_ch").ToString
                txtWarrantyCH.Text = DT.Rows(0).Item("warranty_ch").ToString
                txtPrice.Text = DT.Rows(0).Item("price").ToString
                txtCost.Text = DT.Rows(0).Item("cost").ToString

                Session("ProductImage") = CType(DT.Rows(0).Item("product_image_small"), Byte())
                btnUpdateSmallImage_Click(Nothing, e)

                Session("ProductImage") = CType(DT.Rows(0).Item("product_image_big"), Byte())
                btnUpdateBigImage_Click(Nothing, e)

                chkActive.Checked = IIf(DT.Rows(0).Item("active_status").ToString() = "Y", True, False)

                lblEditMode.Text = "Edit"

                pnlList.Visible = False
                pnlEdit.Visible = True
                ChangeTab(btnTabTH, e)

                SetAuthorization()
            Case "Delete"
                Dim ret As String = BL.DeleteMasterProduct(e.CommandArgument)
                If ret = "" Then
                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.Replace("'", """") & "');", True)
                End If
        End Select
    End Sub


#Region "AddImage"
    Private Sub btnUpdateSmallImage_Click(sender As Object, e As EventArgs) Handles btnUpdateSmallImage.Click
        Dim ByteImage As Byte() = CType(Session("ProductImage"), Byte())
        Dim base64String As String = Convert.ToBase64String(ByteImage, 0, ByteImage.Length)
        SmallImgProduct.Attributes.Remove("src")
        SmallImgProduct.Attributes.Add("src", "data:image/jpeg;base64," + base64String)
        Session("ProductSmallImage") = Session("ProductImage")
        Session.Remove("ProductImage")
    End Sub

    Private Sub btnUpdateBigImage_Click(sender As Object, e As EventArgs) Handles btnUpdateBigImage.Click
        Dim ByteImage As Byte() = CType(Session("ProductImage"), Byte())
        Dim base64String As String = Convert.ToBase64String(ByteImage, 0, ByteImage.Length)
        BigImgProduct.Attributes.Remove("src")
        BigImgProduct.Attributes.Add("src", "data:image/jpeg;base64," + base64String)
        Session("ProductBigImage") = Session("ProductImage")
        Session.Remove("ProductImage")
    End Sub
#End Region


#Region "Tab"
    Protected Enum Tab
        Unknown = 0
        TH = 1
        EN = 2
        JP = 3
        CH = 4
    End Enum

    Protected Property CurrentTab As Tab
        Get
            Select Case True
                Case tabTH.Visible
                    Return Tab.TH
                Case tabEN.Visible
                    Return Tab.EN
                Case tabJP.Visible
                    Return Tab.JP
                Case tabCH.Visible
                    Return Tab.CH
                Case Else
                    Return Tab.Unknown
            End Select
        End Get
        Set(value As Tab)
            tabTH.Visible = False
            tabEN.Visible = False
            tabJP.Visible = False
            tabCH.Visible = False
            liTabTH.Attributes("class") = ""
            liTabEN.Attributes("class") = ""
            liTabJP.Attributes("class") = ""
            liTabCH.Attributes("class") = ""

            Select Case value
                Case Tab.TH
                    tabTH.Visible = True
                    liTabTH.Attributes("class") = "active"
                Case Tab.EN
                    tabEN.Visible = True
                    liTabEN.Attributes("class") = "active"
                Case Tab.JP
                    tabJP.Visible = True
                    liTabJP.Attributes("class") = "active"
                Case Tab.CH
                    tabCH.Visible = True
                    liTabCH.Attributes("class") = "active"
                Case Else
            End Select
        End Set
    End Property

    Private Sub ChangeTab(sender As Object, e As System.EventArgs) Handles btnTabTH.Click, btnTabEN.Click, btnTabJP.Click, btnTabCH.Click
        Select Case True
            Case Equals(sender, btnTabTH)
                CurrentTab = Tab.TH
            Case Equals(sender, btnTabEN)
                CurrentTab = Tab.EN
            Case Equals(sender, btnTabJP)
                CurrentTab = Tab.JP
            Case Equals(sender, btnTabCH)
                CurrentTab = Tab.CH
        End Select
    End Sub


#End Region

End Class
