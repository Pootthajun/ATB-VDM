﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmVendingConfig.aspx.vb" Inherits="frmVendingConfig" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="HeaderContainer" ContentPlaceHolderID="HeaderContainer" runat="server">
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" type="text/css" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css" />
  <!-- end page stylesheets -->

  <style type="text/css">
        .numeric
           {
               text-align:center;
               }
  </style>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title">Setting > Vending Config</div>
        <div class="sub-title"></div>
    </div>
<asp:UpdatePanel ID="udpList" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Visible="True">
            <div class="card-header">
                Found : <asp:Label ID="lblTotalList" runat="server"></asp:Label> Vending(s)
            </div>
            <div class="card-block">
                <div class="no-more-tables">
                <table class="table table-bordered table-striped m-b-0">
                  <thead>
                    <tr>
                     <%-- <th>VENDING ID</th>--%>
                      <th>VENDING NAME</th>
                      <th>IP ADDRESS</th>
                      <th>MAC ADDRESS</th>
                      <th>LOCATION</th>
                      <th id="ColEdit" runat="server">Edit</th>
                     <%-- <th id="ColDelete" runat="server">Delete</th>--%>
                    </tr>
                  </thead>
                  <tbody>
                  <asp:Repeater ID="rptList" runat="server">
                    <ItemTemplate>
                        <tr>                 
                         <%-- <td data-title="VENDING ID"><asp:Label ID="lblVendingID" runat="server"></asp:Label></td>--%>
                          <td data-title="VENDING NAME"><asp:Label ID="lblVendingName" runat="server"></asp:Label></td>
                          <td data-title="IP ADDRESS" id="td" runat="server" ><asp:Label ID="lblIPAddress" runat="server"></asp:Label></td>                         
                          <td data-title="MAC ADDRESS" id="td2" runat="server" ><asp:Label ID="lblMACAddress" runat="server"></asp:Label></td>                         
                          <td data-title="LOCATION" id="td1" runat="server" ><asp:Label ID="lblLocation" runat="server"></asp:Label>
                          <asp:Label ID="lbldbservername" runat="server" Visible="false"></asp:Label>
                          <asp:Label ID="lbldbdatabasename" runat="server" Visible="false"></asp:Label>
                          <asp:Label ID="lbldbuserid" runat="server" Visible="false"></asp:Label>
                          <asp:Label ID="lbldbpassword" runat="server" Visible="false"></asp:Label>
                          </td>               
                          <td data-title="Edit" id="ColEdit" runat="server">
                          <center>
                          <asp:ImageButton src="images/icon/100/pencil.png" class="btn btn-success mr5" ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" style="width:70px;height:40px;"/>
                        </center>
                          </td>
                         
                        </tr>
                     
                    </ItemTemplate>
                  </asp:Repeater>            
                  </tbody>
                </table>
                </div>
            
              
              </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="udpEdit" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlEdit" runat="server"  CssClass="card bg-white">
            <div class="card-header">
                <asp:Label ID="lblEditMode" runat="server"></asp:Label>  Vending Config
            </div>
            <div class="card-block">
                <div class="row m-a-0">
                  <div class="col-lg-12 form-horizontal">
                      <h4 class="card-title">Vending Config Info</h4>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Vending ID</label>
                        <div class="col-sm-2">
                          <asp:TextBox ID="txtVendingID" runat="server" CssClass="form-control" MaxLength="20" Enabled="false"></asp:TextBox>                      
                        </div>
                      </div>                  
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Network Device</label>
                        <div class="col-sm-5">
                            <asp:TextBox ID="txtNetworkDevice" runat="server" CssClass="form-control" MaxLength="200" Enabled="false"></asp:TextBox>
                        </div>                                       
                      </div>
                       <div class="form-group">
                        <label class="col-sm-2 control-label">IP Address</label>
                        <div class="col-sm-5">
                            <asp:TextBox ID="txtIPAddress" runat="server" CssClass="form-control" MaxLength="200" Enabled="false"></asp:TextBox>
                        </div>                                       
                      </div>
                       <div class="form-group">
                        <label class="col-sm-2 control-label">MAC Address</label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtMacAddress" runat="server" CssClass="form-control" MaxLength="200" Enabled="false"></asp:TextBox>
                        </div> 
                                                              
                      </div>
                       <div class="form-group">
                        <label class="col-sm-2 control-label">Time Out</label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtTimeOut" runat="server" CssClass="form-control" Style="text-align: right;" MaxLength="3" ></asp:TextBox>
                        </div>  
                        <label class="col-sm-1 control-label-left">Second</label>                                   
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Message Time</label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtMessageTime" runat="server" CssClass="form-control" Style="text-align: right;" MaxLength="3" ></asp:TextBox>
                        </div>
                          <label class="col-sm-1 control-label-left">Second</label>                             
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Payment Extend</label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtPaymentExtend" runat="server" CssClass="form-control" Style="text-align: right;" MaxLength="3" ></asp:TextBox>
                        </div>
                          <label class="col-sm-1 control-label-left">Second</label>                                    
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Screen Saver</label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtScreenSaver" runat="server" CssClass="form-control" Style="text-align: right;" MaxLength="3" ></asp:TextBox>
                        </div>         
                          <label class="col-sm-1 control-label-left">Second</label>                              
                      </div>
                       <div class="form-group">
                        <label class="col-sm-2 control-label">Shelf Long</label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtShelfLong" runat="server" CssClass="form-control" Style="text-align: right;" MaxLength="5" ></asp:TextBox> 
                        </div>  
                           <label class="col-sm-1 control-label-left">Millimeter</label>                                     
                      </div>
                       <div class="form-group">
                        <label class="col-sm-2 control-label ">Sleep Time</label>
                        <div class="col-sm-5 ">
                            <asp:TextBox ID="txtSleepTimeH" runat="server" CssClass="form-control" MaxLength="2" Width="80px" Style="text-align: center; display: inline; padding: 0;"></asp:TextBox> 
                             :
                         <asp:TextBox ID="txtSleepTimeM" runat="server" CssClass="form-control" MaxLength="2" Width="80px" Style="text-align: center; display: inline; padding: 0;"></asp:TextBox>
                        </div>     
                                                        
                      </div>
                       <div class="form-group">
                        <label class="col-sm-2 control-label">Sleep Duration</label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtSleepDuration" runat="server" CssClass="form-control" Style="text-align: right;" MaxLength="3" ></asp:TextBox> 
                        </div>                                       
                           <label class="col-sm-1 control-label-left">Minute</label>  
                      </div>

                     
                      <div class="form-group" style="text-align:right">
                            <asp:LinkButton CssClass="btn btn-primary btn-icon loading-demo mr5 btn-shadow" ID="btnSave" runat="server">
                              <i class="fa fa-save"></i>
                              <span>Save</span>
                            </asp:LinkButton>

                            <asp:LinkButton CssClass="btn btn-warning btn-icon loading-demo mr5 btn-shadow" ID="btnBack" runat="server">
                              <i class="fa fa-rotate-left"></i>
                              <span>Cancel</span>
                            </asp:LinkButton>
                      </div>
                  </div>
                </div>
              </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="ScriptContainer" ContentPlaceHolderID="ScriptContainer" runat="server">
 <%-- <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/moment/min/moment.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/select2/dist/js/select2.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js" type="text/javascript" language="javascript"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/forms/plugins.js" type="text/javascript"></script>
  <!-- end initialize page scripts -->--%>
</asp:Content>

