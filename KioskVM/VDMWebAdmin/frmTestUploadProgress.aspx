﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmTestUploadProgress.aspx.vb" Inherits="frmTestUploadProgress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
    <script>

        function uploadFile(){
            var file = $("#file1").prop("files")[0];
	        // alert(file.name+" | "+file.size+" | "+file.type);
	        var formdata = new FormData();
	        formdata.append("file1", file);
	        var ajax = new XMLHttpRequest();
	        ajax.upload.addEventListener("progress", progressHandler, false);
	        ajax.addEventListener("load", completeHandler, false);
	        ajax.addEventListener("error", errorHandler, false);
	        ajax.addEventListener("abort", abortHandler, false);
	        ajax.open("POST", "frmVDOUpload.aspx");
	        ajax.send(formdata);
        }
        function progressHandler(event) {
            
	        $("#loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
	        var percent = (event.loaded / event.total) * 100;

            //$("#progressBar").value = Math.round(percent);
	        document.getElementById("progressBar").value = Math.round(percent);
	        document.getElementById("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
        }
        function completeHandler(event) {
            alert("Complete : ");
	        $("#status").innerHTML = event.target.responseText;
	        $("#progressBar").value = 0;
        }
        function errorHandler(event){
	        $("#status").innerHTML = "Upload Failed";
        }
        function abortHandler(event){
	        $("#status").innerHTML = "Upload Aborted";
        }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="page-title">
        <div class="title">Upload File Width Progress bar</div>
        <div class="sub-title"></div>
    </div>


    <input type="file" name="file1" id="file1"  ><br>
    <input type="button" value="Upload File" onclick="uploadFile();">
    <progress id="progressBar"  value="0" max="100" style="width:300px;"></progress>
    <h3 id="status"></h3>
    <p id="loaded_n_total"></p>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
</asp:Content>

