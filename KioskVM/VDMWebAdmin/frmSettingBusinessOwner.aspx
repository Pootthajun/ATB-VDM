﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmSettingBusinessOwner.aspx.vb" Inherits="frmSettingBusinessOwner" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>
<asp:Content ID="HeaderContainer" ContentPlaceHolderID="HeaderContainer" runat="server">
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" type="text/css" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css" />
  <!-- end page stylesheets -->

  <style type="text/css">
        .numeric
           {
               text-align:center;
               }
  </style>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title">Setting > Business Owner</div>
        <div class="sub-title"></div>
    </div>
<asp:UpdatePanel ID="udpList" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Visible="True">
            <div class="card-header">
                Found : <asp:Label ID="lblTotalList" runat="server"></asp:Label> Business Owner(s)
            </div>
            <div class="card-block">
                <div class="no-more-tables">
                <table class="table table-bordered table-striped m-b-0">
                  <thead>
                    <tr>
                      <th>BUSINESS OWNER CODE</th>
                      <th>BUSINESS OWNER NAME</th>
                      <th>STATUS</th>
                      <th id="ColEdit" runat="server">Edit</th>
                      <th id="ColDelete" runat="server">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                  <asp:Repeater ID="rptList" runat="server">
                    <ItemTemplate>
                        <tr>                 
                          <td data-title="BUSINESS OWNER CODE"><asp:Label ID="lblBusinessOwnerCode" runat="server"></asp:Label></td>
                          <td data-title="BUSINESS OWNER NAME" id="td" runat="server" ><asp:Label ID="lblBusinessOwnerName" runat="server"></asp:Label></td>                         
                          <td data-title="STATUS" id="td1" runat="server" ><asp:Label ID="lblStatus" runat="server"></asp:Label></td>               
                          <td data-title="Edit" id="ColEdit" runat="server">
                          <center>
                          <asp:ImageButton src="images/icon/100/pencil.png" class="btn btn-success mr5" ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" style="width:70px;height:40px;"/>
                        </center>
                          </td>
                          <td data-title="Delete" id="ColDelete" runat="server">
                          <center>
                          <asp:ImageButton src="images/icon/100/delete.png" CssClass="btn btn-danger mr5" ID="btnDelete" runat="server" Text="Delete" CommandName="Delete"  style="width:70px;height:40px;"/>
                        </center>
                          </td>
                        </tr>
                        <ajaxToolkit:ConfirmButtonExtender ID="cfmDelete" runat="server" TargetControlID="btnDelete" />
                    </ItemTemplate>
                  </asp:Repeater>            
                  </tbody>
                </table>
                </div>
            
                <div class="row m-t">
                    <asp:LinkButton CssClass="btn btn-primary btn-icon loading-demo mr5 btn-shadow" ID="btnAdd" runat="server">
                      <i class="fa fa-plus-circle"></i>
                      <span>Add new Business Owner</span>
                    </asp:LinkButton>
                </div>
              </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="udpEdit" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlEdit" runat="server"  CssClass="card bg-white">
            <div class="card-header">
                <asp:Label ID="lblEditMode" runat="server"></asp:Label>  Business Owner
            </div>
            <div class="card-block">
                <div class="row m-a-0">
                  <div class="col-lg-12 form-horizontal">
                      <h4 class="card-title">Business Owner Info</h4>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">BUSINESS OWNER CODE</label>
                        <div class="col-sm-4">
                          <asp:TextBox ID="txtBusinessOwnerCode" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>                      
                        </div>
                      </div>                  
                      <div class="form-group">
                        <label class="col-sm-2 control-label">BUSINESS OWNER NAME</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtBusinessOwnerName" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                        </div>                                       
                      </div>
                      <div class="form-group" style="margin-left:-5px;">
                            <h4 class="card-title col-sm-2 control-label" style="text-align:left;">Active Status </h4>  
                        
                            <label class="col-sm-10 cb-checkbox cb-md">
                                <asp:CheckBox ID="chkActive" runat="server"/>
                            </label>
                      </div>
                      <div class="form-group" style="text-align:right">
                            <asp:LinkButton CssClass="btn btn-primary btn-icon loading-demo mr5 btn-shadow" ID="btnSave" runat="server">
                              <i class="fa fa-save"></i>
                              <span>Save</span>
                            </asp:LinkButton>

                            <asp:LinkButton CssClass="btn btn-warning btn-icon loading-demo mr5 btn-shadow" ID="btnBack" runat="server">
                              <i class="fa fa-rotate-left"></i>
                              <span>Cancel</span>
                            </asp:LinkButton>
                      </div>
                  </div>
                </div>
              </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="ScriptContainer" ContentPlaceHolderID="ScriptContainer" runat="server">
  <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/moment/min/moment.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/select2/dist/js/select2.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js" type="text/javascript" language="javascript"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/forms/plugins.js" type="text/javascript"></script>
  <!-- end initialize page scripts -->
</asp:Content>

