﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmSettingProduct.aspx.vb" Inherits="frmSettingProduct" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>
<asp:Content ID="HeaderContainer" ContentPlaceHolderID="HeaderContainer" runat="server">
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" type="text/css" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css" />
  <!-- end page stylesheets -->

  <style type="text/css">
        .numeric
           {
               text-align:center;
               }
  </style>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title">Setting > Product</div>
        <div class="sub-title"></div>
    </div>
<asp:UpdatePanel ID="udpList" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Visible="True">
            <div class="card-header">
                Found : <asp:Label ID="lblTotalList" runat="server"></asp:Label> Product(s)
            </div>
            <div class="card-block">
                <div class="no-more-tables">
                <table class="table table-bordered table-striped m-b-0">
                  <thead>
                    <tr>
                      <th>CATEGORY</th>
                      <th>PRODUCT CODE</th>
                      <th>PRODUCT NAME</th>
                      <th>DETAIL</th>
                      <th>PRICE</th>
                      <th>COST</th>
                      <th id="ColEdit" runat="server">Edit</th>
                      <th id="ColDelete" runat="server">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                  <asp:Repeater ID="rptList" runat="server">
                    <ItemTemplate>
                        <tr>                 
                          <td data-title="Category"><asp:Label ID="lblCategory" runat="server"></asp:Label></td>
                          <td data-title="Product Code">
                          <asp:Label ID="lblProductCode" runat="server"></asp:Label>
                          <asp:Label ID="lblProductID" runat="server" Visible="false"></asp:Label>
                          </td>
                          <td data-title="Product Name" id="td" runat="server"><asp:Label ID="lblProductName" runat="server"></asp:Label></td>
                          <td data-title="Detail"><asp:Label ID="lblDetail" runat="server"></asp:Label></td>
                          <td data-title="Price" class="numeric"><asp:Label ID="lblPrice" runat="server"></asp:Label></td>                          
                          <td data-title="Cost" class="numeric"><asp:Label ID="lblCost" runat="server"></asp:Label></td>                  
                          <td data-title="Edit" id="ColEdit" runat="server">
                          <center>
                          <asp:ImageButton src="images/icon/100/pencil.png" class="btn btn-success mr5" ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" style="width:70px;height:40px;"/>
                        </center>
                          </td>
                          <td data-title="Delete" id="ColDelete" runat="server">
                          <center>
                          <asp:ImageButton src="images/icon/100/delete.png" CssClass="btn btn-danger mr5" ID="btnDelete" runat="server" Text="Delete" CommandName="Delete"  style="width:70px;height:40px;"/>
                        </center>
                          </td>
                        </tr>
                        <ajaxToolkit:ConfirmButtonExtender ID="cfmDelete" runat="server" TargetControlID="btnDelete" />
                    </ItemTemplate>
                  </asp:Repeater>            
                  </tbody>
                </table>
                </div>
            
                <div class="row m-t">
                    <asp:LinkButton CssClass="btn btn-primary btn-icon loading-demo mr5 btn-shadow" ID="btnAdd" runat="server">
                      <i class="fa fa-plus-circle"></i>
                      <span>Add new Product</span>
                    </asp:LinkButton>
                </div>
              </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="udpEdit" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlEdit" runat="server"  CssClass="card bg-white">
            <div class="card-header">
                <asp:Label ID="lblEditMode" runat="server"></asp:Label> Product
            </div>
            <div class="card-block">
                <div class="row m-a-0">
                  <div class="col-lg-12 form-horizontal">
                      <h4 class="card-title">Product Info</h4>
                     
                     <div class="col-sm-3">

                         <div class="row">
                            <%-- <img id="SmallImgProduct" runat="server" src="images/No_image.gif" style="height: 180px; width: 210px; cursor: pointer;" />--%>
                             <img id="SmallImgProduct" runat="server" src="" style="height: 180px; width: 210px; cursor: pointer;" />
                         </div>
                         <div class="row" id="divSmallImage" runat="server" >
                             <a href="#" onclick="UploadInsertSmallImage();return false;" class="btn btn-primary btn-icon loading-demo mr5 m-b btn-shadow col-sm-8" id="btnInsertSmallImage" runat="server">
                                 <i class="icon-plus"></i>
                                 <span>Add Small Image</span>
                             </a>
                             <asp:FileUpload ID="fulInsertSmallImage" ClientIDMode="Static" runat="server" Style="display: none;" onchange="asyncSmallImageInsert();" />
                             <asp:Button ID="btnUpdateSmallImage" ClientIDMode="Static" runat="server" Style="display: none;" />
                         </div>
                         <div class="row">
                         <div class="help-block col-sm-9 m-b">Support only image jpeg gif png, file dimension must be 100x100 and file size must not larger than 1MB</div>
                         </div>

                          <div class="row">
                            <%-- <img id="BigImgProduct" runat="server" src="images/No_image.gif" style="height: 180px; width: 210px; cursor: pointer;" />--%>
                             <img id="BigImgProduct" runat="server" src="" style="height: 180px; width: 210px; cursor: pointer;" />
                         </div>
                         <div class="row" id="divBigImage" runat="server">
                             <a href="#" onclick="UploadInsertBigImage();return false;" class="btn btn-danger btn-icon loading-demo mr5 m-b btn-shadow col-sm-8" id="btnInsertBigImage" runat="server">
                                 <i class="icon-plus"></i>
                                 <span>Add Big Image</span>
                             </a>
                             <asp:FileUpload ID="fulInsertBigImage" ClientIDMode="Static" runat="server" Style="display: none;" onchange="asyncBigImageInsert();" />
                             <asp:Button ID="btnUpdateBigImage" ClientIDMode="Static" runat="server" Style="display: none;" />
                         </div>
                         <div class="row">
                         <div class="help-block col-sm-9 m-b">Support only image jpeg gif png, file dimension must be 500x360 and file size must not larger than 3MB</div>
                         </div>
                         

                       
                    
                     </div>

                     <div class="col-sm-9">
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-4">
                          <asp:DropDownList ID="ddlCategory" runat="server" data-placeholder="Select Category" AutoPostBack="true" CssClass="chosen form-control" Width="100%">
                          </asp:DropDownList>                      
                        </div>
                      </div>                  
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Product Code</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtProductCode" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        </div>                                       
                      </div>
                       <div class="form-group">
                        <label class="col-sm-2 control-label">Price</label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control" MaxLength="7" Width="200px"></asp:TextBox>
                        </div>           
                         <div class="col-sm-8"> 
                        <label class="col-sm-2 control-label">Bath</label>   
                        </div>                             
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Cost</label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtCost" runat="server" CssClass="form-control" MaxLength="7" Width="200px"></asp:TextBox>
                        </div>         
                         <div class="col-sm-8"> 
                        <label class="col-sm-2 control-label">Bath</label>   
                        </div>                               
                      </div>


                         <div class="form-group">
                             <label class="col-sm-2 control-label">Language</label>
                             <div class="col-sm-10">
                                 <div class="card" style="border: 0.125rem solid #e4e4e4;">
                                     <div class="card-block p-a-0">
                                         <div class="box-tab m-b-0">
                                             <ul class="nav nav-tabs">
                                                 <li class="" id="liTabTH" runat="server">
                                                     <asp:LinkButton ID="btnTabTH" runat="server">
                                         <div class="iti-flag gb" style="float: left; margin-top: 3px; margin-right: 3px; cursor: pointer;"></div>
                                         <span>Thai</span>
                                                     </asp:LinkButton>
                                                 </li>
                                                 <li class="" id="liTabEN" runat="server">
                                                     <asp:LinkButton ID="btnTabEN" runat="server">
                                         <div class="iti-flag ru" style="float: left; margin-top: 0px; margin-right: 3px; cursor: pointer;"></div>
                                         <span>English</span>
                                                     </asp:LinkButton>
                                                 </li>
                                                 <li class="" id="liTabJP" runat="server">
                                                     <asp:LinkButton ID="btnTabJP" runat="server">
                                         <div class="iti-flag ru" style="float: left; margin-top: 0px; margin-right: 3px; cursor: pointer;"></div>
                                         <span>Japanese</span>
                                                     </asp:LinkButton>
                                                 </li>
                                                 <li class="" id="liTabCH" runat="server">
                                                     <asp:LinkButton ID="btnTabCH" runat="server">
                                         <div class="iti-flag ru" style="float: left; margin-top: 0px; margin-right: 3px; cursor: pointer;"></div>
                                         <span>Chinese</span>
                                                     </asp:LinkButton>
                                                 </li>
                                             </ul>
                                             <div style="padding: 0.9375rem;">

                                                 <asp:Panel ID="tabTH" runat="server">
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Product Name</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtProductNameTH" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Detail</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtDetailTH" runat="server" CssClass="form-control" Height="60px" TextMode="MultiLine"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Condition</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtWarrantyTH" runat="server" CssClass="form-control" MaxLength="1000" Height="200px" TextMode="MultiLine"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                 </asp:Panel>

                                                 <asp:Panel ID="tabEN" runat="server">
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Product Name</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtProductNameEN" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Detail</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtDetailEN" runat="server" CssClass="form-control" Height="60px" TextMode="MultiLine"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Condition</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtWarrantyEN" runat="server" CssClass="form-control" MaxLength="1000" Height="200px" TextMode="MultiLine"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                 </asp:Panel>

                                                 <asp:Panel ID="tabJP" runat="server">
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Product Name</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtProductNameJP" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Detail</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtDetailJP" runat="server" CssClass="form-control" Height="60px" TextMode="MultiLine"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Condition</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtWarrantyJP" runat="server" CssClass="form-control" MaxLength="1000" Height="200px" TextMode="MultiLine"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                 </asp:Panel>

                                                 <asp:Panel ID="tabCH" runat="server">
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Product Name</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtProductNameCH" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Detail</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtDetailCH" runat="server" CssClass="form-control" Height="60px" TextMode="MultiLine"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                     <div class="form-group">
                                                         <label class="col-sm-2 control-label">Condition</label>
                                                         <div class="col-sm-10">
                                                             <asp:TextBox ID="txtWarrantyCH" runat="server" CssClass="form-control" MaxLength="1000" Height="200px" TextMode="MultiLine"></asp:TextBox>
                                                         </div>
                                                     </div>
                                                 </asp:Panel>

                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>


                     
                       <%--<div class="form-group">
                        <label class="col-sm-2 control-label">Adjust Area</label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtAdjustArea" runat="server" CssClass="form-control" MaxLength="5" Width="200px"></asp:TextBox>
                        </div>  
                        <div class="col-sm-8"> 
                        <label class="col-sm-2 control-label">%</label>   
                        </div>                                 
                      </div>--%>
                       <div class="form-group">
                         <h4 class="card-title col-sm-2 control-label" style="text-align:left;">Active Status </h4> 
                        <div class="col-sm-10">
                            <label class="col-sm-2 cb-checkbox cb-md">
                                <asp:CheckBox ID="chkActive" runat="server"/>
                            </label>
                        </div>                                       
                      </div>
                     </div>

                      <div class="row">
                      
                       <div class="form-group" style="margin-left:-5px;">
                           
                      </div>

                      </div>

                     

                      <div class="form-group" style="text-align:right">
                            <asp:LinkButton CssClass="btn btn-primary btn-icon loading-demo mr5 btn-shadow" ID="btnSave" runat="server">
                              <i class="fa fa-save"></i>
                              <span>Save</span>
                            </asp:LinkButton>

                            <asp:LinkButton CssClass="btn btn-warning btn-icon loading-demo mr5 btn-shadow" ID="btnBack" runat="server">
                              <i class="fa fa-rotate-left"></i>
                              <span>Cancel</span>
                            </asp:LinkButton>
                      </div>
                  </div>
                </div>
              </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="ScriptContainer" ContentPlaceHolderID="ScriptContainer" runat="server">
  <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/moment/min/moment.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/select2/dist/js/select2.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js" type="text/javascript" language="javascript"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts-->
  <script src="scripts/forms/plugins.js" type="text/javascript">

  </script> 
   <script type="text/javascript">
       function UploadInsertSmallImage() {
           $('#fulInsertSmallImage').click();
       }
       function UploadInsertBigImage() {
           $('#fulInsertBigImage').click();
       }
       
  </script>
  <!-- end initialize page scripts -->
</asp:Content>

