﻿Imports System.Data
Public Class Ads_Upload
    Inherits System.Web.UI.Page

    Dim C As New Engine.ConverterENG


    Protected Property ProductImage As Byte()
        Get
            Try
                Return Session("ProductImage")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As Byte())
            Session("ProductImage") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Files.Count = 0 Then Exit Sub

        Dim fileContent As HttpPostedFile = Request.Files(0)
        Dim b As Byte() = C.StreamToByte(fileContent.InputStream)
        ProductImage = b

    End Sub



End Class