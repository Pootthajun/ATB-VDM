﻿Imports System.Data
Imports ServerLinqDB.ConnectDB
Imports ServerLinqDB.TABLE
Imports System.Data.SqlClient
Partial Class frmSettingBusinessOwner
    Inherits System.Web.UI.Page

    Dim BL As New VDMBL
    ''Const FN_ID As Int16 = 8
    ''Const FN_Zone_ID As Int16 = 3

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property EditBusinessOwnerCode As String
        Get
            Try
                Return ViewState("businessowner_code")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("businessowner_code") = value
        End Set
    End Property

    ''Protected Property AuthorizedLevel As TSKBL.AuthorizedLevel
    ''    Get
    ''        Try
    ''            Return ViewState("AuthorizedLevel")
    ''        Catch ex As Exception
    ''            Return TSKBL.AuthorizedLevel.None
    ''        End Try
    ''    End Get
    ''    Set(value As TSKBL.AuthorizedLevel)
    ''        ViewState("AuthorizedLevel") = value
    ''    End Set
    ''End Property

    ''Public Property EditLocationCode As String
    ''    Get
    ''        Return ViewState("Loc_Code").ToString
    ''    End Get
    ''    Set(value As String)
    ''        ViewState("Loc_Code") = value
    ''    End Set
    ''End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        'If ufDt Is Nothing Then
        '    Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        'End If
        'If ufDt.Rows.Count > 0 Then
        '    Dim auDt As DataTable = BL.GetUser_Functional(FN_Zone_ID, FN_ID, ufDt)
        '    If auDt.Rows.Count = 0 Then
        '        Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        '        Exit Sub
        '    End If
        '    SetAuthorizedLevel(auDt)
        '    auDt.Dispose()
        'End If
        'ufDt.Dispose()

        If Not IsPostBack Then
            BindList()
        Else
            initFormPlugin()
        End If

    End Sub

    Protected Sub SetAuthorizedLevel(ByVal DT As DataTable)
        'AuthorizedLevel = BL.GetFunctionalAuthorized(DT, FN_ID)
    End Sub

    Private Sub initFormPlugin()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Plugin", "initFormPlugin();", True)
    End Sub

    Private Sub BindList()
        Dim DT As DataTable = BL.GetList_Business("")
        rptList.DataSource = DT
        rptList.DataBind()

        lblTotalList.Text = FormatNumber(DT.Rows.Count, 0)

        'ColEdit.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'ColDelete.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'btnAdd.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        EditBusinessOwnerCode = ""
        pnlList.Visible = True
        pnlEdit.Visible = False
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        ClearEditForm()

        pnlList.Visible = False
        pnlEdit.Visible = True
    End Sub

    Private Sub ClearEditForm()
        EditBusinessOwnerCode = ""
        txtBusinessOwnerCode.Text = ""
        txtBusinessOwnerName.Text = ""
        chkActive.Checked = False
        lblEditMode.Text = "Add"
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click

        If txtBusinessOwnerCode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Location Code');", True)
            Exit Sub
        End If
        If txtBusinessOwnerName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Location Name');", True)
            Exit Sub
        End If

        Dim DT As DataTable = BL.GetList_Business("")
        DT.DefaultView.RowFilter = "business_Code='" & txtBusinessOwnerCode.Text.Replace("'", "''") & "' AND business_code <>'" & EditBusinessOwnerCode.Replace("'", "''") & "'"
        If DT.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This Business Owner Code is already existed');", True)
            Exit Sub
        End If

        Dim trans As New ServerTransactionDB
        Dim lnq As New MsBusinessOwnerServerLinqDB
        lnq.ChkDataByBUSINESS_CODE(EditBusinessOwnerCode, trans.Trans)
        With lnq
            .BUSINESS_CODE = txtBusinessOwnerCode.Text.Replace("'", "''")
            .BUSINESS_NAME = txtBusinessOwnerName.Text.Replace("'", "''")
            .ACTIVE_STATUS = IIf(chkActive.Checked, "Y", "N")
        End With

        Dim ret As New ExecuteDataInfo
        If lnq.ID = 0 Then
            ret = lnq.InsertData(UserName, trans.Trans)
        Else
            ret = lnq.UpdateData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save success');", True)
            BindList()
        Else
            trans.RollbackTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", """") & "');", True)
        End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        BindList()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim td As HtmlTableCell = e.Item.FindControl("td")
        Dim lblBusinessOwnerCode As Label = e.Item.FindControl("lblBusinessOwnerCode")
        Dim lblBusinessOwnerName As Label = e.Item.FindControl("lblBusinessOwnerName")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim cfmDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfmDelete")

        Dim ColEdit As HtmlTableCell = e.Item.FindControl("ColEdit")
        Dim ColDelete As HtmlTableCell = e.Item.FindControl("ColDelete")

        lblBusinessOwnerCode.Text = e.Item.DataItem("business_code").ToString
        lblBusinessOwnerName.Text = e.Item.DataItem("business_name").ToString

        If Not e.Item.DataItem("active_status").ToString = "Y" Then
            td.Attributes("class") = "text-danger"
        Else
            td.Attributes("class") = "text-primary"
        End If

        lblStatus.Text = IIf(e.Item.DataItem("active_status").ToString = "Y", "Active", "Inactive")
        btnEdit.CommandArgument = e.Item.DataItem("business_code")
        btnDelete.CommandArgument = e.Item.DataItem("business_code")
        cfmDelete.ConfirmText = "Confirm to delete " & e.Item.DataItem("business_name").ToString & " ?"

        'ColEdit.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'ColDelete.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                ClearEditForm()
                Dim Business_Code As String = e.CommandArgument
                EditBusinessOwnerCode = Business_Code

                Dim DT As DataTable = BL.GetList_Business(Business_Code)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & Business_Code.Replace("'", """") & " is not found!');", True)
                    Exit Sub
                End If

                txtBusinessOwnerCode.Text = DT.Rows(0).Item("business_code").ToString
                txtBusinessOwnerName.Text = DT.Rows(0).Item("business_name").ToString

                chkActive.Checked = IIf(DT.Rows(0).Item("active_status").ToString() = "Y", True, False)
                lblEditMode.Text = "Edit"

                pnlList.Visible = False
                pnlEdit.Visible = True

            Case "Delete"
                Dim ret As String = BL.DeleteMasterBusiness(e.CommandArgument)
                If ret = "" Then
                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.Replace("'", """") & " is not found!');", True)
                End If

        End Select
    End Sub
End Class
