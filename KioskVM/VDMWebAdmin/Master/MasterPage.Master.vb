﻿Imports System
Imports System.Data
Imports ConstantsWebAdmin
Public Class MasterPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            PopulateMenu()
            DisplayUserInfo()
        End If

    End Sub

    Protected Sub DisplayUserInfo()
        If Session("UserName") Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
            Exit Sub
        End If
        lblUserName.InnerHtml = Session("UserName")
    End Sub

    Protected Sub PopulateMenu()
        ''-------------- By Authorize------------
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
            Exit Sub
        End If

        Dim DT As DataTable = ufDt.Copy
        Dim BL As New VDMBL
        '-------------- Dashboard ----------------
        mnu_Dashboard.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.Dashboard) > 0

        ''-------------- Report ----------------
        'mnu_Transaction_Log.Visible =
        'mnu_Transaction_Performance.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.ReportTransactionPerformance) > 0
        mnu_Transaction_Reports.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.ReportTransactionLog) > 0

        'mnu_Summary_byLocation.Visible =
        'mnu_Summary_byProduct.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.ReportSummaryByProduct) > 0
        mnu_Summary_Report.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.ReportSummary) > 0

        mnu_MailGroup_Report.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.MailGroupReport) > 0

        mnu_Report.Visible = mnu_Transaction_Reports.Visible Or mnu_Summary_Report.Visible Or mnu_MailGroup_Report.Visible


        ''--------------Setting Data-------------------
        mnu_Location.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.MasterLocation) > 0
        mnu_Vending_Machine.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.MasterVendingMachine) > 0
        mnu_Vending_Config.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.MasterVendingConfig) > 0
        mnu_Advertising.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.MasterAdvertising) > 0
        mnu_ProductGroup.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.MasterProductCategory) > 0
        mnu_Product.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.MasterProduct) > 0
        mnu_Promotion.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.MasterPromotion) > 0
        mnu_Vending_Srceen.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.MasterVendingScreen) > 0

        mnu_Setting_Authorize_Role.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.AuthRole) > 0
        mnu_Setting_Authorize_User.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.AuthUser) > 0
        mnu_Setting_Author.Visible = mnu_Setting_Authorize_Role.Visible Or mnu_Setting_Authorize_User.Visible

        mnu_Setting.Visible = mnu_Location.Visible Or mnu_Vending_Machine.Visible Or mnu_Vending_Config.Visible Or mnu_Advertising.Visible Or
            mnu_ProductGroup.Visible Or mnu_Product.Visible Or mnu_Promotion.Visible Or mnu_Vending_Srceen.Visible Or mnu_Setting_Author.Visible

        ''--------------Alarm & Monitoring-------------------
        mnu_Vending_Monitoring.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.VendingMachineMonitor) > 0
        mnu_Setting_Alarm.Visible = BL.GetFunctionalAuthorized(DT, WebAdminFunctionalID.SettingAlarm) > 0
        mnu_Alarm_Monitoring.Visible = mnu_Vending_Monitoring.Visible Or mnu_Setting_Alarm.Visible

        '-------------- Setting Badge Count ---------
        SetBadgeCount(DT)

    End Sub

    Protected Sub SetBadgeCount(ByVal DT As DataTable)
        Dim Count_Report As Integer = 0
        Dim CountReport_Transactioin_Report As Integer = 0
        Dim CountReport_Summary_Report As Integer = 0
        Dim Count_Setting As Integer = 0
        Dim Count_Setting_Authorize As Integer = 0
        Dim Count_Alarm_Monitoring As Integer = 0

        'CountReport_Transactioin_Report = Math.Abs(CInt(mnu_Transaction_Log.Visible)) + Math.Abs(CInt(mnu_Transaction_Performance.Visible))
        'CountReport_Summary_Report = Math.Abs(CInt(mnu_Summary_byLocation.Visible)) + Math.Abs(CInt(mnu_Summary_byProduct.Visible))
        Count_Report = Math.Abs(CInt(mnu_Transaction_Reports.Visible)) + Math.Abs(CInt(mnu_Summary_Report.Visible)) + Math.Abs(CInt(mnu_MailGroup_Report.Visible))

        Count_Alarm_Monitoring = Math.Abs(CInt(mnu_Setting_Alarm.Visible)) + Math.Abs(CInt(mnu_Vending_Monitoring.Visible))

        Count_Setting_Authorize = Math.Abs(CInt(mnu_Setting_Authorize_Role.Visible)) + Math.Abs(CInt(mnu_Setting_Authorize_User.Visible))
        Count_Setting = Count_Setting_Authorize + Math.Abs(CInt(mnu_Location.Visible)) + Math.Abs(CInt(mnu_Vending_Machine.Visible)) + Math.Abs(CInt(mnu_Vending_Config.Visible)) + Math.Abs(CInt(mnu_Advertising.Visible)) +
            Math.Abs(CInt(mnu_ProductGroup.Visible)) + Math.Abs(CInt(mnu_Product.Visible)) + Math.Abs(CInt(mnu_Promotion.Visible)) + Math.Abs(CInt(mnu_Vending_Srceen.Visible))

        badge_Report.InnerHtml = Count_Report

        badge_Alarm.InnerHtml = Count_Alarm_Monitoring
        badge_Setting.InnerHtml = Count_Setting
        badge_Setting_Authorize.InnerHtml = Count_Setting_Authorize
    End Sub

    Private Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        Session.Remove("List_User_Functional")
        Session.Remove("List_User_Location")
        Session.Remove("UserName")

        Response.Redirect("frmLogin.aspx")
    End Sub

    Private Sub btnChangePassword_Click(sender As Object, e As EventArgs) Handles btnChangePassword.Click
        Response.Redirect("frmChangePassword.aspx")
    End Sub

End Class