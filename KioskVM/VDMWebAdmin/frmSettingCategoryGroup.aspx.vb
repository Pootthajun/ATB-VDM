﻿Imports System.Data
Imports ServerLinqDB.ConnectDB
Imports ServerLinqDB.TABLE
Imports System.Data.SqlClient
Imports ConstantsWebAdmin

Partial Class frmSettingCategoryGroup
    Inherits System.Web.UI.Page
    Dim BL As New VDMBL
    ''Const FN_ID As Int16 = 8
    ''Const FN_Zone_ID As Int16 = 3

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property EditCategoryGroupCode As String
        Get
            Try
                Return ViewState("category_group_code")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("category_group_code") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If

        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.NA, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterProductCategory, UserName)
            If auDt.Rows.Count = 0 Then
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
                Exit Sub
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()

        If Not IsPostBack Then
            BindList()
        Else
            initFormPlugin()
        End If

    End Sub


    Private Sub initFormPlugin()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Plugin", "initFormPlugin();", True)
    End Sub

    Private Sub BindList()
        Dim DT As DataTable = BL.GetList_CategoryGroup("")
        rptList.DataSource = DT
        rptList.DataBind()

        lblTotalList.Text = FormatNumber(DT.Rows.Count, 0)

        'ColEdit.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'ColDelete.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'btnAdd.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        EditCategoryGroupCode = ""
        pnlList.Visible = True
        pnlEdit.Visible = False

        SetAuthorization()
    End Sub

    Private Sub SetAuthorization()
        'ตรวจสอบสิทธิ์การแก้ไขข้อมูล
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.Edit, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterProductCategory, UserName)
            If auDt.Rows.Count = 0 Then
                ColEdit.InnerText = "View"
                For i As Integer = 0 To rptList.Items.Count - 1

                    Dim btnDelete As ImageButton = rptList.Items(i).FindControl("btnDelete")
                    btnDelete.Enabled = False
                Next
                btnAdd.Visible = False


                txtCategoryCode.Enabled = False
                txtCategoryName.Enabled = False

                chkActive.Enabled = False
                btnSave.Visible = False
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        ClearEditForm()

        pnlList.Visible = False
        pnlEdit.Visible = True
    End Sub

    Private Sub ClearEditForm()
        EditCategoryGroupCode = ""
        txtCategoryCode.Text = ""
        txtCategoryName.Text = ""
        chkActive.Checked = True
        lblEditMode.Text = "Add"
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click

        If txtCategoryCode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Category Group Code');", True)
            Exit Sub
        End If
        If txtCategoryName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Category Group Name');", True)
            Exit Sub
        End If

        Dim DT As DataTable = BL.GetList_CategoryGroup("")
        DT.DefaultView.RowFilter = "category_code='" & txtCategoryCode.Text.Replace("'", "''") & "' AND category_code <>'" & EditCategoryGroupCode.Replace("'", "''") & "'"
        If DT.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This Category Group Code is already existed');", True)
            Exit Sub
        End If

        Dim trans As New ServerTransactionDB
        Dim lnq As New MsProductCategoryServerLinqDB
        lnq.ChkDataByCATEGORY_CODE(EditCategoryGroupCode, trans.Trans)
        With lnq
            .CATEGORY_CODE = txtCategoryCode.Text.Replace("'", "''")
            .CATEGORY_NAME = txtCategoryName.Text.Replace("'", "''")
            .ACTIVE_STATUS = IIf(chkActive.Checked, "Y", "N")
        End With

        Dim ret As New ExecuteDataInfo
        If lnq.ID = 0 Then
            ret = lnq.InsertData(UserName, trans.Trans)
        Else
            ret = lnq.UpdateData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save success');", True)
            BindList()
        Else
            trans.RollbackTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", """") & "');", True)
        End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        BindList()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim td As HtmlTableCell = e.Item.FindControl("td")
        Dim lblCategoryCode As Label = e.Item.FindControl("lblCategoryCode")
        Dim lblCategoryName As Label = e.Item.FindControl("lblCategoryName")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim cfmDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfmDelete")

        Dim ColEdit As HtmlTableCell = e.Item.FindControl("ColEdit")
        Dim ColDelete As HtmlTableCell = e.Item.FindControl("ColDelete")

        lblCategoryCode.Text = e.Item.DataItem("category_code").ToString
        lblCategoryName.Text = e.Item.DataItem("category_name").ToString

        If Not e.Item.DataItem("active_status").ToString = "Y" Then
            td.Attributes("class") = "text-danger"
        Else
            td.Attributes("class") = "text-primary"
        End If

        btnEdit.CommandArgument = e.Item.DataItem("category_code")
        btnDelete.CommandArgument = e.Item.DataItem("category_code")
        cfmDelete.ConfirmText = "Confirm to delete " & e.Item.DataItem("category_name").ToString & " ?"

        'ColEdit.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'ColDelete.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                ClearEditForm()
                Dim Category_Code As String = e.CommandArgument
                EditCategoryGroupCode = Category_Code

                Dim DT As DataTable = BL.GetList_CategoryGroup(Category_Code)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & Category_Code.Replace("'", """") & " is not found!');", True)
                    Exit Sub
                End If

                txtCategoryCode.Text = DT.Rows(0).Item("category_code").ToString
                txtCategoryName.Text = DT.Rows(0).Item("category_name").ToString

                chkActive.Checked = IIf(DT.Rows(0).Item("active_status").ToString() = "Y", True, False)
                lblEditMode.Text = "Edit"

                pnlList.Visible = False
                pnlEdit.Visible = True

                SetAuthorization()
            Case "Delete"
                Dim ret As String = BL.DeleteMasterCategory(e.CommandArgument)
                If ret = "" Then
                    BindList()
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.Replace("'", """") & " is not found!');", True)
                End If

        End Select
    End Sub
End Class
