﻿Imports System.Data
Imports ServerLinqDB.ConnectDB
Imports ServerLinqDB.TABLE
Imports System.Data.SqlClient
Imports VendingLinqDB.TABLE
Imports ConstantsWebAdmin

Partial Class frmVendingConfig
    Inherits System.Web.UI.Page
    Dim BL As New VDMBL

    ''Const FN_ID As Int16 = 8
    ''Const FN_Zone_ID As Int16 = 3

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property Edit_Vending_ID As Integer
        Get
            Try
                Return ViewState("Vending_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            ViewState("Vending_ID") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.NA, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterVendingConfig, UserName)
            If auDt.Rows.Count = 0 Then
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
                Exit Sub
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()

        If Not IsPostBack Then
            BindList()

            BL.SetTextIntKeypress(txtTimeOut)
            BL.SetTextIntKeypress(txtMessageTime)
            BL.SetTextIntKeypress(txtPaymentExtend)
            BL.SetTextIntKeypress(txtScreenSaver)
            BL.SetTextIntKeypress(txtShelfLong)
            BL.SetTextIntKeypress(txtSleepTimeH)
            BL.SetTextIntKeypress(txtSleepTimeM)
            BL.SetTextIntKeypress(txtSleepDuration)
        Else
            initFormPlugin()
        End If

    End Sub

    Private Sub initFormPlugin()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Plugin", "initFormPlugin();", True)
    End Sub

    Private Sub BindList()
        Dim DT As DataTable = BL.GetList_Vending_for_config(UserName, 0)
        rptList.DataSource = DT
        rptList.DataBind()

        lblTotalList.Text = FormatNumber(DT.Rows.Count, 0)

        'ColEdit.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'ColDelete.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'btnAdd.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        Edit_Vending_ID = 0
        pnlList.Visible = True
        pnlEdit.Visible = False

        SetAuthorization()
    End Sub

    Private Sub SetAuthorization()
        'ตรวจสอบสิทธิ์การแก้ไขข้อมูล
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(Authorization.Edit, WebAdminFunctionalZoneID.Setting, WebAdminFunctionalID.MasterVendingConfig, UserName)
            If auDt.Rows.Count = 0 Then
                ColEdit.InnerText = "VIEW"

                txtTimeOut.Enabled = False
                txtMessageTime.Enabled = False
                txtPaymentExtend.Enabled = False
                txtScreenSaver.Enabled = False
                txtShelfLong.Enabled = False
                txtSleepTimeH.Enabled = False
                txtSleepTimeM.Enabled = False
                txtSleepDuration.Enabled = False
                btnSave.Visible = False
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()
    End Sub

    Private Sub ClearEditForm()
        Edit_Vending_ID = 0
        txtVendingID.Text = ""
        txtNetworkDevice.Text = ""
        txtIPAddress.Text = ""
        txtMacAddress.Text = ""
        txtTimeOut.Text = ""
        txtMessageTime.Text = ""
        txtPaymentExtend.Text = ""
        txtScreenSaver.Text = ""
        txtShelfLong.Text = ""
        txtSleepTimeH.Text = ""
        txtSleepTimeM.Text = ""
        txtSleepDuration.Text = ""
        lblEditMode.Text = "Add"
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If txtTimeOut.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Time Out');", True)
            Exit Sub
        End If
        If txtMessageTime.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Message Time');", True)
            Exit Sub
        End If
        If txtPaymentExtend.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Payment Extend');", True)
            Exit Sub
        End If
        If txtScreenSaver.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Screen Saver');", True)
            Exit Sub
        End If
        If txtShelfLong.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Shelf Long');", True)
            Exit Sub
        End If
        If txtSleepTimeH.Text = "" Or txtSleepTimeM.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Sleep Time');", True)
            Exit Sub
        End If
        If txtSleepDuration.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Insert Sleep Duration');", True)
            Exit Sub
        End If

        Dim trans As New ServerTransactionDB
        trans.CreateVendingTransaction(Edit_Vending_ID)
        Dim lnq As New CfVendingSysconfigVendingLinqDB
        lnq.ChkDataByMS_VENDING_ID(Edit_Vending_ID, trans.Trans)
        With lnq
            .MS_VENDING_ID = Edit_Vending_ID
            .TIME_OUT_SEC = txtTimeOut.Text
            .SHOW_MSG_SEC = txtMessageTime.Text
            .PAYMENT_EXTEND_SEC = txtPaymentExtend.Text
            .SCREEN_SAVER_SEC = txtScreenSaver.Text
            .SHELF_LONG = txtShelfLong.Text
            .SLEEP_TIME = txtSleepTimeH.Text & ":" & txtSleepTimeM.Text
            .SLEEP_DURATION = txtSleepDuration.Text
        End With

        Dim ret As New VendingLinqDB.ConnectDB.ExecuteDataInfo
        If lnq.ID = 0 Then
            ret = lnq.InsertData(UserName, trans.Trans)
        Else
            ret = lnq.UpdateData(UserName, trans.Trans)
        End If
        If ret.IsSuccess = True Then
            trans.CommitTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save success');", True)
            BindList()
        Else
            trans.RollbackTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", """") & "');", True)
        End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        BindList()
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim td As HtmlTableCell = e.Item.FindControl("td")
        Dim lblVendingName As Label = e.Item.FindControl("lblVendingName")
        Dim lblIPAddress As Label = e.Item.FindControl("lblIPAddress")
        Dim lblMACAddress As Label = e.Item.FindControl("lblMACAddress")
        Dim lblLocation As Label = e.Item.FindControl("lblLocation")
        Dim lbldbservername As Label = e.Item.FindControl("lbldbservername")
        Dim lbldbdatabasename As Label = e.Item.FindControl("lbldbdatabasename")
        Dim lbldbuserid As Label = e.Item.FindControl("lbldbuserid")
        Dim lbldbpassword As Label = e.Item.FindControl("lbldbpassword")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        'Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        'Dim cfmDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfmDelete")

        'Dim ColEdit As HtmlTableCell = e.Item.FindControl("ColEdit")
        'Dim ColDelete As HtmlTableCell = e.Item.FindControl("ColDelete")

        lblVendingName.Text = e.Item.DataItem("com_name").ToString
        lblIPAddress.Text = e.Item.DataItem("ip_address").ToString
        lblMACAddress.Text = e.Item.DataItem("mac_address").ToString
        lblLocation.Text = e.Item.DataItem("location_name").ToString
        lbldbservername.Text = e.Item.DataItem("db_servername").ToString
        lbldbdatabasename.Text = e.Item.DataItem("db_databasename").ToString
        lbldbuserid.Text = e.Item.DataItem("db_userid").ToString
        lbldbpassword.Text = e.Item.DataItem("db_password").ToString

        If Not e.Item.DataItem("active_status").ToString = "Y" Then
            td.Attributes("class") = "text-danger"
        Else
            td.Attributes("class") = "text-primary"
        End If

        btnEdit.CommandArgument = e.Item.DataItem("id") & "|" & e.Item.DataItem("db_databasename")
        'btnDelete.CommandArgument = e.Item.DataItem("id")
        'cfmDelete.ConfirmText = "Confirm to delete " & e.Item.DataItem("ip_address").ToString & " ?"

        'ColEdit.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
        'ColDelete.Visible = AuthorizedLevel = TSKBL.AuthorizedLevel.Edit
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                ClearEditForm()
                Dim arg() As String = e.CommandArgument.ToString.Split("|")
                Dim Vending_ID As String = ""
                Dim DBName As String = ""
                If arg.Length = 2 Then
                    Vending_ID = arg(0)
                    DBName = arg(1)
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This vending is not found!');", True)
                    Exit Sub
                End If

                Edit_Vending_ID = Vending_ID
                Dim DT As DataTable = BL.GetList_VendingConfigInfo(DBName, Vending_ID)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & Vending_ID.Replace("'", """") & " is not found!');", True)
                    Exit Sub
                End If

                Dim sleepTime() As String = DT.Rows(0).Item("sleep_time").ToString.Split(":")
                Dim sleepTimeH As String = ""
                Dim sleepTimeM As String = ""
                If sleepTime.Length = 2 Then
                    sleepTimeH = sleepTime(0)
                    sleepTimeM = sleepTime(1)
                End If

                txtVendingID.Text = DT.Rows(0).Item("id").ToString
                txtNetworkDevice.Text = DT.Rows(0).Item("device_name_th").ToString
                txtIPAddress.Text = DT.Rows(0).Item("ip_address").ToString
                txtMacAddress.Text = DT.Rows(0).Item("mac_address").ToString
                txtTimeOut.Text = DT.Rows(0).Item("time_out_sec").ToString
                txtMessageTime.Text = DT.Rows(0).Item("show_msg_sec").ToString
                txtPaymentExtend.Text = DT.Rows(0).Item("payment_extend_sec").ToString
                txtScreenSaver.Text = DT.Rows(0).Item("screen_saver_sec").ToString
                txtShelfLong.Text = DT.Rows(0).Item("shelf_long").ToString
                txtSleepTimeH.Text = sleepTimeH
                txtSleepTimeM.Text = sleepTimeM
                txtSleepDuration.Text = DT.Rows(0).Item("sleep_duration").ToString

                lblEditMode.Text = "Edit"
                pnlList.Visible = False
                pnlEdit.Visible = True

                SetAuthorization()
            Case "Delete"

        End Select
    End Sub
End Class
