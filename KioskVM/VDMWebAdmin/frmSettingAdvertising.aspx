﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmSettingAdvertising.aspx.vb" Inherits="frmSettingAdvertising" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>
<%@ Register src="~/UpdateProgress.ascx" tagname="UpdateProgress" tagprefix="uc1" %>
<asp:Content ID="HeaderContainer" ContentPlaceHolderID="HeaderContainer" runat="server">
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" type="text/css" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css" />

  <!-- end page stylesheets -->

  <style type="text/css">
        .numeric
           {
               text-align:center;
               }
  </style>
</asp:Content>


<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
<uc1:UpdateProgress ID="UpdateProgress1" runat="server" />
    <div class="page-title">
        <div class="title">Setting > Advertising</div>
        <div class="sub-title"></div>
    </div>
    <asp:UpdatePanel ID="udpList" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Visible="True">
                <div class="card-header">
                    Found :
                    <asp:Label ID="lblTotalList" runat="server"></asp:Label>
                    Advertising(s)
                </div>
                <div class="card-block">
                   

                    <div class="row m-b">
                        <label class="col-sm-3 control-label"></label>
                    </div>
                    <div class="no-more-tables" >
                     <asp:Panel id="panelList" runat="server" Height="1000px" ScrollBars="Vertical">   
                     
                    <asp:Repeater ID="rptList" runat="server">
                        <ItemTemplate>
                            <div class="col-sm-12" style="border-bottom:solid; border-width:2px;  border-radius: 5px"  >
                                <%--<div class="row m-b">
                                    <label class="col-sm-3 control-label"></label>
                                </div>
                                http://localhost/VDO/8.mp4--%>
                                <div class="col-sm-5" >
                                
                                    <video id="displayVDO"  runat="server" width="250" height="200" src="" controls preload="metadata" type="video/mp4">
                                       
                                    </video>
                                </div>
                                <div class="col-sm-7" style="padding-left:30px" >
                                    <div class="row m-b">
                                        <label class="col-sm-4 control-label" style="text-align:right">Advertising Code</label>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblAdsID" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblAdsCode" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row m-b">
                                        <label class="col-sm-4 control-label" style="text-align:right">Advertising Name</label>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblAdsName" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row m-b">
                                        <label class="col-sm-4 control-label" style="text-align:right">Start Date</label>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblStartDate" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row m-b">
                                        <label class="col-sm-4 control-label" style="text-align:right">End Date</label>
                                        <div class="col-sm-8">
                                            <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row m-b">
                                     <div class="col-sm-4" style="text-align:right">
                                      <asp:LinkButton CssClass="btn btn-primary btn-icon loading-demo mr5 btn-shadow" ID="btnEdit" runat="server" CommandName="Edit">
                                          <i class="fa fa-edit m-r"></i>
                                          <span id="btnSpanEdit" runat="server" >Edit</span>
                                        </asp:LinkButton>
                                     </div>
                                      <div class="col-sm-8" style="text-align:Left">
                                      <asp:LinkButton CssClass="btn btn-danger btn-icon loading-demo mr5 btn-shadow" ID="btnDelete" runat="server" CommandName="Delete">
                                          <i class="fa fa-trash m-r"></i>
                                          <span>Delete</span>
                                        </asp:LinkButton>
                                      </div>
                                       

                                        
                                        <ajaxToolkit:ConfirmButtonExtender ID="cfmDelete" runat="server" TargetControlID="btnDelete" />
                                        <%--<button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-trash m-r"></i>Delete</button>--%>
                                     
                                    </div>
                                </div>
                             
                            </div>
                           <div class="row m-b">
                            <label class="col-sm-3 control-label"></label>
                           </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    </asp:Panel>
                    </div>
                     <div class="row m-t" style="padding-left:20px">
                        <asp:LinkButton CssClass="btn btn-primary btn-icon loading-demo mr5 btn-shadow" ID="btnAdd" runat="server">
                      <i class="fa fa-plus-circle"></i>
                      <span>Add new Advertising</span>
                        </asp:LinkButton>
                    </div>
                   
                </div>
                
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="udpEdit" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlEdit" runat="server" CssClass="card bg-white">
                <div class="card-header">
                    <asp:Label ID="lblEditMode" runat="server"></asp:Label>
                    Advertising
                </div>
                 <div class="card-block">
                <div class="row m-a-0">
                  <div class="col-lg-12 form-horizontal">
                      <h4 class="card-title">Advertising Info</h4>
                     
                     
                      <div class="col-sm-12">
                          <div class="form-group" id="divChooseVDO" runat="server" >
                              <label class="col-sm-2 control-label">VDO</label>
                              <div class="col-sm-3">
                                <%--  <asp:FileUpload ID="file1" ClientIDMode="Static" runat="server" Style="display: none;" />
                                  <a href="#" onclick="UploadVDO();return false;" class="btn btn-primary btn-icon loading-demo mr5 m-b btn-shadow col-sm-10">
                                      <i class="icon-plus"></i>
                                      <span>Choose VDO</span>
                                  </a>--%>

                                  <asp:FileUpload ID="file1" ClientIDMode="Static" runat="server" Style="display: none;" onchange="uploadFile();return false;" />
                                  <a href="#" onclick="UploadVDO();" class="btn btn-primary btn-icon loading-demo mr5 m-b btn-shadow col-sm-10">
                                      <i class="icon-plus"></i>
                                      <span>Choose VDO</span>
                                  </a>
                                  <progress id="progressBar" value="0" max="100" style="width: 300px;"></progress>
                                  <h3 id="status"  style="width:500px;"></h3>
                                  <p id="loaded_n_total"></p>

                              </div>
                              <div class="col-sm-7">
                                  <span id="lblVDOFileName" class="col-sm-8 m-b"></span>
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Advertising Code</label>
                              <div class="col-sm-10">
                                  <asp:TextBox ID="txtAdsCode" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 control-label">Advertising Name</label>
                              <div class="col-sm-10">
                                  <asp:TextBox ID="txtAdsName" runat="server" CssClass="form-control" MaxLength="255"></asp:TextBox>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 control-label">Start Date</label>
                              <div class="col-sm-2">
                                  <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="Start Date"></asp:TextBox>
                                  <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                      Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                              </div>
                              <label class="col-sm-1 control-label">End Date</label>
                              <div class="col-sm-2">
                                  <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="End Date"></asp:TextBox>
                                  <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                      Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                              </div>
                          </div>


                          <div class="form-group">
                              <h4 class="card-title col-sm-2 control-label" style="text-align: left;">Active Status </h4>
                              <div class="col-sm-10">
                                  <label class="col-sm-2 cb-checkbox cb-md">
                                      <asp:CheckBox ID="chkActive" runat="server" />
                                  </label>
                              </div>
                          </div>
                      </div>

                      <div class="form-group" style="text-align:right">
                            <asp:LinkButton CssClass="btn btn-primary btn-icon loading-demo mr5 btn-shadow" ID="btnSave" runat="server">
                              <i class="fa fa-save"></i>
                              <span>Save</span>
                            </asp:LinkButton>

                            <asp:LinkButton CssClass="btn btn-warning btn-icon loading-demo mr5 btn-shadow" ID="btnBack" runat="server">
                              <i class="fa fa-rotate-left"></i>
                              <span>Cancel</span>
                            </asp:LinkButton>
                      </div>
                  </div>
                </div>
              </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>



<asp:Content ID="ScriptContainer" ContentPlaceHolderID="ScriptContainer" runat="server">
  <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/moment/min/moment.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/select2/dist/js/select2.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js" type="text/javascript" language="javascript"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js" type="text/javascript" language="javascript"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts-->
  <script src="scripts/forms/plugins.js" type="text/javascript">

  </script> 
   <script type="text/javascript">
       function UploadVDO() {
           $('#file1').click();
       }
       function uploadFile() {
           var file = $("#file1").prop("files")[0];
           // alert(file.name+" | "+file.size+" | "+file.type);
           var formdata = new FormData();
           formdata.append("file1", file);
           var ajax = new XMLHttpRequest();
           ajax.upload.addEventListener("progress", progressHandler, false);
           ajax.addEventListener("load", completeHandler, false);
           ajax.addEventListener("error", errorHandler, false);
           ajax.addEventListener("abort", abortHandler, false);
           ajax.open("POST", "frmVDOUpload.aspx");
           ajax.send(formdata);
       }
       function progressHandler(event) {

           $("#loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
           var percent = (event.loaded / event.total) * 100;

           //$("#progressBar").value = Math.round(percent);
           document.getElementById("progressBar").value = Math.round(percent);
           document.getElementById("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
       }
       function completeHandler(event) {
           alert("Complete : ");
           $("#status").innerHTML = event.target.responseText;
           $("#progressBar").value = 0;
       }
       function errorHandler(event) {
           $("#status").innerHTML = "Upload Failed";
       }
       function abortHandler(event) {
           $("#status").innerHTML = "Upload Aborted";
       }
  </script>
  <!-- end initialize page scripts -->
</asp:Content>

