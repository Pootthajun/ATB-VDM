﻿Imports TITWebService
Imports System.Data
Partial Class frmLogin
    Inherits System.Web.UI.Page

    'akkarawatp
    'kugsu01
    Dim BL As New VDMBL
    Public Function LoginTIT(vUserName As String, vPassword As String, SysCode As String) As LoginReturnData
        Dim ret As New LoginReturnData
        Try
            Dim ws As New TITWebService.TITWebService
            ws.Url = ConfigurationManager.AppSettings("TITWebService.TITWebService").ToString
            ws.Timeout = 10000
            ret = ws.LoginTIT(vUserName, vPassword, SysCode, "WebAdmin", HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.Browser.Browser, HttpContext.Current.Request.Browser.Version, HttpContext.Current.Request.Url.AbsoluteUri)
        Catch ex As Exception
            ret = New LoginReturnData
        End Try


        Return ret
    End Function

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Dim ret As New LoginReturnData
        ret = LoginTIT(txtUserName.Text, txtPassword.Text, "ATB-VDM")

        If ret.LoginStatus = True Then
            Session("UserName") = ret.LoginUsername
            SetUserFunctionalInfo(ret.LoginUsername)

            If ret.ForceChangePwd = "Y" Then
                'ให้ Redirec ไปหน้าจอสำหรับเปลี่ยนรหัสผ่านก่อน
                Session("ForceChangePW") = "Y"
                Response.Redirect("frmChangePassword.aspx")
            Else
                Session("ForceChangePW") = "N"
                Response.Redirect("frmDashboard.aspx")
            End If

        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, Guid.NewGuid().ToString(), "alert('invalid username or password. please try agian.')", True)
        End If
    End Sub

    Private Function GetUserLocationID(dt As DataTable) As String
        Dim ret As String = ""
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                If ret = "" Then
                    ret = dr("id")
                Else
                    ret += "," & dr("id")
                End If
            Next
        End If
        Return ret
    End Function


    Private Sub SetUserFunctionalInfo(Username As String)
        Try
            If Username.Trim <> "" Then
                Session("List_User_Functional") = BL.GetList_User_Functional(0, 0, 0, Username)
                Session("List_User_Location") = BL.GetList_User_Location(Username, "Y")
                Session("List_User_LocationID") = GetUserLocationID(Session("List_User_Location"))
            Else
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
            End If
        Catch ex As Exception
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End Try

    End Sub

End Class
