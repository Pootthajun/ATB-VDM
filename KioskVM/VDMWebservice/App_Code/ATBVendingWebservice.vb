﻿Imports System
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports ServerLinqDB.ConnectDB
Imports ServerLinqDB.TABLE
Imports VendingLinqDB.TABLE
Imports System.Reflection

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
'<System.Web.Script.Services.ScriptService()>
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class ATBVendingWebservice
    Inherits System.Web.Services.WebService

    '<WebMethod()> _
    'Public Function HelloWorld() As String
    '    Return "Hello World"
    'End Function


    <WebMethod()>
    Public Function LoginTIT(vUserName As String, vPassword As String, ModuleName As String) As TITAdminWebService.LoginReturnData
        Dim ret As New TITAdminWebService.LoginReturnData
        Try
            Dim ws As New TITAdminWebService.TITWebService
            ws.Url = ConfigurationManager.AppSettings("TITAdminWebService.TITWebService").ToString
            ws.Timeout = 10000
            ret = ws.LoginTIT(vUserName, vPassword, "ATB-VDM", ModuleName, HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.Browser.Browser, HttpContext.Current.Request.Browser.Version, HttpContext.Current.Request.Url.AbsoluteUri)
        Catch ex As Exception
            ret = New TITAdminWebService.LoginReturnData
        End Try

        Return ret
    End Function

    <WebMethod()>
    Public Function GetKioskStaffConsoleAuthorize(Username As String, MsVendingID As Long) As DataTable
        Dim ret As New DataTable
        Try
            Dim sql As String = "select distinct rf.id,rf.ms_role_id,rf.ms_functional_id, rf.ms_authorization_id, f.ms_functional_zone_id, "
            sql += " r.role_name, f.functional_name, a.authorization_name"
            sql += " from ms_user_role ur "
            sql += " inner join ms_role r On r.id=ur.ms_role_id "
            sql += " inner join ms_role_functional rf on r.id=rf.ms_role_id "
            sql += " inner join ms_functional f On f.id=rf.ms_functional_id "
            sql += " inner join ms_authorization a on a.id=rf.ms_authorization_id "
            sql += " inner join ms_user_location ul On ur.username=ul.username "
            sql += " inner join ms_vending_machine v on v.ms_location_id=ul.ms_location_id "
            sql += " where f.ms_functional_zone_id=5"
            sql += " and r.active_status='Y'"
            sql += " and ul.username=@_USERNAME and v.id=@_VENDING_ID"

            Dim p(2) As SqlParameter
            p(0) = ServerDB.SetText("@_USERNAME", Username)
            p(1) = ServerDB.SetBigInt("@_VENDING_ID", MsVendingID)

            ret = ServerDB.ExecuteTable(sql, p)

        Catch ex As Exception
            ret = New DataTable
        End Try
        ret.TableName = "GetKioskStaffConsoleAuthorize"
        Return ret
    End Function

#Region "Get Vending Machine Master Data"
    <WebMethod()>
    Public Function GetVendingSystemInfo(VendingID As Long, MacAddress As String, IPAddress As String, ComputerName As String) As DataTable
        Dim dt As New DataTable
        Try
            'หา Kiosk Configuration โดยใช้ MacAddress และคิวรี่ข้อมูลจาก Server
            Dim sql As String = "select v.id, l.id location_id, l.location_code,l.location_name "
            sql += " from MS_VENDING_MACHINE v"
            sql += " inner join MS_LOCATION l on l.id=v.ms_location_id "
            sql += " where v.id = @_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetText("@_VENDING_ID", VendingID)

            dt = ServerDB.ExecuteTable(sql, Nothing, p)
            If dt.Rows.Count > 0 Then
                Dim dr As DataRow = dt.Rows(0)
                Dim kLnq As New ServerLinqDB.TABLE.MsVendingMachineServerLinqDB
                kLnq.GetDataByPK(Convert.ToInt64(dr("id")), Nothing)
                If kLnq.ID > 0 Then
                    kLnq.MAC_ADDRESS = MacAddress
                    kLnq.IP_ADDRESS = IPAddress
                    kLnq.COM_NAME = ComputerName

                    Dim trans As New ServerTransactionDB
                    Dim re As ExecuteDataInfo = kLnq.UpdateData(ComputerName, trans.Trans)
                    If re.IsSuccess = True Then
                        trans.CommitTransaction()
                    Else
                        trans.RollbackTransaction()
                    End If
                End If
                kLnq = Nothing
            End If
        Catch ex As Exception
            dt = New DataTable
        End Try
        dt.TableName = "GetVendingSystemInfo"
        Return dt
    End Function

    <WebMethod()>
    Public Function GetAppScreenList() As DataTable
        Dim dt As New DataTable
        Try

            Dim lnq As New MsAppScreenServerLinqDB
            dt = lnq.GetDataList("", "", Nothing, Nothing)
            lnq = Nothing
        Catch ex As Exception

        End Try

        dt.TableName = "GetAppScreenList"
        Return dt
    End Function

    <WebMethod()>
    Public Function GetLangMasterList(VendingID As Long) As DataTable
        Dim dt As New DataTable
        Try
            Dim sql As String = " select ac.ms_app_screen_id, ac.Control_Name, " & vbNewLine
            sql += " isnull(vsc.TH_Display,ac.TH_Display) TH_Display ,isnull(vsc.EN_Display,ac.EN_Display) EN_Display, " & vbNewLine
            sql += " isnull(vsc.CH_Display, ac.CH_Display) CH_Display, isnull(vsc.JP_Display,ac.JP_Display) JP_Display " & vbNewLine
            sql += " From MS_APP_SCREEN_CONTROL ac " & vbNewLine
            sql += " Left Join (" & vbNewLine
            sql += "    Select  vsc.TH_Display,vsc.EN_Display,vsc.CH_Display,vsc.JP_Display, " & vbNewLine
            sql += "    vsc.ms_app_screen_control_id " & vbNewLine
            sql += "    From MS_VENDING_SCREEN_CONTROL vsc " & vbNewLine
            sql += "    inner Join MS_VENDING_SCREEN vs on vs.id=vsc.ms_vending_screen_id " & vbNewLine
            sql += "    where vs.ms_vending_id=@_VENDING_ID " & vbNewLine
            sql += " ) vsc on vsc.ms_app_screen_control_id=ac.id"
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetBigInt("@_VENDING_ID", VendingID)

            dt = ServerDB.ExecuteTable(sql, p)
        Catch ex As Exception

        End Try
        dt.TableName = "GetSetLangMasterList"
        Return dt
    End Function

    <WebMethod()>
    Public Function GetAlarmMasterList() As DataTable
        Dim dt As New DataTable
        Try
            Dim sql As String = "select alarm_problem, alarm_code,eng_desc, sms_message"
            sql += " from MS_MASTER_MONITORING_ALARM "
            dt = ServerDB.ExecuteTable(sql)
        Catch ex As Exception

        End Try
        dt.TableName = "GetAlarmMasterList"
        Return dt
    End Function

    <WebMethod()>
    Public Function GetMasterDeviceInfoList() As DataTable
        Dim dt As New DataTable
        Try
            Dim sql As String = "SELECT dt.id AS device_type_id, dt.device_type_name_th, dt.device_type_name_en, dt.movement_direction, dt.active_status AS type_active_status, "
            sql += " d.id AS device_id, d.device_name_th, d.device_name_en, d.unit_value, d.active_status AS device_active_status "
            sql += " FROM  MS_DEVICE_TYPE AS dt "
            sql += " INNER JOIN MS_DEVICE AS d ON d.ms_device_type_id = dt.id "

            dt = ServerDB.ExecuteTable(sql)
        Catch ex As Exception

        End Try
        dt.TableName = "GetMasterDeviceInfoList"
        Return dt
    End Function

    <WebMethod()>
    Public Function GeMasterDeviceStatus() As DataTable
        Dim dt As New DataTable
        Try
            Dim Sql As String = " select id, status_name, is_problem "
            Sql += " from ms_device_status "
            dt = ServerDB.ExecuteTable(Sql)
        Catch ex As Exception

        End Try
        dt.TableName = "GeMasterDeviceStatus"
        Return dt
    End Function

    <WebMethod()>
    Public Function GetMasterProductInfoList(VendingID As Long) As DataTable
        Dim dt As New DataTable
        Try
            Dim sql As String = "select p.id ms_product_id,  p.product_code,p.product_name_th,p.product_name_en, p.product_name_jp, p.product_name_ch, "
            sql += " p.product_desc_th, p.product_desc_en, p.product_desc_jp, p.product_desc_ch, "
            sql += " p.product_image_big,p.product_image_small, p.cost,p.price, "
            sql += " p.ms_product_category_id, pc.category_code,pc.category_name, vp.profit_sharing "
            sql += " from MS_PRODUCT p "
            sql += " inner join MS_PRODUCT_CATEGORY pc on pc.id=p.ms_product_category_id "
            sql += " inner join MS_VENDING_PRODUCT vp on p.id=vp.ms_product_id "
            sql += " where vp.ms_vending_id = @_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetBigInt("@_VENDING_ID", VendingID)

            dt = ServerDB.ExecuteTable(sql, p)
        Catch ex As Exception

        End Try
        dt.TableName = "GetMasterProductInfoList"
        Return dt
    End Function

    <WebMethod()>
    Public Function GetMasterProductWarrantyList(StrMsProductID As String) As DataTable
        Dim dt As New DataTable
        Try
            Dim sql As String = "select pw.id ms_product_warranty_id, pw.ms_product_id, pw.warranty_th, pw.warranty_en, pw.warranty_jp, pw.warranty_ch"
            sql += " from MS_PRODUCT_WARRANTY pw"
            sql += " where pw.ms_product_id in (" & StrMsProductID & ")"

            dt = ServerDB.ExecuteTable(sql)
        Catch ex As Exception

        End Try
        dt.TableName = "GetMasterProductWarrantyList"
        Return dt
    End Function

    <WebMethod()>
    Public Function GetMasterProductPromotion(VendingID As Long) As DataTable
        Dim dt As New DataTable
        Try
            Dim sql As String = "select pp.discount_percent, pp.ms_product_id "
            sql += " from MS_VENDING_PRODUCT_PROMOTION pp"
            sql += " inner join MS_PRODUCT p on p.id=pp.ms_product_id "
            sql += " where pp.ms_vending_id=@_VENDING_ID "
            sql += " and getdate() between pp.start_date and pp.end_date "

            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetBigInt("@_VENDING_ID", VendingID)

            dt = ServerDB.ExecuteTable(sql, p)
        Catch ex As Exception

        End Try
        dt.TableName = "GetMasterProductPromotion"
        Return dt
    End Function


    <WebMethod()>
    Public Function GetConfigSlipFieldList() As DataTable
        Dim dt As New DataTable
        Try
            Dim lnq As New CfSlipFieldListServerLinqDB
            dt = lnq.GetDataList("", "", Nothing, Nothing)
            lnq = Nothing

        Catch ex As Exception

        End Try
        dt.TableName = "GetConfigSlipFieldList"
        Return dt
    End Function
#End Region

#Region "SyncDataVDMServer"
    <WebMethod()>
    Public Function SyncVDMToServer_CfVendingSysConfig(data As CfVendingSysconfigVendingLinqDB, vending_id As Long) As Boolean
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim lnq As New CfVendingSysconfigVendingLinqDB
            lnq.ChkDataByMS_VENDING_ID(vending_id, vTrans.Trans)
            With lnq
                .SCREEN_LAYOUT = data.SCREEN_LAYOUT
                .SCREEN_SAVER_SEC = data.SCREEN_SAVER_SEC
                .TIME_OUT_SEC = data.TIME_OUT_SEC
                .SHOW_MSG_SEC = data.SHOW_MSG_SEC
                .PAYMENT_EXTEND_SEC = data.PAYMENT_EXTEND_SEC
                .SHELF_LONG = data.SHELF_LONG
                .SLEEP_TIME = data.SLEEP_TIME
                .SLEEP_DURATION = data.SLEEP_DURATION
                .VDM_WEBSERVICE_URL = data.VDM_WEBSERVICE_URL
                .ALARM_WEBSERVICE_URL = data.ALARM_WEBSERVICE_URL
                .MOTOR_PARK_POSITION = data.MOTOR_PARK_POSITION
                .MOTOR_ORIGIN_POSITION = data.MOTOR_ORIGIN_POSITION
                .MOTOR_DELIVERY_POSITION = data.MOTOR_DELIVERY_POSITION
                .ADS_VDO_PATH = data.ADS_VDO_PATH
                .INTERVAL_SYNC_TO_VENDING_MIN = data.INTERVAL_SYNC_TO_VENDING_MIN
                .INTERVAL_SYNC_TO_SERVER_MIN = data.INTERVAL_SYNC_TO_SERVER_MIN
                '.INTERVAL_SYNC_TRANSACTION_MIN = data.INTERVAL_SYNC_TRANSACTION_MIN
                '.INTERVAL_SYNC_MASTER_MIN = data.INTERVAL_SYNC_MASTER_MIN
                '.INTERVAL_SYNC_LOG_MIN = data.INTERVAL_SYNC_LOG_MIN
                .SYNC_TO_SERVER = "Y"
            End With

            Dim ret As New VendingLinqDB.ConnectDB.ExecuteDataInfo
            If lnq.ID > 0 Then
                ret = lnq.UpdateData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
            End If

            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return False
            End If

            vTrans.CommitTransaction()
            Return True
        Catch ex As Exception
            vTrans.RollbackTransaction()
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            Return False
        End Try

    End Function

    <WebMethod()>
    Public Function GetServerCfVendingSysConfig(vending_id As Long) As DataTable
        Dim dt As New DataTable
        Dim vTrans As New ServerTransactionDB(vending_id)
        Try

            Dim sql As String = "select * from cf_vending_sysconfig where sync_to_vending='N' and ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetBigInt("@_VENDING_ID", vending_id)
            dt = ServerDB.ExecuteTable(sql, vTrans.Trans, p)
            vTrans.CommitTransaction()
        Catch ex As Exception
            vTrans.RollbackTransaction()
            dt = New DataTable
        End Try

        dt.TableName = "GetCfVendingSysConfig"
        Return dt
    End Function

    <WebMethod()>
    Public Function UpdateSyncToVDM_CfVendingSysConfig(vending_id As Long) As Boolean
        Dim ret As New ExecuteDataInfo
        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim sql As String = "update cf_vending_sysconfig set sync_to_vending='Y' where ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetBigInt("@_VENDING_ID", vending_id)
            ret = ServerDB.ExecuteNonQuery(sql, vTrans.Trans, p)
            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return ret.IsSuccess
            End If

            vTrans.CommitTransaction()
        Catch ex As Exception
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            vTrans.RollbackTransaction()
        End Try
        Return ret.IsSuccess
    End Function

    <WebMethod()>
    Public Function SyncVDMToServer_MsProductShelf(data As MsProductShelfVendingLinqDB, vending_id As Long) As Boolean
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim lnq As New MsProductShelfVendingLinqDB
            lnq.ChkDataByMS_VENDING_ID_SHELF_ID(vending_id, data.SHELF_ID, vTrans.Trans)
            With lnq
                .MS_VENDING_ID = data.MS_VENDING_ID
                .MS_PRODUCT_ID = data.MS_PRODUCT_ID
                .SHELF_ID = data.SHELF_ID
                .PRODUCT_QTY = data.PRODUCT_QTY
                .SYNC_TO_SERVER = "Y"
            End With

            Dim ret As New VendingLinqDB.ConnectDB.ExecuteDataInfo
            If lnq.ID > 0 Then
                ret = lnq.UpdateData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
            Else
                ret = lnq.InsertData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
            End If

            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return False
            End If

            vTrans.CommitTransaction()
            Return True
        Catch ex As Exception
            vTrans.RollbackTransaction()
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            Return False
        End Try

    End Function

    <WebMethod()>
    Public Function SyncVDMToServer_MsVendingShelf(data As MsVendingShelfVendingLinqDB, vending_id As Long) As Boolean
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim lnq As New MsVendingShelfVendingLinqDB
            lnq.ChkDataByMS_VENDING_ID_SHELF_ID(vending_id, data.SHELF_ID, vTrans.Trans)
            With lnq
                .MS_VENDING_ID = data.MS_VENDING_ID
                .SHELF_ID = data.SHELF_ID
                .POSITION_ROW = data.POSITION_ROW
                .POSITION_COLUMN = data.POSITION_COLUMN
                .POSITION_X = data.POSITION_X
                .POSITION_Y = data.POSITION_Y
                '.REF_NUMBER = data.REF_NUMBER
                .WIDTH = data.WIDTH
                .SYNC_TO_SERVER = "Y"
            End With

            Dim ret As New VendingLinqDB.ConnectDB.ExecuteDataInfo
            If lnq.ID > 0 Then
                ret = lnq.UpdateData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
            Else
                ret = lnq.InsertData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
            End If

            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return False
            End If

            vTrans.CommitTransaction()
            Return True
        Catch ex As Exception
            vTrans.RollbackTransaction()
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            Return False
        End Try

    End Function

    <WebMethod()>
    Public Function SyncVDMToServer_LogVendingActivity(data As TbLogVendingActivityVendingLinqDB, vending_id As Long) As Boolean
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim lnq As New TbLogVendingActivityVendingLinqDB
            With lnq
                .MS_VENDING_ID = data.MS_VENDING_ID
                .TRANS_DATE = data.TRANS_DATE
                .SALES_TRANS_NO = data.SALES_TRANS_NO
                .STAFF_CONSOLE_TRANS_NO = data.STAFF_CONSOLE_TRANS_NO
                .MS_APP_SCREEN_ID = data.MS_APP_SCREEN_ID
                .LOG_DESC = data.LOG_DESC
                .IS_PROBLEM = data.IS_PROBLEM
                .SYNC_TO_SERVER = "Y"
            End With

            Dim ret As New VendingLinqDB.ConnectDB.ExecuteDataInfo
            ret = lnq.InsertData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)

            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return False
            End If

            vTrans.CommitTransaction()
            Return True
        Catch ex As Exception
            vTrans.RollbackTransaction()
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            Return False
        End Try

    End Function

    <WebMethod()>
    Public Function SyncVDMToServer_LogVendingAgent(data As TbLogVendingAgentVendingLinqDB, vending_id As Long) As Boolean
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim lnq As New TbLogVendingAgentVendingLinqDB
            With lnq
                .MS_VENDING_ID = data.MS_VENDING_ID
                .LOG_TYPE = data.LOG_TYPE
                .LOG_MESSAGE = data.LOG_MESSAGE
                .CLASS_NAME = data.CLASS_NAME
                .FUNCTION_NAME = data.FUNCTION_NAME
                .LINE_NO = data.LINE_NO
                .SYNC_TO_SERVER = "Y"
            End With

            Dim ret As New VendingLinqDB.ConnectDB.ExecuteDataInfo
            ret = lnq.InsertData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)

            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return False
            End If

            vTrans.CommitTransaction()
            Return True
        Catch ex As Exception
            vTrans.RollbackTransaction()
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            Return False
        End Try

    End Function

    <WebMethod()>
    Public Function SyncVDMToServer_LogVendingFillMoney(data As TbLogVendingFillMoneyVendingLinqDB, vending_id As Long) As Boolean
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim lnq As New TbLogVendingFillMoneyVendingLinqDB
            With lnq
                .MS_VENDING_ID = data.MS_VENDING_ID
                .COIN_IN_MONEY = data.COIN_IN_MONEY
                .BANKNOTE_IN_MONEY = data.BANKNOTE_IN_MONEY
                .CHECKOUT_RECEIVE_MONEY = data.CHECKOUT_RECEIVE_MONEY
                If Not data.CHECKOUT_DATETIME Is Nothing Then
                    .CHECKOUT_DATETIME = data.CHECKOUT_DATETIME
                End If

                .CHANGE1_REMAIN = data.CHANGE1_REMAIN
                .CHANGE2_REMAIN = data.CHANGE2_REMAIN
                .CHANGE5_REMAIN = data.CHANGE5_REMAIN
                .CHANGE10_REMAIN = data.CHANGE10_REMAIN
                .CHANGE20_REMAIN = data.CHANGE20_REMAIN
                .CHANGE50_REMAIN = data.CHANGE50_REMAIN
                .CHANGE100_REMAIN = data.CHANGE100_REMAIN
                .CHANGE500_REMAIN = data.CHANGE500_REMAIN
                .CHECKIN_CHANGE_MONEY = data.CHECKIN_CHANGE_MONEY

                If Not data.CHECKIN_DATETIME Is Nothing Then
                    .CHECKIN_DATETIME = data.CHECKIN_DATETIME
                End If

                .TOTAL_MONEY_REMAIN = data.TOTAL_MONEY_REMAIN
                .IS_CONFIRM = data.IS_CONFIRM

                If Not data.CONFIRM_CANCEL_DATETIME = Nothing Then
                    .CONFIRM_CANCEL_DATETIME = data.CONFIRM_CANCEL_DATETIME
                End If

                .CHANGE1_QTY = data.CHANGE1_QTY
                .CHANGE2_QTY = data.CHANGE2_QTY
                .CHANGE5_QTY = data.CHANGE5_QTY
                .CHANGE10_QTY = data.CHANGE10_QTY
                .CHANGE20_QTY = data.CHANGE20_QTY
                .CHANGE50_QTY = data.CHANGE50_QTY
                .CHANGE100_QTY = data.CHANGE100_QTY
                .CHANGE500_QTY = data.CHANGE500_QTY
                .SYNC_TO_SERVER = "Y"
            End With

            Dim ret As New VendingLinqDB.ConnectDB.ExecuteDataInfo
            ret = lnq.InsertData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)

            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return False
            End If

            vTrans.CommitTransaction()
            Return True
        Catch ex As Exception
            vTrans.RollbackTransaction()
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            Return False
        End Try

    End Function

    <WebMethod()>
    Public Function SyncVDMToServer_TbProductMovement(data As TbProductMovementVendingLinqDB, vending_id As Long) As Boolean
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim lnq As New TbProductMovementVendingLinqDB
            With lnq
                .MS_VENDING_ID = data.MS_VENDING_ID
                .MOVEMENT_DATE = data.MOVEMENT_DATE
                .MS_PRODUCT_ID = data.MS_PRODUCT_ID
                .SHELF_ID = data.SHELF_ID
                .MOVEMENT_TYPE = data.MOVEMENT_TYPE
                .PRODUCT_QTY = data.PRODUCT_QTY
                .SYNC_TO_SERVER = "Y"
            End With

            Dim ret As New VendingLinqDB.ConnectDB.ExecuteDataInfo
            ret = lnq.InsertData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)

            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return False
            End If

            vTrans.CommitTransaction()
            Return True
        Catch ex As Exception
            vTrans.RollbackTransaction()
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            Return False
        End Try

    End Function

    <WebMethod()>
    Public Function SyncVDMToServer_TbStaffConsoleTransaction(data As TbStaffConsoleTransactionVendingLinqDB, vending_id As Long) As Boolean

        'Public Function SyncVDMToServer_TbStaffConsoleTransaction(vending_id As Long, MS_VENDING_ID As String,
        '                                                          TRANS_NO As String, TRANS_START_TIME As String, TRANS_END_TIME As String, LOGIN_USERNAME As String,
        '                                                          LOGIN_FIRST_NAME As String, LOGIN_LAST_NAME As String, LOGIN_COMPANY_NAME As String, LOGIN_BY As String, MS_APP_STEP_ID As String) As Boolean
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim lnq As New TbStaffConsoleTransactionVendingLinqDB
            With lnq
                .MS_VENDING_ID = data.MS_VENDING_ID
                .TRANS_NO = data.TRANS_NO
                .TRANS_START_TIME = data.TRANS_START_TIME
                .TRANS_END_TIME = data.TRANS_END_TIME
                .LOGIN_USERNAME = data.LOGIN_USERNAME
                .LOGIN_FIRST_NAME = data.LOGIN_FIRST_NAME
                .LOGIN_LAST_NAME = data.LOGIN_LAST_NAME
                .LOGIN_COMPANY_NAME = data.LOGIN_COMPANY_NAME
                .LOGIN_BY = data.LOGIN_BY
                If Not data.MS_APP_STEP_ID Is Nothing Then
                    .MS_APP_STEP_ID = data.MS_APP_STEP_ID
                End If

                .SYNC_TO_SERVER = "Y"
            End With

            Dim ret As New VendingLinqDB.ConnectDB.ExecuteDataInfo
            ret = lnq.InsertData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)

            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return False
            End If

            vTrans.CommitTransaction()
            Return True
        Catch ex As Exception
            vTrans.RollbackTransaction()
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            Return False
        End Try

    End Function

    <WebMethod()>
    Public Function SyncVDMToServer_TbVendingSaleTransaction(data As TbVendingSaleTransactionVendingLinqDB, vending_id As Long) As Boolean
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim lnq As New TbVendingSaleTransactionVendingLinqDB
            With lnq
                .MS_VENDING_ID = data.MS_VENDING_ID
                .TRANS_NO = data.TRANS_NO
                If Not data.TRANS_START_TIME = Nothing Then
                    .TRANS_START_TIME = data.TRANS_START_TIME
                End If

                If Not data.TRANS_END_TIME Is Nothing Then
                    .TRANS_END_TIME = data.TRANS_END_TIME
                End If

                .MS_VENDING_ID = data.MS_VENDING_ID
                .MS_PRODUCT_ID = data.MS_PRODUCT_ID
                .SHELF_ID = data.SHELF_ID
                .PRODUCT_COST = data.PRODUCT_COST
                .PRODUCT_PRICE = data.PRODUCT_PRICE
                .PRODUCT_DISCOUNT_PERCENT = data.PRODUCT_DISCOUNT_PERCENT
                .PRODUCT_NETPRICE = data.PRODUCT_NETPRICE
                .PRODUCT_PROFIT_SHARING = data.PRODUCT_PROFIT_SHARING
                If Not data.PAID_SUCCESS_TIME Is Nothing Then
                    .PAID_SUCCESS_TIME = data.PAID_SUCCESS_TIME
                End If

                .RECEIVE_COIN1 = data.RECEIVE_COIN1
                .RECEIVE_COIN2 = data.RECEIVE_COIN2
                .RECEIVE_COIN5 = data.RECEIVE_COIN5
                .RECEIVE_COIN10 = data.RECEIVE_COIN10
                .RECEIVE_BANKNOTE20 = data.RECEIVE_BANKNOTE20
                .RECEIVE_BANKNOTE50 = data.RECEIVE_BANKNOTE50
                .RECEIVE_BANKNOTE100 = data.RECEIVE_BANKNOTE100
                .RECEIVE_BANKNOTE500 = data.RECEIVE_BANKNOTE500
                .RECEIVE_BANKNOTE1000 = data.RECEIVE_BANKNOTE1000
                .CHANGE_COIN1 = data.CHANGE_COIN1
                .CHANGE_COIN2 = data.CHANGE_COIN2
                .CHANGE_COIN5 = data.CHANGE_COIN5
                .CHANGE_COIN10 = data.CHANGE_COIN10
                .CHANGE_BANKNOTE20 = data.CHANGE_BANKNOTE20
                .CHANGE_BANKNOTE50 = data.CHANGE_BANKNOTE50
                .CHANGE_BANKNOTE100 = data.CHANGE_BANKNOTE100
                .CHANGE_BANKNOTE500 = data.CHANGE_BANKNOTE500
                .TRANS_STATUS = data.TRANS_STATUS
                .MS_APP_STEP_ID = data.MS_APP_STEP_ID
                .SYNC_TO_SERVER = "Y"
            End With

            Dim ret As New VendingLinqDB.ConnectDB.ExecuteDataInfo
            ret = lnq.InsertData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)

            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return False
            End If

            vTrans.CommitTransaction()
            Return True
        Catch ex As Exception
            vTrans.RollbackTransaction()
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            Return False
        End Try

    End Function

    <WebMethod()>
    Public Function SyncVDMToServer_MsVendingDevice(data As MsVendingDeviceVendingLinqDB, vending_id As Long) As Boolean
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim lnq As New MsVendingDeviceVendingLinqDB
            lnq.ChkDataByMS_DEVICE_ID_MS_VENDING_ID(data.MS_DEVICE_ID, vending_id, vTrans.Trans)
            With lnq
                .MS_VENDING_ID = data.MS_VENDING_ID
                .MS_DEVICE_ID = data.MS_DEVICE_ID

                If Not data.MAX_QTY = Nothing Then
                    .MAX_QTY = data.MAX_QTY
                End If

                If Not data.WARNING_QTY = Nothing Then
                    .WARNING_QTY = data.WARNING_QTY
                End If

                If Not data.CRITICAL_QTY = Nothing Then
                    .CRITICAL_QTY = data.CRITICAL_QTY
                End If

                If Not data.CURRENT_QTY = Nothing Then
                    .CURRENT_QTY = data.CURRENT_QTY
                End If

                If Not data.CURRENT_MONEY Is Nothing Then
                    .CURRENT_MONEY = data.CURRENT_MONEY
                End If

                If Not data.MS_DEVICE_STATUS_ID = Nothing Then
                    .MS_DEVICE_STATUS_ID = data.MS_DEVICE_STATUS_ID
                End If

                .COMPORT_VID = data.COMPORT_VID
                .DRIVER_NAME1 = data.DRIVER_NAME1
                .DRIVER_NAME2 = data.DRIVER_NAME2
                .SYNC_TO_SERVER = "Y"
            End With

            Dim ret As New VendingLinqDB.ConnectDB.ExecuteDataInfo
            If lnq.ID > 0 Then
                ret = lnq.UpdateData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
            End If

            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return False
            End If

            vTrans.CommitTransaction()
            Return True
        Catch ex As Exception
            vTrans.RollbackTransaction()
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            Return False
        End Try

    End Function

    <WebMethod()>
    Public Function GetServerMsVendingDevice(vending_id As Long) As DataTable
        Dim ret As New DataTable
        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim sql As String = "select * from ms_vending_device where sync_to_vending='N' and ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetBigInt("@_VENDING_ID", vending_id)
            ret = ServerDB.ExecuteTable(sql, vTrans.trans, p)
            vTrans.CommitTransaction()
        Catch ex As Exception
            vTrans.RollbackTransaction()
            ret = New DataTable
        End Try
        ret.TableName = "GetMsVendingDevice"
        Return ret
    End Function

    <WebMethod()>
    Public Function UpdateSyncToVDM_MsVendingDevice(vending_id As Long, device_id As Long) As Boolean
        Dim ret As New ExecuteDataInfo
        Dim vTrans As New ServerTransactionDB(vending_id)
        Try
            Dim sql As String = "update ms_vending_device set sync_to_vending='Y' where ms_vending_id=@_VENDING_ID and ms_device_id=@_DEVICE_ID"
            Dim p(2) As SqlParameter
            p(0) = ServerDB.SetBigInt("@_VENDING_ID", vending_id)
            p(1) = ServerDB.SetBigInt("@_DEVICE_ID", device_id)
            ret = ServerDB.ExecuteNonQuery(sql, vTrans.Trans, p)
            If ret.IsSuccess = False Then
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return ret.IsSuccess
            End If

            vTrans.CommitTransaction()
        Catch ex As Exception
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            vTrans.RollbackTransaction()
        End Try
        Return ret.IsSuccess
    End Function

    <WebMethod()>
    Public Function GetVDOInfoByVending(vending_id As Long) As DataTable
        Dim ret As New DataTable
        Dim Trans As New ServerTransactionDB()
        Try
            Dim sql As String = "select id,advertise_code,advertise_name,ads_file_bype,ads_file_ext,ads_mime_type,"
            sql &= " Convert(varchar(10),start_date,103) start_date,convert(varchar(10),end_date,103) end_date,active_status "
            sql &= " From  ms_advertise where id in (select ms_advertise_id from ms_vending_advertise where ms_vending_id=@_VENDING_ID)"
            sql &= " And (start_date <= Getdate() And end_date >= getdate()) And active_status ='Y'"
            Dim p(1) As SqlParameter
            p(0) = ServerDB.SetBigInt("@_VENDING_ID", vending_id)
            ret = ServerDB.ExecuteTable(sql, Trans.Trans, p)
            Trans.CommitTransaction()
        Catch ex As Exception
            Trans.RollbackTransaction()
            ret = New DataTable
        End Try
        ret.TableName = "GetVDOInfoByVending"
        Return ret

    End Function

    <WebMethod()>
    Public Function UpdateSyncToVDM_MsAdvertise(vending_id As Long, advertise_id As Long) As Boolean
        Dim ret As New ExecuteDataInfo
        Dim Trans As New ServerTransactionDB()
        Try
            Dim sql As String = "update ms_vending_advertise set sync_to_vending='Y' where ms_vending_id=@_VENDING_ID and ms_advertise_id=@_ADVERTISE_ID"
            Dim p(2) As SqlParameter
            p(0) = ServerDB.SetBigInt("@_VENDING_ID", vending_id)
            p(1) = ServerDB.SetBigInt("@_ADVERTISE_ID", advertise_id)
            ret = ServerDB.ExecuteNonQuery(sql, Trans.Trans, p)
            If ret.IsSuccess = False Then
                Trans.RollbackTransaction()
                Engine.LogFileENG.CreateServerErrorLogAgent(ret.ErrorMessage)
                Return ret.IsSuccess
            End If

            Trans.CommitTransaction()
        Catch ex As Exception
            Engine.LogFileENG.CreateServerExceptionLogAgent(ex.Message, ex.StackTrace)
            Trans.RollbackTransaction()
        End Try
        Return ret.IsSuccess
    End Function

    <WebMethod()>
    Public Function GetServerConfig() As CfServerSysconfigServerLinqDB
        Return Engine.ServerENG.GetServerConfig
    End Function

#End Region

End Class