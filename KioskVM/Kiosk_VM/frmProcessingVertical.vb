﻿

Public Class frmProcessingVertical

    Private Sub frmProductDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.ControlBox = False
        Me.BackColor = bgColor
        Me.BackgroundImage = Image.FromFile(Application.StartupPath & "\Background\bgProcessing.jpg")


    End Sub

    Private Sub frmProcessingVertical_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.Processing
        InsertLogSalesActivity(vTrans.TransNo, "แสดงหน้า Processing ", VendingConfig.SelectForm, False)

        Try
            '1. สั่ง Steper Motor ให้เคลื่อนไปยังตำแหน่งของ Shelf และตรวจสอบตำแหน่งที่ถูกต้องเมื่อหยุด
            InsertLogSalesActivity(vTrans.TransNo, "สั่ง Motor แกน ให้เคลื่อนไปยังตำแหน่งของ Shelf Row=" & vTrans.ShelfRow & " Column=" & vTrans.ShelfColumn & "  X=" & vTrans.PositionX & " Y=" & vTrans.PositionY, VendingConfig.SelectForm, False)

            '2. สั่ง Motor ของ Carriage ให้หมุนเพื่อเลือนสินค้าที่อยู่ใน Shelf
            InsertLogSalesActivity(vTrans.TransNo, "สั่ง Motor ของ Carriage ให้หมุนเพื่อเลือนสินค้าที่อยู่ใน ShelfRow=" & vTrans.ShelfRow & " Column=" & vTrans.ShelfColumn, VendingConfig.SelectForm, False)

            '3. เปิด Sensor ของ Carriage เพื่อรอรับค่าให้ Step ไม่เจอ - เจอ - ไม่เจอ
            InsertLogSalesActivity(vTrans.TransNo, "เปิด Sensor ของ Carriage เพื่อรอรับค่า", VendingConfig.SelectForm, False)
            Do
                'รอ Carriage Sensor จนกว่าจะได้ค่่า CarriageSensorFlag=true
            Loop Until CarriageSensorFlag = True


            '4. สั่งปิด Sensor และ หยุด Motor ของ Carriage
            InsertLogSalesActivity(vTrans.TransNo, "สั่งปิด Sensor และ หยุด Motor ของ Carriage", VendingConfig.SelectForm, False)

            '5. สั่งให้ Steper Motor ให้เคลื่อนไปยังช่องรับสินค้า และตรวจสอบตำแหน่งที่ถูกต้องเมื่อหยุด
            InsertLogSalesActivity(vTrans.TransNo, "สั่งให้ Steper Motor ให้เคลื่อนไปยังช่องรับสินค้า", VendingConfig.SelectForm, False)

            '6. สั่ง Motor ให้เปิดช่องรับสินค้า
            InsertLogSalesActivity(vTrans.TransNo, "สั่ง Motor ให้เปิดช่องรับสินค้า", VendingConfig.SelectForm, False)

            '7. สั่ง Sensor ช่องรับสินค้าให้ทำงาน เพื่อรอรับค่า
            InsertLogSalesActivity(vTrans.TransNo, "สั่ง Sensor ช่องรับสินค้าให้ทำงาน เพื่อรอรับค่า", VendingConfig.SelectForm, False)
            Do
                'รอ Delivery Sensor จนกว่าจะได้ค่่า DeliverySensorFlag=true
            Loop Until DeliverySensorFlag = True
            InsertLogSalesActivity(vTrans.TransNo, "หยุด Motor ช่องรับสินค้า", VendingConfig.SelectForm, False)

            '8. สั่ง Motor ของ Carriage ให้หมุนเพื่อเลือนสินค้าออก  ตรงจุดนี้ไม่มี Sensor เพื่อตรวจสอบ ให้ใช้เวลาจับเวลาเอาเป็น x วินาที
            InsertLogSalesActivity(vTrans.TransNo, "สั่ง Motor ของ Carriage ให้หมุนเพื่อเลือนสินค้าออก เป็นเวลา x วินาที", VendingConfig.SelectForm, False)

            '9. สั่งหยุด Motor ของ Carriage
            InsertLogSalesActivity(vTrans.TransNo, "สั่งหยุด Motor ของ Carriage", VendingConfig.SelectForm, False)

            '10. สั่งปิดช่องรับสินค้า 
            InsertLogSalesActivity(vTrans.TransNo, "สั่งปิดช่องรับสินค้า", VendingConfig.SelectForm, False)
            Do
                'รอ Delivery Sensor จนกว่าจะได้ค่่า DeliverySensorFlag=true
            Loop Until DeliverySensorFlag = True
            InsertLogSalesActivity(vTrans.TransNo, "หยุด Motor ช่องรับสินค้า", VendingConfig.SelectForm, False)

            'พิมพ์ Slip และทอนเงินให้ลูกค้า อยู่ในหน้า Complete
            frmCompleteVertical.Show()
            frmCompleteVertical.PaymentCompletePrintSlip()
            Application.DoEvents()
            Me.Close()
        Catch ex As Exception

        End Try
    End Sub

#Region "Sensor Process Data"
    Dim CarriageSensorFlag As Boolean = False
    Private Sub CarriageSensorReceiveData(DataReceive As String)
        'Carriage Sensor 
        'CarriageSensorFlag=true
    End Sub

    Dim DeliverySensorFlag As Boolean = False
    Private Sub DeliverySensorReceiveData(DataReceive As String)
        'Delivery Sensor 
        'DeliverySensorFlag=true
    End Sub
#End Region

    Private Sub TimerTimeOut_Tick(sender As Object, e As EventArgs) Handles TimerTimeOut.Tick
        TimerTimeOut.Enabled = False
        frmCompleteVertical.Show()
        frmCompleteVertical.PaymentCompletePrintSlip()
        Application.DoEvents()
        Me.Close()
    End Sub
End Class