﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ucSetupSlip
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmtControlMenuStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuAddCaption = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAddImage = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblLocationX = New System.Windows.Forms.Label()
        Me.lblLocationY = New System.Windows.Forms.Label()
        Me.cmtTextMenuStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuTextAlign = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTextAlignLeft = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTextAlignCenter = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTextAlignRight = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFont = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBringToFront = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSendToBack = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRemoveText = New System.Windows.Forms.ToolStripMenuItem()
        Me.fntDialog = New System.Windows.Forms.FontDialog()
        Me.lblPosInnerX = New System.Windows.Forms.Label()
        Me.lblPosInnerY = New System.Windows.Forms.Label()
        Me.cmtPicMenuStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuRemovePictureBox = New System.Windows.Forms.ToolStripMenuItem()
        Me.dgvFieldList = New System.Windows.Forms.DataGridView()
        Me.colParmName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblCaptionFieldList = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnPrintTest = New System.Windows.Forms.Button()
        Me.cbbPrinter = New System.Windows.Forms.ComboBox()
        Me.cmtControlMenuStrip.SuspendLayout()
        Me.cmtTextMenuStrip.SuspendLayout()
        Me.cmtPicMenuStrip.SuspendLayout()
        CType(Me.dgvFieldList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.AllowDrop = True
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.ContextMenuStrip = Me.cmtControlMenuStrip
        Me.Panel1.Location = New System.Drawing.Point(3, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(366, 541)
        Me.Panel1.TabIndex = 0
        '
        'cmtControlMenuStrip
        '
        Me.cmtControlMenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAddCaption, Me.mnuAddImage})
        Me.cmtControlMenuStrip.Name = "cmtControlMenuStrip"
        Me.cmtControlMenuStrip.ShowImageMargin = False
        Me.cmtControlMenuStrip.Size = New System.Drawing.Size(117, 48)
        '
        'mnuAddCaption
        '
        Me.mnuAddCaption.Name = "mnuAddCaption"
        Me.mnuAddCaption.Size = New System.Drawing.Size(116, 22)
        Me.mnuAddCaption.Text = "Add Caption"
        '
        'mnuAddImage
        '
        Me.mnuAddImage.Name = "mnuAddImage"
        Me.mnuAddImage.Size = New System.Drawing.Size(116, 22)
        Me.mnuAddImage.Text = "Add Image"
        '
        'lblLocationX
        '
        Me.lblLocationX.AutoSize = True
        Me.lblLocationX.Location = New System.Drawing.Point(372, 472)
        Me.lblLocationX.Name = "lblLocationX"
        Me.lblLocationX.Size = New System.Drawing.Size(39, 13)
        Me.lblLocationX.TabIndex = 1
        Me.lblLocationX.Text = "Label1"
        Me.lblLocationX.Visible = False
        '
        'lblLocationY
        '
        Me.lblLocationY.AutoSize = True
        Me.lblLocationY.Location = New System.Drawing.Point(417, 472)
        Me.lblLocationY.Name = "lblLocationY"
        Me.lblLocationY.Size = New System.Drawing.Size(39, 13)
        Me.lblLocationY.TabIndex = 3
        Me.lblLocationY.Text = "Label1"
        Me.lblLocationY.Visible = False
        '
        'cmtTextMenuStrip
        '
        Me.cmtTextMenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuTextAlign, Me.mnuFont, Me.mnuBringToFront, Me.mnuSendToBack, Me.mnuRemoveText})
        Me.cmtTextMenuStrip.Name = "cmtTextMenuStrip"
        Me.cmtTextMenuStrip.ShowImageMargin = False
        Me.cmtTextMenuStrip.Size = New System.Drawing.Size(123, 114)
        '
        'mnuTextAlign
        '
        Me.mnuTextAlign.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuTextAlignLeft, Me.mnuTextAlignCenter, Me.mnuTextAlignRight})
        Me.mnuTextAlign.Name = "mnuTextAlign"
        Me.mnuTextAlign.Size = New System.Drawing.Size(122, 22)
        Me.mnuTextAlign.Text = "Text Align"
        '
        'mnuTextAlignLeft
        '
        Me.mnuTextAlignLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.mnuTextAlignLeft.Name = "mnuTextAlignLeft"
        Me.mnuTextAlignLeft.Size = New System.Drawing.Size(109, 22)
        Me.mnuTextAlignLeft.Text = "Left"
        '
        'mnuTextAlignCenter
        '
        Me.mnuTextAlignCenter.Name = "mnuTextAlignCenter"
        Me.mnuTextAlignCenter.Size = New System.Drawing.Size(109, 22)
        Me.mnuTextAlignCenter.Text = "Center"
        '
        'mnuTextAlignRight
        '
        Me.mnuTextAlignRight.Name = "mnuTextAlignRight"
        Me.mnuTextAlignRight.Size = New System.Drawing.Size(109, 22)
        Me.mnuTextAlignRight.Text = "Right"
        '
        'mnuFont
        '
        Me.mnuFont.Name = "mnuFont"
        Me.mnuFont.Size = New System.Drawing.Size(122, 22)
        Me.mnuFont.Text = "Font"
        '
        'mnuBringToFront
        '
        Me.mnuBringToFront.Name = "mnuBringToFront"
        Me.mnuBringToFront.Size = New System.Drawing.Size(122, 22)
        Me.mnuBringToFront.Text = "Bring to Front"
        '
        'mnuSendToBack
        '
        Me.mnuSendToBack.Name = "mnuSendToBack"
        Me.mnuSendToBack.Size = New System.Drawing.Size(122, 22)
        Me.mnuSendToBack.Text = "Send to Back"
        '
        'mnuRemoveText
        '
        Me.mnuRemoveText.Name = "mnuRemoveText"
        Me.mnuRemoveText.Size = New System.Drawing.Size(122, 22)
        Me.mnuRemoveText.Text = "Remove"
        '
        'lblPosInnerX
        '
        Me.lblPosInnerX.AutoSize = True
        Me.lblPosInnerX.Location = New System.Drawing.Point(372, 485)
        Me.lblPosInnerX.Name = "lblPosInnerX"
        Me.lblPosInnerX.Size = New System.Drawing.Size(39, 13)
        Me.lblPosInnerX.TabIndex = 5
        Me.lblPosInnerX.Text = "Label1"
        Me.lblPosInnerX.Visible = False
        '
        'lblPosInnerY
        '
        Me.lblPosInnerY.AutoSize = True
        Me.lblPosInnerY.Location = New System.Drawing.Point(417, 485)
        Me.lblPosInnerY.Name = "lblPosInnerY"
        Me.lblPosInnerY.Size = New System.Drawing.Size(39, 13)
        Me.lblPosInnerY.TabIndex = 6
        Me.lblPosInnerY.Text = "Label1"
        Me.lblPosInnerY.Visible = False
        '
        'cmtPicMenuStrip
        '
        Me.cmtPicMenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuRemovePictureBox})
        Me.cmtPicMenuStrip.Name = "cmtPicMenuStrip"
        Me.cmtPicMenuStrip.Size = New System.Drawing.Size(118, 26)
        '
        'mnuRemovePictureBox
        '
        Me.mnuRemovePictureBox.Name = "mnuRemovePictureBox"
        Me.mnuRemovePictureBox.Size = New System.Drawing.Size(117, 22)
        Me.mnuRemovePictureBox.Text = "Remove"
        '
        'dgvFieldList
        '
        Me.dgvFieldList.AllowUserToAddRows = False
        Me.dgvFieldList.AllowUserToDeleteRows = False
        Me.dgvFieldList.AllowUserToResizeColumns = False
        Me.dgvFieldList.AllowUserToResizeRows = False
        Me.dgvFieldList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFieldList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colParmName})
        Me.dgvFieldList.Location = New System.Drawing.Point(371, 18)
        Me.dgvFieldList.MultiSelect = False
        Me.dgvFieldList.Name = "dgvFieldList"
        Me.dgvFieldList.RowHeadersVisible = False
        Me.dgvFieldList.Size = New System.Drawing.Size(287, 422)
        Me.dgvFieldList.TabIndex = 7
        '
        'colParmName
        '
        Me.colParmName.DataPropertyName = "parm_name"
        Me.colParmName.HeaderText = "Parameter name"
        Me.colParmName.Name = "colParmName"
        Me.colParmName.ReadOnly = True
        Me.colParmName.Width = 200
        '
        'lblCaptionFieldList
        '
        Me.lblCaptionFieldList.AutoSize = True
        Me.lblCaptionFieldList.Location = New System.Drawing.Point(375, 2)
        Me.lblCaptionFieldList.Name = "lblCaptionFieldList"
        Me.lblCaptionFieldList.Size = New System.Drawing.Size(48, 13)
        Me.lblCaptionFieldList.TabIndex = 8
        Me.lblCaptionFieldList.Text = "Field List"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(375, 446)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnPrintTest
        '
        Me.btnPrintTest.Location = New System.Drawing.Point(574, 518)
        Me.btnPrintTest.Name = "btnPrintTest"
        Me.btnPrintTest.Size = New System.Drawing.Size(75, 23)
        Me.btnPrintTest.TabIndex = 10
        Me.btnPrintTest.Text = "Print Test"
        Me.btnPrintTest.UseVisualStyleBackColor = True
        '
        'cbbPrinter
        '
        Me.cbbPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbPrinter.FormattingEnabled = True
        Me.cbbPrinter.Location = New System.Drawing.Point(375, 520)
        Me.cbbPrinter.Name = "cbbPrinter"
        Me.cbbPrinter.Size = New System.Drawing.Size(193, 21)
        Me.cbbPrinter.TabIndex = 11
        '
        'ucSetupSlip
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.cbbPrinter)
        Me.Controls.Add(Me.btnPrintTest)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.lblCaptionFieldList)
        Me.Controls.Add(Me.dgvFieldList)
        Me.Controls.Add(Me.lblPosInnerY)
        Me.Controls.Add(Me.lblPosInnerX)
        Me.Controls.Add(Me.lblLocationY)
        Me.Controls.Add(Me.lblLocationX)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "ucSetupSlip"
        Me.Size = New System.Drawing.Size(662, 546)
        Me.cmtControlMenuStrip.ResumeLayout(False)
        Me.cmtTextMenuStrip.ResumeLayout(False)
        Me.cmtPicMenuStrip.ResumeLayout(False)
        CType(Me.dgvFieldList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblLocationX As Label
    Friend WithEvents cmtControlMenuStrip As ContextMenuStrip
    Friend WithEvents mnuAddCaption As ToolStripMenuItem
    Friend WithEvents mnuAddImage As ToolStripMenuItem
    Friend WithEvents lblLocationY As Label
    Friend WithEvents cmtTextMenuStrip As ContextMenuStrip
    Friend WithEvents mnuTextAlign As ToolStripMenuItem
    Friend WithEvents mnuTextAlignLeft As ToolStripMenuItem
    Friend WithEvents mnuTextAlignCenter As ToolStripMenuItem
    Friend WithEvents mnuTextAlignRight As ToolStripMenuItem
    Friend WithEvents mnuFont As ToolStripMenuItem
    Friend WithEvents mnuBringToFront As ToolStripMenuItem
    Friend WithEvents mnuSendToBack As ToolStripMenuItem
    Friend WithEvents fntDialog As FontDialog
    Friend WithEvents lblPosInnerX As Label
    Friend WithEvents lblPosInnerY As Label
    Friend WithEvents mnuRemoveText As ToolStripMenuItem
    Friend WithEvents cmtPicMenuStrip As ContextMenuStrip
    Friend WithEvents mnuRemovePictureBox As ToolStripMenuItem
    Friend WithEvents dgvFieldList As DataGridView
    Friend WithEvents lblCaptionFieldList As Label
    Friend WithEvents colParmName As DataGridViewTextBoxColumn
    Friend WithEvents btnSave As Button
    Friend WithEvents btnPrintTest As Button
    Friend WithEvents cbbPrinter As ComboBox
End Class
