﻿
Imports Kiosk_VM.Data
Imports Kiosk_VM.Data.VendingConfigData
Imports VendingLinqDB.ConnectDB
Imports Engine

Public Class frmSC_LogIn
    Private Sub frmSC_LogIn_Load(sender As Object, e As EventArgs) Handles Me.Load
        VendingConfig.SelectForm = VendingConfigData.VendingForm.StaffConsoleLogin
        Me.ControlBox = False
    End Sub

    Private Sub frmSC_LogIn_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        'Me.WindowState = FormWindowState.Maximized

        StaffConsole = New StaffConsoleLogonData(VendingData.VendingID)

        lblHeader.Text = "Staff Console"
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "แสดงหน้าจอ Login เข้า Staf Console", VendingConfig.SelectForm, False)
        txtUsername.Focus()
    End Sub

    Private Sub lblCancel_Click(sender As Object, e As EventArgs) Handles lblCancel.Click, btnCancel.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "กดปุ่มยกเลิก", VendingConfig.SelectForm, False)
        Me.Close()
    End Sub

    Private Sub txtUsername_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtUsername.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtPassword.Focus()
        End If
    End Sub

    Private Sub txtPassword_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnLogin_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles lblLogin.Click, btnLogin.Click

        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "กดปุ่ม Login", VendingConfig.SelectForm, False)

        If txtUsername.Text.Trim = "" Then
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Please enter Username", VendingConfig.SelectForm, False)
            ShowDialogErrorMessageSC("Please enter Username")
            Exit Sub
        End If
        If txtPassword.Text.Trim = "" Then
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Please enter Password", VendingConfig.SelectForm, False)
            ShowDialogErrorMessageSC("Please enter Password")
            Exit Sub
        End If

        Dim WS As New VDMWebService.ATBVendingWebservice
        WS.Url = VendingConfig.WebserviceVDMURL
        WS.Timeout = 10000

        Dim SSOLogin As VDMWebService.LoginReturnData = LogInSSO(txtUsername.Text, txtPassword.Text, WS)
        If SSOLogin.LoginStatus = True Then
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "เข้าสู่ระบบสำเร็จ ตรวจสอบสิทธิ์การใช้งาน", VendingConfig.SelectForm, False)
            Dim aDt As DataTable = WS.GetKioskStaffConsoleAuthorize(txtUsername.Text, VendingData.VendingID)
            If aDt.Rows.Count = 0 Then
                InsertLogStaffConsoleActivity(StaffConsole.TransNo, "ผู้ใช้ไม่มีสิทธิ์การใช้งาน Staff Console", VendingConfig.SelectForm, False)
                ShowDialogErrorMessageSC("You not have authorization")
                Exit Sub
            End If

            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Create Staff Console Transaction", VendingConfig.SelectForm, False)
            Dim ret As ExecuteDataInfo = CreateNewStaffConsoleTransaction(txtUsername.Text, SSOLogin.LoginFirstName, SSOLogin.LoginLastName, SSOLogin.LoginCompanyName, "1")
            If ret.IsSuccess = False Then
                InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Cannot Create Staff Console Transection", VendingConfig.SelectForm, True)
                ShowDialogErrorMessageSC("Cannot Create Staff Console Transection")
                Exit Sub
            End If
            StaffConsole.AuthorizeInfo = aDt

            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "ตรวจสอบสถานะการทำงานของอุปกรณ์", VendingConfig.SelectForm, False)
            UpdateAllDeviceStatusByComPort()
            UpdateAllDeviceStatusByUsbPort()

            Me.Hide()
            Me.Close()

            Dim f As New frmSC_StockAndHardware
            f.ShowDialog(Me)
        Else
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "เข้าสู่ระบบไม่สำเร็จ " & SSOLogin.ErrorMessage, VendingConfig.SelectForm, True)
            ShowDialogErrorMessageSC(SSOLogin.ErrorMessage)
        End If
        WS.Dispose()

    End Sub


End Class