﻿Imports Engine

Public Class frmPaymentVertical
    Dim Timeout As Integer = VendingConfig.TimeOutSec
    Dim TimeOutCheckTime As DateTime = DateTime.Now

    Private Delegate Sub myDelegate(data As String)
    Private myForm As myDelegate
    Private PaidAmt As myDelegate

    Dim PayTimeOut As Boolean = False

    Private Sub frmPaymentVertical_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.ControlBox = False
        Me.BackColor = bgColor
        'CheckForIllegalCrossThreadCalls = False
        Me.BackgroundImage = Image.FromFile(Application.StartupPath & "\Background\bgPayment.jpg")


        myForm = AddressOf OpenFormProcessing
        PaidAmt = AddressOf PaidLabel
    End Sub

    Private Sub frmPaymentVertical_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.Payment
        InsertLogSalesActivity(vTrans.TransNo, "แสดงหน้า Payment จำนวนเงินที่ต้องชำระ " & vTrans.NetPrice & " บาท", VendingConfig.SelectForm, False)

        SetChildFormLanguage()
        TimeOutCheckTime = DateTime.Now
        TimerTimeOut.Enabled = True

        Dim theard As New System.Threading.Thread(Sub() BackgroundPayment())
        theard.Start()
    End Sub

    Private Sub BackgroundPayment()
        ''Update Current Status ลง DB
        'UpdateAllDeviceStatusByComPort()
        'UpdateAllDeviceStatusByUsbPort()

        ''เช็คการเชื่อมต่ออุปกรณ์ และ Stock QTY
        'Dim Msg As String = ""
        'Msg = CheckStockAndStatusAllDevice()
        'If Msg <> "" Then
        '    Dim MsAppStepID As KioskLockerStep = KioskLockerStep.DepositPayment_CheckHardwareStatus

        '    If ServiceID = Data.ConstantsData.TransactionType.DepositBelonging Then
        '        UpdateDepositStatus(Customer.ServiceTransactionID, DepositTransactionData.TransactionStatus.Problem, MsAppStepID)
        '        InsertErrorLog(Msg, Customer.DepositTransNo, "", "", KioskConfig.SelectForm, MsAppStepID)
        '        InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, MsAppStepID, "ไม่สามารถใช้งานได้", True)
        '    ElseIf ServiceID = Data.ConstantsData.TransactionType.PickupBelonging Then

        '        UpdatePickupStatus(Pickup.PickupTransactionID, PickupTransactionData.TransactionStatus.Problem, MsAppStepID)
        '        InsertErrorLog(Msg, "", Pickup.TransactionNo, "", KioskConfig.SelectForm, MsAppStepID)
        '        InsertLogTransactionActivity("", Pickup.TransactionNo, "", KioskConfig.SelectForm, MsAppStepID, "ไม่สามารถใช้งานได้", True)
        '    End If

        '    ShowFormError("", "", KioskConfig.SelectForm, MsAppStepID, True)
        'End If

        '''Check Hard Ware Complete
        'If ServiceID = Data.ConstantsData.TransactionType.DepositBelonging Then
        '    InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositPayment_CheckHardwareStatus, "", False)
        'ElseIf ServiceID = Data.ConstantsData.TransactionType.PickupBelonging Then
        '    InsertLogTransactionActivity("", Pickup.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupPayment_CheckHardwareStatus, "", False)
        'End If
    End Sub

    Private Sub frmPaymentVertical_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        BanknoteIn.DisableDeviceCashIn()
        CoinIn.DisableDeviceCoinIn()

        RemoveHandler BanknoteIn.ReceiveEvent, AddressOf BanknoteInDataReceived
        RemoveHandler CoinIn.ReceiveEvent, AddressOf CoinInDataReceived
    End Sub

    Private Sub OpenFormProcessing(data As String)
        BanknoteIn.DisableDeviceCashIn()
        CoinIn.DisableDeviceCoinIn()
        RemoveHandler BanknoteIn.ReceiveEvent, AddressOf BanknoteInDataReceived
        RemoveHandler CoinIn.ReceiveEvent, AddressOf CoinInDataReceived

        Application.DoEvents()
        frmProcessingVertical.Show()
        Me.Close()
    End Sub

    Public Sub StartPaymentInitialDevice()
        'เปิดการใช้งานเครื่องรับเหรียญ และรับแบงค์
        InsertLogSalesActivity(vTrans.TransNo, "เปิดการใช้งานเครื่องรับธนบัตรและเครื่องรับเหรียญ", VendingConfig.SelectForm, False)
        If BanknoteIn.ConnectBanknoteInDevice(VendingConfig.BanknoteInComport) = True Then
            BanknoteIn.EnableDeviceCashIn()
            RemoveHandler BanknoteIn.ReceiveEvent, AddressOf BanknoteInDataReceived
            AddHandler BanknoteIn.ReceiveEvent, AddressOf BanknoteInDataReceived
            InsertLogSalesActivity(vTrans.TransNo, "เครื่องรับธนบัตรพร้อมใช้งาน", VendingConfig.SelectForm, False)

            If CoinIn.ConnectCoinInDevice(VendingConfig.CoinInComport) = True Then
                InsertLogSalesActivity(vTrans.TransNo, "เครื่องรับเหรียญพร้อมใช้งาน", VendingConfig.SelectForm, False)
                CoinIn.EnableDeviceCoinIn()
                RemoveHandler CoinIn.ReceiveEvent, AddressOf CoinInDataReceived
                AddHandler CoinIn.ReceiveEvent, AddressOf CoinInDataReceived
            Else
                InsertLogSalesActivity(vTrans.TransNo, "เครื่องรับเหรียญไม่พร้อมใช้งาน", VendingConfig.SelectForm, True)
                vTrans.TransStatus = Data.SalesTransactionData.TransactionStatus.Problem
                UpdateSaleTransaction(vTrans)

                ShowFormError("", "เครื่องรับเหรียญไม่พร้อมใช้งาน", VendingConfig.SelectForm)
            End If
            TimerTimeOut.Enabled = True
        Else
            InsertLogSalesActivity(vTrans.TransNo, "เครื่องรับธนบัตรไม่พร้อมใช้งาน", VendingConfig.SelectForm, True)
            vTrans.TransStatus = Data.SalesTransactionData.TransactionStatus.Problem
            UpdateSaleTransaction(vTrans)
            ShowFormError("", "เครื่องรับธนบัตรไม่พร้อมใช้งาน", VendingConfig.SelectForm)
        End If
    End Sub

    Public Sub SetProductInformation()
        Dim NetPrice As Integer = 0
        Dim PaidAmt As Integer = 0

        ProductMasterList.DefaultView.RowFilter = "ms_product_id = '" & vTrans.ProductID & "'"
        If ProductMasterList.DefaultView.Count > 0 Then
            Dim pdDrv As DataRowView = ProductMasterList.DefaultView(0)
            lblNetPrice.Text = vTrans.NetPrice

            Select Case VendingConfig.Language
                Case Data.ConstantsData.VendingLanguage.Thai
                    lblProductName.Text = pdDrv("product_name_th")
                    lblNetPrice.Text += " บาท"
                Case Data.ConstantsData.VendingLanguage.English
                    lblProductName.Text = pdDrv("product_name_en")
                    lblNetPrice.Text += THB_EN
                Case Data.ConstantsData.VendingLanguage.China
                    lblProductName.Text = pdDrv("product_name_ch")
                    lblNetPrice.Text += THB_CH
                Case Data.ConstantsData.VendingLanguage.Japan
                    lblProductName.Text = pdDrv("product_name_jp")
                    lblNetPrice.Text += THB_JP
            End Select

            Using pBytes As New IO.MemoryStream(CType(pdDrv("product_image_big"), Byte()))
                Dim pbImg As Image = Image.FromStream(pBytes)
                pbProduct.Image = pbImg
                pbProduct.SizeMode = PictureBoxSizeMode.StretchImage
            End Using
        End If
        ProductMasterList.DefaultView.RowFilter = ""

    End Sub

    Private Sub TimerTimeOut_Tick(sender As Object, e As EventArgs) Handles TimerTimeOut.Tick

        If TimeOutCheckTime.AddSeconds(Timeout) <= DateTime.Now Then
            InsertLogSalesActivity(vTrans.TransNo, "ลูกค้าไม่ชำระเงินภายในเวลาที่กำหนด แสดงหน้าจอต่อเวลา", VendingConfig.SelectForm, False)

            Application.DoEvents()
            TimerTimeOut.Enabled = False

            BanknoteIn.DisableDeviceCashIn()
            CoinIn.DisableDeviceCoinIn()
            Dim f As New frmDialog_TimeOut
            If Plexiglass(f, Me) = DialogResult.Yes Then
                InsertLogSalesActivity(vTrans.TransNo, "ต่อเวลาชำระเงิน " & VendingConfig.PaymentExtendSec & " วินาที", VendingConfig.SelectForm, False)

                BanknoteIn.EnableDeviceCashIn()
                CoinIn.EnableDeviceCoinIn()
                TimeOutCheckTime = DateTime.Now
                TimerTimeOut.Enabled = True
            Else
                If vTrans.PaidAmount > 0 Then
                    Dim RtnMsg As String = "คืนเงิน " & vTrans.PaidAmount & " บาท"
                    ReturnMoney(vTrans.PaidAmount, vTrans)
                End If

                vTrans.TransStatus = Data.SalesTransactionData.TransactionStatus.Cancel
                UpdateSaleTransaction(vTrans)
                InsertLogSalesActivity(vTrans.TransNo, "ลูกค้าไม่ต่อเวลา ยกเลิกรายการและกลับหน้าแรก", VendingConfig.SelectForm, False)

                frmHomeVertical.Show()
                Me.Close()
            End If
        End If
    End Sub

    Private Sub PaidLabel(data As String)
        lblPaidAmt.Text = data
        Application.DoEvents()
    End Sub


    Private Sub BanknoteInDataReceived(ByVal ReceiveData As String)
        If InStr(ReceiveData, "ReceiveCash") > 0 Then
            TimeOutCheckTime = DateTime.Now
            ReceiveData = ReceiveData.Replace("ReceiveCash", "").Trim()

            vTrans.PaidAmount = vTrans.PaidAmount + CInt(ReceiveData)
            vTrans.ChangeAmount = vTrans.PaidAmount - vTrans.NetPrice

            Me.Invoke(PaidAmt, vTrans.PaidAmount.ToString)  'Update หน้าจอก่อนแล้วค่อยไปทำอย่างอื่น

            Select Case ReceiveData
                Case 20
                    vTrans.ReceiveBankNote20 += 1
                Case 50
                    vTrans.ReceiveBankNote50 += 1
                Case 100
                    vTrans.ReceiveBankNote100 += 1
                Case 500
                    vTrans.ReceiveBankNote500 += 1
                Case 1000
                    vTrans.ReceiveBankNote100 += 1
            End Select

            InsertLogSalesActivity(vTrans.TransNo, "รับธนบัตร " & ReceiveData & " บาท", VendingConfig.SelectForm, False)
            UpdateKioskCurrentQty(mdlDeviceInfoENG.DeviceID.BankNoteIn, 1, ReceiveData, False)

            If vTrans.PaidAmount >= vTrans.NetPrice Then
                InsertLogSalesActivity(vTrans.TransNo, "ราคาสินค้า " & vTrans.NetPrice & " บาท จำนวนเงินที่จ่่าย " & vTrans.PaidAmount & " จำนวนเงินทอน " & vTrans.ChangeAmount, VendingConfig.SelectForm, False)
                vTrans.PaidTime = DateTime.Now
                Me.Invoke(myForm, "")
            End If
        End If
    End Sub

    Private Sub CoinInDataReceived(ByVal ReceiveData As String)
        If InStr(ReceiveData, "ReceiveCoin") > 0 Then
            TimeOutCheckTime = DateTime.Now
            ReceiveData = ReceiveData.Replace("ReceiveCoin", "").Trim()

            vTrans.PaidAmount = vTrans.PaidAmount + CInt(ReceiveData)
            vTrans.ChangeAmount = vTrans.PaidAmount - vTrans.NetPrice

            Me.Invoke(PaidAmt, vTrans.PaidAmount.ToString)  'อัพเดทหน้าจอก่อนแล้วค่อยทำอย่างอื่น

            Select Case ReceiveData
                Case 1
                    vTrans.ReceiveCoin1 += 1
                Case 2
                    vTrans.ReceiveCoin2 += 1
                Case 5
                    vTrans.ReceiveCoin5 += 1
                Case 10
                    vTrans.ReceiveCoin10 += 1
            End Select

            InsertLogSalesActivity(vTrans.TransNo, "รับเหรียญ " & ReceiveData & " บาท", VendingConfig.SelectForm, False)
            UpdateKioskCurrentQty(mdlDeviceInfoENG.DeviceID.CoinIn, 1, ReceiveData, False)

            If vTrans.PaidAmount >= vTrans.NetPrice Then
                InsertLogSalesActivity(vTrans.TransNo, "ราคาสินค้า " & vTrans.NetPrice & " บาท จำนวนเงินที่จ่่าย " & vTrans.PaidAmount & " จำนวนเงินทอน " & vTrans.ChangeAmount, VendingConfig.SelectForm, False)

                vTrans.PaidTime = DateTime.Now
                Me.Invoke(myForm, "")
            End If
        End If
    End Sub

    Private Sub btn1000_Click(sender As Object, e As EventArgs) Handles btn1000.Click
        BanknoteInDataReceived("ReceiveCash 1000")
    End Sub
    Private Sub btn500_Click(sender As Object, e As EventArgs) Handles btn500.Click
        BanknoteInDataReceived("ReceiveCash 500")
    End Sub
    Private Sub btn100_Click(sender As Object, e As EventArgs) Handles btn100.Click
        BanknoteInDataReceived("ReceiveCash 100")
    End Sub
    Private Sub btn50_Click(sender As Object, e As EventArgs) Handles btn50.Click
        BanknoteInDataReceived("ReceiveCash 50")
    End Sub
    Private Sub btn20_Click(sender As Object, e As EventArgs) Handles btn20.Click
        BanknoteInDataReceived("ReceiveCash 20")
    End Sub
    Private Sub btn10_Click(sender As Object, e As EventArgs) Handles btn10.Click
        CoinInDataReceived("ReceiveCoin 10")
    End Sub
    Private Sub btn5_Click(sender As Object, e As EventArgs) Handles btn5.Click
        CoinInDataReceived("ReceiveCoin 5")
    End Sub
    Private Sub btn2_Click(sender As Object, e As EventArgs) Handles btn2.Click
        CoinInDataReceived("ReceiveCoin 2")
    End Sub
    Private Sub btn1_Click(sender As Object, e As EventArgs) Handles btn1.Click
        CoinInDataReceived("ReceiveCoin 1")
    End Sub


End Class