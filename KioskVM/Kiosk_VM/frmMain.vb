﻿
Imports System.Management
Imports VendingLinqDB.ConnectDB
Imports VendingLinqDB.TABLE
Imports Engine

Public Class frmMain

    Dim DT_ADS As New DataTable
    Dim Ads_Rec As Int32
    'Dim Ads_Img As Image
    Dim Ads_Interval As Integer

    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Application.Exit()
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub

    Private Sub frmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        'Cursor.Hide()

        'Check DB Connect at Startup
        Dim CurTime As DateTime = DateTime.Now
        Dim IsConnect As Boolean = False
        Do
            IsConnect = VendingDB.ChkConnection()
            If IsConnect = False Then
                If CurTime.AddMinutes(10) < DateTime.Now Then
                    'ถ้ารอนานเกิน 10 นาที
                    ShowFormError("Out of service", "Network is Disconnect", VendingConfig.SelectForm)
                    Exit Sub
                End If
            End If
        Loop Until IsConnect = True

        VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.Main
        GetKioskNetworkData()
        GetVendingConfig()

        Dim WS As New VDMWebService.ATBVendingWebservice
        WS.Url = VendingConfig.WebserviceVDMURL
        WS.Timeout = 10000

        GetKioskSystemData(WS)

        If VendingData.VendingID = "" Then
            ShowFormError("Out of service", "Invalid Kios Informaiton", VendingConfig.SelectForm)
            Exit Sub
        End If

        SetAppScreenList(WS)
        SetLangMasterList(WS)
        SetAlarmMasterList(WS)

        DeviceInfoList = New DataTable
        SetListDeviceInfo(WS)
        If DeviceInfoList.Rows.Count > 0 Then
            GetVendingDeviceConfig()   'ข้อมูลการตั้งค่า Comport ของ HW
            GetMasterProductInfo(WS)
            GetMasterProductPromotion(WS)

            If ProductMasterList.Rows.Count > 0 Then
                StartMoneyDevice()

                Application.DoEvents()

                If VendingConfig.ScreenLayout = "V" Then
                    Dim fHome As New frmHomeVertical
                    fHome.Show()
                    Me.Hide()
                ElseIf VendingConfig.ScreenLayout = "H" Then

                End If
            Else
                ShowFormError("Out of service", "Load Device Fail", VendingConfig.SelectForm)
            End If
        Else
            ShowFormError("Out of service", "Load Device Fail", VendingConfig.SelectForm)
        End If

        WS.Dispose()
    End Sub


#Region "Start Device"

    Private Sub StartMoneyDevice()
        Try
            If DeviceInfoList.Rows.Count > 0 Then
                For i As Int32 = 0 To DeviceInfoList.Rows.Count - 1
                    If Convert.IsDBNull(DeviceInfoList.Rows(i)("comport_vid")) = True Then
                        Continue For
                    End If

                    Dim Comport As String = DeviceInfoList.Rows(i)("comport_vid").ToString
                    Select Case DeviceInfoList.Rows(i).Item("device_id")
                        Case mdlDeviceInfoENG.DeviceID.CoinOut_1
                            If CoinOut_1.ConnectCoinOutDevice(Comport) = True Then
                                CoinOut_1.ResetDeviceCoinOut()
                                SendKioskAlarm("COIN_OUT_DISCONNECTED", False)
                            Else
                                SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                            End If
                        Case mdlDeviceInfoENG.DeviceID.CoinOut_2
                            If CoinOut_2.ConnectCoinOutDevice(Comport) = True Then
                                CoinOut_2.ResetDeviceCoinOut()
                                SendKioskAlarm("COIN_OUT_DISCONNECTED", False)
                            Else
                                SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                            End If
                        Case mdlDeviceInfoENG.DeviceID.CoinOut_5
                            If CoinOut_5.ConnectCoinOutDevice(Comport) = True Then
                                CoinOut_5.ResetDeviceCoinOut()
                                SendKioskAlarm("COIN_OUT_DISCONNECTED", False)
                            Else
                                SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                            End If
                        Case mdlDeviceInfoENG.DeviceID.CoinOut_10
                            If CoinOut_10.ConnectCoinOutDevice(Comport) = True Then
                                CoinOut_10.ResetDeviceCoinOut()
                                SendKioskAlarm("COIN_OUT_DISCONNECTED", False)
                            Else
                                SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                            End If
                        Case mdlDeviceInfoENG.DeviceID.BankNoteOut_20
                            If BanknoteOut_20.ConnectBanknoteOutDevice(Comport) = True Then
                                BanknoteOut_20.RefreshDeviceCashOut()
                                SendKioskAlarm("CASH_OUT_DISCONNECTED", False)
                            Else
                                SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                            End If
                        Case mdlDeviceInfoENG.DeviceID.BankNoteOut_50
                            If BanknoteOut_50.ConnectBanknoteOutDevice(Comport) = True Then
                                BanknoteOut_50.RefreshDeviceCashOut()
                                SendKioskAlarm("CASH_OUT_DISCONNECTED", False)
                            Else
                                SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                            End If
                        Case mdlDeviceInfoENG.DeviceID.BankNoteOut_100
                            If BanknoteOut_100.ConnectBanknoteOutDevice(Comport) = True Then
                                BanknoteOut_100.RefreshDeviceCashOut()
                                SendKioskAlarm("CASH_OUT_DISCONNECTED", False)
                            Else
                                SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                            End If
                        Case mdlDeviceInfoENG.DeviceID.BankNoteOut_500
                            If BanknoteOut_500.ConnectBanknoteOutDevice(Comport) = True Then
                                BanknoteOut_500.RefreshDeviceCashOut()
                                SendKioskAlarm("CASH_OUT_DISCONNECTED", False)
                            Else
                                SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                            End If
                    End Select
                Next

                InsertLogSalesActivity("", "เริ่มต้นการทำงานอุปรกรณ์รับ/ทอน เงิน", VendingConfig.SelectForm, False)
            End If
        Catch ex As Exception
            InsertLogSalesActivity("", "Exception : " & ex.Message & vbNewLine & ex.StackTrace, VendingConfig.SelectForm, True)
        End Try
    End Sub


    Public Event CoinInReceiveEvent(ByVal ReceiveData As String)
    Private Sub DataReceivedCoinIn(ByVal ReceiveData As String)
        Select Case ReceiveData
            Case mdlDeviceInfoENG.CoinInStatus.Ready
                UpdateDeviceStatus(mdlDeviceInfoENG.DeviceID.CoinIn, mdlDeviceInfoENG.CoinInStatus.Ready)
                Exit Sub
            Case mdlDeviceInfoENG.CoinInStatus.Unavailable
                UpdateDeviceStatus(mdlDeviceInfoENG.DeviceID.CoinIn, mdlDeviceInfoENG.CoinInStatus.Ready)
                Exit Sub
            Case mdlDeviceInfoENG.CoinInStatus.Ready
                UpdateDeviceStatus(mdlDeviceInfoENG.DeviceID.CoinIn, mdlDeviceInfoENG.CoinInStatus.Disconnected)
                SendKioskAlarm("COIN_IN_DISCONNECTED", True)
                Exit Sub
            Case mdlDeviceInfoENG.CoinInStatus.Sersor_1_Problem
                UpdateDeviceStatus(mdlDeviceInfoENG.DeviceID.CoinIn, mdlDeviceInfoENG.CoinInStatus.Sersor_1_Problem)
                SendKioskAlarm("COIN_IN_SERSOR_1_PROBLEM", True)
                Exit Sub
            Case mdlDeviceInfoENG.CoinInStatus.Sersor_2_Problem
                UpdateDeviceStatus(mdlDeviceInfoENG.DeviceID.CoinIn, mdlDeviceInfoENG.CoinInStatus.Sersor_2_Problem)
                SendKioskAlarm("COIN_IN_SERSOR_2_PROBLEM", True)
                Exit Sub
            Case mdlDeviceInfoENG.CoinInStatus.Sersor_3_Problem
                UpdateDeviceStatus(mdlDeviceInfoENG.DeviceID.CoinIn, mdlDeviceInfoENG.CoinInStatus.Sersor_3_Problem)
                SendKioskAlarm("COIN_IN_SERSOR_3_PROBLEM", True)
                Exit Sub
        End Select

        If InStr(ReceiveData, "ReceiveCoin") > 0 Then
            RaiseEvent CoinInReceiveEvent(ReceiveData)
        End If
    End Sub

#End Region


    Private Sub SetAppScreenList(WS As VDMWebService.ATBVendingWebservice)
        AppScreenList = New DataTable
        AppScreenList = WS.GetAppScreenList()
        InsertLogSalesActivity("", "โหลดรายชื่อหน้าจอการใช้งาน", VendingConfig.SelectForm, False)
    End Sub

    Private Sub SetLangMasterList(WS As VDMWebService.ATBVendingWebservice)
        LangMasterList = New DataTable
        LangMasterList = WS.GetLangMasterList(VendingData.VendingID)
        InsertLogSalesActivity("", "โหลดข้อมูลภาษา", VendingConfig.SelectForm, False)
    End Sub

    Private Sub SetAlarmMasterList(WS As VDMWebService.ATBVendingWebservice)
        Try
            AlarmMasterList = New DataTable
            AlarmMasterList = WS.GetAlarmMasterList()
            InsertLogSalesActivity("", "โหลดข้อมูล Alarm Master", VendingConfig.SelectForm, False)
        Catch ex As Exception
            AlarmMasterList = New DataTable
        End Try
    End Sub

    Private Sub GetKioskNetworkData()
        'หาข้อมูลของเครื่อง PC ทำเฉพาะครั้งแรกที่เปิดโปรแกรมและเก็บค่านี้ไว้ตลอด

        VendingData.ComputerName = Environment.MachineName
        Try
            VendingData.CardLanDesc = GetCardLanDesc()
            'Network Information
            Dim mc As New ManagementClass("Win32_NetworkAdapterConfiguration")
            Dim moc As ManagementObjectCollection = mc.GetInstances()

            For Each mo As ManagementObject In moc
                If CStr(mo("Description")).Trim = VendingData.CardLanDesc Then
                    If mo("IPEnabled") = True Then
                        VendingData.IpAddress = mo("IPAddress")(0)
                        VendingData.MacAddress = mo("MacAddress").ToString().Replace(":", "-")
                        Exit Sub
                    Else
                        ShowFormError("Attention", "Cannot found Network Device " + VendingData.CardLanDesc, VendingConfig.SelectForm)
                        Exit Sub
                    End If
                End If
                mo.Dispose()
            Next
        Catch ex As Exception
            ShowFormError("Attention", "Cannot found Network Device " + VendingData.CardLanDesc, VendingConfig.SelectForm)
            Exit Sub
        End Try
    End Sub

    Private Sub GetKioskSystemData(WS As VDMWebService.ATBVendingWebservice)
        'หาข้อมูลของเครื่อง PC ทำเฉพาะครั้งแรกที่เปิดโปรแกรมและเก็บค่านี้ไว้ตลอด
        Try
            Dim dt As DataTable = WS.GetVendingSystemInfo(VendingData.VendingID, VendingData.MacAddress, VendingData.IpAddress, VendingData.ComputerName)
            If dt.Rows.Count > 0 Then
                'กำหนดค่าให้สำหรับตัวแปรที่จะต้องใช้ในโปรแกรม
                Dim dr As DataRow = dt.Rows(0)
                VendingData.LocationID = dr("location_id")
                VendingData.LocationCode = dr("location_code")
                VendingData.LocationName = dr("location_name")

                'Update ชื่อของอุปกรณ์ Network
                Dim kdLnq As New MsVendingDeviceVendingLinqDB
                kdLnq.ChkDataByMS_DEVICE_ID_MS_VENDING_ID(mdlDeviceInfoENG.DeviceID.NetworkConnection, VendingData.VendingID, Nothing)

                If kdLnq.ID > 0 Then
                    kdLnq.DRIVER_NAME1 = VendingData.CardLanDesc
                    kdLnq.SYNC_TO_SERVER = "N"

                    Dim trans As New VendingTransactionDB
                    Dim koRe As ExecuteDataInfo = kdLnq.UpdateData(VendingData.ComputerName, trans.Trans)
                    If koRe.IsSuccess = True Then
                        trans.CommitTransaction()
                    Else
                        trans.RollbackTransaction()
                        InsertLogSalesActivity("", koRe.ErrorMessage, VendingConfig.SelectForm, True)
                    End If
                Else
                    ShowFormError("Attention", "Cannot found Kiosk  Network Connection", VendingConfig.SelectForm)
                    Exit Sub
                End If
                kdLnq = Nothing
            End If
            dt.Dispose()

            InsertLogSalesActivity("", "ตรวจสอบข้อมูล Kiosk System", VendingConfig.SelectForm, False)
        Catch ex As Exception
            InsertLogSalesActivity("", "Exception : " & ex.Message & vbNewLine & ex.StackTrace, VendingConfig.SelectForm, True)
        End Try
    End Sub


    Sub CloseAllChildForm()
        For i As Integer = My.Application.OpenForms.Count - 1 To 0 Step -1
            If My.Application.OpenForms.Item(i) IsNot Me Then
                My.Application.OpenForms.Item(i).Close()
            End If
        Next i
    End Sub

    Sub CloseAllChildForm(ShowFrm As Form)
        For i As Integer = My.Application.OpenForms.Count - 1 To 0 Step -1
            If My.Application.OpenForms.Item(i) IsNot Me Then
                If My.Application.OpenForms.Item(i) IsNot ShowFrm Then
                    My.Application.OpenForms.Item(i).Close()
                End If
            End If
        Next i
    End Sub

#Region "Change Language"
    'Private Sub btnJP_Click(sender As Object, e As EventArgs) Handles btnJP.Click
    '    'InsertLogTransactionActivity(Customer.DepositTransNo, Pickup.TransactionNo, "", VendingConfig.VendingForm.Main, VendingConfig.KioskLockerStep.Main_ChangeLangJP, "", False)
    '    'VendingConfig.Language = mdlDeviceInfoENG.KioskLanguage.Japan
    '    ChangeFormMainLanguage()
    '    SetChildFormLanguage()
    'End Sub

    'Private Sub btnEN_Click(sender As Object, e As EventArgs) Handles btnEN.Click
    '    'InsertLogTransactionActivity(Customer.DepositTransNo, Pickup.TransactionNo, "", VendingConfig.VendingForm.Main, VendingConfig.KioskLockerStep.Main_ChangeLangEN, "", False)
    '    'VendingConfig.Language = mdlDeviceInfoENG.KioskLanguage.English
    '    ChangeFormMainLanguage()
    '    SetChildFormLanguage()
    'End Sub

    'Private Sub btnCH_Click(sender As Object, e As EventArgs) Handles btnCH.Click
    '    'InsertLogTransactionActivity(Customer.DepositTransNo, Pickup.TransactionNo, "", VendingConfig.VendingForm.Main, VendingConfig.KioskLockerStep.Main_ChangeLangCH, "", False)
    '    'VendingConfig.Language = mdlDeviceInfoENG.KioskLanguage.China
    '    ChangeFormMainLanguage()
    '    SetChildFormLanguage()
    'End Sub

    'Private Sub btnTH_Click(sender As Object, e As EventArgs) Handles btnTH.Click
    '    'InsertLogTransactionActivity(Customer.DepositTransNo, Pickup.TransactionNo, "", VendingConfig.VendingForm.Main, VendingConfig.KioskLockerStep.Main_ChangeLangTH, "", False)
    '    'VendingConfig.Language = mdlDeviceInfoENG.KioskLanguage.Thai
    '    ChangeFormMainLanguage()
    '    SetChildFormLanguage()
    'End Sub

    Private Sub ChangeFormMainLanguage()
        'Dim fldName As String = ""
        'Select Case VendingConfig.Language
        '    Case Data.ConstantsData.KioskLanguage.Thai
        '        fldName = "TH_Display"
        '    Case Data.ConstantsData.KioskLanguage.English
        '        fldName = "EN_Display"
        '    Case Data.ConstantsData.KioskLanguage.China
        '        fldName = "CH_Display"
        '    Case Data.KioskLanguage.Japan
        '        fldName = "JP_Display"
        'End Select

        'LangMasterList.DefaultView.RowFilter = "ms_app_screen_id='" & Convert.ToInt16(Data.VendingConfigData.VendingForm.Main) & "'"
        'If LangMasterList.DefaultView.Count > 0 Then
        '    For Each dr As DataRowView In LangMasterList.DefaultView
        '        Dim ControlName As String = dr("Control_Name")
        '        Dim cc() As Control = Me.Controls.Find(ControlName, True)
        '        If cc.Length > 0 Then
        '            cc(0).Text = dr(fldName)
        '        End If
        '    Next
        'End If
        'LangMasterList.DefaultView.RowFilter = ""
        'Application.DoEvents()
    End Sub


#End Region



End Class