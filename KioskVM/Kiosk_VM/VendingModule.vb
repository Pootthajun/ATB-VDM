﻿
Imports System.Data.SqlClient
Imports System.Drawing.Text
Imports System.Runtime.InteropServices
Imports System.Threading
Imports Kiosk_VM.Data
Imports Engine.mdlDeviceInfoENG
Imports VendingLinqDB.ConnectDB
Imports VendingLinqDB.TABLE

Module VendingModule

    Public INIFileNameLAN As String = Application.StartupPath & "\VendingLAN.ini"
    Public VendingData As New VendingSystemData
    Public VendingConfig As New VendingConfigData(VendingData.VendingID)
    Public vTrans As New SalesTransactionData(VendingData.VendingID)
    Public StaffConsole As New StaffConsoleLogonData(VendingData.VendingID)
    Public bgColor As Color = Color.FromName("White") 'Color.FromArgb(140, 198, 62)

    Public DeviceInfoList As New DataTable
    Public DeviceStatusList As New DataTable
    Public AppScreenList As New DataTable
    Public AlarmMasterList As New DataTable
    Public LangMasterList As New DataTable
    Public ProductMasterList As New DataTable
    Public ProductWarrantyList As New DataTable
    Public ProductPromotionList As New DataTable
    Public PrintCF As New DataTable
    Public PrintFld As New DataTable

    Public THB_CH As String = " 泰铢"
    Public THB_JP As String = " THB"
    Public THB_EN As String = " THB"

    Public OutOfService As Boolean


    'Hardware Interface
    Public BanknoteIn As New BanknoteIn.BanknoteInClass
    Public BanknoteOut_20 As New BanknoteOut.BanknoteOutClass
    Public BanknoteOut_50 As New BanknoteOut.BanknoteOutClass
    Public BanknoteOut_100 As New BanknoteOut.BanknoteOutClass
    Public BanknoteOut_500 As New BanknoteOut.BanknoteOutClass

    Public CoinIn As New CoinIn.CoinInClass
    Public CoinOut_1 As New CoinOut.CoinOutClass
    Public CoinOut_2 As New CoinOut.CoinOutClass
    Public CoinOut_5 As New CoinOut.CoinOutClass
    Public CoinOut_10 As New CoinOut.CoinOutClass

    Public Printer As New Printer.PrinterClass


    Public Sub getMyVersion()
        Dim version As System.Version = System.Reflection.Assembly.GetExecutingAssembly.GetName().Version
        Dim MyVersion As String = version.Major & "." & version.Minor & "." & version.Build
        frmMain.Text = Replace(frmMain.Text, "[%V%]", MyVersion)
    End Sub

    'Public Function getConnectionString() As String
    '    Return "Data Source=.;Initial Catalog=TSK;Integrated Security=True;Connect Timeout=1;"
    'End Function

    Public Function GetCardLanDesc() As String
        Dim CardLanDesc As String = ""
        Dim ini As New Kiosk_VM.Org.Mentalis.Files.IniReader(INIFileNameLAN)
        ini.Section = "Setting"
        CardLanDesc = ini.ReadString("CardLanDesc").ToString
        ini = Nothing
        Return CardLanDesc
    End Function

    Public Function genNewTransectionNo() As String

        Dim CurrTime As String = DateTime.Now.ToString("yyyyMMddHHmmss", New Globalization.CultureInfo("en-US"))
        Return VendingData.VendingID.PadLeft(3, "0") & CurrTime
    End Function

    Public Function Plexiglass(dialog As Form, MainForm As Form) As DialogResult
        Using plexi = New Form()
            plexi.FormBorderStyle = FormBorderStyle.None
            plexi.Bounds = Screen.FromPoint(dialog.Location).Bounds
            plexi.StartPosition = FormStartPosition.Manual
            plexi.AutoScaleMode = AutoScaleMode.None
            plexi.ShowInTaskbar = False
            plexi.BackColor = Color.Black
            plexi.Opacity = 0.45
            plexi.Show(MainForm)
            dialog.StartPosition = FormStartPosition.CenterParent
            Return dialog.ShowDialog(plexi)
        End Using
    End Function



#Region "Change Money"
    Public Function ChangeMoney(ByVal ChangeAmount As Integer, vTrans As SalesTransactionData) As Boolean
        'ทอนเงิน
        Try
            If ChangeAmount = 0 Then
                Return True
            End If

            Dim sql As String = "select ms_device_id device_id, current_qty kiosk_current_qty"
            sql += " from MS_VENDING_DEVICE "
            sql += " where ms_vending_id=@_KIOSK_ID "

            'เอามาเฉพาะเครื่องทอนเหรียญกับทอนแบงค์
            DeviceInfoList.DefaultView.RowFilter = "device_type_id in (2,4) and type_active_status='Y' and device_active_status='Y'"
            If DeviceInfoList.DefaultView.Count > 0 Then
                Dim tmp As String = ""
                For Each drv As DataRowView In DeviceInfoList.DefaultView
                    If tmp = "" Then
                        tmp = drv("device_id")
                    Else
                        tmp += "," & drv("device_id")
                    End If
                Next
                sql += " and ms_device_id in (" & tmp & ")"
            End If
            DeviceInfoList.DefaultView.RowFilter = ""

            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_KIOSK_ID", Convert.ToInt64(VendingData.VendingID))

            Dim dt As DataTable = VendingDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then
                dt.Columns.Add("device_type_id", GetType(Long))
                dt.Columns.Add("unit_value", GetType(Long))

                For i As Integer = 0 To dt.Rows.Count - 1
                    DeviceInfoList.DefaultView.RowFilter = "device_id='" & dt.Rows(i)("device_id") & "'"
                    If DeviceInfoList.DefaultView.Count > 0 Then
                        dt.Rows(i)("device_type_id") = DeviceInfoList.DefaultView(0)("device_type_id")
                        dt.Rows(i)("unit_value") = DeviceInfoList.DefaultView(0)("unit_value")
                    End If
                    DeviceInfoList.DefaultView.RowFilter = ""
                Next


                dt.DefaultView.Sort = "unit_value desc"
                dt = dt.DefaultView.ToTable

                For i As Integer = 0 To dt.Rows.Count - 1
                    If ChangeAmount <= 0 Then Exit For

                    Dim dr As DataRow = dt.Rows(i)
                    Dim vDeviceTypeID As Integer = Convert.ToInt16(dr("device_type_id"))
                    Dim vDeviceID As Integer = Convert.ToInt16(dr("device_id"))

                    Dim CurrentQty As Integer = Convert.ToInt16(dr("kiosk_current_qty"))
                    Dim UnitValue As Integer = Convert.ToInt16(dr("unit_value"))

                    Dim Num As Integer = 0
                    Do Until ChangeAmount < UnitValue
                        If CurrentQty > 0 Then   'มีเหลือใน Stock อยู่หรือไม่
                            Num += 1
                            ChangeAmount = ChangeAmount - Convert.ToInt16(dr("unit_value"))
                            CurrentQty -= 1  'หักยอดคงเหลือออกจาก Stock
                        Else
                            'ถ้าเงินหมดจาก Stock ให้ออกจาก Loop เลย เพื่อทำการทอนเงินท่าที่มีอยู่
                            Exit Do
                        End If
                    Loop

                    If Num > 0 Then
                        Select Case vDeviceTypeID
                            Case DeviceType.BanknoteOut
                                ChangeBanknote(vDeviceID, Num, vTrans)
                            Case DeviceType.CoinOut
                                If Num > 10 Then
                                    Do Until Num <= 0
                                        If Num >= 10 Then
                                            Thread.Sleep(500)
                                            ChangeCoin(vDeviceID, 10, vTrans)
                                        Else
                                            ChangeCoin(vDeviceID, Num, vTrans)
                                        End If
                                        Num -= 10
                                    Loop
                                Else
                                    ChangeCoin(vDeviceID, Num, vTrans)
                                End If
                        End Select
                    End If
                Next
            End If
            dt.Dispose()

            Return True
        Catch ex As Exception
            'InsertErrorLog("Exception : " & ex.Message & vbNewLine & ex.StackTrace, Customer.DepositTransNo, Pickup.TransactionNo, 0, VendingConfig.SelectForm, 0)
        End Try
        Return False
    End Function

    Public Function ReturnMoney(PaidAmount As Integer, Cust As SalesTransactionData) As Boolean
        'คืนเงิน
        Return ChangeMoney(PaidAmount, Cust)
    End Function
    Private Function ChangeBanknote(ByVal vDeviceID As DeviceID, ByVal Num As Int32, vTrans As SalesTransactionData) As Boolean
        Select Case vDeviceID
            Case DeviceID.BankNoteOut_20
                If BanknoteOut_20.ConnectBanknoteOutDevice(VendingConfig.BanknoteOUT20Comport) Then
                    If BanknoteOut_20.PayCashOut(Num) = "" Then
                        vTrans.ChangeBankNote20 += Num
                        'UpdateSaleTransaction(vTrans)
                        UpdateKioskCurrentQty(vDeviceID, Num * -1, Num * -20, False)
                        Return True
                    End If
                End If
                BanknoteOut_20.Disconnect()
            Case DeviceID.BankNoteOut_50
                If BanknoteOut_50.ConnectBanknoteOutDevice(VendingConfig.BanknoteOUT50Comport) Then
                    If BanknoteOut_50.PayCashOut(Num) = "" Then
                        vTrans.ChangeBankNote50 += Num
                        'UpdateSaleTransaction(vTrans)
                        UpdateKioskCurrentQty(vDeviceID, Num * -1, Num * -50, False)
                        Return True
                    End If
                End If
                BanknoteOut_50.Disconnect()
            Case DeviceID.BankNoteOut_100
                If BanknoteOut_100.ConnectBanknoteOutDevice(VendingConfig.BanknoteOUT100Comport) Then
                    If BanknoteOut_100.PayCashOut(Num) = "" Then
                        vTrans.ChangeBankNote100 += Num
                        'UpdateSaleTransaction(vTrans)
                        UpdateKioskCurrentQty(vDeviceID, Num * -1, Num * -100, False)
                        Return True
                    End If

                End If
                BanknoteOut_100.Disconnect()
            Case DeviceID.BankNoteOut_500
                If BanknoteOut_500.ConnectBanknoteOutDevice(VendingConfig.BanknoteOUT500Comport) Then
                    If BanknoteOut_500.PayCashOut(Num) = "" Then
                        vTrans.ChangeBankNote500 += Num
                        'UpdateSaleTransaction(vTrans)
                        UpdateKioskCurrentQty(vDeviceID, Num * -1, Num * -500, False)
                        Return True
                    End If

                End If
                BanknoteOut_500.Disconnect()
        End Select
        Return False
    End Function

    Private Function ChangeCoin(ByVal vDeviceID As DeviceID, ByVal Num As Int32, vTrans As SalesTransactionData) As Boolean
        Select Case vDeviceID
            Case DeviceID.CoinOut_1
                If CoinOut_1.ConnectCoinOutDevice(VendingConfig.CoinOut1Comport) Then
                    If CoinOut_1.PayCoinOut(Num) = "" Then
                        vTrans.ChangeCoin1 += Num
                        'UpdateSaleTransaction(vTrans)
                        UpdateKioskCurrentQty(vDeviceID, Num * -1, Num * -1, False)
                        Return True
                    End If

                End If
            Case DeviceID.CoinOut_2
                If CoinOut_2.ConnectCoinOutDevice(VendingConfig.CoinOut2Comport) Then
                    If CoinOut_2.PayCoinOut(Num) = "" Then
                        vTrans.ChangeCoin2 += Num
                        'UpdateSaleTransaction(vTrans)
                        UpdateKioskCurrentQty(vDeviceID, Num * -1, Num * -2, False)
                        Return True
                    End If

                End If
            Case DeviceID.CoinOut_5
                If CoinOut_5.ConnectCoinOutDevice(VendingConfig.CoinOut5Comport) Then
                    If CoinOut_5.PayCoinOut(Num) = "" Then
                        vTrans.ChangeCoin5 += Num
                        'UpdateSaleTransaction(vTrans)
                        UpdateKioskCurrentQty(vDeviceID, Num * -1, Num * -5, False)
                        Return True
                    End If

                End If
            Case DeviceID.CoinOut_10
                If CoinOut_10.ConnectCoinOutDevice(VendingConfig.CoinOut10Comport) Then
                    If CoinOut_10.PayCoinOut(Num) = "" Then
                        vTrans.ChangeCoin10 += Num
                        'UpdateSaleTransaction(vTrans)
                        UpdateKioskCurrentQty(vDeviceID, Num * -1, Num * -10, False)
                        Return True
                    End If

                End If
        End Select
        Return False
    End Function


#End Region


    Public Function GetDate() As String
        Dim YY As Int32 = Now.Year
        If YY > 2500 Then
            YY = YY - 543
        End If
        Dim MM As Int32 = Now.Month
        Dim DD As Int32 = Now.Day
        Return YY.ToString & "-" & MM.ToString.PadLeft(2, "0") & "-" & DD.ToString.PadLeft(2, "0")
    End Function

    Public Sub ShowDialogErrorMessageSC(ByVal Message As String)
        Dim f As New frmDialog_OK
        f.lblMessage.Text = Message
        f.pnlDialog.BackColor = Color.FromArgb(97, 78, 72)
        Plexiglass(f, frmMain)
    End Sub

    Public Sub ShowDialogErrorMessage(ByVal Message As String)
        'Dim f As New frmDialog_OK
        'f.lblMessage.Text = Message
        'f.pnlDialog.BackColor = bgColor
        'Plexiglass(f, frmMain)
    End Sub

#Region "Device Status"
    Public Sub SetListDeviceInfo(WS As VDMWebService.ATBVendingWebservice)
        Dim sql As String = " Select kd.id vending_device_id, kd.max_qty vending_max_qty,kd.warning_qty vending_warning_qty, kd.critical_qty vending_critical_qty, "
        sql += " kd.current_qty vending_current_qty, kd.current_money vending_current_money, "
        sql += " kd.comport_vid, kd.driver_name1, kd.driver_name2, kd.ms_vending_id, kd.ms_device_id as device_id "
        sql += " From ms_vending_device kd "
        sql += " where kd.ms_vending_id=@_VENDING_ID"

        Dim p(1) As SqlParameter
        p(0) = VendingDB.SetBigInt("@_VENDING_ID", Convert.ToInt64(VendingData.VendingID))

        DeviceInfoList = VendingDB.ExecuteTable(sql, p)
        If DeviceInfoList.Rows.Count > 0 Then
            DeviceInfoList.Columns.Add("device_type_id")
            DeviceInfoList.Columns.Add("device_type_name_th")
            DeviceInfoList.Columns.Add("device_type_name_en")
            DeviceInfoList.Columns.Add("movement_direction")
            DeviceInfoList.Columns.Add("type_active_status")
            DeviceInfoList.Columns.Add("device_name_th")
            DeviceInfoList.Columns.Add("device_name_en")
            DeviceInfoList.Columns.Add("device_active_status")
            DeviceInfoList.Columns.Add("unit_value")

            Dim mDt As DataTable = WS.GetMasterDeviceInfoList()
            If mDt.Rows.Count > 0 Then
                For i As Integer = 0 To DeviceInfoList.Rows.Count - 1
                    mDt.DefaultView.RowFilter = "device_id='" & DeviceInfoList.Rows(i)("device_id") & "'"
                    If mDt.DefaultView.Count > 0 Then
                        Dim mDrv As DataRowView = mDt.DefaultView(0)
                        DeviceInfoList.Rows(i)("device_type_id") = mDrv("device_type_id")
                        DeviceInfoList.Rows(i)("device_type_name_th") = mDrv("device_type_name_th")
                        DeviceInfoList.Rows(i)("device_type_name_en") = mDrv("device_type_name_en")
                        DeviceInfoList.Rows(i)("movement_direction") = mDrv("movement_direction")
                        DeviceInfoList.Rows(i)("type_active_status") = mDrv("type_active_status")
                        DeviceInfoList.Rows(i)("device_name_th") = mDrv("device_name_th")
                        DeviceInfoList.Rows(i)("device_name_en") = mDrv("device_name_en")
                        DeviceInfoList.Rows(i)("device_active_status") = mDrv("device_active_status")
                        DeviceInfoList.Rows(i)("unit_value") = mDrv("unit_value")
                    End If
                    mDt.DefaultView.RowFilter = ""
                Next
                InsertLogSalesActivity("", "โหลดข้อมูลรายการอุปกรณ์", VendingConfig.SelectForm, False)
            End If
            mDt.Dispose()
        End If

        DeviceStatusList = WS.GeMasterDeviceStatus()
        InsertLogSalesActivity("", "โหลดข้อมูลสถานะของอุปกรณ์", VendingConfig.SelectForm, False)

    End Sub

    Function UpdateAllDeviceStatusByComPort() As String
        Dim Msg As String = ""
        Try

            Dim Dt As DataTable = DeviceInfoList 'KioskDB.ExecuteTable(sql, p)
            If Dt.Rows.Count > 0 Then
                For i As Integer = 0 To Dt.Rows.Count - 1
                    If Convert.IsDBNull(Dt.Rows(i)("comport_vid")) = True Then
                        Continue For
                    End If

                    Dim Comport As String = Dt.Rows(i)("comport_vid").ToString
                    Dim vDeviceID As Long = Convert.ToInt64(Dt.Rows(i)("device_id"))
                    Select Case Dt.Rows(i).Item("device_type_id")
                        Case DeviceType.BanknoteIn
                            If BanknoteIn.ConnectBanknoteInDevice(Comport) = True Then
                                Dim ret As String = BanknoteIn.CheckStatusDeviceCashIn()
                                If ret.Trim = "" Then
                                    UpdateDeviceStatus(vDeviceID, BanknoteInStatus.Ready)
                                    SendKioskAlarm("CASH_IN_Disconnected", False)
                                Else
                                    UpdateDeviceStatus(vDeviceID, BanknoteInStatus.Disconnected)
                                    SendKioskAlarm("CASH_IN_Disconnected", True)
                                End If
                            Else
                                UpdateDeviceStatus(vDeviceID, BanknoteInStatus.Disconnected)
                                SendKioskAlarm("CASH_IN_Disconnected", True)
                            End If
                        Case DeviceType.CoinIn
                            If CoinIn.ConnectCoinInDevice(Comport) = True Then
                                Dim ret As String = CoinIn.CheckStatusDeviceCoinIn()
                                If ret = "" Then
                                    UpdateDeviceStatus(vDeviceID, CoinInStatus.Ready)
                                    SendKioskAlarm("COIN_IN_DISCONNECTED", False)
                                Else
                                    UpdateDeviceStatus(vDeviceID, CoinInStatus.Disconnected)
                                    SendKioskAlarm("COIN_IN_DISCONNECTED", True)
                                End If
                            Else
                                UpdateDeviceStatus(vDeviceID, CoinInStatus.Disconnected)
                                SendKioskAlarm("COIN_IN_DISCONNECTED", True)
                            End If
                        Case DeviceType.BanknoteOut
                            Select Case vDeviceID
                                Case DeviceID.BankNoteOut_20
                                    If BanknoteOut_20.ConnectBanknoteOutDevice(Comport) = True Then
                                        'AddHandler BanknoteOut_20.MySerialPort.DataReceived, AddressOf BanknoteOut_20.MySerialPortDataReceived
                                        AddHandler BanknoteOut_20.ReceiveEvent, AddressOf DataReceivedCashOut20
                                        Dim ret As String = BanknoteOut_20.CheckStatusDeviceCashOut()
                                        If ret = "" Then
                                            UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Ready)
                                            SendKioskAlarm("CASH_OUT_DISCONNECTED", False)
                                        Else
                                            UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Disconnected)
                                            SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                                        End If
                                    Else
                                        UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Disconnected)
                                        SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                                    End If
                                Case DeviceID.BankNoteOut_50
                                    If BanknoteOut_50.ConnectBanknoteOutDevice(Comport) = True Then
                                        'AddHandler BanknoteOut_50.MySerialPort.DataReceived, AddressOf BanknoteOut_50.MySerialPortDataReceived
                                        AddHandler BanknoteOut_50.ReceiveEvent, AddressOf DataReceivedCashOut50
                                        Dim ret As String = BanknoteOut_50.CheckStatusDeviceCashOut()
                                        If ret = "" Then
                                            UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Ready)
                                            SendKioskAlarm("CASH_OUT_DISCONNECTED", False)
                                        Else
                                            UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Disconnected)
                                            SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                                        End If
                                    Else
                                        UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Disconnected)
                                        SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                                    End If
                                Case DeviceID.BankNoteOut_100
                                    If BanknoteOut_100.ConnectBanknoteOutDevice(Comport) = True Then
                                        'AddHandler BanknoteOut_100.MySerialPort.DataReceived, AddressOf BanknoteOut_100.MySerialPortDataReceived
                                        AddHandler BanknoteOut_100.ReceiveEvent, AddressOf DataReceivedCashOut100
                                        Dim ret As String = BanknoteOut_100.CheckStatusDeviceCashOut()
                                        If ret = "" Then
                                            UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Ready)
                                            SendKioskAlarm("CASH_OUT_DISCONNECTED", False)
                                        Else
                                            UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Disconnected)
                                            SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                                        End If
                                    Else
                                        UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Disconnected)
                                        SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                                    End If
                                Case DeviceID.BankNoteOut_500
                                    If BanknoteOut_500.ConnectBanknoteOutDevice(Comport) = True Then
                                        'AddHandler BanknoteOut_500.MySerialPort.DataReceived, AddressOf BanknoteOut_500.MySerialPortDataReceived
                                        AddHandler BanknoteOut_500.ReceiveEvent, AddressOf DataReceivedCashOut500
                                        Dim ret As String = BanknoteOut_500.CheckStatusDeviceCashOut()
                                        If ret = "" Then
                                            UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Ready)
                                            SendKioskAlarm("CASH_OUT_DISCONNECTED", False)
                                        Else
                                            UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Disconnected)
                                            SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                                        End If
                                    Else
                                        UpdateDeviceStatus(vDeviceID, BanknoteOutStatus.Disconnected)
                                        SendKioskAlarm("CASH_OUT_DISCONNECTED", True)
                                    End If
                            End Select
                        Case DeviceType.CoinOut
                            Select Case vDeviceID
                                Case DeviceID.CoinOut_1
                                    If CoinOut_1.ConnectCoinOutDevice(Comport) = True Then
                                        'AddHandler CoinOut_1.MySerialPort.DataReceived, AddressOf CoinOut_1.MySerialPortDataReceived
                                        AddHandler CoinOut_1.ReceiveEvent, AddressOf DataReceivedCoinOut1
                                        Dim ret As String = CoinOut_1.CheckStatusDeviceCoinOut()
                                        If ret = "" Then
                                            UpdateDeviceStatus(vDeviceID, CoinOutStatus.Ready)
                                            SendKioskAlarm("COIN_OUT_DISCONNECTED", False)
                                        Else
                                            UpdateDeviceStatus(vDeviceID, CoinOutStatus.Disconnected)
                                            SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                                        End If
                                    Else
                                        UpdateDeviceStatus(vDeviceID, CoinOutStatus.Disconnected)
                                        SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                                    End If
                                Case DeviceID.CoinOut_2
                                    If CoinOut_2.ConnectCoinOutDevice(Comport) = True Then
                                        'AddHandler CoinOut_2.MySerialPort.DataReceived, AddressOf CoinOut_2.MySerialPortDataReceived
                                        AddHandler CoinOut_2.ReceiveEvent, AddressOf DataReceivedCoinOut2
                                        Dim ret As String = CoinOut_2.CheckStatusDeviceCoinOut()
                                        If ret = "" Then
                                            UpdateDeviceStatus(vDeviceID, CoinOutStatus.Ready)
                                            SendKioskAlarm("COIN_OUT_DISCONNECTED", False)
                                        Else
                                            UpdateDeviceStatus(vDeviceID, CoinOutStatus.Disconnected)
                                            SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                                        End If
                                    Else
                                        UpdateDeviceStatus(vDeviceID, CoinOutStatus.Disconnected)
                                        SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                                    End If
                                Case DeviceID.CoinOut_5
                                    If CoinOut_5.ConnectCoinOutDevice(Comport) = True Then
                                        'AddHandler CoinOut_5.MySerialPort.DataReceived, AddressOf CoinOut_5.MySerialPortDataReceived
                                        AddHandler CoinOut_5.ReceiveEvent, AddressOf DataReceivedCoinOut5
                                        Dim ret As String = CoinOut_5.CheckStatusDeviceCoinOut()
                                        If ret = "" Then
                                            UpdateDeviceStatus(vDeviceID, CoinOutStatus.Ready)
                                            SendKioskAlarm("COIN_OUT_DISCONNECTED", False)
                                        Else
                                            UpdateDeviceStatus(vDeviceID, CoinOutStatus.Disconnected)
                                            SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                                        End If
                                    Else
                                        UpdateDeviceStatus(vDeviceID, CoinOutStatus.Disconnected)
                                        SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                                    End If
                                Case DeviceID.CoinOut_10
                                    If CoinOut_10.ConnectCoinOutDevice(Comport) = True Then
                                        'AddHandler CoinOut_10.MySerialPort.DataReceived, AddressOf CoinOut_10.MySerialPortDataReceived
                                        AddHandler CoinOut_10.ReceiveEvent, AddressOf DataReceivedCoinOut10
                                        Dim ret As String = CoinOut_10.CheckStatusDeviceCoinOut()
                                        If ret = "" Then
                                            UpdateDeviceStatus(vDeviceID, CoinOutStatus.Ready)
                                            SendKioskAlarm("COIN_OUT_DISCONNECTED", False)
                                        Else
                                            UpdateDeviceStatus(vDeviceID, CoinOutStatus.Disconnected)
                                            SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                                        End If
                                    Else
                                        UpdateDeviceStatus(vDeviceID, CoinOutStatus.Disconnected)
                                        SendKioskAlarm("COIN_OUT_DISCONNECTED", True)
                                    End If
                            End Select
                    End Select
                Next
            End If
            Dt.Dispose()

        Catch ex As Exception
            Msg = ex.Message
        End Try
        Return Msg
    End Function

    Function UpdateAllDeviceStatusByUsbPort() As String
        'เป็นฟังก์ชั่นสำหรับ Update Check Status ของ Hardware เพื่อเก็บข้อมูลลงใน DB
        Dim Msg As String = ""
        Try
            If DeviceInfoList.Rows.Count > 0 Then
                For i As Integer = 0 To DeviceInfoList.Rows.Count - 1
                    Dim dr As DataRow = DeviceInfoList.Rows(i)
                    Dim DeviceTypeID As Long = Convert.ToInt64(dr("device_type_id"))
                    Dim DeviceID As Long = Convert.ToInt64(dr("device_id"))
                    Dim VID As String = dr("comport_vid").ToString

                    Select Case DeviceTypeID
                        Case DeviceType.Printer
                            If Convert.IsDBNull(dr("driver_name1")) = False Then
                                Dim pntSts As String = Printer.CheckPrinterStatus(dr("driver_name1"))
                                UpdateDeviceStatus(DeviceID.ToString, pntSts)

                                If pntSts = PrinterStatus.Offline Then
                                    SendKioskAlarm("PRINTER_OFFLINE", True)
                                Else
                                    SendKioskAlarm("PRINTER_OFFLINE", False)
                                End If
                            End If
                        Case DeviceType.NetworkConnection
                            If ServerLinqDB.ConnectDB.ServerDB.ChkConnection() = True Then
                                UpdateDeviceStatus(DeviceID.ToString, NetworkStatus.Ready)
                                SendKioskAlarm("NET_DISCONNECTED", False)
                            Else
                                UpdateDeviceStatus(DeviceID.ToString, NetworkStatus.Disconnected)
                                SendKioskAlarm("NET_DISCONNECTED", True)
                            End If
                    End Select
                Next
            End If

        Catch ex As Exception
            Msg = ex.Message
        End Try
        Return Msg
    End Function

    Public Sub UpdateDeviceStatus(ByVal DeviceID As String, ByVal StatusID As String)
        Try
            'Dim lnq As New MsKioskDeviceKioskLinqDB
            'lnq.ChkDataByMS_DEVICE_ID_MS_KIOSK_ID(Convert.ToInt16(DeviceID), VendingData.VendingID, Nothing)
            'If lnq.ID > 0 Then
            '    lnq.MS_DEVICE_STATUS_ID = Convert.ToInt16(StatusID)
            '    lnq.SYNC_TO_SERVER = "N"
            '    lnq.SYNC_TO_KIOSK = "Y"

            '    Dim trans As New KioskTransactionDB
            '    Dim ret As ExecuteDataInfo = lnq.UpdateData(VendingData.ComputerName, trans.Trans)
            '    If ret.IsSuccess = True Then
            '        trans.CommitTransaction()
            '    Else
            '        trans.RollbackTransaction()
            '        InsertErrorLog(ret.ErrorMessage & vbNewLine & "&DeviceID=" & DeviceID & "&StatusID=" & StatusID, Customer.DepositTransNo, Pickup.TransactionNo, StaffConsole.TransactionID, VendingConfig.SelectForm, 0)
            '    End If
            'End If
            'lnq = Nothing
        Catch ex As Exception
            'InsertErrorLog("Exception : " & ex.Message & vbNewLine & ex.StackTrace & vbNewLine & "&DeviceID=" & DeviceID & "&StatusID=" & StatusID, Customer.DepositTransNo, Pickup.TransactionNo, StaffConsole.TransactionID, VendingConfig.SelectForm, 0)
        End Try
    End Sub

    Public Event BanknoteInReceiveEvent(ByVal ReceiveData As String)
    Private Sub DataReceivedBanknoteIn(ByVal ReceiveData As String)
        Select Case ReceiveData
            Case BanknoteInStatus.Ready
                UpdateDeviceStatus(DeviceID.BankNoteIn, BanknoteInStatus.Ready)
                Exit Sub
            Case BanknoteInStatus.Unavailable
                UpdateDeviceStatus(DeviceID.BankNoteIn, BanknoteInStatus.Unavailable)
                Exit Sub
            Case BanknoteInStatus.Disconnected
                UpdateDeviceStatus(DeviceID.BankNoteIn, BanknoteInStatus.Disconnected)
                Exit Sub
            Case BanknoteInStatus.Power_OFF
                UpdateDeviceStatus(DeviceID.BankNoteIn, BanknoteInStatus.Power_OFF)
                Exit Sub
            Case BanknoteInStatus.Motor_Failure
                UpdateDeviceStatus(DeviceID.BankNoteIn, BanknoteInStatus.Motor_Failure)
                Exit Sub
            Case BanknoteInStatus.Checksum_Error
                UpdateDeviceStatus(DeviceID.BankNoteIn, BanknoteInStatus.Checksum_Error)
                Exit Sub
            Case BanknoteInStatus.Bill_Jam
                UpdateDeviceStatus(DeviceID.BankNoteIn, BanknoteInStatus.Bill_Jam)
                Exit Sub
            Case BanknoteInStatus.Bill_Remove
                UpdateDeviceStatus(DeviceID.BankNoteIn, BanknoteInStatus.Bill_Remove)
                Exit Sub
            Case BanknoteInStatus.Stacker_Open
                UpdateDeviceStatus(DeviceID.BankNoteIn, BanknoteInStatus.Stacker_Open)
                Exit Sub
            Case BanknoteInStatus.Sensor_Problem
                UpdateDeviceStatus(DeviceID.BankNoteIn, BanknoteInStatus.Sensor_Problem)
                Exit Sub
            Case BanknoteInStatus.Bill_Fish
                UpdateDeviceStatus(DeviceID.BankNoteIn, BanknoteInStatus.Bill_Fish)
                Exit Sub
        End Select

        If InStr(ReceiveData, "ReceiveCash") > 0 Then
            RaiseEvent BanknoteInReceiveEvent(ReceiveData)
        End If
    End Sub

    Public Event CoinInReceiveEvent(ByVal ReceiveData As String)
    Private Sub DataReceivedCoinIn(ByVal ReceiveData As String)
        Select Case ReceiveData
            Case CoinInStatus.Ready
                UpdateDeviceStatus(DeviceID.CoinIn, CoinInStatus.Ready)
                Exit Sub
            Case CoinInStatus.Unavailable
                UpdateDeviceStatus(DeviceID.CoinIn, CoinInStatus.Unavailable)
                Exit Sub
            Case CoinInStatus.Ready
                UpdateDeviceStatus(DeviceID.CoinIn, CoinInStatus.Disconnected)
                Exit Sub
            Case CoinInStatus.Sersor_1_Problem
                UpdateDeviceStatus(DeviceID.CoinIn, CoinInStatus.Sersor_1_Problem)
                Exit Sub
            Case CoinInStatus.Sersor_2_Problem
                UpdateDeviceStatus(DeviceID.CoinIn, CoinInStatus.Sersor_2_Problem)
                Exit Sub
            Case CoinInStatus.Sersor_3_Problem
                UpdateDeviceStatus(DeviceID.CoinIn, CoinInStatus.Sersor_3_Problem)
                Exit Sub
        End Select

        If InStr(ReceiveData, "ReceiveCoin") > 0 Then
            RaiseEvent CoinInReceiveEvent(ReceiveData)
        End If
    End Sub

    Private Sub DataReceivedCashOut20(ByVal ReceiveData As String)
        UpdateDataReceivedBanknoteOut(ReceiveData, DeviceID.BankNoteOut_20)
    End Sub
    Private Sub DataReceivedCashOut50(ByVal ReceiveData As String)
        UpdateDataReceivedBanknoteOut(ReceiveData, DeviceID.BankNoteOut_50)
    End Sub
    Private Sub DataReceivedCashOut100(ByVal ReceiveData As String)
        UpdateDataReceivedBanknoteOut(ReceiveData, DeviceID.BankNoteOut_100)
    End Sub
    Private Sub DataReceivedCashOut500(ByVal ReceiveData As String)
        UpdateDataReceivedBanknoteOut(ReceiveData, DeviceID.BankNoteOut_500)
    End Sub

    Private Sub UpdateDataReceivedBanknoteOut(ByVal ReceiveData As String, ByVal DeviceID As String)
        Select Case ReceiveData
            Case BanknoteOutStatus.Ready
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Ready)
            Case BanknoteOutStatus.Disconnected
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Disconnected)
            Case BanknoteOutStatus.Single_machine_payout
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Single_machine_payout)
            Case BanknoteOutStatus.Multiple_machine_payout
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Multiple_machine_payout)
            Case BanknoteOutStatus.Payout_Successful
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Payout_Successful)
            Case BanknoteOutStatus.Payout_fails
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Payout_fails)
            Case BanknoteOutStatus.Empty_note
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Empty_note)
            Case BanknoteOutStatus.Stock_less
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Stock_less)
            Case BanknoteOutStatus.Note_jam
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Note_jam)
            Case BanknoteOutStatus.Over_length
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Over_length)
            Case BanknoteOutStatus.Note_Not_Exit
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Note_Not_Exit)
            Case BanknoteOutStatus.Sensor_Error
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Sensor_Error)
            Case BanknoteOutStatus.Double_note_error
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Double_note_error)
            Case BanknoteOutStatus.Motor_Error
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Motor_Error)
            Case BanknoteOutStatus.Dispensing_busy
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Dispensing_busy)
            Case BanknoteOutStatus.Sensor_adjusting
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Sensor_adjusting)
            Case BanknoteOutStatus.Checksum_Error
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Checksum_Error)
            Case BanknoteOutStatus.Low_power_Error
                UpdateDeviceStatus(DeviceID, BanknoteOutStatus.Low_power_Error)
        End Select
    End Sub

    Private Sub DataReceivedCoinOut1(ByVal ReceiveData As String)
        UpdateDataReceivedCoinOut(ReceiveData, DeviceID.CoinOut_1)
    End Sub
    Private Sub DataReceivedCoinOut2(ByVal ReceiveData As String)
        UpdateDataReceivedCoinOut(ReceiveData, DeviceID.CoinOut_2)
    End Sub
    Private Sub DataReceivedCoinOut5(ByVal ReceiveData As String)
        UpdateDataReceivedCoinOut(ReceiveData, DeviceID.CoinOut_5)
    End Sub
    Private Sub DataReceivedCoinOut10(ByVal ReceiveData As String)
        UpdateDataReceivedCoinOut(ReceiveData, DeviceID.CoinOut_10)
    End Sub

    Private Sub UpdateDataReceivedCoinOut(ByVal ReceiveData As String, ByVal DeviceID As String)
        Select Case ReceiveData
            Case CoinOutStatus.Ready
                UpdateDeviceStatus(DeviceID, CoinOutStatus.Ready)
            Case CoinOutStatus.Disconnected
                UpdateDeviceStatus(DeviceID, CoinOutStatus.Disconnected)
            Case CoinOutStatus.Enable_BA_if_hopper_problems_recovered
                UpdateDeviceStatus(DeviceID, CoinOutStatus.Enable_BA_if_hopper_problems_recovered)
            Case CoinOutStatus.Inhibit_BA_if_hopper_problems_occurred
                UpdateDeviceStatus(DeviceID, CoinOutStatus.Inhibit_BA_if_hopper_problems_occurred)
            Case CoinOutStatus.Mortor_Problem
                UpdateDeviceStatus(DeviceID, CoinOutStatus.Mortor_Problem)
            Case CoinOutStatus.Insufficient_Coin
                UpdateDeviceStatus(DeviceID, CoinOutStatus.Insufficient_Coin)
            Case CoinOutStatus.Dedects_coin_dispensing_activity_after_suspending_the_dispene_signal
                UpdateDeviceStatus(DeviceID, CoinOutStatus.Dedects_coin_dispensing_activity_after_suspending_the_dispene_signal)
            Case CoinOutStatus.Reserved
                UpdateDeviceStatus(DeviceID, CoinOutStatus.Reserved)
            Case CoinOutStatus.Prism_Sersor_Failure
                UpdateDeviceStatus(DeviceID, CoinOutStatus.Prism_Sersor_Failure)
            Case CoinOutStatus.Shaft_Sersor_Failure
                UpdateDeviceStatus(DeviceID, CoinOutStatus.Shaft_Sersor_Failure)
        End Select
    End Sub

    Public Function CheckStockAndStatusAllDevice() As String
        Dim DeviceMsg As String = ""
        Dim StockMsg As String = ""
        Try
            'จะต้องคิวรี่ใหม่ทุกครั้ง เพราะ Status อาจมีการเปลี่ยนแปลง
            Dim sql As String = "select ms_device_id,ms_device_status_id,max_qty,warning_qty, critical_qty, current_qty"
            sql += " from MS_VENDING_DEVICE "
            sql += " where ms_kiosk_id=@_KIOSK_ID"

            'Dim p(1) As SqlParameter
            'p(0) = KioskDB.SetBigInt("@_KIOSK_ID", VendingData.VendingID)

            'Dim dt As DataTable = KioskDB.ExecuteTable(sql, Nothing, p)
            'If dt.Rows.Count > 0 Then
            '    InsertLogTransactionActivity(Customer.DepositTransNo, Pickup.TransactionNo, StaffConsole.TransNo, VendingConfigData.VendingForm.Home, VendingConfigData.KioskLockerStep.Home_CheckHardwareStatus, "", False)
            '    For i As Integer = 0 To dt.Rows.Count - 1
            '        Dim dr As DataRow = dt.Rows(i)
            '        Dim StatusName As String = ""
            '        Dim StatusIsProblem As String = ""
            '        Dim DeviceName As String = ""
            '        Dim DeviceTypeId As Long = 0
            '        Dim movement As String = ""
            '        Dim DeviceID As Long = dr("ms_device_id")

            '        DeviceStatusList.DefaultView.RowFilter = "id='" & dr("ms_device_status_id") & "'"
            '        If DeviceStatusList.DefaultView.Count > 0 Then
            '            StatusName = DeviceStatusList.DefaultView(0)("status_name")
            '            StatusIsProblem = DeviceStatusList.DefaultView(0)("is_problem")
            '        End If
            '        DeviceStatusList.DefaultView.RowFilter = ""

            '        DeviceInfoList.DefaultView.RowFilter = "device_id='" & DeviceID & "'"
            '        If DeviceInfoList.DefaultView.Count > 0 Then
            '            DeviceName = DeviceInfoList.DefaultView(0)("device_name_en")
            '            DeviceTypeId = DeviceInfoList.DefaultView(0)("device_type_id")

            '            If Convert.IsDBNull(DeviceInfoList.DefaultView(0)("movement_direction")) = False Then
            '                movement = DeviceInfoList.DefaultView(0)("movement_direction")  '1 คือ ฝั่งรับ,  -1 คือ ฝั่งจ่าย,  0 คือ ไม่นับ Stock
            '            Else
            '                movement = "0"
            '            End If
            '        End If
            '        DeviceInfoList.DefaultView.RowFilter = ""

            '        If StatusIsProblem = "Y" Then
            '            DeviceMsg = DeviceMsg & DeviceName & " Status Is " & StatusName & vbCrLf
            '        End If


            '        Dim CriticalQty As Integer = Convert.ToInt16(dr("critical_qty"))
            '        Dim CurrentQty As Integer = Convert.ToInt16(dr("current_qty"))
            '        Dim IsAlarm As Boolean = False
            '        Select Case DeviceTypeId
            '            Case DeviceType.BanknoteIn, DeviceType.BanknoteOut, DeviceType.CoinIn, DeviceType.CoinOut, DeviceType.Printer
            '                If CInt(movement) = 1 Then
            '                    If CInt(CurrentQty) >= CInt(CriticalQty) Then
            '                        StockMsg = StockMsg & DeviceName & " Stock Is Critical" & vbCrLf
            '                        IsAlarm = True
            '                    End If
            '                ElseIf CInt(movement) = -1 Then
            '                    If CurrentQty <= CInt(CriticalQty) Then
            '                        StockMsg = StockMsg & DeviceName & " Stock Is Critical" & vbCrLf
            '                        IsAlarm = True
            '                    End If
            '                End If
            '        End Select


            '        Dim AlarmProblem As String = ""
            '        Select Case DeviceTypeId
            '            Case DeviceType.BanknoteIn
            '                AlarmProblem = "CASH_IN_STOCK_CRITICAL"
            '            Case DeviceType.BanknoteOut
            '                AlarmProblem = "CASH_OUT_STOCK_CRITICAL"
            '            Case DeviceType.CoinIn
            '                AlarmProblem = "COIN_IN_STOCK_CRITICAL"
            '            Case DeviceType.CoinOut
            '                AlarmProblem = "COIN_OUT_STOCK_CRITICAL"
            '            Case DeviceType.Printer
            '                AlarmProblem = "PRINTER_STOCK_CRITICAL"
            '        End Select
            '        SendKioskAlarm(AlarmProblem, IsAlarm)
            '    Next

            '    InsertLogTransactionActivity(Customer.DepositTransNo, Pickup.TransactionNo, StaffConsole.TransNo, VendingConfigData.VendingForm.Home, VendingConfigData.KioskLockerStep.Home_CheckStockQty, "", False)
            'End If

            'dt.Dispose()
        Catch ex As Exception
            'InsertErrorLog("Exceptin : " & ex.Message & vbNewLine & ex.StackTrace, Customer.DepositTransNo, Pickup.TransactionNo, StaffConsole.TransactionID, VendingConfig.SelectForm, 0)
            DeviceMsg += "Check Device Status Fail"
        End Try

        Return DeviceMsg & StockMsg
    End Function
#End Region


    Public Sub ShowFormError(ByVal Header As String, ByVal Detail As String, ByVal MsAppScreenID As Long)
        Try
            InsertLogSalesActivity(vTrans.TransNo, Header & vbNewLine & Detail, MsAppScreenID, True)

            'Dim f_err As New frmLockerError
            'f_err.MdiParent = frmMain
            'f_err.Show()
            'frmMain.CloseAllChildForm(f_err)
            frmHomeVertical.lblPointer.Visible = True

        Catch ex As Exception

        End Try
    End Sub

#Region "Transection"
    Public Function CreateNewSaleTransaction() As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim lnq As New TbVendingSaleTransactionVendingLinqDB
            lnq.TRANS_NO = genNewTransectionNo()
            lnq.TRANS_START_TIME = DateTime.Now
            lnq.MS_VENDING_ID = VendingData.VendingID


            Dim trans As New VendingTransactionDB
            ret = lnq.InsertData(VendingData.MacAddress, trans.Trans)
            If ret.IsSuccess = True Then
                trans.CommitTransaction()

                vTrans.TransactionID = lnq.ID
                vTrans.TransStatus = SalesTransactionData.TransactionStatus.Inprogress
            Else
                trans.RollbackTransaction()
            End If
            lnq = Nothing

        Catch ex As Exception
            ret = New ExecuteDataInfo
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception | " & ex.Message & vbNewLine & ex.StackTrace
        End Try
        Return ret
    End Function

    Public Function UpdateSaleTransaction(vTrans As SalesTransactionData) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim lnq As New TbVendingSaleTransactionVendingLinqDB
            lnq.GetDataByPK(vTrans.TransactionID, Nothing)
            If lnq.ID > 0 Then
                lnq.TRANS_END_TIME = DateTime.Now
                lnq.MS_VENDING_ID = VendingData.VendingID
                lnq.MS_PRODUCT_ID = vTrans.ProductID
                lnq.SHELF_ID = vTrans.ShelfID
                lnq.PRODUCT_COST = vTrans.ProductCost
                lnq.PRODUCT_PRICE = vTrans.ProductPrice
                lnq.PRODUCT_DISCOUNT_PERCENT = vTrans.DiscountP
                lnq.PRODUCT_NETPRICE = vTrans.NetPrice
                lnq.PRODUCT_PROFIT_SHARING = vTrans.ProfitSharing
                lnq.PAID_SUCCESS_TIME = vTrans.PaidTime
                lnq.RECEIVE_COIN1 = vTrans.ReceiveCoin1
                lnq.RECEIVE_COIN2 = vTrans.ReceiveCoin2
                lnq.RECEIVE_COIN5 = vTrans.ReceiveCoin5
                lnq.RECEIVE_COIN10 = vTrans.ReceiveCoin10
                lnq.RECEIVE_BANKNOTE20 = vTrans.ReceiveBankNote20
                lnq.RECEIVE_BANKNOTE50 = vTrans.ReceiveBankNote50
                lnq.RECEIVE_BANKNOTE100 = vTrans.ReceiveBankNote100
                lnq.RECEIVE_BANKNOTE500 = vTrans.ReceiveBankNote500
                lnq.RECEIVE_BANKNOTE1000 = vTrans.ReceiveBankNote1000

                lnq.CHANGE_COIN1 = vTrans.ChangeCoin1
                lnq.CHANGE_COIN2 = vTrans.ChangeCoin2
                lnq.CHANGE_COIN5 = vTrans.ChangeCoin5
                lnq.CHANGE_COIN10 = vTrans.ChangeCoin10
                lnq.CHANGE_BANKNOTE20 = vTrans.ChangeBankNote20
                lnq.CHANGE_BANKNOTE50 = vTrans.ChangeBankNote50
                lnq.CHANGE_BANKNOTE100 = vTrans.ChangeBankNote100
                lnq.CHANGE_BANKNOTE500 = vTrans.ChangeBankNote500
                lnq.TRANS_STATUS = Convert.ToInt16(vTrans.TransStatus).ToString
                lnq.MS_APP_STEP_ID = Convert.ToInt16(VendingConfig.SelectForm)
                lnq.SYNC_TO_SERVER = "N"

                Dim trans As New VendingLinqDB.ConnectDB.VendingTransactionDB
                ret = lnq.UpdateData(Environment.MachineName, trans.Trans)
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                Else
                    trans.RollbackTransaction()
                    InsertLogSalesActivity(vTrans.TransNo, ret.ErrorMessage, VendingConfig.SelectForm, True)
                End If
            Else
                InsertLogSalesActivity(vTrans.TransNo, lnq.ErrorMessage, VendingConfig.SelectForm, True)
            End If
            lnq = Nothing
        Catch ex As Exception
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception UpdateSaleTransaction : " & ex.Message & vbNewLine & ex.StackTrace
            InsertLogSalesActivity(vTrans.TransNo, ret.ErrorMessage, VendingConfig.SelectForm, True)
        End Try

        Return ret
    End Function



    Public Function CreateNewStaffConsoleTransaction(vUserName As String, FirstName As String, LastName As String, CompanyName As String, LoginBy As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim lnq As New TbStaffConsoleTransactionVendingLinqDB
            lnq.TRANS_NO = genNewTransectionNo()
            lnq.TRANS_START_TIME = DateTime.Now
            lnq.MS_VENDING_ID = VendingData.VendingID
            lnq.LOGIN_USERNAME = vUserName
            lnq.LOGIN_FIRST_NAME = FirstName
            lnq.LOGIN_LAST_NAME = LastName
            lnq.LOGIN_COMPANY_NAME = CompanyName
            lnq.LOGIN_BY = LoginBy

            Dim trans As New VendingTransactionDB
            ret = lnq.InsertData(VendingData.IpAddress, trans.Trans)
            If ret.IsSuccess = True Then
                trans.CommitTransaction()

                StaffConsole.TransNo = lnq.TRANS_NO
                StaffConsole.TransactionID = lnq.ID
                StaffConsole.StartTime = lnq.TRANS_START_TIME
                StaffConsole.FirstName = lnq.LOGIN_FIRST_NAME
                StaffConsole.LastName = lnq.LOGIN_LAST_NAME
                StaffConsole.LoginTime = lnq.TRANS_START_TIME
                StaffConsole.Username = vUserName
                StaffConsole.LoginBy = lnq.LOGIN_BY
            Else
                trans.RollbackTransaction()
            End If
        Catch ex As Exception
            ret = New ExecuteDataInfo
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception | " & ex.Message & vbNewLine & ex.StackTrace
            'InsertErrorLog("Exception | " & ex.Message & vbNewLine & ex.StackTrace, Customer.DepositTransNo, Pickup.TransactionNo, StaffConsole.TransactionID, VendingConfig.SelectForm, 0)
        End Try
        Return ret
    End Function

    Public Function UpdateKioskCurrentQty(ByVal DeviceID As DeviceID, ByVal Value As Integer, MoneyValue As Integer, ByVal FillSkock As Boolean) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim CurrentQty As Integer = 0
            Dim dLnq As New MsVendingDeviceVendingLinqDB
            dLnq.ChkDataByMS_DEVICE_ID_MS_VENDING_ID(DeviceID, VendingData.VendingID, Nothing)

            If dLnq.ID > 0 Then
                Dim CurrentMoney As Integer = 0

                If FillSkock = True Then
                    'ถ้าเป็นการเติมของใส่ Stock
                    CurrentQty = Value
                    CurrentMoney = MoneyValue
                Else
                    CurrentQty = dLnq.CURRENT_QTY + Value

                    If dLnq.CURRENT_MONEY Is Nothing Then
                        dLnq.CURRENT_MONEY = 0
                    End If
                    CurrentMoney = dLnq.CURRENT_MONEY.Value + MoneyValue
                End If

                dLnq.CURRENT_QTY = CurrentQty
                dLnq.CURRENT_MONEY = CurrentMoney
                dLnq.SYNC_TO_VENDING = "Y"
                dLnq.SYNC_TO_SERVER = "N"

                Dim trans As New VendingTransactionDB
                ret = dLnq.UpdateData(VendingData.ComputerName, trans.Trans)
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()

                    Dim DeviceTypeId As Long = 0
                    DeviceInfoList.DefaultView.RowFilter = "device_id='" & DeviceID & "'"
                    If DeviceInfoList.DefaultView.Count > 0 Then
                        DeviceTypeId = DeviceInfoList.DefaultView(0)("device_type_id")

                        If Convert.IsDBNull(DeviceInfoList.DefaultView(0)("movement_direction")) = False Then
                            '1 คือ ฝั่งรับ,  -1 คือ ฝั่งจ่าย,  0 คือ ไม่นับ Stock
                            Dim movement As String = DeviceInfoList.DefaultView(0)("movement_direction")
                            Dim IsAlarm As Boolean = False

                            If CInt(movement) = 1 Then
                                If CInt(CurrentQty) >= dLnq.CRITICAL_QTY Then
                                    IsAlarm = True
                                End If
                            ElseIf CInt(movement) = -1 Then
                                If CurrentQty <= dLnq.CRITICAL_QTY Then
                                    IsAlarm = True
                                End If
                            End If

                            Dim AlarmProblem As String = ""
                            Select Case DeviceTypeId
                                Case DeviceType.BanknoteIn
                                    AlarmProblem = "CASH_IN_STOCK_CRITICAL"
                                Case DeviceType.BanknoteOut
                                    AlarmProblem = "CASH_OUT_STOCK_CRITICAL"
                                Case DeviceType.CoinIn
                                    AlarmProblem = "COIN_IN_STOCK_CRITICAL"
                                Case DeviceType.CoinOut
                                    AlarmProblem = "COIN_OUT_STOCK_CRITICAL"
                                Case DeviceType.Printer
                                    AlarmProblem = "PRINTER_STOCK_CRITICAL"
                            End Select

                            SendKioskAlarm(AlarmProblem, IsAlarm)
                        End If
                    End If
                    DeviceInfoList.DefaultView.RowFilter = ""
                Else
                    trans.RollbackTransaction()
                    'InsertErrorLog(re.ErrorMessage, Customer.DepositTransNo, Pickup.TransactionNo, StaffConsole.TransactionID, VendingConfig.SelectForm, 0)
                End If
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = dLnq.ErrorMessage
                'InsertErrorLog(dLnq.ErrorMessage, Customer.DepositTransNo, Pickup.TransactionNo, StaffConsole.TransactionID, VendingConfig.SelectForm, 0)
            End If
        Catch ex As Exception
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception : " & ex.Message & vbNewLine & ex.StackTrace
            'InsertErrorLog("Exception : " & ex.Message & vbNewLine & ex.StackTrace, Customer.DepositTransNo, Pickup.TransactionNo, StaffConsole.TransactionID, VendingConfig.SelectForm, 0)
        End Try

        Return ret
    End Function

    Public Sub InsertLogStaffConsoleActivity(StaffConsoleTransNo As String, LogMsg As String, MsAppScreenID As Long, IsProblem As Boolean)
        Dim frame As StackFrame = New StackFrame(1, True)
        Dim ClassName As String = frame.GetMethod.ReflectedType.Name
        Dim FunctionName As String = frame.GetMethod.Name
        Dim LineNo As Integer = frame.GetFileLineNumber

        Dim CreateBy As String = ClassName & "." & FunctionName
        Try

            Dim lnq As New TbLogVendingActivityVendingLinqDB
            lnq.MS_VENDING_ID = VendingData.VendingID
            lnq.TRANS_DATE = DateTime.Now
            lnq.STAFF_CONSOLE_TRANS_NO = StaffConsoleTransNo
            lnq.MS_APP_SCREEN_ID = MsAppScreenID
            lnq.LOG_DESC = LogMsg
            lnq.IS_PROBLEM = IIf(IsProblem = True, "Y", "N")
            lnq.SYNC_TO_SERVER = "N"

            Dim trans As New VendingTransactionDB
            Dim ret As ExecuteDataInfo = lnq.InsertData(CreateBy, trans.Trans)
            If ret.IsSuccess = True Then
                trans.CommitTransaction()
                SendKioskAlarm("KIOSK_ERROR_INSERT_LOG_ACTIVITY", False)
            Else
                trans.RollbackTransaction()

                SendKioskAlarm("KIOSK_ERROR_INSERT_LOG_ACTIVITY", True)
                Dim _Err As String = ret.ErrorMessage & vbNewLine
                _Err += "ClassName=" & ClassName & "&FunctionName=" & FunctionName & vbNewLine
                _Err += "&StaffConsoleTransNo=" & StaffConsoleTransNo & "&MsAppScreenID=" & MsAppScreenID & vbNewLine
                _Err += "&LogMsg=" & LogMsg & "&IsProblem=" & IsProblem

                Engine.LogFileENG.CreateVendingErrorLog(ClassName, _Err, VendingData.VendingID)
            End If
        Catch ex As Exception
            SendKioskAlarm("KIOSK_ERROR_INSERT_LOG_ACTIVITY", True)
            Dim _Err As String = "Exception : " & ex.Message & " " & ex.StackTrace & vbNewLine
            _Err += "ClassName=" & ClassName & "&FunctionName=" & FunctionName & vbNewLine
            _Err += "&StaffConsoleTransNo=" & StaffConsoleTransNo & vbNewLine
            _Err += "&MsAppScreenID=" & MsAppScreenID & vbNewLine
            _Err += "&LogMsg=" & LogMsg & "&IsProblem=" & LogMsg

            Engine.LogFileENG.CreateVendingErrorLog(ClassName, _Err, VendingData.VendingID)
        End Try
    End Sub
    Public Sub InsertLogSalesActivity(SalesTransNo As String, LogMsg As String, MsAppScreenID As Long, IsProblem As Boolean)
        Dim frame As StackFrame = New StackFrame(1, True)
        Dim ClassName As String = frame.GetMethod.ReflectedType.Name
        Dim FunctionName As String = frame.GetMethod.Name
        Dim LineNo As Integer = frame.GetFileLineNumber

        Dim CreateBy As String = ClassName & "." & FunctionName
        'Dim MsAppScreenID As Long = 0
        Try
            'AppScreenList.DefaultView.RowFilter = "form_name='" & ClassName & "'"
            'If AppScreenList.DefaultView.Count > 0 Then
            '    MsAppScreenID = AppScreenList.DefaultView(0)("id")
            'End If
            'AppScreenList.DefaultView.RowFilter = ""

            Dim lnq As New TbLogVendingActivityVendingLinqDB
            lnq.MS_VENDING_ID = VendingData.VendingID
            lnq.TRANS_DATE = DateTime.Now
            lnq.SALES_TRANS_NO = SalesTransNo
            lnq.MS_APP_SCREEN_ID = MsAppScreenID
            lnq.LOG_DESC = LogMsg
            lnq.IS_PROBLEM = IIf(IsProblem = True, "Y", "N")
            lnq.SYNC_TO_SERVER = "N"

            Dim trans As New VendingTransactionDB
            Dim ret As ExecuteDataInfo = lnq.InsertData(CreateBy, trans.Trans)
            If ret.IsSuccess = True Then
                trans.CommitTransaction()
                SendKioskAlarm("KIOSK_ERROR_INSERT_LOG_ACTIVITY", False)
            Else
                trans.RollbackTransaction()

                SendKioskAlarm("KIOSK_ERROR_INSERT_LOG_ACTIVITY", True)
                Dim _Err As String = ret.ErrorMessage & vbNewLine
                _Err += "ClassName=" & ClassName & "&FunctionName=" & FunctionName & vbNewLine
                _Err += "&SalesTransNo=" & SalesTransNo & "&MsAppScreenID=" & MsAppScreenID & vbNewLine
                _Err += "&LogMsg=" & LogMsg & "&IsProblem=" & IsProblem

                Engine.LogFileENG.CreateVendingErrorLog(ClassName, _Err, VendingData.VendingID)
            End If
        Catch ex As Exception

            SendKioskAlarm("KIOSK_ERROR_INSERT_LOG_ACTIVITY", True)
            Dim _Err As String = "Exception : " & ex.Message & " " & ex.StackTrace & vbNewLine
            _Err += "ClassName=" & ClassName & "&FunctionName=" & FunctionName & vbNewLine
            _Err += "&SalesTransNo=" & SalesTransNo & vbNewLine
            _Err += "&MsAppScreenID=" & MsAppScreenID & vbNewLine
            _Err += "&LogMsg=" & LogMsg & "&IsProblem=" & LogMsg
            Engine.LogFileENG.CreateVendingErrorLog(ClassName, _Err, VendingData.VendingID)
        End Try
    End Sub


#End Region

#Region "Call Webservice VDM"

    Public Function LogInSSO(ByVal Username As String, ByVal Password As String, WS As VDMWebService.ATBVendingWebservice) As VDMWebService.LoginReturnData
        Dim ret As New VDMWebService.LoginReturnData
        Try
            ret = WS.LoginTIT(Username, Password, "VDM Staff Console")
        Catch ex As Exception
            ret = New VDMWebService.LoginReturnData
        End Try

        Return ret
    End Function

#End Region

#Region "Call Webservice Alarm"
    Public Sub SendKioskAlarm(AlarmProblem As String, IsProblem As Boolean)
        Try
            If AlarmMasterList.Rows.Count > 0 Then
                AlarmMasterList.DefaultView.RowFilter = "alarm_problem='" & AlarmProblem & "'"
                If AlarmMasterList.DefaultView.Count > 0 Then
                    Dim ws As New AlarmWebReference.ApplicationWebservice
                    ws.Timeout = 10000
                    ws.Url = VendingConfig.WebServiceAlarmURL
                    'ws.Url = "http://localhost:37814/ApplicationWebservice.asmx?WSDL"

                    Dim ret As String = ws.SendAlarmOtherApp(VendingData.MacAddress, AlarmMasterList.DefaultView.ToTable.Copy, IsProblem, VendingData.LocationName, "Autobox Service Info " & VendingData.LocationName, "Alarm Autobox " & AlarmProblem)
                    ws.Dispose()
                End If
                AlarmMasterList.DefaultView.RowFilter = ""
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

    Public Sub GetVendingConfig()
        'กำหนดค่า Config ของ Kiosk ทุกครั้งที่เข้าหน้าแรก
        Dim VendingSelectForm As VendingConfigData.VendingForm = VendingConfigData.VendingForm.StaffConsoleStockAndHardware
        Try
            Dim sql As String = "select top 1 *"
            sql += " from CF_VENDING_SYSCONFIG "
            Dim dt As DataTable = VendingDB.ExecuteTable(sql, Nothing, Nothing)
            If dt.Rows.Count > 0 Then
                Dim dr As DataRow = dt.Rows(0)
                VendingData.VendingID = Convert.ToInt16(dr("ms_vending_id"))

                VendingConfig = New VendingConfigData(VendingData.VendingID)
                VendingConfig.WebserviceVDMURL = dr("VDM_WEBSERVICE_URL")
                VendingConfig.WebServiceAlarmURL = dr("ALARM_WEBSERVICE_URL")
                VendingConfig.ScreenSaverSec = dr("SCREEN_SAVER_SEC")
                VendingConfig.ScreenLayout = dr("SCREEN_LAYOUT")
                VendingConfig.TimeOutSec = dr("TIME_OUT_SEC")
                VendingConfig.ShowMsgSec = dr("SHOW_MSG_SEC")
                VendingConfig.PaymentExtendSec = dr("PAYMENT_EXTEND_SEC")
                VendingConfig.SleepTime = dr("SLEEP_TIME")
                VendingConfig.SleepDuration = dr("SLEEP_DURATION")
                VendingConfig.ShelfLong = dr("SHELF_LONG")
                VendingConfig.SelectForm = VendingSelectForm
            End If
            dt.Dispose()

            InsertLogSalesActivity("", "ตรวจสอบการตั้งค่า Config ของตู้ขายของ", VendingSelectForm, False)
        Catch ex As Exception
            ShowFormError("Attention", "Cannot get Vendiing setting information", VendingSelectForm)
        End Try
    End Sub

    Public Sub GetVendingDeviceConfig()
        Try
            ''กำหนดค่า Hardware Config COMPORT
            Dim dDt As DataTable = DeviceInfoList
            If dDt.Rows.Count > 0 Then
                For i As Integer = 0 To dDt.Rows.Count - 1
                    Dim dDr As DataRow = dDt.Rows(i)
                    If dDr("type_active_status") = "Y" AndAlso dDr("device_active_status") = "Y" Then

                        Select Case Convert.ToInt16(dDr("device_id"))
                            Case DeviceID.BankNoteIn
                                If Convert.IsDBNull(dDr("comport_vid")) = False Then
                                    VendingConfig.BanknoteInComport = dDr("comport_vid")
                                End If
                            Case DeviceID.BankNoteOut_20
                                If Convert.IsDBNull(dDr("comport_vid")) = False Then
                                    VendingConfig.BanknoteOUT20Comport = dDr("comport_vid")
                                End If

                            Case DeviceID.BankNoteOut_50
                                If Convert.IsDBNull(dDr("comport_vid")) = False Then
                                    VendingConfig.BanknoteOUT50Comport = dDr("comport_vid")
                                End If

                            Case DeviceID.BankNoteOut_100
                                If Convert.IsDBNull(dDr("comport_vid")) = False Then
                                    VendingConfig.BanknoteOUT100Comport = dDr("comport_vid")
                                End If

                            Case DeviceID.BankNoteOut_500
                                If Convert.IsDBNull(dDr("comport_vid")) = False Then
                                    VendingConfig.BanknoteOUT500Comport = dDr("comport_vid")
                                End If

                            Case DeviceID.CoinIn
                                If Convert.IsDBNull(dDr("comport_vid")) = False Then
                                    VendingConfig.CoinInComport = dDr("comport_vid")
                                End If

                            Case DeviceID.CoinOut_1
                                If Convert.IsDBNull(dDr("comport_vid")) = False Then
                                    VendingConfig.CoinOut1Comport = dDr("comport_vid")
                                End If

                            Case DeviceID.CoinOut_2
                                If Convert.IsDBNull(dDr("comport_vid")) = False Then
                                    VendingConfig.CoinOut2Comport = dDr("comport_vid")
                                End If

                            Case DeviceID.CoinOut_5
                                If Convert.IsDBNull(dDr("comport_vid")) = False Then
                                    VendingConfig.CoinOut5Comport = dDr("comport_vid")
                                End If

                            Case DeviceID.CoinOut_10
                                If Convert.IsDBNull(dDr("comport_vid")) = False Then
                                    VendingConfig.CoinOut10Comport = dDr("comport_vid")
                                End If

                            Case DeviceID.Printer
                                If Convert.IsDBNull(dDr("driver_name1")) = False Then
                                    VendingConfig.PrinterDeviceName = dDr("driver_name1")
                                End If
                            Case DeviceID.NetworkConnection
                        End Select
                    End If
                Next
                InsertLogSalesActivity("", "โหลดข้อมูลการตั้งค่าการเชื่อมต่ออุปกรณ์", VendingConfig.SelectForm, False)
            Else
                ShowFormError("", "Cannot get Kiosk config information", VendingConfig.SelectForm)
            End If
            dDt.Dispose()
        Catch ex As Exception
            ShowFormError("", "Exception :   " & ex.Message & vbNewLine & ex.StackTrace, VendingConfig.SelectForm)
        End Try
    End Sub

    Public Sub GetMasterProductInfo(WS As VDMWebService.ATBVendingWebservice)
        Try
            ProductMasterList = WS.GetMasterProductInfoList(VendingData.VendingID)

            Dim strProductID As String = ""
            If ProductMasterList.Rows.Count > 0 Then
                For Each dr As DataRow In ProductMasterList.Rows
                    If strProductID = "" Then
                        strProductID = dr("ms_product_id")
                    Else
                        strProductID += "," & dr("ms_product_id")
                    End If
                Next

                ProductWarrantyList = WS.GetMasterProductWarrantyList(strProductID)

            End If

            'Dim sql As String = "select p.id ms_product_id,  p.product_code,p.product_name_th,p.product_name_en, p.product_name_jp, p.product_name_ch, "
            'sql += " p.product_desc_th, p.product_desc_en, p.product_desc_jp, p.product_desc_ch, "
            'sql += " p.product_image_big,p.product_image_small, p.cost,p.price, "
            'sql += " p.ms_product_category_id, pc.category_code,pc.category_name, vp.profit_sharing "
            'sql += " from MS_PRODUCT p "
            'sql += " inner join MS_PRODUCT_CATEGORY pc on pc.id=p.ms_product_category_id "
            'sql += " inner join MS_VENDING_PRODUCT vp on p.id=vp.ms_product_id "
            'sql += " where vp.ms_vending_id = @_VENDING_ID"
            'Dim p(1) As SqlParameter
            'p(0) = ServerLinqDB.ConnectDB.ServerDB.SetBigInt("@_VENDING_ID", VendingData.VendingID)

            'ProductMasterList = ServerLinqDB.ConnectDB.ServerDB.ExecuteTable(sql, p)

            'sql = "select pw.id ms_product_warranty_id, pw.ms_product_id, pw.warranty_th, pw.warranty_en, pw.warranty_jp, pw.warranty_ch"
            'sql += " from MS_PRODUCT_WARRANTY pw"
            'ProductWarrantyList = ServerLinqDB.ConnectDB.ServerDB.ExecuteTable(sql)
        Catch ex As Exception
            ShowFormError("Exception", ex.Message & vbNewLine & ex.StackTrace, VendingConfig.SelectForm)
        End Try

    End Sub

    Public Sub GetMasterProductPromotion(WS As VDMWebService.ATBVendingWebservice)
        ProductPromotionList = New DataTable
        ProductPromotionList = WS.GetMasterProductPromotion(VendingData.VendingID)
    End Sub

    Public Sub SetChildFormLanguage()
        Dim fldName As String = ""
        Select Case VendingConfig.Language
            Case Data.ConstantsData.VendingLanguage.Thai
                fldName = "TH_Display"
            Case Data.ConstantsData.VendingLanguage.English
                fldName = "EN_Display"
            Case Data.ConstantsData.VendingLanguage.China
                fldName = "CH_Display"
            Case Data.ConstantsData.VendingLanguage.Japan
                fldName = "JP_Display"
        End Select

        Try
            LangMasterList.DefaultView.RowFilter = "ms_app_screen_id='" & Convert.ToInt16(VendingConfig.SelectForm) & "'"
            If LangMasterList.DefaultView.Count > 0 Then
                AppScreenList.DefaultView.RowFilter = "id='" & Convert.ToInt16(VendingConfig.SelectForm) & "'"
                If AppScreenList.DefaultView.Count > 0 Then
                    Dim frm As Form = Application.OpenForms(AppScreenList.DefaultView(0)("form_name"))

                    For Each dr As DataRowView In LangMasterList.DefaultView
                        Dim ControlName As String = dr("Control_Name")
                        Dim cc() As Control = frm.Controls.Find(ControlName, True)
                        If cc.Length > 0 Then
                            cc(0).Text = dr(fldName)
                        End If
                    Next
                End If
                AppScreenList.DefaultView.RowFilter = ""
            End If
            LangMasterList.DefaultView.RowFilter = ""

            'If VendingConfig.SelectForm = VendingConfigData.VendingForm.DepositPayment Or VendingConfig.SelectForm = VendingConfigData.VendingForm.PickupPayment Then
            '    frmDepositPayment.SetControlLanguage()
            'End If
        Catch ex As Exception

        End Try
    End Sub



    Public Sub SetShelfLayout(pnl As Panel, IsViewHome As Boolean)
        pnl.Controls.Clear()

        Dim sql As String = "select id, shelf_id, position_row, position_column, position_x, position_y, width " & Environment.NewLine
        sql += " From MS_VENDING_SHELF" & Environment.NewLine
        sql += " where ms_vending_id=@_VENDING_ID" & Environment.NewLine
        sql += " and active_status='Y'" & Environment.NewLine
        sql += " order by position_row, position_column" & Environment.NewLine

        Dim p(1) As SqlParameter
        p(0) = VendingDB.SetBigInt("@_VENDING_ID", VendingData.VendingID)

        Dim dt As DataTable = VendingDB.ExecuteTable(sql, p)
        If dt.Rows.Count > 0 Then
            Dim ShelfRowImg As Image = Image.FromFile(Application.StartupPath & "\Background\bgShelfRow.jpg")
            'Dim ShelfColImg As Image = Image.FromFile(Application.StartupPath & "\Background\bgShelfCol.jpg")

            Dim RowQty As Integer = Convert.ToInt16(dt.Rows(dt.Rows.Count - 1)("position_row"))
            Dim ShelfHeight As Integer = ShelfRowImg.Height
            Dim ShelfDistanct As Integer = Math.Ceiling(pnl.Height / (RowQty + 1))
            Dim ShelfTop As Integer = ShelfDistanct

            For i As Integer = 1 To RowQty
                'Add Shelf
                Dim pbShelf As New PictureBox
                pbShelf.Width = pnl.Width
                pbShelf.Height = ShelfHeight
                pbShelf.Top = ShelfTop
                pbShelf.Left = 0
                pbShelf.Image = ShelfRowImg
                pbShelf.SizeMode = PictureBoxSizeMode.StretchImage
                pnl.Controls.Add(pbShelf)

                dt.DefaultView.RowFilter = "position_row=" & i
                If dt.DefaultView.Count > 0 Then
                    Dim cDt As New DataTable
                    cDt = dt.DefaultView.ToTable().Copy

                    Dim SumColWidth As Integer = Convert.ToInt16(cDt.Compute("sum(width)", ""))

                    Dim ColDistanct As Integer = CalColDistance(SumColWidth, cDt.Rows.Count, 50, pnl.Width) 'Math.Floor((pbShelf.Width - 20) / cDt.Rows.Count)
                    Dim ColLeft As Integer = pbShelf.Left + ((50 * pnl.Width) / VendingConfig.ShelfLong)
                    Dim ColTop As Integer = pbShelf.Top

                    'Add Shelf Column
                    For j As Integer = 0 To cDt.Rows.Count - 1
                        'Find Product at Shelf Column
                        sql = "select ps.ms_product_id, ps.shelf_id, ps.product_qty "
                        sql += " from MS_PRODUCT_SHELF ps "
                        sql += " where  ps.shelf_id=@_SHELF_ID"
                        If IsViewHome = True Then
                            'กรณีเป็นการแสดงข้อมูลในหน้า Home ให้แสดงเฉพาะชั้นที่มีสินค้าเท่านั้น
                            sql += " and  ps.product_qty>0"
                        End If
                        Dim parm(1) As SqlParameter
                        parm(0) = VendingDB.SetInt("@_SHELF_ID", cDt.Rows(j)("shelf_id"))

                        Dim ColWidth As Integer = 0

                        Dim pDt As DataTable = VendingDB.ExecuteTable(sql, parm)
                        If pDt.Rows.Count > 0 Then
                            Dim pDr As DataRow = pDt.Rows(0)
                            'จะต้องมีสินค้าอยู่บนชั้นถึงจะแสดงภาพสินค้า
                            'Add Product To Shelf Column
                            ProductMasterList.DefaultView.RowFilter = "ms_product_id= " & pDr("ms_product_id")
                            If ProductMasterList.DefaultView.Count > 0 Then
                                Dim pmDrv As DataRowView = ProductMasterList.DefaultView(0)

                                Using pBytes As New IO.MemoryStream(CType(pmDrv("product_image_small"), Byte()))
                                    Dim pbImg As Image = Image.FromStream(pBytes)  'Image.FromFile(Application.StartupPath & "\ProductImage\pbProductSmallIcon.jpg")

                                    If IsViewHome = True Then
                                        Dim ctlName As String = "pdShelfID_" & cDt.Rows(j)("id") & "_PID_" & pmDrv("ms_product_id") & "_POSX_" & cDt.Rows(j)("position_x") & "_POSY_" & cDt.Rows(j)("position_y")

                                        Dim pbProd As New PictureBox
                                        pbProd.Name = ctlName
                                        pbProd.Width = pbImg.Width
                                        pbProd.Height = pbImg.Height
                                        pbProd.Top = ColTop - pbProd.Height
                                        pbProd.Left = ColLeft + 2
                                        pbProd.Image = pbImg
                                        pnl.Controls.Add(pbProd)
                                        AddHandler pbProd.Click, AddressOf pbProd_Click

                                        ColWidth = pbProd.Width
                                    Else
                                        Dim ucP As New ucProductShelf
                                        ucP.Top = ColTop - ucP.Height
                                        ucP.Left = ColLeft
                                        ucP.Width = (Convert.ToInt16(cDt.Rows(j)("width")) * pnl.Width) / VendingConfig.ShelfLong
                                        ucP.lblShelfID.Text = cDt.Rows(j)("id")
                                        ucP.lblProductID.Text = pmDrv("ms_product_id")
                                        ucP.lblPosition.Text = cDt.Rows(j)("position_x") & "," & cDt.Rows(j)("position_y")
                                        ucP.lblProductCode.Text = pmDrv("product_code")
                                        ucP.lblProductQty.Text = pDr("product_qty")
                                        ucP.Parent = pnl
                                        ColWidth = ucP.Width

                                        pnl.Controls.Add(ucP)
                                    End If
                                End Using
                            End If

                            ProductMasterList.DefaultView.RowFilter = ""
                        Else
                            If IsViewHome = False Then
                                'กรณีแสดงข้อมูลในหน้าจอ Setting ให้แสดงในกรณีที่ชั้นวางนั้นไม่มีสินค้าด้วย
                                Dim ucP As New ucProductShelf
                                ucP.Top = ColTop - ucP.Height
                                ucP.Left = ColLeft
                                ucP.Width = (Convert.ToInt16(cDt.Rows(j)("width")) * pnl.Width) / VendingConfig.ShelfLong
                                ucP.lblShelfID.Text = cDt.Rows(j)("id")
                                ucP.lblProductID.Text = 0
                                ucP.lblPosition.Text = cDt.Rows(j)("position_x") & "," & cDt.Rows(j)("position_y")
                                ucP.lblProductCode.Text = ""
                                ucP.lblProductQty.Text = 0
                                ucP.Parent = pnl
                                ColWidth = ucP.Width

                                pnl.Controls.Add(ucP)
                            End If
                        End If
                        pDt.Dispose()

                        ColLeft += ColDistanct + ColWidth
                    Next

                    Application.DoEvents()
                    ShelfTop += (ShelfDistanct + ShelfHeight)
                End If
                dt.DefaultView.RowFilter = ""
            Next
        End If
        dt.Dispose()
    End Sub

    Private Sub pbProd_Click(sender As Object, e As EventArgs)
        Dim pbProd As PictureBox = DirectCast(sender, PictureBox)

        Dim pInfo() As String = Split(pbProd.Name, "_")
        If pInfo.Length = 8 Then
            CreateNewSaleTransaction()

            Dim sql As String = "select ps.id, ps.shelf_id, vs.position_x, vs.position_y, vs.position_row, vs.position_column"
            sql += " from MS_PRODUCT_SHELF ps "
            sql += " inner join MS_VENDING_SHELF vs on ps.shelf_id=vs.shelf_id"
            sql += " where ps.id=@_PRODUCT_SHELF_ID"

            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_PRODUCT_SHELF_ID", pInfo(1))

            Dim pDt As DataTable = VendingDB.ExecuteTable(sql, p)
            If pDt.Rows.Count > 0 Then
                vTrans.MsProductShelfID = pDt.Rows(0)("id")
                vTrans.ShelfID = pDt.Rows(0)("shelf_id")
                vTrans.PositionX = pDt.Rows(0)("position_x")
                vTrans.PositionY = pDt.Rows(0)("position_y")
                vTrans.ShelfRow = pDt.Rows(0)("position_row")
                vTrans.ShelfColumn = pDt.Rows(0)("position_column")

                'Get Product Information
                ProductMasterList.DefaultView.RowFilter = "ms_product_id = '" & pInfo(3) & "'"
                If ProductMasterList.DefaultView.Count > 0 Then
                    Dim pdDrv As DataRowView = ProductMasterList.DefaultView(0)

                    vTrans.ProductID = pdDrv("ms_product_id")
                    vTrans.ProductCode = pdDrv("product_code")
                    vTrans.ProductCategoryID = pdDrv("ms_product_category_id")
                    vTrans.ProductCost = pdDrv("cost")
                    vTrans.ProductPrice = pdDrv("price")
                    vTrans.NetPrice = Convert.ToDouble(pdDrv("price"))
                    vTrans.ProfitSharing = pdDrv("profit_sharing")
                    vTrans.DiscountP = 0

                    'Get Data Promotion Info
                    '##########################################
                    ProductPromotionList.DefaultView.RowFilter = "ms_product_id = '" & pInfo(3) & "'"
                    If ProductPromotionList.DefaultView.Count > 0 Then
                        vTrans.DiscountP = ProductPromotionList.DefaultView(0)("discount_percent")
                    End If
                    ProductPromotionList.DefaultView.RowFilter = ""

                    If vTrans.DiscountP > 0 Then
                        vTrans.NetPrice = vTrans.NetPrice - (vTrans.NetPrice * (vTrans.DiscountP / 100))
                    End If

                    InsertLogSalesActivity(vTrans.TransNo, "เลือกสินค้า " & pdDrv("product_name_th"), VendingConfig.SelectForm, False)
                    frmProductDetailVertical.SetProductInfo()

                    Using pBytes As New IO.MemoryStream(CType(pdDrv("product_image_big"), Byte()))
                        Dim pbImg As Image = Image.FromStream(pBytes)
                        frmProductDetailVertical.pbProduct.Image = pbImg
                        frmProductDetailVertical.pbProduct.SizeMode = PictureBoxSizeMode.StretchImage
                    End Using
                End If
                ProductMasterList.DefaultView.RowFilter = ""
            End If
            pDt.Dispose()
        End If

        frmProductDetailVertical.Show()
        frmHomeVertical.TimerCheckPromotion.Enabled = False
        frmHomeVertical.Close()
    End Sub


    Public Function CalColDistance(SumWidth As Integer, ColQty As Integer, LeftRightWidth As Integer, pnlWidth As Integer) As Integer
        Dim ret As Integer = VendingConfig.ShelfLong   'ความยาวของตู้
        ret = ret - SumWidth     'หักความยาวของช่องทั้งหมด
        ret = ret - (LeftRightWidth * 2)   'หักความยาวของช่องว่างซ้ายขวา

        If (ColQty - 1) > 0 Then
            ret = ret / (ColQty - 1)   'ระยะห่างระหว่างช่อง
        End If


        'แปลงให้เป็น pixel
        ret = Math.Floor((ret * pnlWidth) / VendingConfig.ShelfLong)

        Return ret
    End Function

    Public Function SetSlipParameter(TextData As String, FieldList As DataTable, SaleTrans As Data.SalesTransactionData, ProductCode As String, ProductNameTh As String, ProductNameEn As String, ProductCategoryID As Long, CategoryCode As String, CategoryName As String) As String
        Dim ret As String = TextData
        If FieldList.Rows.Count > 0 Then

            For Each fl As DataRow In FieldList.Rows
                Dim ParmName As String = fl("parm_name")

                If TextData.IndexOf(ParmName) > -1 Then
                    Select Case ParmName
                        Case "@{TransactionNo}"
                            ret = TextData.Replace(ParmName, SaleTrans.TransNo)
                        Case "@{TransactionStartTime}"
                            ret = TextData.Replace(ParmName, SaleTrans.TransStartTime.ToString("dd/MM/yyyy HH:mm:ss", New Globalization.CultureInfo("en-US")))
                        Case "@{MsVendingID}"
                            ret = TextData.Replace(ParmName, VendingData.VendingID.ToString)
                        Case "@{VendingComName}"
                            ret = TextData.Replace(ParmName, VendingData.ComputerName)
                        Case "@{MsLocationID}"
                            ret = TextData.Replace(ParmName, VendingData.LocationID)
                        Case "@{LocationCode}"
                            ret = TextData.Replace(ParmName, VendingData.LocationCode)
                        Case "@{LocationName}"
                            ret = TextData.Replace(ParmName, VendingData.LocationName)
                        Case "@{MsProductID}"
                            ret = TextData.Replace(ParmName, SaleTrans.ProductID)
                        Case "@{ProductCode}"
                            ret = TextData.Replace(ParmName, ProductCode)
                        Case "@{ProductNameTh}"
                            ret = TextData.Replace(ParmName, ProductNameTh)
                        Case "@{ProductNameEn}"
                            ret = TextData.Replace(ParmName, ProductNameEn)
                        Case "@{MsProductCategoryID}"
                            ret = TextData.Replace(ParmName, ProductCategoryID)
                        Case "@{CategoryCode}"
                            ret = TextData.Replace(ParmName, CategoryCode)
                        Case "@{CategoryName}"
                            ret = TextData.Replace(ParmName, CategoryName)
                        Case "@{ProductNetprice}"
                            ret = TextData.Replace(ParmName, SaleTrans.NetPrice)
                        Case "@{PaidAmount}"
                            ret = TextData.Replace(ParmName, SaleTrans.PaidAmount)
                        Case "@{ChangeAmount}"
                            ret = TextData.Replace(ParmName, SaleTrans.ChangeAmount)
                    End Select
                End If
            Next
        End If
        Return ret
    End Function



End Module
