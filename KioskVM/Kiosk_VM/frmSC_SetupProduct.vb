﻿Imports System.Data
Imports System.Data.SqlClient
Imports ControllerPLC
Imports VendingLinqDB.ConnectDB
Imports VendingLinqDB.TABLE
Public Class frmSC_SetupProduct

    Private Sub frmSC_SetupProduct_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        SetProductLst()
        SetSetupProductAuthorize()
    End Sub

    Private Sub SetProductLst()
        Try
            Dim dt As New DataTable
            dt = ProductMasterList.Copy
            dgvProductList.AutoGenerateColumns = False
            dgvProductList.DataSource = dt
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SetSetupProductAuthorize()
        If StaffConsole.AuthorizeInfo.Rows.Count > 0 Then
            AppScreenList.DefaultView.RowFilter = "id='" & Convert.ToInt16(VendingConfig.SelectForm) & "'"
            If AppScreenList.DefaultView.Count > 0 Then
                numQty.Enabled = False
                btnSave.Visible = False
                dgvProductList.Enabled = False
                dgvProductList.ClearSelection()

                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & Data.ConstantsData.StaffConsoleFunctionalID.SetupProduct & " and authorization_name='Edit'"
                If StaffConsole.AuthorizeInfo.DefaultView.Count > 0 Then
                    numQty.Enabled = True
                    btnSave.Visible = True
                    dgvProductList.Enabled = True
                End If
                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""
            End If
            AppScreenList.DefaultView.RowFilter = ""
        End If
        Application.DoEvents()
    End Sub


    Private Sub dgvProductList_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvProductList.CellMouseClick
        Try
            If dgvProductList.RowCount > 0 Then
                Dim ProductID As Long = dgvProductList.SelectedRows(0).Cells("colProductID").Value
                lblProductID.Text = ProductID

                Dim dt As New DataTable
                dt = DirectCast(dgvProductList.DataSource, DataTable).Copy
                If dt.Rows.Count > 0 Then
                    dt.DefaultView.RowFilter = "ms_product_id='" & ProductID & "'"
                    If dt.DefaultView.Count > 0 Then
                        Using imgBig As New IO.MemoryStream(CType(dt.DefaultView(0)("product_image_big"), Byte()))
                            pbProductImage.Image = Image.FromStream(imgBig)
                            pbProductImage.SizeMode = PictureBoxSizeMode.StretchImage
                        End Using

                        lblProductCode.Text = dt.DefaultView(0)("product_code")
                        lblProductName.Text = dt.DefaultView(0)("product_name_th")
                        lblPrice.Text = dt.DefaultView(0)("price") & " บาท"

                    End If
                    dt.DefaultView.RowFilter = ""
                End If
                dt.Dispose()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub FillInData(ShelfID As Integer, ProductID As Integer)
        Try
            If ShelfID > 0 Then
                lblProductID.Text = ProductID

                Dim p(2) As SqlParameter

                Dim sql As String = "select  vs.position_row, vs.position_column, isnull(ps.product_qty,0) product_qty "
                sql += " from MS_VENDING_SHELF vs "
                sql += " left join MS_PRODUCT_SHELF ps on ps.shelf_id=vs.shelf_id"
                sql += " where vs.shelf_id=@_SHELF_ID "
                If ProductID > 0 Then
                    sql += " and ps.ms_product_id=@_PRODUCT_ID"
                    p(1) = VendingDB.SetBigInt("@_PRODUCT_ID", ProductID)
                End If
                p(0) = VendingDB.SetBigInt("@_SHELF_ID", ShelfID)

                Dim dt As DataTable = VendingDB.ExecuteTable(sql, p)
                If dt.Rows.Count > 0 Then
                    lblShelfRowColumn.Text = dt.Rows(0)("position_row") & " / " & dt.Rows(0)("position_column")
                    numQty.Value = dt.Rows(0)("product_qty")
                    lblOldQty.Text = dt.Rows(0)("product_qty")

                    ProductMasterList.DefaultView.RowFilter = "ms_product_id='" & ProductID & "'"
                    If ProductMasterList.DefaultView.Count > 0 Then
                        Dim drv As DataRowView = ProductMasterList.DefaultView(0)
                        Using imgBig As New IO.MemoryStream(CType(drv("product_image_big"), Byte()))
                            pbProductImage.Image = Image.FromStream(imgBig)
                            pbProductImage.SizeMode = PictureBoxSizeMode.StretchImage
                        End Using

                        lblProductCode.Text = drv("product_code")
                        lblProductName.Text = drv("product_name_th")
                        lblPrice.Text = drv("price") & " บาท"

                    End If
                    ProductMasterList.DefaultView.RowFilter = ""
                End If

                lblShelfID.Text = ShelfID
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub lblSave_Click(sender As Object, e As EventArgs) Handles lblSave.Click, btnSave.Click
        If numQty.Value = 0 Then
            ShowDialogErrorMessageSC("กรุณาระบุจำนวนสินค้า")
            numQty.Focus()
            Exit Sub
        End If
        If pbProductImage.Image Is Nothing Then
            ShowDialogErrorMessageSC("กรุณาเลือกสินค้า")
            Exit Sub
        End If
        If lblProductID.Text.Trim = "0" Then
            ShowDialogErrorMessageSC("กรุณาเลือกสินค้า")
            Exit Sub
        End If
        If lblProductCode.Text.Trim = "" Then
            ShowDialogErrorMessageSC("กรุณาเลือกสินค้า")
            Exit Sub
        End If
        If lblProductName.Text.Trim = "" Then
            ShowDialogErrorMessageSC("กรุณาเลือกสินค้า")
            Exit Sub
        End If
        If lblPrice.Text.Trim = "" Then
            ShowDialogErrorMessageSC("กรุณาเลือกสินค้า")
            Exit Sub
        End If

        Try
            Dim IsChangeProduct As Boolean = False
            Dim OldProductID As Integer = 0
            Dim OldProductQty As Integer = 0

            Dim lnq As New MsProductShelfVendingLinqDB
            lnq.ChkDataByMS_VENDING_ID_SHELF_ID(VendingData.VendingID, lblShelfID.Text, Nothing)
            lnq.MS_VENDING_ID = VendingData.VendingID

            If lnq.ID > 0 Then
                'ตรวจสอบว่ามีการเปลี่ยนสินค้าที่อยู่ในชั้น
                If lnq.MS_PRODUCT_ID <> lblProductID.Text Then
                    IsChangeProduct = True
                    OldProductID = lnq.MS_PRODUCT_ID
                    OldProductQty = lnq.PRODUCT_QTY
                End If
            End If

            lnq.MS_PRODUCT_ID = lblProductID.Text
            lnq.SHELF_ID = lblShelfID.Text
            lnq.PRODUCT_QTY = numQty.Value
            lnq.SYNC_TO_SERVER = "N"

            Dim trans As New VendingTransactionDB
            Dim ret As ExecuteDataInfo
            If lnq.ID > 0 Then
                ret = lnq.UpdateData(StaffConsole.Username, trans.Trans)
            Else
                ret = lnq.InsertData(StaffConsole.Username, trans.Trans)
            End If
            If ret.IsSuccess = True Then
                ret = UpdateProductMovement(IsChangeProduct, OldProductID, OldProductQty, trans)
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ShowDialogErrorMessageSC("Save Success")
                    Me.DialogResult = DialogResult.OK
                    Me.Close()
                Else
                    trans.RollbackTransaction()
                    ShowDialogErrorMessageSC(ret.ErrorMessage)
                End If
            Else
                trans.RollbackTransaction()
                ShowDialogErrorMessageSC(ret.ErrorMessage)
            End If
            lnq = Nothing
        Catch ex As Exception
            ShowDialogErrorMessageSC("Exception : " & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub

    Private Function UpdateProductMovement(IsChangeProduct As Boolean, OldProductID As Long, OldProductQty As Integer, trans As VendingTransactionDB) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim vDateNow As DateTime = DateTime.Now
            If IsChangeProduct = True Then
                'กรณีเป็นการเปลี่ยนสินค้าในชั้น
                'Insert Product Movement ของสินค้าเดิมเป็นขาออก
                Dim pmLnq As New TbProductMovementVendingLinqDB
                pmLnq.MS_VENDING_ID = VendingData.VendingID
                pmLnq.MOVEMENT_DATE = vDateNow
                pmLnq.MS_PRODUCT_ID = OldProductID
                pmLnq.SHELF_ID = lblShelfID.Text
                pmLnq.MOVEMENT_TYPE = Convert.ToInt16(Data.ConstantsData.ProductMovementType.StockOut).ToString
                pmLnq.PRODUCT_QTY = OldProductQty
                pmLnq.SYNC_TO_SERVER = "N"

                ret = pmLnq.InsertData(Environment.MachineName, trans.Trans)
                If ret.IsSuccess = True Then
                    'Insert Product Movement ของสินค้าใหม่เป็นขาเข้า
                    pmLnq = New TbProductMovementVendingLinqDB
                    pmLnq.MS_VENDING_ID = VendingData.VendingID
                    pmLnq.MOVEMENT_DATE = vDateNow
                    pmLnq.MS_PRODUCT_ID = lblProductID.Text
                    pmLnq.SHELF_ID = lblShelfID.Text
                    pmLnq.MOVEMENT_TYPE = Convert.ToInt16(Data.ConstantsData.ProductMovementType.StockIn).ToString
                    pmLnq.PRODUCT_QTY = numQty.Value
                    pmLnq.SYNC_TO_SERVER = "N"

                    ret = pmLnq.InsertData(Environment.MachineName, trans.Trans)
                End If
            Else
                If numQty.Value <> Convert.ToInt16(lblOldQty.Text) Then
                    Dim pmLnq As New TbProductMovementVendingLinqDB
                    pmLnq.MS_VENDING_ID = VendingData.VendingID
                    pmLnq.MOVEMENT_DATE = vDateNow
                    pmLnq.MS_PRODUCT_ID = lblProductID.Text
                    pmLnq.SHELF_ID = lblShelfID.Text
                    If numQty.Value > Convert.ToInt16(lblOldQty.Text) Then
                        pmLnq.MOVEMENT_TYPE = Convert.ToInt16(Data.ConstantsData.ProductMovementType.StockIn).ToString
                        pmLnq.PRODUCT_QTY = numQty.Value - Convert.ToInt16(lblOldQty.Text)
                    ElseIf numQty.Value < Convert.ToInt16(lblOldQty.Text) Then
                        pmLnq.MOVEMENT_TYPE = Convert.ToInt16(Data.ConstantsData.ProductMovementType.StockOut).ToString
                        pmLnq.PRODUCT_QTY = Convert.ToInt16(lblOldQty.Text) - numQty.Value
                    End If
                    pmLnq.SYNC_TO_SERVER = "N"

                    ret = pmLnq.InsertData(Environment.MachineName, trans.Trans)
                Else
                    ret.IsSuccess = True
                End If
            End If
        Catch ex As Exception
            ret.IsSuccess = False
            ret.ErrorMessage = ex.Message & vbNewLine & ex.StackTrace
        End Try
        Return ret

    End Function


    Private Sub lblCancel_Click(sender As Object, e As EventArgs) Handles lblCancel.Click, btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub


End Class