﻿Imports System.Data.SqlClient
Imports Kiosk_VM.Data
Imports Kiosk_VM.Data.VendingConfigData
Imports VendingLinqDB.ConnectDB
Imports VendingLinqDB.TABLE


Public Class frmHomeVertical

    Private Delegate Sub myDelegate(data As String)
    Dim ChkScreenSaverTime As DateTime = DateTime.Now

    Private Sub frmHome_Load(sender As Object, e As EventArgs) Handles Me.Load

        Me.ControlBox = False
        Me.BackColor = bgColor
        Me.BackgroundImage = Image.FromFile(Application.StartupPath & "\Background\bgHome.jpg")

        GetVendingConfig()
        GetVendingDeviceConfig()
        GetPrintConfig()
    End Sub

    Private Sub GetPrintConfig()
        Dim lnq As New CfSaleSlipVendingLinqDB
        PrintCF = lnq.GetDataList("", "", Nothing, Nothing)
        lnq = Nothing

        Dim ws As New VDMWebService.ATBVendingWebservice
        ws.Timeout = 10000
        ws.Url = VendingConfig.WebserviceVDMURL

        PrintFld = ws.GetConfigSlipFieldList()
        ws.Dispose()
    End Sub


    Private Sub frmHome_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Me.WindowState = WindowState.Maximized

        VendingConfig.SelectForm = VendingForm.Home
        InsertLogSalesActivity(vTrans.TransNo, "แสดงหน้า Home", VendingConfig.SelectForm, False)

        Application.DoEvents()
        CheckForIllegalCrossThreadCalls = False

        ''ต้อง Clear Transaction ทุกครั้งที่เข้าหน้า Home
        vTrans = New SalesTransactionData(VendingData.VendingID)
        StaffConsole = New StaffConsoleLogonData(VendingData.VendingID)
        TimerCheckPromotion.Enabled = True
        TimerCheckAutoSleep.Enabled = True

        'Disable CashIn และ CoinIn เพื่อไม่ให้สามารถใส่เงินเข้าไปได้
        BanknoteIn.DisableDeviceCashIn()
        CoinIn.DisableDeviceCoinIn()

        VendingConfig.Language = Data.ConstantsData.VendingLanguage.Thai
        SetChildFormLanguage()

        Dim thread As New System.Threading.Thread(Sub() BackgroundFormHome())
        thread.Start()

        SetShelfLayout(pnlShelf, True)
        Application.DoEvents()
    End Sub

    Private Sub BackgroundFormHome()
        Try
            '    'Update Current Status ลง DB
            UpdateAllDeviceStatusByComPort()
            '    UpdateAllDeviceStatusByUsbPort()

            '    Dim chk As String = ""
            '    'ตรวจสอบ Status จาก DB
            '    chk += CheckStockAndStatusAllDevice()

            '    If chk.Trim <> "" Then
            '        'Out Of Service

            '        InsertErrorLog(chk, "", "", "", KioskConfig.SelectForm, KioskLockerStep.Home_CheckHardwareStatus)
            '        'ShowFormError("", "", KioskConfig.SelectForm, KioskLockerStep.Home_CheckHardwareStatus, True)
            '        Me.Invoke(FormHomeError, Convert.ToInt64(KioskLockerStep.Home_CheckHardwareStatus).ToString)
            '    Else
            '        SetLockerList()
            '        If LockerList.Rows.Count = 0 Then
            '            InsertErrorLog("Locker information not found", "", "", "", KioskConfig.SelectForm, KioskLockerStep.Home_LoadLockerList)
            '            'ShowFormError("", "", KioskConfig.SelectForm, KioskLockerStep.Home_LoadLockerList, True)
            '            Me.Invoke(FormHomeError, Convert.ToInt64(KioskLockerStep.Home_LoadLockerList).ToString)
            '        End If

            '        ServiceRateData.SetServiceRateData(KioskData.KioskID)
            '        If ServiceRateData.ServiceRateDepositList.Rows.Count = 0 Or ServiceRateData.ServiceRateHourList.Rows.Count = 0 Or ServiceRateData.ServiceRateOvernightList.Rows.Count = 0 Then
            '            'Header = "Out of service"
            '            'Detail = "Service Rate Information not found"
            '            'ApplicationOutOfService = True
            '            'MsAppStepID = KioskLockerStep.Home_LoadLockerList
            '            InsertErrorLog("Service Rate Information not found", "", "", "", KioskConfig.SelectForm, KioskLockerStep.Home_LoadLockerList)
            '            'ShowFormError("", "", KioskConfig.SelectForm, KioskLockerStep.Home_LoadLockerList, True)
            '            Me.Invoke(FormHomeError, Convert.ToInt64(KioskLockerStep.Home_LoadLockerList).ToString)
            '        End If
            '    End If



            '    Engine.SyncMasterDataENG.SyncAllKioskMaster(KioskData.KioskID)
            '    Engine.SyncLogDataENG.SyncAllLog(KioskData.KioskID)
            '    Engine.SyncTransactionDataENG.SyncAllTransaction(KioskData.KioskID)
        Catch ex As Exception
            'InsertErrorLog(ex.Message & vbNewLine & ex.StackTrace, "", "", "", KioskConfig.SelectForm, KioskLockerStep.Home_CheckHardwareStatus)
        End Try
    End Sub

    Private Sub lblPointer_Click(sender As Object, e As EventArgs) Handles lblPointer.Click
        TimerSetPointer.Stop()
        lblPointer.Parent = Me
        If lblPointer.Text = "1" Then
            lblPointer.Text = "2"

            'Move to top right
            lblPointer.Location = New Point(Me.Width - lblPointer.Width, 0)
            lblPointer.BringToFront()
        ElseIf lblPointer.Text = "2" Then
            lblPointer.Text = "3"

            'Move to buttom left
            lblPointer.Location = New Point(0, Me.Height - lblPointer.Height)
            lblPointer.BringToFront()

            ''Move to left
            'lblPointer.Location = New Point(0, 0)
            'lblPointer.BringToFront()
        ElseIf lblPointer.Text = "3" Then
            lblPointer.Text = "4"

            'Move to buttom right
            lblPointer.Location = New Point(Me.Width - lblPointer.Width, Me.Height - lblPointer.Height)
            lblPointer.BringToFront()

            ''Move to  right
            'lblPointer.Location = New Point(Me.Width - lblPointer.Width, 0)
            'lblPointer.BringToFront()
        ElseIf lblPointer.Text = "4" Then
            InsertLogSalesActivity(vTrans.TransNo, "แสดงหน้าจอ Login Staff Console", VendingConfig.SelectForm, False)
            TimerCheckAutoSleep.Enabled = False

            TimerSetPointer.Enabled = True
            Dim frm As New frmSC_LogIn
            frm.ShowDialog(Me)
            frmHome_Shown(Nothing, Nothing)
        End If
        TimerSetPointer.Enabled = True
        TimerSetPointer.Start()
    End Sub

    Private Sub TimerSetPointer_Tick(sender As Object, e As EventArgs) Handles TimerSetPointer.Tick
        lblPointer.Parent = Me
        lblPointer.Text = "1"
        lblPointer.Left = 0
        lblPointer.Top = 0
        TimerSetPointer.Enabled = False
    End Sub

    Private Sub TimerScreenSaver_Tick(sender As Object, e As EventArgs) Handles TimerScreenSaver.Tick
        Try
            If ChkScreenSaverTime.AddSeconds(VendingConfig.ScreenSaverSec) <= DateTime.Now Then
                Dim fld As String = Application.StartupPath & "\ScreenSaver\"
                If IO.Directory.Exists(fld) = True Then
                    Dim fle() As String = IO.Directory.GetFiles(fld)
                    If fle.Length > 0 Then
                        InsertLogSalesActivity(vTrans.TransNo, "แสดงหน้าจอ Screen Saver", VendingConfig.SelectForm, False)

                        TimerScreenSaver.Enabled = False
                        If frmScreenSaver.ShowDialog = DialogResult.Yes Then
                            frmScreenSaver.Close()
                        End If

                        ChkScreenSaverTime = DateTime.Now
                        TimerScreenSaver.Enabled = True
                    End If
                End If
            End If
        Catch ex As Exception : End Try
    End Sub

    Private Sub TimerCheckAutoSleep_Tick(sender As Object, e As EventArgs) Handles TimerCheckAutoSleep.Tick
        If DateTime.Now.ToString("HH:mm") = VendingConfig.SleepTime Then
            Dim wup As New AutoWakeUP
            AddHandler wup.Woken, AddressOf AutoWakeUP_Woken
            wup.SetWakeUpTime(DateTime.Now.AddMinutes(VendingConfig.SleepDuration))
            System.Threading.Thread.Sleep(1000)
            TimerCheckAutoSleep.Enabled = False

            InsertLogSalesActivity(vTrans.TransNo, "เข้าสู่ Sleep Mode เป็นเวลา " & VendingConfig.SleepDuration & " นาที", VendingConfig.SelectForm, False)
            Application.SetSuspendState(PowerState.Suspend, False, False)
        End If
    End Sub

    Private Sub AutoWakeUP_Woken(sender As Object, e As EventArgs)
        InsertLogSalesActivity(vTrans.TransNo, "Vending Weakup", VendingConfig.SelectForm, False)
        TimerCheckAutoSleep.Enabled = True
    End Sub

    Private Sub TimerCheckPromotion_Tick(sender As Object, e As EventArgs) Handles TimerCheckPromotion.Tick
        TimerCheckPromotion.Enabled = False
        Try
            Dim WS As New VDMWebService.ATBVendingWebservice
            WS.Url = VendingConfig.WebserviceVDMURL
            WS.Timeout = 10000
            GetMasterProductPromotion(WS)
            WS.Dispose()

            SetDiscountLayout()
        Catch ex As Exception

        End Try
        TimerCheckPromotion.Enabled = True
    End Sub


    Private Sub SetDiscountLayout()
        For Each lblPromo As Label In pnlShelf.Controls.OfType(Of Label)
            pnlShelf.Controls.Remove(lblPromo)
        Next

        For Each pbProd As PictureBox In pnlShelf.Controls.OfType(Of PictureBox)
            Dim pInfo() As String = Split(pbProd.Name, "_")

            If pInfo.Length = 8 Then
                Dim ProductID As Long = pInfo(3)

                'Add Icon Promotion
                ProductPromotionList.DefaultView.RowFilter = "ms_product_id=" & ProductID
                If ProductPromotionList.DefaultView.Count > 0 Then
                    Dim lblPromotion As New Label
                    lblPromotion.Name = pbProd.Name
                    lblPromotion.Width = pbProd.Width
                    lblPromotion.TextAlign = ContentAlignment.MiddleCenter
                    lblPromotion.Top = pbProd.Top - lblPromotion.Height
                    lblPromotion.Left = pbProd.Left
                    lblPromotion.BackColor = Color.Red
                    lblPromotion.Text = ProductPromotionList.DefaultView(0)("discount_percent") & "%"
                    pnlShelf.Controls.Add(lblPromotion)
                End If
                ProductPromotionList.DefaultView.RowFilter = ""
            End If
        Next
        Application.DoEvents()

    End Sub

    Private Sub frmHomeVertical_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        TimerCheckPromotion.Enabled = False
    End Sub
End Class