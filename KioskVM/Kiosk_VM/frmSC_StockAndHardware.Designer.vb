﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmSC_StockAndHardware
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pbBackground = New System.Windows.Forms.PictureBox()
        Me.TimerCheckDoorClose = New System.Windows.Forms.Timer(Me.components)
        Me.TabContainer1 = New System.Windows.Forms.TabControl()
        Me.tabStockAndHardware = New System.Windows.Forms.TabPage()
        Me.pnlProductShelf = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.flpM_Stock = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.flpHWStatus = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.pbClose = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tabFillPaper = New System.Windows.Forms.TabPage()
        Me.txtFillPaperValue = New System.Windows.Forms.TextBox()
        Me.txtFillPaperMax = New System.Windows.Forms.TextBox()
        Me.btnFillPaperCancel = New System.Windows.Forms.Panel()
        Me.lblFillPaperCancel = New System.Windows.Forms.Label()
        Me.btnFillPaperConfirm = New System.Windows.Forms.Panel()
        Me.lblFillPaperConfirm = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tabFillMoney = New System.Windows.Forms.TabPage()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtFillMoneyTotalMoney = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btnFillMoneyCancel = New System.Windows.Forms.Panel()
        Me.lblFillMoneyCancel = New System.Windows.Forms.Label()
        Me.btnFillMoneyConfirm = New System.Windows.Forms.Panel()
        Me.lblFillMoneyConfirm = New System.Windows.Forms.Label()
        Me.lblFillMoneyMaxBanknote20 = New System.Windows.Forms.Label()
        Me.lblFillMoneyMaxCoin5 = New System.Windows.Forms.Label()
        Me.btnFillMoneyAllFull = New System.Windows.Forms.Panel()
        Me.lblFillMoneyAllFull = New System.Windows.Forms.Label()
        Me.lblFillMoneyTotalBanknoteOut100 = New System.Windows.Forms.Label()
        Me.txtFillMoneyBanknoteOut100 = New System.Windows.Forms.TextBox()
        Me.lblFillMoneyTotalBanknoteOut20 = New System.Windows.Forms.Label()
        Me.txtFillMoneyBanknoteOut20 = New System.Windows.Forms.TextBox()
        Me.lblFillMoneyTotalCoinOut5 = New System.Windows.Forms.Label()
        Me.txtFillMoneyCoinOut5 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnFillMoneyCheckOutMoney = New System.Windows.Forms.Panel()
        Me.lblFillMoneyCheckOutMoney = New System.Windows.Forms.Label()
        Me.txtFillMoneyBanknoteInMoney = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtFillMoneyCoinInMoney = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblFillMoneyMaxBanknote100 = New System.Windows.Forms.Label()
        Me.tabVendingSetting = New System.Windows.Forms.TabPage()
        Me.btnSettingCancel = New System.Windows.Forms.Panel()
        Me.lblSettingCancel = New System.Windows.Forms.Label()
        Me.btnSettingSave = New System.Windows.Forms.Panel()
        Me.lblSettingSave = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtSettingShelfLong = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtSettingScreenSaverTime = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtSettingSleepDurationMin = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtSettingSleepTimeM = New System.Windows.Forms.TextBox()
        Me.txtSettingSleepTimeH = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtSettingPaymentExtend = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtSettingMessageTime = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtSettingTimeOut = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtSettingMacAddress = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtSettingIPAddress = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.cbSettingNetworkDevice = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtSettingVendingID = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.tabDeviceSetting = New System.Windows.Forms.TabPage()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.dgvDeviceSettingCoinOutList = New System.Windows.Forms.DataGridView()
        Me.btnDeviceSettingCancel = New System.Windows.Forms.Panel()
        Me.lblDeviceSettingCancel = New System.Windows.Forms.Label()
        Me.btnDeviceSettingSave = New System.Windows.Forms.Panel()
        Me.lblDeviceSettingSave = New System.Windows.Forms.Label()
        Me.dgvDeviceSettingBanknoteOutList = New System.Windows.Forms.DataGridView()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.cbDeviceSettingCoinIn = New System.Windows.Forms.ComboBox()
        Me.cbDeviceSettingBanknoteIn = New System.Windows.Forms.ComboBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.tabSetupShelf = New System.Windows.Forms.TabPage()
        Me.btnAddShelf = New System.Windows.Forms.Panel()
        Me.lblAddShelf = New System.Windows.Forms.Label()
        Me.pnlSetupShelfLayout = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.tabSetupProduct = New System.Windows.Forms.TabPage()
        Me.pnlSetupProduct = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.tabSetupSlip = New System.Windows.Forms.TabPage()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.colCoinDeviceID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCoinDeviceNameTh = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCoinComport = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colBanknoteDeviceID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDeviceNameTh = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComPort = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.UcSetupSlip1 = New Kiosk_VM.ucSetupSlip()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.cbDeviceSettingPrinterName = New System.Windows.Forms.ComboBox()
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabContainer1.SuspendLayout()
        Me.tabStockAndHardware.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.pbClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabFillPaper.SuspendLayout()
        Me.btnFillPaperCancel.SuspendLayout()
        Me.btnFillPaperConfirm.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.tabFillMoney.SuspendLayout()
        Me.btnFillMoneyCancel.SuspendLayout()
        Me.btnFillMoneyConfirm.SuspendLayout()
        Me.btnFillMoneyAllFull.SuspendLayout()
        Me.btnFillMoneyCheckOutMoney.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.tabVendingSetting.SuspendLayout()
        Me.btnSettingCancel.SuspendLayout()
        Me.btnSettingSave.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.tabDeviceSetting.SuspendLayout()
        CType(Me.dgvDeviceSettingCoinOutList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.btnDeviceSettingCancel.SuspendLayout()
        Me.btnDeviceSettingSave.SuspendLayout()
        CType(Me.dgvDeviceSettingBanknoteOutList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        Me.tabSetupShelf.SuspendLayout()
        Me.btnAddShelf.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.tabSetupProduct.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.tabSetupSlip.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.SuspendLayout()
        '
        'pbBackground
        '
        Me.pbBackground.BackColor = System.Drawing.Color.Transparent
        Me.pbBackground.Location = New System.Drawing.Point(0, 0)
        Me.pbBackground.Name = "pbBackground"
        Me.pbBackground.Size = New System.Drawing.Size(768, 1366)
        Me.pbBackground.TabIndex = 0
        Me.pbBackground.TabStop = False
        '
        'TimerCheckDoorClose
        '
        Me.TimerCheckDoorClose.Enabled = True
        Me.TimerCheckDoorClose.Interval = 2000
        '
        'TabContainer1
        '
        Me.TabContainer1.Controls.Add(Me.tabStockAndHardware)
        Me.TabContainer1.Controls.Add(Me.tabFillPaper)
        Me.TabContainer1.Controls.Add(Me.tabFillMoney)
        Me.TabContainer1.Controls.Add(Me.tabVendingSetting)
        Me.TabContainer1.Controls.Add(Me.tabDeviceSetting)
        Me.TabContainer1.Controls.Add(Me.tabSetupShelf)
        Me.TabContainer1.Controls.Add(Me.tabSetupProduct)
        Me.TabContainer1.Controls.Add(Me.tabSetupSlip)
        Me.TabContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabContainer1.Location = New System.Drawing.Point(0, 0)
        Me.TabContainer1.Name = "TabContainer1"
        Me.TabContainer1.SelectedIndex = 0
        Me.TabContainer1.Size = New System.Drawing.Size(768, 1366)
        Me.TabContainer1.TabIndex = 1
        '
        'tabStockAndHardware
        '
        Me.tabStockAndHardware.BackColor = System.Drawing.Color.FromArgb(CType(CType(97, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(72, Byte), Integer))
        Me.tabStockAndHardware.Controls.Add(Me.pnlProductShelf)
        Me.tabStockAndHardware.Controls.Add(Me.Panel2)
        Me.tabStockAndHardware.Controls.Add(Me.flpM_Stock)
        Me.tabStockAndHardware.Controls.Add(Me.Panel5)
        Me.tabStockAndHardware.Controls.Add(Me.flpHWStatus)
        Me.tabStockAndHardware.Controls.Add(Me.Panel1)
        Me.tabStockAndHardware.Location = New System.Drawing.Point(4, 22)
        Me.tabStockAndHardware.Name = "tabStockAndHardware"
        Me.tabStockAndHardware.Padding = New System.Windows.Forms.Padding(3)
        Me.tabStockAndHardware.Size = New System.Drawing.Size(760, 1340)
        Me.tabStockAndHardware.TabIndex = 0
        Me.tabStockAndHardware.Text = "Stock && Hardware"
        '
        'pnlProductShelf
        '
        Me.pnlProductShelf.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlProductShelf.Location = New System.Drawing.Point(3, 655)
        Me.pnlProductShelf.Name = "pnlProductShelf"
        Me.pnlProductShelf.Size = New System.Drawing.Size(754, 682)
        Me.pnlProductShelf.TabIndex = 9
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(3, 615)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(754, 40)
        Me.Panel2.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(27, 2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(318, 37)
        Me.Label2.TabIndex = 66
        Me.Label2.Text = "Product Shelf"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'flpM_Stock
        '
        Me.flpM_Stock.Dock = System.Windows.Forms.DockStyle.Top
        Me.flpM_Stock.Location = New System.Drawing.Point(3, 356)
        Me.flpM_Stock.Margin = New System.Windows.Forms.Padding(1)
        Me.flpM_Stock.Name = "flpM_Stock"
        Me.flpM_Stock.Size = New System.Drawing.Size(754, 259)
        Me.flpM_Stock.TabIndex = 7
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(3, 316)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(754, 40)
        Me.Panel5.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(27, 2)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(318, 37)
        Me.Label3.TabIndex = 66
        Me.Label3.Text = "Material Stock"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'flpHWStatus
        '
        Me.flpHWStatus.Dock = System.Windows.Forms.DockStyle.Top
        Me.flpHWStatus.Location = New System.Drawing.Point(3, 44)
        Me.flpHWStatus.Name = "flpHWStatus"
        Me.flpHWStatus.Size = New System.Drawing.Size(754, 272)
        Me.flpHWStatus.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel1.Controls.Add(Me.pbClose)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(754, 41)
        Me.Panel1.TabIndex = 1
        '
        'pbClose
        '
        Me.pbClose.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.IconClose
        Me.pbClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbClose.Location = New System.Drawing.Point(700, -3)
        Me.pbClose.Name = "pbClose"
        Me.pbClose.Size = New System.Drawing.Size(46, 44)
        Me.pbClose.TabIndex = 67
        Me.pbClose.TabStop = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(27, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(318, 35)
        Me.Label1.TabIndex = 66
        Me.Label1.Text = "Hardware Status"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabFillPaper
        '
        Me.tabFillPaper.BackColor = System.Drawing.Color.FromArgb(CType(CType(97, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(72, Byte), Integer))
        Me.tabFillPaper.Controls.Add(Me.txtFillPaperValue)
        Me.tabFillPaper.Controls.Add(Me.txtFillPaperMax)
        Me.tabFillPaper.Controls.Add(Me.btnFillPaperCancel)
        Me.tabFillPaper.Controls.Add(Me.btnFillPaperConfirm)
        Me.tabFillPaper.Controls.Add(Me.Label5)
        Me.tabFillPaper.Controls.Add(Me.Label6)
        Me.tabFillPaper.Controls.Add(Me.Panel3)
        Me.tabFillPaper.Location = New System.Drawing.Point(4, 22)
        Me.tabFillPaper.Name = "tabFillPaper"
        Me.tabFillPaper.Padding = New System.Windows.Forms.Padding(3)
        Me.tabFillPaper.Size = New System.Drawing.Size(760, 1340)
        Me.tabFillPaper.TabIndex = 1
        Me.tabFillPaper.Text = "Fill Paper"
        '
        'txtFillPaperValue
        '
        Me.txtFillPaperValue.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtFillPaperValue.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFillPaperValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFillPaperValue.Location = New System.Drawing.Point(363, 536)
        Me.txtFillPaperValue.MaxLength = 15
        Me.txtFillPaperValue.Name = "txtFillPaperValue"
        Me.txtFillPaperValue.Size = New System.Drawing.Size(283, 38)
        Me.txtFillPaperValue.TabIndex = 58
        Me.txtFillPaperValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFillPaperMax
        '
        Me.txtFillPaperMax.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtFillPaperMax.BackColor = System.Drawing.Color.LightGray
        Me.txtFillPaperMax.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFillPaperMax.Enabled = False
        Me.txtFillPaperMax.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFillPaperMax.Location = New System.Drawing.Point(363, 621)
        Me.txtFillPaperMax.MaxLength = 15
        Me.txtFillPaperMax.Name = "txtFillPaperMax"
        Me.txtFillPaperMax.Size = New System.Drawing.Size(283, 38)
        Me.txtFillPaperMax.TabIndex = 59
        Me.txtFillPaperMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnFillPaperCancel
        '
        Me.btnFillPaperCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnFillPaperCancel.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnFillPaperCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnFillPaperCancel.Controls.Add(Me.lblFillPaperCancel)
        Me.btnFillPaperCancel.Location = New System.Drawing.Point(500, 671)
        Me.btnFillPaperCancel.Name = "btnFillPaperCancel"
        Me.btnFillPaperCancel.Size = New System.Drawing.Size(125, 50)
        Me.btnFillPaperCancel.TabIndex = 61
        '
        'lblFillPaperCancel
        '
        Me.lblFillPaperCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillPaperCancel.BackColor = System.Drawing.Color.Transparent
        Me.lblFillPaperCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillPaperCancel.ForeColor = System.Drawing.Color.Black
        Me.lblFillPaperCancel.Location = New System.Drawing.Point(3, 8)
        Me.lblFillPaperCancel.Name = "lblFillPaperCancel"
        Me.lblFillPaperCancel.Size = New System.Drawing.Size(119, 33)
        Me.lblFillPaperCancel.TabIndex = 35
        Me.lblFillPaperCancel.Text = "Cancel"
        Me.lblFillPaperCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnFillPaperConfirm
        '
        Me.btnFillPaperConfirm.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnFillPaperConfirm.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnFillPaperConfirm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnFillPaperConfirm.Controls.Add(Me.lblFillPaperConfirm)
        Me.btnFillPaperConfirm.Location = New System.Drawing.Point(363, 671)
        Me.btnFillPaperConfirm.Name = "btnFillPaperConfirm"
        Me.btnFillPaperConfirm.Size = New System.Drawing.Size(131, 50)
        Me.btnFillPaperConfirm.TabIndex = 60
        '
        'lblFillPaperConfirm
        '
        Me.lblFillPaperConfirm.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillPaperConfirm.BackColor = System.Drawing.Color.Transparent
        Me.lblFillPaperConfirm.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillPaperConfirm.ForeColor = System.Drawing.Color.Black
        Me.lblFillPaperConfirm.Location = New System.Drawing.Point(8, 8)
        Me.lblFillPaperConfirm.Name = "lblFillPaperConfirm"
        Me.lblFillPaperConfirm.Size = New System.Drawing.Size(114, 33)
        Me.lblFillPaperConfirm.TabIndex = 35
        Me.lblFillPaperConfirm.Text = "Confirm"
        Me.lblFillPaperConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(51, 610)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(306, 61)
        Me.Label5.TabIndex = 57
        Me.Label5.Text = "จำนวนครั้งพิมพ์สูงสุด"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(62, 525)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(295, 61)
        Me.Label6.TabIndex = 56
        Me.Label6.Text = "จำนวนครั้งที่พิมพ์ได้"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(754, 41)
        Me.Panel3.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(27, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(318, 35)
        Me.Label4.TabIndex = 66
        Me.Label4.Text = "Fill Paper"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabFillMoney
        '
        Me.tabFillMoney.BackColor = System.Drawing.Color.FromArgb(CType(CType(97, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(72, Byte), Integer))
        Me.tabFillMoney.Controls.Add(Me.Label15)
        Me.tabFillMoney.Controls.Add(Me.txtFillMoneyTotalMoney)
        Me.tabFillMoney.Controls.Add(Me.Label16)
        Me.tabFillMoney.Controls.Add(Me.btnFillMoneyCancel)
        Me.tabFillMoney.Controls.Add(Me.btnFillMoneyConfirm)
        Me.tabFillMoney.Controls.Add(Me.lblFillMoneyMaxBanknote20)
        Me.tabFillMoney.Controls.Add(Me.lblFillMoneyMaxCoin5)
        Me.tabFillMoney.Controls.Add(Me.btnFillMoneyAllFull)
        Me.tabFillMoney.Controls.Add(Me.lblFillMoneyTotalBanknoteOut100)
        Me.tabFillMoney.Controls.Add(Me.txtFillMoneyBanknoteOut100)
        Me.tabFillMoney.Controls.Add(Me.lblFillMoneyTotalBanknoteOut20)
        Me.tabFillMoney.Controls.Add(Me.txtFillMoneyBanknoteOut20)
        Me.tabFillMoney.Controls.Add(Me.lblFillMoneyTotalCoinOut5)
        Me.tabFillMoney.Controls.Add(Me.txtFillMoneyCoinOut5)
        Me.tabFillMoney.Controls.Add(Me.Label12)
        Me.tabFillMoney.Controls.Add(Me.Label13)
        Me.tabFillMoney.Controls.Add(Me.Label14)
        Me.tabFillMoney.Controls.Add(Me.Label8)
        Me.tabFillMoney.Controls.Add(Me.Label9)
        Me.tabFillMoney.Controls.Add(Me.btnFillMoneyCheckOutMoney)
        Me.tabFillMoney.Controls.Add(Me.txtFillMoneyBanknoteInMoney)
        Me.tabFillMoney.Controls.Add(Me.Label10)
        Me.tabFillMoney.Controls.Add(Me.txtFillMoneyCoinInMoney)
        Me.tabFillMoney.Controls.Add(Me.Label11)
        Me.tabFillMoney.Controls.Add(Me.Panel4)
        Me.tabFillMoney.Controls.Add(Me.lblFillMoneyMaxBanknote100)
        Me.tabFillMoney.Location = New System.Drawing.Point(4, 22)
        Me.tabFillMoney.Name = "tabFillMoney"
        Me.tabFillMoney.Padding = New System.Windows.Forms.Padding(3)
        Me.tabFillMoney.Size = New System.Drawing.Size(760, 1340)
        Me.tabFillMoney.TabIndex = 2
        Me.tabFillMoney.Text = "Fill Money"
        '
        'Label15
        '
        Me.Label15.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(512, 523)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(164, 31)
        Me.Label15.TabIndex = 92
        Me.Label15.Text = "บาท"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFillMoneyTotalMoney
        '
        Me.txtFillMoneyTotalMoney.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtFillMoneyTotalMoney.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.txtFillMoneyTotalMoney.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFillMoneyTotalMoney.Enabled = False
        Me.txtFillMoneyTotalMoney.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFillMoneyTotalMoney.Location = New System.Drawing.Point(316, 523)
        Me.txtFillMoneyTotalMoney.MaxLength = 15
        Me.txtFillMoneyTotalMoney.Name = "txtFillMoneyTotalMoney"
        Me.txtFillMoneyTotalMoney.Size = New System.Drawing.Size(190, 31)
        Me.txtFillMoneyTotalMoney.TabIndex = 91
        Me.txtFillMoneyTotalMoney.Text = "0.00"
        Me.txtFillMoneyTotalMoney.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(92, 523)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(220, 31)
        Me.Label16.TabIndex = 90
        Me.Label16.Text = "จำนวนเงินทั้งหมด"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnFillMoneyCancel
        '
        Me.btnFillMoneyCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnFillMoneyCancel.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnFillMoneyCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnFillMoneyCancel.Controls.Add(Me.lblFillMoneyCancel)
        Me.btnFillMoneyCancel.Location = New System.Drawing.Point(402, 663)
        Me.btnFillMoneyCancel.Name = "btnFillMoneyCancel"
        Me.btnFillMoneyCancel.Size = New System.Drawing.Size(185, 54)
        Me.btnFillMoneyCancel.TabIndex = 89
        '
        'lblFillMoneyCancel
        '
        Me.lblFillMoneyCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillMoneyCancel.BackColor = System.Drawing.Color.Transparent
        Me.lblFillMoneyCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillMoneyCancel.ForeColor = System.Drawing.Color.Black
        Me.lblFillMoneyCancel.Location = New System.Drawing.Point(20, 13)
        Me.lblFillMoneyCancel.Name = "lblFillMoneyCancel"
        Me.lblFillMoneyCancel.Size = New System.Drawing.Size(144, 29)
        Me.lblFillMoneyCancel.TabIndex = 35
        Me.lblFillMoneyCancel.Text = "ยกเลิก"
        Me.lblFillMoneyCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnFillMoneyConfirm
        '
        Me.btnFillMoneyConfirm.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnFillMoneyConfirm.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnFillMoneyConfirm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnFillMoneyConfirm.Controls.Add(Me.lblFillMoneyConfirm)
        Me.btnFillMoneyConfirm.Location = New System.Drawing.Point(207, 663)
        Me.btnFillMoneyConfirm.Name = "btnFillMoneyConfirm"
        Me.btnFillMoneyConfirm.Size = New System.Drawing.Size(185, 54)
        Me.btnFillMoneyConfirm.TabIndex = 88
        '
        'lblFillMoneyConfirm
        '
        Me.lblFillMoneyConfirm.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillMoneyConfirm.BackColor = System.Drawing.Color.Transparent
        Me.lblFillMoneyConfirm.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillMoneyConfirm.ForeColor = System.Drawing.Color.Black
        Me.lblFillMoneyConfirm.Location = New System.Drawing.Point(10, 14)
        Me.lblFillMoneyConfirm.Name = "lblFillMoneyConfirm"
        Me.lblFillMoneyConfirm.Size = New System.Drawing.Size(164, 29)
        Me.lblFillMoneyConfirm.TabIndex = 35
        Me.lblFillMoneyConfirm.Text = "ยืนยัน"
        Me.lblFillMoneyConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFillMoneyMaxBanknote20
        '
        Me.lblFillMoneyMaxBanknote20.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillMoneyMaxBanknote20.BackColor = System.Drawing.Color.Transparent
        Me.lblFillMoneyMaxBanknote20.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillMoneyMaxBanknote20.ForeColor = System.Drawing.Color.White
        Me.lblFillMoneyMaxBanknote20.Location = New System.Drawing.Point(386, 341)
        Me.lblFillMoneyMaxBanknote20.Name = "lblFillMoneyMaxBanknote20"
        Me.lblFillMoneyMaxBanknote20.Size = New System.Drawing.Size(34, 31)
        Me.lblFillMoneyMaxBanknote20.TabIndex = 86
        Me.lblFillMoneyMaxBanknote20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblFillMoneyMaxBanknote20.Visible = False
        '
        'lblFillMoneyMaxCoin5
        '
        Me.lblFillMoneyMaxCoin5.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillMoneyMaxCoin5.BackColor = System.Drawing.Color.Transparent
        Me.lblFillMoneyMaxCoin5.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillMoneyMaxCoin5.ForeColor = System.Drawing.Color.White
        Me.lblFillMoneyMaxCoin5.Location = New System.Drawing.Point(387, 299)
        Me.lblFillMoneyMaxCoin5.Name = "lblFillMoneyMaxCoin5"
        Me.lblFillMoneyMaxCoin5.Size = New System.Drawing.Size(34, 31)
        Me.lblFillMoneyMaxCoin5.TabIndex = 85
        Me.lblFillMoneyMaxCoin5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblFillMoneyMaxCoin5.Visible = False
        '
        'btnFillMoneyAllFull
        '
        Me.btnFillMoneyAllFull.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnFillMoneyAllFull.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnFillMoneyAllFull.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnFillMoneyAllFull.Controls.Add(Me.lblFillMoneyAllFull)
        Me.btnFillMoneyAllFull.Location = New System.Drawing.Point(429, 420)
        Me.btnFillMoneyAllFull.Name = "btnFillMoneyAllFull"
        Me.btnFillMoneyAllFull.Size = New System.Drawing.Size(163, 44)
        Me.btnFillMoneyAllFull.TabIndex = 75
        '
        'lblFillMoneyAllFull
        '
        Me.lblFillMoneyAllFull.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillMoneyAllFull.BackColor = System.Drawing.Color.Transparent
        Me.lblFillMoneyAllFull.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillMoneyAllFull.ForeColor = System.Drawing.Color.Black
        Me.lblFillMoneyAllFull.Location = New System.Drawing.Point(3, 5)
        Me.lblFillMoneyAllFull.Name = "lblFillMoneyAllFull"
        Me.lblFillMoneyAllFull.Size = New System.Drawing.Size(157, 35)
        Me.lblFillMoneyAllFull.TabIndex = 35
        Me.lblFillMoneyAllFull.Text = "เติมเต็มจำนวน"
        Me.lblFillMoneyAllFull.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFillMoneyTotalBanknoteOut100
        '
        Me.lblFillMoneyTotalBanknoteOut100.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillMoneyTotalBanknoteOut100.BackColor = System.Drawing.Color.Transparent
        Me.lblFillMoneyTotalBanknoteOut100.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillMoneyTotalBanknoteOut100.ForeColor = System.Drawing.Color.White
        Me.lblFillMoneyTotalBanknoteOut100.Location = New System.Drawing.Point(595, 383)
        Me.lblFillMoneyTotalBanknoteOut100.Name = "lblFillMoneyTotalBanknoteOut100"
        Me.lblFillMoneyTotalBanknoteOut100.Size = New System.Drawing.Size(157, 31)
        Me.lblFillMoneyTotalBanknoteOut100.TabIndex = 84
        Me.lblFillMoneyTotalBanknoteOut100.Text = "/ 500 ใบ"
        Me.lblFillMoneyTotalBanknoteOut100.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFillMoneyBanknoteOut100
        '
        Me.txtFillMoneyBanknoteOut100.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtFillMoneyBanknoteOut100.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFillMoneyBanknoteOut100.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFillMoneyBanknoteOut100.Location = New System.Drawing.Point(426, 383)
        Me.txtFillMoneyBanknoteOut100.MaxLength = 15
        Me.txtFillMoneyBanknoteOut100.Name = "txtFillMoneyBanknoteOut100"
        Me.txtFillMoneyBanknoteOut100.Size = New System.Drawing.Size(163, 31)
        Me.txtFillMoneyBanknoteOut100.TabIndex = 83
        Me.txtFillMoneyBanknoteOut100.Text = "0"
        Me.txtFillMoneyBanknoteOut100.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblFillMoneyTotalBanknoteOut20
        '
        Me.lblFillMoneyTotalBanknoteOut20.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillMoneyTotalBanknoteOut20.BackColor = System.Drawing.Color.Transparent
        Me.lblFillMoneyTotalBanknoteOut20.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillMoneyTotalBanknoteOut20.ForeColor = System.Drawing.Color.White
        Me.lblFillMoneyTotalBanknoteOut20.Location = New System.Drawing.Point(595, 341)
        Me.lblFillMoneyTotalBanknoteOut20.Name = "lblFillMoneyTotalBanknoteOut20"
        Me.lblFillMoneyTotalBanknoteOut20.Size = New System.Drawing.Size(157, 31)
        Me.lblFillMoneyTotalBanknoteOut20.TabIndex = 82
        Me.lblFillMoneyTotalBanknoteOut20.Text = "/ 500 ใบ"
        Me.lblFillMoneyTotalBanknoteOut20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFillMoneyBanknoteOut20
        '
        Me.txtFillMoneyBanknoteOut20.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtFillMoneyBanknoteOut20.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFillMoneyBanknoteOut20.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFillMoneyBanknoteOut20.Location = New System.Drawing.Point(426, 341)
        Me.txtFillMoneyBanknoteOut20.MaxLength = 15
        Me.txtFillMoneyBanknoteOut20.Name = "txtFillMoneyBanknoteOut20"
        Me.txtFillMoneyBanknoteOut20.Size = New System.Drawing.Size(163, 31)
        Me.txtFillMoneyBanknoteOut20.TabIndex = 81
        Me.txtFillMoneyBanknoteOut20.Text = "0"
        Me.txtFillMoneyBanknoteOut20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblFillMoneyTotalCoinOut5
        '
        Me.lblFillMoneyTotalCoinOut5.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillMoneyTotalCoinOut5.BackColor = System.Drawing.Color.Transparent
        Me.lblFillMoneyTotalCoinOut5.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillMoneyTotalCoinOut5.ForeColor = System.Drawing.Color.White
        Me.lblFillMoneyTotalCoinOut5.Location = New System.Drawing.Point(596, 299)
        Me.lblFillMoneyTotalCoinOut5.Name = "lblFillMoneyTotalCoinOut5"
        Me.lblFillMoneyTotalCoinOut5.Size = New System.Drawing.Size(156, 31)
        Me.lblFillMoneyTotalCoinOut5.TabIndex = 80
        Me.lblFillMoneyTotalCoinOut5.Text = "/ 500 เหรียญ"
        Me.lblFillMoneyTotalCoinOut5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFillMoneyCoinOut5
        '
        Me.txtFillMoneyCoinOut5.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtFillMoneyCoinOut5.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFillMoneyCoinOut5.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFillMoneyCoinOut5.Location = New System.Drawing.Point(427, 299)
        Me.txtFillMoneyCoinOut5.MaxLength = 15
        Me.txtFillMoneyCoinOut5.Name = "txtFillMoneyCoinOut5"
        Me.txtFillMoneyCoinOut5.Size = New System.Drawing.Size(163, 31)
        Me.txtFillMoneyCoinOut5.TabIndex = 79
        Me.txtFillMoneyCoinOut5.Text = "0"
        Me.txtFillMoneyCoinOut5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(11, 383)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(411, 31)
        Me.Label12.TabIndex = 78
        Me.Label12.Text = "จำนวนธนบัตร 100 บาทในเครื่องทอน"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(11, 341)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(411, 31)
        Me.Label13.TabIndex = 77
        Me.Label13.Text = "จำนวนธนบัตร 20 บาทในเครื่องทอน"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(11, 299)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(411, 31)
        Me.Label14.TabIndex = 76
        Me.Label14.Text = "จำนวนเหรียญ 5 บาทในเครื่องทอน"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(594, 156)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 42)
        Me.Label8.TabIndex = 70
        Me.Label8.Text = "บาท"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(594, 114)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(63, 42)
        Me.Label9.TabIndex = 69
        Me.Label9.Text = "บาท"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnFillMoneyCheckOutMoney
        '
        Me.btnFillMoneyCheckOutMoney.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnFillMoneyCheckOutMoney.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnFillMoneyCheckOutMoney.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnFillMoneyCheckOutMoney.Controls.Add(Me.lblFillMoneyCheckOutMoney)
        Me.btnFillMoneyCheckOutMoney.Location = New System.Drawing.Point(427, 203)
        Me.btnFillMoneyCheckOutMoney.Name = "btnFillMoneyCheckOutMoney"
        Me.btnFillMoneyCheckOutMoney.Size = New System.Drawing.Size(163, 44)
        Me.btnFillMoneyCheckOutMoney.TabIndex = 66
        '
        'lblFillMoneyCheckOutMoney
        '
        Me.lblFillMoneyCheckOutMoney.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillMoneyCheckOutMoney.BackColor = System.Drawing.Color.Transparent
        Me.lblFillMoneyCheckOutMoney.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillMoneyCheckOutMoney.ForeColor = System.Drawing.Color.Black
        Me.lblFillMoneyCheckOutMoney.Location = New System.Drawing.Point(12, 5)
        Me.lblFillMoneyCheckOutMoney.Name = "lblFillMoneyCheckOutMoney"
        Me.lblFillMoneyCheckOutMoney.Size = New System.Drawing.Size(139, 35)
        Me.lblFillMoneyCheckOutMoney.TabIndex = 35
        Me.lblFillMoneyCheckOutMoney.Text = "นำออกทั้งหมด"
        Me.lblFillMoneyCheckOutMoney.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtFillMoneyBanknoteInMoney
        '
        Me.txtFillMoneyBanknoteInMoney.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtFillMoneyBanknoteInMoney.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.txtFillMoneyBanknoteInMoney.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFillMoneyBanknoteInMoney.Enabled = False
        Me.txtFillMoneyBanknoteInMoney.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFillMoneyBanknoteInMoney.Location = New System.Drawing.Point(427, 162)
        Me.txtFillMoneyBanknoteInMoney.MaxLength = 15
        Me.txtFillMoneyBanknoteInMoney.Name = "txtFillMoneyBanknoteInMoney"
        Me.txtFillMoneyBanknoteInMoney.Size = New System.Drawing.Size(161, 31)
        Me.txtFillMoneyBanknoteInMoney.TabIndex = 68
        Me.txtFillMoneyBanknoteInMoney.Text = "0.00"
        Me.txtFillMoneyBanknoteInMoney.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(30, 156)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(325, 42)
        Me.Label10.TabIndex = 67
        Me.Label10.Text = "จำนวนเงินในเครื่องรับธนบัตร"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFillMoneyCoinInMoney
        '
        Me.txtFillMoneyCoinInMoney.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtFillMoneyCoinInMoney.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.txtFillMoneyCoinInMoney.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFillMoneyCoinInMoney.Enabled = False
        Me.txtFillMoneyCoinInMoney.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFillMoneyCoinInMoney.Location = New System.Drawing.Point(427, 120)
        Me.txtFillMoneyCoinInMoney.MaxLength = 15
        Me.txtFillMoneyCoinInMoney.Name = "txtFillMoneyCoinInMoney"
        Me.txtFillMoneyCoinInMoney.Size = New System.Drawing.Size(161, 31)
        Me.txtFillMoneyCoinInMoney.TabIndex = 65
        Me.txtFillMoneyCoinInMoney.Text = "0.00"
        Me.txtFillMoneyCoinInMoney.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(30, 114)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(325, 42)
        Me.Label11.TabIndex = 64
        Me.Label11.Text = "จำนวนเงินในเครื่องรับเหรียญ"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(3, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(754, 41)
        Me.Panel4.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(27, 3)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(318, 35)
        Me.Label7.TabIndex = 66
        Me.Label7.Text = "Fill Money"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFillMoneyMaxBanknote100
        '
        Me.lblFillMoneyMaxBanknote100.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblFillMoneyMaxBanknote100.BackColor = System.Drawing.Color.Transparent
        Me.lblFillMoneyMaxBanknote100.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblFillMoneyMaxBanknote100.ForeColor = System.Drawing.Color.White
        Me.lblFillMoneyMaxBanknote100.Location = New System.Drawing.Point(388, 383)
        Me.lblFillMoneyMaxBanknote100.Name = "lblFillMoneyMaxBanknote100"
        Me.lblFillMoneyMaxBanknote100.Size = New System.Drawing.Size(34, 31)
        Me.lblFillMoneyMaxBanknote100.TabIndex = 87
        Me.lblFillMoneyMaxBanknote100.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblFillMoneyMaxBanknote100.Visible = False
        '
        'tabVendingSetting
        '
        Me.tabVendingSetting.BackColor = System.Drawing.Color.FromArgb(CType(CType(97, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(72, Byte), Integer))
        Me.tabVendingSetting.Controls.Add(Me.btnSettingCancel)
        Me.tabVendingSetting.Controls.Add(Me.btnSettingSave)
        Me.tabVendingSetting.Controls.Add(Me.Label34)
        Me.tabVendingSetting.Controls.Add(Me.txtSettingShelfLong)
        Me.tabVendingSetting.Controls.Add(Me.Label35)
        Me.tabVendingSetting.Controls.Add(Me.Label33)
        Me.tabVendingSetting.Controls.Add(Me.txtSettingScreenSaverTime)
        Me.tabVendingSetting.Controls.Add(Me.Label32)
        Me.tabVendingSetting.Controls.Add(Me.Label31)
        Me.tabVendingSetting.Controls.Add(Me.txtSettingSleepDurationMin)
        Me.tabVendingSetting.Controls.Add(Me.Label30)
        Me.tabVendingSetting.Controls.Add(Me.Label29)
        Me.tabVendingSetting.Controls.Add(Me.txtSettingSleepTimeM)
        Me.tabVendingSetting.Controls.Add(Me.txtSettingSleepTimeH)
        Me.tabVendingSetting.Controls.Add(Me.Label28)
        Me.tabVendingSetting.Controls.Add(Me.Label22)
        Me.tabVendingSetting.Controls.Add(Me.txtSettingPaymentExtend)
        Me.tabVendingSetting.Controls.Add(Me.Label23)
        Me.tabVendingSetting.Controls.Add(Me.Label24)
        Me.tabVendingSetting.Controls.Add(Me.txtSettingMessageTime)
        Me.tabVendingSetting.Controls.Add(Me.Label25)
        Me.tabVendingSetting.Controls.Add(Me.Label26)
        Me.tabVendingSetting.Controls.Add(Me.txtSettingTimeOut)
        Me.tabVendingSetting.Controls.Add(Me.Label27)
        Me.tabVendingSetting.Controls.Add(Me.txtSettingMacAddress)
        Me.tabVendingSetting.Controls.Add(Me.Label18)
        Me.tabVendingSetting.Controls.Add(Me.txtSettingIPAddress)
        Me.tabVendingSetting.Controls.Add(Me.Label19)
        Me.tabVendingSetting.Controls.Add(Me.cbSettingNetworkDevice)
        Me.tabVendingSetting.Controls.Add(Me.Label20)
        Me.tabVendingSetting.Controls.Add(Me.txtSettingVendingID)
        Me.tabVendingSetting.Controls.Add(Me.Label21)
        Me.tabVendingSetting.Controls.Add(Me.Panel6)
        Me.tabVendingSetting.Location = New System.Drawing.Point(4, 22)
        Me.tabVendingSetting.Name = "tabVendingSetting"
        Me.tabVendingSetting.Padding = New System.Windows.Forms.Padding(3)
        Me.tabVendingSetting.Size = New System.Drawing.Size(760, 1340)
        Me.tabVendingSetting.TabIndex = 3
        Me.tabVendingSetting.Text = "Vending Setting"
        '
        'btnSettingCancel
        '
        Me.btnSettingCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnSettingCancel.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnSettingCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSettingCancel.Controls.Add(Me.lblSettingCancel)
        Me.btnSettingCancel.Location = New System.Drawing.Point(345, 524)
        Me.btnSettingCancel.Name = "btnSettingCancel"
        Me.btnSettingCancel.Size = New System.Drawing.Size(126, 43)
        Me.btnSettingCancel.TabIndex = 106
        '
        'lblSettingCancel
        '
        Me.lblSettingCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblSettingCancel.BackColor = System.Drawing.Color.Transparent
        Me.lblSettingCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSettingCancel.ForeColor = System.Drawing.Color.Black
        Me.lblSettingCancel.Location = New System.Drawing.Point(9, 5)
        Me.lblSettingCancel.Name = "lblSettingCancel"
        Me.lblSettingCancel.Size = New System.Drawing.Size(106, 32)
        Me.lblSettingCancel.TabIndex = 35
        Me.lblSettingCancel.Text = "Cancel"
        Me.lblSettingCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSettingSave
        '
        Me.btnSettingSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnSettingSave.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnSettingSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSettingSave.Controls.Add(Me.lblSettingSave)
        Me.btnSettingSave.Location = New System.Drawing.Point(190, 524)
        Me.btnSettingSave.Name = "btnSettingSave"
        Me.btnSettingSave.Size = New System.Drawing.Size(123, 43)
        Me.btnSettingSave.TabIndex = 105
        '
        'lblSettingSave
        '
        Me.lblSettingSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblSettingSave.BackColor = System.Drawing.Color.Transparent
        Me.lblSettingSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSettingSave.ForeColor = System.Drawing.Color.Black
        Me.lblSettingSave.Location = New System.Drawing.Point(8, 5)
        Me.lblSettingSave.Name = "lblSettingSave"
        Me.lblSettingSave.Size = New System.Drawing.Size(103, 32)
        Me.lblSettingSave.TabIndex = 35
        Me.lblSettingSave.Text = "Save"
        Me.lblSettingSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label34
        '
        Me.Label34.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.Color.Transparent
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.White
        Me.Label34.Location = New System.Drawing.Point(422, 386)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(104, 25)
        Me.Label34.TabIndex = 104
        Me.Label34.Text = "Millimeter"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSettingShelfLong
        '
        Me.txtSettingShelfLong.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSettingShelfLong.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSettingShelfLong.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettingShelfLong.Location = New System.Drawing.Point(190, 386)
        Me.txtSettingShelfLong.MaxLength = 15
        Me.txtSettingShelfLong.Name = "txtSettingShelfLong"
        Me.txtSettingShelfLong.Size = New System.Drawing.Size(207, 24)
        Me.txtSettingShelfLong.TabIndex = 102
        Me.txtSettingShelfLong.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label35
        '
        Me.Label35.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.Transparent
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.White
        Me.Label35.Location = New System.Drawing.Point(13, 386)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(115, 25)
        Me.Label35.TabIndex = 103
        Me.Label35.Text = "Shelf Long"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label33
        '
        Me.Label33.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label33.AutoSize = True
        Me.Label33.BackColor = System.Drawing.Color.Transparent
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.White
        Me.Label33.Location = New System.Drawing.Point(422, 349)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(49, 25)
        Me.Label33.TabIndex = 101
        Me.Label33.Text = "Sec"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSettingScreenSaverTime
        '
        Me.txtSettingScreenSaverTime.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSettingScreenSaverTime.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSettingScreenSaverTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettingScreenSaverTime.Location = New System.Drawing.Point(190, 349)
        Me.txtSettingScreenSaverTime.MaxLength = 15
        Me.txtSettingScreenSaverTime.Name = "txtSettingScreenSaverTime"
        Me.txtSettingScreenSaverTime.Size = New System.Drawing.Size(207, 24)
        Me.txtSettingScreenSaverTime.TabIndex = 99
        Me.txtSettingScreenSaverTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label32
        '
        Me.Label32.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.Color.Transparent
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.White
        Me.Label32.Location = New System.Drawing.Point(13, 349)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(142, 25)
        Me.Label32.TabIndex = 100
        Me.Label32.Text = "Screen Saver"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label31
        '
        Me.Label31.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.Color.Transparent
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.White
        Me.Label31.Location = New System.Drawing.Point(422, 475)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(77, 25)
        Me.Label31.TabIndex = 98
        Me.Label31.Text = "Minute"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSettingSleepDurationMin
        '
        Me.txtSettingSleepDurationMin.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSettingSleepDurationMin.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSettingSleepDurationMin.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettingSleepDurationMin.Location = New System.Drawing.Point(190, 475)
        Me.txtSettingSleepDurationMin.MaxLength = 15
        Me.txtSettingSleepDurationMin.Name = "txtSettingSleepDurationMin"
        Me.txtSettingSleepDurationMin.Size = New System.Drawing.Size(207, 24)
        Me.txtSettingSleepDurationMin.TabIndex = 96
        Me.txtSettingSleepDurationMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label30
        '
        Me.Label30.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label30.AutoSize = True
        Me.Label30.BackColor = System.Drawing.Color.Transparent
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.White
        Me.Label30.Location = New System.Drawing.Point(13, 475)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(154, 25)
        Me.Label30.TabIndex = 97
        Me.Label30.Text = "Sleep Duration"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label29
        '
        Me.Label29.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label29.AutoSize = True
        Me.Label29.BackColor = System.Drawing.Color.Transparent
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.White
        Me.Label29.Location = New System.Drawing.Point(248, 433)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(18, 25)
        Me.Label29.TabIndex = 95
        Me.Label29.Text = ":"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSettingSleepTimeM
        '
        Me.txtSettingSleepTimeM.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSettingSleepTimeM.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSettingSleepTimeM.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettingSleepTimeM.Location = New System.Drawing.Point(272, 433)
        Me.txtSettingSleepTimeM.MaxLength = 2
        Me.txtSettingSleepTimeM.Name = "txtSettingSleepTimeM"
        Me.txtSettingSleepTimeM.Size = New System.Drawing.Size(52, 24)
        Me.txtSettingSleepTimeM.TabIndex = 94
        Me.txtSettingSleepTimeM.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSettingSleepTimeH
        '
        Me.txtSettingSleepTimeH.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSettingSleepTimeH.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSettingSleepTimeH.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettingSleepTimeH.Location = New System.Drawing.Point(190, 433)
        Me.txtSettingSleepTimeH.MaxLength = 2
        Me.txtSettingSleepTimeH.Name = "txtSettingSleepTimeH"
        Me.txtSettingSleepTimeH.Size = New System.Drawing.Size(52, 24)
        Me.txtSettingSleepTimeH.TabIndex = 93
        Me.txtSettingSleepTimeH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label28
        '
        Me.Label28.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.White
        Me.Label28.Location = New System.Drawing.Point(13, 433)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(120, 25)
        Me.Label28.TabIndex = 92
        Me.Label28.Text = "Sleep Time"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label22
        '
        Me.Label22.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(422, 312)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(49, 25)
        Me.Label22.TabIndex = 90
        Me.Label22.Text = "Sec"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSettingPaymentExtend
        '
        Me.txtSettingPaymentExtend.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSettingPaymentExtend.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSettingPaymentExtend.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettingPaymentExtend.Location = New System.Drawing.Point(190, 312)
        Me.txtSettingPaymentExtend.MaxLength = 15
        Me.txtSettingPaymentExtend.Name = "txtSettingPaymentExtend"
        Me.txtSettingPaymentExtend.Size = New System.Drawing.Size(207, 24)
        Me.txtSettingPaymentExtend.TabIndex = 84
        Me.txtSettingPaymentExtend.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label23
        '
        Me.Label23.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(13, 312)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(169, 25)
        Me.Label23.TabIndex = 89
        Me.Label23.Text = "Payment Extend"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label24
        '
        Me.Label24.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(422, 274)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(49, 25)
        Me.Label24.TabIndex = 88
        Me.Label24.Text = "Sec"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSettingMessageTime
        '
        Me.txtSettingMessageTime.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSettingMessageTime.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSettingMessageTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettingMessageTime.Location = New System.Drawing.Point(190, 274)
        Me.txtSettingMessageTime.MaxLength = 15
        Me.txtSettingMessageTime.Name = "txtSettingMessageTime"
        Me.txtSettingMessageTime.Size = New System.Drawing.Size(207, 24)
        Me.txtSettingMessageTime.TabIndex = 83
        Me.txtSettingMessageTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label25
        '
        Me.Label25.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(13, 274)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(153, 25)
        Me.Label25.TabIndex = 87
        Me.Label25.Text = "Message Time"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label26
        '
        Me.Label26.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.White
        Me.Label26.Location = New System.Drawing.Point(422, 235)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(49, 25)
        Me.Label26.TabIndex = 86
        Me.Label26.Text = "Sec"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSettingTimeOut
        '
        Me.txtSettingTimeOut.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSettingTimeOut.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSettingTimeOut.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettingTimeOut.Location = New System.Drawing.Point(190, 235)
        Me.txtSettingTimeOut.MaxLength = 15
        Me.txtSettingTimeOut.Name = "txtSettingTimeOut"
        Me.txtSettingTimeOut.Size = New System.Drawing.Size(207, 24)
        Me.txtSettingTimeOut.TabIndex = 82
        Me.txtSettingTimeOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label27
        '
        Me.Label27.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.White
        Me.Label27.Location = New System.Drawing.Point(13, 235)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(99, 25)
        Me.Label27.TabIndex = 85
        Me.Label27.Text = "Time Out"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSettingMacAddress
        '
        Me.txtSettingMacAddress.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSettingMacAddress.BackColor = System.Drawing.SystemColors.Control
        Me.txtSettingMacAddress.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSettingMacAddress.Enabled = False
        Me.txtSettingMacAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettingMacAddress.Location = New System.Drawing.Point(190, 189)
        Me.txtSettingMacAddress.MaxLength = 15
        Me.txtSettingMacAddress.Name = "txtSettingMacAddress"
        Me.txtSettingMacAddress.Size = New System.Drawing.Size(207, 24)
        Me.txtSettingMacAddress.TabIndex = 80
        '
        'Label18
        '
        Me.Label18.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(13, 189)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(138, 25)
        Me.Label18.TabIndex = 81
        Me.Label18.Text = "Mac Address"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSettingIPAddress
        '
        Me.txtSettingIPAddress.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSettingIPAddress.BackColor = System.Drawing.SystemColors.Control
        Me.txtSettingIPAddress.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSettingIPAddress.Enabled = False
        Me.txtSettingIPAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettingIPAddress.Location = New System.Drawing.Point(190, 150)
        Me.txtSettingIPAddress.MaxLength = 15
        Me.txtSettingIPAddress.Name = "txtSettingIPAddress"
        Me.txtSettingIPAddress.Size = New System.Drawing.Size(207, 24)
        Me.txtSettingIPAddress.TabIndex = 78
        '
        'Label19
        '
        Me.Label19.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(13, 150)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(116, 25)
        Me.Label19.TabIndex = 79
        Me.Label19.Text = "IP Address"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbSettingNetworkDevice
        '
        Me.cbSettingNetworkDevice.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.cbSettingNetworkDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSettingNetworkDevice.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbSettingNetworkDevice.FormattingEnabled = True
        Me.cbSettingNetworkDevice.Location = New System.Drawing.Point(190, 104)
        Me.cbSettingNetworkDevice.Name = "cbSettingNetworkDevice"
        Me.cbSettingNetworkDevice.Size = New System.Drawing.Size(528, 33)
        Me.cbSettingNetworkDevice.TabIndex = 77
        '
        'Label20
        '
        Me.Label20.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(13, 107)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(162, 25)
        Me.Label20.TabIndex = 76
        Me.Label20.Text = "Network Device"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSettingVendingID
        '
        Me.txtSettingVendingID.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSettingVendingID.BackColor = System.Drawing.SystemColors.Control
        Me.txtSettingVendingID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSettingVendingID.Enabled = False
        Me.txtSettingVendingID.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettingVendingID.Location = New System.Drawing.Point(190, 65)
        Me.txtSettingVendingID.MaxLength = 15
        Me.txtSettingVendingID.Name = "txtSettingVendingID"
        Me.txtSettingVendingID.Size = New System.Drawing.Size(207, 24)
        Me.txtSettingVendingID.TabIndex = 74
        Me.txtSettingVendingID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label21
        '
        Me.Label21.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(13, 65)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(117, 25)
        Me.Label21.TabIndex = 75
        Me.Label21.Text = "Vending ID"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel6.Controls.Add(Me.Label17)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel6.Location = New System.Drawing.Point(3, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(754, 41)
        Me.Panel6.TabIndex = 4
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(27, 3)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(396, 35)
        Me.Label17.TabIndex = 66
        Me.Label17.Text = "Vending Machine Setting"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabDeviceSetting
        '
        Me.tabDeviceSetting.BackColor = System.Drawing.Color.FromArgb(CType(CType(97, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(72, Byte), Integer))
        Me.tabDeviceSetting.Controls.Add(Me.cbDeviceSettingPrinterName)
        Me.tabDeviceSetting.Controls.Add(Me.Label44)
        Me.tabDeviceSetting.Controls.Add(Me.Label43)
        Me.tabDeviceSetting.Controls.Add(Me.dgvDeviceSettingCoinOutList)
        Me.tabDeviceSetting.Controls.Add(Me.btnDeviceSettingCancel)
        Me.tabDeviceSetting.Controls.Add(Me.btnDeviceSettingSave)
        Me.tabDeviceSetting.Controls.Add(Me.dgvDeviceSettingBanknoteOutList)
        Me.tabDeviceSetting.Controls.Add(Me.Label42)
        Me.tabDeviceSetting.Controls.Add(Me.cbDeviceSettingCoinIn)
        Me.tabDeviceSetting.Controls.Add(Me.cbDeviceSettingBanknoteIn)
        Me.tabDeviceSetting.Controls.Add(Me.Label40)
        Me.tabDeviceSetting.Controls.Add(Me.Label41)
        Me.tabDeviceSetting.Controls.Add(Me.Panel10)
        Me.tabDeviceSetting.Location = New System.Drawing.Point(4, 22)
        Me.tabDeviceSetting.Name = "tabDeviceSetting"
        Me.tabDeviceSetting.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDeviceSetting.Size = New System.Drawing.Size(760, 1340)
        Me.tabDeviceSetting.TabIndex = 7
        Me.tabDeviceSetting.Text = "Device Setting"
        '
        'Label43
        '
        Me.Label43.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label43.AutoSize = True
        Me.Label43.BackColor = System.Drawing.Color.Transparent
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label43.ForeColor = System.Drawing.Color.White
        Me.Label43.Location = New System.Drawing.Point(382, 261)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(206, 33)
        Me.Label43.TabIndex = 110
        Me.Label43.Text = "เครื่องทอนเหรียญ"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvDeviceSettingCoinOutList
        '
        Me.dgvDeviceSettingCoinOutList.AllowUserToAddRows = False
        Me.dgvDeviceSettingCoinOutList.AllowUserToDeleteRows = False
        Me.dgvDeviceSettingCoinOutList.AllowUserToResizeColumns = False
        Me.dgvDeviceSettingCoinOutList.AllowUserToResizeRows = False
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDeviceSettingCoinOutList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvDeviceSettingCoinOutList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDeviceSettingCoinOutList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colCoinDeviceID, Me.colCoinDeviceNameTh, Me.colCoinComport})
        Me.dgvDeviceSettingCoinOutList.Location = New System.Drawing.Point(388, 297)
        Me.dgvDeviceSettingCoinOutList.Name = "dgvDeviceSettingCoinOutList"
        Me.dgvDeviceSettingCoinOutList.RowHeadersWidth = 10
        Me.dgvDeviceSettingCoinOutList.RowTemplate.Height = 35
        Me.dgvDeviceSettingCoinOutList.Size = New System.Drawing.Size(318, 198)
        Me.dgvDeviceSettingCoinOutList.TabIndex = 109
        '
        'btnDeviceSettingCancel
        '
        Me.btnDeviceSettingCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnDeviceSettingCancel.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnDeviceSettingCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnDeviceSettingCancel.Controls.Add(Me.lblDeviceSettingCancel)
        Me.btnDeviceSettingCancel.Location = New System.Drawing.Point(358, 615)
        Me.btnDeviceSettingCancel.Name = "btnDeviceSettingCancel"
        Me.btnDeviceSettingCancel.Size = New System.Drawing.Size(126, 43)
        Me.btnDeviceSettingCancel.TabIndex = 108
        '
        'lblDeviceSettingCancel
        '
        Me.lblDeviceSettingCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblDeviceSettingCancel.BackColor = System.Drawing.Color.Transparent
        Me.lblDeviceSettingCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblDeviceSettingCancel.ForeColor = System.Drawing.Color.Black
        Me.lblDeviceSettingCancel.Location = New System.Drawing.Point(9, 5)
        Me.lblDeviceSettingCancel.Name = "lblDeviceSettingCancel"
        Me.lblDeviceSettingCancel.Size = New System.Drawing.Size(106, 32)
        Me.lblDeviceSettingCancel.TabIndex = 35
        Me.lblDeviceSettingCancel.Text = "Cancel"
        Me.lblDeviceSettingCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnDeviceSettingSave
        '
        Me.btnDeviceSettingSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnDeviceSettingSave.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnDeviceSettingSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnDeviceSettingSave.Controls.Add(Me.lblDeviceSettingSave)
        Me.btnDeviceSettingSave.Location = New System.Drawing.Point(203, 615)
        Me.btnDeviceSettingSave.Name = "btnDeviceSettingSave"
        Me.btnDeviceSettingSave.Size = New System.Drawing.Size(123, 43)
        Me.btnDeviceSettingSave.TabIndex = 107
        '
        'lblDeviceSettingSave
        '
        Me.lblDeviceSettingSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblDeviceSettingSave.BackColor = System.Drawing.Color.Transparent
        Me.lblDeviceSettingSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblDeviceSettingSave.ForeColor = System.Drawing.Color.Black
        Me.lblDeviceSettingSave.Location = New System.Drawing.Point(8, 5)
        Me.lblDeviceSettingSave.Name = "lblDeviceSettingSave"
        Me.lblDeviceSettingSave.Size = New System.Drawing.Size(103, 32)
        Me.lblDeviceSettingSave.TabIndex = 35
        Me.lblDeviceSettingSave.Text = "Save"
        Me.lblDeviceSettingSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dgvDeviceSettingBanknoteOutList
        '
        Me.dgvDeviceSettingBanknoteOutList.AllowUserToAddRows = False
        Me.dgvDeviceSettingBanknoteOutList.AllowUserToDeleteRows = False
        Me.dgvDeviceSettingBanknoteOutList.AllowUserToResizeColumns = False
        Me.dgvDeviceSettingBanknoteOutList.AllowUserToResizeRows = False
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDeviceSettingBanknoteOutList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvDeviceSettingBanknoteOutList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDeviceSettingBanknoteOutList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colBanknoteDeviceID, Me.colDeviceNameTh, Me.colComPort})
        Me.dgvDeviceSettingBanknoteOutList.Location = New System.Drawing.Point(16, 297)
        Me.dgvDeviceSettingBanknoteOutList.Name = "dgvDeviceSettingBanknoteOutList"
        Me.dgvDeviceSettingBanknoteOutList.RowHeadersWidth = 10
        Me.dgvDeviceSettingBanknoteOutList.RowTemplate.Height = 35
        Me.dgvDeviceSettingBanknoteOutList.Size = New System.Drawing.Size(318, 198)
        Me.dgvDeviceSettingBanknoteOutList.TabIndex = 86
        '
        'Label42
        '
        Me.Label42.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label42.AutoSize = True
        Me.Label42.BackColor = System.Drawing.Color.Transparent
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(10, 261)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(210, 33)
        Me.Label42.TabIndex = 85
        Me.Label42.Text = "เครื่องทอนธนบัตร"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbDeviceSettingCoinIn
        '
        Me.cbDeviceSettingCoinIn.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.cbDeviceSettingCoinIn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbDeviceSettingCoinIn.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbDeviceSettingCoinIn.FormattingEnabled = True
        Me.cbDeviceSettingCoinIn.Location = New System.Drawing.Point(317, 195)
        Me.cbDeviceSettingCoinIn.Name = "cbDeviceSettingCoinIn"
        Me.cbDeviceSettingCoinIn.Size = New System.Drawing.Size(199, 37)
        Me.cbDeviceSettingCoinIn.TabIndex = 84
        '
        'cbDeviceSettingBanknoteIn
        '
        Me.cbDeviceSettingBanknoteIn.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.cbDeviceSettingBanknoteIn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbDeviceSettingBanknoteIn.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbDeviceSettingBanknoteIn.FormattingEnabled = True
        Me.cbDeviceSettingBanknoteIn.Location = New System.Drawing.Point(317, 138)
        Me.cbDeviceSettingBanknoteIn.Name = "cbDeviceSettingBanknoteIn"
        Me.cbDeviceSettingBanknoteIn.Size = New System.Drawing.Size(199, 37)
        Me.cbDeviceSettingBanknoteIn.TabIndex = 83
        '
        'Label40
        '
        Me.Label40.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Transparent
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label40.ForeColor = System.Drawing.Color.White
        Me.Label40.Location = New System.Drawing.Point(87, 194)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(187, 33)
        Me.Label40.TabIndex = 82
        Me.Label40.Text = "เครื่องรับเหรียญ"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label41
        '
        Me.Label41.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label41.AutoSize = True
        Me.Label41.BackColor = System.Drawing.Color.Transparent
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label41.ForeColor = System.Drawing.Color.White
        Me.Label41.Location = New System.Drawing.Point(87, 137)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(191, 33)
        Me.Label41.TabIndex = 81
        Me.Label41.Text = "เครื่องรับธนบัตร"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel10.Controls.Add(Me.Label39)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel10.Location = New System.Drawing.Point(3, 3)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(754, 41)
        Me.Panel10.TabIndex = 8
        '
        'Label39
        '
        Me.Label39.BackColor = System.Drawing.Color.Transparent
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label39.ForeColor = System.Drawing.Color.White
        Me.Label39.Location = New System.Drawing.Point(27, 3)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(396, 35)
        Me.Label39.TabIndex = 66
        Me.Label39.Text = "Device Setting"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabSetupShelf
        '
        Me.tabSetupShelf.BackColor = System.Drawing.Color.FromArgb(CType(CType(97, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(72, Byte), Integer))
        Me.tabSetupShelf.Controls.Add(Me.btnAddShelf)
        Me.tabSetupShelf.Controls.Add(Me.pnlSetupShelfLayout)
        Me.tabSetupShelf.Controls.Add(Me.Panel7)
        Me.tabSetupShelf.Location = New System.Drawing.Point(4, 22)
        Me.tabSetupShelf.Name = "tabSetupShelf"
        Me.tabSetupShelf.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSetupShelf.Size = New System.Drawing.Size(760, 1340)
        Me.tabSetupShelf.TabIndex = 4
        Me.tabSetupShelf.Text = "Setup Shelf"
        '
        'btnAddShelf
        '
        Me.btnAddShelf.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnAddShelf.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnAddShelf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAddShelf.Controls.Add(Me.lblAddShelf)
        Me.btnAddShelf.Location = New System.Drawing.Point(7, 53)
        Me.btnAddShelf.Name = "btnAddShelf"
        Me.btnAddShelf.Size = New System.Drawing.Size(138, 43)
        Me.btnAddShelf.TabIndex = 106
        '
        'lblAddShelf
        '
        Me.lblAddShelf.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblAddShelf.BackColor = System.Drawing.Color.Transparent
        Me.lblAddShelf.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblAddShelf.ForeColor = System.Drawing.Color.Black
        Me.lblAddShelf.Location = New System.Drawing.Point(9, 5)
        Me.lblAddShelf.Name = "lblAddShelf"
        Me.lblAddShelf.Size = New System.Drawing.Size(119, 32)
        Me.lblAddShelf.TabIndex = 35
        Me.lblAddShelf.Text = "Add Shelf"
        Me.lblAddShelf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlSetupShelfLayout
        '
        Me.pnlSetupShelfLayout.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlSetupShelfLayout.Location = New System.Drawing.Point(3, 102)
        Me.pnlSetupShelfLayout.Name = "pnlSetupShelfLayout"
        Me.pnlSetupShelfLayout.Size = New System.Drawing.Size(754, 1235)
        Me.pnlSetupShelfLayout.TabIndex = 6
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel7.Controls.Add(Me.Label36)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel7.Location = New System.Drawing.Point(3, 3)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(754, 41)
        Me.Panel7.TabIndex = 5
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(27, 3)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(396, 35)
        Me.Label36.TabIndex = 66
        Me.Label36.Text = "Setup Shelf"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabSetupProduct
        '
        Me.tabSetupProduct.BackColor = System.Drawing.Color.FromArgb(CType(CType(97, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(72, Byte), Integer))
        Me.tabSetupProduct.Controls.Add(Me.pnlSetupProduct)
        Me.tabSetupProduct.Controls.Add(Me.Panel8)
        Me.tabSetupProduct.Location = New System.Drawing.Point(4, 22)
        Me.tabSetupProduct.Name = "tabSetupProduct"
        Me.tabSetupProduct.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSetupProduct.Size = New System.Drawing.Size(760, 1340)
        Me.tabSetupProduct.TabIndex = 5
        Me.tabSetupProduct.Text = "Setup Product"
        '
        'pnlSetupProduct
        '
        Me.pnlSetupProduct.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlSetupProduct.Location = New System.Drawing.Point(3, 50)
        Me.pnlSetupProduct.Name = "pnlSetupProduct"
        Me.pnlSetupProduct.Size = New System.Drawing.Size(754, 1287)
        Me.pnlSetupProduct.TabIndex = 7
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel8.Controls.Add(Me.Label37)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel8.Location = New System.Drawing.Point(3, 3)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(754, 41)
        Me.Panel8.TabIndex = 6
        '
        'Label37
        '
        Me.Label37.BackColor = System.Drawing.Color.Transparent
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(27, 3)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(396, 35)
        Me.Label37.TabIndex = 66
        Me.Label37.Text = "Setup Product"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabSetupSlip
        '
        Me.tabSetupSlip.BackColor = System.Drawing.Color.FromArgb(CType(CType(97, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(72, Byte), Integer))
        Me.tabSetupSlip.Controls.Add(Me.UcSetupSlip1)
        Me.tabSetupSlip.Controls.Add(Me.Panel9)
        Me.tabSetupSlip.Location = New System.Drawing.Point(4, 22)
        Me.tabSetupSlip.Name = "tabSetupSlip"
        Me.tabSetupSlip.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSetupSlip.Size = New System.Drawing.Size(760, 1340)
        Me.tabSetupSlip.TabIndex = 6
        Me.tabSetupSlip.Text = "Setup Slip"
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel9.Controls.Add(Me.Label38)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel9.Location = New System.Drawing.Point(3, 3)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(754, 41)
        Me.Panel9.TabIndex = 7
        '
        'Label38
        '
        Me.Label38.BackColor = System.Drawing.Color.Transparent
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.White
        Me.Label38.Location = New System.Drawing.Point(27, 3)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(396, 35)
        Me.Label38.TabIndex = 66
        Me.Label38.Text = "Setup Slip"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'colCoinDeviceID
        '
        Me.colCoinDeviceID.DataPropertyName = "device_id"
        Me.colCoinDeviceID.HeaderText = "DeviceID"
        Me.colCoinDeviceID.Name = "colCoinDeviceID"
        Me.colCoinDeviceID.Visible = False
        '
        'colCoinDeviceNameTh
        '
        Me.colCoinDeviceNameTh.DataPropertyName = "device_name_th"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colCoinDeviceNameTh.DefaultCellStyle = DataGridViewCellStyle8
        Me.colCoinDeviceNameTh.HeaderText = "เครื่องทอนเหรียญ"
        Me.colCoinDeviceNameTh.Name = "colCoinDeviceNameTh"
        Me.colCoinDeviceNameTh.Width = 200
        '
        'colCoinComport
        '
        Me.colCoinComport.DataPropertyName = "comport_vid"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colCoinComport.DefaultCellStyle = DataGridViewCellStyle9
        Me.colCoinComport.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.colCoinComport.DropDownWidth = 8
        Me.colCoinComport.HeaderText = "Com Port"
        Me.colCoinComport.Name = "colCoinComport"
        '
        'colBanknoteDeviceID
        '
        Me.colBanknoteDeviceID.DataPropertyName = "device_id"
        Me.colBanknoteDeviceID.HeaderText = "DeviceID"
        Me.colBanknoteDeviceID.Name = "colBanknoteDeviceID"
        Me.colBanknoteDeviceID.Visible = False
        '
        'colDeviceNameTh
        '
        Me.colDeviceNameTh.DataPropertyName = "device_name_th"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colDeviceNameTh.DefaultCellStyle = DataGridViewCellStyle11
        Me.colDeviceNameTh.HeaderText = "เครื่องทอนธนบัตร"
        Me.colDeviceNameTh.Name = "colDeviceNameTh"
        Me.colDeviceNameTh.Width = 200
        '
        'colComPort
        '
        Me.colComPort.DataPropertyName = "comport_vid"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colComPort.DefaultCellStyle = DataGridViewCellStyle12
        Me.colComPort.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.colComPort.DropDownWidth = 8
        Me.colComPort.HeaderText = "Com Port"
        Me.colComPort.Name = "colComPort"
        '
        'UcSetupSlip1
        '
        Me.UcSetupSlip1.Location = New System.Drawing.Point(36, 74)
        Me.UcSetupSlip1.Name = "UcSetupSlip1"
        Me.UcSetupSlip1.Size = New System.Drawing.Size(662, 551)
        Me.UcSetupSlip1.TabIndex = 8
        '
        'Label44
        '
        Me.Label44.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label44.AutoSize = True
        Me.Label44.BackColor = System.Drawing.Color.Transparent
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label44.ForeColor = System.Drawing.Color.White
        Me.Label44.Location = New System.Drawing.Point(87, 73)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(192, 33)
        Me.Label44.TabIndex = 111
        Me.Label44.Text = "เครื่องพิมพ์ Slip"
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbDeviceSettingPrinterName
        '
        Me.cbDeviceSettingPrinterName.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.cbDeviceSettingPrinterName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbDeviceSettingPrinterName.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbDeviceSettingPrinterName.FormattingEnabled = True
        Me.cbDeviceSettingPrinterName.Location = New System.Drawing.Point(317, 74)
        Me.cbDeviceSettingPrinterName.Name = "cbDeviceSettingPrinterName"
        Me.cbDeviceSettingPrinterName.Size = New System.Drawing.Size(389, 37)
        Me.cbDeviceSettingPrinterName.TabIndex = 112
        '
        'frmSC_StockAndHardware
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(97, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(72, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(768, 1366)
        Me.Controls.Add(Me.TabContainer1)
        Me.Controls.Add(Me.pbBackground)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSC_StockAndHardware"
        Me.Text = "frmSC_StockAndHardware"
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabContainer1.ResumeLayout(False)
        Me.tabStockAndHardware.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.pbClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabFillPaper.ResumeLayout(False)
        Me.tabFillPaper.PerformLayout()
        Me.btnFillPaperCancel.ResumeLayout(False)
        Me.btnFillPaperConfirm.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.tabFillMoney.ResumeLayout(False)
        Me.tabFillMoney.PerformLayout()
        Me.btnFillMoneyCancel.ResumeLayout(False)
        Me.btnFillMoneyConfirm.ResumeLayout(False)
        Me.btnFillMoneyAllFull.ResumeLayout(False)
        Me.btnFillMoneyCheckOutMoney.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.tabVendingSetting.ResumeLayout(False)
        Me.tabVendingSetting.PerformLayout()
        Me.btnSettingCancel.ResumeLayout(False)
        Me.btnSettingSave.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.tabDeviceSetting.ResumeLayout(False)
        Me.tabDeviceSetting.PerformLayout()
        CType(Me.dgvDeviceSettingCoinOutList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.btnDeviceSettingCancel.ResumeLayout(False)
        Me.btnDeviceSettingSave.ResumeLayout(False)
        CType(Me.dgvDeviceSettingBanknoteOutList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.tabSetupShelf.ResumeLayout(False)
        Me.btnAddShelf.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.tabSetupProduct.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        Me.tabSetupSlip.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TimerCheckDoorClose As Timer
    Friend WithEvents pbBackground As PictureBox
    Friend WithEvents TabContainer1 As TabControl
    Friend WithEvents tabStockAndHardware As TabPage
    Friend WithEvents tabFillPaper As TabPage
    Friend WithEvents Panel1 As Panel
    Friend WithEvents pbClose As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents flpHWStatus As FlowLayoutPanel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents flpM_Stock As FlowLayoutPanel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents pnlProductShelf As Panel
    Friend WithEvents txtFillPaperValue As TextBox
    Friend WithEvents txtFillPaperMax As TextBox
    Friend WithEvents btnFillPaperCancel As Panel
    Friend WithEvents lblFillPaperCancel As Label
    Friend WithEvents btnFillPaperConfirm As Panel
    Friend WithEvents lblFillPaperConfirm As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label4 As Label
    Friend WithEvents tabFillMoney As TabPage
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents btnFillMoneyCheckOutMoney As Panel
    Friend WithEvents lblFillMoneyCheckOutMoney As Label
    Friend WithEvents txtFillMoneyBanknoteInMoney As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtFillMoneyCoinInMoney As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents lblFillMoneyMaxBanknote100 As Label
    Friend WithEvents lblFillMoneyMaxBanknote20 As Label
    Friend WithEvents lblFillMoneyMaxCoin5 As Label
    Friend WithEvents btnFillMoneyAllFull As Panel
    Friend WithEvents lblFillMoneyAllFull As Label
    Friend WithEvents lblFillMoneyTotalBanknoteOut100 As Label
    Friend WithEvents txtFillMoneyBanknoteOut100 As TextBox
    Friend WithEvents lblFillMoneyTotalBanknoteOut20 As Label
    Friend WithEvents txtFillMoneyBanknoteOut20 As TextBox
    Friend WithEvents lblFillMoneyTotalCoinOut5 As Label
    Friend WithEvents txtFillMoneyCoinOut5 As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents txtFillMoneyTotalMoney As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents btnFillMoneyCancel As Panel
    Friend WithEvents lblFillMoneyCancel As Label
    Friend WithEvents btnFillMoneyConfirm As Panel
    Friend WithEvents lblFillMoneyConfirm As Label
    Friend WithEvents tabVendingSetting As TabPage
    Friend WithEvents Label22 As Label
    Friend WithEvents txtSettingPaymentExtend As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents txtSettingMessageTime As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents txtSettingTimeOut As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents txtSettingMacAddress As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents txtSettingIPAddress As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents cbSettingNetworkDevice As ComboBox
    Friend WithEvents Label20 As Label
    Friend WithEvents txtSettingVendingID As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Label17 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents txtSettingSleepTimeM As TextBox
    Friend WithEvents txtSettingSleepTimeH As TextBox
    Friend WithEvents Label28 As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents txtSettingSleepDurationMin As TextBox
    Friend WithEvents Label30 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents txtSettingScreenSaverTime As TextBox
    Friend WithEvents Label32 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents txtSettingShelfLong As TextBox
    Friend WithEvents Label35 As Label
    Friend WithEvents btnSettingCancel As Panel
    Friend WithEvents lblSettingCancel As Label
    Friend WithEvents btnSettingSave As Panel
    Friend WithEvents lblSettingSave As Label
    Friend WithEvents tabSetupShelf As TabPage
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Label36 As Label
    Friend WithEvents pnlSetupShelfLayout As Panel
    Friend WithEvents btnAddShelf As Panel
    Friend WithEvents lblAddShelf As Label
    Friend WithEvents tabSetupProduct As TabPage
    Friend WithEvents pnlSetupProduct As Panel
    Friend WithEvents Panel8 As Panel
    Friend WithEvents Label37 As Label
    Friend WithEvents tabSetupSlip As TabPage
    Friend WithEvents Panel9 As Panel
    Friend WithEvents Label38 As Label
    Friend WithEvents UcSetupSlip1 As ucSetupSlip
    Friend WithEvents tabDeviceSetting As TabPage
    Friend WithEvents Panel10 As Panel
    Friend WithEvents Label39 As Label
    Friend WithEvents dgvDeviceSettingBanknoteOutList As DataGridView
    Friend WithEvents Label42 As Label
    Friend WithEvents cbDeviceSettingCoinIn As ComboBox
    Friend WithEvents cbDeviceSettingBanknoteIn As ComboBox
    Friend WithEvents Label40 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents btnDeviceSettingCancel As Panel
    Friend WithEvents lblDeviceSettingCancel As Label
    Friend WithEvents btnDeviceSettingSave As Panel
    Friend WithEvents lblDeviceSettingSave As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents dgvDeviceSettingCoinOutList As DataGridView
    Friend WithEvents colCoinDeviceID As DataGridViewTextBoxColumn
    Friend WithEvents colCoinDeviceNameTh As DataGridViewTextBoxColumn
    Friend WithEvents colCoinComport As DataGridViewComboBoxColumn
    Friend WithEvents colBanknoteDeviceID As DataGridViewTextBoxColumn
    Friend WithEvents colDeviceNameTh As DataGridViewTextBoxColumn
    Friend WithEvents colComPort As DataGridViewComboBoxColumn
    Friend WithEvents cbDeviceSettingPrinterName As ComboBox
    Friend WithEvents Label44 As Label
End Class
