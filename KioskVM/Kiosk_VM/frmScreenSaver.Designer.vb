﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmScreenSaver
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmScreenSaver))
        Me.VDO = New AxWMPLib.AxWindowsMediaPlayer()
        Me.TimerSync = New System.Windows.Forms.Timer(Me.components)
        Me.lblCurrentTime = New System.Windows.Forms.Label()
        Me.lblCurrentFile = New System.Windows.Forms.Label()
        Me.TimerSetTime = New System.Windows.Forms.Timer(Me.components)
        CType(Me.VDO, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'VDO
        '
        Me.VDO.Dock = System.Windows.Forms.DockStyle.Fill
        Me.VDO.Enabled = True
        Me.VDO.Location = New System.Drawing.Point(0, 0)
        Me.VDO.Name = "VDO"
        Me.VDO.OcxState = CType(resources.GetObject("VDO.OcxState"), System.Windows.Forms.AxHost.State)
        Me.VDO.Size = New System.Drawing.Size(677, 369)
        Me.VDO.TabIndex = 0
        '
        'TimerSync
        '
        '
        'lblCurrentTime
        '
        Me.lblCurrentTime.AutoSize = True
        Me.lblCurrentTime.Location = New System.Drawing.Point(12, 9)
        Me.lblCurrentTime.Name = "lblCurrentTime"
        Me.lblCurrentTime.Size = New System.Drawing.Size(13, 13)
        Me.lblCurrentTime.TabIndex = 1
        Me.lblCurrentTime.Text = "0"
        '
        'lblCurrentFile
        '
        Me.lblCurrentFile.AutoSize = True
        Me.lblCurrentFile.Location = New System.Drawing.Point(12, 32)
        Me.lblCurrentFile.Name = "lblCurrentFile"
        Me.lblCurrentFile.Size = New System.Drawing.Size(0, 13)
        Me.lblCurrentFile.TabIndex = 2
        '
        'TimerSetTime
        '
        Me.TimerSetTime.Enabled = True
        Me.TimerSetTime.Interval = 1000
        '
        'frmScreenServer
        '
        Me.ClientSize = New System.Drawing.Size(677, 369)
        Me.Controls.Add(Me.lblCurrentFile)
        Me.Controls.Add(Me.lblCurrentTime)
        Me.Controls.Add(Me.VDO)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmScreenServer"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.VDO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents VDO As AxWMPLib.AxWindowsMediaPlayer
    Friend WithEvents TimerSync As Timer
    Friend WithEvents lblCurrentTime As Label
    Friend WithEvents lblCurrentFile As Label
    Friend WithEvents TimerSetTime As Timer
End Class
