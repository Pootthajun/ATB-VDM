﻿Imports AxWMPLib
Imports System.IO

Public Class frmScreenSaver
    Private Sub frmScreenServer_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub frmScreenServer_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.ScreenSaver

            lblCurrentTime.Text = frmHomeVertical.HomeCurrentScreenSaverTime.Text
            lblCurrentFile.Text = frmHomeVertical.HomeCurrentScreenSaverFile.Text
            InsertLogSalesActivity(vTrans.TransNo, "แสดง Screen Saver File " & lblCurrentFile.Text, VendingConfig.SelectForm, False)

            Dim dblStartTime As Double = 0.0
            If lblCurrentTime.Text <> "0" AndAlso lblCurrentTime.Text <> "" Then
                If lblCurrentTime.Text.Length = 5 Then lblCurrentTime.Text = "00:" & lblCurrentTime.Text
                dblStartTime = GetSecFromTimeFormat(lblCurrentTime.Text)
            End If

            Dim myStack As New Stack
            myStack = GetFileFromDirectory(lblCurrentFile.Text)
            If myStack.Count = 0 Then Exit Sub

            TimerSetTime.Start()

            VDO.settings.setMode("loop", True)
            For i As Integer = 0 To myStack.Count - 1
                Dim file As String = myStack.Pop
                Dim songs = VDO.newMedia(file)
                VDO.currentPlaylist.appendItem(songs)
            Next

            VDO.Ctlcontrols.currentPosition = dblStartTime
            VDO.uiMode = "none"
            'VDO.uiMode = "full"
            VDO.stretchToFit = True   'ให้ขยายเต็มจอ
            VDO.Ctlcontrols.play()
            TimerSync.Enabled = True
        Catch ex As Exception
        End Try
    End Sub

    Function GetFileFromDirectory(currentfile As String) As Stack
        Dim myStack As New Stack

        If Directory.Exists(Application.StartupPath & "\ScreenSaver") = True Then
            Dim di As New DirectoryInfo(Application.StartupPath & "\ScreenSaver")
            Dim files As FileSystemInfo() = di.GetFileSystemInfos
            Dim orderedFiles = files.OrderBy(Function(f) f.Name)

            Dim tmpStack1 As New Stack
            Dim tmpStack2 As New Stack
            Dim isMyFile As Boolean = False
            For Each f As FileSystemInfo In orderedFiles
                Dim File As String = f.FullName
                Dim fileType As String = f.Extension.ToLower
                If fileType <> ".mp4" And fileType <> ".wmv" And fileType <> ".flv" Then
                    Continue For
                End If
                If File = currentfile Then
                    tmpStack1.Push(File)
                    isMyFile = True
                Else
                    If isMyFile Then tmpStack1.Push(File) Else tmpStack2.Push(File)
                End If
            Next

            If tmpStack2.Count > 0 Then
                For i As Integer = 0 To tmpStack2.Count - 1
                    myStack.Push(tmpStack2.Pop)
                Next
            End If

            If tmpStack1.Count > 0 Then
                For i As Integer = 0 To tmpStack1.Count - 1
                    myStack.Push(tmpStack1.Pop)
                Next
            End If
        End If
        Return myStack
    End Function

    Public Shared Function GetSecFromTimeFormat(ByVal TimeFormat As String) As Double
        'แปลงเวลาในรูปแบบ HH:mm:ss ไปเป็นวินาที
        Dim ret As Double = 0
        If TimeFormat.Trim <> "" Then
            Dim tmp() As String = Split(TimeFormat, ":")
            Dim TimeSec As Integer = 0
            If CDbl(tmp(0)) > 0 Then
                TimeSec += (CDbl(tmp(0)) * 60 * 60)
            End If
            If Convert.ToInt64(tmp(1)) > 0 Then
                TimeSec += (CDbl(tmp(1)) * 60)
            End If
            ret = TimeSec + CDbl(tmp(2))
        End If
        Return ret
    End Function

    Private Sub VDO_ClickEvent(sender As Object, e As _WMPOCXEvents_ClickEvent) Handles VDO.ClickEvent
        Me.Close()
    End Sub

    Private Sub frmVDO_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        TimerSetTime.Stop()
        frmHomeVertical.HomeCurrentScreenSaverTime.Text = lblCurrentTime.Text
        frmHomeVertical.HomeCurrentScreenSaverFile.Text = lblCurrentFile.Text

        InsertLogSalesActivity(vTrans.TransNo, "กลับหน้า Home ไฟล์ที่เล่นค้าง " & lblCurrentFile.Text & " เวลา " & lblCurrentTime.Text, VendingConfig.SelectForm, False)

        Me.DialogResult = DialogResult.Yes
    End Sub

    Private Sub frmScreenServer_MouseClick(sender As Object, e As MouseEventArgs) Handles Me.MouseClick
        Me.Close()
    End Sub

    Private Sub TimerSetTime_Tick(sender As Object, e As EventArgs) Handles TimerSetTime.Tick
        lblCurrentTime.Text = VDO.Ctlcontrols.currentPositionString
        lblCurrentFile.Text = VDO.Ctlcontrols.currentItem.sourceURL
    End Sub

    Private Sub TimerSync_Tick(sender As Object, e As EventArgs) Handles TimerSync.Tick
        TimerSync.Enabled = False
        Try

        Catch ex As Exception : End Try
    End Sub


#Region "Download Screen Saver"
    Private Sub CheckVendingScreenSaver()
        Try
            'Dim ws As New ServerDataProvider.ServerDataProvider
            'ws.Url = My.Settings.AIS_Kiosk_ServerDataProvider_ServerDataProvider
            'ws.Timeout = 120000
            'Dim dt As DataTable = ws.CheckNewScreenSaver(Kiosk.VendingID)  'ตรวจสอบว่ามีไฟล์ VDO มาใหม่
            'If dt.Rows.Count > 0 Then
            '    For Each dr As DataRow In dt.Rows
            '        Dim TbKioskScreenSaverID As Long = Convert.ToInt64(dr("id"))

            '        Dim ini As New IniReader(INIFileName)
            '        ini.Section = "Setting"

            '        Dim PathNAS As String = ini.ReadString("PathNAS").ToString()
            '        If PathNAS = "" Then
            '            PathNAS = "D:\TempScreenSaverPath\"
            '        End If
            '        ini = Nothing

            '        Dim FileInNAS = PathNAS & TbKioskScreenSaverID & ".avi"

            '        If File.Exists(FileInNAS) = True Then

            '            'ทำการลบไฟล์เก่า (VDO.avi) แล้วแทนที่ด้วยไฟล์ใหม่
            '            Dim VdoFile As String = Application.StartupPath & "\VDO.avi"
            '            If File.Exists(VdoFile) = True Then
            '                Try
            '                    File.SetAttributes(VdoFile, FileAttributes.Normal)
            '                    File.Delete(VdoFile)
            '                Catch ex As Exception

            '                End Try
            '            End If

            '            File.Move(FileInNAS, VdoFile)

            '            ws.Update_VDO_Content_Status(TbKioskScreenSaverID) 'อัพเดท Status เมื่อ Download เสร็จ
            '        End If
            '    Next
            'End If
            'dt.Dispose()
            'ws.Dispose()
        Catch ex As Exception

        End Try
    End Sub


#End Region

End Class