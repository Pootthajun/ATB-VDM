﻿Imports System.Drawing.Printing
Imports VendingLinqDB.ConnectDB
Imports VendingLinqDB.TABLE
Public Class ucSetupSlip

#Region "User Control Property"

#End Region


    Private Sub Panel1_MouseMove(sender As Object, e As MouseEventArgs) Handles Panel1.MouseMove
        lblLocationX.Text = e.Location.X
        lblLocationY.Text = e.Location.Y
    End Sub

#Region "Add Textbox"
    Private Sub mnuAddText_Click(sender As Object, e As EventArgs) Handles mnuAddCaption.Click
        AddTextbox(50, New Point(lblLocationX.Text, lblLocationY.Text), "", New Font("Microsoft Sans Serif", 8, FontStyle.Regular), HorizontalAlignment.Left)
    End Sub

    Private Sub AddTextbox(TextWidth As Integer, TextPoint As Point, TextData As String, TextFont As Font, TextAlign As HorizontalAlignment)
        Dim txt As New TextBox
        txt.Name = "txtBox1"
        txt.Width = TextWidth 'Panel1.Width
        txt.BorderStyle = BorderStyle.FixedSingle
        txt.BackColor = Color.FromName("Control")
        txt.AllowDrop = True
        txt.Location = TextPoint
        txt.TextAlign = TextAlign
        txt.Text = TextData
        txt.Font = TextFont
        AddHandler txt.MouseDown, AddressOf TextBox_MouseDown
        AddHandler txt.MouseMove, AddressOf TextBox_MouseMove
        AddHandler txt.MouseUp, AddressOf TextBox_MouseUp
        AddHandler txt.KeyUp, AddressOf TextBox_KeyUp
        AddHandler txt.MouseClick, AddressOf TextBox_MouseClick
        AddHandler txt.DragEnter, AddressOf TextBox_DragEnter
        AddHandler txt.DragOver, AddressOf TextBox_DragOver
        AddHandler txt.DragDrop, AddressOf TextBox_DragDrop

        Panel1.Controls.Add(txt)
    End Sub

    Dim CurrTextbox As TextBox
    Private TextMouseIsDown As Boolean = False
    Dim InnerTextPosX As Integer = 0
    Dim InnerTextPosY As Integer = 0
    Private Sub TextBox_MouseDown(sender As Object, e As MouseEventArgs)
        Dim txt As TextBox = DirectCast(sender, TextBox)
        txt.BackColor = Color.FromName("ControlLightLight")

        If e.Button = MouseButtons.Right Then
            txt.ContextMenuStrip = cmtTextMenuStrip
            CurrTextbox = txt
        ElseIf e.Button = MouseButtons.Left Then
            TextMouseIsDown = True
            InnerTextPosX = e.Location.X
            InnerTextPosY = e.Location.Y

            If InnerTextPosX >= txt.Width - 5 And InnerTextPosX <= txt.Width - 1 Then
                txt.Cursor = Cursors.VSplit
            Else
                txt.Cursor = Cursors.Hand
            End If
        End If
    End Sub



    Private Sub TextBox_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim txt As TextBox = sender

        lblLocationX.Text = e.Location.X + txt.Location.X
        lblLocationY.Text = e.Location.Y + txt.Location.Y

        lblPosInnerX.Text = e.Location.X
        lblPosInnerY.Text = e.Location.Y

        If TextMouseIsDown = True Then
            If txt.Cursor = Cursors.VSplit Then
                txt.Width = Convert.ToInt16(lblLocationX.Text) - txt.Location.X
            Else
                txt.Location = New Point(Convert.ToInt16(lblLocationX.Text) - InnerTextPosX, Convert.ToInt16(lblLocationY.Text) - InnerTextPosY)
            End If

            Application.DoEvents()
        End If
    End Sub

    Private Sub TextBox_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        TextMouseIsDown = False

        Dim txt As TextBox = sender
        txt.BackColor = Color.FromName("Control")
        txt.Cursor = Cursors.Default
    End Sub

    Private Sub TextBox_KeyUp(sender As Object, e As KeyEventArgs)
        Dim txt As TextBox = DirectCast(sender, TextBox)
        If e.KeyData = Keys.Left Then
            If _SelectText <> "" Then
                txt.Select(txt.SelectionStart - _SelectText.Length + 1, 0)
                _SelectText = ""
            Else
                HighLighParam(txt)
            End If
        Else
            HighLighParam(txt)
        End If

    End Sub

    Private Sub TextBox_MouseClick(sender As Object, e As MouseEventArgs)
        Dim txt As TextBox = DirectCast(sender, TextBox)
        HighLighParam(txt)
    End Sub

    Dim _SelectText As String = ""

    Private Sub HighLighParam(txtBox As TextBox)
        If txtBox.SelectionStart = txtBox.Text.Length Then Exit Sub

        Dim StartParm As Integer = -1
        Dim EndParm As Integer = -1
        For i As Integer = txtBox.SelectionStart - 1 To 0 Step -1
            Dim sLeft As String = "@{"

            If txtBox.Text.Substring(i, 1) = "}" Then
                Exit For
            End If

            If txtBox.Text.Substring(i, sLeft.Length) = sLeft Then
                StartParm = i
                Exit For
            End If
        Next

        For i As Integer = txtBox.SelectionStart To txtBox.Text.Length - 1
            Dim sRight As String = "}"
            If i + 2 < txtBox.Text.Length Then
                If txtBox.Text.Substring(i, 2) = "@{" Then
                    Exit For
                End If
            End If

            If txtBox.Text.Substring(i, sRight.Length) = sRight Then
                EndParm = i
                Exit For
            End If
        Next

        Dim ret As Boolean = False
        If StartParm > -1 And EndParm > -1 Then
            ret = True

            txtBox.Select(StartParm, (EndParm - StartParm) + 1)
            _SelectText = txtBox.SelectedText
        Else
            _SelectText = ""
        End If
    End Sub

#End Region

#Region "Add Picturebox"
    Private Sub mnuAddImage_Click(sender As Object, e As EventArgs) Handles mnuAddImage.Click
        Dim fds As New OpenFileDialog
        fds.Filter = "Image Files(*.PNG;*.BMP;*.JPG;*.GIF)|*.PNG;*.BMP;*.JPG;*.GIF|All files (*.*)|*.*"

        If fds.ShowDialog = DialogResult.OK Then
            Dim img As Image = Image.FromFile(fds.FileName)
            AddPicturebox(img, New Point(lblLocationX.Text, lblLocationY.Text))

            'Dim pb As New PictureBox
            'pb.Name = "PicktureBox1"
            'pb.Width = img.Width
            'pb.Height = img.Height
            'pb.Image = img
            'pb.Location = New Point(lblLocationX.Text, lblLocationY.Text)

            'AddHandler pb.MouseDown, AddressOf PictureBox_MouseDown
            'AddHandler pb.MouseMove, AddressOf PictureBox_MouseMove
            'AddHandler pb.MouseUp, AddressOf PictureBox_MouseUp

            'Panel1.Controls.Add(pb)
        End If
    End Sub

    Private Sub AddPicturebox(img As Image, imgPoint As Point)

        Dim pb As New PictureBox
        pb.Name = "PicktureBox1"
        pb.Width = img.Width
        pb.Height = img.Height
        pb.Image = img
        pb.Location = imgPoint

        AddHandler pb.MouseDown, AddressOf PictureBox_MouseDown
        AddHandler pb.MouseMove, AddressOf PictureBox_MouseMove
        AddHandler pb.MouseUp, AddressOf PictureBox_MouseUp

        Panel1.Controls.Add(pb)
    End Sub

    Dim CurrPicturebox As PictureBox
    Private PicMouseIsDown As Boolean = False
    Dim InnerPicPosX As Integer = 0
    Dim InnerPicPosY As Integer = 0
    Private Sub PictureBox_MouseDown(sender As Object, e As MouseEventArgs)
        Dim pic As PictureBox = DirectCast(sender, PictureBox)

        If e.Button = MouseButtons.Right Then
            CurrPicturebox = pic
            pic.ContextMenuStrip = cmtPicMenuStrip
        ElseIf e.Button = MouseButtons.Left Then
            PicMouseIsDown = True
            InnerPicPosX = e.Location.X
            InnerPicPosY = e.Location.Y
            pic.Cursor = Cursors.Hand
        End If
    End Sub

    Private Sub PictureBox_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim pic As PictureBox = sender

        lblLocationX.Text = e.Location.X + pic.Location.X
        lblLocationY.Text = e.Location.Y + pic.Location.Y

        lblPosInnerX.Text = e.Location.X
        lblPosInnerY.Text = e.Location.Y

        If PicMouseIsDown = True Then
            pic.Location = New Point(Convert.ToInt16(lblLocationX.Text) - InnerPicPosX, Convert.ToInt16(lblLocationY.Text) - InnerPicPosY)
            Application.DoEvents()
        End If
    End Sub

    Private Sub PictureBox_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        PicMouseIsDown = False

        Dim pic As PictureBox = sender
        pic.Cursor = Cursors.Default
    End Sub


#End Region

#Region "Menu Event"
    Private Sub mnuTextAlignLeft_Click(sender As Object, e As EventArgs) Handles mnuTextAlignLeft.Click
        CurrTextbox.TextAlign = HorizontalAlignment.Left
    End Sub

    Private Sub mnuTextAlignCenter_Click(sender As Object, e As EventArgs) Handles mnuTextAlignCenter.Click
        CurrTextbox.TextAlign = HorizontalAlignment.Center
    End Sub

    Private Sub mnuTextAlignRight_Click(sender As Object, e As EventArgs) Handles mnuTextAlignRight.Click
        CurrTextbox.TextAlign = HorizontalAlignment.Right
    End Sub

    Private Sub mnuBringToFront_Click(sender As Object, e As EventArgs) Handles mnuBringToFront.Click
        CurrTextbox.BringToFront()
    End Sub

    Private Sub mnuSendToBack_Click(sender As Object, e As EventArgs) Handles mnuSendToBack.Click
        CurrTextbox.SendToBack()
    End Sub

    Private Sub mnuFont_Click(sender As Object, e As EventArgs) Handles mnuFont.Click
        If fntDialog.ShowDialog = DialogResult.OK Then
            CurrTextbox.Font = fntDialog.Font

        End If
    End Sub

    Private Sub mnuRemoveText_Click(sender As Object, e As EventArgs) Handles mnuRemoveText.Click
        Panel1.Controls.Remove(CurrTextbox)
    End Sub

    Private Sub mnuRemovePictureBox_Click(sender As Object, e As EventArgs) Handles mnuRemovePictureBox.Click
        Panel1.Controls.Remove(CurrPicturebox)
    End Sub

#End Region

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Panel1.Controls.Count > 0 Then
            Dim trans As New VendingTransactionDB

            Dim sql As String = "truncate table CF_SALE_SLIP"
            VendingDB.ExecuteNonQuery(sql, trans.Trans)

            Dim ret As Boolean = False
            For Each ctl As Control In Panel1.Controls
                Dim lnq As New CfSaleSlipVendingLinqDB
                lnq.CTL_LOCATION_X = ctl.Location.X
                lnq.CTL_LOCATION_Y = ctl.Location.Y

                'ตรวจสอบประเภทของ Control ที่อยู่ใน Panel1
                If ctl.GetType Is GetType(TextBox) Then
                    Dim txt As TextBox = DirectCast(ctl, TextBox)

                    lnq.CTL_TYPE = "1"
                    lnq.CTL_TEXT = txt.Text
                    lnq.CTL_TEXT_WIDTH = txt.Width
                    lnq.CTL_TEXT_ALIGN = Convert.ToInt16(txt.TextAlign).ToString
                    lnq.CTL_TEXT_FONTNAME = txt.Font.Name
                    lnq.CTL_TEXT_FONTSIZE = txt.Font.Size
                    lnq.CTL_TEXT_FONTSTYLE = Convert.ToInt16(txt.Font.Style).ToString
                ElseIf ctl.GetType Is GetType(PictureBox) Then
                    Dim pb As PictureBox = DirectCast(ctl, PictureBox)

                    lnq.CTL_TYPE = "2"
                    Dim convt As New ImageConverter
                    lnq.CTL_IMAGE = convt.ConvertTo(pb.Image, GetType(Byte()))
                End If

                Dim re As ExecuteDataInfo = lnq.InsertData(StaffConsole.Username, trans.Trans)
                ret = re.IsSuccess
                If ret = False Then
                    Exit For
                End If
            Next

            If ret = True Then
                trans.CommitTransaction()
                ShowDialogErrorMessageSC("Save Succesfull")
            Else
                trans.RollbackTransaction()
            End If
        End If
    End Sub


    Public Sub ucSetupSlipLoad()
        SetMasterFieldList()
        LoadSlipLayout()
        LoadPrinterList()
        GetPrintConfig()
        SetSetupSlipAuthorize()
    End Sub

    Private Sub SetSetupSlipAuthorize()
        If StaffConsole.AuthorizeInfo.Rows.Count > 0 Then
            AppScreenList.DefaultView.RowFilter = "id='" & Convert.ToInt16(Data.VendingConfigData.VendingForm.StaffConsoleSetupSlip) & "'"
            If AppScreenList.DefaultView.Count > 0 Then
                Panel1.Enabled = False
                dgvFieldList.Enabled = False
                dgvFieldList.ClearSelection()
                btnSave.Visible = False

                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & Data.ConstantsData.StaffConsoleFunctionalID.SetupSlip & " and authorization_name='Edit'"
                If StaffConsole.AuthorizeInfo.DefaultView.Count > 0 Then
                    Panel1.Enabled = True
                    dgvFieldList.Enabled = True
                    btnSave.Visible = True
                End If
                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""
            End If
            AppScreenList.DefaultView.RowFilter = ""
        End If
        Application.DoEvents()
    End Sub

    Private Sub SetMasterFieldList()
        dgvFieldList.AutoGenerateColumns = False
        dgvFieldList.DataSource = PrintFld
    End Sub

    Private Sub GetPrintConfig()
        Dim lnq As New CfSaleSlipVendingLinqDB
        PrintCF = lnq.GetDataList("", "", Nothing, Nothing)
        lnq = Nothing
    End Sub

    Private Sub LoadPrinterList()
        Dim pkInstalledPrinters As String = ""
        cbbPrinter.Items.Add("")
        For i = 0 To PrinterSettings.InstalledPrinters.Count - 1
            pkInstalledPrinters = PrinterSettings.InstalledPrinters.Item(i)
            cbbPrinter.Items.Add(pkInstalledPrinters)
        Next
        cbbPrinter.SelectedIndex = cbbPrinter.FindString(VendingConfig.PrinterDeviceName)
    End Sub


    Private Sub LoadSlipLayout()
        Try
            Panel1.Controls.Clear()

            Dim lnq As New CfSaleSlipVendingLinqDB
            Dim dt As DataTable = lnq.GetDataList("", "", Nothing, Nothing)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows

                    lnq = New CfSaleSlipVendingLinqDB
                    lnq.GetDataByPK(dr("id"), Nothing)
                    If lnq.ID > 0 Then
                        If lnq.CTL_TYPE = "1" Then  'Textbox
                            Dim fntSize As Single = lnq.CTL_TEXT_FONTSIZE.ToString
                            Dim fntStyle As FontStyle = lnq.CTL_TEXT_FONTSTYLE.ToString

                            AddTextbox(lnq.CTL_TEXT_WIDTH, New Point(lnq.CTL_LOCATION_X, lnq.CTL_LOCATION_Y), lnq.CTL_TEXT, New Font(lnq.CTL_TEXT_FONTNAME, fntSize, fntStyle), Convert.ToInt16(lnq.CTL_TEXT_ALIGN.ToString))

                        ElseIf lnq.CTL_TYPE = "2" Then  'Picturebox

                            Using pBytes As New IO.MemoryStream(lnq.CTL_IMAGE)
                                AddPicturebox(Image.FromStream(pBytes), New Point(lnq.CTL_LOCATION_X, lnq.CTL_LOCATION_Y))
                            End Using
                        End If
                    End If

                    Application.DoEvents()
                Next
            End If
            lnq = Nothing
            dt.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnPrintTest_Click(sender As Object, e As EventArgs) Handles btnPrintTest.Click
        Dim lnq As New CfSaleSlipVendingLinqDB
        PrintCF = lnq.GetDataList("", "", Nothing, Nothing)
        If PrintCF.Rows.Count > 0 Then
            'VendingData.VendingID = 1
            Dim ws As New VDMWebService.ATBVendingWebservice
            ws.Timeout = 1000
            GetMasterProductInfo(ws)
            ws.Dispose()

            Dim p As New PrintDocument
            p.PrintController = New Printing.StandardPrintController
            p.PrinterSettings.PrinterName = cbbPrinter.Text

            Dim mgn As New Margins(0, 0, 0, 0)
            p.DefaultPageSettings.Margins = mgn
            'p.DefaultPageSettings.PaperSize = New PaperSize("Slip", 800, 1300)


            AddHandler p.PrintPage, AddressOf p_PrintPage
            p.Print()
        End If
        'PrintDt.Dispose()
    End Sub

    Private Function SetSaleTrans() As Data.SalesTransactionData
        Dim vDate As DateTime = DateTime.Now

        Dim ProductID As Long = 1
        Dim ProductCode As String = ""
        Dim ProductNameTh As String = ""
        Dim ProductNameEn As String = ""
        Dim ProductCategoryID As Long = 0
        Dim CategoryCode As String = ""
        Dim CategoryName As String = ""
        Dim NetPrice As Double = 0

        If ProductMasterList.Rows.Count > 0 Then
            ProductMasterList.DefaultView.RowFilter = "ms_product_id=" & ProductID
            If ProductMasterList.DefaultView.Count > 0 Then
                ProductCode = ProductMasterList.DefaultView(0)("product_code")
                ProductNameTh = ProductMasterList.DefaultView(0)("product_name_th")
                ProductNameEn = ProductMasterList.DefaultView(0)("product_name_en")
                ProductCategoryID = ProductMasterList.DefaultView(0)("ms_product_category_id")
                CategoryCode = ProductMasterList.DefaultView(0)("category_code")
                CategoryName = ProductMasterList.DefaultView(0)("category_name")
                NetPrice = ProductMasterList.DefaultView(0)("price")
            End If
            ProductMasterList.DefaultView.RowFilter = ""
        End If


        Dim t As New Data.SalesTransactionData(VendingData.VendingID)
        t.TransactionID = 1
        t.TransNo = vDate.ToString("yyyyMMddHHmmss")
        t.TransStartTime = vDate
        t.ProductCategoryID = ProductCategoryID
        t.ProductID = ProductID
        t.ProductCode = ProductCode
        t.NetPrice = NetPrice
        t.PaidAmount = 1000
        t.ChangeAmount = t.PaidAmount - NetPrice

        Return t

    End Function


    Private Sub p_PrintPage(sender As System.Object, e As System.Drawing.Printing.PrintPageEventArgs)
        Try
            If PrintCF.Rows.Count > 0 Then
                'Sample Transaction For Test
                Dim SaleTrans As Data.SalesTransactionData = SetSaleTrans()

                Dim ProductCode As String = ""
                Dim ProductNameTh As String = ""
                Dim ProductNameEn As String = ""
                Dim ProductCategoryID As Long = 0
                Dim CategoryCode As String = ""
                Dim CategoryName As String = ""

                If ProductMasterList.Rows.Count > 0 Then
                    ProductMasterList.DefaultView.RowFilter = "ms_product_id=" & SaleTrans.ProductID
                    If ProductMasterList.DefaultView.Count > 0 Then
                        ProductCode = ProductMasterList.DefaultView(0)("product_code")
                        ProductNameTh = ProductMasterList.DefaultView(0)("product_name_th")
                        ProductNameEn = ProductMasterList.DefaultView(0)("product_name_en")
                        ProductCategoryID = SaleTrans.ProductCategoryID

                        CategoryCode = ProductMasterList.DefaultView(0)("category_code")
                        CategoryName = ProductMasterList.DefaultView(0)("category_name")
                    End If
                    ProductMasterList.DefaultView.RowFilter = ""
                End If

                Dim pt As New Printer.PrinterClass

                For Each dr As DataRow In PrintCF.Rows
                    If dr("CTL_TYPE") = "1" Then  'Textbox
                        Dim fntSize As Single = dr("CTL_TEXT_FONTSIZE").ToString
                        Dim fntStyle As FontStyle = dr("CTL_TEXT_FONTSTYLE").ToString

                        Dim TextData As String = SetSlipParameter(dr("CTL_TEXT"), PrintFld, SaleTrans, ProductCategoryID, ProductNameTh, ProductNameEn, ProductCategoryID, CategoryCode, CategoryName)
                        Dim fnt As New Font(dr("CTL_TEXT_FONTNAME").ToString, fntSize, fntStyle)
                        pt.PrintText(TextData, fnt, dr("CTL_TEXT_ALIGN").ToString, dr("CTL_TEXT_WIDTH"), New Point(dr("CTL_LOCATION_X"), dr("CTL_LOCATION_Y")), e)
                    ElseIf dr("CTL_TYPE") = "2" Then  'Picturebox
                        Using pBytes As New IO.MemoryStream(DirectCast(dr("CTL_IMAGE"), Byte()))
                            pt.PrintImage(Image.FromStream(pBytes), New Point(dr("CTL_LOCATION_X"), dr("CTL_LOCATION_Y")), e)
                        End Using
                    End If
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

#Region "Drag and Drop Event"
    Private Const EM_CHARFROMPOS As Int32 = &HD7
    Private Structure POINTAPI
        Public X As Integer
        Public Y As Integer
    End Structure

    Private Declare Function SendMessageLong Lib "user32" Alias "SendMessageA" (ByVal hWnd As IntPtr, ByVal wMsg As Int32, ByVal wParam As Int32, ByVal lParam As Int32) As Long

    ' Return the character position under the mouse.
    Public Function TextBoxCursorPos(ByVal txt As TextBox, ByVal X As Single, ByVal Y As Single) As Long
        ' Convert screen coordinates into control coordinates.
        Dim pt As Point = txt.PointToClient(New Point(X, Y))
        ' Get the character number
        Return SendMessageLong(txt.Handle, EM_CHARFROMPOS, 0&, CLng(pt.X + pt.Y * &H10000)) And &HFFFF&
    End Function

    Private Sub dgvFieldList_MouseDown(sender As Object, e As MouseEventArgs) Handles dgvFieldList.MouseDown
        If e.Button = MouseButtons.Left Then
            Dim info As DataGridView.HitTestInfo = dgvFieldList.HitTest(e.X, e.Y)
            If info.RowIndex > -1 Then
                Dim row As DataGridViewRow = dgvFieldList.Rows(info.RowIndex)
                dgvFieldList.DoDragDrop(row.Cells("colParmName").Value, DragDropEffects.Copy)
            End If
        End If
    End Sub

    Private Sub TextBox_DragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.StringFormat) Then
            e.Effect = DragDropEffects.Copy
        End If
    End Sub
    Private Sub TextBox_DragOver(sender As Object, e As DragEventArgs)
        Dim txt As TextBox = DirectCast(sender, TextBox)
        If e.Data.GetDataPresent(DataFormats.StringFormat) Then
            ' Optionally move the cursor position so
            ' the user can see where the drop would happen.
            txt.Select(TextBoxCursorPos(txt, e.X, e.Y), 0)
            Application.DoEvents()
        End If
    End Sub

    Private Sub TextBox_DragDrop(sender As Object, e As DragEventArgs)
        Dim parmName As String = DirectCast(e.Data.GetData(DataFormats.StringFormat), String)
        Dim txt As TextBox = DirectCast(sender, TextBox)
        txt.SelectedText = parmName
    End Sub
#End Region


End Class
