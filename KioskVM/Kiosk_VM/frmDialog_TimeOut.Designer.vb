﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDialog_TimeOut
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TimerCount = New System.Windows.Forms.Timer(Me.components)
        Me.lblCount = New System.Windows.Forms.Label()
        Me.lblCaptionMessage = New System.Windows.Forms.Label()
        Me.lblCaptionSec = New System.Windows.Forms.Label()
        Me.pnlDialog = New System.Windows.Forms.Panel()
        Me.lblYes = New System.Windows.Forms.Label()
        Me.pnlDialog.SuspendLayout()
        Me.SuspendLayout()
        '
        'TimerCount
        '
        Me.TimerCount.Enabled = True
        Me.TimerCount.Interval = 1000
        '
        'lblCount
        '
        Me.lblCount.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 200.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCount.ForeColor = System.Drawing.Color.White
        Me.lblCount.Location = New System.Drawing.Point(3, 215)
        Me.lblCount.Name = "lblCount"
        Me.lblCount.Size = New System.Drawing.Size(625, 275)
        Me.lblCount.TabIndex = 41
        Me.lblCount.Text = "00"
        Me.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionMessage
        '
        Me.lblCaptionMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 39.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionMessage.ForeColor = System.Drawing.Color.White
        Me.lblCaptionMessage.Location = New System.Drawing.Point(7, 131)
        Me.lblCaptionMessage.Name = "lblCaptionMessage"
        Me.lblCaptionMessage.Size = New System.Drawing.Size(650, 62)
        Me.lblCaptionMessage.TabIndex = 33
        Me.lblCaptionMessage.Text = "ต้องการทำรายการต่อหรือไม่ ?"
        '
        'lblCaptionSec
        '
        Me.lblCaptionSec.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblCaptionSec.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionSec.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionSec.ForeColor = System.Drawing.Color.White
        Me.lblCaptionSec.Location = New System.Drawing.Point(456, 446)
        Me.lblCaptionSec.Name = "lblCaptionSec"
        Me.lblCaptionSec.Size = New System.Drawing.Size(124, 44)
        Me.lblCaptionSec.TabIndex = 43
        Me.lblCaptionSec.Text = "SEC."
        Me.lblCaptionSec.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlDialog
        '
        Me.pnlDialog.BackColor = System.Drawing.Color.Transparent
        Me.pnlDialog.Controls.Add(Me.lblYes)
        Me.pnlDialog.Controls.Add(Me.lblCaptionSec)
        Me.pnlDialog.Controls.Add(Me.lblCaptionMessage)
        Me.pnlDialog.Controls.Add(Me.lblCount)
        Me.pnlDialog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlDialog.Location = New System.Drawing.Point(0, 0)
        Me.pnlDialog.Name = "pnlDialog"
        Me.pnlDialog.Size = New System.Drawing.Size(665, 726)
        Me.pnlDialog.TabIndex = 2
        '
        'lblYes
        '
        Me.lblYes.BackColor = System.Drawing.Color.Transparent
        Me.lblYes.Font = New System.Drawing.Font("Microsoft Sans Serif", 48.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblYes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblYes.Location = New System.Drawing.Point(189, 579)
        Me.lblYes.Name = "lblYes"
        Me.lblYes.Size = New System.Drawing.Size(296, 93)
        Me.lblYes.TabIndex = 39
        Me.lblYes.Text = "ตกลง"
        Me.lblYes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmDialog_TimeOut
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(665, 726)
        Me.Controls.Add(Me.pnlDialog)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmDialog_TimeOut"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmDialog_TimeOut"
        Me.pnlDialog.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TimerCount As Timer
    Friend WithEvents lblCount As Label
    Friend WithEvents lblCaptionMessage As Label
    Friend WithEvents lblCaptionSec As Label
    Friend WithEvents pnlDialog As Panel
    Friend WithEvents lblYes As Label
End Class
