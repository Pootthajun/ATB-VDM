﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucProductShelf
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblProductCode = New System.Windows.Forms.Label()
        Me.lblPosition = New System.Windows.Forms.Label()
        Me.lblProductQty = New System.Windows.Forms.Label()
        Me.lblProductID = New System.Windows.Forms.Label()
        Me.lblShelfID = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblProductCode
        '
        Me.lblProductCode.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblProductCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProductCode.Location = New System.Drawing.Point(0, 7)
        Me.lblProductCode.Name = "lblProductCode"
        Me.lblProductCode.Size = New System.Drawing.Size(50, 24)
        Me.lblProductCode.TabIndex = 0
        Me.lblProductCode.Text = "Product Code"
        Me.lblProductCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblPosition
        '
        Me.lblPosition.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPosition.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblPosition.Location = New System.Drawing.Point(0, 40)
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(50, 24)
        Me.lblPosition.TabIndex = 1
        Me.lblPosition.Text = "Position"
        Me.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblProductQty
        '
        Me.lblProductQty.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblProductQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProductQty.Location = New System.Drawing.Point(0, 74)
        Me.lblProductQty.Name = "lblProductQty"
        Me.lblProductQty.Size = New System.Drawing.Size(50, 24)
        Me.lblProductQty.TabIndex = 2
        Me.lblProductQty.Text = "Product Qty"
        Me.lblProductQty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblProductID
        '
        Me.lblProductID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProductID.Location = New System.Drawing.Point(1, 16)
        Me.lblProductID.Name = "lblProductID"
        Me.lblProductID.Size = New System.Drawing.Size(26, 24)
        Me.lblProductID.TabIndex = 3
        Me.lblProductID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblProductID.Visible = False
        '
        'lblShelfID
        '
        Me.lblShelfID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblShelfID.Location = New System.Drawing.Point(1, 40)
        Me.lblShelfID.Name = "lblShelfID"
        Me.lblShelfID.Size = New System.Drawing.Size(26, 24)
        Me.lblShelfID.TabIndex = 4
        Me.lblShelfID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblShelfID.Visible = False
        '
        'ucProductShelf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me.lblProductQty)
        Me.Controls.Add(Me.lblPosition)
        Me.Controls.Add(Me.lblProductCode)
        Me.Controls.Add(Me.lblProductID)
        Me.Controls.Add(Me.lblShelfID)
        Me.Name = "ucProductShelf"
        Me.Size = New System.Drawing.Size(50, 100)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblProductCode As Label
    Friend WithEvents lblPosition As Label
    Friend WithEvents lblProductQty As Label
    Friend WithEvents lblProductID As Label
    Friend WithEvents lblShelfID As Label
End Class
