﻿

Public Class frmProductWarrantyInfo
    Dim TimeOutCheckTime As DateTime = DateTime.Now
    Private Sub frmProductDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.ControlBox = False
        Me.BackColor = bgColor
        Me.BackgroundImage = Image.FromFile(Application.StartupPath & "\Background\bgWarrantyInfo.jpg")

    End Sub
    Private Sub frmProductDetail_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.ProductWarranty
        InsertLogSalesActivity(vTrans.TransNo, "แสดงหน้า Product Warranty ", VendingConfig.SelectForm, False)
        SetChildFormLanguage()
    End Sub


    Private Sub pbClose_Click(sender As Object, e As EventArgs) Handles pbClose.Click, btnClose.Click
        InsertLogSalesActivity(vTrans.TransNo, "คลิกปุ่มปิด", VendingConfig.SelectForm, False)
        Me.Close()
    End Sub

    Public Sub SetWarrantyInfo()
        ProductWarrantyList.DefaultView.RowFilter = "ms_product_id = '" & vTrans.ProductID & "'"
        If ProductWarrantyList.DefaultView.Count > 0 Then
            For Each pwDrv As DataRowView In ProductWarrantyList.DefaultView
                Select Case VendingConfig.Language
                    Case Data.ConstantsData.VendingLanguage.Thai
                        lblWarranty.Text += pwDrv("warranty_th") & Environment.NewLine + Environment.NewLine
                    Case Data.ConstantsData.VendingLanguage.English
                        lblWarranty.Text += pwDrv("warranty_en") & Environment.NewLine + Environment.NewLine
                    Case Data.ConstantsData.VendingLanguage.China
                        lblWarranty.Text += pwDrv("warranty_ch") & Environment.NewLine + Environment.NewLine
                    Case Data.ConstantsData.VendingLanguage.Japan
                        lblWarranty.Text += pwDrv("warranty_jp") & Environment.NewLine + Environment.NewLine
                End Select
            Next
        End If
        ProductWarrantyList.DefaultView.RowFilter = ""
    End Sub

    Private Sub TimerTimeout_Tick(sender As Object, e As EventArgs) Handles TimerTimeout.Tick
        If TimeOutCheckTime.AddSeconds(VendingConfig.TimeOutSec) <= DateTime.Now Then
            InsertLogSalesActivity(vTrans.TransNo, "ลูกค้าไม่ทำรายการภายในเวลาที่กำหนด (Time out)", VendingConfig.SelectForm, False)
            TimerTimeout.Enabled = False
            Application.DoEvents()

            vTrans.TransStatus = Data.SalesTransactionData.TransactionStatus.TimeOut
            UpdateSaleTransaction(vTrans)

            frmHomeVertical.Show()
            Me.Close()
            frmProductDetailVertical.Close()
        End If
    End Sub
End Class