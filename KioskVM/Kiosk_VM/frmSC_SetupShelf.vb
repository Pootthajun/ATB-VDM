﻿Imports System.Data
Imports System.Data.SqlClient
Imports ControllerPLC
Imports VendingLinqDB.ConnectDB
Imports VendingLinqDB.TABLE
Public Class frmSC_SetupShelf

    Private Sub frmSC_SetupShelf_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        SetSetupShelfAuthorize()
    End Sub


    Private Sub SetSetupShelfAuthorize()
        If StaffConsole.AuthorizeInfo.Rows.Count > 0 Then

            AppScreenList.DefaultView.RowFilter = "id='" & Convert.ToInt16(VendingConfig.SelectForm) & "'"
            If AppScreenList.DefaultView.Count > 0 Then
                numX.Enabled = False
                numY.Enabled = False
                numRow.Enabled = False
                numColumn.Enabled = False
                numWidth.Enabled = False
                chkActive.Enabled = False
                btnSave.Visible = False

                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & Data.ConstantsData.StaffConsoleFunctionalID.SetupShelf & " and authorization_name='Edit'"
                If StaffConsole.AuthorizeInfo.DefaultView.Count > 0 Then
                    numX.Enabled = True
                    numY.Enabled = True
                    numRow.Enabled = True
                    numColumn.Enabled = True
                    numWidth.Enabled = True
                    chkActive.Enabled = True
                    btnSave.Visible = True
                End If
                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""
            End If
            AppScreenList.DefaultView.RowFilter = ""
        End If
        Application.DoEvents()
    End Sub

    Public Sub FillInData(ShelfID As Integer)
        Try
            Dim lnq As New MsVendingShelfVendingLinqDB
            lnq.ChkDataByMS_VENDING_ID_SHELF_ID(VendingData.VendingID, ShelfID, Nothing)
            If lnq.ID > 0 Then
                lblID.Text = lnq.ID
                numX.Value = lnq.POSITION_X
                numY.Value = lnq.POSITION_Y
                numRow.Value = lnq.POSITION_ROW
                numColumn.Value = lnq.POSITION_COLUMN
                numWidth.Value = lnq.WIDTH
                chkActive.Checked = IIf(lnq.ACTIVE_STATUS = "Y", True, False)
            End If
            lnq = Nothing
        Catch ex As Exception

        End Try
    End Sub


    Private Sub lblMoveTo_Click(sender As Object, e As EventArgs) Handles lblMoveTo.Click, btnMoveTo.Click
        SteperMotorPLC.MoveStepMotor(numX.Value, numY.Value)
    End Sub

    Private Sub lblCarriageAction_Click(sender As Object, e As EventArgs) Handles lblCarriageAction.Click, btnCarriageAction.Click
        WorkFlowPLC.CarriageAction(numX.Value, numY.Value, 0, 0)
    End Sub

    Private Sub lblTakeItem_Click(sender As Object, e As EventArgs) Handles lblTakeItem.Click, btnTackItem.Click
        WorkFlowPLC.TakeItem()
    End Sub

    Private Sub lblToBuy_Click(sender As Object, e As EventArgs) Handles lblToBuy.Click, btnToBuy.Click
        WorkFlowPLC.ToBuy()
    End Sub

    Private Sub lblSave_Click(sender As Object, e As EventArgs) Handles lblSave.Click, btnSave.Click
        If numX.Value = 0 Then
            ShowDialogErrorMessageSC("กรุณาระบุตำแหน่ง X")
            numX.Focus()
            Exit Sub
        End If
        If numY.Value = 0 Then
            ShowDialogErrorMessageSC("กรุณาระบุตำแหน่ง Y")
            numY.Focus()
            Exit Sub
        End If
        If numRow.Value = 0 Then
            ShowDialogErrorMessageSC("กรุณาระบุแถว")
            numRow.Focus()
            Exit Sub
        End If
        If numColumn.Value = 0 Then
            ShowDialogErrorMessageSC("กรุณาระบุคอลัมน์")
            numColumn.Focus()
            Exit Sub
        End If
        If numWidth.Value = 0 Then
            ShowDialogErrorMessageSC("กรุณาระบุความกว้าง")
            numWidth.Focus()
            Exit Sub
        End If

        'ตรวจสอบ Row/Column ซ้ำ
        Dim sql As String = "select top 1 id"
        sql += " from MS_VENDING_SHELF "
        sql += " where position_row=@_POSITION_ROW"
        sql += " and position_column=@_POSITION_COLUMN"
        sql += " and id<>@_ID"
        Dim p(3) As SqlParameter
        p(0) = VendingDB.SetInt("@_POSITION_ROW", numRow.Value)
        p(1) = VendingDB.SetInt("@_POSITION_COLUMN", numColumn.Value)
        p(2) = VendingDB.SetInt("@_ID", lblID.Text)
        Dim dt As DataTable = VendingDB.ExecuteTable(sql, p)
        If dt.Rows.Count > 0 Then
            ShowDialogErrorMessageSC("Row และ Column ซ้ำ")
            numRow.Focus()
            dt.Dispose()
            Exit Sub
        End If


        'คำนวณความกว้างรวมของทั้ง Row จะต้องไม่เกิน VendingConfig.ShelfLong
        sql = "select isnull(sum(width),0) row_width "
        sql += " from MS_VENDING_SHELF "
        sql += " where position_row=@_POSITION_ROW "
        sql += " and ms_vending_id=@_VENDING_ID "
        sql += " and id<>@_ID"
        ReDim p(3)
        p(0) = VendingDB.SetInt("@_POSITION_ROW", numRow.Value)
        p(1) = VendingDB.SetBigInt("@_VENDING_ID", VendingData.VendingID)
        p(2) = VendingDB.SetBigInt("@_ID", lblID.Text)

        Dim RowWidth As Integer = numWidth.Value
        dt = VendingDB.ExecuteTable(sql, p)
        If dt.Rows.Count > 0 Then
            RowWidth += Convert.ToInt16(dt.Rows(0)("row_width"))
        End If
        dt.Dispose()

        If RowWidth > VendingConfig.ShelfLong Then
            ShowDialogErrorMessageSC("ความยาวของทุกคอลัมน์ในแถวเดียวกัน ยาวเกินความยาวสูงสุดของแถว")
            numWidth.Focus()
            Exit Sub
        End If


        Try
            Dim lnq As New MsVendingShelfVendingLinqDB
            lnq.GetDataByPK(lblID.Text, Nothing)
            lnq.MS_VENDING_ID = VendingData.VendingID
            lnq.POSITION_ROW = numRow.Value
            lnq.POSITION_COLUMN = numColumn.Value
            lnq.POSITION_X = numX.Value
            lnq.POSITION_Y = numY.Value
            lnq.WIDTH = numWidth.Value
            lnq.ACTIVE_STATUS = IIf(chkActive.Checked = True, "Y", "N")
            lnq.SYNC_TO_SERVER = "N"

            Dim trans As New VendingTransactionDB
            Dim ret As ExecuteDataInfo
            If lnq.ID > 0 Then
                ret = lnq.UpdateData(StaffConsole.Username, trans.Trans)
            Else
                lnq.SHELF_ID = GetNextShelfID(trans)
                ret = lnq.InsertData(StaffConsole.Username, trans.Trans)
            End If
            If ret.IsSuccess = True Then
                trans.CommitTransaction()
                'FillInData(lnq.SHELF_ID)
                ShowDialogErrorMessageSC("Save Success")
                Me.DialogResult = DialogResult.OK
                Me.Close()
            Else
                trans.RollbackTransaction()
                ShowDialogErrorMessageSC(ret.ErrorMessage)
            End If
            lnq = Nothing
        Catch ex As Exception
            ShowDialogErrorMessageSC("Exception : " & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub

    Private Function GetNextShelfID(trans As VendingTransactionDB) As Integer
        Dim ret As Integer = 0
        Try
            Dim sql As String = "select isnull(max(shelf_id),0) max_shelf"
            sql += " from MS_VENDING_SHELF "
            sql += " where ms_vending_id=@_VENDING_ID"

            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_VENDING_ID", VendingData.VendingID)

            Dim dt As DataTable = VendingDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then
                ret = Convert.ToInt16(dt.Rows(0)("max_shelf")) + 1
            End If
            dt.Dispose()
        Catch ex As Exception

        End Try

        Return ret
    End Function

    Private Sub lblCancel_Click(sender As Object, e As EventArgs) Handles lblCancel.Click, btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class