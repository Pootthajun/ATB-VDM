﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmProcessingVertical
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.pbBackground = New System.Windows.Forms.PictureBox()
        Me.pbIconProcessing = New System.Windows.Forms.PictureBox()
        Me.TimerTimeOut = New System.Windows.Forms.Timer(Me.components)
        Me.lblCaptionDelivered = New System.Windows.Forms.Label()
        Me.lblCaptionPayment = New System.Windows.Forms.Label()
        Me.lblCaptionSelectProduct = New System.Windows.Forms.Label()
        Me.lblCaptionHome = New System.Windows.Forms.Label()
        Me.lblCaptionMessage = New System.Windows.Forms.Label()
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbIconProcessing, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pbBackground
        '
        Me.pbBackground.BackColor = System.Drawing.Color.Transparent
        Me.pbBackground.Location = New System.Drawing.Point(0, 0)
        Me.pbBackground.Name = "pbBackground"
        Me.pbBackground.Size = New System.Drawing.Size(768, 1366)
        Me.pbBackground.TabIndex = 2
        Me.pbBackground.TabStop = False
        '
        'pbIconProcessing
        '
        Me.pbIconProcessing.BackColor = System.Drawing.Color.Transparent
        Me.pbIconProcessing.Image = Global.Kiosk_VM.My.Resources.Resources.IconProcessing
        Me.pbIconProcessing.Location = New System.Drawing.Point(134, 436)
        Me.pbIconProcessing.Name = "pbIconProcessing"
        Me.pbIconProcessing.Size = New System.Drawing.Size(500, 500)
        Me.pbIconProcessing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbIconProcessing.TabIndex = 3
        Me.pbIconProcessing.TabStop = False
        '
        'TimerTimeOut
        '
        Me.TimerTimeOut.Enabled = True
        Me.TimerTimeOut.Interval = 10000
        '
        'lblCaptionDelivered
        '
        Me.lblCaptionDelivered.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionDelivered.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionDelivered.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionDelivered.Location = New System.Drawing.Point(563, 40)
        Me.lblCaptionDelivered.Name = "lblCaptionDelivered"
        Me.lblCaptionDelivered.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionDelivered.TabIndex = 99
        Me.lblCaptionDelivered.Text = "รับสินค้า"
        Me.lblCaptionDelivered.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionPayment
        '
        Me.lblCaptionPayment.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionPayment.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionPayment.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionPayment.Location = New System.Drawing.Point(389, 40)
        Me.lblCaptionPayment.Name = "lblCaptionPayment"
        Me.lblCaptionPayment.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionPayment.TabIndex = 98
        Me.lblCaptionPayment.Text = "ชำระเงิน"
        Me.lblCaptionPayment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionSelectProduct
        '
        Me.lblCaptionSelectProduct.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionSelectProduct.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionSelectProduct.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionSelectProduct.Location = New System.Drawing.Point(216, 40)
        Me.lblCaptionSelectProduct.Name = "lblCaptionSelectProduct"
        Me.lblCaptionSelectProduct.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionSelectProduct.TabIndex = 97
        Me.lblCaptionSelectProduct.Text = "เลือกสินค้า"
        Me.lblCaptionSelectProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionHome
        '
        Me.lblCaptionHome.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionHome.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionHome.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionHome.Location = New System.Drawing.Point(44, 40)
        Me.lblCaptionHome.Name = "lblCaptionHome"
        Me.lblCaptionHome.Size = New System.Drawing.Size(146, 36)
        Me.lblCaptionHome.TabIndex = 96
        Me.lblCaptionHome.Text = "หน้าหลัก"
        Me.lblCaptionHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionMessage
        '
        Me.lblCaptionMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionMessage.ForeColor = System.Drawing.Color.White
        Me.lblCaptionMessage.Location = New System.Drawing.Point(78, 315)
        Me.lblCaptionMessage.Name = "lblCaptionMessage"
        Me.lblCaptionMessage.Size = New System.Drawing.Size(611, 92)
        Me.lblCaptionMessage.TabIndex = 100
        Me.lblCaptionMessage.Text = "ระบบกำลังทำรายการ" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "กรุณารอสักครู่"
        Me.lblCaptionMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmProcessingVertical
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(768, 1339)
        Me.Controls.Add(Me.lblCaptionMessage)
        Me.Controls.Add(Me.lblCaptionDelivered)
        Me.Controls.Add(Me.lblCaptionPayment)
        Me.Controls.Add(Me.lblCaptionSelectProduct)
        Me.Controls.Add(Me.lblCaptionHome)
        Me.Controls.Add(Me.pbIconProcessing)
        Me.Controls.Add(Me.pbBackground)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmProcessingVertical"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbIconProcessing, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pbBackground As PictureBox
    Friend WithEvents pbIconProcessing As PictureBox
    Friend WithEvents TimerTimeOut As Timer
    Friend WithEvents lblCaptionDelivered As Label
    Friend WithEvents lblCaptionPayment As Label
    Friend WithEvents lblCaptionSelectProduct As Label
    Friend WithEvents lblCaptionHome As Label
    Friend WithEvents lblCaptionMessage As Label
End Class
