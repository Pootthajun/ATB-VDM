﻿

Public Class frmProductDetailVertical

    Dim TimeOutCheckTime As DateTime = DateTime.Now
    Private Sub frmProductDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.ControlBox = False
        Me.BackColor = bgColor
        Me.BackgroundImage = Image.FromFile(Application.StartupPath & "\Background\bgProductDetail.jpg")
    End Sub
    Private Sub frmProductDetail_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Me.WindowState = FormWindowState.Maximized
        VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.ProductDetail
        InsertLogSalesActivity(vTrans.TransNo, "แสดงหน้า Product Detail ", VendingConfig.SelectForm, False)

        SetChildFormLanguage()

        TimerTimeOut.Enabled = True
    End Sub

    Private Sub BuyNow_Click(sender As Object, e As EventArgs) Handles lblBuyNow.Click
        Application.DoEvents()
        InsertLogSalesActivity(vTrans.TransNo, "คลิกปุ่มซื้อเลย", VendingConfig.SelectForm, False)
        frmPaymentVertical.SetProductInformation()
        frmPaymentVertical.StartPaymentInitialDevice()
        frmPaymentVertical.Show()
        Me.Close()
    End Sub


    Public Sub SetProductInfo()
        ProductMasterList.DefaultView.RowFilter = "ms_product_id = '" & vTrans.ProductID & "'"
        If ProductMasterList.DefaultView.Count > 0 Then
            Dim pdDrv As DataRowView = ProductMasterList.DefaultView(0)

            lblNetPrice.Text = vTrans.NetPrice
            Select Case VendingConfig.Language
                Case Data.ConstantsData.VendingLanguage.Thai
                    lblProductName.Text = pdDrv("product_name_th")
                    lblProductDesc.Text = pdDrv("product_desc_th")
                    lblNetPrice.Text += " บาท"
                Case Data.ConstantsData.VendingLanguage.English
                    lblProductName.Text = pdDrv("product_name_en")
                    lblProductDesc.Text = pdDrv("product_desc_en")
                    lblNetPrice.Text += THB_EN
                Case Data.ConstantsData.VendingLanguage.China
                    lblProductName.Text = pdDrv("product_name_ch")
                    lblProductDesc.Text = pdDrv("product_desc_ch")
                    lblNetPrice.Text += THB_CH
                Case Data.ConstantsData.VendingLanguage.Japan
                    lblProductName.Text = pdDrv("product_name_jp")
                    lblProductDesc.Text = pdDrv("product_desc_jp")
                    lblNetPrice.Text += THB_JP
            End Select
        End If
        ProductMasterList.DefaultView.RowFilter = ""
    End Sub

    Private Sub lblWarranty_Click(sender As Object, e As EventArgs) Handles lblWarranty.Click
        InsertLogSalesActivity(vTrans.TransNo, "คลิกปุ่มเงื่อนไขประกันสินค้า", VendingConfig.SelectForm, False)

        frmProductWarrantyInfo.SetWarrantyInfo()
        frmProductWarrantyInfo.ShowDialog(Me)
        VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.ProductDetail
    End Sub

    Private Sub TimerTimeOut_Tick(sender As Object, e As EventArgs) Handles TimerTimeOut.Tick
        If TimeOutCheckTime.AddSeconds(VendingConfig.TimeOutSec) <= DateTime.Now Then
            InsertLogSalesActivity(vTrans.TransNo, "ลูกค้าไม่ทำรายการภายในเวลาที่กำหนด (Time out)", VendingConfig.SelectForm, False)
            TimerTimeOut.Enabled = False
            Application.DoEvents()

            vTrans.TransStatus = Data.SalesTransactionData.TransactionStatus.TimeOut
            UpdateSaleTransaction(vTrans)

            frmHomeVertical.Show()
            Me.Close()
        End If
    End Sub
End Class