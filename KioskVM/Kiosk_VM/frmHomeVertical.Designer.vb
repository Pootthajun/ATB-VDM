﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmHomeVertical
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.pbBackground = New System.Windows.Forms.PictureBox()
        Me.pnlShelf = New System.Windows.Forms.Panel()
        Me.lblPointer = New System.Windows.Forms.Label()
        Me.TimerSetPointer = New System.Windows.Forms.Timer(Me.components)
        Me.HomeCurrentScreenSaverTime = New System.Windows.Forms.Label()
        Me.HomeCurrentScreenSaverFile = New System.Windows.Forms.Label()
        Me.TimerScreenSaver = New System.Windows.Forms.Timer(Me.components)
        Me.TimerCheckAutoSleep = New System.Windows.Forms.Timer(Me.components)
        Me.TimerCheckPromotion = New System.Windows.Forms.Timer(Me.components)
        Me.lblCaptionDelivered = New System.Windows.Forms.Label()
        Me.lblCaptionPayment = New System.Windows.Forms.Label()
        Me.lblCaptionSelectProduct = New System.Windows.Forms.Label()
        Me.lblCaptionHome = New System.Windows.Forms.Label()
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pbBackground
        '
        Me.pbBackground.BackColor = System.Drawing.Color.Transparent
        Me.pbBackground.Location = New System.Drawing.Point(0, 0)
        Me.pbBackground.Name = "pbBackground"
        Me.pbBackground.Size = New System.Drawing.Size(768, 1366)
        Me.pbBackground.TabIndex = 1
        Me.pbBackground.TabStop = False
        '
        'pnlShelf
        '
        Me.pnlShelf.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlShelf.BackColor = System.Drawing.Color.Silver
        Me.pnlShelf.Location = New System.Drawing.Point(40, 365)
        Me.pnlShelf.Name = "pnlShelf"
        Me.pnlShelf.Size = New System.Drawing.Size(508, 902)
        Me.pnlShelf.TabIndex = 2
        '
        'lblPointer
        '
        Me.lblPointer.BackColor = System.Drawing.Color.Transparent
        Me.lblPointer.Font = New System.Drawing.Font("Microsoft Sans Serif", 1.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblPointer.ForeColor = System.Drawing.Color.Transparent
        Me.lblPointer.Location = New System.Drawing.Point(0, 0)
        Me.lblPointer.Name = "lblPointer"
        Me.lblPointer.Size = New System.Drawing.Size(64, 64)
        Me.lblPointer.TabIndex = 3
        Me.lblPointer.Text = "1"
        '
        'TimerSetPointer
        '
        Me.TimerSetPointer.Interval = 2000
        '
        'HomeCurrentScreenSaverTime
        '
        Me.HomeCurrentScreenSaverTime.AutoSize = True
        Me.HomeCurrentScreenSaverTime.Location = New System.Drawing.Point(95, 182)
        Me.HomeCurrentScreenSaverTime.Name = "HomeCurrentScreenSaverTime"
        Me.HomeCurrentScreenSaverTime.Size = New System.Drawing.Size(0, 17)
        Me.HomeCurrentScreenSaverTime.TabIndex = 4
        Me.HomeCurrentScreenSaverTime.Visible = False
        '
        'HomeCurrentScreenSaverFile
        '
        Me.HomeCurrentScreenSaverFile.AutoSize = True
        Me.HomeCurrentScreenSaverFile.Location = New System.Drawing.Point(136, 247)
        Me.HomeCurrentScreenSaverFile.Name = "HomeCurrentScreenSaverFile"
        Me.HomeCurrentScreenSaverFile.Size = New System.Drawing.Size(0, 17)
        Me.HomeCurrentScreenSaverFile.TabIndex = 5
        Me.HomeCurrentScreenSaverFile.Visible = False
        '
        'TimerScreenSaver
        '
        Me.TimerScreenSaver.Enabled = True
        Me.TimerScreenSaver.Interval = 1000
        '
        'TimerCheckAutoSleep
        '
        Me.TimerCheckAutoSleep.Enabled = True
        Me.TimerCheckAutoSleep.Interval = 60000
        '
        'TimerCheckPromotion
        '
        Me.TimerCheckPromotion.Enabled = True
        Me.TimerCheckPromotion.Interval = 60000
        '
        'lblCaptionDelivered
        '
        Me.lblCaptionDelivered.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionDelivered.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionDelivered.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionDelivered.Location = New System.Drawing.Point(563, 40)
        Me.lblCaptionDelivered.Name = "lblCaptionDelivered"
        Me.lblCaptionDelivered.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionDelivered.TabIndex = 22
        Me.lblCaptionDelivered.Text = "รับสินค้า"
        Me.lblCaptionDelivered.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionPayment
        '
        Me.lblCaptionPayment.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionPayment.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionPayment.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionPayment.Location = New System.Drawing.Point(389, 40)
        Me.lblCaptionPayment.Name = "lblCaptionPayment"
        Me.lblCaptionPayment.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionPayment.TabIndex = 21
        Me.lblCaptionPayment.Text = "ชำระเงิน"
        Me.lblCaptionPayment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionSelectProduct
        '
        Me.lblCaptionSelectProduct.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionSelectProduct.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionSelectProduct.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionSelectProduct.Location = New System.Drawing.Point(216, 40)
        Me.lblCaptionSelectProduct.Name = "lblCaptionSelectProduct"
        Me.lblCaptionSelectProduct.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionSelectProduct.TabIndex = 20
        Me.lblCaptionSelectProduct.Text = "เลือกสินค้า"
        Me.lblCaptionSelectProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionHome
        '
        Me.lblCaptionHome.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionHome.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionHome.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionHome.Location = New System.Drawing.Point(44, 40)
        Me.lblCaptionHome.Name = "lblCaptionHome"
        Me.lblCaptionHome.Size = New System.Drawing.Size(146, 36)
        Me.lblCaptionHome.TabIndex = 19
        Me.lblCaptionHome.Text = "หน้าหลัก"
        Me.lblCaptionHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmHomeVertical
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(768, 1339)
        Me.Controls.Add(Me.lblCaptionDelivered)
        Me.Controls.Add(Me.lblCaptionPayment)
        Me.Controls.Add(Me.lblCaptionSelectProduct)
        Me.Controls.Add(Me.lblCaptionHome)
        Me.Controls.Add(Me.HomeCurrentScreenSaverFile)
        Me.Controls.Add(Me.HomeCurrentScreenSaverTime)
        Me.Controls.Add(Me.lblPointer)
        Me.Controls.Add(Me.pnlShelf)
        Me.Controls.Add(Me.pbBackground)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmHomeVertical"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pbBackground As PictureBox
    Friend WithEvents pnlShelf As Panel
    Friend WithEvents lblPointer As Label
    Friend WithEvents TimerSetPointer As Timer
    Friend WithEvents HomeCurrentScreenSaverTime As Label
    Friend WithEvents HomeCurrentScreenSaverFile As Label
    Friend WithEvents TimerScreenSaver As Timer
    Friend WithEvents TimerCheckAutoSleep As Timer
    Friend WithEvents TimerCheckPromotion As Timer
    Friend WithEvents lblCaptionDelivered As Label
    Friend WithEvents lblCaptionPayment As Label
    Friend WithEvents lblCaptionSelectProduct As Label
    Friend WithEvents lblCaptionHome As Label
End Class
