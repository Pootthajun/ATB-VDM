﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCompleteVertical
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TimerBackToHome = New System.Windows.Forms.Timer(Me.components)
        Me.pbBackground = New System.Windows.Forms.PictureBox()
        Me.pbIconCheck = New System.Windows.Forms.PictureBox()
        Me.lblCaptionDelivered = New System.Windows.Forms.Label()
        Me.lblCaptionPayment = New System.Windows.Forms.Label()
        Me.lblCaptionSelectProduct = New System.Windows.Forms.Label()
        Me.lblCaptionHome = New System.Windows.Forms.Label()
        Me.lblCaptionMessage = New System.Windows.Forms.Label()
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbIconCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TimerBackToHome
        '
        Me.TimerBackToHome.Enabled = True
        Me.TimerBackToHome.Interval = 1000
        '
        'pbBackground
        '
        Me.pbBackground.BackColor = System.Drawing.Color.Transparent
        Me.pbBackground.Location = New System.Drawing.Point(0, 0)
        Me.pbBackground.Name = "pbBackground"
        Me.pbBackground.Size = New System.Drawing.Size(768, 1366)
        Me.pbBackground.TabIndex = 0
        Me.pbBackground.TabStop = False
        '
        'pbIconCheck
        '
        Me.pbIconCheck.Image = Global.Kiosk_VM.My.Resources.Resources.check_icon
        Me.pbIconCheck.Location = New System.Drawing.Point(174, 609)
        Me.pbIconCheck.Name = "pbIconCheck"
        Me.pbIconCheck.Size = New System.Drawing.Size(400, 400)
        Me.pbIconCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbIconCheck.TabIndex = 1
        Me.pbIconCheck.TabStop = False
        '
        'lblCaptionDelivered
        '
        Me.lblCaptionDelivered.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionDelivered.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionDelivered.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionDelivered.Location = New System.Drawing.Point(563, 40)
        Me.lblCaptionDelivered.Name = "lblCaptionDelivered"
        Me.lblCaptionDelivered.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionDelivered.TabIndex = 103
        Me.lblCaptionDelivered.Text = "รับสินค้า"
        Me.lblCaptionDelivered.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionPayment
        '
        Me.lblCaptionPayment.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionPayment.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionPayment.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionPayment.Location = New System.Drawing.Point(389, 40)
        Me.lblCaptionPayment.Name = "lblCaptionPayment"
        Me.lblCaptionPayment.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionPayment.TabIndex = 102
        Me.lblCaptionPayment.Text = "ชำระเงิน"
        Me.lblCaptionPayment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionSelectProduct
        '
        Me.lblCaptionSelectProduct.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionSelectProduct.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionSelectProduct.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionSelectProduct.Location = New System.Drawing.Point(216, 40)
        Me.lblCaptionSelectProduct.Name = "lblCaptionSelectProduct"
        Me.lblCaptionSelectProduct.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionSelectProduct.TabIndex = 101
        Me.lblCaptionSelectProduct.Text = "เลือกสินค้า"
        Me.lblCaptionSelectProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionHome
        '
        Me.lblCaptionHome.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionHome.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionHome.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionHome.Location = New System.Drawing.Point(44, 40)
        Me.lblCaptionHome.Name = "lblCaptionHome"
        Me.lblCaptionHome.Size = New System.Drawing.Size(146, 36)
        Me.lblCaptionHome.TabIndex = 100
        Me.lblCaptionHome.Text = "หน้าหลัก"
        Me.lblCaptionHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionMessage
        '
        Me.lblCaptionMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionMessage.ForeColor = System.Drawing.Color.White
        Me.lblCaptionMessage.Location = New System.Drawing.Point(71, 466)
        Me.lblCaptionMessage.Name = "lblCaptionMessage"
        Me.lblCaptionMessage.Size = New System.Drawing.Size(611, 92)
        Me.lblCaptionMessage.TabIndex = 104
        Me.lblCaptionMessage.Text = "ระบบกำลังทำรายการ" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "กรุณารอสักครู่"
        Me.lblCaptionMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmCompleteVertical
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(768, 1339)
        Me.Controls.Add(Me.lblCaptionMessage)
        Me.Controls.Add(Me.lblCaptionDelivered)
        Me.Controls.Add(Me.lblCaptionPayment)
        Me.Controls.Add(Me.lblCaptionSelectProduct)
        Me.Controls.Add(Me.lblCaptionHome)
        Me.Controls.Add(Me.pbIconCheck)
        Me.Controls.Add(Me.pbBackground)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmCompleteVertical"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbIconCheck, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TimerBackToHome As Timer
    Friend WithEvents pbBackground As PictureBox
    Friend WithEvents pbIconCheck As PictureBox
    Friend WithEvents lblCaptionDelivered As Label
    Friend WithEvents lblCaptionPayment As Label
    Friend WithEvents lblCaptionSelectProduct As Label
    Friend WithEvents lblCaptionHome As Label
    Friend WithEvents lblCaptionMessage As Label
End Class
