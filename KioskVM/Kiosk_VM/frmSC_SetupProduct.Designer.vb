﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmSC_SetupProduct
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TimerCount = New System.Windows.Forms.Timer(Me.components)
        Me.pnlDialog = New System.Windows.Forms.Panel()
        Me.dgvProductList = New System.Windows.Forms.DataGridView()
        Me.colProductCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProductName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProductID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlProductDetail = New System.Windows.Forms.Panel()
        Me.lblProductID = New System.Windows.Forms.Label()
        Me.lblOldQty = New System.Windows.Forms.Label()
        Me.lblPrice = New System.Windows.Forms.Label()
        Me.lblProductCode = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Panel()
        Me.lblCancel = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Panel()
        Me.lblSave = New System.Windows.Forms.Label()
        Me.pbProductImage = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblShelfRowColumn = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblShelfID = New System.Windows.Forms.Label()
        Me.lblProductName = New System.Windows.Forms.Label()
        Me.numQty = New System.Windows.Forms.NumericUpDown()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.pnlDialog.SuspendLayout()
        CType(Me.dgvProductList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlProductDetail.SuspendLayout()
        Me.btnCancel.SuspendLayout()
        Me.btnSave.SuspendLayout()
        CType(Me.pbProductImage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numQty, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.SuspendLayout()
        '
        'TimerCount
        '
        Me.TimerCount.Enabled = True
        Me.TimerCount.Interval = 1000
        '
        'pnlDialog
        '
        Me.pnlDialog.BackColor = System.Drawing.Color.Transparent
        Me.pnlDialog.Controls.Add(Me.dgvProductList)
        Me.pnlDialog.Controls.Add(Me.pnlProductDetail)
        Me.pnlDialog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlDialog.Location = New System.Drawing.Point(0, 0)
        Me.pnlDialog.Name = "pnlDialog"
        Me.pnlDialog.Size = New System.Drawing.Size(700, 1050)
        Me.pnlDialog.TabIndex = 2
        '
        'dgvProductList
        '
        Me.dgvProductList.AllowUserToAddRows = False
        Me.dgvProductList.AllowUserToDeleteRows = False
        Me.dgvProductList.AllowUserToResizeColumns = False
        Me.dgvProductList.AllowUserToResizeRows = False
        Me.dgvProductList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvProductList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvProductList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProductList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colProductCode, Me.colProductName, Me.colProductID})
        Me.dgvProductList.Location = New System.Drawing.Point(10, 612)
        Me.dgvProductList.Name = "dgvProductList"
        Me.dgvProductList.ReadOnly = True
        Me.dgvProductList.RowHeadersVisible = False
        Me.dgvProductList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvProductList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvProductList.Size = New System.Drawing.Size(679, 426)
        Me.dgvProductList.TabIndex = 114
        '
        'colProductCode
        '
        Me.colProductCode.DataPropertyName = "product_code"
        Me.colProductCode.HeaderText = "Code"
        Me.colProductCode.Name = "colProductCode"
        Me.colProductCode.ReadOnly = True
        Me.colProductCode.Width = 200
        '
        'colProductName
        '
        Me.colProductName.DataPropertyName = "product_name_th"
        Me.colProductName.HeaderText = "Product Name"
        Me.colProductName.Name = "colProductName"
        Me.colProductName.ReadOnly = True
        Me.colProductName.Width = 300
        '
        'colProductID
        '
        Me.colProductID.DataPropertyName = "ms_product_id"
        Me.colProductID.HeaderText = "ProductID"
        Me.colProductID.Name = "colProductID"
        Me.colProductID.ReadOnly = True
        Me.colProductID.Visible = False
        '
        'pnlProductDetail
        '
        Me.pnlProductDetail.BackColor = System.Drawing.Color.White
        Me.pnlProductDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlProductDetail.Controls.Add(Me.lblProductID)
        Me.pnlProductDetail.Controls.Add(Me.lblOldQty)
        Me.pnlProductDetail.Controls.Add(Me.lblPrice)
        Me.pnlProductDetail.Controls.Add(Me.lblProductCode)
        Me.pnlProductDetail.Controls.Add(Me.btnCancel)
        Me.pnlProductDetail.Controls.Add(Me.Label4)
        Me.pnlProductDetail.Controls.Add(Me.btnSave)
        Me.pnlProductDetail.Controls.Add(Me.pbProductImage)
        Me.pnlProductDetail.Controls.Add(Me.Label2)
        Me.pnlProductDetail.Controls.Add(Me.lblShelfRowColumn)
        Me.pnlProductDetail.Controls.Add(Me.Label1)
        Me.pnlProductDetail.Controls.Add(Me.lblShelfID)
        Me.pnlProductDetail.Controls.Add(Me.lblProductName)
        Me.pnlProductDetail.Controls.Add(Me.numQty)
        Me.pnlProductDetail.Location = New System.Drawing.Point(10, 47)
        Me.pnlProductDetail.Name = "pnlProductDetail"
        Me.pnlProductDetail.Size = New System.Drawing.Size(679, 540)
        Me.pnlProductDetail.TabIndex = 113
        '
        'lblProductID
        '
        Me.lblProductID.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblProductID.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProductID.Location = New System.Drawing.Point(16, 380)
        Me.lblProductID.Name = "lblProductID"
        Me.lblProductID.Size = New System.Drawing.Size(79, 31)
        Me.lblProductID.TabIndex = 124
        Me.lblProductID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblProductID.Visible = False
        '
        'lblOldQty
        '
        Me.lblOldQty.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblOldQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblOldQty.Location = New System.Drawing.Point(565, 111)
        Me.lblOldQty.Name = "lblOldQty"
        Me.lblOldQty.Size = New System.Drawing.Size(59, 31)
        Me.lblOldQty.TabIndex = 123
        Me.lblOldQty.Text = "0"
        Me.lblOldQty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblOldQty.Visible = False
        '
        'lblPrice
        '
        Me.lblPrice.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblPrice.Location = New System.Drawing.Point(450, 429)
        Me.lblPrice.Name = "lblPrice"
        Me.lblPrice.Size = New System.Drawing.Size(196, 31)
        Me.lblPrice.TabIndex = 122
        Me.lblPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblProductCode
        '
        Me.lblProductCode.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblProductCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProductCode.Location = New System.Drawing.Point(120, 429)
        Me.lblProductCode.Name = "lblProductCode"
        Me.lblProductCode.Size = New System.Drawing.Size(236, 31)
        Me.lblProductCode.TabIndex = 121
        Me.lblProductCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnCancel.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnCancel.Controls.Add(Me.lblCancel)
        Me.btnCancel.Location = New System.Drawing.Point(361, 479)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(126, 43)
        Me.btnCancel.TabIndex = 111
        '
        'lblCancel
        '
        Me.lblCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblCancel.BackColor = System.Drawing.Color.Transparent
        Me.lblCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCancel.ForeColor = System.Drawing.Color.Black
        Me.lblCancel.Location = New System.Drawing.Point(9, 5)
        Me.lblCancel.Name = "lblCancel"
        Me.lblCancel.Size = New System.Drawing.Size(106, 32)
        Me.lblCancel.TabIndex = 35
        Me.lblCancel.Text = "Cancel"
        Me.lblCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(35, 429)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(79, 31)
        Me.Label4.TabIndex = 120
        Me.Label4.Text = "Code"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnSave.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSave.Controls.Add(Me.lblSave)
        Me.btnSave.Location = New System.Drawing.Point(206, 479)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(123, 43)
        Me.btnSave.TabIndex = 110
        '
        'lblSave
        '
        Me.lblSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblSave.BackColor = System.Drawing.Color.Transparent
        Me.lblSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSave.ForeColor = System.Drawing.Color.Black
        Me.lblSave.Location = New System.Drawing.Point(8, 5)
        Me.lblSave.Name = "lblSave"
        Me.lblSave.Size = New System.Drawing.Size(103, 32)
        Me.lblSave.TabIndex = 35
        Me.lblSave.Text = "Save"
        Me.lblSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pbProductImage
        '
        Me.pbProductImage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbProductImage.Location = New System.Drawing.Point(134, 111)
        Me.pbProductImage.Name = "pbProductImage"
        Me.pbProductImage.Size = New System.Drawing.Size(416, 300)
        Me.pbProductImage.TabIndex = 119
        Me.pbProductImage.TabStop = False
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(496, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 31)
        Me.Label2.TabIndex = 117
        Me.Label2.Text = "Qty"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblShelfRowColumn
        '
        Me.lblShelfRowColumn.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblShelfRowColumn.Location = New System.Drawing.Point(120, 70)
        Me.lblShelfRowColumn.Name = "lblShelfRowColumn"
        Me.lblShelfRowColumn.Size = New System.Drawing.Size(115, 31)
        Me.lblShelfRowColumn.TabIndex = 116
        Me.lblShelfRowColumn.Text = "Row / Column"
        Me.lblShelfRowColumn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(35, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 31)
        Me.Label1.TabIndex = 115
        Me.Label1.Text = "Shelf"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblShelfID
        '
        Me.lblShelfID.AutoSize = True
        Me.lblShelfID.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblShelfID.Location = New System.Drawing.Point(494, 10)
        Me.lblShelfID.Name = "lblShelfID"
        Me.lblShelfID.Size = New System.Drawing.Size(39, 42)
        Me.lblShelfID.TabIndex = 112
        Me.lblShelfID.Text = "0"
        Me.lblShelfID.Visible = False
        '
        'lblProductName
        '
        Me.lblProductName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblProductName.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProductName.Location = New System.Drawing.Point(3, 10)
        Me.lblProductName.Name = "lblProductName"
        Me.lblProductName.Size = New System.Drawing.Size(669, 42)
        Me.lblProductName.TabIndex = 114
        Me.lblProductName.Text = "Product Name"
        Me.lblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'numQty
        '
        Me.numQty.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.numQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.numQty.Location = New System.Drawing.Point(559, 69)
        Me.numQty.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.numQty.Name = "numQty"
        Me.numQty.Size = New System.Drawing.Size(87, 35)
        Me.numQty.TabIndex = 8
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel7.Controls.Add(Me.Label36)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel7.Location = New System.Drawing.Point(0, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(700, 41)
        Me.Panel7.TabIndex = 6
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(27, 3)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(396, 35)
        Me.Label36.TabIndex = 66
        Me.Label36.Text = "Setup Product"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmSC_SetupProduct
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(700, 1050)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.pnlDialog)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSC_SetupProduct"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmDialog_TimeOut"
        Me.pnlDialog.ResumeLayout(False)
        CType(Me.dgvProductList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlProductDetail.ResumeLayout(False)
        Me.pnlProductDetail.PerformLayout()
        Me.btnCancel.ResumeLayout(False)
        Me.btnSave.ResumeLayout(False)
        CType(Me.pbProductImage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numQty, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TimerCount As Timer
    Friend WithEvents pnlDialog As Panel
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Label36 As Label
    Friend WithEvents numQty As NumericUpDown
    Friend WithEvents btnCancel As Panel
    Friend WithEvents lblCancel As Label
    Friend WithEvents btnSave As Panel
    Friend WithEvents lblSave As Label
    Friend WithEvents lblShelfID As Label
    Friend WithEvents pnlProductDetail As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents lblShelfRowColumn As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lblProductName As Label
    Friend WithEvents lblProductCode As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents pbProductImage As PictureBox
    Friend WithEvents lblPrice As Label
    Friend WithEvents dgvProductList As DataGridView
    Friend WithEvents colProductCode As DataGridViewTextBoxColumn
    Friend WithEvents colProductName As DataGridViewTextBoxColumn
    Friend WithEvents colProductID As DataGridViewTextBoxColumn
    Friend WithEvents lblOldQty As Label
    Friend WithEvents lblProductID As Label
End Class
