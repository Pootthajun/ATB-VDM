﻿Imports System.IO.Ports
Imports System.IO


Public Class frmTestForm
    Private Sub frmTestForm_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Dim ws As New VDMWebService.ATBVendingWebservice
        'ws.Timeout = 10000
        ''ws.Url = VendingConfig.WebserviceVDMURL

        'PrintFld = ws.GetConfigSlipFieldList()
        'If PrintFld.Rows.Count > 0 Then
        '    VendingData.VendingID = 1
        '    UcSetupSlip1.SetMasterFieldList = PrintFld
        'End If
    End Sub

    Private Sub frmTestForm_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        UcSetupSlip1.ucSetupSlipLoad()
    End Sub

    Private Sub btnReStart_Click(sender As Object, e As EventArgs) Handles btnReStart.Click
        Application.Restart()
    End Sub

    Private Sub btnOpenComport_Click(sender As Object, e As EventArgs) Handles btnOpenComport.Click
        If SerialPort1.IsOpen Then SerialPort1.Close()
        SerialPort1.PortName = "COM10"
        SerialPort1.BaudRate = 9600
        SerialPort1.DataBits = 8
        SerialPort1.StopBits = IO.Ports.StopBits.One
        SerialPort1.DtrEnable = True
        SerialPort1.Parity = IO.Ports.Parity.Even
        SerialPort1.Open()


    End Sub

    Private Sub SerialPort1_DataReceived(sender As Object, e As SerialDataReceivedEventArgs) Handles SerialPort1.DataReceived
        Dim Hex As String = ""
        Try
            If Not SerialPort1.IsOpen Then SerialPort1.Open()
            For i As Integer = 0 To SerialPort1.BytesToRead - 1
                Dim d As Integer = SerialPort1.ReadByte
                Dim b(0) As Byte
                b(0) = d
                Hex &= Engine.ConverterENG.BytesToHexString(b) & " "
            Next

            CreateTextErrorLog(Hex)
        Catch ex As Exception : End Try
    End Sub



    Private Shared Sub CreateTextErrorLog(LogMsg As String)
        Try
            Dim frame As StackFrame = New StackFrame(1, True)
            Dim ClassName As String = frame.GetMethod.ReflectedType.Name
            Dim FunctionName As String = frame.GetMethod.Name
            Dim LineNo As Integer = frame.GetFileLineNumber

            Dim MY As String = DateTime.Now.ToString("yyyyMM")
            Dim DD As String = DateTime.Now.ToString("dd")
            Dim LogFolder As String = Application.StartupPath & "\ErrorLog\" & MY & "\" & DD & "\"
            If Directory.Exists(LogFolder) = False Then
                Directory.CreateDirectory(LogFolder)
            End If

            Dim FileName As String = LogFolder & ClassName & "_" & DateTime.Now.ToShortDateString("yyyyMMddHH") & ".txt"
            Dim obj As New StreamWriter(FileName, True)
            obj.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") & " " & FunctionName + " Line No :" + LineNo + Environment.NewLine + LogMsg + Environment.NewLine + Environment.NewLine)
            obj.Flush()
            obj.Close()
        Catch ex As Exception

        End Try
    End Sub
End Class