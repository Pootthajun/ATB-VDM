﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmSC_SetupShelf
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TimerCount = New System.Windows.Forms.Timer(Me.components)
        Me.pnlDialog = New System.Windows.Forms.Panel()
        Me.lblID = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Panel()
        Me.lblCancel = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Panel()
        Me.lblSave = New System.Windows.Forms.Label()
        Me.btnToBuy = New System.Windows.Forms.Panel()
        Me.lblToBuy = New System.Windows.Forms.Label()
        Me.btnTackItem = New System.Windows.Forms.Panel()
        Me.lblTakeItem = New System.Windows.Forms.Label()
        Me.chkActive = New System.Windows.Forms.CheckBox()
        Me.btnCarriageAction = New System.Windows.Forms.Panel()
        Me.lblCarriageAction = New System.Windows.Forms.Label()
        Me.btnMoveTo = New System.Windows.Forms.Panel()
        Me.lblMoveTo = New System.Windows.Forms.Label()
        Me.numWidth = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.numColumn = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.numRow = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.numY = New System.Windows.Forms.NumericUpDown()
        Me.numX = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.pnlDialog.SuspendLayout()
        Me.btnCancel.SuspendLayout()
        Me.btnSave.SuspendLayout()
        Me.btnToBuy.SuspendLayout()
        Me.btnTackItem.SuspendLayout()
        Me.btnCarriageAction.SuspendLayout()
        Me.btnMoveTo.SuspendLayout()
        CType(Me.numWidth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numColumn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numRow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numY, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numX, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.SuspendLayout()
        '
        'TimerCount
        '
        Me.TimerCount.Enabled = True
        Me.TimerCount.Interval = 1000
        '
        'pnlDialog
        '
        Me.pnlDialog.BackColor = System.Drawing.Color.Transparent
        Me.pnlDialog.Controls.Add(Me.lblID)
        Me.pnlDialog.Controls.Add(Me.btnCancel)
        Me.pnlDialog.Controls.Add(Me.btnSave)
        Me.pnlDialog.Controls.Add(Me.btnToBuy)
        Me.pnlDialog.Controls.Add(Me.btnTackItem)
        Me.pnlDialog.Controls.Add(Me.chkActive)
        Me.pnlDialog.Controls.Add(Me.btnCarriageAction)
        Me.pnlDialog.Controls.Add(Me.btnMoveTo)
        Me.pnlDialog.Controls.Add(Me.numWidth)
        Me.pnlDialog.Controls.Add(Me.Label5)
        Me.pnlDialog.Controls.Add(Me.numColumn)
        Me.pnlDialog.Controls.Add(Me.Label4)
        Me.pnlDialog.Controls.Add(Me.numRow)
        Me.pnlDialog.Controls.Add(Me.Label3)
        Me.pnlDialog.Controls.Add(Me.Label2)
        Me.pnlDialog.Controls.Add(Me.numY)
        Me.pnlDialog.Controls.Add(Me.numX)
        Me.pnlDialog.Controls.Add(Me.Label1)
        Me.pnlDialog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlDialog.Location = New System.Drawing.Point(0, 0)
        Me.pnlDialog.Name = "pnlDialog"
        Me.pnlDialog.Size = New System.Drawing.Size(700, 726)
        Me.pnlDialog.TabIndex = 2
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblID.Location = New System.Drawing.Point(26, 107)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(39, 42)
        Me.lblID.TabIndex = 112
        Me.lblID.Text = "0"
        Me.lblID.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnCancel.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnCancel.Controls.Add(Me.lblCancel)
        Me.btnCancel.Location = New System.Drawing.Point(374, 627)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(126, 43)
        Me.btnCancel.TabIndex = 111
        '
        'lblCancel
        '
        Me.lblCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblCancel.BackColor = System.Drawing.Color.Transparent
        Me.lblCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCancel.ForeColor = System.Drawing.Color.Black
        Me.lblCancel.Location = New System.Drawing.Point(9, 5)
        Me.lblCancel.Name = "lblCancel"
        Me.lblCancel.Size = New System.Drawing.Size(106, 32)
        Me.lblCancel.TabIndex = 35
        Me.lblCancel.Text = "Cancel"
        Me.lblCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnSave.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSave.Controls.Add(Me.lblSave)
        Me.btnSave.Location = New System.Drawing.Point(219, 627)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(123, 43)
        Me.btnSave.TabIndex = 110
        '
        'lblSave
        '
        Me.lblSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblSave.BackColor = System.Drawing.Color.Transparent
        Me.lblSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSave.ForeColor = System.Drawing.Color.Black
        Me.lblSave.Location = New System.Drawing.Point(8, 5)
        Me.lblSave.Name = "lblSave"
        Me.lblSave.Size = New System.Drawing.Size(103, 32)
        Me.lblSave.TabIndex = 35
        Me.lblSave.Text = "Save"
        Me.lblSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnToBuy
        '
        Me.btnToBuy.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnToBuy.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnToBuy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnToBuy.Controls.Add(Me.lblToBuy)
        Me.btnToBuy.Location = New System.Drawing.Point(387, 490)
        Me.btnToBuy.Name = "btnToBuy"
        Me.btnToBuy.Size = New System.Drawing.Size(161, 72)
        Me.btnToBuy.TabIndex = 109
        '
        'lblToBuy
        '
        Me.lblToBuy.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblToBuy.BackColor = System.Drawing.Color.Transparent
        Me.lblToBuy.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblToBuy.ForeColor = System.Drawing.Color.Black
        Me.lblToBuy.Location = New System.Drawing.Point(21, 13)
        Me.lblToBuy.Name = "lblToBuy"
        Me.lblToBuy.Size = New System.Drawing.Size(119, 45)
        Me.lblToBuy.TabIndex = 35
        Me.lblToBuy.Text = "To Buy"
        Me.lblToBuy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnTackItem
        '
        Me.btnTackItem.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnTackItem.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnTackItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTackItem.Controls.Add(Me.lblTakeItem)
        Me.btnTackItem.Location = New System.Drawing.Point(164, 490)
        Me.btnTackItem.Name = "btnTackItem"
        Me.btnTackItem.Size = New System.Drawing.Size(161, 72)
        Me.btnTackItem.TabIndex = 108
        '
        'lblTakeItem
        '
        Me.lblTakeItem.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTakeItem.BackColor = System.Drawing.Color.Transparent
        Me.lblTakeItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTakeItem.ForeColor = System.Drawing.Color.Black
        Me.lblTakeItem.Location = New System.Drawing.Point(21, 13)
        Me.lblTakeItem.Name = "lblTakeItem"
        Me.lblTakeItem.Size = New System.Drawing.Size(119, 45)
        Me.lblTakeItem.TabIndex = 35
        Me.lblTakeItem.Text = "Take Item"
        Me.lblTakeItem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkActive
        '
        Me.chkActive.AutoSize = True
        Me.chkActive.Checked = True
        Me.chkActive.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkActive.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkActive.Location = New System.Drawing.Point(496, 288)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Size = New System.Drawing.Size(114, 37)
        Me.chkActive.TabIndex = 109
        Me.chkActive.Text = "Active"
        Me.chkActive.UseVisualStyleBackColor = True
        '
        'btnCarriageAction
        '
        Me.btnCarriageAction.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnCarriageAction.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnCarriageAction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnCarriageAction.Controls.Add(Me.lblCarriageAction)
        Me.btnCarriageAction.Location = New System.Drawing.Point(387, 391)
        Me.btnCarriageAction.Name = "btnCarriageAction"
        Me.btnCarriageAction.Size = New System.Drawing.Size(161, 72)
        Me.btnCarriageAction.TabIndex = 108
        '
        'lblCarriageAction
        '
        Me.lblCarriageAction.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblCarriageAction.BackColor = System.Drawing.Color.Transparent
        Me.lblCarriageAction.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCarriageAction.ForeColor = System.Drawing.Color.Black
        Me.lblCarriageAction.Location = New System.Drawing.Point(12, 13)
        Me.lblCarriageAction.Name = "lblCarriageAction"
        Me.lblCarriageAction.Size = New System.Drawing.Size(137, 45)
        Me.lblCarriageAction.TabIndex = 35
        Me.lblCarriageAction.Text = "Carriage Action"
        Me.lblCarriageAction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnMoveTo
        '
        Me.btnMoveTo.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnMoveTo.BackgroundImage = Global.Kiosk_VM.My.Resources.Resources.btnColWhite
        Me.btnMoveTo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMoveTo.Controls.Add(Me.lblMoveTo)
        Me.btnMoveTo.Location = New System.Drawing.Point(164, 391)
        Me.btnMoveTo.Name = "btnMoveTo"
        Me.btnMoveTo.Size = New System.Drawing.Size(161, 72)
        Me.btnMoveTo.TabIndex = 107
        '
        'lblMoveTo
        '
        Me.lblMoveTo.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblMoveTo.BackColor = System.Drawing.Color.Transparent
        Me.lblMoveTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblMoveTo.ForeColor = System.Drawing.Color.Black
        Me.lblMoveTo.Location = New System.Drawing.Point(21, 13)
        Me.lblMoveTo.Name = "lblMoveTo"
        Me.lblMoveTo.Size = New System.Drawing.Size(119, 45)
        Me.lblMoveTo.TabIndex = 35
        Me.lblMoveTo.Text = "Move To"
        Me.lblMoveTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'numWidth
        '
        Me.numWidth.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.numWidth.Location = New System.Drawing.Point(182, 279)
        Me.numWidth.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.numWidth.Name = "numWidth"
        Me.numWidth.Size = New System.Drawing.Size(120, 49)
        Me.numWidth.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(63, 281)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 42)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Width"
        '
        'numColumn
        '
        Me.numColumn.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.numColumn.Location = New System.Drawing.Point(496, 196)
        Me.numColumn.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.numColumn.Name = "numColumn"
        Me.numColumn.Size = New System.Drawing.Size(120, 49)
        Me.numColumn.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(344, 198)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(146, 42)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Column"
        '
        'numRow
        '
        Me.numRow.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.numRow.Location = New System.Drawing.Point(182, 196)
        Me.numRow.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.numRow.Name = "numRow"
        Me.numRow.Size = New System.Drawing.Size(120, 49)
        Me.numRow.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(83, 198)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 42)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Row"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(447, 109)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 42)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Y"
        '
        'numY
        '
        Me.numY.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.numY.Location = New System.Drawing.Point(496, 107)
        Me.numY.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.numY.Name = "numY"
        Me.numY.Size = New System.Drawing.Size(120, 49)
        Me.numY.TabIndex = 3
        '
        'numX
        '
        Me.numX.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.numX.Location = New System.Drawing.Point(182, 107)
        Me.numX.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.numX.Name = "numX"
        Me.numX.Size = New System.Drawing.Size(120, 49)
        Me.numX.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(133, 109)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 42)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "X"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel7.Controls.Add(Me.Label36)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel7.Location = New System.Drawing.Point(0, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(700, 41)
        Me.Panel7.TabIndex = 6
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(27, 3)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(396, 35)
        Me.Label36.TabIndex = 66
        Me.Label36.Text = "Setup Shelf"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmSC_SetupShelf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(700, 726)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.pnlDialog)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSC_SetupShelf"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmDialog_TimeOut"
        Me.pnlDialog.ResumeLayout(False)
        Me.pnlDialog.PerformLayout()
        Me.btnCancel.ResumeLayout(False)
        Me.btnSave.ResumeLayout(False)
        Me.btnToBuy.ResumeLayout(False)
        Me.btnTackItem.ResumeLayout(False)
        Me.btnCarriageAction.ResumeLayout(False)
        Me.btnMoveTo.ResumeLayout(False)
        CType(Me.numWidth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numColumn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numRow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numY, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numX, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TimerCount As Timer
    Friend WithEvents pnlDialog As Panel
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Label36 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents numWidth As NumericUpDown
    Friend WithEvents Label5 As Label
    Friend WithEvents numColumn As NumericUpDown
    Friend WithEvents Label4 As Label
    Friend WithEvents numRow As NumericUpDown
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents numY As NumericUpDown
    Friend WithEvents numX As NumericUpDown
    Friend WithEvents chkActive As CheckBox
    Friend WithEvents btnCarriageAction As Panel
    Friend WithEvents lblCarriageAction As Label
    Friend WithEvents btnMoveTo As Panel
    Friend WithEvents lblMoveTo As Label
    Friend WithEvents btnToBuy As Panel
    Friend WithEvents lblToBuy As Label
    Friend WithEvents btnTackItem As Panel
    Friend WithEvents lblTakeItem As Label
    Friend WithEvents btnCancel As Panel
    Friend WithEvents lblCancel As Label
    Friend WithEvents btnSave As Panel
    Friend WithEvents lblSave As Label
    Friend WithEvents lblID As Label
End Class
