﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTestForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnReStart = New System.Windows.Forms.Button()
        Me.UcSetupSlip1 = New Kiosk_VM.ucSetupSlip()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.btnOpenComport = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnReStart
        '
        Me.btnReStart.Location = New System.Drawing.Point(687, 491)
        Me.btnReStart.Name = "btnReStart"
        Me.btnReStart.Size = New System.Drawing.Size(75, 23)
        Me.btnReStart.TabIndex = 1
        Me.btnReStart.Text = "Restart"
        Me.btnReStart.UseVisualStyleBackColor = True
        '
        'UcSetupSlip1
        '
        Me.UcSetupSlip1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.UcSetupSlip1.Location = New System.Drawing.Point(69, 12)
        Me.UcSetupSlip1.Name = "UcSetupSlip1"
        Me.UcSetupSlip1.Size = New System.Drawing.Size(651, 473)
        Me.UcSetupSlip1.TabIndex = 0
        '
        'btnOpenComport
        '
        Me.btnOpenComport.Location = New System.Drawing.Point(12, 491)
        Me.btnOpenComport.Name = "btnOpenComport"
        Me.btnOpenComport.Size = New System.Drawing.Size(107, 23)
        Me.btnOpenComport.TabIndex = 2
        Me.btnOpenComport.Text = "Open ComPort"
        Me.btnOpenComport.UseVisualStyleBackColor = True
        '
        'frmTestForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(807, 529)
        Me.Controls.Add(Me.btnOpenComport)
        Me.Controls.Add(Me.btnReStart)
        Me.Controls.Add(Me.UcSetupSlip1)
        Me.Name = "frmTestForm"
        Me.Text = "frmTestForm"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents UcSetupSlip1 As ucSetupSlip
    Friend WithEvents btnReStart As Button
    Friend WithEvents SerialPort1 As IO.Ports.SerialPort
    Friend WithEvents btnOpenComport As Button
End Class
