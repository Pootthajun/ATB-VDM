﻿Imports System.Drawing.Printing
Imports System.Drawing.Drawing2D
Imports VendingLinqDB.ConnectDB
Imports VendingLinqDB.TABLE
Imports Engine

Public Class frmCompleteVertical


    Private Sub frmComplete_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.ControlBox = False
        Me.BackColor = bgColor
        Me.BackgroundImage = Image.FromFile(Application.StartupPath & "\Background\bgComplete.jpg")

        SetChildFormLanguage()
    End Sub
    Private Sub frmComplete_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.Complete
        InsertLogSalesActivity(vTrans.TransNo, "แสดงหน้าจอ Complete", VendingConfig.SelectForm, False)

        'พิมพ์ Slip ,ทอนเงินให้ลูกค้า ปิดช่องรับสินค้า เคลื่อน Motor กลับไปยังจุด Park และแสดงหน้าจอ Complete

    End Sub


    Public Sub PaymentCompletePrintSlip()
        Try
            'พิมพ์ slip
            If PrintCF.Rows.Count > 0 Then
                InsertLogSalesActivity(vTrans.TransNo, "พิมพ์ Slip", VendingConfig.SelectForm, False)
                PrintSlip()
            End If

            If vTrans.ChangeAmount > 0 Then
                InsertLogSalesActivity(vTrans.TransNo, "ทอนเงินให้ลูกค้า จำนวน " & vTrans.ChangeAmount & " บาท", VendingConfig.SelectForm, False)
                ChangeMoney(vTrans.ChangeAmount, vTrans)
            End If

            If UpdateProductMovement().IsSuccess = True Then
                vTrans.TransStatus = Data.SalesTransactionData.TransactionStatus.Success
                UpdateSaleTransaction(vTrans)
            End If
        Catch ex As Exception
            InsertLogSalesActivity(vTrans.TransNo, "Exception PaymentCompletePrintSlip : " & ex.Message & " " & ex.StackTrace, VendingConfig.SelectForm, True)
        End Try
    End Sub

    Private Function UpdateProductMovement() As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            InsertLogSalesActivity(vTrans.TransNo, "ปรับปรุงข้อมูลจำนวนสินค้า", VendingConfig.SelectForm, False)

            Dim vPlnq As New MsProductShelfVendingLinqDB
            vPlnq.GetDataByPK(vTrans.MsProductShelfID, Nothing)
            If vPlnq.ID > 0 Then
                vPlnq.PRODUCT_QTY -= 1

                Dim trans As New VendingTransactionDB
                ret = vPlnq.UpdateData(Environment.MachineName, trans.Trans)
                If ret.IsSuccess = True Then

                    Dim pmLnq As New TbProductMovementVendingLinqDB
                    pmLnq.MS_VENDING_ID = VendingData.VendingID
                    pmLnq.MOVEMENT_DATE = DateTime.Now
                    pmLnq.MS_PRODUCT_ID = vTrans.ProductID
                    pmLnq.SHELF_ID = vTrans.ShelfID
                    pmLnq.MOVEMENT_TYPE = Convert.ToInt16(Data.ConstantsData.ProductMovementType.Sales).ToString
                    pmLnq.PRODUCT_QTY = 1
                    pmLnq.SYNC_TO_SERVER = "N"

                    ret = pmLnq.InsertData(Environment.MachineName, trans.Trans)
                    If ret.IsSuccess = True Then
                        trans.CommitTransaction()
                    Else
                        trans.RollbackTransaction()
                        InsertLogSalesActivity(vTrans.TransNo, ret.ErrorMessage, VendingConfig.SelectForm, True)
                    End If
                Else
                    trans.RollbackTransaction()
                    InsertLogSalesActivity(vTrans.TransNo, ret.ErrorMessage, VendingConfig.SelectForm, True)
                End If
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = vPlnq.ErrorMessage
                InsertLogSalesActivity(vTrans.TransNo, vPlnq.ErrorMessage, VendingConfig.SelectForm, True)
            End If
            vPlnq = Nothing
        Catch ex As Exception
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception UpdateProductMovement : " & ex.Message & " " & ex.StackTrace

            InsertLogSalesActivity(vTrans.TransNo, ret.ErrorMessage, VendingConfig.SelectForm, True)
        End Try
        Return ret
    End Function

#Region "Print With PrintDocument"
    Private Sub PrintSlip()
        Try

            Dim p As New PrintDocument
            p.PrintController = New Printing.StandardPrintController
            'p.PrinterSettings.PrinterName = KioskConfig.PrinterDeviceName

            AddHandler p.PrintPage, AddressOf p_PrintPage
            p.DefaultPageSettings.Margins = New Margins(0, 0, 0, 0)

            p.Print()

            UpdateKioskCurrentQty(mdlDeviceInfoENG.DeviceID.Printer, -1, 0, False)
        Catch ex As Exception
            InsertLogSalesActivity(vTrans.TransNo, "Exception PrintSlip : " & ex.Message & " " & ex.StackTrace, VendingConfig.SelectForm, True)
        End Try

    End Sub

    Private Sub p_PrintPage(sender As System.Object, e As System.Drawing.Printing.PrintPageEventArgs)
        Try
            If PrintCF.Rows.Count > 0 Then

                Dim ProductCode As String = ""
                Dim ProductNameTh As String = ""
                Dim ProductNameEn As String = ""
                Dim ProductCategoryID As Long = 0
                Dim CategoryCode As String = ""
                Dim CategoryName As String = ""

                If ProductMasterList.Rows.Count > 0 Then
                    ProductMasterList.DefaultView.RowFilter = "ms_product_id=" & vTrans.ProductID
                    If ProductMasterList.DefaultView.Count > 0 Then
                        ProductCode = ProductMasterList.DefaultView(0)("product_code")
                        ProductNameTh = ProductMasterList.DefaultView(0)("product_name_th")
                        ProductNameEn = ProductMasterList.DefaultView(0)("product_name_en")
                        ProductCategoryID = vTrans.ProductCategoryID

                        CategoryCode = ProductMasterList.DefaultView(0)("category_code")
                        CategoryName = ProductMasterList.DefaultView(0)("category_name")
                    End If
                    ProductMasterList.DefaultView.RowFilter = ""
                End If

                Dim pt As New Printer.PrinterClass

                For Each dr As DataRow In PrintCF.Rows
                    If dr("CTL_TYPE") = "1" Then  'Textbox
                        Dim fntSize As Single = dr("CTL_TEXT_FONTSIZE").ToString
                        Dim fntStyle As FontStyle = dr("CTL_TEXT_FONTSTYLE").ToString

                        Dim TextData As String = SetSlipParameter(dr("CTL_TEXT"), PrintFld, vTrans, ProductCategoryID, ProductNameTh, ProductNameEn, ProductCategoryID, CategoryCode, CategoryName)
                        Dim fnt As New Font(dr("CTL_TEXT_FONTNAME").ToString, fntSize, fntStyle)
                        pt.PrintText(TextData, fnt, dr("CTL_TEXT_ALIGN").ToString, dr("CTL_TEXT_WIDTH"), New Point(dr("CTL_LOCATION_X"), dr("CTL_LOCATION_Y")), e)
                    ElseIf dr("CTL_TYPE") = "2" Then  'Picturebox
                        Using pBytes As New IO.MemoryStream(DirectCast(dr("CTL_IMAGE"), Byte()))
                            pt.PrintImage(Image.FromStream(pBytes), New Point(dr("CTL_LOCATION_X"), dr("CTL_LOCATION_Y")), e)
                        End Using
                    End If
                Next
            End If
        Catch ex As Exception
            InsertLogSalesActivity(vTrans.TransNo, "Exception p_PrintPage : " & ex.Message & " " & ex.StackTrace, VendingConfig.SelectForm, True)
        End Try
    End Sub

#End Region


    Dim Timeout As Integer = VendingConfig.ShowMsgSec
    Dim TimeOutCheckTime As DateTime = DateTime.Now
    Private Sub TimerBackToHome_Tick(sender As Object, e As EventArgs) Handles TimerBackToHome.Tick
        If TimeOutCheckTime.AddSeconds(Timeout) <= DateTime.Now Then
            'ปิดช่องรับสินค้า สั่งเคลื่อน Motor แกนให้กลับไปจุด Park และกลับหน้าแรก
            InsertLogSalesActivity(vTrans.TransNo, "ปิดช่องรับสินค้า", VendingConfig.SelectForm, False)

            'สั่งให้ Motor กลับจุด Park สามารถทำพร้อมกับการปิดช่องรับสินค้าได้
            InsertLogSalesActivity(vTrans.TransNo, "สั่งเคลื่อน Motor แกนให้กลับไปจุดเริ่มต้น", VendingConfig.SelectForm, False)




            InsertLogSalesActivity(vTrans.TransNo, "รายการเสร็จสมบูรณ์ กลับหน้าแรก", VendingConfig.SelectForm, False)
            frmHomeVertical.Show()
            Me.Close()
        End If
    End Sub


End Class