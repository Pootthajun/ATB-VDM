﻿Imports Kiosk_VM.Data
Namespace Data
    Public Class SalesTransactionData
        Dim _VendingID As String = ""
        Dim _TransactionID As Long = 0
        Dim _TransNo As String = ""
        Dim _TransStartTime As New DateTime(1, 1, 1)
        Dim _ProductCategoryID As Long = 0
        Dim _ProductID As Long = 0
        Dim _ProductCode As String = ""
        Dim _ProductCost As Double = 0
        Dim _ProductPrice As Double = 0
        'Dim _ProductDesc As String = ""
        'Dim _PromotionID As Long = 0
        Dim _DiscountP As Integer = 0
        Dim _NetPrice As Double = 0
        Dim _ProfitSharing As Integer = 0

        Dim _MsProductShelfID As Long = 0
        Dim _ShelfID As Integer = 0
        Dim _PositionX As Integer = 0
        Dim _PositionY As Integer = 0
        Dim _ShelfRow As Integer = 0
        Dim _ShelfColumn As Integer = 0

        Dim _TransStatus As TransactionStatus = TransactionStatus.Inprogress

        Dim _PaidTime As DateTime = New DateTime(1, 1, 1)
        Dim _PaidAmount As Integer = 0
        Dim _ReceiveCoin1 As Integer = 0
        Dim _ReceiveCoin2 As Integer = 0
        Dim _ReceiveCoin5 As Integer = 0
        Dim _ReceiveCoin10 As Integer = 0
        Dim _ReceiveBankNote20 As Integer = 0
        Dim _ReceiveBankNote50 As Integer = 0
        Dim _ReceiveBankNote100 As Integer = 0
        Dim _ReceiveBankNote500 As Integer = 0
        Dim _ReceiveBankNote1000 As Integer = 0

        Dim _ChangeAmount As Integer = 0
        Dim _ChangeCoin1 As Integer = 0
        Dim _ChangeCoin2 As Integer = 0
        Dim _ChangeCoin5 As Integer = 0
        Dim _ChangeCoin10 As Integer = 0
        Dim _ChangeBankNote20 As Integer = 0
        Dim _ChangeBankNote50 As Integer = 0
        Dim _ChangeBankNote100 As Integer = 0
        Dim _ChangeBankNote500 As Integer = 0

        Public Sub New(VendingID As String)
            _VendingID = VendingID
        End Sub

        Public Property TransactionID As Long
            Get
                Return _TransactionID
            End Get
            Set(value As Long)
                _TransactionID = value
            End Set
        End Property
        Public Property TransNo As String
            Get
                Return _TransNo.Trim
            End Get
            Set(value As String)
                _TransNo = value
            End Set
        End Property

        Public Property TransStartTime As DateTime
            Get
                Return _TransStartTime
            End Get
            Set(value As DateTime)
                _TransStartTime = value
            End Set
        End Property

        Public Property ProductCategoryID As Long
            Get
                Return _ProductCategoryID
            End Get
            Set(value As Long)
                _ProductCategoryID = value
            End Set
        End Property

        Public Property ProductID As Long
            Get
                Return _ProductID
            End Get
            Set(value As Long)
                _ProductID = value
            End Set
        End Property

        Public Property ProductCode As String
            Get
                Return _ProductCode.Trim
            End Get
            Set(value As String)
                _ProductCode = value
            End Set
        End Property

        Public Property ProductCost As Double
            Get
                Return _ProductCost
            End Get
            Set(value As Double)
                _ProductCost = value
            End Set
        End Property

        Public Property ProductPrice As Double
            Get
                Return _ProductPrice
            End Get
            Set(value As Double)
                _ProductPrice = value
            End Set
        End Property
        'Public Property ProductDesc As String
        '    Get
        '        Return _ProductDesc.Trim
        '    End Get
        '    Set(value As String)
        '        _ProductDesc = value
        '    End Set
        'End Property


        'Public Property PromotionID As Long
        '    Get
        '        Return _PromotionID
        '    End Get
        '    Set(value As Long)
        '        _PromotionID = value
        '    End Set
        'End Property

        Public Property DiscountP As Integer
            Get
                Return _DiscountP
            End Get
            Set(value As Integer)
                _DiscountP = value
            End Set
        End Property

        Public Property NetPrice As Double
            Get
                Return _NetPrice
            End Get
            Set(value As Double)
                _NetPrice = value
            End Set
        End Property
        Public Property ProfitSharing As Integer
            Get
                Return _ProfitSharing
            End Get
            Set(value As Integer)
                _ProfitSharing = value
            End Set
        End Property

        Public Property MsProductShelfID As Long
            Get
                Return _MsProductShelfID
            End Get
            Set(value As Long)
                _MsProductShelfID = value
            End Set
        End Property

        Public Property ShelfID As Integer
            Get
                Return _ShelfID
            End Get
            Set(value As Integer)
                _ShelfID = value
            End Set
        End Property

        Public Property PositionY As Integer
            Get
                Return _PositionY
            End Get
            Set(value As Integer)
                _PositionY = value
            End Set
        End Property

        Public Property PositionX As Integer
            Get
                Return _PositionX
            End Get
            Set(value As Integer)
                _PositionX = value
            End Set
        End Property

        Public Property ShelfRow As Integer
            Get
                Return _ShelfRow
            End Get
            Set(value As Integer)
                _ShelfRow = value
            End Set
        End Property
        Public Property ShelfColumn As Integer
            Get
                Return _ShelfColumn
            End Get
            Set(value As Integer)
                _ShelfColumn = value
            End Set
        End Property

        Public Property TransStatus As TransactionStatus
            Get
                Return _TransStatus
            End Get
            Set(value As TransactionStatus)
                _TransStatus = value
            End Set
        End Property

        Public Property PaidTime As DateTime
            Get
                Return _PaidTime
            End Get
            Set(value As DateTime)
                _PaidTime = value
            End Set
        End Property

        Public Property PaidAmount As Integer
            Get
                Return _PaidAmount
            End Get
            Set(value As Integer)
                _PaidAmount = value
            End Set
        End Property

        Public Property ReceiveCoin1 As Integer
            Get
                Return _ReceiveCoin1
            End Get
            Set(value As Integer)
                _ReceiveCoin1 = value
            End Set
        End Property

        Public Property ReceiveCoin2 As Integer
            Get
                Return _ReceiveCoin2
            End Get
            Set(value As Integer)
                _ReceiveCoin2 = value
            End Set
        End Property
        Public Property ReceiveCoin5 As Integer
            Get
                Return _ReceiveCoin5
            End Get
            Set(value As Integer)
                _ReceiveCoin5 = value
            End Set
        End Property
        Public Property ReceiveCoin10 As Integer
            Get
                Return _ReceiveCoin10
            End Get
            Set(value As Integer)
                _ReceiveCoin10 = value
            End Set
        End Property
        Public Property ReceiveBankNote20 As Integer
            Get
                Return _ReceiveBankNote20
            End Get
            Set(value As Integer)
                _ReceiveBankNote20 = value
            End Set
        End Property

        Public Property ReceiveBankNote50 As Integer
            Get
                Return _ReceiveBankNote50
            End Get
            Set(value As Integer)
                _ReceiveBankNote50 = value
            End Set
        End Property

        Public Property ReceiveBankNote100 As Integer
            Get
                Return _ReceiveBankNote100
            End Get
            Set(value As Integer)
                _ReceiveBankNote100 = value
            End Set
        End Property

        Public Property ReceiveBankNote500 As Integer
            Get
                Return _ReceiveBankNote500
            End Get
            Set(value As Integer)
                _ReceiveBankNote500 = value
            End Set
        End Property

        Public Property ReceiveBankNote1000 As Integer
            Get
                Return _ReceiveBankNote1000
            End Get
            Set(value As Integer)
                _ReceiveBankNote1000 = value
            End Set
        End Property

        Public Property ChangeAmount As Integer
            Get
                Return _ChangeAmount
            End Get
            Set(value As Integer)
                _ChangeAmount = value
            End Set
        End Property
        Public Property ChangeCoin1 As Integer
            Get
                Return _ChangeCoin1
            End Get
            Set(value As Integer)
                _ChangeCoin1 = value
            End Set
        End Property
        Public Property ChangeCoin2 As Integer
            Get
                Return _ChangeCoin2
            End Get
            Set(value As Integer)
                _ChangeCoin2 = value
            End Set
        End Property
        Public Property ChangeCoin5 As Integer
            Get
                Return _ChangeCoin5
            End Get
            Set(value As Integer)
                _ChangeCoin5 = value
            End Set
        End Property
        Public Property ChangeCoin10 As Integer
            Get
                Return _ChangeCoin10
            End Get
            Set(value As Integer)
                _ChangeCoin10 = value
            End Set
        End Property
        Public Property ChangeBankNote20 As Integer
            Get
                Return _ChangeBankNote20
            End Get
            Set(value As Integer)
                _ChangeBankNote20 = value
            End Set
        End Property
        Public Property ChangeBankNote50 As Integer
            Get
                Return _ChangeBankNote50
            End Get
            Set(value As Integer)
                _ChangeBankNote50 = value
            End Set
        End Property
        Public Property ChangeBankNote100 As Integer
            Get
                Return _ChangeBankNote100
            End Get
            Set(value As Integer)
                _ChangeBankNote100 = value
            End Set
        End Property
        Public Property ChangeBankNote500 As Integer
            Get
                Return _ChangeBankNote500
            End Get
            Set(value As Integer)
                _ChangeBankNote500 = value
            End Set
        End Property


        Public Enum TransactionStatus
            Inprogress = 0
            Success = 1
            Cancel = 2
            Problem = 3
            TimeOut = 4
        End Enum


    End Class
End Namespace