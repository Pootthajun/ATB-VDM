﻿Namespace Data
    Module ConstantsData
        Public Enum VendingLanguage
            Thai = 1
            English = 2
            China = 3
            Japan = 4
        End Enum

        Public Enum ProductMovementType
            StockIn = 1
            Sales = 2
            MoveOut = 3
            MoveIn = 4
            StockOut = 5
        End Enum

        Public Enum StaffConsoleFunctionalID
            FillPaper = 13
            FillMoney = 14
            VendingSetting = 15
            DeviceSetting = 16
            SetupShelf = 17
            SetupProduct = 18
            SetupSlip = 25
        End Enum

    End Module

End Namespace
