﻿
Namespace Data

    Public Class VendingConfigData
        Dim _VendingID As String = ""
        Dim _SelectForm As VendingForm

        Dim _Language As String = Data.ConstantsData.VendingLanguage.Thai
        Dim _ScreenLayout As String = "V"
        Dim _ScreenSaverSec As Integer = 0
        Dim _TimeOutSec As Integer = 0
        Dim _ShowMsgSec As Integer = 0
        Dim _PaymentExtendSec As Integer = 0
        Dim _SleepTime As String = ""
        Dim _SleepDuration As Integer = 0
        Dim _ShelfLong As Integer = 0
        Dim _WebserviceVDMURL As String = ""
        Dim _WebServiceAlarmURL As String = ""

        Dim _BanknoteInComport As String = ""
        Dim _BanknoteOUT20Comport As String = ""
        Dim _BanknoteOUT50Comport As String = ""
        Dim _BanknoteOUT100Comport As String = ""
        Dim _BanknoteOUT500Comport As String = ""

        Dim _CoinINComport As String = ""
        Dim _CoinOut1Comport As String = ""
        Dim _CoinOut2Comport As String = ""
        Dim _CoinOut5Comport As String = ""
        Dim _CoinOut10Comport As String = ""

        Dim _PrinterDeviceName As String = ""

        Public Sub New(VendingID As String)
            _VendingID = VendingID
        End Sub

        Public Property SelectForm As VendingForm
            Get
                Return _SelectForm
            End Get
            Set(value As VendingForm)
                _SelectForm = value
            End Set
        End Property

        Public Property Language As String
            Get
                Return _Language.Trim
            End Get
            Set(value As String)
                _Language = value
            End Set
        End Property
        Public Property ScreenLayout As String
            Get
                Return _ScreenLayout.Trim
            End Get
            Set(value As String)
                _ScreenLayout = value
            End Set
        End Property
        Public Property ScreenSaverSec As Integer
            Get
                Return _ScreenSaverSec
            End Get
            Set(value As Integer)
                _ScreenSaverSec = value
            End Set
        End Property
        Public Property TimeOutSec As Integer
            Get
                Return _TimeOutSec
            End Get
            Set(value As Integer)
                _TimeOutSec = value
            End Set
        End Property
        Public Property ShowMsgSec As Integer
            Get
                Return _ShowMsgSec
            End Get
            Set(value As Integer)
                _ShowMsgSec = value
            End Set
        End Property
        Public Property PaymentExtendSec As Integer
            Get
                Return _PaymentExtendSec
            End Get
            Set(value As Integer)
                _PaymentExtendSec = value
            End Set
        End Property
        Public Property SleepTime As String
            Get
                Return _SleepTime.Trim
            End Get
            Set(value As String)
                _SleepTime = value
            End Set
        End Property
        Public Property SleepDuration As Integer
            Get
                Return _SleepDuration
            End Get
            Set(value As Integer)
                _SleepDuration = value
            End Set
        End Property
        Public Property ShelfLong As Integer
            Get
                Return _ShelfLong
            End Get
            Set(value As Integer)
                _ShelfLong = value
            End Set
        End Property


        Public Property WebserviceVDMURL As String
            Get
                Return _WebserviceVDMURL.Trim
            End Get
            Set(value As String)
                _WebserviceVDMURL = value
            End Set
        End Property
        Public Property WebServiceAlarmURL As String
            Get
                Return _WebServiceAlarmURL.Trim
            End Get
            Set(value As String)
                _WebServiceAlarmURL = value
            End Set
        End Property

        Public Property CoinInComport As String
            Get
                Return _CoinINComport.Trim
            End Get
            Set(value As String)
                _CoinINComport = value
            End Set
        End Property
        Public Property CoinOut1Comport As String
            Get
                Return _CoinOut1Comport.Trim
            End Get
            Set(value As String)
                _CoinOut1Comport = value
            End Set
        End Property
        Public Property CoinOut2Comport As String
            Get
                Return _CoinOut2Comport.Trim
            End Get
            Set(value As String)
                _CoinOut2Comport = value
            End Set
        End Property
        Public Property CoinOut5Comport As String
            Get
                Return _CoinOut5Comport.Trim
            End Get
            Set(value As String)
                _CoinOut5Comport = value
            End Set
        End Property
        Public Property CoinOut10Comport As String
            Get
                Return _CoinOut10Comport.Trim
            End Get
            Set(value As String)
                _CoinOut10Comport = value
            End Set
        End Property

        Public Property BanknoteInComport As String
            Get
                Return _BanknoteInComport.Trim
            End Get
            Set(value As String)
                _BanknoteInComport = value
            End Set
        End Property
        Public Property BanknoteOUT20Comport As String
            Get
                Return _BanknoteOUT20Comport.Trim
            End Get
            Set(value As String)
                _BanknoteOUT20Comport = value
            End Set
        End Property
        Public Property BanknoteOUT50Comport As String
            Get
                Return _BanknoteOUT50Comport.Trim
            End Get
            Set(value As String)
                _BanknoteOUT50Comport = value
            End Set
        End Property
        Public Property BanknoteOUT100Comport As String
            Get
                Return _BanknoteOUT100Comport.Trim
            End Get
            Set(value As String)
                _BanknoteOUT100Comport = value
            End Set
        End Property
        Public Property BanknoteOUT500Comport As String
            Get
                Return _BanknoteOUT500Comport.Trim
            End Get
            Set(value As String)
                _BanknoteOUT500Comport = value
            End Set
        End Property

        Public Property PrinterDeviceName As String
            Get
                Return _PrinterDeviceName.Trim
            End Get
            Set(value As String)
                _PrinterDeviceName = value
            End Set
        End Property

        Public Enum VendingForm

            Home = 1
            ProductDetail = 2
            ProductWarranty = 3
            Payment = 4
            Processing = 5
            Complete = 6
            VendingError = 7
            Main = 8

            StaffConsoleLogin = 9
            StaffConsoleStockAndHardware = 10
            StaffConsoleFillPaper = 11
            StaffConsoleFillMoney = 12
            StaffConsoleVendingSetting = 13
            StaffConsoleDeviceSetting = 14
            StaffConsoleSetupShelf = 15
            StaffConsoleAddEditShelf = 16
            StaffConsoleSetupProduct = 17
            StaffConsoleSetupSlip = 18

            ScreenSaver = 19
        End Enum

        'Public Enum VendingStep
        '    Main_GetKioskSystemData = 101
        '    Main_GetDeviceInfo = 102
        '    Main_StartPassportDevice = 103
        '    Main_StartMoneyDevice = 104
        '    Main_StartBoardDevice = 105
        '    Main_LoadLockerList = 106
        '    Main_LoadCabinetList = 107
        '    Main_LoadCabinetModelList = 108
        '    Main_LoadAppScreenList = 109
        '    Main_OpenFormLoginStaffConsole = 110
        '    Main_ChangeLangTH = 111
        '    Main_ChangeLangEN = 112
        '    Main_ChangeLangCH = 113
        '    Main_ChangeLangJP = 114
        '    Main_Cancel = 115
        '    Main_GetKioskConfig = 116
        '    Main_SetLEDStatus = 117

        '    Home_CheckHardwareStatus = 201
        '    Home_CheckStockQty = 202
        '    Home_LoadLockerList = 203
        '    Home_ClickDeposit = 204
        '    Home_ClickPickup = 205

        '    DepositSelectLocker_OpenForm = 301
        '    DepositSelectLocker_SelectLocker = 302
        '    DepositSelectLocker_LoadLockerList = 303

        '    DepositScanPersonInfo_OpenForm = 401
        '    DepositScanPersonInfo_CheckPassportDevice = 402
        '    DepositScanPersonInfo_CheckIDCardDevice = 403
        '    DepositScanPersonInfo_ScanIDCard = 404
        '    DepositScanPersonInfo_ScanPassport = 405
        '    DepositScanPersonInfo_CheckIDCardExpire = 406
        '    DepositScanPersonInfo_CheckPassportExpire = 407

        '    DepositPayment_OpenForm = 501
        '    DepositPayment_CheckHardwareStatus = 502
        '    DepositPayment_CheckStockQty = 503
        '    DepositPayment_StartDeviceBanknoteIn = 504
        '    DepositPayment_StartDeviceCoinIn = 505
        '    DepositPayment_ReceiveBankNote = 506
        '    DepositPayment_ReceiveCoin = 507
        '    DepositPayment_PaidSuccess = 508
        '    DepositPayment_ShowExtend = 509  'แสดง Dialog Extend Timeout
        '    DepositPayment_OKExtend = 510
        '    DepositPayment_CancelExtend = 511
        '    DepositPayment_PaidTimeOut = 512
        '    DepositPayment_ReturnMoney = 513

        '    DepositPrintQRCode_OpenForm = 601
        '    DepositPrintQRCode_OpenLocker = 602
        '    DepositPrintQRCode_ChangeMoney = 603
        '    DepositPrintQRCode_LEDBlinkOn = 604
        '    DepositPrintQRCode_PrintSlip = 605
        '    DepositPrintQRCode_ReturnMoney = 606    'OpenLockerFail

        '    DepositThankYou_OpenForm = 701
        '    DepositThankYou_StartSensor = 702
        '    DepositThankYou_CloseLocker = 703
        '    DepositThankYou_LEDBlinkOff = 704
        '    DepositThankYou_BackToHome = 705

        '    PickupScanQRCode_OpenForm = 801
        '    PickupScanQRCode_CheckDataQRCode = 802
        '    PickupScanQRCode_CalServiceAmount = 803
        '    PickupScanQRCode_ClickQRCodeLost = 804
        '    PickupScanQRCode_Timeout = 805

        '    PickupPayment_OpenForm = 1101
        '    PickupPayment_CheckHardwareStatus = 1102
        '    PickupPayment_CheckStockQty = 1103
        '    PickupPayment_StartDeviceBanknoteIn = 1104
        '    PickupPayment_StartDeviceCoinIn = 1105
        '    PickupPayment_ReceiveBankNote = 1106
        '    PickupPayment_ReceiveCoin = 1107
        '    PickupPayment_PaidSuccess = 1108
        '    PickupPayment_ShowExtend = 1109  'แสดง Dialog Extend Timeout
        '    PickupPayment_OKExtend = 1110
        '    PickupPayment_CancelExtend = 1111
        '    PickupPayment_PaidTimeOut = 1112
        '    PickupPayment_ReturnMoney = 1113

        '    PickupScanPersonInfo_OpenForm = 1001
        '    PickupScanPersonInfo_CheckPassportDevice = 1002
        '    PickupScanPersonInfo_CheckIDCardDevice = 1003
        '    PickupScanPersonInfo_ScanIDCard = 1004
        '    PickupScanPersonInfo_ScanPassport = 1005
        '    PickupScanPersonInfo_CheckIDCardExpire = 1006
        '    PickupScanPersonInfo_CheckPassportExpire = 1007
        '    PickupScanPersonInfo_GetPickupWithScanIDCard = 1008
        '    PickupScanPersonInfo_GetPickupWithScanPassport = 1009
        '    PickupScanPersonInfo_HaveData = 1010
        '    PickupScanPersonInfo_NoPersonData = 1011
        '    PickupScanPersonInfo_GetPickupDataFail = 1012


        '    PickupConformOpenLocker_OpenForm = 901
        '    PickupConformOpenLocker_ClickOpenLocker = 902
        '    PickupConformOpenLocker_TimeOutOpenLocker = 903
        '    PickupConformOpenLocker_LEDBlinkOn = 904
        '    PickupConformOpenLocker_ReturnMoney = 905    'OpenLockerFail

        '    PickupThankYou_OpenForm = 1201
        '    PickupThankYou_StartSensor = 1202
        '    PickupThankYou_CloseLocker = 1203
        '    PickupThankYou_LEDStart = 1204
        '    PickupThankYou_ChangeMoney = 1205
        '    PickupThankYou_BackToHome = 1206

        '    StaffConsoleLogin_OpenFOrm = 1501
        '    StaffConsoleLogin_ClickLogin = 1502
        '    StaffConsoleLogin_LoginValidate = 1503
        '    StaffConsoleLogin_GetAuthorize = 1504
        '    StaffConsoleLogin_CreateTransaction = 1505
        '    StaffConsoleLogin_UpdateHardwareAndStock = 1506
        '    StaffConsoleLogin_ClickCancel = 1507

        '    StaffConsoleStoakAndHardware_OpenForm = 1601
        '    StaffConsoleStoakAndHardware_GetKioskConfig = 1602
        '    StaffConsoleStoakAndHardware_SetStockAndHardwareStatus = 1603
        '    StaffConsoleStoakAndHardware_CheckAuthorize = 1604
        '    StaffConsoleStoakAndHardware_ClickClose = 1605
        '    StaffConsoleStoakAndHardware_ClickFillPaper = 1606
        '    StaffConsoleStoakAndHardware_ClickFillMoney = 1607
        '    StaffConsoleStoakAndHardware_ClickKioskSetting = 1608
        '    StaffConsoleStoakAndHardware_ClickDeviceSetting = 1609
        '    StaffConsoleStoakAndHardware_ClickLockerSetting = 1610
        '    StaffConsoleStoakAndHardware_ClickOpenAll = 1611
        '    StaffConsoleStoakAndHardware_ClickExitProgram = 1612

        '    StaffConsoleFillPaper_OpenForm = 1701
        '    StaffConsoleFillPaper_CheckAuthorize = 1702
        '    StaffConsoleFillPaper_ClickConfirm = 1703
        '    StaffConsoleFillPaper_ClickCancel = 1704

        '    StaffConsoleFillMoney_OpenFOrm = 1801
        '    StaffConsoleFillMoney_CheckKioskMoney = 1802
        '    StaffConsoleFillMoney_InsertFillMoney = 1803
        '    StaffConsoleFillMoney_CheckAuthorize = 1804
        '    StaffConsoleFillMoney_ClickCheckOutMoney = 1805
        '    StaffConsoleFillMoney_ClickFillAllFull = 1806
        '    StaffConsoleFillMoney_ClickConfirm = 1807
        '    StaffConsoleFillMoney_ClickCancel = 1808

        '    StaffConsoleKioskSetting_OpenForm = 1901
        '    StaffConsoleKioskSetting_SetKioskSetting = 1902
        '    StaffConsoleKioskSetting_CheckAuthorize = 1903
        '    StaffConsoleKioskSetting_ClickSave = 1904
        '    StaffConsoleKioskSetting_ClickCancel = 1905

        '    StaffConsoleDeviceSetting_OpenForm = 2001
        '    StaffConsoleDeviceSetting_SetDefaultSetting = 2002
        '    StaffConsoleDeviceSetting_CheckAuthorize = 2003
        '    StaffConsoleDeviceSetting_ClickSave = 2004
        '    StaffConsoleDeviceSetting_ClickCancel = 2005

        '    StaffConsoleLockerSetting_OpenForm = 2101
        '    StaffConsoleLockerSetting_LoadCabinetInfo = 2102
        '    StaffConsoleLockerSetting_AdjustLayout = 2103
        '    StaffConsoleLockerSetting_CheckAuthorize = 2104
        '    StaffConsoleLockerSetting_ClickSave = 2105
        '    StaffConsoleLockerSetting_ClickClose = 2106
        '    StaffConsoleLockerSetting_CabinetAdd = 2107
        '    StaffConsoleLockerSetting_CabinetEdit = 2108
        '    StaffConsoleLockerSetting_CabinetDelete = 2109
        '    StaffConsoleLockerSetting_LockerDbClick = 2110

        'End Enum
    End Class
End Namespace


