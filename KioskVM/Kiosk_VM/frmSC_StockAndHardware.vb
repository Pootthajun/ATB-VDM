﻿Imports System.Management
Imports System.Net.NetworkInformation
Imports System.Data.SqlClient
Imports System.IO
Imports System.Drawing.Printing
Imports VendingLinqDB.ConnectDB
Imports VendingLinqDB.TABLE
'Imports Kiosk_VM.Data.KioskConfigData
Imports Engine
Imports Kiosk_VM.Data.ConstantsData

Public Class frmSC_StockAndHardware

    Public Enum FormColor
        Red = 1
        Yellow = 2
        Green = 3
    End Enum
    Private Sub frmSC_StockAndHardware_Load(sender As Object, e As EventArgs) Handles Me.Load
        VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.StaffConsoleStockAndHardware
        Me.ControlBox = False
    End Sub

    Private Sub frmSC_StockAndHardware_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "แสดงหน้าจอ Stock and Hardware", VendingConfig.SelectForm, False)
        LoadTabStockAndHardware()
    End Sub

    Private Sub CheckStaffConsoleAuthorization()
        If StaffConsole.AuthorizeInfo.Rows.Count > 0 Then
            'Fill Paper
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.FillPaper
            If StaffConsole.AuthorizeInfo.DefaultView.Count = 0 Then
                TabContainer1.TabPages.Remove(tabFillPaper)
            End If
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""

            'Fill Money
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.FillMoney
            If StaffConsole.AuthorizeInfo.DefaultView.Count = 0 Then
                TabContainer1.TabPages.Remove(tabFillMoney)
            End If
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""

            'Vending Setting
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.VendingSetting
            If StaffConsole.AuthorizeInfo.DefaultView.Count = 0 Then
                TabContainer1.TabPages.Remove(tabVendingSetting)
            End If
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""

            'Device Setting
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.DeviceSetting
            If StaffConsole.AuthorizeInfo.DefaultView.Count = 0 Then
                TabContainer1.TabPages.Remove(tabDeviceSetting)
            End If
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""

            'Setup Product
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.SetupProduct
            If StaffConsole.AuthorizeInfo.DefaultView.Count = 0 Then
                TabContainer1.TabPages.Remove(tabSetupProduct)
            End If
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""

            'Setup Shelf
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.SetupShelf
            If StaffConsole.AuthorizeInfo.DefaultView.Count = 0 Then
                TabContainer1.TabPages.Remove(tabSetupShelf)
            End If
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""

            ''Setup Slip
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.SetupSlip
            If StaffConsole.AuthorizeInfo.DefaultView.Count = 0 Then
                TabContainer1.TabPages.Remove(tabSetupSlip)
            End If
            StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""
        End If
    End Sub


#Region "Code Stock and Hardware"
    Private Sub LoadTabStockAndHardware()
        Application.DoEvents()
        VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.StaffConsoleStockAndHardware
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "ตรวจสอบข้อมูล Vending Config", VendingConfig.SelectForm, False)
        GetVendingConfig()

        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "แสดงสถานะ Stock และ อุปกรณ์", VendingConfig.SelectForm, False)
        SetStockAndHardwareStatus()

        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "ตรวจสอบสิทธิ์การใช้งาน Staff Console", VendingConfig.SelectForm, False)
        CheckStaffConsoleAuthorization()

        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "แสดงรายการสินค้าในตู้", VendingConfig.SelectForm, False)
        SetShelfLayout(pnlProductShelf, False)
    End Sub


    Private Sub SetStockAndHardwareStatus()
        Dim sql As String = "select kd.device_type_id, kd.device_id, kd.device_name_en, kd.icon_white, kd.icon_green, kd.icon_red,"
        sql += " kd.current_status_name, kd.is_problem,kd.movement_direction,"
        sql += " kd.vending_max_qty, kd.vending_warning_qty, kd.vending_critical_qty, kd.vending_current_qty"
        sql += " from v_vending_device_info kd "
        sql += " where kd.ms_vending_id=@_VENDING_ID"
        sql += " and kd.type_active_status='Y' and kd.device_active_status='Y'"
        sql += " order by kd.device_order"
        Dim p(1) As SqlParameter
        p(0) = VendingDB.SetBigInt("@_VENDING_ID", Convert.ToInt16(VendingData.VendingID))

        Dim Dt As DataTable = VendingDB.ExecuteTable(sql, p)
        If Dt.Rows.Count > 0 Then
            flpHWStatus.Controls.Clear()
            flpM_Stock.Controls.Clear()

            For i As Integer = 0 To Dt.Rows.Count - 1
                Dim dr As DataRow = Dt.Rows(i)

                Dim DeviceName As String = ""
                Dim IconGreen() As Byte = {}
                Dim IconYellow() As Byte = {}
                Dim IconRed() As Byte = {}

                DeviceName = dr("device_name_en")
                If Convert.IsDBNull(dr("icon_green")) = False Then
                    IconGreen = CType(dr("icon_green"), Byte())
                    IconYellow = CType(dr("icon_green"), Byte())
                End If
                If Convert.IsDBNull(dr("icon_red")) = False Then
                    IconRed = CType(dr("icon_red"), Byte())
                End If

                Dim StatusName As String = dr("current_status_name")
                Dim IsProblem As Boolean = IIf(dr("is_problem") = "Y", True, False)
                Dim Movement As String = dr("movement_direction")

                'Hardware Status
                Dim frm As New ucfrmStatus
                If IsProblem = False Then
                    SetForm(frm, IconGreen, FormColor.Green, DeviceName, StatusName)
                Else
                    SetForm(frm, IconRed, FormColor.Red, DeviceName, StatusName)
                End If
                flpHWStatus.Controls.Add(frm)

                'Material Stock
                frm = New ucfrmStatus
                If Dt.Rows(i).Item("device_type_id") = mdlDeviceInfoENG.DeviceType.BanknoteIn Or
                   Dt.Rows(i).Item("device_type_id") = mdlDeviceInfoENG.DeviceType.BanknoteOut Or
                   Dt.Rows(i).Item("device_type_id") = mdlDeviceInfoENG.DeviceType.CoinIn Or
                   Dt.Rows(i).Item("device_type_id") = mdlDeviceInfoENG.DeviceType.CoinOut Or
                   Dt.Rows(i).Item("device_type_id") = mdlDeviceInfoENG.DeviceType.Printer Then

                    If Movement <> "" Then
                        Dim CurrentQty As Integer = 0
                        Dim MaxQty As Integer = 0
                        Dim CriticalQty As Integer = 0
                        Dim WarningQty As Integer = 0

                        If Convert.IsDBNull(dr("vending_current_qty")) = False Then
                            CurrentQty = Convert.ToInt64(dr("vending_current_qty"))
                        End If
                        If Convert.IsDBNull(dr("vending_max_qty")) = False Then
                            MaxQty = Convert.ToInt64(dr("vending_max_qty"))
                        End If
                        If Convert.IsDBNull(dr("vending_critical_qty")) = False Then
                            CriticalQty = Convert.ToInt64(dr("vending_critical_qty"))
                        End If
                        If Convert.IsDBNull(dr("vending_warning_qty")) = False Then
                            WarningQty = Convert.ToInt64(dr("vending_warning_qty"))
                        End If

                        Dim Detail As String = CurrentQty.ToString & "/" & MaxQty.ToString
                        If Movement = "1" Then
                            If CurrentQty < WarningQty Then
                                'Normal
                                SetForm(frm, IconGreen, FormColor.Green, DeviceName, Detail)
                            ElseIf CurrentQty < CriticalQty Then
                                'Warning
                                SetForm(frm, IconYellow, FormColor.Yellow, DeviceName, Detail)
                            Else
                                'Critical
                                SetForm(frm, IconRed, FormColor.Red, DeviceName, Detail)
                            End If
                        Else
                            If CurrentQty > WarningQty Then
                                'Normal
                                SetForm(frm, IconGreen, FormColor.Green, DeviceName, Detail)
                            ElseIf CurrentQty > CriticalQty Then
                                'Warning
                                SetForm(frm, IconYellow, FormColor.Yellow, DeviceName, Detail)
                            Else
                                'Critical
                                SetForm(frm, IconRed, FormColor.Red, DeviceName, Detail)
                            End If
                        End If
                    End If
                    flpM_Stock.Controls.Add(frm)
                End If
            Next
        End If
        Dt.Dispose()
    End Sub

    Sub SetForm(ByVal frm As ucfrmStatus, ByVal ImgByte() As Byte, ByVal frmColor As FormColor, ByVal frmHeader As String, ByVal frmMessage As String)
        If ImgByte.Length > 0 Then
            Dim ms As New MemoryStream(ImgByte)
            Dim im As Image
            im = Image.FromStream(ms)
            Dim Img As Image
            Img = im
            frm.pb.Image = Img
        End If

        frm.lblHeader.Text = frmHeader
        frm.lblDetail.Text = frmMessage
        Select Case frmColor
            Case FormColor.Red
                frm.pnlDetail.BackColor = Color.Red
            Case FormColor.Yellow
                frm.pnlDetail.BackColor = Color.Yellow
            Case FormColor.Green
                frm.pnlDetail.BackColor = Color.Green
        End Select

    End Sub

#End Region

    Private Sub pbClose_Click(sender As Object, e As EventArgs) Handles pbClose.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "ปิดหน้าจอ Staff Console", VendingConfig.SelectForm, False)
        'Me.Close()
        Application.Restart()
    End Sub

    Private Sub TabContainer1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabContainer1.SelectedIndexChanged
        If TabContainer1.SelectedTab Is tabStockAndHardware Then
            VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.StaffConsoleStockAndHardware
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "แสดงหน้าจอ Stock and Hardware", VendingConfig.SelectForm, False)
            LoadTabStockAndHardware()

        ElseIf TabContainer1.SelectedTab Is tabFillPaper Then
            VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.StaffConsoleFillPaper
            LoadTabFillPaper()
        ElseIf TabContainer1.SelectedTab Is tabFillMoney Then
            VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.StaffConsoleFillMoney
            LoadTabFillMoney()
        ElseIf TabContainer1.SelectedTab Is tabVendingSetting Then
            VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.StaffConsoleVendingSetting
            LoadTabVendingSetting()
        ElseIf TabContainer1.SelectedTab Is tabSetupShelf Then
            VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.StaffConsoleSetupShelf
            LoadTabSetupShelf()
        ElseIf TabContainer1.SelectedTab Is tabSetupProduct Then
            VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.StaffConsoleSetupProduct
            LoadTabSetupProduct()
        ElseIf TabContainer1.SelectedTab Is tabSetupSlip Then
            VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.StaffConsoleSetupSlip
            UcSetupSlip1.ucSetupSlipLoad()
        ElseIf TabContainer1.SelectedTab Is tabDeviceSetting Then
            VendingConfig.SelectForm = Data.VendingConfigData.VendingForm.StaffConsoleDeviceSetting
            LoadDeviceSetting()
        End If
    End Sub


    Dim Warning As Integer = 0
    Dim Critical As Integer = 0
    Dim Max As Integer = 0

#Region "Code Fill Paper"

    Private Sub LoadTabFillPaper()
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "แสดงหน้าจอ Fill Paper", VendingConfig.SelectForm, False)
        txtFillPaperValue.Focus()

        Dim lnq As New MsVendingDeviceVendingLinqDB
        lnq.ChkDataByMS_DEVICE_ID_MS_VENDING_ID(mdlDeviceInfoENG.DeviceID.Printer, VendingData.VendingID, Nothing)
        If lnq.ID > 0 Then
            txtFillPaperValue.Text = lnq.CURRENT_QTY
            txtFillPaperValue.SelectAll()

            Warning = lnq.WARNING_QTY
            Critical = lnq.CRITICAL_QTY
            Max = lnq.MAX_QTY
            FillPaperMax = lnq.MAX_QTY
        End If
        lnq = Nothing

        txtFillPaperMax.Text = FillPaperMax
        ChangeColor()

        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "ตรวจสอบสิทธิ์การใช้งานหน้าจอ Fill Paper", VendingConfig.SelectForm, False)
        SetFillPaperAuthorize()
    End Sub

    Private Sub SetFillPaperAuthorize()
        If StaffConsole.AuthorizeInfo.Rows.Count > 0 Then
            AppScreenList.DefaultView.RowFilter = "id='" & Convert.ToInt16(VendingConfig.SelectForm) & "'"
            If AppScreenList.DefaultView.Count > 0 Then
                txtFillPaperValue.Enabled = False
                btnFillPaperConfirm.Visible = False

                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.FillPaper & " and authorization_name='Edit'"
                If StaffConsole.AuthorizeInfo.DefaultView.Count > 0 Then
                    txtFillPaperValue.Enabled = True
                    btnFillPaperConfirm.Visible = True
                End If
                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""
            End If
            AppScreenList.DefaultView.RowFilter = ""
        End If
    End Sub

    Dim FillPaperMax As Integer = 0
    Private Sub btnFillPaperConfirm_Click(sender As Object, e As EventArgs) Handles btnFillPaperConfirm.Click, lblFillPaperConfirm.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่มยืนยันการเติมกระดาษ", VendingConfig.SelectForm, False)

        If txtFillPaperValue.Text = "" Then
            ShowDialogErrorMessageSC("กรุณากรอกข้อมูลจำนวนครั้งที่พิมพ์")
            Exit Sub
        ElseIf CInt(txtFillPaperValue.Text) > FillPaperMax Then
            ShowDialogErrorMessageSC("กรอกข้อมูลจำนวนครั้งที่พิมพ์ เกินจำนวนเต็มที่กำหนด")
            Exit Sub
        End If

        Dim ret As ExecuteDataInfo = UpdateKioskCurrentQty(mdlDeviceInfoENG.DeviceID.Printer, CInt(txtFillPaperValue.Text), 0, True)
        If ret.IsSuccess = True Then
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "เติมกระดาษจำนวน " & txtFillPaperValue.Text & " ใบ", VendingConfig.SelectForm, False)
            ShowDialogErrorMessageSC("Fill Paper Success")
            TabContainer1.SelectedTab = tabStockAndHardware
        Else
            ShowDialogErrorMessageSC(ret.ErrorMessage)
        End If
    End Sub

    Private Sub ChangeColor()
        If txtFillPaperValue.Text = "" Then
            txtFillPaperValue.BackColor = Color.White
            Exit Sub
        End If
        If CInt(txtFillPaperValue.Text) <= Critical Then
            txtFillPaperValue.BackColor = Color.Red
        ElseIf CInt(txtFillPaperValue.Text) <= Warning Then
            txtFillPaperValue.BackColor = Color.Yellow
        Else
            txtFillPaperValue.BackColor = Color.Green
        End If
    End Sub

    Private Sub lblFillPaperCancel_Click(sender As Object, e As EventArgs) Handles btnFillPaperCancel.Click, lblFillPaperCancel.Click

        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่มยกเลิกการเติมกระดาษ", VendingConfig.SelectForm, False)
        TabContainer1.SelectedTab = tabStockAndHardware
    End Sub
#End Region

#Region "Code Fill Money"
    Dim _FillMoneyID As Long = 0


    Private Sub LoadTabFillMoney()
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "แสดงหน้าจอเติมเงิน", VendingConfig.SelectForm, False)

        Dim sql As String = "select kd.device_id, kd.vending_max_qty max_qty, kd.vending_current_qty current_qty, kd.vending_current_money current_money,kd.unit_value"
        sql += " from v_vending_device_info kd "
        sql += " where kd.ms_vending_id=@_VENDING_ID"
        sql += " and kd.type_active_status='Y' and kd.device_active_status='Y'"
        sql += " order by kd.device_name_en"
        Dim p(1) As SqlParameter
        p(0) = VendingDB.SetBigInt("@_VENDING_ID", Convert.ToInt16(VendingData.VendingID))

        Dim TotalMoney As Integer = 0
        Dim Dt As DataTable = VendingDB.ExecuteTable(sql, p)
        If Dt.Rows.Count > 0 Then
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "ตรวจสอบจำนวนเงินคงเหลือในตู้", VendingConfig.SelectForm, False)

            For i As Integer = 0 To Dt.Rows.Count - 1
                Dim dr As DataRow = Dt.Rows(i)

                Dim vDeviceID As Long = Convert.ToInt64(dr("device_id"))

                Select Case vDeviceID
                    Case mdlDeviceInfoENG.DeviceID.CoinIn
                        If Convert.IsDBNull(dr("current_money")) = False Then
                            txtFillMoneyCoinInMoney.Text = Convert.ToInt64(dr("current_money")).ToString("#,##0.00")
                            TotalMoney += Convert.ToInt64(dr("current_money"))
                        End If
                    Case mdlDeviceInfoENG.DeviceID.BankNoteIn
                        If Convert.IsDBNull(dr("current_money")) = False Then
                            txtFillMoneyBanknoteInMoney.Text = Convert.ToInt64(dr("current_money")).ToString("#,##0.00")
                            TotalMoney += Convert.ToInt64(dr("current_money"))
                        End If
                    Case mdlDeviceInfoENG.DeviceID.CoinOut_5
                        If Convert.IsDBNull(dr("current_qty")) = False Then
                            txtFillMoneyCoinOut5.Text = Convert.ToInt64(dr("current_qty"))
                            TotalMoney += (Convert.ToInt64(dr("current_qty")) * Convert.ToInt16(dr("unit_value")))
                        End If

                        If Convert.IsDBNull(dr("max_qty")) = False Then
                            lblFillMoneyTotalCoinOut5.Text = "/ " & Convert.ToInt16(dr("max_qty")) & " เหรียญ"
                            lblFillMoneyMaxCoin5.Text = Convert.ToInt16(dr("max_qty")).ToString("#,##0")
                        End If
                    Case mdlDeviceInfoENG.DeviceID.BankNoteOut_20
                        If Convert.IsDBNull(dr("current_qty")) = False Then
                            txtFillMoneyBanknoteOut20.Text = Convert.ToInt64(dr("current_qty"))
                            TotalMoney += (Convert.ToInt64(dr("current_qty")) * Convert.ToInt16(dr("unit_value")))
                        End If
                        If Convert.IsDBNull(dr("max_qty")) = False Then
                            lblFillMoneyTotalBanknoteOut20.Text = "/ " & Convert.ToInt16(dr("max_qty")) & " ใบ"
                            lblFillMoneyMaxBanknote20.Text = Convert.ToInt16(dr("max_qty")).ToString("#,##0")
                        End If
                    Case mdlDeviceInfoENG.DeviceID.BankNoteOut_100
                        If Convert.IsDBNull(dr("current_qty")) = False Then
                            txtFillMoneyBanknoteOut100.Text = Convert.ToInt64(dr("current_qty"))
                            TotalMoney += (Convert.ToInt64(dr("current_qty")) * Convert.ToInt16(dr("unit_value")))
                        End If
                        If Convert.IsDBNull(dr("max_qty")) = False Then
                            lblFillMoneyTotalBanknoteOut100.Text = "/ " & Convert.ToInt16(dr("max_qty")) & " ใบ"
                            lblFillMoneyMaxBanknote100.Text = Convert.ToInt16(dr("max_qty")).ToString("#,##0")
                        End If
                End Select
            Next

            txtFillMoneyTotalMoney.Text = TotalMoney.ToString("#,##0.00")
        End If
        Dt.Dispose()

        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "บันทึกประวัติการเติมเงิน", VendingConfig.SelectForm, False)
        InsertFillMoney(TotalMoney)

        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "ตรวจสอบสิทธิ์การใช้งานหน้าจอเติมเงิน", VendingConfig.SelectForm, False)
        SetFillMoneyAuthorize()
    End Sub

    Private Sub SetFillMoneyAuthorize()
        If StaffConsole.AuthorizeInfo.Rows.Count > 0 Then
            AppScreenList.DefaultView.RowFilter = "id='" & Convert.ToInt16(VendingConfig.SelectForm) & "'"
            If AppScreenList.DefaultView.Count > 0 Then
                lblFillMoneyCheckOutMoney.Visible = False
                btnFillMoneyCheckOutMoney.Visible = False
                txtFillMoneyCoinOut5.Enabled = False
                txtFillMoneyBanknoteOut20.Enabled = False
                txtFillMoneyBanknoteOut100.Enabled = False
                lblFillMoneyAllFull.Visible = False
                btnFillMoneyAllFull.Visible = False
                btnFillMoneyConfirm.Visible = False

                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.FillMoney & " and authorization_name='Edit'"
                If StaffConsole.AuthorizeInfo.DefaultView.Count > 0 Then
                    btnFillMoneyCheckOutMoney.Visible = True
                    lblFillMoneyCheckOutMoney.Visible = True
                    txtFillMoneyCoinOut5.Enabled = True
                    txtFillMoneyBanknoteOut20.Enabled = True
                    txtFillMoneyBanknoteOut100.Enabled = True
                    btnFillMoneyAllFull.Visible = True
                    lblFillMoneyAllFull.Visible = True
                    btnFillMoneyConfirm.Visible = True
                End If
                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""
            End If
            AppScreenList.DefaultView.RowFilter = ""
        End If
        Application.DoEvents()
    End Sub


    Private Function InsertFillMoney(TotalMoneyRemain As Double) As Boolean
        Dim ret As Boolean = False

        Dim CoinInMoney As Double = Convert.ToDouble(Replace(txtFillMoneyCoinInMoney.Text, ",", ""))
        Dim BanknoteInMoney As Double = Convert.ToDouble(Replace(txtFillMoneyBanknoteInMoney.Text, ",", ""))
        Dim Change5Remain As Integer = Convert.ToInt16(Replace(txtFillMoneyCoinOut5.Text, ",", ""))
        Dim Change20Remain As Integer = Convert.ToInt16(Replace(txtFillMoneyBanknoteOut20.Text, ",", ""))
        Dim Change100Remain As Integer = Convert.ToInt16(Replace(txtFillMoneyBanknoteOut100.Text, ",", ""))

        Try
            Dim lnq As New TbLogVendingFillMoneyVendingLinqDB
            lnq.MS_VENDING_ID = VendingData.VendingID
            lnq.COIN_IN_MONEY = CoinInMoney
            lnq.BANKNOTE_IN_MONEY = BanknoteInMoney
            lnq.CHANGE1_REMAIN = 0
            lnq.CHANGE2_REMAIN = 0
            lnq.CHANGE5_REMAIN = Change5Remain
            lnq.CHANGE10_REMAIN = 0
            lnq.CHANGE20_REMAIN = Change20Remain
            lnq.CHANGE50_REMAIN = 0
            lnq.CHANGE100_REMAIN = Change100Remain
            lnq.CHANGE500_REMAIN = 0
            lnq.TOTAL_MONEY_REMAIN = TotalMoneyRemain
            lnq.IS_CONFIRM = "Z"  'ยังไม่มีการกดปุ่ม Confirm หรือ Cancel
            lnq.CONFIRM_CANCEL_DATETIME = DateTime.Now
            lnq.SYNC_TO_SERVER = "N"


            Dim trans As New VendingTransactionDB
            Dim re As ExecuteDataInfo
            If lnq.ID = 0 Then
                re = lnq.InsertData(StaffConsole.Username, trans.Trans)
            Else
                re = lnq.UpdateData(StaffConsole.Username, trans.Trans)
            End If

            If re.IsSuccess = True Then
                _FillMoneyID = lnq.ID
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
                InsertLogStaffConsoleActivity(StaffConsole.TransNo, re.ErrorMessage, VendingConfig.SelectForm, True)
            End If
            lnq = Nothing
            ret = re.IsSuccess
        Catch ex As Exception
            ret = False
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Exception InsertFillMoney : " & ex.Message & vbNewLine & ex.StackTrace, VendingConfig.SelectForm, True)
        End Try
        Return ret
    End Function

    Private Sub lblFillMoneyCancel_Click(sender As Object, e As EventArgs) Handles lblFillMoneyCancel.Click, btnFillMoneyCancel.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่มยกเลิกการเติมเงิน", VendingConfig.SelectForm, False)

        ConfirmFillMoney("N")
        TabContainer1.SelectedTab = tabStockAndHardware
    End Sub

    Private Function ConfirmFillMoney(IsConfirm As String) As TbLogVendingFillMoneyVendingLinqDB
        Dim lnq As New TbLogVendingFillMoneyVendingLinqDB
        Try
            lnq.GetDataByPK(_FillMoneyID, Nothing)
            If lnq.ID > 0 Then
                lnq.IS_CONFIRM = IsConfirm
                lnq.CHECKOUT_DATETIME = DateTime.Now
                lnq.CHANGE1_QTY = 0
                lnq.CHANGE2_QTY = 0
                lnq.CHANGE5_QTY = 0
                lnq.CHANGE10_QTY = 0
                lnq.CHANGE5_QTY = Convert.ToInt16(Replace(txtFillMoneyCoinOut5.Text, ",", ""))
                lnq.CHANGE10_QTY = 0
                lnq.CHANGE20_QTY = Convert.ToInt16(Replace(txtFillMoneyBanknoteOut20.Text, ",", ""))
                lnq.CHANGE50_QTY = 0
                lnq.CHANGE100_QTY = Convert.ToInt16(Replace(txtFillMoneyBanknoteOut100.Text, ",", ""))
                lnq.CHANGE500_QTY = 0
                lnq.SYNC_TO_SERVER = "N"

                Dim trans As New VendingTransactionDB
                Dim re As ExecuteDataInfo = lnq.UpdateData(StaffConsole.Username, trans.Trans)
                If re.IsSuccess = True Then
                    trans.CommitTransaction()
                Else
                    trans.RollbackTransaction()
                    InsertLogStaffConsoleActivity(StaffConsole.TransNo, re.ErrorMessage, VendingConfig.SelectForm, True)
                    ShowDialogErrorMessageSC("Fill Money Fail")
                End If
            End If
        Catch ex As Exception
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Exception ConfirmFillMoney : " & ex.Message & vbNewLine & ex.StackTrace, VendingConfig.SelectForm, True)
            ShowDialogErrorMessageSC("Fill Money  Fail")
        End Try
        Return lnq
    End Function

    Private Sub lblFillMoneyConfirm_Click(sender As Object, e As EventArgs) Handles lblFillMoneyConfirm.Click, btnFillMoneyConfirm.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่มยืนยันการเติมเงิน", VendingConfig.SelectForm, False)

        Dim lnq As TbLogVendingFillMoneyVendingLinqDB = ConfirmFillMoney("Y")
        If lnq.ID > 0 Then
            If lnq.CHECKOUT_RECEIVE_MONEY = "Y" Then
                'ถ้ามีการกดปุ่มนำออกทั้งหมด ให้อัพเดท Stock ของเครือ่งรับเหรียญและรับแบงค์ให้เป็น 0
                UpdateKioskCurrentQty(mdlDeviceInfoENG.DeviceID.CoinIn, 0, 0, True)
                UpdateKioskCurrentQty(mdlDeviceInfoENG.DeviceID.BankNoteIn, 0, 0, True)
            End If

            UpdateKioskCurrentQty(mdlDeviceInfoENG.DeviceID.CoinOut_5, Convert.ToInt16(txtFillMoneyCoinOut5.Text.Replace(",", "")), Convert.ToInt16(txtFillMoneyCoinOut5.Text.Replace(",", "")) * 5, True)
            UpdateKioskCurrentQty(mdlDeviceInfoENG.DeviceID.BankNoteOut_20, Convert.ToInt16(txtFillMoneyBanknoteOut20.Text.Replace(",", "")), Convert.ToInt16(txtFillMoneyBanknoteOut20.Text.Replace(",", "")) * 20, True)
            UpdateKioskCurrentQty(mdlDeviceInfoENG.DeviceID.BankNoteOut_100, Convert.ToInt16(txtFillMoneyBanknoteOut100.Text.Replace(",", "")), Convert.ToInt16(txtFillMoneyBanknoteOut100.Text.Replace(",", "")) * 100, True)
        End If
        lnq = Nothing

        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "เติมเงินในตู้จำนวน " & txtFillMoneyTotalMoney.Text & " บาท", VendingConfig.SelectForm, False)
        ShowDialogErrorMessageSC("Fill Money Success")
        TabContainer1.SelectedTab = tabStockAndHardware
    End Sub

    Private Sub lblFillMoneyCheckOutMoney_Click(sender As Object, e As EventArgs) Handles lblFillMoneyCheckOutMoney.Click, btnFillMoneyCheckOutMoney.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่มนำออกทั้งหมด", VendingConfig.SelectForm, False)

        txtFillMoneyBanknoteInMoney.Text = "0.00"
        txtFillMoneyCoinInMoney.Text = "0.00"
        FillMoneyCalTotalMoney()
        UpdateCheckoutMoney()
    End Sub

    Private Sub UpdateCheckoutMoney()
        Try
            Dim lnq As New TbLogVendingFillMoneyVendingLinqDB
            lnq.GetDataByPK(_FillMoneyID, Nothing)
            If lnq.ID > 0 Then
                lnq.CHECKOUT_RECEIVE_MONEY = "Y"
                lnq.CHECKOUT_DATETIME = DateTime.Now
                lnq.SYNC_TO_SERVER = "N"

                Dim trans As New VendingTransactionDB

                Dim ret As ExecuteDataInfo = lnq.UpdateData(StaffConsole.Username, trans.Trans)
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                Else
                    trans.RollbackTransaction()
                    InsertLogStaffConsoleActivity(StaffConsole.TransNo, ret.ErrorMessage, VendingConfig.SelectForm, True)

                    ShowDialogErrorMessageSC("Update Fail")
                End If
            End If
            lnq = Nothing
        Catch ex As Exception
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Exception UpdateCheckoutMoney : " & ex.Message & vbNewLine & ex.StackTrace, VendingConfig.SelectForm, True)
            ShowDialogErrorMessageSC("Update Fail : Exception " & ex.Message & " " & ex.StackTrace)
        End Try
    End Sub

    Private Sub FillMoneyUpdateFillAllFull()
        Try
            Dim lnq As New TbLogVendingFillMoneyVendingLinqDB
            lnq.GetDataByPK(_FillMoneyID, Nothing)
            If lnq.ID > 0 Then
                lnq.CHECKIN_CHANGE_MONEY = "Y"
                lnq.CHECKIN_DATETIME = DateTime.Now
                lnq.SYNC_TO_SERVER = "N"

                Dim trans As New VendingTransactionDB

                Dim ret As ExecuteDataInfo = lnq.UpdateData(StaffConsole.Username, trans.Trans)
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                Else
                    trans.RollbackTransaction()
                    InsertLogStaffConsoleActivity(StaffConsole.TransNo, ret.ErrorMessage, VendingConfig.SelectForm, True)
                    ShowDialogErrorMessageSC("Update Fail")
                End If
            End If
            lnq = Nothing
        Catch ex As Exception
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Exception FillMoneyUpdateFillAllFull : " & ex.Message & vbNewLine & ex.StackTrace, VendingConfig.SelectForm, True)
            ShowDialogErrorMessageSC("Update Fail : Exception " & ex.Message & " " & ex.StackTrace)
        End Try
    End Sub

    Private Sub lblFillMoneyAllFull_Click(sender As Object, e As EventArgs) Handles lblFillMoneyAllFull.Click, btnFillMoneyAllFull.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่มเติมเต็มจำนวน", VendingConfig.SelectForm, False)

        txtFillMoneyCoinOut5.Text = lblFillMoneyMaxCoin5.Text
        txtFillMoneyBanknoteOut20.Text = lblFillMoneyMaxBanknote20.Text
        txtFillMoneyBanknoteOut100.Text = lblFillMoneyMaxBanknote100.Text

        FillMoneyCalTotalMoney()

        Dim TotalMoney As Double = Convert.ToDouble(txtFillMoneyTotalMoney.Text.Replace(",", ""))
        FillMoneyUpdateFillAllFull()
    End Sub

    Private Sub FillMoneyCalTotalMoney()
        Dim CoinInMoney As Double = Convert.ToDouble(Replace(txtFillMoneyCoinInMoney.Text, ",", ""))
        Dim BanknoteInMoney As Double = Convert.ToDouble(Replace(txtFillMoneyBanknoteInMoney.Text, ",", ""))
        Dim CoinOut5Money As Integer = Convert.ToInt16(Replace(txtFillMoneyCoinOut5.Text, ",", "")) * 5
        Dim BankNote20Money As Integer = Convert.ToInt16(Replace(txtFillMoneyBanknoteOut20.Text, ",", "")) * 20
        Dim BankNote100Money As Integer = Convert.ToInt16(Replace(txtFillMoneyBanknoteOut100.Text, ",", "")) * 100

        Dim TotalMoney As Double = CoinInMoney + BanknoteInMoney + CoinOut5Money + BankNote20Money + BankNote100Money
        txtFillMoneyTotalMoney.Text = TotalMoney.ToString("#,##0.00")
    End Sub

    Private Sub txtMoneyOut_LostFocus(sender As Object, e As EventArgs) Handles txtFillMoneyCoinOut5.LostFocus, txtFillMoneyBanknoteOut20.LostFocus, txtFillMoneyBanknoteOut100.LostFocus
        If DirectCast(sender, TextBox).Text.Trim = "" Then
            DirectCast(sender, TextBox).Text = "0"
        End If
        FillMoneyCalTotalMoney()
    End Sub

    Private Sub txtMoneyOut_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtFillMoneyCoinOut5.KeyPress, txtFillMoneyBanknoteOut20.KeyPress, txtFillMoneyBanknoteOut100.KeyPress
        If (Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57) And e.KeyChar <> Chr(8) Then
            e.Handled = True
        End If
    End Sub
#End Region

#Region "Code Vending Setting"
    Private Sub LoadTabVendingSetting()
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "แสดงหน้าจอ Vending Setting", VendingConfig.SelectForm, False)

        SettingBindDDLNetworkDevice()

        txtSettingVendingID.Text = VendingData.VendingID
        txtSettingTimeOut.Text = VendingConfig.TimeOutSec
        txtSettingMessageTime.Text = VendingConfig.ShowMsgSec
        txtSettingPaymentExtend.Text = VendingConfig.PaymentExtendSec
        txtSettingScreenSaverTime.Text = VendingConfig.ScreenSaverSec
        txtSettingShelfLong.Text = VendingConfig.ShelfLong

        Dim vSleepTime() As String = Split(VendingConfig.SleepTime, ":")
        If vSleepTime.Length = 2 Then
            txtSettingSleepTimeH.Text = vSleepTime(0)
            txtSettingSleepTimeM.Text = vSleepTime(1)
            txtSettingSleepDurationMin.Text = VendingConfig.SleepDuration
        End If

        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "ตรวจสอบสิทธิ์การใช้งานหน้าจอ Vending Setting", VendingConfig.SelectForm, False)
        SetVendingSettingAuthorize()
    End Sub

    Private Sub SetVendingSettingAuthorize()
        If StaffConsole.AuthorizeInfo.Rows.Count > 0 Then
            AppScreenList.DefaultView.RowFilter = "id='" & Convert.ToInt16(VendingConfig.SelectForm) & "'"
            If AppScreenList.DefaultView.Count > 0 Then
                cbSettingNetworkDevice.Enabled = False
                txtSettingTimeOut.Enabled = False
                txtSettingMessageTime.Enabled = False
                txtSettingPaymentExtend.Enabled = False
                txtSettingScreenSaverTime.Enabled = False
                txtSettingShelfLong.Enabled = False
                txtSettingSleepTimeH.Enabled = False
                txtSettingSleepTimeM.Enabled = False
                txtSettingSleepDurationMin.Enabled = False
                btnSettingSave.Visible = False

                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.VendingSetting & " and authorization_name='Edit'"
                If StaffConsole.AuthorizeInfo.DefaultView.Count > 0 Then
                    cbSettingNetworkDevice.Enabled = True
                    txtSettingTimeOut.Enabled = True
                    txtSettingMessageTime.Enabled = True
                    txtSettingPaymentExtend.Enabled = True
                    txtSettingScreenSaverTime.Enabled = True
                    txtSettingShelfLong.Enabled = True
                    txtSettingSleepTimeH.Enabled = True
                    txtSettingSleepTimeM.Enabled = True
                    txtSettingSleepDurationMin.Enabled = True
                    btnSettingSave.Visible = True
                End If
                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""
            End If
            AppScreenList.DefaultView.RowFilter = ""
        End If
        Application.DoEvents()
    End Sub


    Private Sub SettingBindDDLNetworkDevice()
        Dim dt As New DataTable
        Dim adapters() As NetworkInterface = NetworkInterface.GetAllNetworkInterfaces()
        If adapters.Length > 0 Then
            For Each adp As NetworkInterface In adapters
                cbSettingNetworkDevice.Items.Add(adp.Description)
            Next
        End If

        cbSettingNetworkDevice.SelectedIndex = cbSettingNetworkDevice.FindString(GetCardLanDesc())

        'Network Information
        SettingNetworkInformation()
    End Sub

    Private Sub SettingNetworkInformation()
        txtSettingIPAddress.Text = ""
        txtSettingMacAddress.Text = ""

        Dim mc As New ManagementClass("Win32_NetworkAdapterConfiguration")
        Dim moc As ManagementObjectCollection = mc.GetInstances()
        For Each mo As ManagementObject In moc
            If CStr(mo("Description")).Trim = cbSettingNetworkDevice.Text Then
                If mo("IPEnabled") = True Then
                    txtSettingIPAddress.Text = mo("IPAddress")(0)
                    txtSettingMacAddress.Text = mo("MacAddress").ToString().Replace(":", "-")
                Else
                    InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Cannot found Network Device " + cbSettingNetworkDevice.Text, VendingConfig.SelectForm, True)
                End If
            End If
            mo.Dispose()
        Next
    End Sub

    Private Sub cbSettingNetworkDevice_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cbSettingNetworkDevice.SelectionChangeCommitted
        If cbSettingNetworkDevice.Text.Trim <> "" Then
            SettingNetworkInformation()
        End If
    End Sub

    Private Sub txtSettingSleepTimeH_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSettingSleepTimeH.KeyPress
        Dim txt As TextBox = DirectCast(sender, TextBox)
        If txt.Text.Length = 0 Then
            Select Case e.KeyChar.ToString
                Case "0", "1", "2", vbBack
                Case Else
                    e.Handled = True
            End Select
        ElseIf txt.Text.Length = 1 Then
            If txt.Text.Trim = "2" Then
                Select Case e.KeyChar.ToString
                    Case "0", "1", "2", "3", vbBack
                    Case Else
                        e.Handled = True
                End Select
            Else
                Select Case e.KeyChar.ToString
                    Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", vbBack
                    Case Else
                        e.Handled = True
                End Select
            End If
        End If

    End Sub

    Private Sub txtSettingSleepTimeM_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSettingSleepTimeM.KeyPress
        Dim txt As TextBox = DirectCast(sender, TextBox)
        If txt.Text.Length = 0 Then
            Select Case e.KeyChar.ToString
                Case "0", "1", "2", "3", "4", "5", vbBack
                Case Else
                    e.Handled = True
            End Select
        ElseIf txt.Text.Length = 1 Then
            Select Case e.KeyChar.ToString
                Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", vbBack
                Case Else
                    e.Handled = True
            End Select
        End If
    End Sub

    Private Sub txtSettingSleepTime_LostFocus(sender As Object, e As EventArgs) Handles txtSettingSleepTimeH.LostFocus, txtSettingSleepTimeM.LostFocus
        Dim txt As TextBox = DirectCast(sender, TextBox)
        If txt.Text.Length = 1 Then
            txt.Text = "0" & txt.Text
        End If
    End Sub

    Private Sub lblSettingSave_Click(sender As Object, e As EventArgs) Handles lblSettingSave.Click, btnSettingSave.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่มบันทึก", VendingConfig.SelectForm, False)

        If txtSettingVendingID.Text = "" Then
            ShowDialogErrorMessageSC("Please enter Vending Machine ID")
            Exit Sub
        End If
        If cbSettingNetworkDevice.Text = "" Then
            ShowDialogErrorMessageSC("Please select Network Device")
            Exit Sub
        End If
        If txtSettingIPAddress.Text = "" Then
            ShowDialogErrorMessageSC("Please enter IP Address")
            Exit Sub
        End If
        If txtSettingMacAddress.Text = "" Then
            ShowDialogErrorMessageSC("Please enger Mac Address")
            Exit Sub
        End If

        If txtSettingScreenSaverTime.Text = "" Then
            ShowDialogErrorMessageSC("Please enter Screen Saver")
            Exit Sub
        End If
        If txtSettingTimeOut.Text = "" Then
            ShowDialogErrorMessageSC("Please enter Vending Machine Time Out")
            Exit Sub
        End If
        If txtSettingMessageTime.Text = "" Then
            ShowDialogErrorMessageSC("Please enter Vending Machine Message Time")
            Exit Sub
        End If
        If txtSettingPaymentExtend.Text = "" Then
            ShowDialogErrorMessageSC("Please enter Payment Extend")
            Exit Sub
        End If

        If txtSettingShelfLong.Text.Trim = "" Then
            ShowDialogErrorMessageSC("Please enter Shelf Long")
            Exit Sub
        End If

        If Convert.ToInt16(txtSettingShelfLong.Text) <= 0 Then
            ShowDialogErrorMessageSC("Shelf Long not less than zero")
            Exit Sub
        End If

        If txtSettingSleepTimeH.Text.Trim = "" Then
            ShowDialogErrorMessageSC("Please enter Sleep Time")
            txtSettingSleepTimeH.Focus()
            Exit Sub
        End If
        If txtSettingSleepTimeM.Text.Trim = "" Then
            ShowDialogErrorMessageSC("Please enter Sleep Time")
            txtSettingSleepTimeM.Focus()
            Exit Sub
        End If
        If txtSettingSleepDurationMin.Text.Trim = "" Then
            ShowDialogErrorMessageSC("Please enter Sleep Duration")
            txtSettingSleepDurationMin.Focus()
            Exit Sub
        End If

        Try
            Dim ini As New Kiosk_VM.Org.Mentalis.Files.IniReader(INIFileNameLAN)
            ini.Section = "Setting"
            ini.Write("CardLanDesc", cbSettingNetworkDevice.Text)
            ini = Nothing

            Dim lnq As New CfVendingSysconfigVendingLinqDB
            lnq.ChkDataByMS_VENDING_ID(VendingData.VendingID, Nothing)
            If lnq.ID > 0 Then
                lnq.SCREEN_SAVER_SEC = txtSettingScreenSaverTime.Text
                lnq.TIME_OUT_SEC = txtSettingTimeOut.Text
                lnq.SHOW_MSG_SEC = txtSettingMessageTime.Text
                lnq.PAYMENT_EXTEND_SEC = txtSettingPaymentExtend.Text
                lnq.SHELF_LONG = txtSettingShelfLong.Text
                lnq.SLEEP_TIME = txtSettingSleepTimeH.Text.PadLeft(2, "0") & ":" & txtSettingSleepTimeM.Text.PadLeft(2, "0")
                lnq.SLEEP_DURATION = txtSettingSleepDurationMin.Text
                lnq.SYNC_TO_VENDING = "Y"
                lnq.SYNC_TO_SERVER = "N"

                Dim trans As New VendingTransactionDB
                Dim ret As ExecuteDataInfo = lnq.UpdateData(StaffConsole.Username, trans.Trans)
                If ret.IsSuccess = True Then
                    Dim kdLnq As New MsVendingDeviceVendingLinqDB
                    kdLnq.ChkDataByMS_DEVICE_ID_MS_VENDING_ID(mdlDeviceInfoENG.DeviceID.NetworkConnection, txtSettingVendingID.Text, Nothing)
                    If kdLnq.ID > 0 Then
                        kdLnq.DRIVER_NAME1 = cbSettingNetworkDevice.Text
                        kdLnq.SYNC_TO_VENDING = "Y"
                        kdLnq.SYNC_TO_SERVER = "N"

                        ret = kdLnq.UpdateData(StaffConsole.Username, trans.Trans)
                        If ret.IsSuccess = True Then
                            trans.CommitTransaction()

                            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "บันทึกข้อมูล Vending Config", VendingConfig.SelectForm, False)
                            GetVendingConfig()
                            ShowDialogErrorMessageSC("Save Success")

                            TabContainer1.SelectedTab = tabStockAndHardware
                        Else
                            trans.RollbackTransaction()
                            InsertLogStaffConsoleActivity(StaffConsole.TransNo, ret.ErrorMessage, VendingConfig.SelectForm, True)
                            ShowDialogErrorMessageSC(ret.ErrorMessage)
                        End If
                    End If
                    kdLnq = Nothing
                Else
                    trans.RollbackTransaction()
                    InsertLogStaffConsoleActivity(StaffConsole.TransNo, ret.ErrorMessage, VendingConfig.SelectForm, True)
                    ShowDialogErrorMessageSC(ret.ErrorMessage)
                End If
            End If
            lnq = Nothing
        Catch ex As Exception
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Exception lblSettingSave_Click: " & ex.Message & " " & ex.StackTrace, VendingConfig.SelectForm, True)
            ShowDialogErrorMessageSC(ex.Message & " " & ex.StackTrace)
        End Try
    End Sub

    Private Sub lblSettingCancel_Click(sender As Object, e As EventArgs) Handles lblSettingCancel.Click, btnSettingCancel.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่มยกเลิก", VendingConfig.SelectForm, False)
        TabContainer1.SelectedTab = tabStockAndHardware
    End Sub

    Private Sub txtSettingTextInt_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSettingShelfLong.KeyPress, txtSettingMessageTime.KeyPress, txtSettingPaymentExtend.KeyPress, txtSettingScreenSaverTime.KeyPress, txtSettingSleepDurationMin.KeyPress, txtSettingTimeOut.KeyPress
        If (e.KeyChar < "0" Or e.KeyChar > "9") And Asc(e.KeyChar) <> 8 Then
            e.Handled = True
        End If
    End Sub


#End Region

#Region "Code Device Setting"



    Private Sub SetDeviceSettingAuthorize()
        If StaffConsole.AuthorizeInfo.Rows.Count > 0 Then
            AppScreenList.DefaultView.RowFilter = "id='" & Convert.ToInt16(VendingConfig.SelectForm) & "'"
            If AppScreenList.DefaultView.Count > 0 Then


                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.DeviceSetting & " and authorization_name='Edit'"
                If StaffConsole.AuthorizeInfo.DefaultView.Count > 0 Then

                End If
                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""
            End If
            AppScreenList.DefaultView.RowFilter = ""
        End If
        Application.DoEvents()
    End Sub
#End Region

#Region "Code Setup Shelf"

    Public Sub LoadTabSetupShelf()
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "แสดงหน้าจอ Setup Shelf", VendingConfig.SelectForm, False)
        SetupShelfLayout(pnlSetupShelfLayout)

        SetSetupShelfAuthorize()
        Application.DoEvents()
    End Sub

    Private Sub SetSetupShelfAuthorize()
        If StaffConsole.AuthorizeInfo.Rows.Count > 0 Then
            AppScreenList.DefaultView.RowFilter = "id='" & Convert.ToInt16(VendingConfig.SelectForm) & "'"
            If AppScreenList.DefaultView.Count > 0 Then
                btnAddShelf.Visible = False

                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = "ms_functional_id=" & StaffConsoleFunctionalID.SetupShelf & " and authorization_name='Edit'"
                If StaffConsole.AuthorizeInfo.DefaultView.Count > 0 Then
                    btnAddShelf.Visible = True

                    'For Each ctl As ucProductShelf In pnlSetupShelfLayout.Controls.OfType(Of ucProductShelf)
                    '    AddHandler ctl.ProductShelfClick, AddressOf pdShelf_Click
                    'Next
                End If
                StaffConsole.AuthorizeInfo.DefaultView.RowFilter = ""
            End If
            AppScreenList.DefaultView.RowFilter = ""
        End If
        Application.DoEvents()
    End Sub

    Private Sub lblAddShelf_Click(sender As Object, e As EventArgs) Handles lblAddShelf.Click, btnAddShelf.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่ม Add Shelf", VendingConfig.SelectForm, False)
        Dim frm As New frmSC_SetupShelf
        frm.ShowDialog()

        If frm.ShowDialog() = DialogResult.OK Then
            SetupShelfLayout(pnlSetupShelfLayout)
            SetSetupShelfAuthorize()
            Application.DoEvents()
        End If
    End Sub

    Private Sub pdShelf_Click(sender As Object, e As EventArgs)
        Dim pd As ucProductShelf = DirectCast(sender, ucProductShelf)

        Dim frm As New frmSC_SetupShelf
        frm.StartPosition = FormStartPosition.CenterParent
        frm.FillInData(pd.lblShelfID.Text)
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่มแก้ไข Shelf", VendingConfig.SelectForm, False)
        If frm.ShowDialog() = DialogResult.OK Then
            SetupShelfLayout(pnlSetupShelfLayout)
            Application.DoEvents()
        End If

    End Sub

    Private Sub SetupShelfLayout(pnl As Panel)
        pnl.Controls.Clear()

        Dim sql As String = "select id, shelf_id, position_row, position_column, position_x, position_y, width " & Environment.NewLine
        sql += " From MS_VENDING_SHELF" & Environment.NewLine
        sql += " where ms_vending_id=@_VENDING_ID" & Environment.NewLine
        sql += " and active_status='Y'" & Environment.NewLine
        sql += " order by position_row, position_column" & Environment.NewLine

        Dim p(1) As SqlParameter
        p(0) = VendingDB.SetBigInt("@_VENDING_ID", VendingData.VendingID)

        Dim dt As DataTable = VendingDB.ExecuteTable(sql, p)
        If dt.Rows.Count > 0 Then
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Load รายการ Vending Shelf", VendingConfig.SelectForm, False)

            Dim ShelfRowImg As Image = Image.FromFile(Application.StartupPath & "\Background\bgShelfRow.jpg")
            'Dim ShelfColImg As Image = Image.FromFile(Application.StartupPath & "\Background\bgShelfCol.jpg")

            Dim RowQty As Integer = Convert.ToInt16(dt.Rows(dt.Rows.Count - 1)("position_row"))
            Dim ShelfHeight As Integer = ShelfRowImg.Height
            Dim ShelfDistanct As Integer = Math.Ceiling(pnl.Height / (RowQty + 1))
            Dim ShelfTop As Integer = ShelfDistanct

            For i As Integer = 1 To RowQty
                'Add Shelf
                Dim pbShelf As New PictureBox
                pbShelf.Width = pnl.Width
                pbShelf.Height = ShelfHeight
                pbShelf.Top = ShelfTop
                pbShelf.Left = 0
                pbShelf.Image = ShelfRowImg
                pbShelf.SizeMode = PictureBoxSizeMode.StretchImage
                pnl.Controls.Add(pbShelf)

                dt.DefaultView.RowFilter = "position_row=" & i
                If dt.DefaultView.Count > 0 Then
                    Dim cDt As New DataTable
                    cDt = dt.DefaultView.ToTable().Copy

                    Dim SumColWidth As Integer = Convert.ToInt16(cDt.Compute("sum(width)", ""))

                    Dim ColDistanct As Integer = CalColDistance(SumColWidth, cDt.Rows.Count, 50, pnl.Width) 'Math.Floor((pbShelf.Width - 20) / cDt.Rows.Count)
                    Dim ColLeft As Integer = pbShelf.Left + ((50 * pnl.Width) / VendingConfig.ShelfLong)
                    Dim ColTop As Integer = pbShelf.Top

                    'Add Shelf Column
                    For j As Integer = 0 To cDt.Rows.Count - 1
                        Dim ColWidth As Integer = 0

                        'กรณีแสดงข้อมูลในหน้าจอ Setting ให้แสดงในกรณีที่ชั้นวางนั้นไม่มีสินค้าด้วย
                        Dim ucP As New ucProductShelf
                        ucP.Top = ColTop - ucP.Height
                        ucP.Left = ColLeft
                        ucP.Width = (Convert.ToInt16(cDt.Rows(j)("width")) * pnl.Width) / VendingConfig.ShelfLong
                        ucP.lblShelfID.Text = cDt.Rows(j)("shelf_id")
                        ucP.lblProductID.Text = 0
                        ucP.lblPosition.Text = cDt.Rows(j)("position_x") & "," & cDt.Rows(j)("position_y")
                        ucP.lblProductCode.Text = ""
                        ucP.lblProductQty.Text = 0
                        ucP.Parent = pnl
                        ColWidth = ucP.Width
                        AddHandler ucP.ProductShelfClick, AddressOf pdShelf_Click

                        pnl.Controls.Add(ucP)

                        ColLeft += ColDistanct + ColWidth
                    Next

                    Application.DoEvents()
                    ShelfTop += (ShelfDistanct + ShelfHeight)
                End If
                dt.DefaultView.RowFilter = ""
            Next
        Else
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "ไม่พบรายการ Vending Shelf", VendingConfig.SelectForm, False)
        End If
        dt.Dispose()
    End Sub


#End Region

#Region "Code Setup Product"
    Private Sub LoadTabSetupProduct()
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "แสดงหน้าจอ Setup Product", VendingConfig.SelectForm, False)

        SetupProductLayout(pnlSetupProduct)
    End Sub

    Private Sub SetupProductLayout(pnl As Panel)
        pnl.Controls.Clear()

        Dim sql As String = "select id, shelf_id, position_row, position_column, position_x, position_y, width " & Environment.NewLine
        sql += " From MS_VENDING_SHELF" & Environment.NewLine
        sql += " where ms_vending_id=@_VENDING_ID" & Environment.NewLine
        sql += " and active_status='Y'" & Environment.NewLine
        sql += " order by position_row, position_column" & Environment.NewLine

        Dim p(1) As SqlParameter
        p(0) = VendingDB.SetBigInt("@_VENDING_ID", VendingData.VendingID)

        Dim dt As DataTable = VendingDB.ExecuteTable(sql, p)
        If dt.Rows.Count > 0 Then
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "Load รายการ Vending Shelf", VendingConfig.SelectForm, False)

            Dim ShelfRowImg As Image = Image.FromFile(Application.StartupPath & "\Background\bgShelfRow.jpg")

            Dim RowQty As Integer = Convert.ToInt16(dt.Rows(dt.Rows.Count - 1)("position_row"))
            Dim ShelfHeight As Integer = ShelfRowImg.Height
            Dim ShelfDistanct As Integer = Math.Ceiling(pnl.Height / (RowQty + 1))
            Dim ShelfTop As Integer = ShelfDistanct


            'Find Product on this Vending Machine
            sql = "select ps.ms_product_id, ps.shelf_id, ps.product_qty "
            sql += " from MS_PRODUCT_SHELF ps "
            'sql += " where  ps.shelf_id=@_SHELF_ID"
            'Dim parm(1) As SqlParameter
            'parm(0) = VendingDB.SetInt("@_SHELF_ID", cDt.Rows(j)("shelf_id"))
            Dim pDt As DataTable = VendingDB.ExecuteTable(sql)

            For i As Integer = 1 To RowQty
                'Add Shelf
                Dim pbShelf As New PictureBox
                pbShelf.Width = pnl.Width
                pbShelf.Height = ShelfHeight
                pbShelf.Top = ShelfTop
                pbShelf.Left = 0
                pbShelf.Image = ShelfRowImg
                pbShelf.SizeMode = PictureBoxSizeMode.StretchImage
                pnl.Controls.Add(pbShelf)

                dt.DefaultView.RowFilter = "position_row=" & i
                If dt.DefaultView.Count > 0 Then
                    Dim cDt As New DataTable
                    cDt = dt.DefaultView.ToTable().Copy

                    Dim SumColWidth As Integer = Convert.ToInt16(cDt.Compute("sum(width)", ""))

                    Dim ColDistanct As Integer = CalColDistance(SumColWidth, cDt.Rows.Count, 50, pnl.Width) 'Math.Floor((pbShelf.Width - 20) / cDt.Rows.Count)
                    Dim ColLeft As Integer = pbShelf.Left + ((50 * pnl.Width) / VendingConfig.ShelfLong)
                    Dim ColTop As Integer = pbShelf.Top

                    'Add Shelf Column
                    For j As Integer = 0 To cDt.Rows.Count - 1
                        Dim ColWidth As Integer = 0
                        pDt.DefaultView.RowFilter = "shelf_id=" & cDt.Rows(j)("shelf_id")
                        If pDt.DefaultView.Count > 0 Then
                            Dim pDr As DataRowView = pDt.DefaultView(0)
                            'จะต้องมีสินค้าอยู่บนชั้นถึงจะแสดงภาพสินค้า
                            'Add Product To Shelf Column
                            ProductMasterList.DefaultView.RowFilter = "ms_product_id= " & pDr("ms_product_id")
                            If ProductMasterList.DefaultView.Count > 0 Then
                                Dim pmDrv As DataRowView = ProductMasterList.DefaultView(0)

                                Using pBytes As New IO.MemoryStream(CType(pmDrv("product_image_small"), Byte()))
                                    Dim pbImg As Image = Image.FromStream(pBytes)
                                    Dim ucP As New ucProductShelf
                                    ucP.Top = ColTop - ucP.Height
                                    ucP.Left = ColLeft
                                    ucP.Width = (Convert.ToInt16(cDt.Rows(j)("width")) * pnl.Width) / VendingConfig.ShelfLong
                                    ucP.lblShelfID.Text = cDt.Rows(j)("shelf_id")
                                    ucP.lblProductID.Text = pmDrv("ms_product_id")
                                    ucP.lblPosition.Text = cDt.Rows(j)("position_x") & "," & cDt.Rows(j)("position_y")
                                    ucP.lblProductCode.Text = pmDrv("product_code")
                                    ucP.lblProductQty.Text = pDr("product_qty")
                                    ucP.BackgroundImage = pbImg
                                    ucP.BackgroundImageLayout = ImageLayout.Stretch
                                    ucP.lblProductCode.Visible = False
                                    ucP.lblPosition.Visible = False
                                    ucP.lblProductQty.Visible = False
                                    ucP.Parent = pnl
                                    ColWidth = ucP.Width
                                    AddHandler ucP.ProductShelfClick, AddressOf pdSetupProduct_Click
                                End Using
                            End If
                        Else
                            'กรณีแสดงข้อมูลในหน้าจอ Setting ให้แสดงในกรณีที่ชั้นวางนั้นไม่มีสินค้าด้วย
                            Dim ucP As New ucProductShelf
                            ucP.Top = ColTop - ucP.Height
                            ucP.Left = ColLeft
                            ucP.Width = (Convert.ToInt16(cDt.Rows(j)("width")) * pnl.Width) / VendingConfig.ShelfLong
                            ucP.lblShelfID.Text = cDt.Rows(j)("shelf_id")
                            ucP.lblProductID.Text = 0
                            ucP.lblPosition.Text = cDt.Rows(j)("position_x") & "," & cDt.Rows(j)("position_y")
                            ucP.lblProductCode.Text = ""
                            ucP.lblProductQty.Text = 0
                            ucP.Parent = pnl
                            ColWidth = ucP.Width
                            AddHandler ucP.ProductShelfClick, AddressOf pdSetupProduct_Click

                            pnl.Controls.Add(ucP)
                        End If
                        'pDt.Dispose()

                        pDt.DefaultView.RowFilter = ""

                        ColLeft += ColDistanct + ColWidth
                    Next

                    Application.DoEvents()
                    ShelfTop += (ShelfDistanct + ShelfHeight)
                End If
                dt.DefaultView.RowFilter = ""
            Next

            pDt.Dispose()
        Else
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, "ไม่พบรายการ Vending Shelf", VendingConfig.SelectForm, False)
        End If

        dt.Dispose()
    End Sub

    Private Sub pdSetupProduct_Click(sender As Object, e As EventArgs)
        Dim pd As ucProductShelf = DirectCast(sender, ucProductShelf)

        Dim frm As New frmSC_SetupProduct
        frm.FillInData(pd.lblShelfID.Text, pd.lblProductID.Text)
        If frm.ShowDialog() = DialogResult.OK Then
            SetupProductLayout(pnlSetupProduct)
            Application.DoEvents()
        End If
    End Sub


#End Region

#Region "Code Device Setting"
    Private Sub LoadDeviceSetting()
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "แสดงหน้าจอ Device Setting", VendingConfig.SelectForm, False)
        DeviceSettingBindDDLComport()

        DeviceSettingBindDDLPrinter()

        DeviceSettingBindDGVDeviceMoneyOutList(mdlDeviceInfoENG.DeviceType.BanknoteOut, dgvDeviceSettingBanknoteOutList, "colComPort")
        DeviceSettingBindDGVDeviceMoneyOutList(mdlDeviceInfoENG.DeviceType.CoinOut, dgvDeviceSettingCoinOutList, "colCoinComPort")

        DeviceSettingSetDefaultSetting()
    End Sub

    Private Sub DeviceSettingBindDDLPrinter()
        Dim pkInstalledPrinters As String = ""
        For i = 0 To PrinterSettings.InstalledPrinters.Count - 1
            pkInstalledPrinters = PrinterSettings.InstalledPrinters.Item(i)
            cbDeviceSettingPrinterName.Items.Add(pkInstalledPrinters)
        Next
    End Sub

    Private Sub DeviceSettingBindDGVDeviceMoneyOutList(vDeviceTypeID As Long, dgv As DataGridView, colComPort As String)

        Dim comDT As New DataTable
        comDT.Columns.Add("comport_vid")
        comDT.Columns.Add("port_display")

        Dim dr As DataRow = comDT.NewRow
        dr("comport_vid") = ""
        dr("port_display") = ""
        comDT.Rows.Add(dr)
        For Each sp As String In My.Computer.Ports.SerialPortNames
            dr = comDT.NewRow
            dr("comport_vid") = sp
            dr("port_display") = sp
            comDT.Rows.Add(dr)
        Next

        Dim colPort As DataGridViewComboBoxColumn = DirectCast(dgv.Columns(colComPort), DataGridViewComboBoxColumn)
        colPort.DataSource = comDT
        colPort.DisplayMember = "port_display"
        colPort.ValueMember = "comport_vid"


        DeviceInfoList.DefaultView.RowFilter = "unit_value>0 and device_type_id=" & vDeviceTypeID & " and device_active_status='Y'"
        Dim dt As New DataTable
        dt = DeviceInfoList.DefaultView.ToTable.Copy
        If dt.Rows.Count > 0 Then
            dgv.AutoGenerateColumns = False
            dgv.DataSource = dt
        End If
        DeviceInfoList.DefaultView.RowFilter = ""
    End Sub

    Private Sub DeviceSettingBindDDLComport()
        cbDeviceSettingBanknoteIn.Items.Clear()
        cbDeviceSettingBanknoteIn.Items.Add("")

        cbDeviceSettingCoinIn.Items.Clear()
        cbDeviceSettingCoinIn.Items.Add("")

        For Each sp As String In My.Computer.Ports.SerialPortNames
            cbDeviceSettingBanknoteIn.Items.Add(sp)
            cbDeviceSettingCoinIn.Items.Add(sp)
        Next
    End Sub

    Private Sub dgvDeviceSettingBanknoteOutList_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvDeviceSettingBanknoteOutList.DataError

    End Sub

    Private Sub dgvDeviceSettingCoinOutList_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvDeviceSettingCoinOutList.DataError

    End Sub

    Private Sub DeviceSettingSave_Click(sender As Object, e As EventArgs) Handles lblDeviceSettingSave.Click, btnDeviceSettingSave.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่มบันทึก", VendingConfig.SelectForm, False)

        UpdateDeviceSettingByDeviceID(DeviceID.Printer, "", cbDeviceSettingPrinterName.Text, "")
        UpdateDeviceSettingByDeviceID(mdlDeviceInfoENG.DeviceID.BankNoteIn, cbDeviceSettingBanknoteIn.Text, "", "")
        UpdateDeviceSettingByDeviceID(mdlDeviceInfoENG.DeviceID.CoinIn, cbDeviceSettingCoinIn.Text, "", "")

        For Each dgr As DataGridViewRow In dgvDeviceSettingBanknoteOutList.Rows
            Dim DeviceID As Long = dgr.Cells("colBanknoteDeviceID").Value
            Dim ComportName As String = ""
            If dgr.Cells("colComPort") IsNot Nothing Then
                Dim cb As DataGridViewComboBoxCell = DirectCast(dgvDeviceSettingBanknoteOutList("colComPort", dgr.Index), DataGridViewComboBoxCell)
                ComportName = cb.EditedFormattedValue

                UpdateDeviceSettingByDeviceID(DeviceID, ComportName, "", "")
            End If
        Next

        For Each dgr As DataGridViewRow In dgvDeviceSettingCoinOutList.Rows
            Dim DeviceID As Long = dgr.Cells("colCoinDeviceID").Value
            Dim ComportName As String = ""
            If dgr.Cells("colCoinComPort") IsNot Nothing Then
                Dim cb As DataGridViewComboBoxCell = DirectCast(dgvDeviceSettingCoinOutList("colCoinComPort", dgr.Index), DataGridViewComboBoxCell)
                ComportName = cb.EditedFormattedValue

                UpdateDeviceSettingByDeviceID(DeviceID, ComportName, "", "")
            End If
        Next

        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "บันทึกข้อมูล Device Setting", VendingConfig.SelectForm, False)
        ShowDialogErrorMessageSC("Save Success")

        Try
            Dim WS As New VDMWebService.ATBVendingWebservice
            WS.Url = VendingConfig.WebserviceVDMURL
            WS.Timeout = 10000

            SetListDeviceInfo(WS)

            WS.Dispose()
        Catch ex As Exception

        End Try

        UpdateAllDeviceStatusByComPort()
        UpdateAllDeviceStatusByUsbPort()

        TabContainer1.SelectedTab = tabStockAndHardware
    End Sub

    Private Function UpdateDeviceSettingByDeviceID(vDeviceID As mdlDeviceInfoENG.DeviceID, ComportVID As String, DriverName1 As String, DriverName2 As String) As Boolean
        Dim ret As Boolean = False
        Try
            Dim lnq As New MsVendingDeviceVendingLinqDB
            lnq.ChkDataByMS_DEVICE_ID_MS_VENDING_ID(vDeviceID, VendingData.VendingID, Nothing)
            If lnq.ID > 0 Then
                lnq.COMPORT_VID = ComportVID
                lnq.DRIVER_NAME1 = DriverName1
                lnq.DRIVER_NAME2 = DriverName2
                lnq.SYNC_TO_SERVER = "N"
                lnq.SYNC_TO_VENDING = "Y"

                Dim trans As New VendingTransactionDB
                Dim re As ExecuteDataInfo = lnq.UpdateData(StaffConsole.Username, trans.Trans)
                If re.IsSuccess = True Then
                    trans.CommitTransaction()
                    ret = True
                Else
                    trans.RollbackTransaction()

                    Dim _err As String = re.ErrorMessage
                    _err += "&vDeviceID=" & vDeviceID
                    _err += "&ComportVID=" & ComportVID
                    _err += "&DriverName1=" & DriverName1
                    _err += "&DriverName2=" & DriverName2
                    InsertLogStaffConsoleActivity(StaffConsole.TransNo, _err, VendingConfig.SelectForm, True)
                End If
            End If
            lnq = Nothing
        Catch ex As Exception
            Dim _err As String = "Exception : " & ex.Message & vbNewLine & ex.StackTrace
            _err += "&vDeviceID=" & vDeviceID
            _err += "&ComportVID=" & ComportVID
            _err += "&DriverName1=" & DriverName1
            _err += "&DriverName2=" & DriverName2
            InsertLogStaffConsoleActivity(StaffConsole.TransNo, _err, VendingConfig.SelectForm, True)
        End Try

        Return ret
    End Function

    Private Sub DeviceSettingCancel_Click(sender As Object, e As EventArgs) Handles lblDeviceSettingCancel.Click, btnDeviceSettingCancel.Click
        InsertLogStaffConsoleActivity(StaffConsole.TransNo, "คลิกปุ่มยกเลิก", VendingConfig.SelectForm, False)
        TabContainer1.SelectedTab = tabStockAndHardware
    End Sub

    Private Sub DeviceSettingSetDefaultSetting()
        For i As Integer = 0 To DeviceInfoList.Rows.Count - 1
            Dim dDr As DataRow = DeviceInfoList.Rows(i)

            If dDr("type_active_status") = "Y" AndAlso dDr("device_active_status") = "Y" Then
                Select Case Convert.ToInt16(dDr("device_id"))
                    Case DeviceID.BankNoteIn
                        If Convert.IsDBNull(dDr("comport_vid")) = False Then
                            cbDeviceSettingBanknoteIn.SelectedIndex = cbDeviceSettingBanknoteIn.FindString(dDr("comport_vid"))
                        End If
                    Case DeviceID.CoinIn
                        If Convert.IsDBNull(dDr("comport_vid")) = False Then
                            cbDeviceSettingCoinIn.SelectedIndex = cbDeviceSettingCoinIn.FindString(dDr("comport_vid"))
                        End If
                    Case DeviceID.Printer
                        If Convert.IsDBNull(dDr("driver_name1")) = False Then
                            cbDeviceSettingPrinterName.SelectedIndex = cbDeviceSettingPrinterName.FindString(dDr("driver_name1"))
                        End If

                    Case Else
                        Select Case Convert.ToInt16(dDr("device_type_id"))
                            Case DeviceType.BanknoteOut
                                DeviceSettingBindDGVDeviceMoneyOutList(mdlDeviceInfoENG.DeviceType.BanknoteOut, dgvDeviceSettingBanknoteOutList, "colComPort")
                            Case DeviceType.CoinOut

                                DeviceSettingBindDGVDeviceMoneyOutList(mdlDeviceInfoENG.DeviceType.CoinOut, dgvDeviceSettingCoinOutList, "colCoinComPort")
                        End Select
                End Select
            End If
        Next
    End Sub

#End Region
End Class