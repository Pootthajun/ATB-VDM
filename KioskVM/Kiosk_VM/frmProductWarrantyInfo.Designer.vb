﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmProductWarrantyInfo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.pbBackground = New System.Windows.Forms.PictureBox()
        Me.pbClose = New System.Windows.Forms.PictureBox()
        Me.lblWarranty = New System.Windows.Forms.Label()
        Me.TimerTimeout = New System.Windows.Forms.Timer(Me.components)
        Me.btnClose = New System.Windows.Forms.Label()
        Me.lblCaptionHome = New System.Windows.Forms.Label()
        Me.lblCaptionSelectProduct = New System.Windows.Forms.Label()
        Me.lblCaptionPayment = New System.Windows.Forms.Label()
        Me.lblCaptionDelivered = New System.Windows.Forms.Label()
        Me.lblCaptionWarrantyInfo = New System.Windows.Forms.Label()
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pbBackground
        '
        Me.pbBackground.BackColor = System.Drawing.Color.Transparent
        Me.pbBackground.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbBackground.Location = New System.Drawing.Point(0, 0)
        Me.pbBackground.Name = "pbBackground"
        Me.pbBackground.Size = New System.Drawing.Size(768, 1366)
        Me.pbBackground.TabIndex = 5
        Me.pbBackground.TabStop = False
        '
        'pbClose
        '
        Me.pbClose.BackColor = System.Drawing.Color.Transparent
        Me.pbClose.Location = New System.Drawing.Point(226, 906)
        Me.pbClose.Name = "pbClose"
        Me.pbClose.Size = New System.Drawing.Size(314, 102)
        Me.pbClose.TabIndex = 12
        Me.pbClose.TabStop = False
        '
        'lblWarranty
        '
        Me.lblWarranty.BackColor = System.Drawing.Color.Transparent
        Me.lblWarranty.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblWarranty.ForeColor = System.Drawing.Color.White
        Me.lblWarranty.Location = New System.Drawing.Point(74, 426)
        Me.lblWarranty.Name = "lblWarranty"
        Me.lblWarranty.Size = New System.Drawing.Size(621, 465)
        Me.lblWarranty.TabIndex = 13
        '
        'TimerTimeout
        '
        Me.TimerTimeout.Enabled = True
        Me.TimerTimeout.Interval = 1000
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.Transparent
        Me.btnClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 60.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.btnClose.Location = New System.Drawing.Point(240, 915)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(291, 83)
        Me.btnClose.TabIndex = 14
        Me.btnClose.Text = "ปิด"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionHome
        '
        Me.lblCaptionHome.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionHome.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionHome.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionHome.Location = New System.Drawing.Point(44, 40)
        Me.lblCaptionHome.Name = "lblCaptionHome"
        Me.lblCaptionHome.Size = New System.Drawing.Size(146, 36)
        Me.lblCaptionHome.TabIndex = 15
        Me.lblCaptionHome.Text = "หน้าหลัก"
        Me.lblCaptionHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionSelectProduct
        '
        Me.lblCaptionSelectProduct.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionSelectProduct.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionSelectProduct.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionSelectProduct.Location = New System.Drawing.Point(216, 40)
        Me.lblCaptionSelectProduct.Name = "lblCaptionSelectProduct"
        Me.lblCaptionSelectProduct.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionSelectProduct.TabIndex = 16
        Me.lblCaptionSelectProduct.Text = "เลือกสินค้า"
        Me.lblCaptionSelectProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionPayment
        '
        Me.lblCaptionPayment.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionPayment.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionPayment.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionPayment.Location = New System.Drawing.Point(389, 40)
        Me.lblCaptionPayment.Name = "lblCaptionPayment"
        Me.lblCaptionPayment.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionPayment.TabIndex = 17
        Me.lblCaptionPayment.Text = "ชำระเงิน"
        Me.lblCaptionPayment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionDelivered
        '
        Me.lblCaptionDelivered.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionDelivered.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionDelivered.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionDelivered.Location = New System.Drawing.Point(563, 40)
        Me.lblCaptionDelivered.Name = "lblCaptionDelivered"
        Me.lblCaptionDelivered.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionDelivered.TabIndex = 18
        Me.lblCaptionDelivered.Text = "รับสินค้า"
        Me.lblCaptionDelivered.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionWarrantyInfo
        '
        Me.lblCaptionWarrantyInfo.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionWarrantyInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionWarrantyInfo.ForeColor = System.Drawing.Color.White
        Me.lblCaptionWarrantyInfo.Location = New System.Drawing.Point(84, 369)
        Me.lblCaptionWarrantyInfo.Name = "lblCaptionWarrantyInfo"
        Me.lblCaptionWarrantyInfo.Size = New System.Drawing.Size(611, 36)
        Me.lblCaptionWarrantyInfo.TabIndex = 19
        Me.lblCaptionWarrantyInfo.Text = "เงื่อนไขรับประกันสินค้า"
        Me.lblCaptionWarrantyInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmProductWarrantyInfo
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(768, 1366)
        Me.Controls.Add(Me.lblCaptionWarrantyInfo)
        Me.Controls.Add(Me.lblCaptionDelivered)
        Me.Controls.Add(Me.lblCaptionPayment)
        Me.Controls.Add(Me.lblCaptionSelectProduct)
        Me.Controls.Add(Me.lblCaptionHome)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.lblWarranty)
        Me.Controls.Add(Me.pbClose)
        Me.Controls.Add(Me.pbBackground)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximumSize = New System.Drawing.Size(768, 1366)
        Me.Name = "frmProductWarrantyInfo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pbBackground As PictureBox
    Friend WithEvents pbClose As PictureBox
    Friend WithEvents lblWarranty As Label
    Friend WithEvents TimerTimeout As Timer
    Friend WithEvents btnClose As Label
    Friend WithEvents lblCaptionHome As Label
    Friend WithEvents lblCaptionSelectProduct As Label
    Friend WithEvents lblCaptionPayment As Label
    Friend WithEvents lblCaptionDelivered As Label
    Friend WithEvents lblCaptionWarrantyInfo As Label
End Class
