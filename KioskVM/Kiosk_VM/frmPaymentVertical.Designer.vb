﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPaymentVertical
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TimerTimeOut = New System.Windows.Forms.Timer(Me.components)
        Me.btn1 = New System.Windows.Forms.Button()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.btn5 = New System.Windows.Forms.Button()
        Me.btn10 = New System.Windows.Forms.Button()
        Me.btn20 = New System.Windows.Forms.Button()
        Me.btn50 = New System.Windows.Forms.Button()
        Me.btn100 = New System.Windows.Forms.Button()
        Me.btn500 = New System.Windows.Forms.Button()
        Me.btn1000 = New System.Windows.Forms.Button()
        Me.lblPaidAmt = New System.Windows.Forms.Label()
        Me.lblNetPrice = New System.Windows.Forms.Label()
        Me.lblProductName = New System.Windows.Forms.Label()
        Me.pbProduct = New System.Windows.Forms.PictureBox()
        Me.pbBackground = New System.Windows.Forms.PictureBox()
        Me.lblCaptionNetPrice = New System.Windows.Forms.Label()
        Me.lblCaptionYouPut = New System.Windows.Forms.Label()
        Me.lblCaptionDelivered = New System.Windows.Forms.Label()
        Me.lblCaptionPayment = New System.Windows.Forms.Label()
        Me.lblCaptionSelectProduct = New System.Windows.Forms.Label()
        Me.lblCaptionHome = New System.Windows.Forms.Label()
        CType(Me.pbProduct, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TimerTimeOut
        '
        Me.TimerTimeOut.Interval = 1000
        '
        'btn1
        '
        Me.btn1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btn1.Location = New System.Drawing.Point(669, 845)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(56, 23)
        Me.btn1.TabIndex = 85
        Me.btn1.Text = "1"
        Me.btn1.UseVisualStyleBackColor = True
        '
        'btn2
        '
        Me.btn2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btn2.Location = New System.Drawing.Point(579, 845)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(56, 23)
        Me.btn2.TabIndex = 84
        Me.btn2.Text = "2"
        Me.btn2.UseVisualStyleBackColor = True
        '
        'btn5
        '
        Me.btn5.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btn5.Location = New System.Drawing.Point(498, 845)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(56, 23)
        Me.btn5.TabIndex = 83
        Me.btn5.Text = "5"
        Me.btn5.UseVisualStyleBackColor = True
        '
        'btn10
        '
        Me.btn10.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btn10.Location = New System.Drawing.Point(417, 845)
        Me.btn10.Name = "btn10"
        Me.btn10.Size = New System.Drawing.Size(56, 23)
        Me.btn10.TabIndex = 82
        Me.btn10.Text = "10"
        Me.btn10.UseVisualStyleBackColor = True
        '
        'btn20
        '
        Me.btn20.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btn20.Location = New System.Drawing.Point(336, 845)
        Me.btn20.Name = "btn20"
        Me.btn20.Size = New System.Drawing.Size(56, 23)
        Me.btn20.TabIndex = 81
        Me.btn20.Text = "20"
        Me.btn20.UseVisualStyleBackColor = True
        '
        'btn50
        '
        Me.btn50.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btn50.Location = New System.Drawing.Point(255, 845)
        Me.btn50.Name = "btn50"
        Me.btn50.Size = New System.Drawing.Size(56, 23)
        Me.btn50.TabIndex = 80
        Me.btn50.Text = "50"
        Me.btn50.UseVisualStyleBackColor = True
        '
        'btn100
        '
        Me.btn100.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btn100.Location = New System.Drawing.Point(174, 845)
        Me.btn100.Name = "btn100"
        Me.btn100.Size = New System.Drawing.Size(56, 23)
        Me.btn100.TabIndex = 79
        Me.btn100.Text = "100"
        Me.btn100.UseVisualStyleBackColor = True
        '
        'btn500
        '
        Me.btn500.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btn500.Location = New System.Drawing.Point(93, 845)
        Me.btn500.Name = "btn500"
        Me.btn500.Size = New System.Drawing.Size(56, 23)
        Me.btn500.TabIndex = 78
        Me.btn500.Text = "500"
        Me.btn500.UseVisualStyleBackColor = True
        '
        'btn1000
        '
        Me.btn1000.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btn1000.Location = New System.Drawing.Point(12, 845)
        Me.btn1000.Name = "btn1000"
        Me.btn1000.Size = New System.Drawing.Size(56, 23)
        Me.btn1000.TabIndex = 77
        Me.btn1000.Text = "1000"
        Me.btn1000.UseVisualStyleBackColor = True
        '
        'lblPaidAmt
        '
        Me.lblPaidAmt.BackColor = System.Drawing.Color.Transparent
        Me.lblPaidAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 80.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblPaidAmt.Location = New System.Drawing.Point(257, 997)
        Me.lblPaidAmt.Name = "lblPaidAmt"
        Me.lblPaidAmt.Size = New System.Drawing.Size(255, 129)
        Me.lblPaidAmt.TabIndex = 86
        Me.lblPaidAmt.Text = "0"
        Me.lblPaidAmt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNetPrice
        '
        Me.lblNetPrice.BackColor = System.Drawing.Color.Transparent
        Me.lblNetPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblNetPrice.Location = New System.Drawing.Point(378, 372)
        Me.lblNetPrice.Name = "lblNetPrice"
        Me.lblNetPrice.Size = New System.Drawing.Size(188, 36)
        Me.lblNetPrice.TabIndex = 87
        Me.lblNetPrice.Text = "350"
        Me.lblNetPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblProductName
        '
        Me.lblProductName.BackColor = System.Drawing.Color.Transparent
        Me.lblProductName.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProductName.Location = New System.Drawing.Point(95, 314)
        Me.lblProductName.Name = "lblProductName"
        Me.lblProductName.Size = New System.Drawing.Size(576, 42)
        Me.lblProductName.TabIndex = 88
        Me.lblProductName.Text = "Product Name"
        Me.lblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pbProduct
        '
        Me.pbProduct.Location = New System.Drawing.Point(135, 438)
        Me.pbProduct.Name = "pbProduct"
        Me.pbProduct.Size = New System.Drawing.Size(500, 360)
        Me.pbProduct.TabIndex = 89
        Me.pbProduct.TabStop = False
        '
        'pbBackground
        '
        Me.pbBackground.BackColor = System.Drawing.Color.Transparent
        Me.pbBackground.Location = New System.Drawing.Point(0, 0)
        Me.pbBackground.Name = "pbBackground"
        Me.pbBackground.Size = New System.Drawing.Size(768, 1366)
        Me.pbBackground.TabIndex = 2
        Me.pbBackground.TabStop = False
        '
        'lblCaptionNetPrice
        '
        Me.lblCaptionNetPrice.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionNetPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionNetPrice.Location = New System.Drawing.Point(242, 372)
        Me.lblCaptionNetPrice.Name = "lblCaptionNetPrice"
        Me.lblCaptionNetPrice.Size = New System.Drawing.Size(130, 36)
        Me.lblCaptionNetPrice.TabIndex = 90
        Me.lblCaptionNetPrice.Text = "ราคา"
        Me.lblCaptionNetPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionYouPut
        '
        Me.lblCaptionYouPut.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionYouPut.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionYouPut.Location = New System.Drawing.Point(252, 933)
        Me.lblCaptionYouPut.Name = "lblCaptionYouPut"
        Me.lblCaptionYouPut.Size = New System.Drawing.Size(255, 36)
        Me.lblCaptionYouPut.TabIndex = 91
        Me.lblCaptionYouPut.Text = "YOU PUT"
        Me.lblCaptionYouPut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionDelivered
        '
        Me.lblCaptionDelivered.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionDelivered.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionDelivered.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionDelivered.Location = New System.Drawing.Point(563, 40)
        Me.lblCaptionDelivered.Name = "lblCaptionDelivered"
        Me.lblCaptionDelivered.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionDelivered.TabIndex = 95
        Me.lblCaptionDelivered.Text = "รับสินค้า"
        Me.lblCaptionDelivered.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionPayment
        '
        Me.lblCaptionPayment.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionPayment.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionPayment.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionPayment.Location = New System.Drawing.Point(389, 40)
        Me.lblCaptionPayment.Name = "lblCaptionPayment"
        Me.lblCaptionPayment.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionPayment.TabIndex = 94
        Me.lblCaptionPayment.Text = "ชำระเงิน"
        Me.lblCaptionPayment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionSelectProduct
        '
        Me.lblCaptionSelectProduct.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionSelectProduct.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionSelectProduct.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionSelectProduct.Location = New System.Drawing.Point(216, 40)
        Me.lblCaptionSelectProduct.Name = "lblCaptionSelectProduct"
        Me.lblCaptionSelectProduct.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionSelectProduct.TabIndex = 93
        Me.lblCaptionSelectProduct.Text = "เลือกสินค้า"
        Me.lblCaptionSelectProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionHome
        '
        Me.lblCaptionHome.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionHome.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionHome.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionHome.Location = New System.Drawing.Point(44, 40)
        Me.lblCaptionHome.Name = "lblCaptionHome"
        Me.lblCaptionHome.Size = New System.Drawing.Size(146, 36)
        Me.lblCaptionHome.TabIndex = 92
        Me.lblCaptionHome.Text = "หน้าหลัก"
        Me.lblCaptionHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmPaymentVertical
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(768, 1339)
        Me.Controls.Add(Me.lblCaptionDelivered)
        Me.Controls.Add(Me.lblCaptionPayment)
        Me.Controls.Add(Me.lblCaptionSelectProduct)
        Me.Controls.Add(Me.lblCaptionHome)
        Me.Controls.Add(Me.lblCaptionYouPut)
        Me.Controls.Add(Me.lblCaptionNetPrice)
        Me.Controls.Add(Me.pbProduct)
        Me.Controls.Add(Me.lblProductName)
        Me.Controls.Add(Me.lblNetPrice)
        Me.Controls.Add(Me.lblPaidAmt)
        Me.Controls.Add(Me.btn1)
        Me.Controls.Add(Me.btn2)
        Me.Controls.Add(Me.btn5)
        Me.Controls.Add(Me.btn10)
        Me.Controls.Add(Me.btn20)
        Me.Controls.Add(Me.btn50)
        Me.Controls.Add(Me.btn100)
        Me.Controls.Add(Me.btn500)
        Me.Controls.Add(Me.btn1000)
        Me.Controls.Add(Me.pbBackground)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmPaymentVertical"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.pbProduct, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TimerTimeOut As Timer
    Friend WithEvents pbBackground As PictureBox
    Friend WithEvents btn1 As Button
    Friend WithEvents btn2 As Button
    Friend WithEvents btn5 As Button
    Friend WithEvents btn10 As Button
    Friend WithEvents btn20 As Button
    Friend WithEvents btn50 As Button
    Friend WithEvents btn100 As Button
    Friend WithEvents btn500 As Button
    Friend WithEvents btn1000 As Button
    Friend WithEvents lblPaidAmt As Label
    Friend WithEvents lblNetPrice As Label
    Friend WithEvents lblProductName As Label
    Friend WithEvents pbProduct As PictureBox
    Friend WithEvents lblCaptionNetPrice As Label
    Friend WithEvents lblCaptionYouPut As Label
    Friend WithEvents lblCaptionDelivered As Label
    Friend WithEvents lblCaptionPayment As Label
    Friend WithEvents lblCaptionSelectProduct As Label
    Friend WithEvents lblCaptionHome As Label
End Class
