﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmProductDetailVertical
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.pbBackground = New System.Windows.Forms.PictureBox()
        Me.lblProductName = New System.Windows.Forms.Label()
        Me.lblProductDesc = New System.Windows.Forms.Label()
        Me.pbProduct = New System.Windows.Forms.PictureBox()
        Me.lblNetPrice = New System.Windows.Forms.Label()
        Me.lblCaptionNetPrice = New System.Windows.Forms.Label()
        Me.lblBuyNow = New System.Windows.Forms.Label()
        Me.lblWarranty = New System.Windows.Forms.Label()
        Me.TimerTimeOut = New System.Windows.Forms.Timer(Me.components)
        Me.lblCaptionDelivered = New System.Windows.Forms.Label()
        Me.lblCaptionPayment = New System.Windows.Forms.Label()
        Me.lblCaptionSelectProduct = New System.Windows.Forms.Label()
        Me.lblCaptionHome = New System.Windows.Forms.Label()
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbProduct, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pbBackground
        '
        Me.pbBackground.BackColor = System.Drawing.Color.Transparent
        Me.pbBackground.Location = New System.Drawing.Point(0, 0)
        Me.pbBackground.Name = "pbBackground"
        Me.pbBackground.Size = New System.Drawing.Size(768, 1366)
        Me.pbBackground.TabIndex = 5
        Me.pbBackground.TabStop = False
        '
        'lblProductName
        '
        Me.lblProductName.BackColor = System.Drawing.Color.Transparent
        Me.lblProductName.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProductName.Location = New System.Drawing.Point(89, 356)
        Me.lblProductName.Name = "lblProductName"
        Me.lblProductName.Size = New System.Drawing.Size(586, 32)
        Me.lblProductName.TabIndex = 8
        Me.lblProductName.Text = "ProductName"
        Me.lblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblProductDesc
        '
        Me.lblProductDesc.BackColor = System.Drawing.Color.Transparent
        Me.lblProductDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProductDesc.Location = New System.Drawing.Point(89, 840)
        Me.lblProductDesc.Name = "lblProductDesc"
        Me.lblProductDesc.Size = New System.Drawing.Size(586, 126)
        Me.lblProductDesc.TabIndex = 7
        Me.lblProductDesc.Text = "ProductDesc"
        '
        'pbProduct
        '
        Me.pbProduct.Location = New System.Drawing.Point(134, 426)
        Me.pbProduct.Name = "pbProduct"
        Me.pbProduct.Size = New System.Drawing.Size(500, 360)
        Me.pbProduct.TabIndex = 6
        Me.pbProduct.TabStop = False
        '
        'lblNetPrice
        '
        Me.lblNetPrice.BackColor = System.Drawing.Color.Transparent
        Me.lblNetPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblNetPrice.Location = New System.Drawing.Point(377, 1004)
        Me.lblNetPrice.Name = "lblNetPrice"
        Me.lblNetPrice.Size = New System.Drawing.Size(170, 32)
        Me.lblNetPrice.TabIndex = 11
        Me.lblNetPrice.Text = "350"
        Me.lblNetPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionNetPrice
        '
        Me.lblCaptionNetPrice.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionNetPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionNetPrice.Location = New System.Drawing.Point(246, 1004)
        Me.lblCaptionNetPrice.Name = "lblCaptionNetPrice"
        Me.lblCaptionNetPrice.Size = New System.Drawing.Size(106, 32)
        Me.lblCaptionNetPrice.TabIndex = 13
        Me.lblCaptionNetPrice.Text = "ราคา"
        '
        'lblBuyNow
        '
        Me.lblBuyNow.BackColor = System.Drawing.Color.Transparent
        Me.lblBuyNow.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblBuyNow.ForeColor = System.Drawing.Color.White
        Me.lblBuyNow.Location = New System.Drawing.Point(426, 1116)
        Me.lblBuyNow.Name = "lblBuyNow"
        Me.lblBuyNow.Size = New System.Drawing.Size(249, 72)
        Me.lblBuyNow.TabIndex = 14
        Me.lblBuyNow.Text = "ซื้อเลย"
        Me.lblBuyNow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblWarranty
        '
        Me.lblWarranty.BackColor = System.Drawing.Color.Transparent
        Me.lblWarranty.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblWarranty.ForeColor = System.Drawing.Color.White
        Me.lblWarranty.Location = New System.Drawing.Point(82, 1112)
        Me.lblWarranty.Name = "lblWarranty"
        Me.lblWarranty.Size = New System.Drawing.Size(296, 76)
        Me.lblWarranty.TabIndex = 15
        Me.lblWarranty.Text = "เงื่อนไขประกันสินค้า"
        Me.lblWarranty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TimerTimeOut
        '
        Me.TimerTimeOut.Interval = 1000
        '
        'lblCaptionDelivered
        '
        Me.lblCaptionDelivered.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionDelivered.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionDelivered.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionDelivered.Location = New System.Drawing.Point(563, 40)
        Me.lblCaptionDelivered.Name = "lblCaptionDelivered"
        Me.lblCaptionDelivered.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionDelivered.TabIndex = 26
        Me.lblCaptionDelivered.Text = "รับสินค้า"
        Me.lblCaptionDelivered.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionPayment
        '
        Me.lblCaptionPayment.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionPayment.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionPayment.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionPayment.Location = New System.Drawing.Point(389, 40)
        Me.lblCaptionPayment.Name = "lblCaptionPayment"
        Me.lblCaptionPayment.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionPayment.TabIndex = 25
        Me.lblCaptionPayment.Text = "ชำระเงิน"
        Me.lblCaptionPayment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionSelectProduct
        '
        Me.lblCaptionSelectProduct.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionSelectProduct.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionSelectProduct.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionSelectProduct.Location = New System.Drawing.Point(216, 40)
        Me.lblCaptionSelectProduct.Name = "lblCaptionSelectProduct"
        Me.lblCaptionSelectProduct.Size = New System.Drawing.Size(167, 36)
        Me.lblCaptionSelectProduct.TabIndex = 24
        Me.lblCaptionSelectProduct.Text = "เลือกสินค้า"
        Me.lblCaptionSelectProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaptionHome
        '
        Me.lblCaptionHome.BackColor = System.Drawing.Color.Transparent
        Me.lblCaptionHome.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblCaptionHome.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCaptionHome.Location = New System.Drawing.Point(44, 40)
        Me.lblCaptionHome.Name = "lblCaptionHome"
        Me.lblCaptionHome.Size = New System.Drawing.Size(146, 36)
        Me.lblCaptionHome.TabIndex = 23
        Me.lblCaptionHome.Text = "หน้าหลัก"
        Me.lblCaptionHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmProductDetailVertical
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(768, 1366)
        Me.Controls.Add(Me.lblCaptionDelivered)
        Me.Controls.Add(Me.lblCaptionPayment)
        Me.Controls.Add(Me.lblCaptionSelectProduct)
        Me.Controls.Add(Me.lblCaptionHome)
        Me.Controls.Add(Me.lblWarranty)
        Me.Controls.Add(Me.lblBuyNow)
        Me.Controls.Add(Me.lblCaptionNetPrice)
        Me.Controls.Add(Me.lblNetPrice)
        Me.Controls.Add(Me.lblProductName)
        Me.Controls.Add(Me.lblProductDesc)
        Me.Controls.Add(Me.pbProduct)
        Me.Controls.Add(Me.pbBackground)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximumSize = New System.Drawing.Size(768, 1366)
        Me.Name = "frmProductDetailVertical"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.pbBackground, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbProduct, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pbBackground As PictureBox
    Friend WithEvents lblProductName As Label
    Friend WithEvents lblProductDesc As Label
    Friend WithEvents pbProduct As PictureBox
    Friend WithEvents lblNetPrice As Label
    Friend WithEvents lblCaptionNetPrice As Label
    Friend WithEvents lblBuyNow As Label
    Friend WithEvents lblWarranty As Label
    Friend WithEvents TimerTimeOut As Timer
    Friend WithEvents lblCaptionDelivered As Label
    Friend WithEvents lblCaptionPayment As Label
    Friend WithEvents lblCaptionSelectProduct As Label
    Friend WithEvents lblCaptionHome As Label
End Class
