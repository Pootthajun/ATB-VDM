Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = VendingLinqDB.ConnectDB.VendingDB
Imports VendingLinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for CF_VENDING_SYSCONFIG table VendingLinqDB.
    '[Create by  on September, 21 2016]
    Public Class CfVendingSysconfigVendingLinqDB
        Public sub CfVendingSysconfigVendingLinqDB()

        End Sub 
        ' CF_VENDING_SYSCONFIG
        Const _tableName As String = "CF_VENDING_SYSCONFIG"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _MS_VENDING_ID As Long = 0
        Dim _SCREEN_LAYOUT As  System.Nullable(Of Char)  = "H"
        Dim _SCREEN_SAVER_SEC As Long = 0
        Dim _TIME_OUT_SEC As Long = 0
        Dim _SHOW_MSG_SEC As Long = 0
        Dim _PAYMENT_EXTEND_SEC As Long = 0
        Dim _SHELF_LONG As Long = 0
        Dim _SLEEP_TIME As String = ""
        Dim _SLEEP_DURATION As Long = 0
        Dim _VDM_WEBSERVICE_URL As String = ""
        Dim _ALARM_WEBSERVICE_URL As String = ""
        Dim _ADS_VDO_PATH As String = "A"
        Dim _MOTOR_PARK_POSITION As String = ""
        Dim _MOTOR_ORIGIN_POSITION As String = ""
        Dim _MOTOR_DELIVERY_POSITION As String = ""
        Dim _INTERVAL_SYNC_TO_VENDING_MIN As Long = 0
        Dim _INTERVAL_SYNC_TO_SERVER_MIN As Long = 0
        Dim _SYNC_TO_VENDING As Char = "N"
        Dim _SYNC_TO_SERVER As Char = "N"

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_MS_VENDING_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property MS_VENDING_ID() As Long
            Get
                Return _MS_VENDING_ID
            End Get
            Set(ByVal value As Long)
               _MS_VENDING_ID = value
            End Set
        End Property 
        <Column(Storage:="_SCREEN_LAYOUT", DbType:="Char(1)")>  _
        Public Property SCREEN_LAYOUT() As  System.Nullable(Of Char) 
            Get
                Return _SCREEN_LAYOUT
            End Get
            Set(ByVal value As  System.Nullable(Of Char) )
               _SCREEN_LAYOUT = value
            End Set
        End Property 
        <Column(Storage:="_SCREEN_SAVER_SEC", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property SCREEN_SAVER_SEC() As Long
            Get
                Return _SCREEN_SAVER_SEC
            End Get
            Set(ByVal value As Long)
               _SCREEN_SAVER_SEC = value
            End Set
        End Property 
        <Column(Storage:="_TIME_OUT_SEC", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property TIME_OUT_SEC() As Long
            Get
                Return _TIME_OUT_SEC
            End Get
            Set(ByVal value As Long)
               _TIME_OUT_SEC = value
            End Set
        End Property 
        <Column(Storage:="_SHOW_MSG_SEC", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property SHOW_MSG_SEC() As Long
            Get
                Return _SHOW_MSG_SEC
            End Get
            Set(ByVal value As Long)
               _SHOW_MSG_SEC = value
            End Set
        End Property 
        <Column(Storage:="_PAYMENT_EXTEND_SEC", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property PAYMENT_EXTEND_SEC() As Long
            Get
                Return _PAYMENT_EXTEND_SEC
            End Get
            Set(ByVal value As Long)
               _PAYMENT_EXTEND_SEC = value
            End Set
        End Property 
        <Column(Storage:="_SHELF_LONG", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property SHELF_LONG() As Long
            Get
                Return _SHELF_LONG
            End Get
            Set(ByVal value As Long)
               _SHELF_LONG = value
            End Set
        End Property 
        <Column(Storage:="_SLEEP_TIME", DbType:="VarChar(5) NOT NULL ",CanBeNull:=false)>  _
        Public Property SLEEP_TIME() As String
            Get
                Return _SLEEP_TIME
            End Get
            Set(ByVal value As String)
               _SLEEP_TIME = value
            End Set
        End Property 
        <Column(Storage:="_SLEEP_DURATION", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property SLEEP_DURATION() As Long
            Get
                Return _SLEEP_DURATION
            End Get
            Set(ByVal value As Long)
               _SLEEP_DURATION = value
            End Set
        End Property 
        <Column(Storage:="_VDM_WEBSERVICE_URL", DbType:="VarChar(255) NOT NULL ",CanBeNull:=false)>  _
        Public Property VDM_WEBSERVICE_URL() As String
            Get
                Return _VDM_WEBSERVICE_URL
            End Get
            Set(ByVal value As String)
               _VDM_WEBSERVICE_URL = value
            End Set
        End Property 
        <Column(Storage:="_ALARM_WEBSERVICE_URL", DbType:="VarChar(255) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARM_WEBSERVICE_URL() As String
            Get
                Return _ALARM_WEBSERVICE_URL
            End Get
            Set(ByVal value As String)
               _ALARM_WEBSERVICE_URL = value
            End Set
        End Property 
        <Column(Storage:="_ADS_VDO_PATH", DbType:="VarChar(255) NOT NULL ",CanBeNull:=false)>  _
        Public Property ADS_VDO_PATH() As String
            Get
                Return _ADS_VDO_PATH
            End Get
            Set(ByVal value As String)
               _ADS_VDO_PATH = value
            End Set
        End Property 
        <Column(Storage:="_MOTOR_PARK_POSITION", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property MOTOR_PARK_POSITION() As String
            Get
                Return _MOTOR_PARK_POSITION
            End Get
            Set(ByVal value As String)
               _MOTOR_PARK_POSITION = value
            End Set
        End Property 
        <Column(Storage:="_MOTOR_ORIGIN_POSITION", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property MOTOR_ORIGIN_POSITION() As String
            Get
                Return _MOTOR_ORIGIN_POSITION
            End Get
            Set(ByVal value As String)
               _MOTOR_ORIGIN_POSITION = value
            End Set
        End Property 
        <Column(Storage:="_MOTOR_DELIVERY_POSITION", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property MOTOR_DELIVERY_POSITION() As String
            Get
                Return _MOTOR_DELIVERY_POSITION
            End Get
            Set(ByVal value As String)
               _MOTOR_DELIVERY_POSITION = value
            End Set
        End Property 
        <Column(Storage:="_INTERVAL_SYNC_TO_VENDING_MIN", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property INTERVAL_SYNC_TO_VENDING_MIN() As Long
            Get
                Return _INTERVAL_SYNC_TO_VENDING_MIN
            End Get
            Set(ByVal value As Long)
               _INTERVAL_SYNC_TO_VENDING_MIN = value
            End Set
        End Property 
        <Column(Storage:="_INTERVAL_SYNC_TO_SERVER_MIN", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property INTERVAL_SYNC_TO_SERVER_MIN() As Long
            Get
                Return _INTERVAL_SYNC_TO_SERVER_MIN
            End Get
            Set(ByVal value As Long)
               _INTERVAL_SYNC_TO_SERVER_MIN = value
            End Set
        End Property 
        <Column(Storage:="_SYNC_TO_VENDING", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property SYNC_TO_VENDING() As Char
            Get
                Return _SYNC_TO_VENDING
            End Get
            Set(ByVal value As Char)
               _SYNC_TO_VENDING = value
            End Set
        End Property 
        <Column(Storage:="_SYNC_TO_SERVER", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property SYNC_TO_SERVER() As Char
            Get
                Return _SYNC_TO_SERVER
            End Get
            Set(ByVal value As Char)
               _SYNC_TO_SERVER = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
            _MS_VENDING_ID = 0
            _SCREEN_LAYOUT = "H"
            _SCREEN_SAVER_SEC = 0
            _TIME_OUT_SEC = 0
            _SHOW_MSG_SEC = 0
            _PAYMENT_EXTEND_SEC = 0
            _SHELF_LONG = 0
            _SLEEP_TIME = ""
            _SLEEP_DURATION = 0
            _VDM_WEBSERVICE_URL = ""
            _ALARM_WEBSERVICE_URL = ""
            _ADS_VDO_PATH = "A"
            _MOTOR_PARK_POSITION = ""
            _MOTOR_ORIGIN_POSITION = ""
            _MOTOR_DELIVERY_POSITION = ""
            _INTERVAL_SYNC_TO_VENDING_MIN = 0
            _INTERVAL_SYNC_TO_SERVER_MIN = 0
            _SYNC_TO_VENDING = "N"
            _SYNC_TO_SERVER = "N"
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into CF_VENDING_SYSCONFIG table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to CF_VENDING_SYSCONFIG table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to CF_VENDING_SYSCONFIG table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from CF_VENDING_SYSCONFIG table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of CF_VENDING_SYSCONFIG by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of CF_VENDING_SYSCONFIG by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As CfVendingSysconfigVendingLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of CF_VENDING_SYSCONFIG by specified MS_VENDING_ID key is retrieved successfully.
        '/// <param name=cMS_VENDING_ID>The MS_VENDING_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByMS_VENDING_ID(cMS_VENDING_ID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MS_VENDING_ID", cMS_VENDING_ID) 
            Return doChkData("MS_VENDING_ID = @_MS_VENDING_ID", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of CF_VENDING_SYSCONFIG by specified MS_VENDING_ID key is retrieved successfully.
        '/// <param name=cMS_VENDING_ID>The MS_VENDING_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByMS_VENDING_ID(cMS_VENDING_ID As Long, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MS_VENDING_ID", cMS_VENDING_ID) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("MS_VENDING_ID = @_MS_VENDING_ID And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of CF_VENDING_SYSCONFIG by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into CF_VENDING_SYSCONFIG table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to CF_VENDING_SYSCONFIG table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from CF_VENDING_SYSCONFIG table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(23) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(1).Value = _CREATED_BY.Trim

            cmbParam(2) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(2).Value = _CREATED_DATE

            cmbParam(3) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(3).Value = _UPDATED_BY.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(4).Value = _UPDATED_DATE.Value
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_MS_VENDING_ID", SqlDbType.BigInt)
            cmbParam(5).Value = _MS_VENDING_ID

            cmbParam(6) = New SqlParameter("@_SCREEN_LAYOUT", SqlDbType.Char)
            If _SCREEN_LAYOUT.Value <> "" Then 
                cmbParam(6).Value = _SCREEN_LAYOUT.Value
            Else
                cmbParam(6).Value = DBNull.value
            End IF

            cmbParam(7) = New SqlParameter("@_SCREEN_SAVER_SEC", SqlDbType.Int)
            cmbParam(7).Value = _SCREEN_SAVER_SEC

            cmbParam(8) = New SqlParameter("@_TIME_OUT_SEC", SqlDbType.Int)
            cmbParam(8).Value = _TIME_OUT_SEC

            cmbParam(9) = New SqlParameter("@_SHOW_MSG_SEC", SqlDbType.Int)
            cmbParam(9).Value = _SHOW_MSG_SEC

            cmbParam(10) = New SqlParameter("@_PAYMENT_EXTEND_SEC", SqlDbType.Int)
            cmbParam(10).Value = _PAYMENT_EXTEND_SEC

            cmbParam(11) = New SqlParameter("@_SHELF_LONG", SqlDbType.Int)
            cmbParam(11).Value = _SHELF_LONG

            cmbParam(12) = New SqlParameter("@_SLEEP_TIME", SqlDbType.VarChar)
            cmbParam(12).Value = _SLEEP_TIME.Trim

            cmbParam(13) = New SqlParameter("@_SLEEP_DURATION", SqlDbType.Int)
            cmbParam(13).Value = _SLEEP_DURATION

            cmbParam(14) = New SqlParameter("@_VDM_WEBSERVICE_URL", SqlDbType.VarChar)
            cmbParam(14).Value = _VDM_WEBSERVICE_URL.Trim

            cmbParam(15) = New SqlParameter("@_ALARM_WEBSERVICE_URL", SqlDbType.VarChar)
            cmbParam(15).Value = _ALARM_WEBSERVICE_URL.Trim

            cmbParam(16) = New SqlParameter("@_ADS_VDO_PATH", SqlDbType.VarChar)
            cmbParam(16).Value = _ADS_VDO_PATH.Trim

            cmbParam(17) = New SqlParameter("@_MOTOR_PARK_POSITION", SqlDbType.VarChar)
            cmbParam(17).Value = _MOTOR_PARK_POSITION.Trim

            cmbParam(18) = New SqlParameter("@_MOTOR_ORIGIN_POSITION", SqlDbType.VarChar)
            cmbParam(18).Value = _MOTOR_ORIGIN_POSITION.Trim

            cmbParam(19) = New SqlParameter("@_MOTOR_DELIVERY_POSITION", SqlDbType.VarChar)
            cmbParam(19).Value = _MOTOR_DELIVERY_POSITION.Trim

            cmbParam(20) = New SqlParameter("@_INTERVAL_SYNC_TO_VENDING_MIN", SqlDbType.Int)
            cmbParam(20).Value = _INTERVAL_SYNC_TO_VENDING_MIN

            cmbParam(21) = New SqlParameter("@_INTERVAL_SYNC_TO_SERVER_MIN", SqlDbType.Int)
            cmbParam(21).Value = _INTERVAL_SYNC_TO_SERVER_MIN

            cmbParam(22) = New SqlParameter("@_SYNC_TO_VENDING", SqlDbType.Char)
            cmbParam(22).Value = _SYNC_TO_VENDING

            cmbParam(23) = New SqlParameter("@_SYNC_TO_SERVER", SqlDbType.Char)
            cmbParam(23).Value = _SYNC_TO_SERVER

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of CF_VENDING_SYSCONFIG by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("ms_vending_id")) = False Then _ms_vending_id = Convert.ToInt64(Rdr("ms_vending_id"))
                        If Convert.IsDBNull(Rdr("screen_layout")) = False Then _screen_layout = Rdr("screen_layout").ToString()
                        If Convert.IsDBNull(Rdr("screen_saver_sec")) = False Then _screen_saver_sec = Convert.ToInt32(Rdr("screen_saver_sec"))
                        If Convert.IsDBNull(Rdr("time_out_sec")) = False Then _time_out_sec = Convert.ToInt32(Rdr("time_out_sec"))
                        If Convert.IsDBNull(Rdr("show_msg_sec")) = False Then _show_msg_sec = Convert.ToInt32(Rdr("show_msg_sec"))
                        If Convert.IsDBNull(Rdr("payment_extend_sec")) = False Then _payment_extend_sec = Convert.ToInt32(Rdr("payment_extend_sec"))
                        If Convert.IsDBNull(Rdr("shelf_long")) = False Then _shelf_long = Convert.ToInt32(Rdr("shelf_long"))
                        If Convert.IsDBNull(Rdr("sleep_time")) = False Then _sleep_time = Rdr("sleep_time").ToString()
                        If Convert.IsDBNull(Rdr("sleep_duration")) = False Then _sleep_duration = Convert.ToInt32(Rdr("sleep_duration"))
                        If Convert.IsDBNull(Rdr("vdm_webservice_url")) = False Then _vdm_webservice_url = Rdr("vdm_webservice_url").ToString()
                        If Convert.IsDBNull(Rdr("alarm_webservice_url")) = False Then _alarm_webservice_url = Rdr("alarm_webservice_url").ToString()
                        If Convert.IsDBNull(Rdr("ads_vdo_path")) = False Then _ads_vdo_path = Rdr("ads_vdo_path").ToString()
                        If Convert.IsDBNull(Rdr("motor_park_position")) = False Then _motor_park_position = Rdr("motor_park_position").ToString()
                        If Convert.IsDBNull(Rdr("motor_origin_position")) = False Then _motor_origin_position = Rdr("motor_origin_position").ToString()
                        If Convert.IsDBNull(Rdr("motor_delivery_position")) = False Then _motor_delivery_position = Rdr("motor_delivery_position").ToString()
                        If Convert.IsDBNull(Rdr("interval_sync_to_vending_min")) = False Then _interval_sync_to_vending_min = Convert.ToInt32(Rdr("interval_sync_to_vending_min"))
                        If Convert.IsDBNull(Rdr("interval_sync_to_server_min")) = False Then _interval_sync_to_server_min = Convert.ToInt32(Rdr("interval_sync_to_server_min"))
                        If Convert.IsDBNull(Rdr("sync_to_vending")) = False Then _sync_to_vending = Rdr("sync_to_vending").ToString()
                        If Convert.IsDBNull(Rdr("sync_to_server")) = False Then _sync_to_server = Rdr("sync_to_server").ToString()
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of CF_VENDING_SYSCONFIG by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As CfVendingSysconfigVendingLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("ms_vending_id")) = False Then _ms_vending_id = Convert.ToInt64(Rdr("ms_vending_id"))
                        If Convert.IsDBNull(Rdr("screen_layout")) = False Then _screen_layout = Rdr("screen_layout").ToString()
                        If Convert.IsDBNull(Rdr("screen_saver_sec")) = False Then _screen_saver_sec = Convert.ToInt32(Rdr("screen_saver_sec"))
                        If Convert.IsDBNull(Rdr("time_out_sec")) = False Then _time_out_sec = Convert.ToInt32(Rdr("time_out_sec"))
                        If Convert.IsDBNull(Rdr("show_msg_sec")) = False Then _show_msg_sec = Convert.ToInt32(Rdr("show_msg_sec"))
                        If Convert.IsDBNull(Rdr("payment_extend_sec")) = False Then _payment_extend_sec = Convert.ToInt32(Rdr("payment_extend_sec"))
                        If Convert.IsDBNull(Rdr("shelf_long")) = False Then _shelf_long = Convert.ToInt32(Rdr("shelf_long"))
                        If Convert.IsDBNull(Rdr("sleep_time")) = False Then _sleep_time = Rdr("sleep_time").ToString()
                        If Convert.IsDBNull(Rdr("sleep_duration")) = False Then _sleep_duration = Convert.ToInt32(Rdr("sleep_duration"))
                        If Convert.IsDBNull(Rdr("vdm_webservice_url")) = False Then _vdm_webservice_url = Rdr("vdm_webservice_url").ToString()
                        If Convert.IsDBNull(Rdr("alarm_webservice_url")) = False Then _alarm_webservice_url = Rdr("alarm_webservice_url").ToString()
                        If Convert.IsDBNull(Rdr("ads_vdo_path")) = False Then _ads_vdo_path = Rdr("ads_vdo_path").ToString()
                        If Convert.IsDBNull(Rdr("motor_park_position")) = False Then _motor_park_position = Rdr("motor_park_position").ToString()
                        If Convert.IsDBNull(Rdr("motor_origin_position")) = False Then _motor_origin_position = Rdr("motor_origin_position").ToString()
                        If Convert.IsDBNull(Rdr("motor_delivery_position")) = False Then _motor_delivery_position = Rdr("motor_delivery_position").ToString()
                        If Convert.IsDBNull(Rdr("interval_sync_to_vending_min")) = False Then _interval_sync_to_vending_min = Convert.ToInt32(Rdr("interval_sync_to_vending_min"))
                        If Convert.IsDBNull(Rdr("interval_sync_to_server_min")) = False Then _interval_sync_to_server_min = Convert.ToInt32(Rdr("interval_sync_to_server_min"))
                        If Convert.IsDBNull(Rdr("sync_to_vending")) = False Then _sync_to_vending = Rdr("sync_to_vending").ToString()
                        If Convert.IsDBNull(Rdr("sync_to_server")) = False Then _sync_to_server = Rdr("sync_to_server").ToString()
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table CF_VENDING_SYSCONFIG
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (CREATED_BY, CREATED_DATE, MS_VENDING_ID, SCREEN_LAYOUT, SCREEN_SAVER_SEC, TIME_OUT_SEC, SHOW_MSG_SEC, PAYMENT_EXTEND_SEC, SHELF_LONG, SLEEP_TIME, SLEEP_DURATION, VDM_WEBSERVICE_URL, ALARM_WEBSERVICE_URL, ADS_VDO_PATH, MOTOR_PARK_POSITION, MOTOR_ORIGIN_POSITION, MOTOR_DELIVERY_POSITION, INTERVAL_SYNC_TO_VENDING_MIN, INTERVAL_SYNC_TO_SERVER_MIN, SYNC_TO_VENDING, SYNC_TO_SERVER)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE, INSERTED.MS_VENDING_ID, INSERTED.SCREEN_LAYOUT, INSERTED.SCREEN_SAVER_SEC, INSERTED.TIME_OUT_SEC, INSERTED.SHOW_MSG_SEC, INSERTED.PAYMENT_EXTEND_SEC, INSERTED.SHELF_LONG, INSERTED.SLEEP_TIME, INSERTED.SLEEP_DURATION, INSERTED.VDM_WEBSERVICE_URL, INSERTED.ALARM_WEBSERVICE_URL, INSERTED.ADS_VDO_PATH, INSERTED.MOTOR_PARK_POSITION, INSERTED.MOTOR_ORIGIN_POSITION, INSERTED.MOTOR_DELIVERY_POSITION, INSERTED.INTERVAL_SYNC_TO_VENDING_MIN, INSERTED.INTERVAL_SYNC_TO_SERVER_MIN, INSERTED.SYNC_TO_VENDING, INSERTED.SYNC_TO_SERVER"
                Sql += " VALUES("
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_MS_VENDING_ID" & ", "
                sql += "@_SCREEN_LAYOUT" & ", "
                sql += "@_SCREEN_SAVER_SEC" & ", "
                sql += "@_TIME_OUT_SEC" & ", "
                sql += "@_SHOW_MSG_SEC" & ", "
                sql += "@_PAYMENT_EXTEND_SEC" & ", "
                sql += "@_SHELF_LONG" & ", "
                sql += "@_SLEEP_TIME" & ", "
                sql += "@_SLEEP_DURATION" & ", "
                sql += "@_VDM_WEBSERVICE_URL" & ", "
                sql += "@_ALARM_WEBSERVICE_URL" & ", "
                sql += "@_ADS_VDO_PATH" & ", "
                sql += "@_MOTOR_PARK_POSITION" & ", "
                sql += "@_MOTOR_ORIGIN_POSITION" & ", "
                sql += "@_MOTOR_DELIVERY_POSITION" & ", "
                sql += "@_INTERVAL_SYNC_TO_VENDING_MIN" & ", "
                sql += "@_INTERVAL_SYNC_TO_SERVER_MIN" & ", "
                sql += "@_SYNC_TO_VENDING" & ", "
                sql += "@_SYNC_TO_SERVER"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table CF_VENDING_SYSCONFIG
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" & ", "
                Sql += "MS_VENDING_ID = " & "@_MS_VENDING_ID" & ", "
                Sql += "SCREEN_LAYOUT = " & "@_SCREEN_LAYOUT" & ", "
                Sql += "SCREEN_SAVER_SEC = " & "@_SCREEN_SAVER_SEC" & ", "
                Sql += "TIME_OUT_SEC = " & "@_TIME_OUT_SEC" & ", "
                Sql += "SHOW_MSG_SEC = " & "@_SHOW_MSG_SEC" & ", "
                Sql += "PAYMENT_EXTEND_SEC = " & "@_PAYMENT_EXTEND_SEC" & ", "
                Sql += "SHELF_LONG = " & "@_SHELF_LONG" & ", "
                Sql += "SLEEP_TIME = " & "@_SLEEP_TIME" & ", "
                Sql += "SLEEP_DURATION = " & "@_SLEEP_DURATION" & ", "
                Sql += "VDM_WEBSERVICE_URL = " & "@_VDM_WEBSERVICE_URL" & ", "
                Sql += "ALARM_WEBSERVICE_URL = " & "@_ALARM_WEBSERVICE_URL" & ", "
                Sql += "ADS_VDO_PATH = " & "@_ADS_VDO_PATH" & ", "
                Sql += "MOTOR_PARK_POSITION = " & "@_MOTOR_PARK_POSITION" & ", "
                Sql += "MOTOR_ORIGIN_POSITION = " & "@_MOTOR_ORIGIN_POSITION" & ", "
                Sql += "MOTOR_DELIVERY_POSITION = " & "@_MOTOR_DELIVERY_POSITION" & ", "
                Sql += "INTERVAL_SYNC_TO_VENDING_MIN = " & "@_INTERVAL_SYNC_TO_VENDING_MIN" & ", "
                Sql += "INTERVAL_SYNC_TO_SERVER_MIN = " & "@_INTERVAL_SYNC_TO_SERVER_MIN" & ", "
                Sql += "SYNC_TO_VENDING = " & "@_SYNC_TO_VENDING" & ", "
                Sql += "SYNC_TO_SERVER = " & "@_SYNC_TO_SERVER" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table CF_VENDING_SYSCONFIG
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table CF_VENDING_SYSCONFIG
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE, MS_VENDING_ID, SCREEN_LAYOUT, SCREEN_SAVER_SEC, TIME_OUT_SEC, SHOW_MSG_SEC, PAYMENT_EXTEND_SEC, SHELF_LONG, SLEEP_TIME, SLEEP_DURATION, VDM_WEBSERVICE_URL, ALARM_WEBSERVICE_URL, ADS_VDO_PATH, MOTOR_PARK_POSITION, MOTOR_ORIGIN_POSITION, MOTOR_DELIVERY_POSITION, INTERVAL_SYNC_TO_VENDING_MIN, INTERVAL_SYNC_TO_SERVER_MIN, SYNC_TO_VENDING, SYNC_TO_SERVER FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
