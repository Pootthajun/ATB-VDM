Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = VendingLinqDB.ConnectDB.VendingDB
Imports VendingLinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_VENDING_SALE_TRANSACTION table VendingLinqDB.
    '[Create by  on September, 21 2016]
    Public Class TbVendingSaleTransactionVendingLinqDB
        Public sub TbVendingSaleTransactionVendingLinqDB()

        End Sub 
        ' TB_VENDING_SALE_TRANSACTION
        Const _tableName As String = "TB_VENDING_SALE_TRANSACTION"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _TRANS_NO As String = ""
        Dim _TRANS_START_TIME As DateTime = New DateTime(1,1,1)
        Dim _TRANS_END_TIME As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _MS_VENDING_ID As Long = 0
        Dim _MS_PRODUCT_ID As Long = 0
        Dim _SHELF_ID As Long = 0
        Dim _PRODUCT_COST As  System.Nullable(Of Double) 
        Dim _PRODUCT_PRICE As  System.Nullable(Of Double) 
        Dim _PRODUCT_DISCOUNT_PERCENT As  System.Nullable(Of Long) 
        Dim _PRODUCT_NETPRICE As  System.Nullable(Of Double) 
        Dim _PRODUCT_PROFIT_SHARING As  System.Nullable(Of Long) 
        Dim _PAID_SUCCESS_TIME As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _RECEIVE_COIN1 As Long = 0
        Dim _RECEIVE_COIN2 As Long = 0
        Dim _RECEIVE_COIN5 As Long = 0
        Dim _RECEIVE_COIN10 As Long = 0
        Dim _RECEIVE_BANKNOTE20 As Long = 0
        Dim _RECEIVE_BANKNOTE50 As Long = 0
        Dim _RECEIVE_BANKNOTE100 As Long = 0
        Dim _RECEIVE_BANKNOTE500 As Long = 0
        Dim _RECEIVE_BANKNOTE1000 As Long = 0
        Dim _CHANGE_COIN1 As Long = 0
        Dim _CHANGE_COIN2 As Long = 0
        Dim _CHANGE_COIN5 As Long = 0
        Dim _CHANGE_COIN10 As Long = 0
        Dim _CHANGE_BANKNOTE20 As Long = 0
        Dim _CHANGE_BANKNOTE50 As Long = 0
        Dim _CHANGE_BANKNOTE100 As Long = 0
        Dim _CHANGE_BANKNOTE500 As Long = 0
        Dim _TRANS_STATUS As Char = "0"
        Dim _MS_APP_STEP_ID As Long = 0
        Dim _SYNC_TO_SERVER As Char = "N"

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_TRANS_NO", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property TRANS_NO() As String
            Get
                Return _TRANS_NO
            End Get
            Set(ByVal value As String)
               _TRANS_NO = value
            End Set
        End Property 
        <Column(Storage:="_TRANS_START_TIME", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property TRANS_START_TIME() As DateTime
            Get
                Return _TRANS_START_TIME
            End Get
            Set(ByVal value As DateTime)
               _TRANS_START_TIME = value
            End Set
        End Property 
        <Column(Storage:="_TRANS_END_TIME", DbType:="DateTime")>  _
        Public Property TRANS_END_TIME() As  System.Nullable(Of DateTime) 
            Get
                Return _TRANS_END_TIME
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _TRANS_END_TIME = value
            End Set
        End Property 
        <Column(Storage:="_MS_VENDING_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property MS_VENDING_ID() As Long
            Get
                Return _MS_VENDING_ID
            End Get
            Set(ByVal value As Long)
               _MS_VENDING_ID = value
            End Set
        End Property 
        <Column(Storage:="_MS_PRODUCT_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property MS_PRODUCT_ID() As Long
            Get
                Return _MS_PRODUCT_ID
            End Get
            Set(ByVal value As Long)
               _MS_PRODUCT_ID = value
            End Set
        End Property 
        <Column(Storage:="_SHELF_ID", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property SHELF_ID() As Long
            Get
                Return _SHELF_ID
            End Get
            Set(ByVal value As Long)
               _SHELF_ID = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_COST", DbType:="Float")>  _
        Public Property PRODUCT_COST() As  System.Nullable(Of Double) 
            Get
                Return _PRODUCT_COST
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _PRODUCT_COST = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_PRICE", DbType:="Float")>  _
        Public Property PRODUCT_PRICE() As  System.Nullable(Of Double) 
            Get
                Return _PRODUCT_PRICE
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _PRODUCT_PRICE = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_DISCOUNT_PERCENT", DbType:="Int")>  _
        Public Property PRODUCT_DISCOUNT_PERCENT() As  System.Nullable(Of Long) 
            Get
                Return _PRODUCT_DISCOUNT_PERCENT
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _PRODUCT_DISCOUNT_PERCENT = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_NETPRICE", DbType:="Float")>  _
        Public Property PRODUCT_NETPRICE() As  System.Nullable(Of Double) 
            Get
                Return _PRODUCT_NETPRICE
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _PRODUCT_NETPRICE = value
            End Set
        End Property 
        <Column(Storage:="_PRODUCT_PROFIT_SHARING", DbType:="Int")>  _
        Public Property PRODUCT_PROFIT_SHARING() As  System.Nullable(Of Long) 
            Get
                Return _PRODUCT_PROFIT_SHARING
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _PRODUCT_PROFIT_SHARING = value
            End Set
        End Property 
        <Column(Storage:="_PAID_SUCCESS_TIME", DbType:="DateTime")>  _
        Public Property PAID_SUCCESS_TIME() As  System.Nullable(Of DateTime) 
            Get
                Return _PAID_SUCCESS_TIME
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _PAID_SUCCESS_TIME = value
            End Set
        End Property 
        <Column(Storage:="_RECEIVE_COIN1", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property RECEIVE_COIN1() As Long
            Get
                Return _RECEIVE_COIN1
            End Get
            Set(ByVal value As Long)
               _RECEIVE_COIN1 = value
            End Set
        End Property 
        <Column(Storage:="_RECEIVE_COIN2", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property RECEIVE_COIN2() As Long
            Get
                Return _RECEIVE_COIN2
            End Get
            Set(ByVal value As Long)
               _RECEIVE_COIN2 = value
            End Set
        End Property 
        <Column(Storage:="_RECEIVE_COIN5", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property RECEIVE_COIN5() As Long
            Get
                Return _RECEIVE_COIN5
            End Get
            Set(ByVal value As Long)
               _RECEIVE_COIN5 = value
            End Set
        End Property 
        <Column(Storage:="_RECEIVE_COIN10", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property RECEIVE_COIN10() As Long
            Get
                Return _RECEIVE_COIN10
            End Get
            Set(ByVal value As Long)
               _RECEIVE_COIN10 = value
            End Set
        End Property 
        <Column(Storage:="_RECEIVE_BANKNOTE20", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property RECEIVE_BANKNOTE20() As Long
            Get
                Return _RECEIVE_BANKNOTE20
            End Get
            Set(ByVal value As Long)
               _RECEIVE_BANKNOTE20 = value
            End Set
        End Property 
        <Column(Storage:="_RECEIVE_BANKNOTE50", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property RECEIVE_BANKNOTE50() As Long
            Get
                Return _RECEIVE_BANKNOTE50
            End Get
            Set(ByVal value As Long)
               _RECEIVE_BANKNOTE50 = value
            End Set
        End Property 
        <Column(Storage:="_RECEIVE_BANKNOTE100", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property RECEIVE_BANKNOTE100() As Long
            Get
                Return _RECEIVE_BANKNOTE100
            End Get
            Set(ByVal value As Long)
               _RECEIVE_BANKNOTE100 = value
            End Set
        End Property 
        <Column(Storage:="_RECEIVE_BANKNOTE500", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property RECEIVE_BANKNOTE500() As Long
            Get
                Return _RECEIVE_BANKNOTE500
            End Get
            Set(ByVal value As Long)
               _RECEIVE_BANKNOTE500 = value
            End Set
        End Property 
        <Column(Storage:="_RECEIVE_BANKNOTE1000", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property RECEIVE_BANKNOTE1000() As Long
            Get
                Return _RECEIVE_BANKNOTE1000
            End Get
            Set(ByVal value As Long)
               _RECEIVE_BANKNOTE1000 = value
            End Set
        End Property 
        <Column(Storage:="_CHANGE_COIN1", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property CHANGE_COIN1() As Long
            Get
                Return _CHANGE_COIN1
            End Get
            Set(ByVal value As Long)
               _CHANGE_COIN1 = value
            End Set
        End Property 
        <Column(Storage:="_CHANGE_COIN2", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property CHANGE_COIN2() As Long
            Get
                Return _CHANGE_COIN2
            End Get
            Set(ByVal value As Long)
               _CHANGE_COIN2 = value
            End Set
        End Property 
        <Column(Storage:="_CHANGE_COIN5", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property CHANGE_COIN5() As Long
            Get
                Return _CHANGE_COIN5
            End Get
            Set(ByVal value As Long)
               _CHANGE_COIN5 = value
            End Set
        End Property 
        <Column(Storage:="_CHANGE_COIN10", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property CHANGE_COIN10() As Long
            Get
                Return _CHANGE_COIN10
            End Get
            Set(ByVal value As Long)
               _CHANGE_COIN10 = value
            End Set
        End Property 
        <Column(Storage:="_CHANGE_BANKNOTE20", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property CHANGE_BANKNOTE20() As Long
            Get
                Return _CHANGE_BANKNOTE20
            End Get
            Set(ByVal value As Long)
               _CHANGE_BANKNOTE20 = value
            End Set
        End Property 
        <Column(Storage:="_CHANGE_BANKNOTE50", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property CHANGE_BANKNOTE50() As Long
            Get
                Return _CHANGE_BANKNOTE50
            End Get
            Set(ByVal value As Long)
               _CHANGE_BANKNOTE50 = value
            End Set
        End Property 
        <Column(Storage:="_CHANGE_BANKNOTE100", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property CHANGE_BANKNOTE100() As Long
            Get
                Return _CHANGE_BANKNOTE100
            End Get
            Set(ByVal value As Long)
               _CHANGE_BANKNOTE100 = value
            End Set
        End Property 
        <Column(Storage:="_CHANGE_BANKNOTE500", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property CHANGE_BANKNOTE500() As Long
            Get
                Return _CHANGE_BANKNOTE500
            End Get
            Set(ByVal value As Long)
               _CHANGE_BANKNOTE500 = value
            End Set
        End Property 
        <Column(Storage:="_TRANS_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property TRANS_STATUS() As Char
            Get
                Return _TRANS_STATUS
            End Get
            Set(ByVal value As Char)
               _TRANS_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_MS_APP_STEP_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property MS_APP_STEP_ID() As Long
            Get
                Return _MS_APP_STEP_ID
            End Get
            Set(ByVal value As Long)
               _MS_APP_STEP_ID = value
            End Set
        End Property 
        <Column(Storage:="_SYNC_TO_SERVER", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property SYNC_TO_SERVER() As Char
            Get
                Return _SYNC_TO_SERVER
            End Get
            Set(ByVal value As Char)
               _SYNC_TO_SERVER = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
            _TRANS_NO = ""
            _TRANS_START_TIME = New DateTime(1,1,1)
            _TRANS_END_TIME = New DateTime(1,1,1)
            _MS_VENDING_ID = 0
            _MS_PRODUCT_ID = 0
            _SHELF_ID = 0
            _PRODUCT_COST = Nothing
            _PRODUCT_PRICE = Nothing
            _PRODUCT_DISCOUNT_PERCENT = Nothing
            _PRODUCT_NETPRICE = Nothing
            _PRODUCT_PROFIT_SHARING = Nothing
            _PAID_SUCCESS_TIME = New DateTime(1,1,1)
            _RECEIVE_COIN1 = 0
            _RECEIVE_COIN2 = 0
            _RECEIVE_COIN5 = 0
            _RECEIVE_COIN10 = 0
            _RECEIVE_BANKNOTE20 = 0
            _RECEIVE_BANKNOTE50 = 0
            _RECEIVE_BANKNOTE100 = 0
            _RECEIVE_BANKNOTE500 = 0
            _RECEIVE_BANKNOTE1000 = 0
            _CHANGE_COIN1 = 0
            _CHANGE_COIN2 = 0
            _CHANGE_COIN5 = 0
            _CHANGE_COIN10 = 0
            _CHANGE_BANKNOTE20 = 0
            _CHANGE_BANKNOTE50 = 0
            _CHANGE_BANKNOTE100 = 0
            _CHANGE_BANKNOTE500 = 0
            _TRANS_STATUS = "0"
            _MS_APP_STEP_ID = 0
            _SYNC_TO_SERVER = "N"
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_VENDING_SALE_TRANSACTION table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_VENDING_SALE_TRANSACTION table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_VENDING_SALE_TRANSACTION table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_VENDING_SALE_TRANSACTION table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_VENDING_SALE_TRANSACTION by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_VENDING_SALE_TRANSACTION by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbVendingSaleTransactionVendingLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_VENDING_SALE_TRANSACTION by specified TRANS_NO key is retrieved successfully.
        '/// <param name=cTRANS_NO>The TRANS_NO key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByTRANS_NO(cTRANS_NO As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_TRANS_NO", cTRANS_NO) 
            Return doChkData("TRANS_NO = @_TRANS_NO", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_VENDING_SALE_TRANSACTION by specified TRANS_NO key is retrieved successfully.
        '/// <param name=cTRANS_NO>The TRANS_NO key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByTRANS_NO(cTRANS_NO As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_TRANS_NO", cTRANS_NO) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("TRANS_NO = @_TRANS_NO And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_VENDING_SALE_TRANSACTION by specified MS_PRODUCT_ID_MS_VENDING_ID_SHELF_ID key is retrieved successfully.
        '/// <param name=cMS_PRODUCT_ID_MS_VENDING_ID_SHELF_ID>The MS_PRODUCT_ID_MS_VENDING_ID_SHELF_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByMS_PRODUCT_ID_MS_VENDING_ID_SHELF_ID(cMS_PRODUCT_ID As Long, cMS_VENDING_ID As Long, cSHELF_ID As Integer, trans As SQLTransaction) As Boolean
            Dim cmdPara(4)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MS_PRODUCT_ID", cMS_PRODUCT_ID) 
            cmdPara(1) = DB.SetText("@_MS_VENDING_ID", cMS_VENDING_ID) 
            cmdPara(2) = DB.SetText("@_SHELF_ID", cSHELF_ID) 
            Return doChkData("MS_PRODUCT_ID = @_MS_PRODUCT_ID AND MS_VENDING_ID = @_MS_VENDING_ID AND SHELF_ID = @_SHELF_ID", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_VENDING_SALE_TRANSACTION by specified MS_PRODUCT_ID_MS_VENDING_ID_SHELF_ID key is retrieved successfully.
        '/// <param name=cMS_PRODUCT_ID_MS_VENDING_ID_SHELF_ID>The MS_PRODUCT_ID_MS_VENDING_ID_SHELF_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByMS_PRODUCT_ID_MS_VENDING_ID_SHELF_ID(cMS_PRODUCT_ID As Long, cMS_VENDING_ID As Long, cSHELF_ID As Integer, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(4)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MS_PRODUCT_ID", cMS_PRODUCT_ID) 
            cmdPara(1) = DB.SetText("@_MS_VENDING_ID", cMS_VENDING_ID) 
            cmdPara(2) = DB.SetText("@_SHELF_ID", cSHELF_ID) 
            cmdPara(3) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("MS_PRODUCT_ID = @_MS_PRODUCT_ID AND MS_VENDING_ID = @_MS_VENDING_ID AND SHELF_ID = @_SHELF_ID And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_VENDING_SALE_TRANSACTION by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_VENDING_SALE_TRANSACTION table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_VENDING_SALE_TRANSACTION table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_VENDING_SALE_TRANSACTION table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(36) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(1).Value = _CREATED_BY.Trim

            cmbParam(2) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(2).Value = _CREATED_DATE

            cmbParam(3) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(3).Value = _UPDATED_BY.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(4).Value = _UPDATED_DATE.Value
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_TRANS_NO", SqlDbType.VarChar)
            cmbParam(5).Value = _TRANS_NO.Trim

            cmbParam(6) = New SqlParameter("@_TRANS_START_TIME", SqlDbType.DateTime)
            cmbParam(6).Value = _TRANS_START_TIME

            cmbParam(7) = New SqlParameter("@_TRANS_END_TIME", SqlDbType.DateTime)
            If _TRANS_END_TIME.Value.Year > 1 Then 
                cmbParam(7).Value = _TRANS_END_TIME.Value
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_MS_VENDING_ID", SqlDbType.BigInt)
            cmbParam(8).Value = _MS_VENDING_ID

            cmbParam(9) = New SqlParameter("@_MS_PRODUCT_ID", SqlDbType.BigInt)
            cmbParam(9).Value = _MS_PRODUCT_ID

            cmbParam(10) = New SqlParameter("@_SHELF_ID", SqlDbType.Int)
            cmbParam(10).Value = _SHELF_ID

            cmbParam(11) = New SqlParameter("@_PRODUCT_COST", SqlDbType.Float)
            If _PRODUCT_COST IsNot Nothing Then 
                cmbParam(11).Value = _PRODUCT_COST.Value
            Else
                cmbParam(11).Value = DBNull.value
            End IF

            cmbParam(12) = New SqlParameter("@_PRODUCT_PRICE", SqlDbType.Float)
            If _PRODUCT_PRICE IsNot Nothing Then 
                cmbParam(12).Value = _PRODUCT_PRICE.Value
            Else
                cmbParam(12).Value = DBNull.value
            End IF

            cmbParam(13) = New SqlParameter("@_PRODUCT_DISCOUNT_PERCENT", SqlDbType.Int)
            If _PRODUCT_DISCOUNT_PERCENT IsNot Nothing Then 
                cmbParam(13).Value = _PRODUCT_DISCOUNT_PERCENT.Value
            Else
                cmbParam(13).Value = DBNull.value
            End IF

            cmbParam(14) = New SqlParameter("@_PRODUCT_NETPRICE", SqlDbType.Float)
            If _PRODUCT_NETPRICE IsNot Nothing Then 
                cmbParam(14).Value = _PRODUCT_NETPRICE.Value
            Else
                cmbParam(14).Value = DBNull.value
            End IF

            cmbParam(15) = New SqlParameter("@_PRODUCT_PROFIT_SHARING", SqlDbType.Int)
            If _PRODUCT_PROFIT_SHARING IsNot Nothing Then 
                cmbParam(15).Value = _PRODUCT_PROFIT_SHARING.Value
            Else
                cmbParam(15).Value = DBNull.value
            End IF

            cmbParam(16) = New SqlParameter("@_PAID_SUCCESS_TIME", SqlDbType.DateTime)
            If _PAID_SUCCESS_TIME.Value.Year > 1 Then 
                cmbParam(16).Value = _PAID_SUCCESS_TIME.Value
            Else
                cmbParam(16).Value = DBNull.value
            End If

            cmbParam(17) = New SqlParameter("@_RECEIVE_COIN1", SqlDbType.Int)
            cmbParam(17).Value = _RECEIVE_COIN1

            cmbParam(18) = New SqlParameter("@_RECEIVE_COIN2", SqlDbType.Int)
            cmbParam(18).Value = _RECEIVE_COIN2

            cmbParam(19) = New SqlParameter("@_RECEIVE_COIN5", SqlDbType.Int)
            cmbParam(19).Value = _RECEIVE_COIN5

            cmbParam(20) = New SqlParameter("@_RECEIVE_COIN10", SqlDbType.Int)
            cmbParam(20).Value = _RECEIVE_COIN10

            cmbParam(21) = New SqlParameter("@_RECEIVE_BANKNOTE20", SqlDbType.Int)
            cmbParam(21).Value = _RECEIVE_BANKNOTE20

            cmbParam(22) = New SqlParameter("@_RECEIVE_BANKNOTE50", SqlDbType.Int)
            cmbParam(22).Value = _RECEIVE_BANKNOTE50

            cmbParam(23) = New SqlParameter("@_RECEIVE_BANKNOTE100", SqlDbType.Int)
            cmbParam(23).Value = _RECEIVE_BANKNOTE100

            cmbParam(24) = New SqlParameter("@_RECEIVE_BANKNOTE500", SqlDbType.Int)
            cmbParam(24).Value = _RECEIVE_BANKNOTE500

            cmbParam(25) = New SqlParameter("@_RECEIVE_BANKNOTE1000", SqlDbType.Int)
            cmbParam(25).Value = _RECEIVE_BANKNOTE1000

            cmbParam(26) = New SqlParameter("@_CHANGE_COIN1", SqlDbType.Int)
            cmbParam(26).Value = _CHANGE_COIN1

            cmbParam(27) = New SqlParameter("@_CHANGE_COIN2", SqlDbType.Int)
            cmbParam(27).Value = _CHANGE_COIN2

            cmbParam(28) = New SqlParameter("@_CHANGE_COIN5", SqlDbType.Int)
            cmbParam(28).Value = _CHANGE_COIN5

            cmbParam(29) = New SqlParameter("@_CHANGE_COIN10", SqlDbType.Int)
            cmbParam(29).Value = _CHANGE_COIN10

            cmbParam(30) = New SqlParameter("@_CHANGE_BANKNOTE20", SqlDbType.Int)
            cmbParam(30).Value = _CHANGE_BANKNOTE20

            cmbParam(31) = New SqlParameter("@_CHANGE_BANKNOTE50", SqlDbType.Int)
            cmbParam(31).Value = _CHANGE_BANKNOTE50

            cmbParam(32) = New SqlParameter("@_CHANGE_BANKNOTE100", SqlDbType.Int)
            cmbParam(32).Value = _CHANGE_BANKNOTE100

            cmbParam(33) = New SqlParameter("@_CHANGE_BANKNOTE500", SqlDbType.Int)
            cmbParam(33).Value = _CHANGE_BANKNOTE500

            cmbParam(34) = New SqlParameter("@_TRANS_STATUS", SqlDbType.Char)
            cmbParam(34).Value = _TRANS_STATUS

            cmbParam(35) = New SqlParameter("@_MS_APP_STEP_ID", SqlDbType.BigInt)
            cmbParam(35).Value = _MS_APP_STEP_ID

            cmbParam(36) = New SqlParameter("@_SYNC_TO_SERVER", SqlDbType.Char)
            cmbParam(36).Value = _SYNC_TO_SERVER

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_VENDING_SALE_TRANSACTION by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("trans_no")) = False Then _trans_no = Rdr("trans_no").ToString()
                        If Convert.IsDBNull(Rdr("trans_start_time")) = False Then _trans_start_time = Convert.ToDateTime(Rdr("trans_start_time"))
                        If Convert.IsDBNull(Rdr("trans_end_time")) = False Then _trans_end_time = Convert.ToDateTime(Rdr("trans_end_time"))
                        If Convert.IsDBNull(Rdr("ms_vending_id")) = False Then _ms_vending_id = Convert.ToInt64(Rdr("ms_vending_id"))
                        If Convert.IsDBNull(Rdr("ms_product_id")) = False Then _ms_product_id = Convert.ToInt64(Rdr("ms_product_id"))
                        If Convert.IsDBNull(Rdr("shelf_id")) = False Then _shelf_id = Convert.ToInt32(Rdr("shelf_id"))
                        If Convert.IsDBNull(Rdr("product_cost")) = False Then _product_cost = Convert.ToDouble(Rdr("product_cost"))
                        If Convert.IsDBNull(Rdr("product_price")) = False Then _product_price = Convert.ToDouble(Rdr("product_price"))
                        If Convert.IsDBNull(Rdr("product_discount_percent")) = False Then _product_discount_percent = Convert.ToInt32(Rdr("product_discount_percent"))
                        If Convert.IsDBNull(Rdr("product_netprice")) = False Then _product_netprice = Convert.ToDouble(Rdr("product_netprice"))
                        If Convert.IsDBNull(Rdr("product_profit_sharing")) = False Then _product_profit_sharing = Convert.ToInt32(Rdr("product_profit_sharing"))
                        If Convert.IsDBNull(Rdr("paid_success_time")) = False Then _paid_success_time = Convert.ToDateTime(Rdr("paid_success_time"))
                        If Convert.IsDBNull(Rdr("receive_coin1")) = False Then _receive_coin1 = Convert.ToInt32(Rdr("receive_coin1"))
                        If Convert.IsDBNull(Rdr("receive_coin2")) = False Then _receive_coin2 = Convert.ToInt32(Rdr("receive_coin2"))
                        If Convert.IsDBNull(Rdr("receive_coin5")) = False Then _receive_coin5 = Convert.ToInt32(Rdr("receive_coin5"))
                        If Convert.IsDBNull(Rdr("receive_coin10")) = False Then _receive_coin10 = Convert.ToInt32(Rdr("receive_coin10"))
                        If Convert.IsDBNull(Rdr("receive_banknote20")) = False Then _receive_banknote20 = Convert.ToInt32(Rdr("receive_banknote20"))
                        If Convert.IsDBNull(Rdr("receive_banknote50")) = False Then _receive_banknote50 = Convert.ToInt32(Rdr("receive_banknote50"))
                        If Convert.IsDBNull(Rdr("receive_banknote100")) = False Then _receive_banknote100 = Convert.ToInt32(Rdr("receive_banknote100"))
                        If Convert.IsDBNull(Rdr("receive_banknote500")) = False Then _receive_banknote500 = Convert.ToInt32(Rdr("receive_banknote500"))
                        If Convert.IsDBNull(Rdr("receive_banknote1000")) = False Then _receive_banknote1000 = Convert.ToInt32(Rdr("receive_banknote1000"))
                        If Convert.IsDBNull(Rdr("change_coin1")) = False Then _change_coin1 = Convert.ToInt32(Rdr("change_coin1"))
                        If Convert.IsDBNull(Rdr("change_coin2")) = False Then _change_coin2 = Convert.ToInt32(Rdr("change_coin2"))
                        If Convert.IsDBNull(Rdr("change_coin5")) = False Then _change_coin5 = Convert.ToInt32(Rdr("change_coin5"))
                        If Convert.IsDBNull(Rdr("change_coin10")) = False Then _change_coin10 = Convert.ToInt32(Rdr("change_coin10"))
                        If Convert.IsDBNull(Rdr("change_banknote20")) = False Then _change_banknote20 = Convert.ToInt32(Rdr("change_banknote20"))
                        If Convert.IsDBNull(Rdr("change_banknote50")) = False Then _change_banknote50 = Convert.ToInt32(Rdr("change_banknote50"))
                        If Convert.IsDBNull(Rdr("change_banknote100")) = False Then _change_banknote100 = Convert.ToInt32(Rdr("change_banknote100"))
                        If Convert.IsDBNull(Rdr("change_banknote500")) = False Then _change_banknote500 = Convert.ToInt32(Rdr("change_banknote500"))
                        If Convert.IsDBNull(Rdr("trans_status")) = False Then _trans_status = Rdr("trans_status").ToString()
                        If Convert.IsDBNull(Rdr("ms_app_step_id")) = False Then _ms_app_step_id = Convert.ToInt64(Rdr("ms_app_step_id"))
                        If Convert.IsDBNull(Rdr("sync_to_server")) = False Then _sync_to_server = Rdr("sync_to_server").ToString()
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_VENDING_SALE_TRANSACTION by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbVendingSaleTransactionVendingLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("trans_no")) = False Then _trans_no = Rdr("trans_no").ToString()
                        If Convert.IsDBNull(Rdr("trans_start_time")) = False Then _trans_start_time = Convert.ToDateTime(Rdr("trans_start_time"))
                        If Convert.IsDBNull(Rdr("trans_end_time")) = False Then _trans_end_time = Convert.ToDateTime(Rdr("trans_end_time"))
                        If Convert.IsDBNull(Rdr("ms_vending_id")) = False Then _ms_vending_id = Convert.ToInt64(Rdr("ms_vending_id"))
                        If Convert.IsDBNull(Rdr("ms_product_id")) = False Then _ms_product_id = Convert.ToInt64(Rdr("ms_product_id"))
                        If Convert.IsDBNull(Rdr("shelf_id")) = False Then _shelf_id = Convert.ToInt32(Rdr("shelf_id"))
                        If Convert.IsDBNull(Rdr("product_cost")) = False Then _product_cost = Convert.ToDouble(Rdr("product_cost"))
                        If Convert.IsDBNull(Rdr("product_price")) = False Then _product_price = Convert.ToDouble(Rdr("product_price"))
                        If Convert.IsDBNull(Rdr("product_discount_percent")) = False Then _product_discount_percent = Convert.ToInt32(Rdr("product_discount_percent"))
                        If Convert.IsDBNull(Rdr("product_netprice")) = False Then _product_netprice = Convert.ToDouble(Rdr("product_netprice"))
                        If Convert.IsDBNull(Rdr("product_profit_sharing")) = False Then _product_profit_sharing = Convert.ToInt32(Rdr("product_profit_sharing"))
                        If Convert.IsDBNull(Rdr("paid_success_time")) = False Then _paid_success_time = Convert.ToDateTime(Rdr("paid_success_time"))
                        If Convert.IsDBNull(Rdr("receive_coin1")) = False Then _receive_coin1 = Convert.ToInt32(Rdr("receive_coin1"))
                        If Convert.IsDBNull(Rdr("receive_coin2")) = False Then _receive_coin2 = Convert.ToInt32(Rdr("receive_coin2"))
                        If Convert.IsDBNull(Rdr("receive_coin5")) = False Then _receive_coin5 = Convert.ToInt32(Rdr("receive_coin5"))
                        If Convert.IsDBNull(Rdr("receive_coin10")) = False Then _receive_coin10 = Convert.ToInt32(Rdr("receive_coin10"))
                        If Convert.IsDBNull(Rdr("receive_banknote20")) = False Then _receive_banknote20 = Convert.ToInt32(Rdr("receive_banknote20"))
                        If Convert.IsDBNull(Rdr("receive_banknote50")) = False Then _receive_banknote50 = Convert.ToInt32(Rdr("receive_banknote50"))
                        If Convert.IsDBNull(Rdr("receive_banknote100")) = False Then _receive_banknote100 = Convert.ToInt32(Rdr("receive_banknote100"))
                        If Convert.IsDBNull(Rdr("receive_banknote500")) = False Then _receive_banknote500 = Convert.ToInt32(Rdr("receive_banknote500"))
                        If Convert.IsDBNull(Rdr("receive_banknote1000")) = False Then _receive_banknote1000 = Convert.ToInt32(Rdr("receive_banknote1000"))
                        If Convert.IsDBNull(Rdr("change_coin1")) = False Then _change_coin1 = Convert.ToInt32(Rdr("change_coin1"))
                        If Convert.IsDBNull(Rdr("change_coin2")) = False Then _change_coin2 = Convert.ToInt32(Rdr("change_coin2"))
                        If Convert.IsDBNull(Rdr("change_coin5")) = False Then _change_coin5 = Convert.ToInt32(Rdr("change_coin5"))
                        If Convert.IsDBNull(Rdr("change_coin10")) = False Then _change_coin10 = Convert.ToInt32(Rdr("change_coin10"))
                        If Convert.IsDBNull(Rdr("change_banknote20")) = False Then _change_banknote20 = Convert.ToInt32(Rdr("change_banknote20"))
                        If Convert.IsDBNull(Rdr("change_banknote50")) = False Then _change_banknote50 = Convert.ToInt32(Rdr("change_banknote50"))
                        If Convert.IsDBNull(Rdr("change_banknote100")) = False Then _change_banknote100 = Convert.ToInt32(Rdr("change_banknote100"))
                        If Convert.IsDBNull(Rdr("change_banknote500")) = False Then _change_banknote500 = Convert.ToInt32(Rdr("change_banknote500"))
                        If Convert.IsDBNull(Rdr("trans_status")) = False Then _trans_status = Rdr("trans_status").ToString()
                        If Convert.IsDBNull(Rdr("ms_app_step_id")) = False Then _ms_app_step_id = Convert.ToInt64(Rdr("ms_app_step_id"))
                        If Convert.IsDBNull(Rdr("sync_to_server")) = False Then _sync_to_server = Rdr("sync_to_server").ToString()
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_VENDING_SALE_TRANSACTION
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (CREATED_BY, CREATED_DATE, TRANS_NO, TRANS_START_TIME, TRANS_END_TIME, MS_VENDING_ID, MS_PRODUCT_ID, SHELF_ID, PRODUCT_COST, PRODUCT_PRICE, PRODUCT_DISCOUNT_PERCENT, PRODUCT_NETPRICE, PRODUCT_PROFIT_SHARING, PAID_SUCCESS_TIME, RECEIVE_COIN1, RECEIVE_COIN2, RECEIVE_COIN5, RECEIVE_COIN10, RECEIVE_BANKNOTE20, RECEIVE_BANKNOTE50, RECEIVE_BANKNOTE100, RECEIVE_BANKNOTE500, RECEIVE_BANKNOTE1000, CHANGE_COIN1, CHANGE_COIN2, CHANGE_COIN5, CHANGE_COIN10, CHANGE_BANKNOTE20, CHANGE_BANKNOTE50, CHANGE_BANKNOTE100, CHANGE_BANKNOTE500, TRANS_STATUS, MS_APP_STEP_ID, SYNC_TO_SERVER)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE, INSERTED.TRANS_NO, INSERTED.TRANS_START_TIME, INSERTED.TRANS_END_TIME, INSERTED.MS_VENDING_ID, INSERTED.MS_PRODUCT_ID, INSERTED.SHELF_ID, INSERTED.PRODUCT_COST, INSERTED.PRODUCT_PRICE, INSERTED.PRODUCT_DISCOUNT_PERCENT, INSERTED.PRODUCT_NETPRICE, INSERTED.PRODUCT_PROFIT_SHARING, INSERTED.PAID_SUCCESS_TIME, INSERTED.RECEIVE_COIN1, INSERTED.RECEIVE_COIN2, INSERTED.RECEIVE_COIN5, INSERTED.RECEIVE_COIN10, INSERTED.RECEIVE_BANKNOTE20, INSERTED.RECEIVE_BANKNOTE50, INSERTED.RECEIVE_BANKNOTE100, INSERTED.RECEIVE_BANKNOTE500, INSERTED.RECEIVE_BANKNOTE1000, INSERTED.CHANGE_COIN1, INSERTED.CHANGE_COIN2, INSERTED.CHANGE_COIN5, INSERTED.CHANGE_COIN10, INSERTED.CHANGE_BANKNOTE20, INSERTED.CHANGE_BANKNOTE50, INSERTED.CHANGE_BANKNOTE100, INSERTED.CHANGE_BANKNOTE500, INSERTED.TRANS_STATUS, INSERTED.MS_APP_STEP_ID, INSERTED.SYNC_TO_SERVER"
                Sql += " VALUES("
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_TRANS_NO" & ", "
                sql += "@_TRANS_START_TIME" & ", "
                sql += "@_TRANS_END_TIME" & ", "
                sql += "@_MS_VENDING_ID" & ", "
                sql += "@_MS_PRODUCT_ID" & ", "
                sql += "@_SHELF_ID" & ", "
                sql += "@_PRODUCT_COST" & ", "
                sql += "@_PRODUCT_PRICE" & ", "
                sql += "@_PRODUCT_DISCOUNT_PERCENT" & ", "
                sql += "@_PRODUCT_NETPRICE" & ", "
                sql += "@_PRODUCT_PROFIT_SHARING" & ", "
                sql += "@_PAID_SUCCESS_TIME" & ", "
                sql += "@_RECEIVE_COIN1" & ", "
                sql += "@_RECEIVE_COIN2" & ", "
                sql += "@_RECEIVE_COIN5" & ", "
                sql += "@_RECEIVE_COIN10" & ", "
                sql += "@_RECEIVE_BANKNOTE20" & ", "
                sql += "@_RECEIVE_BANKNOTE50" & ", "
                sql += "@_RECEIVE_BANKNOTE100" & ", "
                sql += "@_RECEIVE_BANKNOTE500" & ", "
                sql += "@_RECEIVE_BANKNOTE1000" & ", "
                sql += "@_CHANGE_COIN1" & ", "
                sql += "@_CHANGE_COIN2" & ", "
                sql += "@_CHANGE_COIN5" & ", "
                sql += "@_CHANGE_COIN10" & ", "
                sql += "@_CHANGE_BANKNOTE20" & ", "
                sql += "@_CHANGE_BANKNOTE50" & ", "
                sql += "@_CHANGE_BANKNOTE100" & ", "
                sql += "@_CHANGE_BANKNOTE500" & ", "
                sql += "@_TRANS_STATUS" & ", "
                sql += "@_MS_APP_STEP_ID" & ", "
                sql += "@_SYNC_TO_SERVER"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_VENDING_SALE_TRANSACTION
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" & ", "
                Sql += "TRANS_NO = " & "@_TRANS_NO" & ", "
                Sql += "TRANS_START_TIME = " & "@_TRANS_START_TIME" & ", "
                Sql += "TRANS_END_TIME = " & "@_TRANS_END_TIME" & ", "
                Sql += "MS_VENDING_ID = " & "@_MS_VENDING_ID" & ", "
                Sql += "MS_PRODUCT_ID = " & "@_MS_PRODUCT_ID" & ", "
                Sql += "SHELF_ID = " & "@_SHELF_ID" & ", "
                Sql += "PRODUCT_COST = " & "@_PRODUCT_COST" & ", "
                Sql += "PRODUCT_PRICE = " & "@_PRODUCT_PRICE" & ", "
                Sql += "PRODUCT_DISCOUNT_PERCENT = " & "@_PRODUCT_DISCOUNT_PERCENT" & ", "
                Sql += "PRODUCT_NETPRICE = " & "@_PRODUCT_NETPRICE" & ", "
                Sql += "PRODUCT_PROFIT_SHARING = " & "@_PRODUCT_PROFIT_SHARING" & ", "
                Sql += "PAID_SUCCESS_TIME = " & "@_PAID_SUCCESS_TIME" & ", "
                Sql += "RECEIVE_COIN1 = " & "@_RECEIVE_COIN1" & ", "
                Sql += "RECEIVE_COIN2 = " & "@_RECEIVE_COIN2" & ", "
                Sql += "RECEIVE_COIN5 = " & "@_RECEIVE_COIN5" & ", "
                Sql += "RECEIVE_COIN10 = " & "@_RECEIVE_COIN10" & ", "
                Sql += "RECEIVE_BANKNOTE20 = " & "@_RECEIVE_BANKNOTE20" & ", "
                Sql += "RECEIVE_BANKNOTE50 = " & "@_RECEIVE_BANKNOTE50" & ", "
                Sql += "RECEIVE_BANKNOTE100 = " & "@_RECEIVE_BANKNOTE100" & ", "
                Sql += "RECEIVE_BANKNOTE500 = " & "@_RECEIVE_BANKNOTE500" & ", "
                Sql += "RECEIVE_BANKNOTE1000 = " & "@_RECEIVE_BANKNOTE1000" & ", "
                Sql += "CHANGE_COIN1 = " & "@_CHANGE_COIN1" & ", "
                Sql += "CHANGE_COIN2 = " & "@_CHANGE_COIN2" & ", "
                Sql += "CHANGE_COIN5 = " & "@_CHANGE_COIN5" & ", "
                Sql += "CHANGE_COIN10 = " & "@_CHANGE_COIN10" & ", "
                Sql += "CHANGE_BANKNOTE20 = " & "@_CHANGE_BANKNOTE20" & ", "
                Sql += "CHANGE_BANKNOTE50 = " & "@_CHANGE_BANKNOTE50" & ", "
                Sql += "CHANGE_BANKNOTE100 = " & "@_CHANGE_BANKNOTE100" & ", "
                Sql += "CHANGE_BANKNOTE500 = " & "@_CHANGE_BANKNOTE500" & ", "
                Sql += "TRANS_STATUS = " & "@_TRANS_STATUS" & ", "
                Sql += "MS_APP_STEP_ID = " & "@_MS_APP_STEP_ID" & ", "
                Sql += "SYNC_TO_SERVER = " & "@_SYNC_TO_SERVER" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_VENDING_SALE_TRANSACTION
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_VENDING_SALE_TRANSACTION
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE, TRANS_NO, TRANS_START_TIME, TRANS_END_TIME, MS_VENDING_ID, MS_PRODUCT_ID, SHELF_ID, PRODUCT_COST, PRODUCT_PRICE, PRODUCT_DISCOUNT_PERCENT, PRODUCT_NETPRICE, PRODUCT_PROFIT_SHARING, PAID_SUCCESS_TIME, RECEIVE_COIN1, RECEIVE_COIN2, RECEIVE_COIN5, RECEIVE_COIN10, RECEIVE_BANKNOTE20, RECEIVE_BANKNOTE50, RECEIVE_BANKNOTE100, RECEIVE_BANKNOTE500, RECEIVE_BANKNOTE1000, CHANGE_COIN1, CHANGE_COIN2, CHANGE_COIN5, CHANGE_COIN10, CHANGE_BANKNOTE20, CHANGE_BANKNOTE50, CHANGE_BANKNOTE100, CHANGE_BANKNOTE500, TRANS_STATUS, MS_APP_STEP_ID, SYNC_TO_SERVER FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
