Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = VendingLinqDB.ConnectDB.VendingDB
Imports VendingLinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for CF_SALE_SLIP table VendingLinqDB.
    '[Create by  on September, 21 2016]
    Public Class CfSaleSlipVendingLinqDB
        Public sub CfSaleSlipVendingLinqDB()

        End Sub 
        ' CF_SALE_SLIP
        Const _tableName As String = "CF_SALE_SLIP"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As DateTime = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _CTL_TYPE As Char = ""
        Dim _CTL_LOCATION_X As Long = 0
        Dim _CTL_LOCATION_Y As Long = 0
        Dim _CTL_TEXT As  String  = ""
        Dim _CTL_TEXT_WIDTH As  System.Nullable(Of Long) 
        Dim _CTL_TEXT_ALIGN As  System.Nullable(Of Char)  = ""
        Dim _CTL_TEXT_FONTNAME As  String  = ""
        Dim _CTL_TEXT_FONTSIZE As  System.Nullable(Of Double) 
        Dim _CTL_TEXT_FONTSTYLE As  System.Nullable(Of Char)  = ""
        Dim _CTL_IMAGE() As Byte

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATED_DATE() As DateTime
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As DateTime)
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_CTL_TYPE", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property CTL_TYPE() As Char
            Get
                Return _CTL_TYPE
            End Get
            Set(ByVal value As Char)
               _CTL_TYPE = value
            End Set
        End Property 
        <Column(Storage:="_CTL_LOCATION_X", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property CTL_LOCATION_X() As Long
            Get
                Return _CTL_LOCATION_X
            End Get
            Set(ByVal value As Long)
               _CTL_LOCATION_X = value
            End Set
        End Property 
        <Column(Storage:="_CTL_LOCATION_Y", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property CTL_LOCATION_Y() As Long
            Get
                Return _CTL_LOCATION_Y
            End Get
            Set(ByVal value As Long)
               _CTL_LOCATION_Y = value
            End Set
        End Property 
        <Column(Storage:="_CTL_TEXT", DbType:="VarChar(255)")>  _
        Public Property CTL_TEXT() As  String 
            Get
                Return _CTL_TEXT
            End Get
            Set(ByVal value As  String )
               _CTL_TEXT = value
            End Set
        End Property 
        <Column(Storage:="_CTL_TEXT_WIDTH", DbType:="Int")>  _
        Public Property CTL_TEXT_WIDTH() As  System.Nullable(Of Long) 
            Get
                Return _CTL_TEXT_WIDTH
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _CTL_TEXT_WIDTH = value
            End Set
        End Property 
        <Column(Storage:="_CTL_TEXT_ALIGN", DbType:="Char(1)")>  _
        Public Property CTL_TEXT_ALIGN() As  System.Nullable(Of Char) 
            Get
                Return _CTL_TEXT_ALIGN
            End Get
            Set(ByVal value As  System.Nullable(Of Char) )
               _CTL_TEXT_ALIGN = value
            End Set
        End Property 
        <Column(Storage:="_CTL_TEXT_FONTNAME", DbType:="VarChar(255)")>  _
        Public Property CTL_TEXT_FONTNAME() As  String 
            Get
                Return _CTL_TEXT_FONTNAME
            End Get
            Set(ByVal value As  String )
               _CTL_TEXT_FONTNAME = value
            End Set
        End Property 
        <Column(Storage:="_CTL_TEXT_FONTSIZE", DbType:="Float")>  _
        Public Property CTL_TEXT_FONTSIZE() As  System.Nullable(Of Double) 
            Get
                Return _CTL_TEXT_FONTSIZE
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _CTL_TEXT_FONTSIZE = value
            End Set
        End Property 
        <Column(Storage:="_CTL_TEXT_FONTSTYLE", DbType:="Char(1)")>  _
        Public Property CTL_TEXT_FONTSTYLE() As  System.Nullable(Of Char) 
            Get
                Return _CTL_TEXT_FONTSTYLE
            End Get
            Set(ByVal value As  System.Nullable(Of Char) )
               _CTL_TEXT_FONTSTYLE = value
            End Set
        End Property 
        <Column(Storage:="_CTL_IMAGE", DbType:="IMAGE")>  _
        Public Property CTL_IMAGE() As  Byte() 
            Get
                Return _CTL_IMAGE
            End Get
            Set(ByVal value() As Byte)
               _CTL_IMAGE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
            _CTL_TYPE = ""
            _CTL_LOCATION_X = 0
            _CTL_LOCATION_Y = 0
            _CTL_TEXT = ""
            _CTL_TEXT_WIDTH = Nothing
            _CTL_TEXT_ALIGN = ""
            _CTL_TEXT_FONTNAME = ""
            _CTL_TEXT_FONTSIZE = Nothing
            _CTL_TEXT_FONTSTYLE = ""
             _CTL_IMAGE = Nothing
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into CF_SALE_SLIP table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to CF_SALE_SLIP table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to CF_SALE_SLIP table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from CF_SALE_SLIP table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of CF_SALE_SLIP by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of CF_SALE_SLIP by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As CfSaleSlipVendingLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of CF_SALE_SLIP by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into CF_SALE_SLIP table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to CF_SALE_SLIP table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from CF_SALE_SLIP table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(14) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            cmbParam(1).Value = _CREATED_BY.Trim

            cmbParam(2) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            cmbParam(2).Value = _CREATED_DATE

            cmbParam(3) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(3).Value = _UPDATED_BY.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(4).Value = _UPDATED_DATE.Value
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_CTL_TYPE", SqlDbType.Char)
            cmbParam(5).Value = _CTL_TYPE

            cmbParam(6) = New SqlParameter("@_CTL_LOCATION_X", SqlDbType.Int)
            cmbParam(6).Value = _CTL_LOCATION_X

            cmbParam(7) = New SqlParameter("@_CTL_LOCATION_Y", SqlDbType.Int)
            cmbParam(7).Value = _CTL_LOCATION_Y

            cmbParam(8) = New SqlParameter("@_CTL_TEXT", SqlDbType.VarChar)
            If _CTL_TEXT.Trim <> "" Then 
                cmbParam(8).Value = _CTL_TEXT.Trim
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_CTL_TEXT_WIDTH", SqlDbType.Int)
            If _CTL_TEXT_WIDTH IsNot Nothing Then 
                cmbParam(9).Value = _CTL_TEXT_WIDTH.Value
            Else
                cmbParam(9).Value = DBNull.value
            End IF

            cmbParam(10) = New SqlParameter("@_CTL_TEXT_ALIGN", SqlDbType.Char)
            If _CTL_TEXT_ALIGN.Value <> "" Then 
                cmbParam(10).Value = _CTL_TEXT_ALIGN.Value
            Else
                cmbParam(10).Value = DBNull.value
            End IF

            cmbParam(11) = New SqlParameter("@_CTL_TEXT_FONTNAME", SqlDbType.VarChar)
            If _CTL_TEXT_FONTNAME.Trim <> "" Then 
                cmbParam(11).Value = _CTL_TEXT_FONTNAME.Trim
            Else
                cmbParam(11).Value = DBNull.value
            End If

            cmbParam(12) = New SqlParameter("@_CTL_TEXT_FONTSIZE", SqlDbType.Float)
            If _CTL_TEXT_FONTSIZE IsNot Nothing Then 
                cmbParam(12).Value = _CTL_TEXT_FONTSIZE.Value
            Else
                cmbParam(12).Value = DBNull.value
            End IF

            cmbParam(13) = New SqlParameter("@_CTL_TEXT_FONTSTYLE", SqlDbType.Char)
            If _CTL_TEXT_FONTSTYLE.Value <> "" Then 
                cmbParam(13).Value = _CTL_TEXT_FONTSTYLE.Value
            Else
                cmbParam(13).Value = DBNull.value
            End IF

            If _CTL_IMAGE IsNot Nothing Then 
                cmbParam(14) = New SqlParameter("@_CTL_IMAGE",SqlDbType.Image, _CTL_IMAGE.Length)
                cmbParam(14).Value = _CTL_IMAGE
            Else
                cmbParam(14) = New SqlParameter("@_CTL_IMAGE", SqlDbType.Image)
                cmbParam(14).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of CF_SALE_SLIP by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("ctl_type")) = False Then _ctl_type = Rdr("ctl_type").ToString()
                        If Convert.IsDBNull(Rdr("ctl_location_x")) = False Then _ctl_location_x = Convert.ToInt32(Rdr("ctl_location_x"))
                        If Convert.IsDBNull(Rdr("ctl_location_y")) = False Then _ctl_location_y = Convert.ToInt32(Rdr("ctl_location_y"))
                        If Convert.IsDBNull(Rdr("ctl_text")) = False Then _ctl_text = Rdr("ctl_text").ToString()
                        If Convert.IsDBNull(Rdr("ctl_text_width")) = False Then _ctl_text_width = Convert.ToInt32(Rdr("ctl_text_width"))
                        If Convert.IsDBNull(Rdr("ctl_text_align")) = False Then _ctl_text_align = Rdr("ctl_text_align").ToString()
                        If Convert.IsDBNull(Rdr("ctl_text_fontname")) = False Then _ctl_text_fontname = Rdr("ctl_text_fontname").ToString()
                        If Convert.IsDBNull(Rdr("ctl_text_fontsize")) = False Then _ctl_text_fontsize = Convert.ToDouble(Rdr("ctl_text_fontsize"))
                        If Convert.IsDBNull(Rdr("ctl_text_fontstyle")) = False Then _ctl_text_fontstyle = Rdr("ctl_text_fontstyle").ToString()
                        If Convert.IsDBNull(Rdr("ctl_image")) = False Then _ctl_image = CType(Rdr("ctl_image"), Byte())
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of CF_SALE_SLIP by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As CfSaleSlipVendingLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("ctl_type")) = False Then _ctl_type = Rdr("ctl_type").ToString()
                        If Convert.IsDBNull(Rdr("ctl_location_x")) = False Then _ctl_location_x = Convert.ToInt32(Rdr("ctl_location_x"))
                        If Convert.IsDBNull(Rdr("ctl_location_y")) = False Then _ctl_location_y = Convert.ToInt32(Rdr("ctl_location_y"))
                        If Convert.IsDBNull(Rdr("ctl_text")) = False Then _ctl_text = Rdr("ctl_text").ToString()
                        If Convert.IsDBNull(Rdr("ctl_text_width")) = False Then _ctl_text_width = Convert.ToInt32(Rdr("ctl_text_width"))
                        If Convert.IsDBNull(Rdr("ctl_text_align")) = False Then _ctl_text_align = Rdr("ctl_text_align").ToString()
                        If Convert.IsDBNull(Rdr("ctl_text_fontname")) = False Then _ctl_text_fontname = Rdr("ctl_text_fontname").ToString()
                        If Convert.IsDBNull(Rdr("ctl_text_fontsize")) = False Then _ctl_text_fontsize = Convert.ToDouble(Rdr("ctl_text_fontsize"))
                        If Convert.IsDBNull(Rdr("ctl_text_fontstyle")) = False Then _ctl_text_fontstyle = Rdr("ctl_text_fontstyle").ToString()
                        If Convert.IsDBNull(Rdr("ctl_image")) = False Then _ctl_image = CType(Rdr("ctl_image"), Byte())
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table CF_SALE_SLIP
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (CREATED_BY, CREATED_DATE, CTL_TYPE, CTL_LOCATION_X, CTL_LOCATION_Y, CTL_TEXT, CTL_TEXT_WIDTH, CTL_TEXT_ALIGN, CTL_TEXT_FONTNAME, CTL_TEXT_FONTSIZE, CTL_TEXT_FONTSTYLE, CTL_IMAGE)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE, INSERTED.CTL_TYPE, INSERTED.CTL_LOCATION_X, INSERTED.CTL_LOCATION_Y, INSERTED.CTL_TEXT, INSERTED.CTL_TEXT_WIDTH, INSERTED.CTL_TEXT_ALIGN, INSERTED.CTL_TEXT_FONTNAME, INSERTED.CTL_TEXT_FONTSIZE, INSERTED.CTL_TEXT_FONTSTYLE, INSERTED.CTL_IMAGE"
                Sql += " VALUES("
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE" & ", "
                sql += "@_CTL_TYPE" & ", "
                sql += "@_CTL_LOCATION_X" & ", "
                sql += "@_CTL_LOCATION_Y" & ", "
                sql += "@_CTL_TEXT" & ", "
                sql += "@_CTL_TEXT_WIDTH" & ", "
                sql += "@_CTL_TEXT_ALIGN" & ", "
                sql += "@_CTL_TEXT_FONTNAME" & ", "
                sql += "@_CTL_TEXT_FONTSIZE" & ", "
                sql += "@_CTL_TEXT_FONTSTYLE" & ", "
                sql += "@_CTL_IMAGE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table CF_SALE_SLIP
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" & ", "
                Sql += "CTL_TYPE = " & "@_CTL_TYPE" & ", "
                Sql += "CTL_LOCATION_X = " & "@_CTL_LOCATION_X" & ", "
                Sql += "CTL_LOCATION_Y = " & "@_CTL_LOCATION_Y" & ", "
                Sql += "CTL_TEXT = " & "@_CTL_TEXT" & ", "
                Sql += "CTL_TEXT_WIDTH = " & "@_CTL_TEXT_WIDTH" & ", "
                Sql += "CTL_TEXT_ALIGN = " & "@_CTL_TEXT_ALIGN" & ", "
                Sql += "CTL_TEXT_FONTNAME = " & "@_CTL_TEXT_FONTNAME" & ", "
                Sql += "CTL_TEXT_FONTSIZE = " & "@_CTL_TEXT_FONTSIZE" & ", "
                Sql += "CTL_TEXT_FONTSTYLE = " & "@_CTL_TEXT_FONTSTYLE" & ", "
                Sql += "CTL_IMAGE = " & "@_CTL_IMAGE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table CF_SALE_SLIP
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table CF_SALE_SLIP
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE, CTL_TYPE, CTL_LOCATION_X, CTL_LOCATION_Y, CTL_TEXT, CTL_TEXT_WIDTH, CTL_TEXT_ALIGN, CTL_TEXT_FONTNAME, CTL_TEXT_FONTSIZE, CTL_TEXT_FONTSTYLE, CTL_IMAGE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
