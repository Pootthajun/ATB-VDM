﻿Imports System.Data
Imports System.Data.SqlClient
Imports VendingLinqDB.ConnectDB
Imports VendingLinqDB.TABLE
Imports System.Reflection
Imports System.Windows.Forms
Imports System.IO
Imports System.Web

Public Class SyncDataVDMServerENG

    Public Function GetVendingSysConfig() As DataTable
        Dim trans As New VendingTransactionDB
        Dim cfLnq As New CfVendingSysconfigVendingLinqDB
        Dim dt As DataTable = cfLnq.GetDataList("", "", trans.Trans, Nothing)
        trans.CommitTransaction()
        Return dt
    End Function

    Public Sub SyncVDMToServer(vending_id As Long, wsUrl As String)
        SyncVDMToServer_CfVendingSysConfig(vending_id, wsUrl)
        SyncVDMToServer_MsProductShelf(vending_id, wsUrl)
        SyncVDMToServer_MsVendingShelf(vending_id, wsUrl)
        SyncVDMToServer_LogVendingActivity(vending_id, wsUrl)
        SyncVDMToServer_LogVendingAgent(vending_id, wsUrl)
        SyncVDMToServer_LogVendingFillMoney(vending_id, wsUrl)
        SyncVDMToServer_TbProductMovement(vending_id, wsUrl)
        SyncVDMToServer_TbStaffConsoleTransaction(vending_id, wsUrl)
        SyncVDMToServer_TbVendingSaleTransaction(vending_id, wsUrl)
        SyncVDMToServer_MsVendingDevice(vending_id, wsUrl)
    End Sub

    Public Sub SyncServerToVDM(vending_id As Long, wsUrl As String)
        SyncServerToVDM_CfVendingSysConfig(vending_id, wsUrl)
        SyncServerToVDM_MsVendingDevice(vending_id, wsUrl)
        SyncServerToVDM_VDO_ADVERTISE(vending_id, wsUrl)
    End Sub

#Region "_SyncVDMServer"

#Region "CfVendingSysConfig"
    Public Sub SyncServerToVDM_CfVendingSysConfig(vending_id As Long, wsUrl As String)
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim ws As New VDMWebService.ATBVendingWebservice
        ws.Url = wsUrl

        '## Get Vending SysConfig From Server wheresync_to_vending='N'
        Dim dt As New DataTable
        ws.Timeout = 10000
        dt = ws.GetServerCfVendingSysConfig(vending_id)

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim vTrans As New VendingTransactionDB
        Try


            '## Update To Local Vending
            Dim ret As New ExecuteDataInfo
            Dim vcf As New CfVendingSysconfigVendingLinqDB
            vcf.ChkDataByMS_VENDING_ID(vending_id, vTrans.Trans)
            If vcf.ID > 0 Then
                With vcf
                    Dim dr As DataRow
                    dr = dt.Rows(0)

                    .SCREEN_LAYOUT = dr("SCREEN_LAYOUT").ToString
                    .SCREEN_SAVER_SEC = dr("SCREEN_SAVER_SEC").ToString
                    .TIME_OUT_SEC = dr("TIME_OUT_SEC").ToString
                    .SHOW_MSG_SEC = dr("SHOW_MSG_SEC").ToString
                    .PAYMENT_EXTEND_SEC = dr("PAYMENT_EXTEND_SEC").ToString
                    .SHELF_LONG = dr("SHELF_LONG").ToString
                    .SLEEP_TIME = dr("SLEEP_TIME").ToString
                    .SLEEP_DURATION = dr("SLEEP_DURATION").ToString
                    .VDM_WEBSERVICE_URL = dr("VDM_WEBSERVICE_URL").ToString
                    .ALARM_WEBSERVICE_URL = dr("ALARM_WEBSERVICE_URL").ToString
                    .MOTOR_PARK_POSITION = dr("MOTOR_PARK_POSITION").ToString
                    .MOTOR_ORIGIN_POSITION = dr("MOTOR_ORIGIN_POSITION").ToString
                    .MOTOR_DELIVERY_POSITION = dr("MOTOR_DELIVERY_POSITION").ToString
                    .ADS_VDO_PATH = dr("ADS_VDO_PATH").ToString
                    .INTERVAL_SYNC_TO_VENDING_MIN = dr("INTERVAL_SYNC_TO_VENDING_MIN").ToString
                    .INTERVAL_SYNC_TO_SERVER_MIN = dr("INTERVAL_SYNC_TO_SERVER_MIN").ToString
                    '.INTERVAL_SYNC_TRANSACTION_MIN = dr("INTERVAL_SYNC_TRANSACTION_MIN").ToString
                    '.INTERVAL_SYNC_MASTER_MIN = dr("INTERVAL_SYNC_MASTER_MIN").ToString
                    '.INTERVAL_SYNC_LOG_MIN = dr("INTERVAL_SYNC_LOG_MIN").ToString
                    .SYNC_TO_VENDING = "Y"
                    ret = .UpdateData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
                End With
            End If

            If ret.IsSuccess = True Then

                '## Update Cf_Vending_SysConfig.Sync_To_Vending = ''Y  On Server
                Dim wsRet As Boolean = False
                ws.Timeout = 10000
                wsRet = ws.UpdateSyncToVDM_CfVendingSysConfig(vending_id)

                If wsRet = False Then
                    Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice UpdateSyncToVDM_CfVendingSysConfig")
                End If

                vTrans.CommitTransaction()
            Else
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateErrorLogAgent(vending_id, ret.ErrorMessage)
            End If
        Catch ex As Exception
            vTrans.RollbackTransaction()
        End Try
    End Sub

    Public Sub SyncVDMToServer_CfVendingSysConfig(vending_id As Long, wsUrl As String)
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        If vending_id = 0 Then
            Exit Sub
        End If

        Dim vTrans As New VendingTransactionDB
        Try

            '## Get Vending SysConfig From Local Vending Where Sync_To_Server ='N'
            Dim sql As String = "select * from cf_vending_sysconfig where sync_to_server='N' and ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_VENDING_ID", vending_id)
            Dim dt As New DataTable
            dt = VendingDB.ExecuteTable(sql, vTrans.Trans, p)
            If dt.Rows.Count > 0 Then
                Dim dr As DataRow
                dr = dt.Rows(0)

                Dim ret As Boolean
                Dim vscf As New VDMWebService.CfVendingSysconfigVendingLinqDB
                With vscf
                    .SCREEN_LAYOUT = dr("SCREEN_LAYOUT").ToString
                    .SCREEN_SAVER_SEC = dr("SCREEN_SAVER_SEC").ToString
                    .TIME_OUT_SEC = dr("TIME_OUT_SEC").ToString
                    .SHOW_MSG_SEC = dr("SHOW_MSG_SEC").ToString
                    .PAYMENT_EXTEND_SEC = dr("PAYMENT_EXTEND_SEC").ToString
                    .SHELF_LONG = dr("SHELF_LONG").ToString
                    .SLEEP_TIME = dr("SLEEP_TIME").ToString
                    .SLEEP_DURATION = dr("SLEEP_DURATION").ToString
                    .VDM_WEBSERVICE_URL = dr("VDM_WEBSERVICE_URL").ToString
                    .ALARM_WEBSERVICE_URL = dr("ALARM_WEBSERVICE_URL").ToString
                    .MOTOR_PARK_POSITION = dr("MOTOR_PARK_POSITION").ToString
                    .MOTOR_ORIGIN_POSITION = dr("MOTOR_ORIGIN_POSITION").ToString
                    .MOTOR_DELIVERY_POSITION = dr("MOTOR_DELIVERY_POSITION").ToString
                    .ADS_VDO_PATH = dr("ADS_VDO_PATH").ToString
                    .INTERVAL_SYNC_TO_VENDING_MIN = dr("INTERVAL_SYNC_TO_VENDING_MIN").ToString
                    .INTERVAL_SYNC_TO_SERVER_MIN = dr("INTERVAL_SYNC_TO_SERVER_MIN").ToString
                    '.INTERVAL_SYNC_TRANSACTION_MIN = dr("INTERVAL_SYNC_TRANSACTION_MIN").ToString
                    '.INTERVAL_SYNC_MASTER_MIN = dr("INTERVAL_SYNC_MASTER_MIN").ToString
                    '.INTERVAL_SYNC_LOG_MIN = dr("INTERVAL_SYNC_LOG_MIN").ToString
                End With

                '##Call WebService For Update Cf_Vending_Sysconfig On Server
                Dim ws As New VDMWebService.ATBVendingWebservice
                ws.Url = wsUrl
                ws.Timeout = 10000
                ret = ws.SyncVDMToServer_CfVendingSysConfig(vscf, vending_id)
                If ret = True Then

                    '##Update Cf_Vending_Sysconfig.Sync_To_Server ='Y'
                    Dim vcf As New CfVendingSysconfigVendingLinqDB
                    vcf.ChkDataByMS_VENDING_ID(vending_id, vTrans.Trans)
                    vcf.SYNC_TO_SERVER = "Y"
                    If vcf.ID > 0 Then
                        Dim re As ExecuteDataInfo = vcf.UpdateData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
                        If re.IsSuccess = True Then
                            vTrans.CommitTransaction()
                        Else
                            vTrans.RollbackTransaction()
                            Engine.LogFileENG.CreateErrorLogAgent(vending_id, re.ErrorMessage)
                        End If
                    End If
                Else
                    vTrans.RollbackTransaction()
                    Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice SyncVDMToServer_CfVendingSysConfig")
                End If
            Else
                vTrans.RollbackTransaction()
            End If
        Catch ex As Exception
            vTrans.RollbackTransaction()
            Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
        End Try

    End Sub
#End Region

    Public Sub SyncVDMToServer_MsProductShelf(vending_id As Long, wsUrl As String)
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        If vending_id = 0 Then
            Exit Sub
        End If

        Try

            '## Get Data From Local Vending Where Sync_To_Server ='N'
            Dim sql As String = "select * from ms_product_shelf where sync_to_server='N'  and  ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_VENDING_ID", vending_id)
            Dim dt As New DataTable
            dt = VendingDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then
                Dim ws As New VDMWebService.ATBVendingWebservice
                ws.Url = wsUrl
                ws.Timeout = 10000

                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim vscf As New VDMWebService.MsProductShelfVendingLinqDB
                    With vscf
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID")
                        .MS_PRODUCT_ID = dt.Rows(i)("MS_PRODUCT_ID")
                        .SHELF_ID = dt.Rows(i)("SHELF_ID")
                        .PRODUCT_QTY = dt.Rows(i)("PRODUCT_QTY")
                    End With

                    '##Call WebService For Update Data On Server
                    Dim ret As Boolean
                    ret = ws.SyncVDMToServer_MsProductShelf(vscf, vending_id)

                    Dim vTrans As New VendingTransactionDB
                    Try
                        If ret = True Then
                            '##Update Sync_To_Server ='Y'
                            Dim vcf As New MsProductShelfVendingLinqDB
                            vcf.ChkDataByMS_VENDING_ID_SHELF_ID(vending_id, dt.Rows(i)("SHELF_ID"), vTrans.Trans)
                            vcf.SYNC_TO_SERVER = "Y"
                            If vcf.ID > 0 Then
                                Dim re As ExecuteDataInfo = vcf.UpdateData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
                                If re.IsSuccess = True Then
                                    vTrans.CommitTransaction()
                                Else
                                    vTrans.RollbackTransaction()
                                    Engine.LogFileENG.CreateErrorLogAgent(vending_id, re.ErrorMessage)
                                End If
                            End If
                        Else
                            vTrans.RollbackTransaction()
                            Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice SyncVDMToServer_MsProductShelf")
                        End If
                    Catch ex As Exception
                        vTrans.RollbackTransaction()
                        Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
                    End Try

                Next
                ws.Dispose()
            End If
        Catch ex As Exception
            Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
        End Try
    End Sub

    Public Sub SyncVDMToServer_MsVendingShelf(vending_id As Long, wsUrl As String)
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        If vending_id = 0 Then
            Exit Sub
        End If

        Try

            '## Get Data From Local Vending Where Sync_To_Server ='N'
            Dim sql As String = "select * from ms_vending_shelf where sync_to_server='N'  and  ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_VENDING_ID", vending_id)
            Dim dt As New DataTable
            dt = VendingDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then

                Dim ws As New VDMWebService.ATBVendingWebservice
                ws.Url = wsUrl
                ws.Timeout = 10000
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim vscf As New VDMWebService.MsVendingShelfVendingLinqDB
                    With vscf
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .SHELF_ID = dt.Rows(i)("SHELF_ID").ToString
                        .POSITION_ROW = dt.Rows(i)("POSITION_ROW").ToString
                        .POSITION_COLUMN = dt.Rows(i)("POSITION_COLUMN").ToString
                        .POSITION_X = dt.Rows(i)("POSITION_X").ToString
                        .POSITION_Y = dt.Rows(i)("POSITION_Y").ToString
                        '.REF_NUMBER = dt.Rows(i)("REF_NUMBER").ToString
                        .WIDTH = dt.Rows(i)("WIDTH").ToString
                    End With

                    '##Call WebService For Update Data On Server
                    Dim ret As Boolean
                    ret = ws.SyncVDMToServer_MsVendingShelf(vscf, vending_id)

                    Dim vTrans As New VendingTransactionDB
                    Try
                        If ret = True Then
                            '##Update Sync_To_Server ='Y'
                            Dim vcf As New MsVendingShelfVendingLinqDB
                            vcf.ChkDataByMS_VENDING_ID_SHELF_ID(vending_id, dt.Rows(i)("SHELF_ID"), vTrans.Trans)
                            vcf.SYNC_TO_SERVER = "Y"
                            If vcf.ID > 0 Then
                                Dim re As ExecuteDataInfo = vcf.UpdateData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
                                If re.IsSuccess = True Then
                                    vTrans.CommitTransaction()
                                Else
                                    vTrans.RollbackTransaction()
                                    Engine.LogFileENG.CreateErrorLogAgent(vending_id, re.ErrorMessage)
                                End If
                            End If
                        Else
                            vTrans.RollbackTransaction()
                            Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice SyncVDMToServer_MsVendingShelf")
                        End If
                    Catch ex As Exception
                        vTrans.RollbackTransaction()
                        Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
                    End Try

                Next
                ws.Dispose()
            End If
        Catch ex As Exception
            Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
        End Try
    End Sub

    Public Sub SyncVDMToServer_LogVendingActivity(vending_id As Long, wsUrl As String)
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        If vending_id = 0 Then
            Exit Sub
        End If

        Try

            '## Get Data From Local Vending Where Sync_To_Server ='N'
            Dim sql As String = "select * from TB_LOG_VENDING_ACTIVITY where ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_VENDING_ID", vending_id)
            Dim dt As New DataTable
            dt = VendingDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then

                Dim ws As New VDMWebService.ATBVendingWebservice
                ws.Url = wsUrl
                ws.Timeout = 10000
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim vscf As New VDMWebService.TbLogVendingActivityVendingLinqDB
                    With vscf
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .TRANS_DATE = dt.Rows(i)("TRANS_DATE").ToString
                        .SALES_TRANS_NO = dt.Rows(i)("SALES_TRANS_NO").ToString
                        .STAFF_CONSOLE_TRANS_NO = dt.Rows(i)("STAFF_CONSOLE_TRANS_NO").ToString
                        .MS_APP_SCREEN_ID = dt.Rows(i)("MS_APP_SCREEN_ID").ToString
                        .LOG_DESC = dt.Rows(i)("LOG_DESC").ToString
                        .IS_PROBLEM = dt.Rows(i)("IS_PROBLEM").ToString
                    End With



                    Try
                        '##Call WebService For Update Data On Server
                        Dim ret As Boolean
                        ret = ws.SyncVDMToServer_LogVendingActivity(vscf, vending_id)


                        If ret = True Then

                            Dim vTrans As New VendingTransactionDB
                            '##Delete Data
                            Dim lnq As New TbLogVendingActivityVendingLinqDB
                            Dim re As ExecuteDataInfo = lnq.DeleteByPK(dt.Rows(i)("id"), vTrans.Trans)
                            If re.IsSuccess = True Then
                                vTrans.CommitTransaction()
                            Else
                                vTrans.RollbackTransaction()
                                Engine.LogFileENG.CreateErrorLogAgent(vending_id, re.ErrorMessage)
                            End If
                            lnq = Nothing

                        Else
                            Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice SyncVDMToServer_LogVendingActivity")
                        End If
                    Catch ex As Exception

                        Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
                    End Try

                Next
                ws.Dispose()
            End If
        Catch ex As Exception
            Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
        End Try
    End Sub

    Public Sub SyncVDMToServer_LogVendingAgent(vending_id As Long, wsUrl As String)
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        If vending_id = 0 Then
            Exit Sub
        End If

        Try

            '## Get Data From Local Vending Where Sync_To_Server ='N'
            Dim sql As String = "select * from TB_LOG_VENDING_AGENT where ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_VENDING_ID", vending_id)
            Dim dt As New DataTable
            dt = VendingDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then

                Dim ws As New VDMWebService.ATBVendingWebservice
                ws.Url = wsUrl
                ws.Timeout = 10000
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim vscf As New VDMWebService.TbLogVendingAgentVendingLinqDB
                    With vscf
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .LOG_TYPE = dt.Rows(i)("LOG_TYPE").ToString
                        .LOG_MESSAGE = dt.Rows(i)("LOG_MESSAGE").ToString
                        .CLASS_NAME = dt.Rows(i)("CLASS_NAME").ToString
                        .FUNCTION_NAME = dt.Rows(i)("FUNCTION_NAME").ToString
                        .LINE_NO = dt.Rows(i)("LINE_NO").ToString
                    End With

                    '##Call WebService For Update Data On Server
                    Dim ret As Boolean
                    ret = ws.SyncVDMToServer_LogVendingAgent(vscf, vending_id)

                    Try
                        If ret = True Then

                            Dim vTrans As New VendingTransactionDB
                            '##Delete Data
                            Dim lnq As New TbLogVendingAgentVendingLinqDB
                            Dim re As ExecuteDataInfo = lnq.DeleteByPK(dt.Rows(i)("id"), vTrans.Trans)
                            If re.IsSuccess = True Then
                                vTrans.CommitTransaction()
                            Else
                                vTrans.RollbackTransaction()
                                Engine.LogFileENG.CreateErrorLogAgent(vending_id, re.ErrorMessage)
                            End If
                            lnq = Nothing

                        Else
                            Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice SyncVDMToServer_LogVendingAgent")
                        End If
                    Catch ex As Exception

                        Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
                    End Try

                Next
                ws.Dispose()
            End If
        Catch ex As Exception
            Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
        End Try
    End Sub

    Public Sub SyncVDMToServer_LogVendingFillMoney(vending_id As Long, wsUrl As String)
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        If vending_id = 0 Then
            Exit Sub
        End If

        Try

            '## Get Data From Local Vending Where Sync_To_Server ='N'
            Dim sql As String = "select * from TB_LOG_VENDING_FILL_MONEY where ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_VENDING_ID", vending_id)
            Dim dt As New DataTable
            dt = VendingDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then

                Dim ws As New VDMWebService.ATBVendingWebservice
                ws.Url = wsUrl
                ws.Timeout = 10000
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim vscf As New VDMWebService.TbLogVendingFillMoneyVendingLinqDB
                    With vscf
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .COIN_IN_MONEY = dt.Rows(i)("COIN_IN_MONEY").ToString
                        .BANKNOTE_IN_MONEY = dt.Rows(i)("BANKNOTE_IN_MONEY").ToString
                        .CHECKOUT_RECEIVE_MONEY = dt.Rows(i)("CHECKOUT_RECEIVE_MONEY").ToString
                        If dt.Rows(i)("CHECKOUT_DATETIME").ToString <> "" Then
                            .CHECKOUT_DATETIME = dt.Rows(i)("CHECKOUT_DATETIME")
                        End If

                        .CHANGE1_REMAIN = dt.Rows(i)("CHANGE1_REMAIN").ToString
                        .CHANGE2_REMAIN = dt.Rows(i)("CHANGE2_REMAIN").ToString
                        .CHANGE5_REMAIN = dt.Rows(i)("CHANGE5_REMAIN").ToString
                        .CHANGE10_REMAIN = dt.Rows(i)("CHANGE10_REMAIN").ToString
                        .CHANGE20_REMAIN = dt.Rows(i)("CHANGE20_REMAIN").ToString
                        .CHANGE50_REMAIN = dt.Rows(i)("CHANGE50_REMAIN").ToString
                        .CHANGE100_REMAIN = dt.Rows(i)("CHANGE100_REMAIN").ToString
                        .CHANGE500_REMAIN = dt.Rows(i)("CHANGE500_REMAIN").ToString
                        .CHECKIN_CHANGE_MONEY = dt.Rows(i)("CHECKIN_CHANGE_MONEY").ToString
                        If dt.Rows(i)("CHECKIN_DATETIME").ToString <> "" Then
                            .CHECKIN_DATETIME = dt.Rows(i)("CHECKIN_DATETIME")
                        End If

                        .TOTAL_MONEY_REMAIN = dt.Rows(i)("TOTAL_MONEY_REMAIN").ToString
                        .IS_CONFIRM = dt.Rows(i)("IS_CONFIRM").ToString
                        If dt.Rows(i)("CONFIRM_CANCEL_DATETIME").ToString <> "" Then
                            .CONFIRM_CANCEL_DATETIME = dt.Rows(i)("CONFIRM_CANCEL_DATETIME")
                        End If

                        .CHANGE1_QTY = dt.Rows(i)("CHANGE1_QTY").ToString
                        .CHANGE2_QTY = dt.Rows(i)("CHANGE2_QTY").ToString
                        .CHANGE5_QTY = dt.Rows(i)("CHANGE5_QTY").ToString
                        .CHANGE10_QTY = dt.Rows(i)("CHANGE10_QTY").ToString
                        .CHANGE20_QTY = dt.Rows(i)("CHANGE20_QTY").ToString
                        .CHANGE50_QTY = dt.Rows(i)("CHANGE50_QTY").ToString
                        .CHANGE100_QTY = dt.Rows(i)("CHANGE100_QTY").ToString
                        .CHANGE500_QTY = dt.Rows(i)("CHANGE500_QTY").ToString
                    End With

                    '##Call WebService For Update Data On Server
                    Dim ret As Boolean
                    ret = ws.SyncVDMToServer_LogVendingFillMoney(vscf, vending_id)

                    Try
                        If ret = True Then

                            Dim vTrans As New VendingTransactionDB
                            '##Delete Data
                            Dim lnq As New TbLogVendingFillMoneyVendingLinqDB
                            Dim re As ExecuteDataInfo = lnq.DeleteByPK(dt.Rows(i)("id"), vTrans.Trans)
                            If re.IsSuccess = True Then
                                vTrans.CommitTransaction()
                            Else
                                vTrans.RollbackTransaction()
                                Engine.LogFileENG.CreateErrorLogAgent(vending_id, re.ErrorMessage)
                            End If
                            lnq = Nothing

                        Else
                            Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice SyncVDMToServer_LogVendingFillMoney")
                        End If
                    Catch ex As Exception

                        Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
                    End Try

                Next
                ws.Dispose()
            End If
        Catch ex As Exception
            Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
        End Try
    End Sub

    Public Sub SyncVDMToServer_TbProductMovement(vending_id As Long, wsUrl As String)
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        If vending_id = 0 Then
            Exit Sub
        End If

        Try

            '## Get Data From Local Vending Where Sync_To_Server ='N'
            Dim sql As String = "select * from TB_PRODUCT_MOVEMENT where ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_VENDING_ID", vending_id)
            Dim dt As New DataTable
            dt = VendingDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then

                Dim ws As New VDMWebService.ATBVendingWebservice
                ws.Url = wsUrl
                ws.Timeout = 10000
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim vscf As New VDMWebService.TbProductMovementVendingLinqDB
                    With vscf
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .MOVEMENT_DATE = dt.Rows(i)("MOVEMENT_DATE").ToString
                        .MS_PRODUCT_ID = dt.Rows(i)("MS_PRODUCT_ID").ToString
                        .SHELF_ID = dt.Rows(i)("SHELF_ID").ToString
                        .MOVEMENT_TYPE = dt.Rows(i)("MOVEMENT_TYPE").ToString
                        .PRODUCT_QTY = dt.Rows(i)("PRODUCT_QTY").ToString
                    End With

                    '##Call WebService For Update Data On Server
                    Dim ret As Boolean
                    ret = ws.SyncVDMToServer_TbProductMovement(vscf, vending_id)

                    Try
                        If ret = True Then

                            Dim vTrans As New VendingTransactionDB
                            '##Delete Data
                            Dim lnq As New TbProductMovementVendingLinqDB
                            Dim re As ExecuteDataInfo = lnq.DeleteByPK(dt.Rows(i)("id"), vTrans.Trans)
                            If re.IsSuccess = True Then
                                vTrans.CommitTransaction()
                            Else
                                vTrans.RollbackTransaction()
                                Engine.LogFileENG.CreateErrorLogAgent(vending_id, re.ErrorMessage)
                            End If
                            lnq = Nothing

                        Else
                            Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice SyncVDMToServer_TbProductMovement")
                        End If
                    Catch ex As Exception

                        Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
                    End Try

                Next
                ws.Dispose()
            End If
        Catch ex As Exception
            Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
        End Try
    End Sub

    Public Sub SyncVDMToServer_TbStaffConsoleTransaction(vending_id As Long, wsUrl As String)
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        If vending_id = 0 Then
            Exit Sub
        End If

        Try

            '## Get Data From Local Vending Where Sync_To_Server ='N'
            Dim sql As String = "select * from TB_STAFF_CONSOLE_TRANSACTION where trans_end_time is not null and ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_VENDING_ID", vending_id)
            Dim dt As New DataTable
            dt = VendingDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then

                Dim ws As New VDMWebService.ATBVendingWebservice
                ws.Url = wsUrl
                ws.Timeout = 10000
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim vscf As New VDMWebService.TbStaffConsoleTransactionVendingLinqDB
                    With vscf
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .TRANS_NO = dt.Rows(i)("TRANS_NO").ToString
                        .TRANS_START_TIME = dt.Rows(i)("TRANS_START_TIME").ToString
                        .TRANS_END_TIME = dt.Rows(i)("TRANS_END_TIME").ToString
                        .LOGIN_USERNAME = dt.Rows(i)("LOGIN_USERNAME").ToString
                        .LOGIN_FIRST_NAME = dt.Rows(i)("LOGIN_FIRST_NAME").ToString
                        .LOGIN_LAST_NAME = dt.Rows(i)("LOGIN_LAST_NAME").ToString
                        .LOGIN_COMPANY_NAME = dt.Rows(i)("LOGIN_COMPANY_NAME").ToString
                        .LOGIN_BY = dt.Rows(i)("LOGIN_BY").ToString
                        If dt.Rows(i)("MS_APP_STEP_ID").ToString <> "" Then
                            .MS_APP_STEP_ID = dt.Rows(i)("MS_APP_STEP_ID").ToString
                        End If
                    End With

                    '##Call WebService For Update Data On Server
                    Dim ret As Boolean
                    ret = ws.SyncVDMToServer_TbStaffConsoleTransaction(vscf, vending_id)

                    Try
                        If ret = True Then

                            Dim vTrans As New VendingTransactionDB
                            '##Delete Data
                            Dim lnq As New TbStaffConsoleTransactionVendingLinqDB
                            Dim re As ExecuteDataInfo = lnq.DeleteByPK(dt.Rows(i)("id"), vTrans.Trans)
                            If re.IsSuccess = True Then
                                vTrans.CommitTransaction()
                            Else
                                vTrans.RollbackTransaction()
                                Engine.LogFileENG.CreateErrorLogAgent(vending_id, re.ErrorMessage)
                            End If
                            lnq = Nothing

                        Else
                            Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice SyncVDMToServer_TbStaffConsoleTransaction")
                        End If
                    Catch ex As Exception

                        Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
                    End Try

                Next
                ws.Dispose()
            End If
        Catch ex As Exception
            Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
        End Try
    End Sub

    Public Sub SyncVDMToServer_TbVendingSaleTransaction(vending_id As Long, wsUrl As String)
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        If vending_id = 0 Then
            Exit Sub
        End If

        Try

            '## Get Data From Local Vending Where Sync_To_Server ='N'
            Dim sql As String = "select * from TB_VENDING_SALE_TRANSACTION where trans_status <> '0' and ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_VENDING_ID", vending_id)
            Dim dt As New DataTable
            dt = VendingDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then
                Dim ws As New VDMWebService.ATBVendingWebservice
                ws.Url = wsUrl
                ws.Timeout = 10000

                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim vscf As New VDMWebService.TbVendingSaleTransactionVendingLinqDB
                    With vscf
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .TRANS_NO = dt.Rows(i)("TRANS_NO").ToString
                        If dt.Rows(i)("TRANS_START_TIME").ToString <> "" Then
                            .TRANS_START_TIME = dt.Rows(i)("TRANS_START_TIME").ToString
                        End If

                        If dt.Rows(i)("TRANS_END_TIME").ToString <> "" Then
                            .TRANS_END_TIME = dt.Rows(i)("TRANS_END_TIME").ToString
                        End If

                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .MS_PRODUCT_ID = dt.Rows(i)("MS_PRODUCT_ID").ToString
                        .SHELF_ID = dt.Rows(i)("SHELF_ID").ToString
                        .PRODUCT_COST = dt.Rows(i)("PRODUCT_COST").ToString
                        .PRODUCT_PRICE = dt.Rows(i)("PRODUCT_PRICE").ToString
                        .PRODUCT_DISCOUNT_PERCENT = dt.Rows(i)("PRODUCT_DISCOUNT_PERCENT").ToString
                        .PRODUCT_NETPRICE = dt.Rows(i)("PRODUCT_NETPRICE").ToString
                        .PRODUCT_PROFIT_SHARING = dt.Rows(i)("PRODUCT_PROFIT_SHARING").ToString
                        If dt.Rows(i)("PAID_SUCCESS_TIME").ToString <> "" Then
                            .PAID_SUCCESS_TIME = dt.Rows(i)("PAID_SUCCESS_TIME").ToString
                        End If

                        .RECEIVE_COIN1 = dt.Rows(i)("RECEIVE_COIN1").ToString
                        .RECEIVE_COIN2 = dt.Rows(i)("RECEIVE_COIN2").ToString
                        .RECEIVE_COIN5 = dt.Rows(i)("RECEIVE_COIN5").ToString
                        .RECEIVE_COIN10 = dt.Rows(i)("RECEIVE_COIN10").ToString
                        .RECEIVE_BANKNOTE20 = dt.Rows(i)("RECEIVE_BANKNOTE20").ToString
                        .RECEIVE_BANKNOTE50 = dt.Rows(i)("RECEIVE_BANKNOTE50").ToString
                        .RECEIVE_BANKNOTE100 = dt.Rows(i)("RECEIVE_BANKNOTE100").ToString
                        .RECEIVE_BANKNOTE500 = dt.Rows(i)("RECEIVE_BANKNOTE500").ToString
                        .RECEIVE_BANKNOTE1000 = dt.Rows(i)("RECEIVE_BANKNOTE1000").ToString
                        .CHANGE_COIN1 = dt.Rows(i)("CHANGE_COIN1").ToString
                        .CHANGE_COIN2 = dt.Rows(i)("CHANGE_COIN2").ToString
                        .CHANGE_COIN5 = dt.Rows(i)("CHANGE_COIN5").ToString
                        .CHANGE_COIN10 = dt.Rows(i)("CHANGE_COIN10").ToString
                        .CHANGE_BANKNOTE20 = dt.Rows(i)("CHANGE_BANKNOTE20").ToString
                        .CHANGE_BANKNOTE50 = dt.Rows(i)("CHANGE_BANKNOTE50").ToString
                        .CHANGE_BANKNOTE100 = dt.Rows(i)("CHANGE_BANKNOTE100").ToString
                        .CHANGE_BANKNOTE500 = dt.Rows(i)("CHANGE_BANKNOTE500").ToString
                        .TRANS_STATUS = dt.Rows(i)("TRANS_STATUS").ToString
                        .MS_APP_STEP_ID = dt.Rows(i)("MS_APP_STEP_ID").ToString
                    End With

                    '##Call WebService For Update Data On Server                    
                    Dim ret As Boolean
                    ret = ws.SyncVDMToServer_TbVendingSaleTransaction(vscf, vending_id)

                    Try
                        If ret = True Then

                            Dim vTrans As New VendingTransactionDB
                            '##Delete Data
                            Dim lnq As New TbVendingSaleTransactionVendingLinqDB
                            Dim re As ExecuteDataInfo = lnq.DeleteByPK(dt.Rows(i)("id"), vTrans.Trans)
                            If re.IsSuccess = True Then
                                vTrans.CommitTransaction()
                            Else
                                vTrans.RollbackTransaction()
                                Engine.LogFileENG.CreateErrorLogAgent(vending_id, re.ErrorMessage)
                            End If
                            lnq = Nothing

                        Else
                            Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice SyncVDMToServer_TbVendingSaleTransaction")
                        End If
                    Catch ex As Exception

                        Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
                    End Try

                Next
                ws.Dispose()
            End If
        Catch ex As Exception
            Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
        End Try
    End Sub

#Region "MsVendingDevice"

    Public Sub SyncServerToVDM_MsVendingDevice(vending_id As Long, wsUrl As String)
        ''### Current Class and Function name
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        Dim ws As New VDMWebService.ATBVendingWebservice
        ws.Url = wsUrl
        ws.Timeout = 10000

        '## Get Data From Server where sync_to_vending='N'
        Dim dt As New DataTable
        dt = ws.GetServerMsVendingDevice(vending_id)

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If


        '## Update To Local Vending
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim vTrans As New VendingTransactionDB
            Try

                Dim ret As New ExecuteDataInfo
                Dim lnq As New MsVendingDeviceVendingLinqDB
                lnq.ChkDataByMS_DEVICE_ID_MS_VENDING_ID(dt.Rows(i)("MS_DEVICE_ID"), vending_id, vTrans.Trans)
                If lnq.ID > 0 Then
                    With lnq
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .MS_DEVICE_ID = dt.Rows(i)("MS_DEVICE_ID").ToString

                        If dt.Rows(i)("MAX_QTY").ToString <> "" Then
                            .MAX_QTY = dt.Rows(i)("MAX_QTY").ToString
                        End If
                        If dt.Rows(i)("WARNING_QTY").ToString <> "" Then
                            .WARNING_QTY = dt.Rows(i)("WARNING_QTY").ToString
                        End If
                        If dt.Rows(i)("CRITICAL_QTY").ToString <> "" Then
                            .CRITICAL_QTY = dt.Rows(i)("CRITICAL_QTY").ToString
                        End If
                        If dt.Rows(i)("CURRENT_QTY").ToString <> "" Then
                            .CURRENT_QTY = dt.Rows(i)("CURRENT_QTY").ToString
                        End If
                        If dt.Rows(i)("CURRENT_MONEY").ToString <> "" Then
                            .CURRENT_MONEY = dt.Rows(i)("CURRENT_MONEY").ToString
                        End If
                        If dt.Rows(i)("MS_DEVICE_STATUS_ID").ToString <> "" Then
                            .MS_DEVICE_STATUS_ID = dt.Rows(i)("MS_DEVICE_STATUS_ID").ToString
                        End If

                        .COMPORT_VID = dt.Rows(i)("COMPORT_VID").ToString
                        .DRIVER_NAME1 = dt.Rows(i)("DRIVER_NAME1").ToString
                        .DRIVER_NAME2 = dt.Rows(i)("DRIVER_NAME2").ToString
                        .SYNC_TO_VENDING = "Y"
                        ret = .UpdateData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
                    End With
                End If

                If ret.IsSuccess = True Then
                    '## Update Sync_To_Vending = 'Y'  On Server
                    Dim wsRet As Boolean = False
                    wsRet = ws.UpdateSyncToVDM_MsVendingDevice(vending_id, dt.Rows(i)("MS_DEVICE_ID"))
                    If wsRet = False Then
                        Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice UpdateSyncToVDM_MsVendingDevice")
                    End If

                    vTrans.CommitTransaction()
                Else
                    vTrans.RollbackTransaction()
                    Engine.LogFileENG.CreateErrorLogAgent(vending_id, ret.ErrorMessage)
                End If
            Catch ex As Exception
                vTrans.RollbackTransaction()
                Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
            End Try
        Next
        ws.Dispose()

    End Sub

    Public Sub SyncVDMToServer_MsVendingDevice(vending_id As Long, wsUrl As String)
        Dim m As MethodBase = MethodBase.GetCurrentMethod()
        Dim ThisClassName As String = m.ReflectedType.Name
        Dim ThisFunctionName As String = m.Name

        If vending_id = 0 Then
            Exit Sub
        End If

        Try

            '## Get Data From Local Vending Where Sync_To_Server ='N'
            Dim sql As String = "select * from MS_VENDING_DEVICE  where sync_to_server='N' and ms_vending_id=@_VENDING_ID"
            Dim p(1) As SqlParameter
            p(0) = VendingDB.SetBigInt("@_VENDING_ID", vending_id)
            Dim dt As New DataTable
            dt = VendingDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then
                Dim ws As New VDMWebService.ATBVendingWebservice
                ws.Url = wsUrl
                ws.Timeout = 10000

                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim vscf As New VDMWebService.MsVendingDeviceVendingLinqDB
                    With vscf
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .MS_VENDING_ID = dt.Rows(i)("MS_VENDING_ID").ToString
                        .MS_DEVICE_ID = dt.Rows(i)("MS_DEVICE_ID").ToString

                        If dt.Rows(i)("MAX_QTY").ToString <> "" Then
                            .MAX_QTY = dt.Rows(i)("MAX_QTY").ToString
                        End If
                        If dt.Rows(i)("WARNING_QTY").ToString <> "" Then
                            .WARNING_QTY = dt.Rows(i)("WARNING_QTY").ToString
                        End If
                        If dt.Rows(i)("CRITICAL_QTY").ToString <> "" Then
                            .CRITICAL_QTY = dt.Rows(i)("CRITICAL_QTY").ToString
                        End If
                        If dt.Rows(i)("CURRENT_QTY").ToString <> "" Then
                            .CURRENT_QTY = dt.Rows(i)("CURRENT_QTY").ToString
                        End If
                        If dt.Rows(i)("CURRENT_MONEY").ToString <> "" Then
                            .CURRENT_MONEY = dt.Rows(i)("CURRENT_MONEY").ToString
                        End If
                        If dt.Rows(i)("MS_DEVICE_STATUS_ID").ToString <> "" Then
                            .MS_DEVICE_STATUS_ID = dt.Rows(i)("MS_DEVICE_STATUS_ID").ToString
                        End If

                        .COMPORT_VID = dt.Rows(i)("COMPORT_VID").ToString
                        .DRIVER_NAME1 = dt.Rows(i)("DRIVER_NAME1").ToString
                        .DRIVER_NAME2 = dt.Rows(i)("DRIVER_NAME2").ToString
                    End With

                    '##Call WebService For Update Data On Server
                    Dim ret As Boolean
                    ret = ws.SyncVDMToServer_MsVendingDevice(vscf, vending_id)

                    Try
                        If ret = True Then

                            Dim vTrans As New VendingTransactionDB
                            '##Update Cf_Vending_Sysconfig.Sync_To_Server ='Y'
                            Dim lnq As New MsVendingDeviceVendingLinqDB
                            lnq.ChkDataByMS_DEVICE_ID_MS_VENDING_ID(dt.Rows(i)("MS_DEVICE_ID"), vending_id, vTrans.Trans)
                            lnq.SYNC_TO_SERVER = "Y"
                            If lnq.ID > 0 Then
                                Dim re As ExecuteDataInfo = lnq.UpdateData(ThisClassName & "." & ThisFunctionName, vTrans.Trans)
                                If re.IsSuccess = True Then
                                    vTrans.CommitTransaction()
                                Else
                                    vTrans.RollbackTransaction()
                                    Engine.LogFileENG.CreateErrorLogAgent(vending_id, re.ErrorMessage)
                                End If
                            End If

                        Else
                            Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice SyncVDMToServer_MsVendingDevice")
                        End If
                    Catch ex As Exception

                        Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
                    End Try

                Next
                ws.Dispose()
            End If
        Catch ex As Exception
            Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
        End Try
    End Sub
#End Region

#End Region


    Public Sub SyncServerToVDM_VDO_ADVERTISE(vending_id As Long, wsUrl As String)
        Dim ws As New VDMWebService.ATBVendingWebservice
        ws.Url = wsUrl
        ws.Timeout = 10000

        Dim cf As VDMWebService.CfServerSysconfigServerLinqDB = ws.GetServerConfig
        Dim vcf As New CfVendingSysconfigVendingLinqDB
        Dim vTrans As New VendingTransactionDB
        vcf.ChkDataByMS_VENDING_ID(vending_id, vTrans.Trans)


        Dim dt As New DataTable
        dt = ws.GetVDOInfoByVending(vending_id)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim ads_id As String = dt.Rows(i)("id").ToString
            Dim vdo_path_server As String = cf.ADS_VDO_URL & ads_id & ".mp4"
            Dim vdo_path_vending As String = vcf.ADS_VDO_PATH & ads_id & ".mp4"

            If File.Exists(vdo_path_vending) = True Then
                Try
                    File.Delete(vdo_path_vending)
                Catch ex As Exception
                    Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
                End Try
            End If

            Try
                Dim _Downloader As New WebFileDownloader
                Dim ret As Boolean = _Downloader.DownloadFileWithProgress(vdo_path_server, vdo_path_vending)
                If ret Then
                    Dim ws_ret As Boolean
                    ws_ret = ws.UpdateSyncToVDM_MsAdvertise(vending_id, ads_id)
                    If ws_ret = False Then
                        Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from webservice UpdateSyncToVDM_MsAdvertise")
                    End If
                Else
                    Engine.LogFileENG.CreateErrorLogAgent(vending_id, "Error from _Downloader.DownloadFileWithProgress")
                End If
            Catch ex As Exception
                Engine.LogFileENG.CreateExceptionLogAgent(vending_id, ex.Message, ex.StackTrace)
            End Try

        Next
        ws.Dispose()
    End Sub


    Public Sub CreateHartbeat(TimerName As String)
        Dim frame As StackFrame = New StackFrame(1, True)
        Dim ClassName As String = frame.GetMethod.ReflectedType.Name
        Dim FunctionName As String = frame.GetMethod.Name
        Dim LineNo As Integer = frame.GetFileLineNumber

        Try
            Dim hbPath As String = Application.StartupPath & "\HeartBeat\" & ClassName & "\"
            If Directory.Exists(hbPath) = False Then
                Directory.CreateDirectory(hbPath)
            End If

            Dim FileName As String = hbPath & FunctionName & "_Timer_" & TimerName & ".txt"
            Dim obj As New StreamWriter(FileName, False)
            obj.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"))
            obj.Flush()
            obj.Close()
        Catch ex As Exception
            ''### Current Class and Function name
            'Dim m As MethodBase = MethodBase.GetCurrentMethod()
            'Dim ThisClassName As String = m.ReflectedType.Name
            'Dim ThisFunctionName As String = m.Name

            CreateTextErrorLog("Exception : " & ex.Message & " " & ex.StackTrace & "&ClassName=" & ClassName & "&FunctionName=" & FunctionName & "&LineNo=" & LineNo & "&TimerName=" & TimerName)
        End Try
    End Sub
    Private Shared Sub CreateTextErrorLog(LogMsg As String)
        Try
            Dim frame As StackFrame = New StackFrame(1, True)
            Dim ClassName As String = frame.GetMethod.ReflectedType.Name
            Dim FunctionName As String = frame.GetMethod.Name
            Dim LineNo As Integer = frame.GetFileLineNumber

            Dim MY As String = DateTime.Now.ToString("yyyyMM")
            Dim DD As String = DateTime.Now.ToString("dd")
            Dim LogFolder As String = Application.StartupPath & "\ErrorLog\" & MY & "\" & DD & "\"
            If Directory.Exists(LogFolder) = False Then
                Directory.CreateDirectory(LogFolder)
            End If

            Dim FileName As String = LogFolder & ClassName & "_" & DateTime.Now.ToShortDateString("yyyyMMddHH") & ".txt"
            Dim obj As New StreamWriter(FileName, True)
            obj.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") & " " & FunctionName + " Line No :" + LineNo + Environment.NewLine + LogMsg + Environment.NewLine + Environment.NewLine)
            obj.Flush()
            obj.Close()
        Catch ex As Exception

        End Try
    End Sub
End Class
