﻿'Imports ServerLinqDB.ConnectDB
'Imports ServerLinqDB.TABLE
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports VendingLinqDB.ConnectDB
Imports VendingLinqDB.TABLE
Imports Engine

Public Class SyncDataVDMServer

    Dim Eng As New SyncDataVDMServerENG
    Dim tmSyncVDMToServer As System.Timers.Timer
    Dim tmSyncServerToVDM As System.Timers.Timer
    Dim vdt As DataTable
    Dim VendingID As Long = 0
    Dim wsUrl As String = ""
    Dim interval_vdmtoserver As Integer = 60000
    Dim interval_servertovdm As Integer = 60000

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        vdt = Eng.GetVendingSysConfig()
        If vdt.Rows.Count > 0 Then
            VendingID = vdt.Rows(0)("ID")
            wsUrl = vdt.Rows(0)("VDM_WEBSERVICE_URL")
            interval_vdmtoserver = vdt.Rows(0)("INTERVAL_SYNC_TO_SERVER_MIN")
            interval_servertovdm = vdt.Rows(0)("INTERVAL_SYNC_TO_VENDING_MIN")
        End If

        tmSyncVDMToServer = New System.Timers.Timer
        tmSyncVDMToServer.Interval = interval_vdmtoserver
        AddHandler tmSyncVDMToServer.Elapsed, AddressOf tmSyncVDMToServer_Tick
        tmSyncVDMToServer.Start()
        tmSyncVDMToServer.Enabled = True

        tmSyncServerToVDM = New System.Timers.Timer
        tmSyncServerToVDM.Interval = interval_servertovdm
        AddHandler tmSyncServerToVDM.Elapsed, AddressOf tmSyncServerToVDM_Tick
        tmSyncServerToVDM.Start()
        tmSyncServerToVDM.Enabled = True
    End Sub


    Private Sub tmSyncVDMToServer_Tick(sender As Object, e As System.Timers.ElapsedEventArgs)
        tmSyncVDMToServer.Enabled = False
        Eng.CreateHartbeat("tmSyncVDMToServer")
        Eng.SyncVDMToServer(VendingID, wsUrl)

        vdt = Eng.GetVendingSysConfig()
        If vdt.Rows.Count > 0 Then
            wsUrl = vdt.Rows(0)("VDM_WEBSERVICE_URL")
            interval_vdmtoserver = vdt.Rows(0)("INTERVAL_SYNC_TO_SERVER_MIN")
            tmSyncVDMToServer.Interval = interval_vdmtoserver
        End If
        tmSyncVDMToServer.Enabled = True
    End Sub

    Private Sub tmSyncServerToVDM_Tick(sender As Object, e As System.Timers.ElapsedEventArgs)
        tmSyncServerToVDM.Enabled = False
        Eng.CreateHartbeat("tmSyncServerToVDM")
        Eng.SyncServerToVDM(VendingID, wsUrl)

        vdt = Eng.GetVendingSysConfig()
        If vdt.Rows.Count > 0 Then
            wsUrl = vdt.Rows(0)("VDM_WEBSERVICE_URL")
            interval_vdmtoserver = vdt.Rows(0)("INTERVAL_SYNC_TO_VENDING_MIN")
            tmSyncServerToVDM.Interval = interval_servertovdm
        End If
        tmSyncServerToVDM.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

End Class
