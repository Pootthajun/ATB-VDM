﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmAlarmMonitoring.aspx.vb" Inherits="frmAlarmMonitoring" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Setting > Vending</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Found : 3 Machine(s)</div>
          <div class="card-block p-a-0"> 
          </div>
        </div>

         <div class="col-md-6">
            <div class="row">
          <div class="col-md-12 col-lg-12">
            <div class="row">
              <div class="col-sm-12">
                <div class="card card-block no-border bg-white row-equal align-middle">
                  <div class="column col-lg-6">
                   <a href="frmDetailAlarmMonitoring.aspx">
                    <img src="images/icon//100/koisk_ok.png" alt="Mountain View" style="width:80px;height:80px;">
                    <h3 class="m-a-0 text-primary">AUTOBOX-PC-1</h3>
                    <div class="overflow-hidden" style="margin-top:1px;">
                <h4 class="m-a-0 text-muted">IP : 192.168.1.113</h4>
                <h6 class="m-a-0 text-muted">ARL Suvanabhumi Station</h6><br /><br />   
              </div></a>
                  </div>
                  <div class="column col-lg-6">
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple"><i class="fa fa-dashboard"></i>Hardware Condition</a>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple"><i class="fa fa-gears"></i>Peripheral Condition</a>
                    <a class="btn btn-block btn-icon btn-pinterest btn-shadow ripple"><i class="fa fa-line-chart left"></i>Stock Level</a>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple"><i class="fa fa-plug"></i>Network Connection</a>
               
                  </div>
                 </div>
                 </div>    
               </div>
             </div>
        </div>
         
          </div>
         <div class="col-md-6">
            <div class="row">
          <div class="col-md-12 col-lg-12">
            <div class="row">
              <div class="col-sm-12">
                <div class="card card-block no-border bg-white row-equal align-middle">
                  <div class="column col-lg-6">
                    <img src="images/icon//100/koisk_ok.png" alt="Mountain View" style="width:80px;height:80px;">
                    <h3 class="m-a-0 text-primary">AUTOBOX-PC-2</h3>
                    <div class="overflow-hidden" style="margin-top:1px;">
                <h4 class="m-a-0 text-muted">IP : 192.168.1.113</h4>
                <h6 class="m-a-0 text-muted">ARL Suvanabhumi Station</h6><br /><br />   
              </div>
                  </div>
                  <div class="column col-lg-6">
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple"><i class="fa fa-dashboard"></i>Hardware Condition</a>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple"><i class="fa fa-gears"></i>Peripheral Condition</a>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple"><i class="fa fa-line-chart left"></i>Stock Level</a>
                    <a class="btn btn-block btn-icon btn-pinterest btn-shadow ripple"><i class="fa fa-plug"></i>Network Connection</a>
               
                  </div>
                 </div>
                 </div>    
               </div>
              </div>
             </div>
          </div>
        <div class="col-md-6">
            <div class="row">
          <div class="col-md-12 col-lg-12">
            <div class="row">
              <div class="col-sm-12">
                <div class="card card-block no-border bg-white row-equal align-middle">
                  <div class="column col-lg-6">
                    <img src="images/icon//100/koisk_ok.png" alt="Mountain View" style="width:80px;height:80px;">
                    <h3 class="m-a-0 text-primary">AUTOBOX-PC-3</h3>
                    <div class="overflow-hidden" style="margin-top:1px;">
                <h4 class="m-a-0 text-muted">IP : 192.168.1.113</h4>
                <h6 class="m-a-0 text-muted">ARL Suvanabhumi Station</h6><br /><br />   
              </div>
                  </div>
                  <div class="column col-lg-6">
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple"><i class="fa fa-dashboard"></i>Hardware Condition</a>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple"><i class="fa fa-gears"></i>Peripheral Condition</a>
                    <a class="btn btn-block btn-icon btn-pinterest btn-shadow ripple"><i class="fa fa-line-chart left"></i>Stock Level</a>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple"><i class="fa fa-plug"></i>Network Connection</a>
               
                  </div>
                 </div>
                 </div>    
               </div>
             </div>
           </div>
          </div> 
       </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
    <script src="scripts/helpers/colors.js"></script>
    <script src="vendor/Chart.js/Chart.min.js"></script>
    <script src="vendor/flot/jquery.flot.js"></script>
    <script src="vendor/flot/jquery.flot.resize.js"></script>
    <script src="vendor/flot/jquery.flot.categories.js"></script>
    <script src="vendor/flot/jquery.flot.stack.js"></script>
    <script src="vendor/flot/jquery.flot.time.js"></script>
    <script src="vendor/flot/jquery.flot.pie.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendor/d3/d3.min.js" charset="utf-8"></script>
    <script src="vendor/c3/c3.min.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>

    <!-- end page scripts -->
    <!-- initialize page scripts -->
    <script src="scripts/helpers/sameheight.js"></script>
    <script src="scripts/ui/dashboard.js"></script>
    <script src="scripts/charts/c3.js"></script>
  <script src="vendor/jquery.easy-pie-chart/dist/jquery.easypiechart.js"></script>
    <!-- end initialize page scripts -->
    
    <script src="scripts/charts/easypie.js"></script>
    <script type="text/javascript" lang="javascript">

        var timerRefresh;
        var refreshInterval = 60000;  //อัพเดททุกๆ 1 นาที
        function setRefreshMonitoring() {
            timerRefresh = setTimeout(updateMonitoring, refreshInterval);
        }
        $(document).ready(function () {
            setRefreshMonitoring();
        });

        function updateMonitoring() {

            var btn = document.getElementById('btnRefreshData');
            if (btn) {
                btn.click();
                timerRefresh = setTimeout(updateMonitoring, refreshInterval);
            }
        }
    </script>
</asp:Content>


