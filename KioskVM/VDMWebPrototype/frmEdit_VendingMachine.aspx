﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmEdit_VendingMachine.aspx.vb" Inherits="frmEdit_VendingMachine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" href="vendor/jquery.tagsinput/src/jquery.tagsinput.css">
  <link rel="stylesheet" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css">
  <link rel="stylesheet" href="vendor/intl-tel-input/build/css/intlTelInput.css">
  <link rel="stylesheet" href="vendor/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
  <link rel="stylesheet" href="vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
  <link rel="stylesheet" href="vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="vendor/jquery-labelauty/source/jquery-labelauty.css">
  <link rel="stylesheet" href="vendor/multiselect/css/multi-select.css">
  <link rel="stylesheet" href="vendor/ui-select/dist/select.css">
  <link rel="stylesheet" href="vendor/select2/dist/css/select2.css">
  <link rel="stylesheet" href="vendor/selectize/dist/css/selectize.css">
  <!-- end page stylesheets -->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Master Data > Vending</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Edit Vending</div>
          <div class="card-block p-a-0">
          <div class="card-block">
            <div class="row m-a-0">
             
              <div class="col-lg-12">
                  
                <form class="form-horizontal" role="form">
                   <h4 class="title">Vending Info</h4>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Location :</b></label>
                    <div class="col-sm-10">
                <div class="m-b">
                  <select data-placeholder="All Location" class="chosen" style="width: 100%;">
                    <option>American Black Bear</option>
                    <option>Asiatic Black Bear</option>
                    <option>Brown Bear</option>
                    <option>Giant Panda</option>
                    <option>Sloth Bear</option>
                    <option>Sun Bear</option>
                    <option>Polar Bear</option>
                    <option>Spectacled Bear</option>
                  </select>
                </div>
                    </div>
                  </div>
                   <h4 class="title">Network Information</h4>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Computer Name :</b></label>
                    <div class="col-sm-10">
                      <input class="form-control m-b" type="text" placeholder="Default input">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Mac Address :</b></label>
                    <div class="col-sm-5">
                      <div class="row">
                        <div class="col-xs-2">
                          <input type="text" class="form-control" placeholder="6C">
                        </div>
                        <div class="col-xs-2">
                          <input type="text" class="form-control" placeholder="71">
                        </div>
                        <div class="col-xs-2">
                          <input type="text" class="form-control" placeholder="D9">
                        </div>
                        <div class="col-xs-2">
                          <input type="text" class="form-control" placeholder="FA">
                        </div>
                        <div class="col-xs-2">
                          <input type="text" class="form-control" placeholder="2E">
                        </div>
                        
                        <div class="col-xs-2">
                          <input type="text" class="form-control" placeholder="07">
                        </div>
                      </div>
                    </div>
                  </div><br /><br /><br /><br /><br />
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>IP Address :</b></label>
                    <div class="col-sm-5">
                      <div class="row">
                        <div class="col-xs-2">
                          <input type="text" class="form-control" placeholder="192">
                        </div>
                        <div class="col-xs-2">
                          <input type="text" class="form-control" placeholder="168">
                        </div>
                        <div class="col-xs-2">
                          <input type="text" class="form-control" placeholder="1">
                        </div>
                        <div class="col-xs-2">
                          <input type="text" class="form-control" placeholder="113">
                        </div>

                      </div>
                    </div>
                  </div>

                </form>
              </div>
              <div class="row mb25">
              <h4 class="card-title">Material Stock Control Level</h4>
               <div class="col-md-4">
                <div class="card bg-white">
                  <div class="card-header bg-info text-white">
                    <img src="images/icon//100/coin_in.png" alt="Mountain View" style="width:20px;height:20px;">
                    <b>Coin In</b>
                  </div>
                  <div class="card-block">
                      <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-green"><b>Stock Capaity :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                       <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-danger"><b>Alert at :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                       <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-warning"><b>Warning at :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                   </div>
                </div>
              </div>
               <div class="col-md-4">
                <div class="card bg-white">
                  <div class="card-header bg-info text-white">
                    <img src="images/icon//100/Banknotein.png" alt="Mountain View" style="width:20px;height:20px;">
                     <b>Banknote In</b>
                  </div>
                  <div class="card-block">
                      <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-green"><b>Stock Capaity :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                       <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-danger"><b>Alert at :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                       <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-warning"><b>Warning at :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                   </div>
                </div>
              </div>
               <div class="col-md-4">
                <div class="card bg-white">
                  <div class="card-header bg-info text-white">
                    <img src="images/icon//100/coin5.png" alt="Mountain View" style="width:20px;height:20px;">
                     <b>Coin Out 5</b>
                  </div>
                  <div class="card-block">
                      <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-green"><b>Stock Capaity :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                       <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-danger"><b>Alert at :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                       <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-warning"><b>Warning at :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                   </div>
                </div>
              </div>
               <div class="col-md-4">
                <div class="card bg-white">
                  <div class="card-header bg-info text-white">
                    <img src="images/icon//100/20_out.png" alt="Mountain View" style="width:20px;height:20px;">
                    <b>Banknote Out 20</b>
                  </div>
                  <div class="card-block">
                      <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-green"><b>Stock Capaity :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                       <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-danger"><b>Alert at :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                       <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-warning"><b>Warning at :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                   </div>
                </div>
              </div>
               <div class="col-md-4">
                <div class="card bg-white">
                  <div class="card-header bg-info text-white">
                    <img src="images/icon//100/Banknote100.png" alt="Mountain View" style="width:20px;height:20px;">
                    <b>Banknote Out 100</b>
                  </div>
                  <div class="card-block">
                      <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-green"><b>Stock Capaity :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                       <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-danger"><b>Alert at :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                       <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-sm-5 control-label text-warning"><b>Warning at :</b></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                     </form>
                   </div>
                </div>
              </div>
                   <div class="col-md-4">
                    <div class="card bg-white">
                      <div class="card-header bg-info text-white">
                        <img src="images/icon//100/printer.png" alt="Mountain View" style="width:20px;height:20px;">
                        <b>Printer</b> 
                      </div>
                      <div class="card-block">
                          <form class="form-horizontal" role="form">
                          <div class="form-group">
                            <label class="col-sm-5 control-label text-green"><b>Stock Capaity :</b></label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control">
                            </div>
                          </div>
                         </form>
                           <form class="form-horizontal" role="form">
                          <div class="form-group">
                            <label class="col-sm-5 control-label text-danger"><b>Alert at :</b></label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control">
                            </div>
                          </div>
                         </form>
                           <form class="form-horizontal" role="form">
                          <div class="form-group">
                            <label class="col-sm-5 control-label text-warning"><b>Warning at :</b></label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control">
                            </div>
                          </div>
                         </form>
                       </div>
                    </div>
                  </div>
               </div><br />
                <div class="row mb25">
                     <button type="button" class="btn btn-info btn-shadow ripple center-content"><i class="fa fa-plus m-r"></i>Add Product</button>
                     <br /><br />
                   <div class="col-md-12">
                     <div class="card bg-white">
                       <div class="card-block">
                           <h4 class="card-title">Select Product</h4>
                            <form class="form-horizontal" role="form"><br /><br />
                               <div class="row gallery chocolat-parent" data-chocolat-title="Reactor Gallery">
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                  <div class="form-group">
                                  <h5><label class="col-sm-6 control-label">XXXXXXXXXXX</label></h5>
                                  <div class="col-sm-6">
                                   <label class="cb-checkbox cb-md">
                                   <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                                 </label>
                                </div>
                              </div>
                                <a href="#" class="chocolat-image "data-toggle="modal" data-target="#myModal" title="App Gallery">
                                <img alt="" data-toggle="tooltip" data-placement="bottom" title="Show" src="images/Product/1-1.jpg" height="170" width="170"/></a>
                                <h6 class="text-danger">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Sale 50%</h6>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                 <div class="form-group">
                                  <h5><label class="col-sm-6 control-label">XXXXXXXXXXX</label></h5>
                                   <div class="col-sm-6">
                                   <label class="cb-checkbox cb-md">
                                    <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" />
                                   </label>
                                 </div>
                               </div>
                                <a href="#" class="chocolat-image" title="App Gallery">
                                   <img alt="" data-toggle="tooltip" data-placement="bottom" title="Show" src="images/Product/1-2.jpg" height="120" width="120">
                                </a>
                                <h6 class="text-danger">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Sale 50%</h6>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                     <div class="form-group">
                                  <h5><label class="col-sm-6 control-label">XXXXXXXXXXX</label></h5>
                                   <div class="col-sm-6">
                                   <label class="cb-checkbox cb-md">
                                    <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                                   </label>
                                 </div>
                               </div>
                                    <a href="#" class="chocolat-image" title="App Gallery">
                                    <img alt="" data-toggle="tooltip" data-placement="bottom" title="Show" src="images/Product/1-3.jpg" height="130" width="130">
                                    </a>
                                    <h6 class="text-danger">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Sale 50%</h6>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                 <div class="form-group">
                                  <h5><label class="col-sm-6 control-label">XXXXXXXXXXX</label></h5>
                                   <div class="col-sm-6">
                                   <label class="cb-checkbox cb-md">
                                    <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" />
                                   </label>
                                   </div>
                                 </div>
                                    <a href="#" class="chocolat-image" title="App Gallery">
                                    <img alt="" data-toggle="tooltip" data-placement="bottom" title="Show" src="images/Product/1-4.jpg" height="120" width="120">
                                    </a>
                                    <h6 class="text-danger">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Sale 50%</h6>
                                  </div>
                                 </div>
                               </form>

                                <form class="form-horizontal" role="form"><br /><br />
                               <div class="row gallery chocolat-parent" data-chocolat-title="Reactor Gallery">
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                  <div class="form-group">
                                      <h5><label class="col-sm-6 control-label">XXXXXXXXXXX</label></h5>
                                       <div class="col-sm-6">
                                       <label class="cb-checkbox cb-md">
                                        <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                                       </label>
                                     </div>
                                   </div>
                                <a href="#" class="chocolat-image" title="App Gallery">
                                <img alt="" data-toggle="tooltip" data-placement="bottom" title="Show" src="images/Product/1-1.jpg" height="170" width="170">
                                    </a>
                                    <h6 class="text-danger">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Sale 50%</h6>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                 <div class="form-group">
                                  <h5><label class="col-sm-6 control-label">XXXXXXXXXXX</label></h5>
                                   <div class="col-sm-6">
                                   <label class="cb-checkbox cb-md">
                                    <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" />
                                   </label>
                                 </div>
                               </div>
                                <a href="#" class="chocolat-image" title="App Gallery">
                                   <img alt="" data-toggle="tooltip" data-placement="bottom" title="Show" src="images/Product/1-2.jpg" height="120" width="120">
                                </a>
                                <h6 class="text-danger">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Sale 50%</h6>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                     <div class="form-group">
                                  <h5><label class="col-sm-6 control-label">XXXXXXXXXXX</label></h5>
                                   <div class="col-sm-6">
                                   <label class="cb-checkbox cb-md">
                                    <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" />
                                   </label>
                                 </div>
                               </div>
                                    <a href="#" class="chocolat-image" title="App Gallery">
                                    <img alt="" data-toggle="tooltip" data-placement="bottom" title="Show" src="images/Product/1-3.jpg" height="130" width="130">
                                    </a>
                                    <h6 class="text-danger">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Sale 50%</h6>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                     <div class="form-group">
                                  <h5><label class="col-sm-6 control-label">XXXXXXXXXXX</label></h5>
                                   <div class="col-sm-6">
                                   <label class="cb-checkbox cb-md">
                                    <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" />
                                   </label>
                                 </div>
                               </div>
                                    <a href="#" class="chocolat-image" title="App Gallery">
                                    <img alt="" data-toggle="tooltip" data-placement="bottom" title="Show" src="images/Product/1-4.jpg" height="120" width="120">
                                    </a>
                                    <h6 class="text-danger">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Sale 50%</h6>
                                </div>
                                </div>
                               </form>
                               
                                </div>
                              </div>
                             </div>
                            <h4 class="card-title">Advertising </h4>
                                <div class="card bg-white">
                                   <div class="card-block">
                                    <div class="table-responsive">
                                      <table class="table m-b-0">
                                        <thead>
                                          <tr>
                                            <th>No.</th>
                                            <th>Advertising</th>
                                            <th>Select</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td>1</td>
                                            <td>xxxxxxxxxxxxxxxxxx</td>
                                            <td class="center-block"><label class="cb-checkbox cb-md">
                                            <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                                            </label></td>
                                          </tr>
                                           <tr>
                                            <td>2</td>
                                            <td>xxxxxxxxxxxxxxxxxx</td>
                                            <td class="center-block"><label class="cb-checkbox cb-md">
                                            <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                                            </label></td>
                                          </tr>
                                           <tr>
                                            <td>3</td>
                                            <td>xxxxxxxxxxxxxxxxxx</td>
                                            <td class="center-block"><label class="cb-checkbox cb-md">
                                            <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" />
                                            </label></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                   </div>
                                </div>
                            <div class="form-group">
                              <h4><label class="col-sm-2 control-label">Active Staus</label></h4>
                               <div class="col-sm-10">
                                <label class="cb-checkbox cb-md">
                               <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                              </label>
                             </div>
                            </div>
                  <div class="wizard-pager pull-right">
                     <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-save m-r"></i>Save</button>
                     <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-rotate-left m-r"></i>Cancel</button>
                  </div>
                 </div>

                    <!-- Modal Pic1-->
                  <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
    
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Promotions</h4>
                        </div>
                        <div class="modal-body">
                           <div class="card-block">
                          <form class="form-horizontal" role="form">
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><b>Price </b></label>
                            <div class="col-sm-4">
                              <input type="text" class="form-control">
                            </div>
                            <label class="col-sm-2 control-label"><b>Discount (%)</b></label>
                            <div class="col-sm-4">
                              <input type="text" class="form-control">
                            </div>
                          </div>
                         </form>
                          <form class="form-horizontal" role="form">
                          <div class="form-group">
                            <label class="col-sm-2 control-label"><b>From </b></label>
                            <div class="col-sm-4">
                              <input type="text" class="form-control m-b" data-provide="datepicker" placeholder="Start Date">
                            </div>
                            <label class="col-sm-2 control-label"><b>To </b></label>
                            <div class="col-sm-4">
                              <input type="text" class="form-control m-b" data-provide="datepicker" placeholder="End Date">
                            </div>
                          </div>
                         </form>
                            <h5 class="modal-title">History</h5><br />
                            <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover table-condensed responsive m-b-0" data-sortable>
                            <thead>
                              <tr class="bg-amber-light">
                                <th>Price</th>
                                <th>Discount (%)</th>
                                <th>From</th>
                                <th>To</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                              </tr>
                               <tr>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                              </tr>
                               <tr>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                       </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-save m-r"></i>Save</button>
                          <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-rotate-left m-r"></i>Cancel</button>
                        </div>
                      </div>
      
                    </div>
                  </div>
               
                </div>
               </div>
             </div>
            </div>
           </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js"></script>
  <script src="vendor/moment/min/moment.min.js"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
  <script src="vendor/select2/dist/js/select2.js"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js"></script>
  <script src="vendor/sweetalert/dist/sweetalert.min.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/forms/plugins.js"></script>
  <script src="scripts/ui/alert.js"></script>
    <script type="text/javascript">
    $("[data-toggle=tooltip]").tooltip();
    $("[data-toggle=popover]")
      .popover()
      .click(function (e) {
        e.preventDefault();
      });
  </script>
  <!-- end initialize page scripts -->

    
</asp:Content>


