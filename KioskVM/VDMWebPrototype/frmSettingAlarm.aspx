﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmSettingAlarm.aspx.vb" Inherits="frmSettingAlarm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Setting & Monitoring > Alarm Setting</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Found : 3 Condition(s)</div>
          <div class="card-block p-a-0">
             <div class="card-block">
           <div style="width:100%; overflow:auto;">
            <div class="table-responsive flip-scroll">
              <table class="table table-bordered m-b-0">
                <thead>
                  <tr>
                    <th><b class="text-primary">GROUP CODE</b></th>
                    <th><b class="text-primary">GROUP NAME</b></th>
                    <th><b class="text-primary">COUNT ALARM</b></th>
                    <th><b class="text-primary">COUNT EMAIL</b></th>
                    <th><b class="text-primary">Edit</b></th>
                    <th><b class="text-primary">Delete</b></th>
                  </tr>
                </thead>
                <tbody>
                 
                  <tr>
                    <td>xxxxxxx</td>
                    <td>xxxxxxxx</td>
                    <td>xxxxxxx</td>
                    <td>xxxxxxxx</td>
                    <td><a href="frmEditAlarm.aspx" class="btn btn-success mr5">
                        <img src="images/icon/100/pencil.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                    <td><a href="#" class="btn btn-danger mr5">
                        <img src="images/icon/100/delete.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                  </tr>
                  <tr>
                    <td>xxxxxxx</td>
                    <td>xxxxxxxx</td>
                    <td>xxxxxxx</td>
                    <td>xxxxxxxx</td>
                    <td><a href="#" class="btn btn-success mr5">
                        <img src="images/icon/100/pencil.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                    <td><a href="#" class="btn btn-danger mr5">
                        <img src="images/icon/100/delete.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                  </tr>
                  <tr>
                    <td>xxxxxxx</td>
                    <td>xxxxxxxx</td>
                    <td>xxxxxxx</td>
                    <td>xxxxxxxx</td>
                    <td><a href="#" class="btn btn-success mr5">
                        <img src="images/icon/100/pencil.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                    <td><a href="#" class="btn btn-danger mr5">
                        <img src="images/icon/100/delete.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                  </tr>
                </tbody>
              </table><br />
            <div class="row demo-button3">
              <div class="col-sm-3">
                <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple">
                  <i class="fa fa-plus"></i><b> Add New Group</b>
                </a>
              </div>
            </div>
            </div> 

           </div>
          </div>
        </div>
        </div>
    </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
    <script src="scripts/helpers/colors.js"></script>
    <script src="vendor/Chart.js/Chart.min.js"></script>
    <script src="vendor/flot/jquery.flot.js"></script>
    <script src="vendor/flot/jquery.flot.resize.js"></script>
    <script src="vendor/flot/jquery.flot.categories.js"></script>
    <script src="vendor/flot/jquery.flot.stack.js"></script>
    <script src="vendor/flot/jquery.flot.time.js"></script>
    <script src="vendor/flot/jquery.flot.pie.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendor/d3/d3.min.js" charset="utf-8"></script>
    <script src="vendor/c3/c3.min.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>

    <!-- end page scripts -->
    <!-- initialize page scripts -->
    <script src="scripts/helpers/sameheight.js"></script>
    <script src="scripts/ui/dashboard.js"></script>
    <script src="scripts/charts/c3.js"></script>
    <!-- end initialize page scripts -->

</asp:Content>


