﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmEditUser.aspx.vb" Inherits="frmEditUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" href="vendor/jquery.tagsinput/src/jquery.tagsinput.css">
  <link rel="stylesheet" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css">
  <link rel="stylesheet" href="vendor/intl-tel-input/build/css/intlTelInput.css">
  <link rel="stylesheet" href="vendor/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
  <link rel="stylesheet" href="vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
  <link rel="stylesheet" href="vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="vendor/jquery-labelauty/source/jquery-labelauty.css">
  <link rel="stylesheet" href="vendor/multiselect/css/multi-select.css">
  <link rel="stylesheet" href="vendor/ui-select/dist/select.css">
  <link rel="stylesheet" href="vendor/select2/dist/css/select2.css">
  <link rel="stylesheet" href="vendor/selectize/dist/css/selectize.css">
  <!-- end page stylesheets -->

</asp:Content><asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Edit > User</div>
        </div>
         <div class="card bg-white m-b">
            <div class="card-header">Edit User</div>
          <div class="card-block p-a-0">
          <div class="card-block">
            <div class="row m-a-0">
              <div class="col-lg-12">
                <form class="form-horizontal" role="form">
                  <h4 class="title">User Info</h4>
                  <div class="form-group"><br />
                    <label class="col-sm-2 control-label"><b>Account No :</b></label>
                    <div class="col-sm-4">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>First Name :</b></label>
                    <div class="col-sm-4">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Last Name :</b></label>
                    <div class="col-sm-4">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Company Name :</b></label>
                    <div class="col-sm-10">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                    </div>
                  <div class="clearfix"></div>
                    <div class="from-group">
                    <label class="col-sm-2 control-label"><b>Mobile Name :</b></label>
                    <div class="col-sm-4">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Email :</b></label>
                    <div class="col-sm-4">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                  </div>
                </form>
                
              </div>
              </div>
              <div class="row m-a-0">
              <div class="col-lg-12">
                <form class="form-horizontal" role="form">
                  <h4 class="title">Login Info</h4>
                  <div class="form-group"><br />
                    <label class="col-sm-2 control-label"><b>User Name :</b></label>
                    <div class="col-sm-4">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Password :</b></label>
                    <div class="col-sm-4">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                    </div>
                  <div class="clearfix"></div>
                    <div class="from-group">
                    <label class="col-sm-2 control-label"><b>Confirm Password :</b></label>
                    <div class="col-sm-4">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                    </div>
                  <div class="clearfix"></div>
                    <div class="col-lg-6">
                    <h4 class="title">Authorization</h4>
                    <br />
                    <div class="card">
                      <div class="card-block p-a-0">
                        <div class="box-tab justified m-b-0">
                      
                          <ul class="wizard-tabs">
                            <li class="active"><a href="#Role" data-toggle="tab">Role &nbsp; &nbsp;<span class="badge bg-success m-r">3</span></a>
                            </li>
                            <li><a href="#Location" data-toggle="tab">Location &nbsp; &nbsp;<span class="badge bg-success m-r">3</span></a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <div class="tab-pane active in" id="Role">
                             <div class="card-block">
                                <div class="row">
                                  <div class="col-sm-12">
                   
                                    <label class="cb-checkbox cb-md">
                                      <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" /><span class="icon icon-user m-r"></span>Administrator
                                    </label>
                                    <br/>
                                    <label class="cb-checkbox cb-md">
                                      <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" /><span class="icon icon-user m-r"></span>User
                                    </label>
                                    <br/>
                                    <label class="cb-checkbox cb-md">
                                      <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" /><span class="icon icon-user m-r"></span>merchan
                                    </label>
                                    <br/>
                                  </div>
                                </div>
                            </div>
                          </div>
                            <div class="tab-pane" id="Location">
                            <div class="card-block">
                                <div class="row">
                                  <div class="col-sm-12">
                   
                                    <label class="cb-checkbox cb-md">
                                      <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" /><span class="icon icon-pointer m-r"></span>ARL Suvanabhumi Station
                                    </label>
                                    <br/>
                                  </div>
                                </div>
                            </div>
                          </div>
                          </div>
                       
                       </div>
                     </div>   
                  </div> 
                 </div>
                  <div class="clearfix"></div> 
                <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Active Staus :</b></label>
                    <div class="col-sm-4">
                      <label class="cb-checkbox cb-md">
                      <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                    </label>
                    </div>
                  </div>
                  
                  <div class="wizard-pager pull-right">
                    
                      <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-save m-r"></i>Save</button>
                      <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-rotate-left m-r"></i>Cancel</button>
                  
                  </div>
                </form>
                
              </div>

                
           
              </div>
            </div>
          </div>
        </div>
        </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js"></script>
  <script src="vendor/moment/min/moment.min.js"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
  <script src="vendor/select2/dist/js/select2.js"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/forms/plugins.js"></script>
  <!-- end initialize page scripts -->

    
</asp:Content>


