﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmSettingVending_Srceen.aspx.vb" Inherits="frmSettingVending_Srceen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <!-- main area -->
     <div class="row">
       <div class="page-title">
        <div class="title">Setting > Vending Srceen</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Page : 5  </div>
          <div class="card-block p-a-0">
              <ul class="notifications">
                <li>
                  <ul class="notifications-list">
                    <li>
                      <a href="frmEditVending_SrceenHome.aspx">
                        <div class="notification-icon">
                          <div class="circle-icon bg-warning-dark text-white">
                            <img src="images/icon/home.png" alt="Mountain View" style="width:15px;height:15px;">
                          </div>
                        </div>
                        <span class="notification-message"><b>Home</b></span>
                        <span class="btn btn-success mr5 pull-right">
                            <img src="images/icon/100/pencil.png" alt="Mountain View" style="width:15px;height:15px;"></span>
                        </a> 
                    </li>
                    <li>
                      <a href="frmEditVending_SrceenProduct.aspx">
                        <div class="notification-icon">
                          <div class="circle-icon bg-warning-dark text-white">
                            <img src="images/icon/home.png" alt="Mountain View" style="width:15px;height:15px;">
                          </div>
                        </div>
                       <span class="notification-message"><b>Product Detail</b></span>
                        <span class="btn btn-success mr5 pull-right">
                            <img src="images/icon/100/pencil.png" alt="Mountain View" style="width:15px;height:15px;"></span>
                        </a>
                    </li>
                    <li>
                      <a href="frmEditVending_SrceenCondition.aspx">
                        <div class="notification-icon">
                          <div class="circle-icon bg-warning-dark text-white">
                            <img src="images/icon/home.png" alt="Mountain View" style="width:15px;height:15px;">
                          </div>
                        </div>
                        <span class="notification-message"><b>Product Warranty</b></span>
                        <span class="btn btn-success mr5 pull-right">
                            <img src="images/icon/100/pencil.png" alt="Mountain View" style="width:15px;height:15px;"></span>
                        </a>
                     </li>
                    <li>
                      <a href="frmEditVending_SrceenPayment.aspx">
                        <div class="notification-icon">
                          <div class="circle-icon bg-warning-dark text-white">
                            <img src="images/icon/home.png" alt="Mountain View" style="width:15px;height:15px;">
                          </div>
                        </div>
                        <span class="notification-message"><b>Payment</b></span>
                        <span class="btn btn-success mr5 pull-right">
                            <img src="images/icon/100/pencil.png" alt="Mountain View" style="width:15px;height:15px;"></span>
                        </a>
                    </li>
                    <li>
                      <a href="frmEditVending_SrceenComplete.aspx">
                        <div class="notification-icon">
                          <div class="circle-icon bg-warning-dark text-white">
                            <img src="images/icon/home.png" alt="Mountain View" style="width:15px;height:15px;">
                          </div>
                        </div>
                        <span class="notification-message"><b>Complete</b></span>
                        <span class="btn btn-success mr5 pull-right">
                            <img src="images/icon/100/pencil.png" alt="Mountain View" style="width:15px;height:15px;"></span>
                        </a>
                    </li>
                    
                  </ul>
                </li>
              </ul>
            </div>
         </div>
        </div>
      <!-- /main area -->
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
    <script src="scripts/helpers/colors.js"></script>
    <script src="vendor/Chart.js/Chart.min.js"></script>
    <script src="vendor/flot/jquery.flot.js"></script>
    <script src="vendor/flot/jquery.flot.resize.js"></script>
    <script src="vendor/flot/jquery.flot.categories.js"></script>
    <script src="vendor/flot/jquery.flot.stack.js"></script>
    <script src="vendor/flot/jquery.flot.time.js"></script>
    <script src="vendor/flot/jquery.flot.pie.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendor/d3/d3.min.js" charset="utf-8"></script>
    <script src="vendor/c3/c3.min.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>

    <!-- end page scripts -->
    <!-- initialize page scripts -->
    <script src="scripts/helpers/sameheight.js"></script>
    <script src="scripts/ui/dashboard.js"></script>
    <script src="scripts/charts/c3.js"></script>
    <!-- end initialize page scripts -->

    
</asp:Content>

