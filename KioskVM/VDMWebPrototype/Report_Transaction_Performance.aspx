﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="Report_Transaction_Performance.aspx.vb" Inherits="Report_Transaction_Performance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" href="vendor/jquery.tagsinput/src/jquery.tagsinput.css">
  <link rel="stylesheet" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css">
  <link rel="stylesheet" href="vendor/intl-tel-input/build/css/intlTelInput.css">
  <link rel="stylesheet" href="vendor/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
  <link rel="stylesheet" href="vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
  <link rel="stylesheet" href="vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="vendor/jquery-labelauty/source/jquery-labelauty.css">
  <link rel="stylesheet" href="vendor/multiselect/css/multi-select.css">
  <link rel="stylesheet" href="vendor/ui-select/dist/select.css">
  <link rel="stylesheet" href="vendor/select2/dist/css/select2.css">
  <link rel="stylesheet" href="vendor/selectize/dist/css/selectize.css">
  <!-- end page stylesheets -->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Reports > Transaction Performance</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Display Condition</div>
          <div class="card-block p-a-0">
          <div class="card-block">
            <div class="row m-a-0">
             
              <div class="col-lg-12">
                  
               <div class="col-lg-6">
                <form class="form-horizontal" role="form">
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><b>Location :</b></label>
                    <div class="col-sm-8">
                <div class="m-b">
                  <select data-placeholder="All Location" class="chosen" style="width: 100%;">
                    <option>APL Suvanaphumi Station</option>
                    <option>Test Location</option>
                  </select>
                </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><b>From :</b></label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control m-b" data-provide="datepicker" placeholder="Start Date">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><b>Transaction Type :</b></label>
                    <div class="col-sm-8">
                      <div class="btn-group btn-group-justified">
                        <a class="btn btn-warning" role="button"><i class="fa fa-check m-r"></i>Deposit</a>
                        <a class="btn btn-default" role="button"><i class="fa fa-times m-r"></i>Collect</a>
                      </div>
                    </div>
                  </div>
                  <div class="wizard-pager pull-left">
                    
                      <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-check m-r"></i>Apply</button>
                      <button type="button" class="btn btn-success btn-shadow ripple"><i class="fa fa-align-justify m-r"></i>Export Excel</button>
                  
                  </div>
                </form>
              </div>

               <div class="col-lg-6">
                <form class="form-horizontal" role="form">
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><b>By :</b></label>
                    <div class="col-sm-8">
                      <div class="btn-group btn-group-justified">
                        <a class="btn btn-warning" role="button">Hourly</a>
                        <a class="btn btn-default" role="button">Daily</a>
                        <a class="btn btn-default" role="button">Monthly</a>
                        <a class="btn btn-default" role="button">Yearly</a>
                </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"><b>To :</b></label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control m-b" data-provide="datepicker" placeholder="End Date">
                    </div>
                  </div>
                </form>
              </div>
               
             </div>
            </div>
          </div>
        </div>
        </div>

        <div class="card bg-white m-b">
          <div class="card-header">
            <h5>รายงานสรุป Transaction Performance ทุก Location All Locations พบ 1 Record(s)</h5>
          </div>
          <div class="card-block">
             <div style="width:100%; overflow:auto;">
              <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover table-condensed responsive m-b-0" data-sortable>
                <thead>
                  <tr class="bg-info-light">
                    <th><b class="text-primary">DATE</b></th>
                    <th><b class="text-primary">LOCATION</b></th>
                    <th><b class="text-primary">TRANSACTION TYPE</b></th>
                    <th><b class="text-primary">SUCCESS</b></th>
                    <th><b class="text-primary">CANCELED</b></th>
                    <th><b class="text-primary">PROBLEM</b></th>
                    <th><b class="text-primary">TIMEOUT</b></th>
                    <th><b class="text-primary">TOTAL</b></th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                    <td>Jul 28 2016 10:00</td>
                    <td>ARL Suvanabhumi Station</td>
                    <td>DEPOSIT</td>
                    <td>1</td>
                    <td>2</td>
                    <td>0</td>
                    <td>0</td>
                    <td>3</td>
                  </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3"><b class="pull-right">Average</b></td>
                        <td><b>1</b></td>
                        <td><b>2</b></td>
                        <td><b>0</b></td>
                        <td><b>0</b></td>
                        <td><b>0</b></td>
                    </tr>
                    <tr>
                        <td colspan="3"><b class="pull-right">Total</b></td>
                        <td><b>2</b></td>
                        <td><b>1</b></td>
                        <td><b>0</b></td>
                        <td><b>0</b></td>
                        <td><b>3</b></td>
                    </tr>
                </tfoot>
              </table>
            </div>
             </div>
          </div>
        </div>
    </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js"></script>
  <script src="vendor/moment/min/moment.min.js"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
  <script src="vendor/select2/dist/js/select2.js"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/forms/plugins.js"></script>
  <!-- end initialize page scripts -->

    
</asp:Content>

