﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmEditVending_SrceenPayment.aspx.vb" Inherits="frmEditVending_SrceenPayment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" href="vendor/jquery.tagsinput/src/jquery.tagsinput.css">
  <link rel="stylesheet" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css">
  <link rel="stylesheet" href="vendor/intl-tel-input/build/css/intlTelInput.css">
  <link rel="stylesheet" href="vendor/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
  <link rel="stylesheet" href="vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
  <link rel="stylesheet" href="vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="vendor/jquery-labelauty/source/jquery-labelauty.css">
  <link rel="stylesheet" href="vendor/multiselect/css/multi-select.css">
  <link rel="stylesheet" href="vendor/ui-select/dist/select.css">
  <link rel="stylesheet" href="vendor/select2/dist/css/select2.css">
  <link rel="stylesheet" href="vendor/selectize/dist/css/selectize.css">
  <!-- end page stylesheets -->

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
        <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Setting > Vending Srceen</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Edit : Vending Srceen</div>
          <div class="card-block p-a-0">
          <div class="card-block">
            <div class="row m-a-0">
             
              <div class="col-lg-12">
                <form class="form-horizontal" role="form">
                  <h4 class="title">Payment Page</h4>
                  <div class="m-b clearfix">
                 <button type="button" class="btn btn-warning btn-shadow ripple"><p align = "center"><img class="card-img img-responsive center-content" 
                          src="images/icon/National/th.jpg" alt="Card image"  height="40" width="40"></p>Thai Language</button>
                 <button type="button" class="btn btn-warning btn-shadow ripple"><p align = "center"><img class="card-img img-responsive center-content" 
                          src="images/icon/National/en.png" alt="Card image"  height="40" width="40"></p>English Language</button>
                 <button type="button" class="btn btn-warning btn-shadow ripple"><p align = "center"><img class="card-img img-responsive center-content" 
                          src="images/icon/National/jp.png" alt="Card image"  height="40" width="40"></p>Japanese Language</button>
                 <button type="button" class="btn btn-warning btn-shadow ripple"><p align = "center"><img class="card-img img-responsive center-content" 
                          src="images/icon/National/cn.png" alt="Card image"  height="40" width="40"></p>Chinese Language</button>
                <div class="pull-right">
                  <button type="button" class="btn btn-success btn-shadow ripple"><i class="icon-picture"></i>  New picture</button>
                <div class="btn-group">
                  <button type="button" class="btn btn-success btn-shadow ripple" data-toggle="tooltip" data-placement="bottom" title="Show"">
                    <i class="icon-arrow-left"></i>
                  </button>
                  <button type="button" class="btn btn-success btn-shadow ripple" data-toggle="tooltip" data-placement="bottom" title="Next"">
                    <i class="icon-arrow-right"></i>
                   </button>
                  </div>
                 </div>
                </div>
                <div class="card card-inverse bg-white">
              <img class="card-img img-responsive" src="images/DefaultBG/bgPayment.jpg" alt="Card image" style="width:100%;height:100%;">
              <div class="card-img-overlay">
                <h1 class="card-title" data-toggle="modal" data-target=".bs-modal-header">
                    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                    <a class="text-bluegrey"><p align = "center">Card title</p> </a></h1>
                    <br /><br />
                <h4 class="card-text" data-toggle="modal" data-target=".bs-modal-body"><a class="text-bluegrey"><p align = "center">
                    This is a wider card with supporting text below as a natural lead-in.<br />
                    This is a wider card with supporting text below as a natural lead-in <br />
                    This is a wider card with supporting text below as a natural lead-in.<br />
                    This is a wider card with supporting text below as a natural lead-in <br />
                    This is a wider card with supporting text below as a natural lead-in.<br />
                    This is a wider card with supporting text below as a natural lead-in <br />
                    This is a wider card with supporting text below as a natural lead-in.<br />
                    This is a wider card with supporting text below as a natural lead-in <br />
                    This is a wider card with supporting text below as a natural lead-in.<br />
                    This is a wider card with supporting text below as a natural lead-in <br />
                    This is a wider card with supporting text below as a natural lead-in.<br />
                    This is a wider card with supporting text below as a natural lead-in <br />
                    This is a wider card with supporting text below as a natural lead-in.<br />
                    This is a wider card with supporting text below as a natural lead-in <br />
                    This is a wider card with supporting text below as a natural lead-in.<br />
                    This is a wider card with supporting text below as a natural lead-in <br />
                    This is a wider card with supporting text below as a natural lead-in.<br />
                    This is a wider card with supporting text below as a natural lead-in <br />
                    This is a wider card with supporting text below as a natural lead-in.<br />
                    This is a wider card with supporting text below as a natural lead-in </p></a></h4>
                
                   <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                   <h1 class="card-text" data-toggle="modal" data-target=".bs-modal-footer">
                    <a class="text-bluegrey">
                       <p align = "center">XXXXXXXXXXXXXXXX <br />XXXXXXXXXXXXXXXX <br />
                       XXXXXXXXXXXXXXXX <br />XXXXXXXXXXXXXXXX <br />
                       XXXXXXXXXXXXXXXX <br />XXXXXXXXXXXXXXXX</p>
                       
                    </a></h1>
                 </div>
              </div>
                            <div class="form-group">
                              <h4><label class="col-sm-2 control-label">Active Staus</label></h4>
                               <div class="col-sm-10">
                                <label class="cb-checkbox cb-md">
                               <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                              </label>
                             </div>
                            </div>
                            <div class="wizard-pager pull-right">
                              <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-save m-r"></i>Save</button>
                              <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-rotate-left m-r"></i>Cancel</button>
                            </div>
             </form>
              </div>

                <%---------------Header----------------%>
                <div class="modal bs-modal-header" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h4 class="modal-title">Header</h4>
                    </div>
                    <div class="modal-body">
                      <form class="form-horizontal" role="form">
                         <div class="form-group">
                          <label class="col-sm-2 control-label"> <img class="card-img img-responsive center-content" 
                          src="images/icon/National/th.jpg" alt="Card image"  height="70" width="70"></label>
                          <div class="col-sm-10">
                            <textarea class="form-control" rows="3" placeholder="กรุณากรอกข้อความ"></textarea>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-2 control-label"> <img class="card-img img-responsive center-content" 
                          src="images/icon/National/en.png" alt="Card image"  height="70" width="70"></label>
                          <div class="col-sm-10">
                            <textarea class="form-control" rows="3" placeholder="Input your message"></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"> <img class="card-img img-responsive center-content"
                          src="images/icon/National/jp.png" alt="Card image"  height="70" width="70"></label>
                          <div class="col-sm-10">
                            <textarea class="form-control" rows="3" placeholder="メッセージを入力してください"></textarea>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-2 control-label"> <img class="card-img img-responsive center-content" 
                          src="images/icon/National/cn.png" alt="Card image"  height="70" width="70"></label>
                          <div class="col-sm-10">
                            <textarea class="form-control" rows="3" placeholder="请输入信息"></textarea>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer no-border">
                      <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-save m-r"></i>Save</button>
                      <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-rotate-left m-r"></i>Cancel</button>
                  
                    </div>
                  </div>
                </div>
                </div>

                 <%---------------Body----------------%>
                <div class="modal bs-modal-body" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h4 class="modal-title">Body</h4>
                    </div>
                    <div class="modal-body">
                      <div class="card">
                          <div class="card-block p-a-0">
                            <div class="box-tab justified m-b-0">
                              <ul class="nav nav-tabs">
                                <li class="active"><a href="#thai" data-toggle="tab"><img class="card-img img-responsive center-block" 
                          src="images/icon/National/th.jpg" alt="Card image"  height="40" width="40"><br />Thai</a>
                                </li>
                                <li><a href="#english" data-toggle="tab"><img class="card-img img-responsive center-block" 
                          src="images/icon/National/en.png" alt="Card image"  height="40" width="40"><br />English</a>
                                </li>
                                <li><a href="#chinese" data-toggle="tab"><img class="card-img img-responsive center-block" 
                          src="images/icon/National/cn.png" alt="Card image"  height="40" width="40"><br />Chinese</a>
                                </li>
                                <li><a href="#japan" data-toggle="tab"><img class="card-img img-responsive center-block" 
                          src="images/icon/National/jp.png" alt="Card image"  height="40" width="40"><br />Japan</a>
                                </li>
                              </ul>
                              <div class="tab-content">
                                <div class="tab-pane active in" id="thai">
                                    <textarea class="form-control" rows="10" placeholder="กรุณากรอกข้อความ"></textarea>
                                </div>
                                <div class="tab-pane" id="english">
                                    <textarea class="form-control" rows="10" placeholder="Input your message"></textarea>
                                </div>
                                <div class="tab-pane" id="chinese">
                                    <textarea class="form-control" rows="10" placeholder="请输入信息"></textarea>
                                </div>
                                <div class="tab-pane" id="japan">
                                    <textarea class="form-control" rows="10" placeholder="メッセージを入力してください"></textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                      
                    </div>
                    <div class="modal-footer no-border">
                     <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-save m-r"></i>Save</button>
                     <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-rotate-left m-r"></i>Cancel</button>
                  
                    </div>
                  </div>
                </div>
                </div>

                <%---------------Footer----------------%>
                <div class="modal bs-modal-footer" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h4 class="modal-title">Footer</h4>
                    </div>
                    <div class="modal-body">
                      <form class="form-horizontal" role="form">
                         <div class="form-group">
                          <label class="col-sm-2 control-label"> <img class="card-img img-responsive center-content" 
                          src="images/icon/National/th.jpg" alt="Card image"  height="70" width="70"></label>
                          <div class="col-sm-10">
                            <textarea class="form-control" rows="3" placeholder="กรุณากรอกข้อความ"></textarea>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-2 control-label"> <img class="card-img img-responsive center-content" 
                          src="images/icon/National/en.png" alt="Card image"  height="70" width="70"></label>
                          <div class="col-sm-10">
                            <textarea class="form-control" rows="3" placeholder="Input your message"></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label"> <img class="card-img img-responsive center-content"
                          src="images/icon/National/jp.png" alt="Card image"  height="70" width="70"></label>
                          <div class="col-sm-10">
                            <textarea class="form-control" rows="3" placeholder="メッセージを入力してください"></textarea>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-2 control-label"> <img class="card-img img-responsive center-content" 
                          src="images/icon/National/cn.png" alt="Card image"  height="70" width="70"></label>
                          <div class="col-sm-10">
                            <textarea class="form-control" rows="3" placeholder="请输入信息"></textarea>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer no-border">
                      <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-save m-r"></i>Save</button>
                      <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-rotate-left m-r"></i>Cancel</button>
                  
                    </div>
                  </div>
                </div>
                </div>

              <%--<div class="col-lg-12"><br /><br /><br />
                <form class="form-horizontal" role="form">
               <div class="row gallery chocolat-parent" data-chocolat-title="Reactor Gallery">
                <div class="col-md-2 col-sm-2 col-xs-6">
                    <a href="http://lorempixel.com/1920/1080?1" class="chocolat-image" title="App Gallery">
                    <img alt="" src="http://lorempixel.com/600/500?1">
                    </a>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6">
                    <a href="http://lorempixel.com/1920/1080?2" class="chocolat-image" title="App Gallery">
                    <img alt="" src="http://lorempixel.com/600/500?2">
                    </a>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6">
                    <a href="http://lorempixel.com/1920/1080?3" class="chocolat-image" title="App Gallery">
                    <img alt="" src="http://lorempixel.com/600/500?3">
                    </a>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6">
                    <a href="http://lorempixel.com/1920/1080?4" class="chocolat-image" title="App Gallery">
                    <img alt="" src="http://lorempixel.com/600/500?4">
                    </a>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6">
                    <a href="http://lorempixel.com/1920/1080?3" class="chocolat-image" title="App Gallery">
                    <img alt="" src="http://lorempixel.com/600/500?3">
                    </a>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6">
                    <a href="http://lorempixel.com/1920/1080?4" class="chocolat-image" title="App Gallery">
                    <img alt="" src="http://lorempixel.com/600/500?4">
                    </a>
                </div>
                </div>
                </form>
              </div>--%>
            </div>
          </div>
        </div>
        </div>
    </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js"></script>
  <script src="vendor/moment/min/moment.min.js"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
  <script src="vendor/select2/dist/js/select2.js"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/forms/plugins.js"></script>
  <!-- end initialize page scripts -->

    
</asp:Content>
