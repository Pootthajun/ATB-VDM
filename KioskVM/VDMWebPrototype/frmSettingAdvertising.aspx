﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmSettingAdvertising.aspx.vb" Inherits="frmSettingAdvertising" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" href="vendor/jquery.tagsinput/src/jquery.tagsinput.css">
  <link rel="stylesheet" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css">
  <link rel="stylesheet" href="vendor/intl-tel-input/build/css/intlTelInput.css">
  <link rel="stylesheet" href="vendor/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
  <link rel="stylesheet" href="vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
  <link rel="stylesheet" href="vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="vendor/jquery-labelauty/source/jquery-labelauty.css">
  <link rel="stylesheet" href="vendor/multiselect/css/multi-select.css">
  <link rel="stylesheet" href="vendor/ui-select/dist/select.css">
  <link rel="stylesheet" href="vendor/select2/dist/css/select2.css">
  <link rel="stylesheet" href="vendor/selectize/dist/css/selectize.css">
  <!-- end page stylesheets -->

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
        <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Setting > Advertising</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Edit : Advertising</div>
          <div class="card-block p-a-0">
<<<<<<< HEAD
             <div class="card-block">
             <div style="width:100%; overflow:auto;">
            <div class="table-responsive flip-scroll">
              <table class="table table-bordered m-b-0">
                <thead>
                  <tr>
                    <th><b class="text-primary">Ads Code</b></th>
                    <th><b class="text-primary">Ads Name</b></th>
                    <th><b class="text-primary">Duration(sec)</b></th>
                    <th><b class="text-primary">Edit</b></th>
                    <th><b class="text-primary">Delete</b></th>
                  </tr>
                </thead>
                <tbody>
                 
                  <tr>
                    <td>001</td>
                    <td>XXXXXXXXXXXXXXXXXXXX</td>
                    <td>30</td>
                    <td><a href="frmEditAdvertising.aspx" class="btn btn-success mr5">
                        <img src="images/icon/100/pencil.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                    <td><a href="#" class="btn btn-danger mr5">
                        <img src="images/icon/100/delete.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                  </tr>
                  <tr>
                    <td>002</td>
                    <td>XXXXXXXXXXXXXXXXXXXX</td>
                    <td class="center-block">25</td>
                     <td><a href="frmEditAdvertising.aspx" class="btn btn-success mr5">
                        <img src="images/icon/100/pencil.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                    <td><a href="#" class="btn btn-danger mr5">
                        <img src="images/icon/100/delete.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                  </tr>
                  <tr>
                    <td>003</td>
                    <td>XXXXXXXXXXXXXXXXXXXX</td>
                    <td>45</td>
                     <td><a href="frmEditProduct.aspx" class="btn btn-success mr5">
                        <img src="images/icon/100/pencil.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                    <td><a href="#" class="btn btn-danger mr5">
                        <img src="images/icon/100/delete.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                    </td>
                  </tr>
=======
          <div class="card-block">
            <div class="row m-a-0">
           
 <!----------------1-------------------->     
                      <div class="col-lg-4">
                        <form class="form-horizontal" role="form">
                          <div class="row gallery chocolat-parent" data-chocolat-title="Reactor Gallery">
                           <div class="col-md-12 col-sm-12 col-xs-12">
                           <iframe width="280" height="220" src="https://www.youtube.com/embed/gWVz9uozdBw">
                           </iframe>
                           </div>
                          </div>
                         </form>
                       </div>
                    <div class="col-lg-8">
                    <div class="form-group">
                      <label class="col-sm-3 control-label text-primary"><b>Ads Code : </b></label>
                        <div class="col-sm-6">
                         <input type="text" class="form-control">
                        </div> 
                      </div>
                   <div class="m-b clearfix"></div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label text-primary"><b>Name : </b></label>
                        <div class="col-sm-6">
                         <input type="text" class="form-control">
                        </div>
                        <div class="clearfix"></div><br />
                      <label class="col-sm-3 control-label text-primary"><b>Duration(sec) : </b></label>
                        <div class="col-sm-6">
                         <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                     <div class="form-group">
                      <label class="col-sm-3 control-label text-primary"><b>Active Staus :</b></label>
                         <div class="col-sm-6">
                         <label class="cb-checkbox cb-md">
                          <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                          </label>
                        </div>
                      </div>
                  </div>
                    <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-trash m-r"></i>Delete</button>
>>>>>>> ceb7b36d43e7dbbca45bb5f5058d780770a72b5c
                  
     <!----------------2-------------------->
               <div class="clearfix"></div><br /><br />
                    <div class="col-lg-4">
                    <form class="form-horizontal" role="form">
                      <div class="row gallery chocolat-parent" data-chocolat-title="Reactor Gallery">
                       <div class="col-md-12 col-sm-12 col-xs-12">
                       <iframe width="280" height="220" src="https://www.youtube.com/embed/gWVz9uozdBw">
                       </iframe>
                       </div>
                      </div>
                     </form>
                   </div>
                    <div class="col-lg-8">
                   <div class="m-b clearfix">
                    <div class="form-group">
                      <label class="col-sm-3 control-label text-primary"><b>Ads Code : </b></label>
                        <div class="col-sm-6">
                         <input type="text" class="form-control">
                        </div> 
                      </div>
                     <div class="clearfix"></div><br />
                    <div class="form-group">
                      <label class="col-sm-3 control-label text-primary"><b>Name : </b></label>
                        <div class="col-sm-6">
                         <input type="text" class="form-control">
                        </div>
                        <div class="clearfix"></div><br />
                      <label class="col-sm-3 control-label text-primary"><b>Duration(sec) : </b></label>
                        <div class="col-sm-6">
                         <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                     <div class="form-group">
                      <label class="col-sm-3 control-label text-primary"><b>Active Staus :</b></label>
                         <div class="col-sm-6">
                         <label class="cb-checkbox cb-md">
                          <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                          </label>
                        </div>
                      </div> 
               
                   </div>
                  </div>
                    <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-trash m-r"></i>Delete</button>

            </div><br /><br />
            
           <a href="frmEditAdvertising.aspx"><button type="button" class="btn btn-success mr5"><i class="icon-plus m-r"></i>Add Video</button></a>
            </div>
          </div>
        </div>
        </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js"></script>
  <script src="vendor/moment/min/moment.min.js"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
  <script src="vendor/select2/dist/js/select2.js"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/forms/plugins.js"></script>
  <!-- end initialize page scripts -->

    
</asp:Content>
