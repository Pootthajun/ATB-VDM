﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmEditAlarm.aspx.vb" Inherits="frmEditAlarm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" href="vendor/jquery.tagsinput/src/jquery.tagsinput.css">
  <link rel="stylesheet" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css">
  <link rel="stylesheet" href="vendor/intl-tel-input/build/css/intlTelInput.css">
  <link rel="stylesheet" href="vendor/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
  <link rel="stylesheet" href="vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
  <link rel="stylesheet" href="vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="vendor/jquery-labelauty/source/jquery-labelauty.css">
  <link rel="stylesheet" href="vendor/multiselect/css/multi-select.css">
  <link rel="stylesheet" href="vendor/ui-select/dist/select.css">
  <link rel="stylesheet" href="vendor/select2/dist/css/select2.css">
  <link rel="stylesheet" href="vendor/selectize/dist/css/selectize.css">
  <!-- end page stylesheets -->
</asp:Content><asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Setting & Monitoring > Alarm Setting</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Add Alarm</div>
          <div class="card-block p-a-0">
          <div class="card-block">
            <div class="row m-a-0">
              <div class="col-lg-12">
                <form class="form-horizontal" role="form">
                  <h4 class="title">Alarm Group Info</h4>
                  <div class="form-group"><br />
                    <label class="col-sm-3 control-label"><b>GROUP CODE :</b></label>
                    <div class="col-sm-9">
                      <input class="form-control m-b" type="text" placeholder="Default input">
                    </div>
                    <div class="form-group"><br />
                    <label class="col-sm-3 control-label"><b>GROUP NAME :</b></label>
                    <div class="col-sm-9">
                      <input class="form-control m-b" type="text" placeholder="Default input">
                    </div>
                  </div>
                  </div>
                </form>
                
                <form class="form-horizontal" role="form">
                  <h4 class="title">Alam List</h4>
                    <div class="card bg-white m-b">
                      <div class="card-block">
                      <div class="card-block p-a-0">
                        <div class="box-tab justified m-b-0">
                         <div class="table-responsive flip-scroll">
                          <table class="table table-bordered table-striped datatable responsive align-middle bordered">
                            <thead>
                              <tr>
                                <th><label class="cb-checkbox cb-md">
                                     <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                                     </label></th>
                                <th><b class="text-primary">Company</b></th>
                                <th><b class="text-primary">ALARM PROBLEM </b></th>
                                <th><b class="text-primary">ENG DESC</b></th>
                                <th><b class="text-primary">SMS MESSAGE</b></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th><label class="cb-checkbox cb-md">
                                     <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" />
                                     </label></th>
                                <td>10101</td>
                                <td class="numeric">OVER_CPU_USAGE</td>
                                <td class="numeric">CPU usage is overload</td>
                                <td class="numeric">ตู้ที่ มีการใช้ CPU สูงกว่าที่กำหนดไว้ กรุณาตรวจสอบ</td>
                              </tr>
                              <tr>
                                <th><label class="cb-checkbox cb-md">
                                     <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" />
                                     </label></th>
                                <td>10102</td>
                                <td class="numeric">LOW_MEMORY</td>
                                <td class="numeric">System Low Memory</td>
                                <td class="numeric">ตู้ที่ เหลือ Memory ว่างน้อยกว่ากำหนด กรุณาตรวจสอบ</td>
                              </tr>
                              <tr>
                                <th><label class="cb-checkbox cb-md">
                                     <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" />
                                     </label></th>
                                <td>10103</td>
                                <td class="numeric">LOW_DISK_SPACE</td>
                                <td class="numeric">Disk Space Low</td>
                                <td class="numeric">ตู้ที่ เหลือพื้นที่บน Drive น้อยกว่ากำหนด กรุณาตรวจสอบ</td>
                              </tr>
                              <tr>
                                <th><label class="cb-checkbox cb-md">
                                     <input type="checkbox" checked="" name="checked-checkbox-name" value="checked-checkbox-val" />
                                     </label></th>
                                <td>10105</td>
                                <td class="numeric">WINDOW_PROCESS_NOT_START</td>
                                <td class="numeric">Window Process not Start</td>
                                <td class="numeric">ตู้ที่ Window Process ไม่ทำงาน กรุณาตรวจสอบ</td>
                              </tr>
                 
                            </tbody>
                           </table>
                          </div>  
                        </div>
                       </div>
                      </div>
                    </div>
                </form>

                 <form class="form-horizontal" role="form">
                  <h4 class="title">Vending Alam</h4>
                    <div class="card bg-white m-b">
         
                      <div class="card-block">

                      <div class="card-block p-a-0">
                        <div class="box-tab justified m-b-0">
                          <div class="table-responsive flip-scroll">
                          <table class="table table-bordered table-striped datatable responsive align-middle bordered">
                            <thead>
                              <tr>
                                <th><b class="text-primary">NO.</b></th>
                                <th><b class="text-primary">MAC ADDRESS</b></th>
                                <th><b class="text-primary">DELETE</b></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class="numeric">1</td>
                                <td class="numeric"><div class="col-sm-10">
                                <input class="form-control m-b" type="text" placeholder=" ">
                                </div></td>
                                <td><a href="#" class="btn btn-danger mr5">
                                <img src="images/icon/100/delete.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                                </td>
                              </tr>
                        
                            </tbody>
                           </table><br />
                            <div class="row demo-button3">
                              <div class="col-sm-3">
                                <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple">
                                  <i class="fa fa-plus"></i><b> Add New Computer</b>
                                </a>
                              </div>
                            </div>
                          </div>  
                          </div>
                      </div>
                      </div>
                    </div>
                </form>

                <form class="form-horizontal" role="form">
                  <h4 class="title">Email Alarm</h4>
                    <div class="card bg-white m-b">
         
                      <div class="card-block">

                      <div class="card-block p-a-0">
                        <div class="box-tab justified m-b-0">
                          <div class="table-responsive flip-scroll">
                          <table class="table table-bordered table-striped datatable responsive align-middle bordered">
                            <thead>
                              <tr>
                                <th><b class="text-primary">NO.</b></th>
                                <th><b class="text-primary">EMAIL</b></th>
                                <th><b class="text-primary">DELETE</b></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class="numeric">1</td>
                                <td class="numeric"><div class="col-sm-10">
                                <input class="form-control m-b" type="text" placeholder=" ">
                                </div></td>
                                <td><a href="#" class="btn btn-danger mr5">
                                <img src="images/icon/100/delete.png" alt="Mountain View" style="width:15px;height:15px;"></a>
                                </td>
                              </tr>
                        
                            </tbody>
                           </table><br />
                            <div class="row demo-button3">
                              <div class="col-sm-3">
                                <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple">
                                  <i class="fa fa-plus"></i><b> Add New Email</b>
                                </a>
                              </div>
                            </div>
                          </div>  
                          </div>
                      </div>
                      </div>
                    </div>
                </form>
              </div>
              <div class="form-group">
                    <label class="col-sm-3 control-label"><h4 class="title">Active Status :</h4></label>
                    <div class="col-sm-9">
                      <label class="cb-checkbox cb-md">
                      <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                    </label>
                    </div>
                  </div>
                  
                  <div class="wizard-pager pull-right">
                    
                      <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-save m-r"></i>Save</button>
                      <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-rotate-left m-r"></i>Cancel</button>
                  
                  </div>
              </div>
            </div>
          </div>
        </div>
        </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
   <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js"></script>
  <script src="vendor/moment/min/moment.min.js"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
  <script src="vendor/select2/dist/js/select2.js"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/forms/plugins.js"></script>
  <!-- end initialize page scripts -->
    
</asp:Content>
