﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmEditProduct.aspx.vb" Inherits="frmEditProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" href="vendor/jquery.tagsinput/src/jquery.tagsinput.css">
  <link rel="stylesheet" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css">
  <link rel="stylesheet" href="vendor/intl-tel-input/build/css/intlTelInput.css">
  <link rel="stylesheet" href="vendor/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
  <link rel="stylesheet" href="vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
  <link rel="stylesheet" href="vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="vendor/jquery-labelauty/source/jquery-labelauty.css">
  <link rel="stylesheet" href="vendor/multiselect/css/multi-select.css">
  <link rel="stylesheet" href="vendor/ui-select/dist/select.css">
  <link rel="stylesheet" href="vendor/select2/dist/css/select2.css">
  <link rel="stylesheet" href="vendor/selectize/dist/css/selectize.css">
  <!-- end page stylesheets -->

</asp:Content><asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Setting > Product</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Edit : Product</div>
          <div class="card-block p-a-0">
          <div class="card-block">
            <div class="row m-a-0">
              
              <div class="col-lg-3">
                <form class="form-horizontal" role="form">
                  <h4 class="title">Picture Example</h4><br />
                  <div class="row gallery chocolat-parent" data-chocolat-title="Reactor Gallery"><br />
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <a href="images/unsplash/10.jpg" class="chocolat-image" title="App Gallery" style="width:200px;height:200px;">
                        <img alt="" src="images/unsplash/10.jpg">
                        </a>
                      </div>
                    <button type="button" class="btn btn-success center-wrapper btn-shadow ripple"><i class="fa fa-picture-o m-r"></i>New Images</button>
                    </div>
                 
                </form>
              </div>
              <div class="col-lg-9">
                <form class="form-horizontal" role="form">
                  <h4 class="title center-wrapper"> Product Info</h4>
                  <div class="form-group"><br />
                    <label class="col-sm-3 control-label"><b>Category Code :</b></label>
                    <div class="col-sm-8">
                      <input class="form-control m-b" type="text" placeholder="A-01">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label"><b>Product Code :</b></label>
                    <div class="col-sm-8">
                      <input class="form-control m-b" type="text" placeholder="001">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label"><b>Product Name :</b></label>
                    <div class="col-sm-8">
                      <textarea class="form-control" rows="3"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label"><b>Detail :</b></label>
                    <div class="col-sm-8">
                      <textarea class="form-control" rows="4"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label"><b>Price :</b></label>
                    <div class="col-sm-8">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label"><b>Cost :</b></label>
                    <div class="col-sm-8">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                  </div>
                 <div class="form-group">
                    <label class="col-sm-3 control-label"><b>Adjust area :</b></label>
                    <div class="col-sm-8">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                  </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><b>Active Staus :</b></label>
                    <div class="col-sm-8">
                      <label class="cb-checkbox cb-md">
                      <input type="checkbox" checked="checked" name="checked-checkbox-name" value="checked-checkbox-val" />
                    </label>
                    </div>
                  </div>
                 <div class="wizard-pager pull-right">
                    
                      <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-save m-r"></i>Save</button>
                      <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-rotate-left m-r"></i>Cancel</button>
                  
                  </div>
                </form>
              </div>

              </div>
            </div>
          </div>
        </div>
        </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js"></script>
  <script src="vendor/moment/min/moment.min.js"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
  <script src="vendor/select2/dist/js/select2.js"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/forms/plugins.js"></script>
  <!-- end initialize page scripts -->

    
</asp:Content>
