﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="Report_Transaction_Log.aspx.vb" Inherits="Report_Transaction_Log" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" href="vendor/jquery.tagsinput/src/jquery.tagsinput.css">
  <link rel="stylesheet" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css">
  <link rel="stylesheet" href="vendor/intl-tel-input/build/css/intlTelInput.css">
  <link rel="stylesheet" href="vendor/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
  <link rel="stylesheet" href="vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
  <link rel="stylesheet" href="vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="vendor/jquery-labelauty/source/jquery-labelauty.css">
  <link rel="stylesheet" href="vendor/multiselect/css/multi-select.css">
  <link rel="stylesheet" href="vendor/ui-select/dist/select.css">
  <link rel="stylesheet" href="vendor/select2/dist/css/select2.css">
  <link rel="stylesheet" href="vendor/selectize/dist/css/selectize.css">
  <!-- end page stylesheets -->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Reports > Transaction Log</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Display Condition</div>
          <div class="card-block p-a-0">
          <div class="card-block">
            <div class="row m-a-0">
             
              <div class="col-lg-12"> 
                <form class="form-horizontal" role="form">
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Location :</b></label>
                    <div class="col-sm-4">
                    <div class="m-b">
                      <select data-placeholder="All Location" class="chosen" style="width: 100%;">
                       <option>APL Suvanaphumi Station</option>
                       <option>Test Location</option>
                      </select>
                     </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Deposit Transaction No :</b></label>
                    <div class="col-sm-4">
                      <input class="form-control m-b" type="text" placeholder="Deposit Transaction No">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Collect Transaction No :</b></label>
                    <div class="col-sm-4">
                      <input class="form-control m-b" type="text" placeholder="Collect Transaction No">
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Deposit Status :</b></label>
                    <div class="col-sm-4">
                  <div class="m-b">
                    <select data-placeholder="All Status" class="chosen" style="width: 100%;">
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                     </select>
                    </div>
                   </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Collect Status :</b></label>
                    <div class="col-sm-4">
                  <div class="m-b">
                    <select data-placeholder="All Status" class="chosen" style="width: 100%;">
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                     </select>
                    </div>
                   </div>
                  </div>
                  <div class="clearfix"></div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Customer :</b></label>
                    <div class="col-sm-4">
                      <input class="form-control m-b" type="text" placeholder="">
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-sm-2 control-label"><b>Locker Size :</b></label>
                    <div class="col-sm-4">
                  <div class="m-b">
                    <select data-placeholder="All Size" class="chosen" style="width: 100%;">
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                    <option>xxxxxxxxxx</option>
                     </select>
                    </div>
                   </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>From :</b></label>
                    <div class="col-sm-2">
                    <input type="text" class="form-control m-b" data-provide="datepicker" placeholder="Datepicker">
                    </div>
                    <div class="col-sm-2">
                    <div class="input-group">
                        <input type="text" class="form-control time-picker" />
                        <span class="input-group-addon add-on">
                    <i class="fa fa-clock-o"></i>
                    </span>
                   </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><b>To :</b></label>
                    <div class="col-sm-2">
                    <input type="text" class="form-control m-b" data-provide="datepicker" placeholder="Datepicker">
                    </div>
                    <div class="col-sm-2">
                    <div class="input-group">
                        <input type="text" class="form-control time-picker" />
                        <span class="input-group-addon add-on">
                    <i class="fa fa-clock-o"></i>
                    </span>
                   </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>

                </form>
                <div class="clearfix"></div>

               <div class="wizard-pager pull-left">
                    
                      <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-check m-r"></i>Apply</button>
                      <button type="button" class="btn btn-success btn-shadow ripple"><i class="fa fa-align-justify m-r"></i>Export Excel</button>
                  
                  </div>
             </div>
            </div>
          </div>
        </div>
        </div>

        <div class="card bg-white m-b">
          <div class="card-header">
            <h5>Customer Transaction Log ทุก Location ตั้งแต่ Jul 31 2016 เวลา 12:00 AM ถึง Aug 01 2016 เวลา 12:59 PMไม่พบข้อมูล</h5>
          </div>
          <div class="card-block">
             <div style="width:100%; overflow:auto;">
              <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover table-condensed responsive m-b-0" data-sortable>
                <thead>
                  <tr class="bg-info-light">
                    <th rowspan="3"><b class="text-primary">Code</b></th>
                    <th rowspan="3"><b class="text-primary">Company</b></th>
                    <th rowspan="3"><b class="text-primary">DEPOSIT TRANSACTION</b></th>
                    <th rowspan="3"><b class="text-primary">START TIME</b></th>
                    <th rowspan="3"><b class="text-primary">DEPOSIT STATUS</b></th>
                    <th rowspan="3"><b class="text-primary">LOCATION</b></th>
                    <th rowspan="3"><b class="text-primary">VENDING</b></th>
                    <th rowspan="3"><b class="text-primary">DEPOSIT AMOUNT</b></th>
                    <th rowspan="3"><b class="text-primary">CARD TYPE</b></th>
                    <th rowspan="3"><b class="text-primary">NATIONALITY</b></th>
                    <th rowspan="3"><b class="text-primary">PAID TIME</b></th>
                    <th rowspan="3"><b class="text-primary">LAST ACTIVITY</b></th>
                    <th colspan="7"><b class="text-primary">PAYMENT</b></th>
                    <th colspan="3"><b class="text-primary">CHANGE</b></th>
                    <th rowspan="3"><b class="text-primary">COLLECT TRANSACTION</b></th>
                    <th rowspan="3"><b class="text-primary">COLLECT TIME</b></th>
                    <th rowspan="3"><b class="text-primary">PAID TIME</b></th>
                    <th rowspan="3"><b class="text-primary">COLLECT STATUS</b></th>
                    <th rowspan="3"><b class="text-primary">LOST QR CODE</b></th>
                    <th rowspan="3"><b class="text-primary">SERVICE TIME</b></th>
                    <th rowspan="3"><b class="text-primary">SERVICE AMOUNT</b></th>
                    <th colspan="7"><b class="text-primary">PAYMENT</b></th>
                    <th colspan="3"><b class="text-primary">CHANGE</b></th>
                  </tr>
                  <tr class="bg-info-light">
                      <th colspan="2"><b class="text-primary">COIN</b></th>
                      <th colspan="5"><b class="text-primary">BANKNOTE</th>
                      <th colspan="1"><b class="text-primary">COIN</th>
                      <th colspan="2"><b class="text-primary">BANKNOTE</th>
                      <th colspan="2"><b class="text-primary">COIN</th>
                      <th colspan="5"><b class="text-primary">BANKNOTE</th>
                      <th colspan="1"><b class="text-primary">COIN</th>
                      <th colspan="2"><b class="text-primary">BANKNOTE</th>
                  </tr>
                  <tr class="bg-info-light">
                      <th><b class="text-primary">5</b></th>
                      <th><b class="text-primary">10</b></th>
                      <th><b class="text-primary">20</b></th>
                      <th><b class="text-primary">50</b></th>
                      <th><b class="text-primary">100</b></th>
                      <th><b class="text-primary">500</b></th>
                      <th><b class="text-primary">1000</b></th>
                      <th><b class="text-primary">5</b></th>
                      <th><b class="text-primary">10</b></th>
                      <th><b class="text-primary">100</b></th>
                      <th><b class="text-primary">5</b></th>
                      <th><b class="text-primary">10</b></th>
                      <th><b class="text-primary">20</b></th>
                      <th><b class="text-primary">50</b></th>
                      <th><b class="text-primary">100</b></th>
                      <th><b class="text-primary">500</b></th>
                      <th><b class="text-primary">1000</b></th>
                      <th><b class="text-primary">5</b></th>
                      <th><b class="text-primary">10</b></th>
                      <th><b class="text-primary">100</b></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
             </div>
          </div>
        </div>
    </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js"></script>
  <script src="vendor/moment/min/moment.min.js"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
  <script src="vendor/select2/dist/js/select2.js"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/forms/plugins.js"></script>
  <!-- end initialize page scripts -->

    
</asp:Content>



