﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmDetailAlarmMonitoring.aspx.vb" Inherits="frmDetailAlarmMonitoring" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Alarm & Monitoring > Vending Monitoring</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header"><b class="text-blue">Monitoring : AUTOBOX-PC (Offline)</b></div>
          <div class="card-block p-a-0"> 
          </div>
        </div>

         <div class="col-md-12">
            <div class="row">
          <div class="col-sm-4">
            <div class="card card-block bg-white">
              <h4 class="card-title"><img src="images/icon//100/koisk_ab.png" alt="Mountain View" style="width:50px;height:50px;">Detail</h4>
              <div class="table-responsive">
              <table class="table m-b-0">
                <tbody>
                  <tr>
                   <th>Computer Name :</th>
                   <td><b class="text-blue">AUTOBOX-PC</b></td>
                  </tr>
                  <tr>
                    <th>Location :</th>
                    <td><b class="text-blue">ARL Suvanabhumi Station</b></td>
                  </tr>
                  <tr>
                   <th>IP Address :</th>
                   <td><b class="text-blue">192.168.1.113</b></td>
                  </tr>
                  <tr>
                    <th>Mac Address :</th>
                    <td><b class="text-blue">6C-71-D9-FA-2E-07</b></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
         </div>
         <div class="col-sm-4">
            <div class="card card-block bg-white">
              <h4 class="card-title">PERIPHERAL CONDITION</h4>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple">
                        <img src="images/icon//100/coin_in.png" alt="Mountain View" style="width:40px;height:40px;">Coin In</a>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple">
                        <img src="images/icon//100/cash_in.png" alt="Mountain View" style="width:40px;height:40px;">Banknote In</a>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple">
                        <img src="images/icon//100/5_out.png" alt="Mountain View" style="width:40px;height:40px;">Coin 5</a>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple">
                        <img src="images/icon//100/20_out.png" alt="Mountain View" style="width:40px;height:40px;">Banknote Out 20</a>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple">
                        <img src="images/icon//100/100_out.png" alt="Mountain View" style="width:40px;height:40px;">Banknote Out 100</a>
                    <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple">
                        <img src="images/icon//100/printer.png" alt="Mountain View" style="width:40px;height:40px;">Printer</a>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card card-block bg-white">
              <h4 class="card-title">MONEY STOCK</h4>
              <b class="text-blue">Banknote In Level <i class="pull-right">50/500</i></b>
             <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="500" data-percent="20" style="width:20%">
              </div>
            </div>
            <b class="text-blue">Banknote Out 20 Level <i class="pull-right">160/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="160" aria-valuemin="0" aria-valuemax="500" data-percent="160" style="width:40%">
              </div>
            </div>
            <b class="text-blue">Coin In Level <i class="pull-right">100/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="500" data-percent="30" style="width:60%">
              </div>
            </div>
            <b class="text-blue">Coin 5 Level <i class="pull-right">200/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="500" data-percent=45" style="width:20%">
              </div>
            </div>
            <b class="text-blue">Banknote Out 100 Level <i class="pull-right">300/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="500" data-percent="60" style="width:40%">
              </div>
            </div>
             <h4 class="card-title">PRINTER PAPER</h4>
            <b class="text-blue">Printer Level <i class="pull-right">140/200</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="200" data-percent="70" style="width:60%">
              </div>
            </div>
            </div>
          </div>
        </div>
         <!-- main area -->
       
          <div class="col-md-6 col-lg-3">
            <div class="card bg-white">
              <div class="card-header">
                <b>TODAY AVALIABLE</b>
              </div>
              <div class="card-block text-center">
                <div class="piechart">
                  <div class="bounce" data-percent="86">
                    <div>
                      <div class="percent h1"></div>
                      <small>Bounce Rate</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="card bg-white">
              <div class="card-header">
                <b>CPU USAGE</b>
              </div>
              <div class="card-block text-center">
                <div class="piechart">
                  <div class="signup" data-percent="52">
                    <div>
                      <div class="percent h1"></div>
                      <small>Signup Rate</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="card bg-white">
              <div class="card-header">
              <b>RAM USAGE</b>  
              </div>
              <div class="card-block text-center">
                <div class="piechart">
                  <div class="bounce" data-percent="86">
                    <div>
                      <div class="percent h1"></div>
                      <small>Bounce Rate</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="card bg-white">
              <div class="card-header">
               <b>DISK USAGE</b> 
              </div>
              <div class="card-block text-center">
                <div class="piechart">
                  <div class="signup" data-percent="52">
                    <div>
                      <div class="percent h1"></div>
                      <small>Signup Rate</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      <!-- /main area -->
          </div>
        
    </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
    <script src="scripts/helpers/colors.js"></script>
    <script src="vendor/Chart.js/Chart.min.js"></script>
    <script src="vendor/flot/jquery.flot.js"></script>
    <script src="vendor/flot/jquery.flot.resize.js"></script>
    <script src="vendor/flot/jquery.flot.categories.js"></script>
    <script src="vendor/flot/jquery.flot.stack.js"></script>
    <script src="vendor/flot/jquery.flot.time.js"></script>
    <script src="vendor/flot/jquery.flot.pie.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendor/d3/d3.min.js" charset="utf-8"></script>
    <script src="vendor/c3/c3.min.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>

    <!-- end page scripts -->
    <!-- initialize page scripts -->
    <script src="scripts/helpers/sameheight.js"></script>
    <script src="scripts/ui/dashboard.js"></script>
    <script src="scripts/charts/c3.js"></script>
  <script src="vendor/jquery.easy-pie-chart/dist/jquery.easypiechart.js"></script>
    <!-- end initialize page scripts -->
    
    <script src="scripts/charts/easypie.js"></script>
    <script type="text/javascript" lang="javascript">

        var timerRefresh;
        var refreshInterval = 60000;  //อัพเดททุกๆ 1 นาที
        function setRefreshMonitoring() {
            timerRefresh = setTimeout(updateMonitoring, refreshInterval);
        }
        $(document).ready(function () {
            setRefreshMonitoring();
        });

        function updateMonitoring() {

            var btn = document.getElementById('btnRefreshData');
            if (btn) {
                btn.click();
                timerRefresh = setTimeout(updateMonitoring, refreshInterval);
            }
        }
    </script>
</asp:Content>

