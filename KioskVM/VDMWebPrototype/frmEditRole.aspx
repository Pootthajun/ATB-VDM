﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmEditRole.aspx.vb" Inherits="frmEditRole" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>
  <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" href="vendor/jquery.tagsinput/src/jquery.tagsinput.css">
  <link rel="stylesheet" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css">
  <link rel="stylesheet" href="vendor/intl-tel-input/build/css/intlTelInput.css">
  <link rel="stylesheet" href="vendor/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
  <link rel="stylesheet" href="vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css">
  <link rel="stylesheet" href="vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="vendor/jquery-labelauty/source/jquery-labelauty.css">
  <link rel="stylesheet" href="vendor/multiselect/css/multi-select.css">
  <link rel="stylesheet" href="vendor/ui-select/dist/select.css">
  <link rel="stylesheet" href="vendor/select2/dist/css/select2.css">
  <link rel="stylesheet" href="vendor/selectize/dist/css/selectize.css">
  <!-- end page stylesheets -->

</asp:Content><asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Setting > Location</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Found : 2 Location(s)</div>
          <div class="card-block p-a-0">
          <div class="card-block">
            <div class="row m-a-0">
             
              <div class="col-lg-12">
                <form class="form-horizontal" role="form">
                  <h4 class="title">Role Info</h4>
                  <div class="form-group"><br />
                    <label class="col-sm-2 control-label"><b>Role Name :</b></label>
                    <div class="col-sm-8">
                      <input class="form-control m-b" type="text" placeholder="Administrator">
                    </div>
                  </div>
                </form>
               
                <form class="form-horizontal" role="form">
                    <br /><br /><br />
                    <h4 class="title">Authorization</h4>
                       <div class="row">
                      <div class="col-md-12">
                        <div class="card">
                            <div class="card-block p-a-0">
                                <div class="box-tab vertical m-b-0">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#Admin" data-toggle="tab"><b>Admin Web&nbsp; &nbsp;<span class="badge bg-success m-r">3</span></b></a>
                                    </li>
                                    <li><a href="#Vending" data-toggle="tab"><b>Vending Staff Console&nbsp; &nbsp;<span class="badge bg-success m-r">3</span></b></a>
                                    </li>
                                    <li><a href="#Member" data-toggle="tab"><b>Member List&nbsp; &nbsp;<span class="badge bg-success m-r">3</span></b></a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                  <div class="tab-pane active in" id="Admin">  
                                      <h4 class="title text-blue">Alarm & Monitoring</h4> 
                                          <div class="form-group"><br />
                                            <label class="col-sm-4 control-label">Vending Monitoring :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-openid" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                </div>
                                             </div>
                                            </div>

                                       <h4 class="title text-blue">Report</h4> 
                                          <div class="form-group">
                                            <label class="col-sm-4 control-label">Report Transaction Log :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;View</a>
                                                </div>
                                             </div>
                                            </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Transaction Performance :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;View</a>
                                                </div>
                                             </div>
                                            </div>
                                       <div class="form-group">
                                            <label class="col-sm-4 control-label">Summary Report By Location :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;View</a>
                                                </div>
                                             </div>
                                            </div>
                                       <div class="form-group">
                                            <label class="col-sm-4 control-label">Summary Report By Locker Size :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;View</a>
                                                </div>
                                             </div>
                                            </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Mail Group Reports :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                      
                                        <h4 class="title text-blue">Setting</h4> 
                                          <div class="form-group"><br />
                                            <label class="col-sm-4 control-label">Location :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-sm-4 control-label">Business Owner :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-sm-4 control-label">Vending Machine :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-sm-4 control-label">Advertising :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-sm-4 control-label">Category Group :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-sm-4 control-label">Product :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-sm-4 control-label">Vending Srceen :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-sm-4 control-label">Authorization :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-sm-4 control-label">Role :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-sm-4 control-label">User :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                       <h4 class="title text-blue">Alarm & Monitoring</h4> 
                                          <div class="form-group"><br />
                                            <label class="col-sm-4 control-label">Vending Monitoring :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-openid" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                </div>
                                             </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-sm-4 control-label">Alarm Setting :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>

                                          </div>
                            
                                <div class="tab-pane" id="Vending">
                                <h4 class="title text-blue">Report</h4> 
                                          <div class="form-group">
                                            <label class="col-sm-4 control-label">Fill Paper :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Fill Money :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                       <div class="form-group">
                                            <label class="col-sm-4 control-label">Vending Setting :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Device Setting :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                      <div class="form-group">
                                            <label class="col-sm-4 control-label">Locker Setting :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;View</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;Edit</a>
                                                </div>
                                             </div>
                                            </div>
                                      <div class="form-group">
                                            <label class="col-sm-4 control-label">Open Vending :</label>
                                            <div class="col-sm-8 m-b">
                                                <div class="btn-group btn-group-justified">
                                                    <a class="btn btn-default" role="button"><i class="climacon moon new"></i>&nbsp; &nbsp;N/A</a>
                                                    <a class="btn btn-openid" role="button"><i class="fa fa-check"></i>&nbsp; &nbsp;View</a>
                                                </div>
                                             </div>
                                            </div>
                                        </div>

                                <div class="tab-pane" id="Member"><br />
                                        <h4 class="title text-blue">Total 3 members</h4> 
                                        <input class="col-lg-12" id="tags" type="text" value="akkarawatp,autobox,nongnuch ,TIT-DevPC,autobox,nongnuch ,TIT-DevPC"" />   
                                     <div class="wizard-pager pull-left">
                                       <button type="button" class="btn btn-vimeo btn-shadow ripple"><i class="fa fa-plus m-r"></i>Add New Member</button>
                                       <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-times-circle m-r"></i>Clear All Member</button>
                                    </div>
                                   </div>
                                  </div>
                                 </div>
                                </div>
                               </div>
                              </div>
                     
                  <div class="wizard-pager pull-right">
                    
                      <button type="button" class="btn btn-primary btn-shadow ripple"><i class="fa fa-save m-r"></i>Save</button>
                      <button type="button" class="btn btn-danger btn-shadow ripple"><i class="fa fa-rotate-left m-r"></i>Cancel</button>
                    </div>
                  </div>
                </form>

              </div>
              </div>
            </div>
          </div>
        </div>
        </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
   <<!-- page scripts -->
  <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js"></script>
  <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js"></script>
  <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js"></script>
  <script src="vendor/moment/min/moment.min.js"></script>
  <script src="vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
  <script src="vendor/select2/dist/js/select2.js"></script>
  <script src="vendor/selectize/dist/js/standalone/selectize.min.js"></script>
  <script src="vendor/jquery-labelauty/source/jquery-labelauty.js"></script>
  <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="vendor/typeahead.js/dist/typeahead.bundle.js"></script>
  <script src="vendor/multiselect/js/jquery.multi-select.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/forms/plugins.js"></script>
  <!-- end initialize page scripts -->

    
</asp:Content>
