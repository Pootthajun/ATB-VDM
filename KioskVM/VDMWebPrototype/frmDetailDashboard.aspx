﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmDetailDashboard.aspx.vb" Inherits="frmDetailDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem; }
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
       <!-- main area -->
  
        <div class="page-title">
          <div class="title">Detail </div>
          <div class="sub-title">Flot chart plugin</div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="card bg-white">
              <div class="card-header text-blue">
                  <b>Grouped bar series</b> 
              </div>
              <div class="card-block text-center">
                <div class="chart line" style="height:150px"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card bg-white">
                <div class="p-a bb">
                NOTIFICATIONS
              </div>
              <ul class="notifications">
                <li>
                  <ul class="notifications-list">
                    <li>
                      <a href="javascript:;">
                        <div class="notification-icon">
                          <div class="circle-icon bg-success text-white">
                            <i class="icon-bulb"></i>
                          </div>
                        </div>
                        <span class="notification-message"><b>XXXXXXXX</b>xxxxxxxxxxxxxxxxxxxx</span>
                        <span class="time">2s</span>
                      </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                        <div class="notification-icon">
                          <div class="circle-icon bg-danger text-white">
                            <i class="icon-cursor"></i>
                          </div>
                        </div>
                        <span class="notification-message"><b>XXXXXXXX</b>xxxxxxxxxxxxxxxxxxxx</span>
                        <span class="time">4h</span>
                      </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                        <div class="notification-icon">
                          <div class="circle-icon bg-primary text-white">
                            <i class="icon-basket"></i>
                          </div>
                        </div>
                        <span class="notification-message"><b>XXXXXXXX</b><b>xxxxxxxxxxxxxxxxxxxx</b></span>
                        <span class="time">2d</span>
                      </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                        <div class="notification-icon">
                          <div class="circle-icon bg-info text-white">
                            <i class="icon-bubble"></i>
                          </div>
                        </div>
                        <span class="notification-message"><b>XXXXXXXX</b><b>xxxxxxxxxxxxxxxxxxxx</b></span>
                        <span class="time">2s</span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
            </div>
         <%--Bar--%>
        <div class="col-sm-4">
            <div class="card bg-white" id="BarBlock">
                <div class="card-block text-center p-t-0">
                    <h5 class="text-green">Annual Service Transactions</h5>
                    <div class="chart bar" style="height: 200px"></div>
                    <a href="javascript:;" class="btn btn-success btn-xs" >Success</a>
                    <a href="javascript:;" class="btn btn-danger btn-xs">Lost Transaction</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card bg-white" id="chartpie">
                <div class="card-block text-center p-t-0">
                    <h5 class="text-green">Frequency Service Time in This Month</h5>
                    <div class="chart pie" style="height:200px"></div>
                    <a href="javascript:;" class="btn btn-success btn-xs" >Success</a>
                    <a href="javascript:;" class="btn btn-danger btn-xs">Lost Transaction</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card bg-white" id="chartrealtime">
                <div class="card-block text-center p-t-0">
                    <h5 class="text-green">Annual Service Transactions</h5>
                    <div class="chart realtime" style="height:200px"></div>
                    <a href="javascript:;" class="btn btn-success btn-xs" >Success</a>
                    <a href="javascript:;" class="btn btn-danger btn-xs">Lost Transaction</a>
                </div>
            </div>
        </div>
        
        </div>
        </div>
 
    <asp:Button ID="Button1" runat="server" style="display:none;" ClientIDMode="Static" /></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
    <script src="scripts/helpers/colors.js"></script>
    <script src="vendor/Chart.js/Chart.min.js"></script>
    <script src="vendor/flot/jquery.flot.js"></script>
    <script src="vendor/flot/jquery.flot.resize.js"></script>
    <script src="vendor/flot/jquery.flot.categories.js"></script>
    <script src="vendor/flot/jquery.flot.stack.js"></script>
    <script src="vendor/flot/jquery.flot.time.js"></script>
    <script src="vendor/flot/jquery.flot.pie.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendor/d3/d3.min.js" charset="utf-8"></script>
    <script src="vendor/c3/c3.min.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>

    <!-- end page scripts -->
    <!-- initialize page scripts -->
    <script src="scripts/helpers/sameheight.js"></script>
    <script src="scripts/ui/dashboard.js"></script>
    <script src="scripts/charts/c3.js"></script>
    <!-- end initialize page scripts -->
    <script src="scripts/charts/flot.js"></script>
    <script type="text/javascript" lang="javascript">

        var timerRefresh;
        var refreshInterval = 60000;  //อัพเดททุกๆ 1 นาที
        function setRefreshMonitoring() {
            timerRefresh = setTimeout(updateMonitoring, refreshInterval);
        }
        $(document).ready(function () {
            setRefreshMonitoring();
        });

        function updateMonitoring() {

            var btn = document.getElementById('btnRefreshData');
            if (btn) {
                btn.click();
                timerRefresh = setTimeout(updateMonitoring, refreshInterval);
            }
        }
    </script>
    <!-- page scripts -->
  <script src="vendor/flot/jquery.flot.js"></script>
  <script src="vendor/flot/jquery.flot.pie.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script type="text/javascript">
    var data = [{
      label: 'IE',
      data: 34,
      color: $.staticApp.primary
            }, {
      label: 'Safari',
      data: 14,
      color: $.staticApp.info
            }, {
      label: 'Chrome',
      data: 15,
      color: $.staticApp.warning
            }];
    $.plot($('.pie-chart'), data, {
      series: {
        pie: {
          show: true,
          //innerRadius: 0.6,
          stroke: {
            width: 0
          },
          label: {
            show: false,
          }
        }
      },
      legend: {
        show: false
      }
    });
  </script>
  <!-- end initialize page scripts -->

</asp:Content>


    
