﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmSetting_VendingMachine.aspx.vb" Inherits="frmSetting_VendingMachine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem;}
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
      <div class="row">
        <!-- main area -->
        <div class="page-title">
          <div class="title">Setting > Vending Machine</div>
        </div>
         <div class="card bg-white m-b">
          <div class="card-header">Found : 3 Vending Machine</div>
          <div class="card-block p-a-0"> 
          </div>
        </div>
            <div class="row demo-button3">
              <div class="col-sm-3">
                <a class="btn btn-block btn-icon btn-vimeo btn-shadow ripple">
                  <i class="fa fa-plus"></i><b> Add New Vending</b>
                </a>
              </div>
            </div>
           <div class="col-md-12 col-lg-12">
            <div class="row">
              <div class="col-sm-12">
                <div class="card card-block no-border bg-white row-equal align-middle">
                  <div class="column col-lg-3">
                    <img src="images/icon//100/koisk_ok.png" alt="Mountain View" style="width:80px;height:80px;">
                    <h3 class="m-a-0 text-primary">AUTOBOX-PC</h3>
                    <div class="overflow-hidden" style="margin-top:1px;">
                    <h4 class="m-a-0 text-muted">IP : 192.168.1.113</h4>
                    <h6 class="m-a-0 text-muted">ARL Suvanabhumi Station</h6><br /><br />

                <button type="button" class="btn btn-primary btn-sm btn-icon loading-demo mr5">
                  <i class="icon-target mr5"></i>
                  <span>Retime Monitoring</span>
                </button><br />
                <a href="frmEdit_VendingMachine.aspx">
                 <button type="button" class="btn btn-danger btn-sm btn-icon mr5">
                  <i class="fa fa-cog"></i>
                  <span>Chang Configuration</span>
                </button></a><br />
                <button type="button" class="btn btn-info btn-sm btn-icon mr5">
                  <i class="icon-trash"></i>
                  <span>Remove This Machine</span>
                </button>
              </div>
                  </div>
             <div class="column col-lg-3">
             <b class="text-blue">Banknote In Level <i class="pull-right">160/500</i></b>
              <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="500" data-percent="20" style="width:20%">
              </div>
            </div>
            <b class="text-blue">Banknote 20 Level <i class="pull-right">160/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="160" aria-valuemin="0" aria-valuemax="500" data-percent="160" style="width:40%">
              </div>
            </div>
            <b class="text-blue">Coin In Level <i class="pull-right">100/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="500" data-percent="30" style="width:60%">
              </div>
            </div>
            <b class="text-blue">Coin 5 Level <i class="pull-right">200/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="500" data-percent=45" style="width:20%">
              </div>
            </div>
            <b class="text-blue">Banknote 100 Level <i class="pull-right">300/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="500" data-percent="60" style="width:40%">
              </div>
            </div>
             <h4 class="card-title">PRINTER PAPER</h4>
            <b class="text-blue">Printer Level <i class="pull-right">140/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="500" data-percent="40" style="width:60%">
              </div>
            </div>
                  </div>
                  <div class="column col-lg-6">
              <div class="row demo-button3">

                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon//100/coin_in.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Coin In</h6></button>
                   <button type="button" class="btn btn-microsoft ripple">
                       <img src="images/icon/100/Banknotein.png" alt="Mountain View" style="width:30px;height:30px;">
                           <h6>Banknote In</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/coin5.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Coin Out 5</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/20_out.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Banknote Out 20</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/Banknote100.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Banknote Out 100</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/printer.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Printer</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/network.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Network Connection</h6></button>
                  
                  </div>
                </div>
             
               </div>
             </div>
          
         
        <div class="col-sm-12">
                <div class="card card-block no-border bg-white row-equal align-middle">
                  <div class="column col-lg-3">
                    <img src="images/icon//100/koisk_ok.png" alt="Mountain View" style="width:80px;height:80px;">
                    <h3 class="m-a-0 text-primary">TIT-DEVPC</h3>
                    <div class="overflow-hidden" style="margin-top:1px;">
                <h4 class="m-a-0 text-muted">IP : 192.168.1.113</h4>
                <h6 class="m-a-0 text-muted">ARL Suvanabhumi Station</h6><br /><br />

                <button type="button" class="btn btn-primary btn-sm btn-icon loading-demo mr5">
                  <i class="icon-target mr5"></i>
                  <span>Retime Monitoring</span>
                </button><br />
                <button type="button" class="btn btn-danger btn-sm btn-icon mr5">
                  <i class="fa fa-cog"></i>
                  <span>Chang Configuration</span>
                </button><br />
                <button type="button" class="btn btn-info btn-sm btn-icon mr5">
                   <i class="icon-trash"></i>
                  <span>Remove This Machine</span>
                </button>
              </div>
                  </div>
            <div class="column col-lg-3">
             <b class="text-blue">Banknote In Level <i class="pull-right">160/500</i></b>
              <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="500" data-percent="20" style="width:20%">
              </div>
            </div>
            <b class="text-blue">Banknote 20 Level <i class="pull-right">160/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="160" aria-valuemin="0" aria-valuemax="500" data-percent="160" style="width:40%">
              </div>
            </div>
            <b class="text-blue">Coin In Level <i class="pull-right">100/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="500" data-percent="30" style="width:60%">
              </div>
            </div>
            <b class="text-blue">Coin 5 Level <i class="pull-right">200/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="500" data-percent=45" style="width:20%">
              </div>
            </div>
            <b class="text-blue">Banknote 100 Level <i class="pull-right">300/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="500" data-percent="60" style="width:40%">
              </div>
            </div>
             <h4 class="card-title">PRINTER PAPER</h4>
            <b class="text-blue">Printer Level <i class="pull-right">140/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="500" data-percent="40" style="width:60%">
              </div>
            </div>
                  </div>
                 <div class="column col-lg-6">
              <div class="row demo-button3">

                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon//100/coin_in.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Coin In</h6></button>
                   <button type="button" class="btn btn-microsoft ripple">
                       <img src="images/icon/100/Banknotein.png" alt="Mountain View" style="width:30px;height:30px;">
                           <h6>Banknote In</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/coin5.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Coin Out 5</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/20_out.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Banknote Out 20</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/Banknote100.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Banknote Out 100</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/printer.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Printer</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/network.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Network Connection</h6></button>
                  
                  </div>
                </div>
             
               </div>
             </div>
          <div class="col-sm-12">
                <div class="card card-block no-border bg-white row-equal align-middle">
                  <div class="column col-lg-3">
                    <img src="images/icon//100/koisk_ok.png" alt="Mountain View" style="width:80px;height:80px;">
                    <h3 class="m-a-0 text-primary">T-DELL-PC</h3>
                    <div class="overflow-hidden" style="margin-top:1px;">
                <h4 class="m-a-0 text-muted">IP : 192.168.1.113</h4>
                <h6 class="m-a-0 text-muted">ARL Suvanabhumi Station</h6><br /><br />

                <button type="button" class="btn btn-primary btn-sm btn-icon loading-demo mr5">
                  <i class="icon-target mr5"></i>
                  <span>Retime Monitoring</span>
                </button><br />
                <button type="button" class="btn btn-danger btn-sm btn-icon mr5">
                  <i class="fa fa-cog"></i>
                  <span>Chang Configuration</span>
                </button><br />
                <button type="button" class="btn btn-info btn-sm btn-icon mr5">
                   <i class="icon-trash"></i>
                  <span>Remove This Machine</span>
                </button>
               
              </div>
                  </div>
                  <div class="column col-lg-3">
             <b class="text-blue">Banknote In Level <i class="pull-right">160/500</i></b>
              <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="500" data-percent="20" style="width:20%">
              </div>
            </div>
            <b class="text-blue">Banknote 20 Level <i class="pull-right">160/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="160" aria-valuemin="0" aria-valuemax="500" data-percent="160" style="width:40%">
              </div>
            </div>
            <b class="text-blue">Coin In Level <i class="pull-right">100/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="500" data-percent="30" style="width:60%">
              </div>
            </div>
            <b class="text-blue">Coin 5 Level <i class="pull-right">200/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="500" data-percent=45" style="width:20%">
              </div>
            </div>
            <b class="text-blue">Banknote 100 Level <i class="pull-right">300/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="500" data-percent="60" style="width:40%">
              </div>
            </div>
             <h4 class="card-title">PRINTER PAPER</h4>
            <b class="text-blue">Printer Level <i class="pull-right">140/500</i></b>
            <div class="progress progress-striped active m-b">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="500" data-percent="40" style="width:60%">
              </div>
            </div>
                  </div>
                  <div class="column col-lg-6">
              <div class="row demo-button3">

                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon//100/coin_in.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Coin In</h6></button>
                   <button type="button" class="btn btn-microsoft ripple">
                       <img src="images/icon/100/Banknotein.png" alt="Mountain View" style="width:30px;height:30px;">
                           <h6>Banknote In</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/coin5.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Coin Out 5</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/20_out.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Banknote Out 20</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/Banknote100.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Banknote Out 100</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/printer.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Printer</h6></button>
                  <button type="button" class="btn btn-microsoft ripple">
                      <img src="images/icon/100/network.png" alt="Mountain View" style="width:30px;height:30px;">
                          <h6>Network Connection</h6></button>
                  
                  </div>
                </div>
             </div>
          </div>
        </div>
     </div>
       </div>

    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
    <script src="scripts/helpers/colors.js"></script>
    <script src="vendor/Chart.js/Chart.min.js"></script>
    <script src="vendor/flot/jquery.flot.js"></script>
    <script src="vendor/flot/jquery.flot.resize.js"></script>
    <script src="vendor/flot/jquery.flot.categories.js"></script>
    <script src="vendor/flot/jquery.flot.stack.js"></script>
    <script src="vendor/flot/jquery.flot.time.js"></script>
    <script src="vendor/flot/jquery.flot.pie.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendor/d3/d3.min.js" charset="utf-8"></script>
    <script src="vendor/c3/c3.min.js"></script>
    <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
    <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>

    <!-- end page scripts -->
    <!-- initialize page scripts -->
    <script src="scripts/helpers/sameheight.js"></script>
    <script src="scripts/ui/dashboard.js"></script>
    <!-- end initialize page scripts -->
    
    <script type="text/javascript" lang="javascript">

        var timerRefresh;
        var refreshInterval = 60000;  //อัพเดททุกๆ 1 นาที
        function setRefreshMonitoring() {
            timerRefresh = setTimeout(updateMonitoring, refreshInterval);
        }
        $(document).ready(function () {
            setRefreshMonitoring();
        });

        function updateMonitoring() {

            var btn = document.getElementById('btnRefreshData');
            if (btn) {
                btn.click();
                timerRefresh = setTimeout(updateMonitoring, refreshInterval);
            }
        }
    </script>
</asp:Content>
