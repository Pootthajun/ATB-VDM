﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmDashboard.aspx.vb" Inherits="frmDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
      <!-- page stylesheets -->
  <link rel="stylesheet" href="vendor/c3/c3.min.css">
  <!-- end page stylesheets -->

  <style type="text/css">
      .m-t-n-g { margin-top:-1.5rem !important; }

      .notifications .notifications-list li a { padding: 0.5rem; }
  </style>
  <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div class="row">
       <!-- main area -->
  
        <div class="page-title">
          <div class="title">Dashborad</div>
          <div class="sub-title">Detail</div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="card bg-white">
              
              <div class="col-md-3">
              <a href="frmDetailDashboard.aspx">
            <div class="card card-block bg-white p-b-0 no-border">
              
              <div class="text-center m-b-md">
                <h4 class="m-a-0 text-blue">ARL Suvanabhumi Station</h4>
              </div>
              <ul class="list-unstyled m-x-n m-b-0">
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-primary m-r"></i><b>Daily Sales :</b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-info m-r"></i><b>Weekly Sales :</b> 
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-warning m-r"></i><b>Monthly Sales : </b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-success m-r"></i><b>Yearly Sales :</b> 
                </li>
              </ul>
            </div></a>
          </div>
              <div class="col-md-3">
            <div class="card card-block bg-white p-b-0 no-border">
              
              <div class="text-center m-b-md">
                <h4 class="m-a-0 text-blue">ARL Suvanabhumi Station</h4>
              </div>
              <ul class="list-unstyled m-x-n m-b-0">
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-primary m-r"></i><b>Daily Sales :</b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-info m-r"></i><b>Weekly Sales :</b> 
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-warning m-r"></i><b>Monthly Sales : </b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-success m-r"></i><b>Yearly Sales :</b> 
                </li>
              </ul>
            </div>
          </div>
              <div class="col-md-3">
            <div class="card card-block bg-white p-b-0 no-border">
              
              <div class="text-center m-b-md">
                <h4 class="m-a-0 text-blue">ARL Suvanabhumi Station</h4>
              </div>
              <ul class="list-unstyled m-x-n m-b-0">
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-primary m-r"></i><b>Daily Sales :</b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-info m-r"></i><b>Weekly Sales :</b> 
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-warning m-r"></i><b>Monthly Sales : </b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-success m-r"></i><b>Yearly Sales :</b> 
                </li>
              </ul>
            </div>
          </div>
              <div class="col-md-3">
            <div class="card card-block bg-white p-b-0 no-border">
              
              <div class="text-center m-b-md">
                <h4 class="m-a-0 text-blue">ARL Suvanabhumi Station</h4>
              </div>
              <ul class="list-unstyled m-x-n m-b-0">
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-primary m-r"></i><b>Daily Sales :</b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-info m-r"></i><b>Weekly Sales :</b> 
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-warning m-r"></i><b>Monthly Sales : </b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-success m-r"></i><b>Yearly Sales :</b> 
                </li>
              </ul>
            </div>
          </div>
              <div class="col-md-3">
            <div class="card card-block bg-white p-b-0 no-border">
              
              <div class="text-center m-b-md">
                <h4 class="m-a-0 text-blue">ARL Suvanabhumi Station</h4>
              </div>
              <ul class="list-unstyled m-x-n m-b-0">
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-primary m-r"></i><b>Daily Sales :</b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-info m-r"></i><b>Weekly Sales :</b> 
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-warning m-r"></i><b>Monthly Sales : </b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-success m-r"></i><b>Yearly Sales :</b> 
                </li>
              </ul>
            </div>
          </div>
              <div class="col-md-3">
            <div class="card card-block bg-white p-b-0 no-border">
              
              <div class="text-center m-b-md">
                <h4 class="m-a-0 text-blue">ARL Suvanabhumi Station</h4>
              </div>
              <ul class="list-unstyled m-x-n m-b-0">
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-primary m-r"></i><b>Daily Sales :</b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-info m-r"></i><b>Weekly Sales :</b> 
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-warning m-r"></i><b>Monthly Sales : </b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-success m-r"></i><b>Yearly Sales :</b> 
                </li>
              </ul>
            </div>
          </div>
              <div class="col-md-3">
            <div class="card card-block bg-white p-b-0 no-border">
              
              <div class="text-center m-b-md">
                <h4 class="m-a-0 text-blue">ARL Suvanabhumi Station</h4>
              </div>
              <ul class="list-unstyled m-x-n m-b-0">
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-primary m-r"></i><b>Daily Sales :</b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-info m-r"></i><b>Weekly Sales :</b> 
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-warning m-r"></i><b>Monthly Sales : </b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-success m-r"></i><b>Yearly Sales :</b> 
                </li>
              </ul>
            </div>
          </div>
              <div class="col-md-3">
            <div class="card card-block bg-white p-b-0 no-border">
              
              <div class="text-center m-b-md">
                <h4 class="m-a-0 text-blue">ARL Suvanabhumi Station</h4>
              </div>
              <ul class="list-unstyled m-x-n m-b-0">
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-primary m-r"></i><b>Daily Sales :</b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-info m-r"></i><b>Weekly Sales :</b> 
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-warning m-r"></i><b>Monthly Sales : </b>
                </li>
                <li class="b-t p-a-md">
                  <span class="pull-right"><b>-</b></span>
                  <i class="fa fa-circle text-success m-r"></i><b>Yearly Sales :</b> 
                </li>
              </ul>
            </div>
          </div>
            </div>
          </div>
          
        </div>
 
      <!-- /main area -->
        
     </div>
    <asp:Button ID="btnRefreshData" runat="server" style="display:none;" ClientIDMode="Static" /></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    
    <!-- page scripts -->
  <script src="vendor/flot/jquery.flot.js"></script>
  <script src="vendor/flot/jquery.flot.resize.js"></script>
  <script src="vendor/flot/jquery.flot.categories.js"></script>
  <script src="vendor/flot/jquery.flot.stack.js"></script>
  <script src="vendor/flot/jquery.flot.time.js"></script>
  <script src="vendor/flot/jquery.flot.pie.js"></script>
  <script src="vendor/flot-spline/js/jquery.flot.spline.js"></script>
  <script src="vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="scripts/helpers/sameheight.js"></script>
  <script src="scripts/ui/dashboard.js"></script>
  <!-- end initialize page scripts -->
</asp:Content>

